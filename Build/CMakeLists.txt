cmake_minimum_required(VERSION 3.0)
project(Common)

SET (CMAKE_CXX_COMPILER             "/usr/bin/clang++")
SET (CMAKE_CXX_FLAGS " -std=c++1z -Wl -rdynamic -w  -g -Wno-write-strings")
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY "/var/data/out/library") #for shared libraries
SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "/var/data/out/library") #for static libraries
set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CMAKE_COMMAND} -E time") #prints the time of compilation

# Configure CCache if available
find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)

include_directories(
        "/usr/local/include/"
        "/usr/include"
        "/home/vagrant/workspace/Common/src/main/cpp"
        "/home/vagrant/workspace/Common/src/main/cpp/Util"
        "/home/vagrant/workspace/Common/src/main/cpp/concurrency"
        "/home/vagrant/workspace/Common/src/main/cpp/Util/JsonUtil"
        "/home/vagrant/workspace/Common/src/main/cpp/GeneralController"
"/home/vagrant/workspace/Common/src/main/cpp/GeneralController/DataRequest"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels"
        "/home/vagrant/workspace/Common/src/main/test/cpp/Helper"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/campaign" "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/campaign/campaign-offer"
	"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/deal"
      "/home/vagrant/workspace/openrtbmodels/OpenRtbModels"
       "/home/vagrant/workspace/openrtbmodels/OpenRtbModels/src/main/cplusplus/2_3/"
       "/home/vagrant/workspace/openrtbmodels/OpenRtbModels/src/main/cplusplus/2_3/request-response"
"/home/vagrant/workspace/openrtbmodels/OpenRtbModels/src/main/cplusplus/2_3/common-models"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/deal"
      "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/creative"
  "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/net"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modelScore"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/predictive"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/recencyModel"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/devicefeature"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/devicesegment"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/featuredevice"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/OfferPixel"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/OfferPixel/map"
	    "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/threadPool"
      "/home/vagrant/workspace/targetgroup/src/main/cpp/" "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/targetgroup-offer"
       "/home/vagrant/workspace/targetgroup/src/main/cpp"
"/home/vagrant/workspace/targetgroup/src/main/cpp/bidding-performance-metric"
"/home/vagrant/workspace/targetgroup/src/main/cpp/bidding-performance-metric/http-request-model"
"/home/vagrant/workspace/targetgroup/src/main/cpp/bidding-performance-metric/mysql"
"/home/vagrant/workspace/targetgroup/src/main/cpp/bidding-performance-metric/models"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/winning-performance-metric"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/tgStats"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgBwlist"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgCreative"
"/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgMgrsSegment"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgDaypart"

       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeolocation"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeosegment"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeosegmentlist"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgInventory"
       "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgRecencyModel"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/offersegment"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/segmentgroup"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/segmentgroup_segment_map"
	    "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/threadPool"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup-delivery-info"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup-delivery-info/hourly"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup-delivery-info/daily"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Pacing"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Metric"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/validation-services"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/common-modules"
        "/home/vagrant/workspace/Common/src/main/cpp/config"
        "/home/vagrant/workspace/Common/src/main/cpp/Kafka"
        "/home/vagrant/workspace/Common/src/main/cpp/gumbo"
        "/home/vagrant/workspace/Common/src/main/cpp/cassandra"
        "/home/vagrant/workspace/Common/src/main/cpp/aerospike"
        "/home/vagrant/workspace/Common/src/main/cpp/recency" "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/device"
        "/home/vagrant/workspace/Common/src/main/cpp/mysql"
        "/home/vagrant/workspace/Common/src/main/cpp/mysql/targetgroup_maps"
        "/home/vagrant/workspace/Common/src/main/cpp/GeneralController"
        "/home/vagrant/workspace/Common/src/main/cpp/GeneralController/DataRequest"
        "/home/vagrant/workspace/Common/src/main/cpp/Util/Caching"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/adhistory"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/wiretap"

"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/client"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/advertiser"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/clusterer"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/bad-device"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/adserver"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/bwlist"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/whitelistedBiddingDomains"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/whitelistedModelingDomains"

        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/counter"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/eventlog"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/geo"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/userAgentParser"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/geo/places"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/inventory"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/http"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/ipdeviceidmap"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/device"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/GicapodsIdToExchangeIdsMap"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-container"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-container"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-realtime"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-container"


        "src"
        "/home/vagrant/workspace/Common/src/main/cpp/GeneralController"
        "/home/vagrant/workspace/Common/src/main/cpp/cassandra"
        "/home/vagrant/workspace/Common/src/main/cpp/mysql"
        "/home/vagrant/workspace/Common/src/main/cpp/mysql/targetgroup_maps"
        "/home/vagrant/workspace/Common/src/main/cpp/Kafka"
        "/home/vagrant/workspace/Common/src/main/cpp/concurrency"
        "/home/vagrant/workspace/Common/src/main/cpp/cassandra/Model"
        "/home/vagrant/workspace/Common/src/main/cpp/cassandra/Adserver"
        "/home/vagrant/workspace/Common/src/main/cpp/cassandra/Bidder"
        "/home/vagrant/workspace/Common/src/main/cpp/config"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Metric")

link_directories("/usr/local/lib"
                 "/var/data/out/library"
                 "/usr/lib/x86_64-linux-gnu/"
                 "/usr/lib")



FILE(GLOB GeneralControllers_SOURCE_FILES
"/home/vagrant/workspace/Common/src/main/cpp/GeneralController/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/GeneralController/DataRequest/*.cpp"
)

FILE(GLOB MyConcurrency_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/concurrency/*.cpp")

FILE(GLOB MyGeneralUtilLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/Util/*.cpp")

FILE(GLOB TestHelperLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Tests/Helper/*.cpp")

FILE(GLOB MyKafkaLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/Kafka/*.cpp")
FILE(GLOB MyGumboLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/gumbo/*.cpp")

FILE(GLOB MyConfigLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/config/*.cpp")

FILE(GLOB MyCampaignLib_SOURCE_FILES
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/campaign/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/campaign/campaign-offer/*.cpp"
)

FILE(GLOB MyDealLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/deal/*.cpp")

FILE(GLOB MyCreativeLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/creative/*.cpp")
FILE(GLOB MyNetLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/net/*.cpp")

FILE(GLOB MyOfferPixelLib_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/OfferPixel/*.cpp" "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/OfferPixel/map/*.cpp")

FILE(GLOB MyModelingLib_SOURCE_FILES
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/predictive/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/recencyModel/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/devicefeature/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/devicesegment/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/featuredevice/*.cpp"
)

FILE(GLOB NomadiniModelScoreLib_SOURCE_FILES
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modelScore/*.cpp"
)


FILE(GLOB MyMetrics_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Metric/*.cpp")
FILE(GLOB MyValidationServices_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/validation-services/*.cpp")

FILE(GLOB MyDevice_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/device/*.cpp")
FILE(GLOB AERO_SPIKE_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/aerospike/*.cpp" "/home/vagrant/workspace/Common/src/main/cpp/aerospike/*.c")


FILE(GLOB CACHE_CONTAINER_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-container/*.cpp")

FILE(GLOB MyGeneralObjectModelsLib_SOURCE_FILES
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/*.cpp"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Pacing/*.cpp")

FILE(GLOB COMMON_MODULES_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/common-modules/*.cpp")

FILE(GLOB MyRecency_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/recency/*.cpp")
FILE(GLOB AdHistory_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/adhistory/*.cpp")
FILE(GLOB Wiretap_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/wiretap/*.cpp")

FILE(GLOB Client_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/client/*.cpp")
FILE(GLOB Advertiser_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/advertiser/*.cpp")
FILE(GLOB Clusterer_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/clusterer/*.cpp")
FILE(GLOB BadDevice_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/bad-device/*.cpp")
FILE(GLOB Adserver_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/adserver/*.cpp")
FILE(GLOB BwList_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/bwlist/*.cpp")

FILE(GLOB whitelistedBiddingDomains_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/whitelistedBiddingDomains/*.cpp")
FILE(GLOB whitelistedModelingDomains_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/whitelistedModelingDomains/*.cpp")

FILE(GLOB Counter_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/counter/*.cpp")
FILE(GLOB Eventlog_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/eventlog/*.cpp")
FILE(GLOB Geo_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/geo/*.cpp" "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/geo/places/*.cpp")
FILE(GLOB userAgentParser_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/userAgentParser/*.cpp")
FILE(GLOB Inventory_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/inventory/*.cpp")

FILE(GLOB Http_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/http/*.cpp")
FILE(GLOB Ipdeviceidmap_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/ipdeviceidmap/*.cpp")
FILE(GLOB GicapodsIdToExchangeIdsMap_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/GicapodsIdToExchangeIdsMap/*.cpp")
FILE(GLOB Cassandra_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/cassandra/*.cpp")
FILE(GLOB MySql_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/mysql/*.cpp")


FILE(GLOB segment_SOURCE_FILES
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/segmentgroup/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/offersegment/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/segmentgroup_segment_map/*.cpp"
)

FILE(GLOB threadPool_SOURCE_FILES  "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/threadPool/*.cpp")

FILE(GLOB tgRealtime_SOURCE_FILES
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup-delivery-info/daily/*.cpp"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup-delivery-info/hourly/*.cpp"
)


FILE(GLOB MyCacheRealTime_SOURCE_FILES "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-realtime/*.cpp")

#add_library(CommonModulesLib STATIC ${COMMON_MODULES_FILES})
add_library(CommonModulesLib STATIC ${COMMON_MODULES_FILES})

add_library(MyGeneralUtilLib STATIC ${MyGeneralUtilLib_SOURCE_FILES})
add_library(MyCacheContainer STATIC ${CACHE_CONTAINER_SOURCE_FILES})

add_library(MyBwListLib STATIC ${BwList_SOURCE_FILES})
add_library(MyWhitelistedBiddingDomains  STATIC ${whitelistedBiddingDomains_SOURCE_FILES})
add_library(MywhitelistedModelingDomains  STATIC ${whitelistedModelingDomains_SOURCE_FILES})

#these are fast changing libraries , comment and uncomment based on your needs



add_library(MyMetricsLib STATIC ${MyMetrics_SOURCE_FILES})
add_library(MyValidationServicesLib STATIC ${MyValidationServices_SOURCE_FILES})
add_library(MyDeviceLib STATIC ${MyDevice_SOURCE_FILES})

add_library(MyGeneralObjectModelsLib STATIC ${MyGeneralObjectModelsLib_SOURCE_FILES})


add_library(MyClientLib STATIC ${Client_SOURCE_FILES})
add_library(MyAdvertiserLib STATIC ${Advertiser_SOURCE_FILES})
add_library(MyClustererLib STATIC ${Clusterer_SOURCE_FILES})
add_library(MyBadDeviceLib STATIC ${BadDevice_SOURCE_FILES})

add_library(MyCacheRealTimeLib STATIC ${MyCacheRealTime_SOURCE_FILES})

add_library(MyCampaignLib STATIC ${MyCampaignLib_SOURCE_FILES})

add_library(MyDealLib STATIC ${MyDealLib_SOURCE_FILES})

add_library(MyCreativeLib STATIC ${MyCreativeLib_SOURCE_FILES})
add_library(MyNetLib STATIC ${MyNetLib_SOURCE_FILES})


add_library(nomadiniThreadPool STATIC ${threadPool_SOURCE_FILES})

add_library(MyGeoLib STATIC ${Geo_SOURCE_FILES})
add_library(nomadiniUserAgentParser STATIC ${userAgentParser_SOURCE_FILES})


#these are rarely changing libraries, uncomment them when you need to rebuild them
add_library(MyModelingLib STATIC ${MyModelingLib_SOURCE_FILES})
add_library(NomadiniModelScoreLib STATIC ${NomadiniModelScoreLib_SOURCE_FILES})
add_library(MyRecency STATIC ${MyRecency_SOURCE_FILES})
add_library(MyAdHistory STATIC ${AdHistory_SOURCE_FILES})
add_library(wiretap STATIC ${Wiretap_SOURCE_FILES})
add_library(MySqlLib STATIC ${MySql_SOURCE_FILES})
add_library(segment STATIC ${segment_SOURCE_FILES})
add_library(tgRealtime  STATIC ${tgRealtime_SOURCE_FILES})

add_library(MyAdserverLib STATIC ${Adserver_SOURCE_FILES})
add_library(MyEventLogLib STATIC ${Eventlog_SOURCE_FILES})

add_library(MyInventoryLib STATIC ${Inventory_SOURCE_FILES})
add_library(MyHttpLib STATIC ${Http_SOURCE_FILES})
add_library(MyIpToDeviceIdsMapLib STATIC ${Ipdeviceidmap_SOURCE_FILES})

add_library(GicapodsIdToExchangeIdsMapLib STATIC ${GicapodsIdToExchangeIdsMap_SOURCE_FILES})

add_library(MyCassandraLib STATIC ${Cassandra_SOURCE_FILES})
add_library(MyAerospike STATIC ${AERO_SPIKE_SOURCE_FILES})
add_library(MyKafkaLib STATIC ${MyKafkaLib_SOURCE_FILES})
add_library(MyGumboLib STATIC ${MyGumboLib_SOURCE_FILES})

add_library(MyConfigLib STATIC ${MyConfigLib_SOURCE_FILES})
add_library(MyOfferPixelLib STATIC ${MyOfferPixelLib_SOURCE_FILES})
add_library(MyConcurrencyLib STATIC ${MyConcurrency_SOURCE_FILES})
add_library(GeneralControllers STATIC ${GeneralControllers_SOURCE_FILES})


#this is absolutely required for every library to be able to see their source code line informations
#if any exception happen there
set_property(TARGET MyGeneralUtilLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET CommonModulesLib PROPERTY COMPILE_FLAGS "-g")

set_property(TARGET tgRealtime  PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyCacheRealTimeLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyDealLib PROPERTY COMPILE_FLAGS "-g")

set_property(TARGET MyMetricsLib PROPERTY COMPILE_FLAGS "-g")

set_property(TARGET MyGeneralObjectModelsLib PROPERTY COMPILE_FLAGS "-g")

set_property(TARGET MyClientLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyAdvertiserLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyClustererLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyBadDeviceLib PROPERTY COMPILE_FLAGS "-g")


set_property(TARGET MyCampaignLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyCreativeLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyNetLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyGeoLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET nomadiniUserAgentParser PROPERTY COMPILE_FLAGS "-g")


#these are rarely changing libraries, uncomment them when you need to rebuild them
set_property(TARGET MyBwListLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyWhitelistedBiddingDomains PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MywhitelistedModelingDomains PROPERTY COMPILE_FLAGS "-g")


set_property(TARGET MyModelingLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET NomadiniModelScoreLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyAdHistory PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MySqlLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET segment PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET tgRealtime  PROPERTY COMPILE_FLAGS "-g")

set_property(TARGET MyAdserverLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyEventLogLib PROPERTY COMPILE_FLAGS "-g")

set_property(TARGET MyInventoryLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyHttpLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyIpToDeviceIdsMapLib PROPERTY COMPILE_FLAGS "-g")

set_property(TARGET MyCassandraLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyAerospike PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyKafkaLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyGumboLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyConfigLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyOfferPixelLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET MyConcurrencyLib PROPERTY COMPILE_FLAGS "-g")
set_property(TARGET GeneralControllers PROPERTY COMPILE_FLAGS "-g")


#cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja
