cmake_minimum_required(VERSION 3.0)
project(Scorer)

SET (CMAKE_CXX_COMPILER             "/usr/bin/clang++")
SET (CMAKE_CXX_FLAGS " -std=c++1z -Wl -rdynamic -w  -g  -Wno-write-strings")
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY "/var/data/out/library") #for shared libraries
SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "/var/data/out/library") #for static libraries
set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CMAKE_COMMAND} -E time") #prints the time of compilation

# Configure CCache if available
find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)

include_directories(
        "/usr/local/include/"
        "/usr/include"
        "/home/vagrant/workspace/Common/src/main/cpp"
        "/home/vagrant/workspace/Common/src/main/cpp/Util"
        "/home/vagrant/workspace/Common/src/main/cpp/concurrency"
        "/home/vagrant/workspace/Common/src/main/cpp/Util/JsonUtil"
        "/home/vagrant/workspace/Common/src/main/cpp/GeneralController"
"/home/vagrant/workspace/Common/src/main/cpp/GeneralController/DataRequest"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/common-modules"
        "/home/vagrant/workspace/Common/src/main/test/cpp/Helper"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/campaign" "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/campaign/campaign-offer"
	"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/deal"
      "/home/vagrant/workspace/openrtbmodels/OpenRtbModels"
      "/home/vagrant/workspace/openrtbmodels/OpenRtbModels/src/main/cplusplus/2_3/"
"/home/vagrant/workspace/openrtbmodels/OpenRtbModels/src/main/cplusplus/2_3/request-response"
"/home/vagrant/workspace/openrtbmodels/OpenRtbModels/src/main/cplusplus/2_3/common-models"
      "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/creative"
  "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/net"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modelScore"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/predictive"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/recencyModel"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/devicefeature"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/devicesegment"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/modeling/featuredevice"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/OfferPixel"   "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/OfferPixel/map"
        "/home/vagrant/workspace/openrtbmodels/OpenRtbModels"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/offersegment"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/segmentgroup"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/segmentgroup_segment_map"
	    "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/threadPool"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup"
        "/home/vagrant/workspace/targetgroup/src/main/cpp"
"/home/vagrant/workspace/targetgroup/src/main/cpp/bidding-performance-metric"
"/home/vagrant/workspace/targetgroup/src/main/cpp/bidding-performance-metric/http-request-model"
"/home/vagrant/workspace/targetgroup/src/main/cpp/bidding-performance-metric/mysql"
"/home/vagrant/workspace/targetgroup/src/main/cpp/bidding-performance-metric/models"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/winning-performance-metric"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/tgStats"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgBwlist"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgCreative"
"/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgMgrsSegment"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgDaypart"
        
        "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeolocation"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeosegment"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeosegmentlist"
        "/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgInventory"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/offersegment"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/segmentgroup"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/segment/segmentgroup_segment_map"
	    "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/threadPool"

"/home/vagrant/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgRecencyModel"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup-delivery-info"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup-delivery-info/hourly"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/targetgroup-delivery-info/daily"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Pacing"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Metric"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/validation-services"
        "/home/vagrant/workspace/Common/src/main/cpp/config"
        "/home/vagrant/workspace/Common/src/main/cpp/Kafka"  "/home/vagrant/workspace/Common/src/main/cpp/gumbo"
        "/home/vagrant/workspace/Common/src/main/cpp/cassandra"
        "/home/vagrant/workspace/Common/src/main/cpp/aerospike"
        "/home/vagrant/workspace/Common/src/main/cpp/recency" "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/device"
        "/home/vagrant/workspace/Common/src/main/cpp/mysql"
        "/home/vagrant/workspace/Common/src/main/cpp/mysql/targetgroup_maps"
        "/home/vagrant/workspace/Common/src/main/cpp/GeneralController"
"/home/vagrant/workspace/Common/src/main/cpp/GeneralController/DataRequest"
        "/home/vagrant/workspace/Common/src/main/cpp/Util/Caching"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/adhistory"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/wiretap"

"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/client"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/advertiser"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/clusterer"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/clusterer"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/bad-device"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/adserver"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/bwlist"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/whitelistedBiddingDomains"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/whitelistedModelingDomains"

        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/counter"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/eventlog"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/geo"
         "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/geo/places"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/inventory"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/http"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/ipdeviceidmap"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/GicapodsIdToExchangeIdsMap"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-container"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-container"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/cache-realtime"

        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/Metric"
"/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/validation-services"
        "/home/vagrant/workspace/Common/src/main/cpp/ObjectModels/common-modules"


        "/home/vagrant/workspace/Common/PropertyServer/src/main/cpp/"
        )


        FILE(GLOB SOURCE_FILES_FOR_LIB
                "/home/vagrant/workspace/Common/PropertyServer/src/main/cpp/*.cpp"
                )

        link_directories(
                "/usr/local/lib"
                "/usr/lib/x86_64-linux-gnu/"
                "/home/vagrant/workspace/Common/"
                "/usr/lib")


FILE(GLOB SOURCE_FILES
  "/home/vagrant/workspace/Common/PropertyServer/src/main/cpp/*.cpp"
  "/home/vagrant/workspace/Common/PropertyServer/src/main/cpp/app/*.cpp"
)
link_directories(
        "/usr/local/lib"
          "/var/data/out/library"
        "/usr/lib/x86_64-linux-gnu/"
        "/home/vagrant/workspace/Common/"
        "/usr/lib")

add_executable(PropertyServer.out ${SOURCE_FILES})
TARGET_LINK_LIBRARIES(PropertyServer.out LINK_PUBLIC
"segment"
"tgBwlist"

"tgMgrsSegment" "tgCreative"
"tgDaypart" "targetGroup"

"tgGeolocation"
"tgGeosegment"
"tgGeosegmentlist"
"tgInventory" "tgRecencyModel"
"tgRealtime"
"MyRecency"
"GeneralControllers" "NomadiniModelScoreLib" "MyCassandraLib"
"MyCampaignLib"
"MyCreativeLib"
"MyCacheContainer"
"CommonModulesLib"
"MyConfigLib"
"MyGeneralUtilLib"
"MyModelingLib"
"MyOfferPixelLib"
"MyMetricsLib"
"MyValidationServicesLib"

 "MyBadDeviceLib"
"MyCacheRealTimeLib" "Geographic"

"MyDealLib"
"segment"

"MyGeneralObjectModelsLib"
"MyEventLogLib"
"MyGeoLib"
"MyAdHistory" "wiretap"
"MyBwListLib" "MyWhitelistedBiddingDomains" "MywhitelistedModelingDomains"
"MyClientLib"
"MyAdvertiserLib" "MyClustererLib"
 "MyBadDeviceLib"
"MyCacheRealTimeLib" "Geographic"

"MyDealLib"
"tgBwlist"
"tgMgrsSegment" "tgCreative"
"MyDeviceLib"
"MyModelingLib"
"targetGroup"
"MyCreativeLib" "MyCacheContainer" "CommonModulesLib"
"MyInventoryLib"
"MyHttpLib"
"MyDeviceLib"
"MyKafkaLib"
"MyAerospike"
"aerospike"
"ev"
"dl"
"crypto"

"MySqlLib"
"NomadiniModelScoreLib" "MyCassandraLib"
"GicapodsIdToExchangeIdsMapLib"
"MyIpToDeviceIdsMapLib"
"MyConcurrencyLib" "MyNetLib" "MyDeviceLib"
"glog"
"gtest"
"gmock"
"tbb"
"maxminddb"
"unwind"
"nomadiniThreadPool"
"pthread"
"boost_random"
"boost_system"
"boost_thread"
"boost_regex"
"boost_program_options"
"PocoNet"
"PocoFoundation" "PocoNetSSL"

"PocoUtil"
"boost_filesystem"
"boost_date_time"
"mysqlcppconn"
"cassandra_static" "cassandra" "uv"
"curl" "ssl"
"crypto"
"rdkafka++"
"rdkafka"
        )

#cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja
