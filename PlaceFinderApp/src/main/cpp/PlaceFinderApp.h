/*
 * PlaceFinderApp.h
 *
 *  Created on: Aug 26, 2015
 *      Author: mtaabodi
 */

#ifndef PlaceFinderApp_H_
#define PlaceFinderApp_H_
#include <memory>
#include "ConfigService.h"
#include <string>
class PlaceFinderApp;
class PlaceFinderModule;
class MySqlPlaceService;
class MySqlTagService;
class MySqlTagPlaceService;

class PlaceFinderApp {

public:
void populateMapAsync(
        std::shared_ptr<PlaceFinderModule> placeFinderModule,
        MySqlPlaceService* mySqlPlaceService,
        MySqlTagService* mySqlTagService,
        MySqlTagPlaceService* mySqlTagPlaceService,
        gicapods::ConfigService* configService);

void startTheApp(int argc, char* argv[]);
void processArguments(
        int argc,
        char* argv[],
        std::string& propertyFileName,
        unsigned short& port,
        std::string& appName,
        std::string& logDirectory,
        std::string& appVersion);
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_BIDDINGMODE_H_ */
