#ifndef PlaceFinderRequestHandler_H
#define PlaceFinderRequestHandler_H

#include "SignalHandler.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "Object.h"
class PlaceFinderModule;
class Place;
class PlaceFinderRequestHandler;

#include "MaxMindService.h"

class PlaceFinderRequestHandler : public Poco::Net::HTTPRequestHandler, public Object {

public:
EntityToModuleStateStats* entityToModuleStateStats;

PlaceFinderModule* placeFinderModule;
std::shared_ptr<gicapods::AtomicLong> logResponseOneInEveryXRequests;
PlaceFinderRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats);

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

std::string convertArrayOfPlacesToJson(std::vector<std::shared_ptr<Place> > finalPlaces);
std::string calculatePlacesFromSpec(double lat, double lon, int maxNumebrOfWantedFeatures);

virtual ~PlaceFinderRequestHandler();
};

#endif
