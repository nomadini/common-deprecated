
#include "PlaceFinderRequestHandler.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "StringUtil.h"
#include "ConverterUtil.h"
#include "HttpUtil.h"
#include "StatisticsUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "Place.h"
#include "GUtil.h"
#include "PlaceFinderModule.h"
#include "ExceptionUtil.h"
#include "EntityToModuleStateStats.h"

PlaceFinderRequestHandler::PlaceFinderRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;


}

void PlaceFinderRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                              Poco::Net::HTTPServerResponse &response) {
        try {

                entityToModuleStateStats->addStateModuleForEntity ("numberOfBidsRecieved",
                                                                   "PlaceFinderRequestHandler",
                                                                   "ALL");
                auto requestInJson = HttpUtil::getRequestBody (request);
                auto doc = parseJsonSafely(requestInJson);
                double lat = JsonUtil::GetDoubleSafely(*doc,"lat");
                double lon = JsonUtil::GetDoubleSafely(*doc,"lon");
                int maxNumebrOfWantedFeatures = JsonUtil::GetIntSafely(*doc,"maxNumebrOfWantedFeatures");


                auto finalResponseContent =
                        calculatePlacesFromSpec(lat, lon, maxNumebrOfWantedFeatures);
                std::ostream &ostr = response.send ();
                ostr << finalResponseContent;

                LOG_EVERY_N(INFO, logResponseOneInEveryXRequests->getValue())<< google::COUNTER<<"th finalResponseContent : "<< finalResponseContent;
        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                   "PlaceFinderRequestHandler",
                                                                   "ALL");
        } catch (...) {
                entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                   "PlaceFinderRequestHandler",
                                                                   "ALL");
        }

        HttpUtil::setEmptyResponse(response);
}

std::string PlaceFinderRequestHandler::calculatePlacesFromSpec(
        double lat, double lon, int maxNumebrOfWantedFeatures) {

        std::vector<std::shared_ptr<Place> >
        places = placeFinderModule->
                 findIntersectingFeatures(lat,
                                          lon,
                                          maxNumebrOfWantedFeatures);

        std::vector<std::shared_ptr<Place> > places2 =
                placeFinderModule->
                findNearestFeatures(
                        lat,
                        lon,
                        maxNumebrOfWantedFeatures);

        auto finalPlaces = CollectionUtil::combineTwoLists(places, places2);
        auto finalResponseContent = convertArrayOfPlacesToJson(finalPlaces);
        return finalResponseContent;
}

std::string PlaceFinderRequestHandler::convertArrayOfPlacesToJson(
        std::vector<std::shared_ptr<Place> > finalPlaces) {
        std::string finalResponseContent;
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonArrayUtil::addMemberToValue_FromPair(
                doc.get(),
                "places",
                finalPlaces,
                *doc);
        return JsonUtil::valueToString (*doc);
}

PlaceFinderRequestHandler::~PlaceFinderRequestHandler() {
}
