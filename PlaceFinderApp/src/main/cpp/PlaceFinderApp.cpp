#include "GUtil.h"
#include "StringUtil.h"
#include "PlaceFinderApp.h"
#include "ConfigService.h"
#include "TempUtil.h"
#include "ConverterUtil.h"
#include "PlaceFinder.h"
#include "BeanFactory.h"
#include "PlaceFinderModule.h"
#include "TreeOfPlacesHolder.h"
#include <thread>

#include "MySqlPlaceService.h"
#include "MySqlTagPlaceService.h"
#include "MySqlTagService.h"
#include "PlaceFinderModule.h"
#include "ObjectGlobalMapContainer.h"
#include "Place.h"
#include "PlaceFinderRequestHandlerFactory.h"
#include "ProgramOptionParser.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"
#include "Tag.h"

void PlaceFinderApp::processArguments(
        int argc,
        char* argv[],
        std::string& propertyFileName,
        unsigned short& port,
        std::string& appName,
        std::string& logDirectory,
        std::string& appVersion) {


        //The default_value will be used when the option is not specified at all. The implicit_value will be used when the option is specific without a value. If a value is specified, it will override the default and implicit.
        namespace po = boost::program_options;
        boost::program_options::options_description desc("Allowed options");
        desc.add_options()
        // First parameter describes option name/short name
        // The second is parameter to option
        // The third is description
                ("help,h", "print usage message")
                ("appname,a",
                po::value(&appName)->default_value("PlaceFinder"), "app name")
                ("property,p", boost::program_options::value(&propertyFileName)->default_value("placeFinder.properties"), "PlaceFinderApp property file name")
                ("port,t", boost::program_options::value(&port)->default_value(1), "PlaceFinderApp port")
                ("version,v", boost::program_options::value(&appVersion)->default_value("SNAPSHOT"), "version")
                ("log_dir,l", boost::program_options::value(&logDirectory)->default_value("/var/log/"), "PlaceFinderApp log directory location");

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
                std::cout << desc << "\n";
                return;
        }


        std::cout << "property = " << vm["property"].as<std::string>() << "\n";
        std::cout << "port = " << vm["port"].as<unsigned short>() << "\n";
        std::cout << "appname = " << vm["appname"].as<std::string>() << "\n";

        //THE REAL ASSIGNMENT HAPPENS HERE !!
        //THE REAL ASSIGNMENT HAPPENS HERE !!
        //THE REAL ASSIGNMENT HAPPENS HERE !!
        appName = vm["appname"].as<std::string>();
        appVersion = vm["version"].as<std::string>();
        propertyFileName = vm["property"].as<std::string>();
        port = vm["port"].as<unsigned short>();
}

void PlaceFinderApp::populateMapAsync(
        std::shared_ptr<PlaceFinderModule> placeFinderModule,
        MySqlPlaceService* mySqlPlaceService,
        MySqlTagService* mySqlTagService,
        MySqlTagPlaceService* mySqlTagPlaceService,
        gicapods::ConfigService* configService) {


        std::vector<std::shared_ptr<Tag> > allTags = mySqlTagService->readAll();
        auto tagMap =
                std::make_shared<gicapods::ConcurrentHashMap<int, Tag> >();
        for (auto tag : allTags) {
                tagMap->put(tag->id, tag);
        }
        std::vector<std::shared_ptr<TagPlace> > allTagPlaces = mySqlTagPlaceService->readAll();


        for (auto tagPlace : allTagPlaces) {
                auto tagHolder = placeFinderModule->treeOfPlacesHolder->placeIdsToTags->getOptional(tagPlace->placeId);
                if (tagHolder == nullptr) {
                        tagHolder = std::make_shared<ObjectVectorHolder<Tag> >();
                        placeFinderModule->treeOfPlacesHolder->placeIdsToTags->put(tagPlace->placeId, tagHolder);
                }
                tagHolder->values->push_back(tagMap->get(tagPlace->tagId));
        }

        LOG(INFO)<<"started loading the treeOfPlaces";

        mySqlPlaceService->readIntoMap(placeFinderModule->treeOfPlacesHolder);

        LOG(INFO)<<"finished loading the treeOfPlaces , size : "<<
                placeFinderModule->treeOfPlacesHolder->getSize();

}

void PlaceFinderApp::startTheApp(int argc, char* argv[]) {

        std::string appName = "";
        std::string appVersion = "";
        std::string logDirectory = "";
        std::string propertyFileName = "placeFinderApp.properties";
        unsigned short placeFinderAppPort = 1;

        processArguments(argc,
                         argv,
                         propertyFileName,
                         placeFinderAppPort,
                         appName,
                         logDirectory,
                         appVersion);


        TempUtil::configureLogging (appName, argv);
        LOG(ERROR) << appName <<" starting with properties "
                   << propertyFileName <<std::endl
                   <<" original arguments : argc "
                   << argc << " argv : " <<  *argv;

        auto beanFactory = std::make_unique<BeanFactory>(appVersion);
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;
        beanFactory->initializeModules();

        auto placeIdsToTags =
                std::make_shared<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Tag> > >();

        auto treeOfPlacesHolder = std::make_shared<TreeOfPlacesHolder>(placeIdsToTags);

        auto placeFinderModule = std::make_shared<PlaceFinderModule>(treeOfPlacesHolder);
        std::thread populateMapAsyncThread (
                &PlaceFinderApp::populateMapAsync, this,
                placeFinderModule,
                beanFactory->mySqlPlaceService.get(),
                beanFactory->mySqlTagService.get(),
                beanFactory->mySqlTagPlaceService.get(),
                beanFactory->configService.get());

        populateMapAsyncThread.detach ();


        placeFinderModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        auto placeFinderRequestHandlerFactory = std::make_shared<PlaceFinderRequestHandlerFactory>();
        placeFinderRequestHandlerFactory->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        placeFinderRequestHandlerFactory->configService = beanFactory->configService.get();
        placeFinderRequestHandlerFactory->placeFinderModule = placeFinderModule.get();
        placeFinderRequestHandlerFactory->logResponseOneInEveryXRequests = std::make_shared<gicapods::AtomicLong>(
                beanFactory->configService->getAsInt("logResponseOneInEveryXRequests")
                );
        placeFinderRequestHandlerFactory->init();

        auto placeFinder = std::make_shared<PlaceFinder>();
        placeFinder->placeFinderRequestHandlerFactory = placeFinderRequestHandlerFactory.get();
        placeFinder->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        placeFinder->configService = beanFactory->configService.get();
        placeFinder->entityToModuleStateStatsPersistenceService = beanFactory->entityToModuleStateStatsPersistenceService.get();
        placeFinder->beanFactory = std::move(beanFactory);

        placeFinder->run(argc, argv);
}
