
#include "StringUtil.h"
#include "TempUtil.h"

#include "GUtil.h"
#include "UnknownRequestHandler.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>
#include "ConfigService.h"
#include "TargetGroup.h"
#include "MaxmindHttpProxyRequestHandler.h"
#include "MaxmindHttpProxyRequestHandlerFactory.h"
#include "NetworkUtil.h"
#include "HttpUtil.h"
MaxmindHttpProxyRequestHandlerFactory::MaxmindHttpProxyRequestHandlerFactory() : Object(__FILE__) {

}

void MaxmindHttpProxyRequestHandlerFactory::init() {

        maxMindService->openDatabase(StringUtil::toStr(maxmindCityDbFilePath.c_str()));
}


Poco::Net::HTTPRequestHandler
*MaxmindHttpProxyRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {

        try {


                MLOG(10) << "MaxmindHttpProxyRequestHandlerFactory : request uri to process : " << request.getURI ();

                if (StringUtil::containsCaseInSensitive (request.getURI (), "/maxmind-data-fetch/")) {
                        auto maxmindHttpProxyRequestHandler = new MaxmindHttpProxyRequestHandler (entityToModuleStateStats);
                        maxmindHttpProxyRequestHandler->maxMindService = maxMindService;
                        return maxmindHttpProxyRequestHandler;
                }

        } catch (...) {
                LOG(ERROR) <<"unknow exception happened";
        }
        return new UnknownRequestHandler (entityToModuleStateStats);
}
