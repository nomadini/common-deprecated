#ifndef MaxmindHttpProxyRequestHandler_H
#define MaxmindHttpProxyRequestHandler_H

#include "SignalHandler.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "Object.h"
class MaxmindHttpProxyRequestHandler;

#include "MaxMindService.h"

class MaxmindHttpProxyRequestHandler : public Poco::Net::HTTPRequestHandler {

public:
EntityToModuleStateStats* entityToModuleStateStats;

MaxMindService* maxMindService;
MaxmindHttpProxyRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats);

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

virtual ~MaxmindHttpProxyRequestHandler();
};

#endif
