#ifndef MaxmindHttpProxy_H_
#define MaxmindHttpProxy_H_

#include <memory>
#include <string>
#include "MaxmindHttpProxyRequestHandlerFactory.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
#include "Object.h"
#include "AtomicBoolean.h"
class EntityToModuleStateStatsPersistenceService;
class DataReloadService;
namespace gicapods { class ConfigService; }

#include <boost/thread.hpp>
#include <thread>
#include "PocoHttpServer.h"
class ComparatorService;
class TgFilterMeasuresService;
class BeanFactory;
class ModuleContainer;
class MaxmindHttpProxy;



class MaxmindHttpProxy :
        public std::enable_shared_from_this<MaxmindHttpProxy>,
        public Poco::Util::ServerApplication
{

public:
gicapods::ConfigService* configService;
EntityToModuleStateStats* entityToModuleStateStats;
std::unique_ptr<BeanFactory> beanFactory;
MaxmindHttpProxyRequestHandlerFactory* maxmindHttpProxyRequestHandlerFactory;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;

MaxmindHttpProxy();
std::shared_ptr<Poco::Net::HTTPServer> httpServer;

void runEveryMinute();

virtual ~MaxmindHttpProxy();
void init();
std::string getName();
int main(const std::vector<std::string> &args);

private:
bool _helpRequested;
};

#endif
