#ifndef MaxmindHttpProxyRequestHandlerFactory_H
#define MaxmindHttpProxyRequestHandlerFactory_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include <unordered_map>
class OpportunityContext;
class BidRequestHandlerPipelineProcessor;
class EntityToModuleStateStats;
class TargetGroup;
class BidRequestHandler;
class CommonRequestHandlerFactory;
namespace gicapods { class ConfigService; }
class BidModeControllerService;
class TgBiddingPerformanceMetricInBiddingPeriodService;
#include "AtomicLong.h"
#include "AtomicBoolean.h"
#include "AtomicDouble.h"
#include "Poco/Logger.h"
#include "Object.h"
class MaxmindHttpProxyRequestHandlerFactory;

#include "MaxMindService.h"


class MaxmindHttpProxyRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory, public Object {

public:
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::ConfigService* configService;
std::shared_ptr<MMDB_s> mmdb;
MaxMindService* maxMindService;
std::string maxmindCityDbFilePath;
MaxmindHttpProxyRequestHandlerFactory();
void init();
Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request);

};

#endif
