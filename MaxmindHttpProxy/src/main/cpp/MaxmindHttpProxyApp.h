/*
 * MaxmindHttpProxyApp.h
 *
 *  Created on: Aug 26, 2015
 *      Author: mtaabodi
 */

#ifndef MaxmindHttpProxyApp_H_
#define MaxmindHttpProxyApp_H_
#include <memory>
#include <string>
#include "MaxmindHttpProxy.h"
class MaxmindHttpProxyApp;



class MaxmindHttpProxyApp {

public:
void startTheApp(int argc, char* argv[]);
void processArguments(
        int argc,
        char* argv[],
        std::string& propertyFileName,
        unsigned short& port,
        std::string& appName,
        std::string& logDirectory,
        std::string& appVersion);

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_BIDDINGMODE_H_ */
