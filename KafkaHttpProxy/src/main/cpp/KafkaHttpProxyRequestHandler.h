#ifndef KafkaHttpProxyRequestHandler_H
#define KafkaHttpProxyRequestHandler_H

#include "SignalHandler.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "Object.h"
class KafkaHttpProxyRequestHandler;
class KafkaProducer;


class KafkaHttpProxyRequestHandler : public Poco::Net::HTTPRequestHandler, public Object {

public:
EntityToModuleStateStats* entityToModuleStateStats;
KafkaProducer* kafkaProducer;
KafkaHttpProxyRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats);

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

virtual ~KafkaHttpProxyRequestHandler();
};

#endif
