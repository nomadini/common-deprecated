
#include "StringUtil.h"
#include "TempUtil.h"

#include "GUtil.h"
#include "UnknownRequestHandler.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>
#include "ConfigService.h"
#include "TargetGroup.h"
#include "KafkaHttpProxyRequestHandler.h"
#include "KafkaHttpProxyRequestHandlerFactory.h"
#include "NetworkUtil.h"
#include "KafkaProducer.h"
#include "HttpUtil.h"
KafkaHttpProxyRequestHandlerFactory::KafkaHttpProxyRequestHandlerFactory() : Object(__FILE__) {
        kafkaProducer = nullptr;
}

Poco::Net::HTTPRequestHandler
*KafkaHttpProxyRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {

        try {


                MLOG(10) << "KafkaHttpProxyRequestHandlerFactory : request uri to process : " << request.getURI ();

                // if (StringUtil::containsCaseInSensitive (request.getURI (), "/kafk")) {
                auto kafkaHttpProxyRequestHandler = new KafkaHttpProxyRequestHandler (entityToModuleStateStats);
                assertAndThrow(kafkaProducer != nullptr);
                assertAndThrow(entityToModuleStateStats != nullptr);
                kafkaHttpProxyRequestHandler->kafkaProducer = kafkaProducer;;
                return kafkaHttpProxyRequestHandler;
                // }

        } catch (...) {
                LOG(ERROR) <<"unknow exception happened";
        }
        return new UnknownRequestHandler (entityToModuleStateStats);
}
