#ifndef KafkaHttpProxyRequestHandlerFactory_H
#define KafkaHttpProxyRequestHandlerFactory_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include <unordered_map>
class OpportunityContext;
class BidRequestHandlerPipelineProcessor;
class EntityToModuleStateStats;
class TargetGroup;
class BidRequestHandler;
class CommonRequestHandlerFactory;
namespace gicapods { class ConfigService; }
class BidModeControllerService;
class TgBiddingPerformanceMetricInBiddingPeriodService;
#include "AtomicLong.h"
#include "AtomicBoolean.h"
#include "AtomicDouble.h"
#include "Poco/Logger.h"
#include "Object.h"
class KafkaHttpProxyRequestHandlerFactory;
class KafkaProducer;



class KafkaHttpProxyRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory, public Object {

public:
EntityToModuleStateStats* entityToModuleStateStats;
KafkaProducer* kafkaProducer;
gicapods::ConfigService* configService;


KafkaHttpProxyRequestHandlerFactory();

Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request);

};

#endif
