#ifndef KafkaHttpProxy_H_
#define KafkaHttpProxy_H_

#include <memory>
#include <string>
#include "KafkaHttpProxyRequestHandlerFactory.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
#include "Object.h"
#include "AtomicBoolean.h"
class EntityToModuleStateStatsPersistenceService;
class DataReloadService;
namespace gicapods { class ConfigService; }

#include <boost/thread.hpp>
#include <thread>
class ComparatorService;
class TgFilterMeasuresService;
class BeanFactory;
class ModuleContainer;
class KafkaHttpProxy;



class KafkaHttpProxy : public std::enable_shared_from_this<KafkaHttpProxy>
        , public Poco::Util::ServerApplication
{

public:

gicapods::ConfigService* configService;
KafkaHttpProxyRequestHandlerFactory* kafkaHttpProxyRequestHandlerFactory;
EntityToModuleStateStats* entityToModuleStateStats;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;

KafkaHttpProxy();
virtual ~KafkaHttpProxy();
void initFactory();
std::string getName();

int main(const std::vector<std::string> &args);

private:
bool _helpRequested;
};

#endif
