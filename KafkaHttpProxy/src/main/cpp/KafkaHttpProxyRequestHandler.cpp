
#include "KafkaHttpProxyRequestHandler.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "StringUtil.h"
#include "ConverterUtil.h"
#include "HttpUtil.h"
#include "GUtil.h"
#include "StatisticsUtil.h"
#include "ExceptionUtil.h"
#include "KafkaProducer.h"
#include "EntityToModuleStateStats.h"

KafkaHttpProxyRequestHandler::KafkaHttpProxyRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;

}

void KafkaHttpProxyRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                                 Poco::Net::HTTPServerResponse &response) {


        std::string requestBody = HttpUtil::getRequestBody(request);
        LOG_EVERY_N(INFO, 10)<< google::COUNTER<< "sending msg to kafka, requestBody : "<< requestBody;

        kafkaProducer->produceMessage(requestBody);
        entityToModuleStateStats->addStateModuleForEntity ("request_processed",
                                                           "KafkaHttpProxyRequestHandler",
                                                           "ALL");
        HttpUtil::setEmptyResponse(response);

}

KafkaHttpProxyRequestHandler::~KafkaHttpProxyRequestHandler() {
}
