#include "GUtil.h"
#include "KafkaHttpProxyApp.h"
#include <memory>
#include <string>


int main(int argc, char* argv[]) {
        auto kafkaHttpProxyApp = std::make_shared<KafkaHttpProxyApp>();

        kafkaHttpProxyApp->startTheApp(argc, argv);
}
