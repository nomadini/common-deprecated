
#include "PropertyServerRequestHandler.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "StringUtil.h"
#include "ConverterUtil.h"
#include "HttpUtil.h"
#include "GUtil.h"
#include "StatisticsUtil.h"
#include "JsonUtil.h"
#include "JsonMapUtil.h"
#include "ExceptionUtil.h"
#include "EntityToModuleStateStats.h"

PropertyServerRequestHandler::PropertyServerRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::BootableConfigService* bootableConfigService) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->bootableConfigService = bootableConfigService;


}

void PropertyServerRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                                 Poco::Net::HTTPServerResponse &response) {



        try {

                entityToModuleStateStats->addStateModuleForEntity ("numberOfBidsRecieved",
                                                                   "PropertyServerRequestHandler",
                                                                   "ALL");
                auto requestInJson = HttpUtil::getRequestBody (request);
                LOG(INFO)<< "requestInJson : "<<requestInJson;

                auto doc = parseJsonSafely(requestInJson);
                std::string appName = JsonUtil::GetStringSafely(*doc,"appName");
                std::string cluster = JsonUtil::GetStringSafely(*doc,"cluster");
                std::string host = JsonUtil::GetStringSafely(*doc,"host");
                LOG(ERROR)
                        <<" appName : "<< appName
                        <<" cluster : "<< cluster
                        <<" host : "<< host;

                auto bootableProps = bootableConfigService->loadProperties(appName, cluster, host);
                auto propsAsJson = JsonMapUtil::convertMapToJsonArray(*bootableProps);


                LOG(ERROR) << "loading these props : "<< propsAsJson;
                std::ostream &ostr = response.send ();
                ostr << propsAsJson;

        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                   "PropertyServerRequestHandler",
                                                                   "ALL");
        } catch (...) {
                entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                   "PropertyServerRequestHandler",
                                                                   "ALL");
        }

        HttpUtil::setEmptyResponse(response);


}

PropertyServerRequestHandler::~PropertyServerRequestHandler() {
}
