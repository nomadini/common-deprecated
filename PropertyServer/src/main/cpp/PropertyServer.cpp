#include "GUtil.h"
#include "BeanFactory.h"

#include "SignalHandler.h"
#include "Poco/Util/HelpFormatter.h"
#include "PropertyServer.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/ThreadPool.h"
#include "ConverterUtil.h"
#include <boost/exception/all.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include "FileUtil.h"
#include "ConfigService.h"
#include "PropertyServerRequestHandlerFactory.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "PocoHttpServer.h"


#include <new>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <thread>


PropertyServer::PropertyServer() {

}

PropertyServer::~PropertyServer() {
}

void PropertyServer::initFactory() {

}

std::string PropertyServer::getName() {
        return "PropertyServer";
}

int PropertyServer::main(const std::vector<std::string> &args) {
        this->entityToModuleStateStatsPersistenceService->startThread();

        auto srv = PocoHttpServer::createHttpServer(configService, propertyServerRequestHandlerFactory);

        // start the HTTPServer
        srv->start ();
        // wait for CTRL-C or kill

        waitForTerminationRequest ();
        // Stop the HTTPServer
        srv->stop ();
        LOG(INFO)<<"server is shutting down......";
        //sleeping for 3 seconds for threads to stop
        gicapods::Util::sleepViaBoost(_L_, 3);

        LOG(INFO)<<"exiting program.";
        return Application::EXIT_OK;
}
