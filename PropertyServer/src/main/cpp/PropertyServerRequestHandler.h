#ifndef PropertyServerRequestHandler_H
#define PropertyServerRequestHandler_H

#include "SignalHandler.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "Object.h"
#include "BootableConfigService.h"
class PropertyServerRequestHandler;

class PropertyServerRequestHandler : public Poco::Net::HTTPRequestHandler, public Object {

public:
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::BootableConfigService* bootableConfigService;
PropertyServerRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::BootableConfigService* bootableConfigService);

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

virtual ~PropertyServerRequestHandler();
};

#endif
