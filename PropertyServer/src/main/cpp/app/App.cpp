#include "GUtil.h"
#include "PropertyServerApp.h"
#include <memory>
#include <string>


int main(int argc, char* argv[]) {
        auto propertyServerApp = std::make_shared<PropertyServerApp>();

        propertyServerApp->startTheApp(argc, argv);
}
