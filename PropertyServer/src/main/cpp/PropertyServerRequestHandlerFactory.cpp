
#include "StringUtil.h"
#include "TempUtil.h"

#include "GUtil.h"
#include "UnknownRequestHandler.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>
#include "ConfigService.h"
#include "TargetGroup.h"
#include "PropertyServerRequestHandler.h"
#include "PropertyServerRequestHandlerFactory.h"
#include "NetworkUtil.h"
#include "BootableConfigService.h"
#include "HttpUtil.h"
PropertyServerRequestHandlerFactory::PropertyServerRequestHandlerFactory() : Object(__FILE__) {

}

void PropertyServerRequestHandlerFactory::init() {
}


Poco::Net::HTTPRequestHandler
*PropertyServerRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {

        try {

                MLOG(10) << "PropertyServerRequestHandlerFactory : request uri to process : " << request.getURI ();

                auto propertyServerRequestHandler = new PropertyServerRequestHandler (entityToModuleStateStats,
                                                                                      bootableConfigService);
                return propertyServerRequestHandler;

        } catch (...) {
                LOG(ERROR) <<"unknow exception happened";
        }
        return new UnknownRequestHandler (entityToModuleStateStats);
}
