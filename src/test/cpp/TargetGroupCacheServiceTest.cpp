// //
// // Created by mtaabodi on 7/13/2016.
// //
// #include "GUtil.h"
// #include "JsonUtil.h"
// #include "JsonArrayUtil.h"
// #include "JsonArrayUtil.h"
// #include "TargetGroup.h"
// #include "StringUtil.h"
// #include <string>
// #include <memory>
// #include "DateTimeUtil.h"
// #include "TargetGroupCacheServiceTest.h"
// #include "TargetGroupCacheService.h"
// #include "EventLog.h"
// #include "BeanFactory.h"
// #include "CollectionUtil.h"
// #include "HttpUtilServiceMock.h"
//
// TargetGroupCacheServiceTest::TargetGroupCacheServiceTest() {
//         // this->service = beanFactory->targetGroupCacheService;
//
// }
//
// TargetGroupCacheServiceTest::~TargetGroupCacheServiceTest() {
//
// }
//
// std::string TargetGroupCacheServiceTest::createArrayOfTargetGroups(std::shared_ptr<TargetGroup> targetGroupExpected) {
//         RapidJsonValueTypeNoRef crtObject(rapidjson::kObjectType);
//         auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
//         targetGroupExpected->addPropertiesToJsonValue (crtObject, doc);
//
//         doc->PushBack (crtObject, doc->GetAllocator ());
//         return JsonUtil::docToString (doc.get());
// }
//
// void TargetGroupCacheServiceTest::givenHttpServiceReturnsJsonData(){
//
//         auto targetGroupExpected = TargetGroupTestHelper::createSampleTargetGroup();
//         allTargetGroupsExpected.insert(std::make_pair(targetGroupExpected->getId (), targetGroupExpected));
//
//         std::string sampleJsonOfListOfTargetGroups = createArrayOfTargetGroups(targetGroupExpected);
//         MLOG(3)<<"sampleJsonOfListOfTargetGroups : "<<sampleJsonOfListOfTargetGroups;
//         ON_CALL(*httpUtilServiceMock,
//                 sendPostRequest (_,_,_))
//         .WillByDefault(Return(sampleJsonOfListOfTargetGroups));
//
// }
//
// void TargetGroupCacheServiceTest::whenTargetGroupReloaCachesViaHtppIsIsCalled() {
//         this->service->reloadCachesViaHttp();
// }
//
// void TargetGroupCacheServiceTest::SetUp() {
//         this->service->clearCaches();
//         this->allTargetGroupsExpected.clear();
// }
//
// void TargetGroupCacheServiceTest::TearDown() {
//
// }
//
// void TargetGroupCacheServiceTest::thenBothTargetGroupsAreEqual(std::shared_ptr<TargetGroup> actualTargetGroup, std::shared_ptr<TargetGroup> expectedTargetGroup) {
//         EXPECT_THAT( actualTargetGroup->getId(),
//                      testing::Eq(expectedTargetGroup->getId()));
//         EXPECT_THAT( actualTargetGroup->getName(),
//                      testing::Eq(expectedTargetGroup->getName()));
//         EXPECT_THAT( actualTargetGroup->getStatus(),
//                      testing::Eq(expectedTargetGroup->getStatus()));
//         EXPECT_THAT( actualTargetGroup->getDescription(),
//                      testing::Eq(expectedTargetGroup->getDescription()));
//
//         EXPECT_TRUE( CollectionUtil::areListsEqual(*actualTargetGroup->getIabCategory(), *expectedTargetGroup->getIabCategory()));
//         EXPECT_TRUE( CollectionUtil::areListsEqual(*actualTargetGroup->getIabSubCategory(), *expectedTargetGroup->getIabSubCategory()));
//
//         EXPECT_TRUE( CollectionUtil::areMapsEqual(*actualTargetGroup->getAdPositions(), *expectedTargetGroup->getAdPositions()));
//
//         EXPECT_THAT( actualTargetGroup->getCampaignId(),
//                      testing::Eq(expectedTargetGroup->getCampaignId()));
//         EXPECT_THAT( actualTargetGroup->getMaxImpression(),
//                      testing::Eq(expectedTargetGroup->getMaxImpression()));
//         EXPECT_THAT( actualTargetGroup->getDailyMaxImpression(),
//                      testing::Eq(expectedTargetGroup->getDailyMaxImpression()));
//         EXPECT_THAT( actualTargetGroup->getMaxBudget(),
//                      testing::Eq(expectedTargetGroup->getMaxBudget()));
//         EXPECT_THAT( actualTargetGroup->getDailyMaxBudget(),
//                      testing::Eq(expectedTargetGroup->getDailyMaxBudget()));
//
//         EXPECT_TRUE( CollectionUtil::areMapsEqual(actualTargetGroup->getPacingPlan()->hourToCumulativePercentageLimit,
//                                                   expectedTargetGroup->getPacingPlan()->hourToCumulativePercentageLimit));
//
//         EXPECT_THAT( actualTargetGroup->getPacingPlan()->timeZone,
//                      testing::Eq(expectedTargetGroup->getPacingPlan()->timeZone));
//
//         EXPECT_THAT( actualTargetGroup->getFrequencyInSec(),
//                      testing::Eq(expectedTargetGroup->getFrequencyInSec()));
//         EXPECT_THAT( actualTargetGroup->getStartDate(),
//                      testing::Eq(expectedTargetGroup->getStartDate()));
//         EXPECT_THAT( actualTargetGroup->getEndDate(),
//                      testing::Eq(expectedTargetGroup->getEndDate()));
//         EXPECT_THAT( actualTargetGroup->getCreatedAt(),
//                      testing::Eq(expectedTargetGroup->getCreatedAt()));
//         EXPECT_THAT( actualTargetGroup->getUpdatedAt(),
//                      testing::Eq(expectedTargetGroup->getUpdatedAt()));
// }
//
// void TargetGroupCacheServiceTest::thenAllTargetGroupsAreAsExpected()  {
//
//         std::unordered_map<int, std::shared_ptr<TargetGroup>> actualTargetGroupsMap;
//         EXPECT_TRUE (!this->service->allEntities->empty());
//
//         for (std::shared_ptr<TargetGroup> actualTargetGroup :  *this->service->allEntities) {
//                 actualTargetGroupsMap.insert(std::pair<int, std::shared_ptr<TargetGroup>>(actualTargetGroup->getId(), actualTargetGroup));
//         }
//
//         EXPECT_THAT(actualTargetGroupsMap.size(),
//                     testing::Eq(this->allTargetGroupsExpected.size()));
//
//

//
// //    BOOST_FOREACH_MAP
//         for(auto const& entry :  allTargetGroupsExpected) {
//                 auto expectedTargetGroup = entry.second;
//                 auto pair = actualTargetGroupsMap.find(expectedTargetGroup->getId());
//                 if (pair == actualTargetGroupsMap.end()) {
//                         LOG(WARNING)<<"couldnt find this targetGroup id in actual map : " <<expectedTargetGroup->getId();
//                         EXPECT_TRUE (false);//we didn't the expected targetGroup in actual targetGroups map, so we fail here
//                         continue;
//                 }
//                 auto actualTargetGroup = pair->second;
//                 thenBothTargetGroupsAreEqual(actualTargetGroup, expectedTargetGroup);
//         }
// }
//
// void TargetGroupCacheServiceTest::givenDataInCache() {
//         auto targetGroup = TargetGroupTestHelper::createSampleTargetGroup();
//         this->service->allEntities->push_back(targetGroup);
//         allTargetGroupsExpected.insert(std::make_pair(targetGroup->getId (), targetGroup));
//
//         targetGroup = TargetGroupTestHelper::createSampleTargetGroup();
//         this->service->allEntities->push_back(targetGroup);
//         allTargetGroupsExpected.insert(std::make_pair(targetGroup->getId (), targetGroup));
// }
//
// void TargetGroupCacheServiceTest::whenGetAllAsJsonIsCalled() {
//         this->allEntitiesInJsonReturned = this->service->cacheServiceServercacheServiceServer->getAllAsJson ();
//         MLOG(3)<<"allEntitiesInJsonReturned : "<<allEntitiesInJsonReturned<<std::endl;
// }
//
// void TargetGroupCacheServiceTest::thenAllEntitiesJosnIsCreatedAsExpected() {
//         std::unordered_map<int, std::shared_ptr<TargetGroup>> actualTargetGroupsMap;
//         auto docReturned = parseJsonSafely(this->allEntitiesInJsonReturned);
//         for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//                 auto& advValue = (*docReturned)[i];
//                 std::shared_ptr<TargetGroup> adv = TargetGroup::fromJsonValue(advValue);
//                 MLOG(3) << "parsed this tg : "<<adv->toString();
//                 actualTargetGroupsMap.insert(std::make_pair(adv->getId (), adv));
//         }
//
//         EXPECT_THAT(actualTargetGroupsMap.size(),
//                     testing::Eq(this->allTargetGroupsExpected.size()));
//
//

//
// //    BOOST_FOREACH_MAP
//         for(auto const& entry :  allTargetGroupsExpected) {
//                 auto expectedTargetGroup = entry.second;
//                 auto pair = actualTargetGroupsMap.find(expectedTargetGroup->getId());
//                 if (pair == actualTargetGroupsMap.end()) {
//                         LOG(WARNING)<<"couldnt find this creative id in actual map : " <<expectedTargetGroup->getId();
//                         EXPECT_TRUE (false);//we didn't the expected creative in actual creatives map, so we fail here
//                         continue;
//                 }
//                 auto actualTargetGroup = pair->second;
//                 thenBothTargetGroupsAreEqual(actualTargetGroup, expectedTargetGroup);
//         }
// }
//
// TEST_F(TargetGroupCacheServiceTest, testParsingTargetGroupFromHttpUtilService) {
//
//         givenHttpServiceReturnsJsonData();
//
//         whenTargetGroupReloaCachesViaHtppIsIsCalled();
//
//         thenAllTargetGroupsAreAsExpected();
// }
//
//
// TEST_F(TargetGroupCacheServiceTest, testGetAllEntitiesAsJson) {
//
//         givenDataInCache();
//
//         whenGetAllAsJsonIsCalled();
//
//         thenAllEntitiesJosnIsCreatedAsExpected();
// }
