
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlMetricServiceTest.h"
#include "MySqlMetricService.h"
#include "MySqlDriver.h"
#include "EventLog.h"
#include "BeanFactory.h"
#include "CollectionUtil.h"
#include "MetricDbRecord.h"
#include "EntityToModuleStateStats.h"
#include "NetworkUtil.h"

MySqlMetricServiceTest::MySqlMetricServiceTest() {
}
MySqlMetricServiceTest::~MySqlMetricServiceTest() {

}

void MySqlMetricServiceTest::thenMetricsAreEqual(std::shared_ptr<MetricDbRecord> metricsFromDb, std::shared_ptr<MetricDbRecord> metric) {

        EXPECT_THAT(metricsFromDb->value, metric->value);
        EXPECT_THAT(metricsFromDb->appName, metric->appName);
        EXPECT_THAT(metricsFromDb->hostName, metric->hostName);
        EXPECT_THAT(metricsFromDb->domain, metric->domain);
}

TEST_F(MySqlMetricServiceTest, testCalculatingPercentagesOfStateProperly) {
        std::shared_ptr<EntityToModuleStateStats> entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();

        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module1", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module1", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module1", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module1", StringUtil::toStr ("tg001"));

        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module2", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg001"));


        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg002"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg002"));

        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module3", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module4", StringUtil::toStr ("tg001"));

        std::vector<std::string> entities = entityToModuleStateStats->getAllEntities();
        MLOG(3) << "entities : "<<JsonArrayUtil::convertListToJson(entities);

        for(std::string entity : entities) {

                std::vector<std::string> allModulesInLastOneMinute = entityToModuleStateStats->getAllModulesForEntity(entity);
                for(std::string module : allModulesInLastOneMinute) {
                        std::vector <std::string> allStatesForModule = entityToModuleStateStats->getAllStatesForModule(module);
                        MLOG(3) << "allStatesForModule : "<<JsonArrayUtil::convertListToJson(allStatesForModule);
                        for(std::string state :  allStatesForModule) {
                                double percentage = 0;
                                double count = entityToModuleStateStats->getValueOfStateModuleForEntity(state, module, entity);
                                MLOG(3) << "entity : "<<entity
                                        <<" , module : " << module
                                        << ", state : " << state
                                        << " percentage : " << percentage
                                        << " count : "<< count;

                                if (StringUtil::equalsIgnoreCase(entity, "tg001")) {
                                        if (StringUtil::equalsIgnoreCase(state, "PASSED")) {
                                                if (StringUtil::equalsIgnoreCase(module, "Module2")) {
                                                        EXPECT_THAT(1, count);
                                                }
                                                if (StringUtil::equalsIgnoreCase(module, "Module1")) {
                                                        EXPECT_THAT(3, count);
                                                }
                                                if (StringUtil::equalsIgnoreCase(module, "Module3")) {
                                                        EXPECT_THAT(1, count);
                                                }
                                        }
                                        if (StringUtil::equalsIgnoreCase(state, "FAILED")) {
                                                if (StringUtil::equalsIgnoreCase(module, "Module2")) {
                                                        EXPECT_THAT(3, count);
                                                }
                                                if (StringUtil::equalsIgnoreCase(module, "Module1")) {
                                                        EXPECT_THAT(1, count);
                                                }
                                                if (StringUtil::equalsIgnoreCase(module, "Module4")) {
                                                        EXPECT_THAT(1, count);
                                                }
                                        }
                                } else if (StringUtil::equalsIgnoreCase(entity, "tg002")) {
                                        if (StringUtil::equalsIgnoreCase(state, "FAILED")) {
                                                if (StringUtil::equalsIgnoreCase(module, "Module2")) {
                                                        EXPECT_THAT(100, percentage);
                                                }
                                        }
                                } else if (StringUtil::equalsIgnoreCase(state, "PASSED")) {
                                        if (StringUtil::equalsIgnoreCase(module, "Module2")) {
                                                EXPECT_THAT(0,percentage);
                                        }
                                }

                        }
                }

        }

}

TEST_F(MySqlMetricServiceTest, testWritingAndReadingEvents) {
        std::shared_ptr<EntityToModuleStateStats> entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();

        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module1", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module1", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module1", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module1", StringUtil::toStr ("tg001"));

        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module2", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg001"));

        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg002"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module2", StringUtil::toStr ("tg002"));

        entityToModuleStateStats->addStateModuleForEntity ("PASSED", "Module3", StringUtil::toStr ("tg001"));
        entityToModuleStateStats->addStateModuleForEntity ("FAILED", "Module4", StringUtil::toStr ("tg001"));

//    entityToModuleStateStats->addStateModuleForEntity ("FAILED" , "Module1" , StringUtil::toStr ("tg001"));
//    entityToModuleStateStats->addStateModuleForEntity ("FAILED" , "Module1" , StringUtil::toStr ("tg001"));
//    entityToModuleStateStats->addStateModuleForEntity ("FAILED" , "Module1" , StringUtil::toStr ("tg002"));
//    entityToModuleStateStats->addStateModuleForEntity ("FAILED" , "Module1" , StringUtil::toStr ("tg003"));
//    entityToModuleStateStats->addStateModuleForEntity ("FAILED" , "Module1" , StringUtil::toStr ("tg004"));
        auto mySqlMetricService = std::make_shared<MySqlMetricService> (
                nullptr);


        std::vector<std::string> entities = entityToModuleStateStats->getAllEntities();
        MLOG(3) << "entities : "<<JsonArrayUtil::convertListToJson(entities);

        mySqlMetricService->deleteAll();
        for(std::string entity : entities) {

                std::vector<std::string> allModulesInLastOneMinute = entityToModuleStateStats->getAllModulesForEntity(entity);
                for(std::string module :  allModulesInLastOneMinute) {
                        std::vector <std::string> allStatesForModule = entityToModuleStateStats->getAllStatesForModule(module);
                        MLOG(3) << "allStatesForModule : "<<JsonArrayUtil::convertListToJson(allStatesForModule);
                        for(std::string state :  allStatesForModule) {
                                // entityToModuleStateStats->getOneMinuteStatePercForModuleAndEntity(state, module, entity);
                                double valueDouble = 0;
                                MLOG(3) << "entity : "<<entity<<" , module : " << module << ", state : " << state  << " : " << valueDouble;

                                std::shared_ptr<MetricDbRecord> metric = std::make_shared<MetricDbRecord>();
                                metric->appName = "MySqlMetricServiceTest";
                                metric->hostName = NetworkUtil::getHostName();
                                metric->value = valueDouble;
                                metric->domain = module;
                                std::vector<std::shared_ptr<MetricDbRecord>> allMetrics;

                                allMetrics.push_back (metric);

                                mySqlMetricService->insert(allMetrics);
                                auto metricReadFromDb = mySqlMetricService->readMetricByStateModuleEntity(metric->state,
                                                                                                          metric->module,
                                                                                                          metric->entity);
                                MLOG(3) << "metricReadFromDb : " << metricReadFromDb->toJson();
                                MLOG(3) << "metric : " << metric->toJson();
                                thenMetricsAreEqual(metricReadFromDb, metric);

                        }
                }
        }




}
