// //
// // Created by mtaabodi on 7/13/2016.
// //
// #include "GUtil.h"
// #include "JsonUtil.h"
// #include "JsonArrayUtil.h"
// #include "JsonMapUtil.h"
// #include "StringUtil.h"
// #include <string>
// #include <memory>
// #include "DateTimeUtil.h"
// #include "TargetGroupDayPartCacheServiceTest.h"
// #include "TargetGroupDayPartCacheService.h"
// #include "EventLog.h"
// #include "BeanFactory.h"
// #include "CollectionUtil.h"
// #include "HttpUtilServiceMock.h"
// #include "TargetGroupCreativeCacheService.h"
// #include "RandomUtil.h"
//
// TargetGroupDayPartCacheServiceTest::TargetGroupDayPartCacheServiceTest() {
//     this->service = beanFactory->targetGroupDayPartCacheService;
// }
//
// TargetGroupDayPartCacheServiceTest::~TargetGroupDayPartCacheServiceTest() {
//
// }
//
// std::string TargetGroupDayPartCacheServiceTest::createArrayOfTargetGroupDayParts(
//   std::shared_ptr<TargetGroupDayPartTargetMap> targetGroupDayPartTargetMap) {
//     RapidJsonValueTypeNoRef crtObject(rapidjson::kObjectType);
//     auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
//     targetGroupDayPartTargetMap->addPropertiesToJsonValue (crtObject, doc);
//
//     doc->PushBack (crtObject , doc->GetAllocator ());
//     return JsonUtil::docToString (doc.get());
// }
//
//
// std::shared_ptr<TargetGroupDayPartTargetMap> TargetGroupDayPartCacheServiceTest::createSampleEntity() {
//   std::shared_ptr<TargetGroupDayPartTargetMap> entity = std::make_shared<TargetGroupDayPartTargetMap>();
//   entity->id = RandomUtil::sudoRandomNumber(10000);
//   entity->targetGroupId = RandomUtil::sudoRandomNumber(10000);
//   entity->hours = "{\"0\":[\"0\",\"1\",\"2\"],\"1\":[\"0\",\"22\",\"23\"],\"6\":[\"5\",\"23\"]}";
//   entity->dateCreated = DateTimeUtil::getNowInMySqlFormat();
//   entity->dateModified =  DateTimeUtil::getNowInMySqlFormat();
//   MLOG(3) << "  entity->hours : " <<  entity->hours;
//   entity->hoursMap = entity->buildHoursMap(entity->hours);
//   return entity;
// }
//
// void TargetGroupDayPartCacheServiceTest::givenHttpServiceReturnsJsonData(){
//     auto expectedDayParts = TargetGroupDayPartCacheServiceTest::createSampleEntity();
//     allTargetGroupDayPartsExpected.insert(std::make_pair(expectedDayParts->targetGroupId, expectedDayParts));
//
//     std::string sampleJsonOfListOfTargetGroupDayparts = createArrayOfTargetGroupDayParts(expectedDayParts);
//     MLOG(3)<<"sampleJsonOfListOfTargetGroupDayparts : "<<sampleJsonOfListOfTargetGroupDayparts;
//     ON_CALL(*httpUtilServiceMock,
//             sendPostRequest (_,_,_))
//             .WillByDefault(Return(sampleJsonOfListOfTargetGroupDayparts));
//
// }
//
// void TargetGroupDayPartCacheServiceTest::whenTargetGroupDayPartReloaCachesViaHtppIsIsCalled() {
//     this->service->reloadCachesViaHttp();
// }
//
// void TargetGroupDayPartCacheServiceTest::SetUp() {
//   this->httpUtilServiceMock = std::make_shared<HttpUtilServiceMock>();
//   this->service->httpUtilService = httpUtilServiceMock;
//   this->allTargetGroupDayPartsExpected.clear();
//   this->service->allTargetGroupsDayPartMaps.clear();
//   this->service->clearCaches();
// }
//
// void TargetGroupDayPartCacheServiceTest::TearDown() {
//
// }
//
// void TargetGroupDayPartCacheServiceTest::thenBothTargetGroupDayPartsAreEqual(
//   std::shared_ptr<TargetGroupDayPartTargetMap> actualEntity,
//    std::shared_ptr<TargetGroupDayPartTargetMap> expectedEntity) {
//   EXPECT_THAT( actualEntity->id, testing::Eq(expectedEntity->id));
//   EXPECT_THAT( actualEntity->targetGroupId, testing::Eq(expectedEntity->targetGroupId));
//   EXPECT_THAT( actualEntity->hours, testing::Eq(expectedEntity->hours));
//   EXPECT_THAT( actualEntity->dateCreated, testing::Eq(expectedEntity->dateCreated));
//   EXPECT_THAT( actualEntity->dateModified, testing::Eq(expectedEntity->dateModified));
// }
//
// void TargetGroupDayPartCacheServiceTest::thenAllTargetGroupDayPartsAreAsExpected()  {
//   std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> actualTargetGroupsMap;
//   for(auto entity :  this->service->allTargetGroupsDayPartMaps) {
//       actualTargetGroupsMap.insert(std::make_pair(entity->id, entity));
//   }
//   TargetGroupDayPartCacheServiceTest::assertBothMapsAreEqual(actualTargetGroupsMap, allTargetGroupDayPartsExpected );
// }
// void TargetGroupDayPartCacheServiceTest::givenDataInCache() {
//     auto targetGroup = createSampleEntity();
//
//     allTargetGroupDayPartsExpected.insert(std::make_pair(targetGroup->id, targetGroup));
//     this->service->allTargetGroupsDayPartMaps.push_back(targetGroup);
//
//
//     targetGroup = TargetGroupDayPartCacheServiceTest::createSampleEntity();
//     this->service->allTargetGroupsDayPartMaps.push_back(targetGroup);
//     allTargetGroupDayPartsExpected.insert(std::make_pair(targetGroup->id, targetGroup));
//
// }
//
// void TargetGroupDayPartCacheServiceTest::whenGetAllAsJsonIsCalled() {
//     this->allEntitiesInJsonReturned = this->servicecacheServiceServer->getAllAsJson ();
//     MLOG(3)<<"allEntitiesInJsonReturned : "<<allEntitiesInJsonReturned<<std::endl;
// }
//
// std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> TargetGroupDayPartCacheServiceTest::parseDtoReturnedToMap(std::string json) {
//
//   std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> actualTargetGroupsMap;
//   auto docReturned = parseJsonSafely(json);
//   for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//       auto& advValue = (*docReturned)[i];
//       std::shared_ptr<TargetGroupDayPartTargetMap> adv = TargetGroupDayPartTargetMap::fromJsonValue(advValue);
//       MLOG(3) << "parsed this TargetGroupDayPartTargetMap : "<<adv->toString();
//       actualTargetGroupsMap.insert(std::make_pair(adv->id, adv));
//   }
//   return actualTargetGroupsMap;
// }
//
// void TargetGroupDayPartCacheServiceTest::assertBothMapsAreEqual(
//                       std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> actualTargetGroupsMap,
//                        std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> allTargetGroupCreativesExpected ) {
//                          EXPECT_THAT(actualTargetGroupsMap.size(),
//                                      testing::Eq(allTargetGroupCreativesExpected.size()));
//

//
//                          //    BOOST_FOREACH_MAP
//                          for(auto const& entry  :  allTargetGroupCreativesExpected) {
//                              auto expectedTgCreativeMap = entry.second;
//                              auto pair = actualTargetGroupsMap.find(expectedTgCreativeMap->id);
//                              if (pair == actualTargetGroupsMap.end()) {
//                                  LOG(WARNING)<<"couldnt find this tg map id in actual map : " <<expectedTgCreativeMap->id;
//                                  EXPECT_TRUE (false);//we didn't the expected creative in actual creatives map, so we fail here
//                                  continue;
//                              }
//                              auto actualTgCreativeMap = pair->second;
//                              thenBothTargetGroupDayPartsAreEqual(actualTgCreativeMap, expectedTgCreativeMap);
//                          }
// }
// void TargetGroupDayPartCacheServiceTest::thenAllEntitiesJosnIsCreatedAsExpected() {
//
//   std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> actualTargetGroupsMap = parseDtoReturnedToMap(this->allEntitiesInJsonReturned);
//   assertBothMapsAreEqual(actualTargetGroupsMap, this->allTargetGroupDayPartsExpected );
// }
//
// TEST_F(TargetGroupDayPartCacheServiceTest, testParsingTargetGroupDayPartFromHttpUtilService) {
//
//     givenHttpServiceReturnsJsonData();
//
//     whenTargetGroupDayPartReloaCachesViaHtppIsIsCalled();
//
//     thenAllTargetGroupDayPartsAreAsExpected();
// }
//
//
// TEST_F(TargetGroupDayPartCacheServiceTest, testGetAllEntitiesAsJson) {
//     MLOG(3)<<"testGetAllEntitiesAsJson ";
//     givenDataInCache();
//
//     whenGetAllAsJsonIsCalled();
//
//     thenAllEntitiesJosnIsCreatedAsExpected();
// }
