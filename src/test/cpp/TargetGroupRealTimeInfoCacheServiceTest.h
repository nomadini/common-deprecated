//
// Created by mtaabodi on 7/13/2016.
//

#ifndef TgRealTimeDeliveryInfoCacheService_H
#define TgRealTimeDeliveryInfoCacheService_H



#include "EntityRealTimeDeliveryInfo.h"
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "EntityRealTimeDeliveryInfo.h"
class EntityDeliveryInfoCacheService;
#include "HttpUtilServiceMock.h"

class TgRealTimeDeliveryInfoCacheServiceTest  : public ::testing::Test {

private:

public:

    TgRealTimeDeliveryInfoCacheService* service;
    std::string allEntitiesInJsonReturned;

    std::string allEntitiesInJsonExpected;

    std::unordered_map<int, std::shared_ptr<EntityRealTimeDeliveryInfo>> allTgRealTimeDeliveryInfosExpected;

    HttpUtilServiceMockPtr httpUtilServiceMock;

    void SetUp();

    void TearDown();

    TgRealTimeDeliveryInfoCacheServiceTest() ;

    void givenHttpServiceReturnsJsonData();

    void whenTgRealTimeDeliveryInfoReloaCachesViaHtppIsIsCalled();

    void thenAllTgRealTimeDeliveryInfosAreAsExpected();

    std::string createArrayOfTgRealTimeDeliveryInfos(std::shared_ptr<EntityRealTimeDeliveryInfo> targetGroupExpected);

    void thenBothTgRealTimeDeliveryInfosAreEqual(std::shared_ptr<EntityRealTimeDeliveryInfo> actualTgRealTimeDeliveryInfo, std::shared_ptr<EntityRealTimeDeliveryInfo> expectedTgRealTimeDeliveryInfo);

    void givenDataInCache();

    void whenGetAllAsJsonIsCalled();

    void thenAllEntitiesJosnIsCreatedAsExpected();

    virtual ~TgRealTimeDeliveryInfoCacheServiceTest() ;

};
#endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
