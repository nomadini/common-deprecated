// //
// // Created by mtaabodi on 7/13/2016.
// //
//
// #ifndef CampaignCacheService_H
// #define CampaignCacheService_H
//
//
//
// #include "CampaignTypeDefs.h"
// class MySqlDriver;
// #include "CollectionUtil.h"
// #include <memory>
// #include <string>
// #include <gtest/gtest.h>
// #include "TestsCommon.h"
// #include "CampaignTypeDefs.h"
// class CampaignCacheService;
// #include "HttpUtilServiceMock.h"
//
// class CampaignCacheServiceTest  : public ::testing::Test {
//
// private:
//
// public:
//
//     CampaignCacheService* service;
//     std::string allEntitiesInJsonReturned;
//
//     std::string allEntitiesInJsonExpected;
//
//     std::unordered_map<int, std::shared_ptr<Campaign>> allCampaignsExpected;
//
//     HttpUtilServiceMockPtr httpUtilServiceMock;
//
//     void SetUp();
//
//     void TearDown();
//
//     CampaignCacheServiceTest() ;
//
//     void givenHttpServiceReturnsJsonData();
//
//     void whenCampaignReloaCachesViaHtppIsIsCalled();
//
//     void thenAllCampaignsAreAsExpected();
//
//     std::string createArrayOfCampaigns(std::shared_ptr<Campaign> campaignExpected);
//
//     void thenBothCampaignsAreEqual(std::shared_ptr<Campaign> actualCampaign, std::shared_ptr<Campaign> expectedCampaign);
//
//     void givenDataInCache();
//
//     void whenGetAllAsJsonIsCalled();
//
//     void thenAllEntitiesJosnIsCreatedAsExpected();
//
//     virtual ~CampaignCacheServiceTest() ;
//
// };
// #endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
