//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef CommonTestBase_H
#define CommonTestBase_H

#include <gtest/gtest.h>
#include <memory>
#include <string>
#include "TestsCommon.h"
class BeanFactory;


class CommonTestBase : public ::testing::Test {
public:

CommonTestBase();

std::shared_ptr<BeanFactory> beanFactory;



virtual ~CommonTestBase();

void SetUp();

void TearDown();

};
#endif //CommonTestBase_H
