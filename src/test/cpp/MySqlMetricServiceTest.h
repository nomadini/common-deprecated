/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef MySqlMetricServiceTests_H
#define MySqlMetricServiceTests_H

#include "TargetGroupTypeDefs.h"
class TargetGroup;
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "MySqlMetricService.h"
#include "MetricDbRecord.h"

class MySqlMetricServiceTest  : public ::testing::Test {

private:

public:

	MySqlMetricServiceTest() ;

    void thenMetricsAreEqual(std::shared_ptr<MetricDbRecord> metricsFromDb, std::shared_ptr<MetricDbRecord> metrics);

    virtual ~MySqlMetricServiceTest() ;

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */
