//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef AerospikeDriverAtomicWritesOneVisitHistoryTest_H
#define AerospikeDriverAtomicWritesOneVisitHistoryTest_H


#include "AerospikeDriver.h"
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
class EntityToModuleStateStats;
/*
   in this test, multiple threads update one object
   in a bin in aerospike ...I want to see if we can use only object
   the result is, we can update the same object in a bin...
   but some of the changes are lost...like
   here...we are using 10 threads , each updating a  number in a complicated
   object 10 times....instead of seeing 100 as the final value...we see 20
 */
class AerospikeDriverAtomicWritesOneVisitHistoryTest : public ::testing::Test {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
AerospikeDriverAtomicWritesOneVisitHistoryTest();

virtual ~AerospikeDriverAtomicWritesOneVisitHistoryTest();

void SetUp();

void TearDown();
};

#endif //COMMON_AerospikeDriverAtomicWritesOneVisitHistoryTestS_H
