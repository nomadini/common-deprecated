//
// Created by Ms.Rahmani on 7/12/2016.
//
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlCampaignServiceTest.h"
#include "Campaign.h"
#include "MySqlCampaignService.h"
#include "MySqlDriver.h"
#include "EventLog.h"
#include "BeanFactory.h"
#include "CollectionUtil.h"

MySqlCampaignServiceTest::MySqlCampaignServiceTest() {
        // this->service = beanFactory->mySqlCampaignService;
}
MySqlCampaignServiceTest::~MySqlCampaignServiceTest() {

}

void MySqlCampaignServiceTest::SetUp() {
        service->deleteAll ();
}

void MySqlCampaignServiceTest::TearDown() {

}

std::shared_ptr<Campaign> MySqlCampaignServiceTest::givenCampaign(){
        auto cmp = std::make_shared<Campaign>();
        cmp->setName("abc");
        cmp->setStatus("1a");
        cmp->setDescription("abc is 123");
        cmp->setAdvertiserId(21);

        cmp->setMaxImpression(3222);
        cmp->setDailyMaxImpression(4444);
        cmp->setMaxBudget(564);
        cmp->setDailyMaxBudget(56);
        cmp->setCpm(7.2);
        return cmp;
}

void MySqlCampaignServiceTest::whenCampaignIsInserted(std::shared_ptr<Campaign> cmp){


        service->insert(cmp);

}

std::shared_ptr<Campaign> MySqlCampaignServiceTest::thenCampaignIsReadById(int id){


        std::shared_ptr<MySqlCampaignService> service;//= beanFactory->mySqlCampaignService;

        std::shared_ptr<Campaign> ser=service->readById(id);
        return ser;
}

void MySqlCampaignServiceTest::thenCampaignsAreEqual(std::shared_ptr<Campaign> cmpReadFromDb, std::shared_ptr<Campaign> cmpInsretedInDb)  {

        EXPECT_THAT( cmpReadFromDb->getName(),
                     testing::Eq(cmpInsretedInDb->getName()));
        EXPECT_THAT( cmpReadFromDb->getStatus(),
                     testing::Eq(cmpInsretedInDb->getStatus()));
        EXPECT_THAT( cmpReadFromDb->getDescription(),
                     testing::Eq(cmpInsretedInDb->getDescription()));
        EXPECT_THAT( cmpReadFromDb->getAdvertiserId(),
                     testing::Eq(cmpInsretedInDb->getAdvertiserId()));
        EXPECT_THAT( cmpReadFromDb->getMaxImpression(),
                     testing::Eq(cmpInsretedInDb->getMaxImpression()));
        EXPECT_THAT( cmpReadFromDb->getDailyMaxImpression(),
                     testing::Eq(cmpInsretedInDb->getDailyMaxImpression()));
        EXPECT_THAT( cmpReadFromDb->getMaxBudget(),
                     testing::Eq(cmpInsretedInDb->getMaxBudget()));
        EXPECT_THAT( cmpReadFromDb->getDailyMaxBudget(),
                     testing::Eq(cmpInsretedInDb->getDailyMaxBudget()));
        EXPECT_THAT( cmpReadFromDb->getCpm(),
                     testing::Eq(cmpInsretedInDb->getCpm()));

}

TEST_F(MySqlCampaignServiceTest, testInsertingAndReadingCampaign) {

        auto cmp = givenCampaign();
        whenCampaignIsInserted(cmp);
        auto cmpReadFromDb= thenCampaignIsReadById(cmp->getId());
        thenCampaignsAreEqual(cmpReadFromDb, cmp);
}
