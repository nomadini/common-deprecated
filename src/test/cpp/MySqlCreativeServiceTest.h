//
// Created by mtaabodi on 7/13/2016.
//

#ifndef EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
#define EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H



#include "CreativeTypeDefs.h"
class Creative;
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "CreativeTypeDefs.h"
class Creative;
#include "MySqlCreativeService.h"
class MySqlCreativeServiceTest  : public ::testing::Test {

private:

public:

    std::shared_ptr<MySqlCreativeService> service;

    void SetUp();

    void TearDown();

    MySqlCreativeServiceTest() ;

    std::shared_ptr<Creative> givenCreative();

    void whenCreativeIsInserted(std::shared_ptr<Creative> crv);

    std::shared_ptr<Creative> thenCreativeIsReadById(int id);

    void thenCreativesAreEqual(std::shared_ptr<Creative> crvReadFromDb, std::shared_ptr<Creative> crvInsretedInDb);

    virtual ~MySqlCreativeServiceTest() ;

};
#endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
