//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef RecencyModelTest_H
#define RecencyModelTest_H


#include "AerospikeDriver.h"
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "RecencyModel.h"
class EntityToModuleStateStats;
class RecencyModelTest : public ::testing::Test {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
RecencyModelTest();

virtual ~RecencyModelTest();

void SetUp();

void testHistogram();

void TearDown();
};

#endif //COMMON_RecencyModelTestS_H
