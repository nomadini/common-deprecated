// //
// // Created by mtaabodi on 7/13/2016.
// //
// #include "GUtil.h"
// #include "JsonUtil.h"
// #include "JsonArrayUtil.h"
// #include "JsonMapUtil.h"
// #include "StringUtil.h"
// #include <string>
// #include <memory>
// #include "DateTimeUtil.h"
// #include "AdvertiserCacheServiceTest.h"
// 
// #include "EventLog.h"
// #include "BeanFactory.h"
// #include "CollectionUtil.h"
// #include "HttpUtilServiceMock.h"
// #include "AdvertiserTestHelper.h"
// #include "Advertiser.h"
//
// AdvertiserCacheServiceTest::AdvertiserCacheServiceTest() {
// }
//
// AdvertiserCacheServiceTest::~AdvertiserCacheServiceTest() {
//
// }
//
// std::string AdvertiserCacheServiceTest::createArrayOfAdvertisers(std::shared_ptr<Advertiser> advertiserExpected) {
//         RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
//         auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
//         advertiserExpected->addPropertiesToJsonValue (value, doc);
//
//         doc->PushBack (value, doc->GetAllocator ());
//         return JsonUtil::docToString (doc.get());
// }
//
// void AdvertiserCacheServiceTest::givenHttpServiceReturnsJsonData(){
//
//         auto advertiserExpected = AdvertiserTestHelper::createSampleAdvertiser();
//         allAdvertisersExpected.insert(std::make_pair(advertiserExpected->getId (), advertiserExpected));
//
//         std::string sampleJsonOfListOfAdvertisers = createArrayOfAdvertisers(advertiserExpected);
//         MLOG(3)<<"sampleJsonOfListOfAdvertisers : "<<sampleJsonOfListOfAdvertisers;
//         ON_CALL(*httpUtilServiceMock,
//                 sendPostRequest (_,_,_))
//         .WillByDefault(Return(sampleJsonOfListOfAdvertisers));
//
// }
//
// void AdvertiserCacheServiceTest::whenAdvertiserReloaCachesViaHtppIsIsCalled() {
//         this->service->reloadCachesViaHttp();
// }
//
// void AdvertiserCacheServiceTest::SetUp() {
//         this->httpUtilServiceMock = std::make_shared<HttpUtilServiceMock>();
//         this->service->httpUtilService = httpUtilServiceMock;
//         this->service->clearCaches();
//         allAdvertisersExpected.clear();
// }
//
// void AdvertiserCacheServiceTest::TearDown() {
//
// }
//
// void AdvertiserCacheServiceTest::thenBothAdvertisersAreEqual(std::shared_ptr<Advertiser> actualAdvertiser, std::shared_ptr<Advertiser> expectedAdvertiser) {
//         EXPECT_THAT( actualAdvertiser->id,
//                      testing::Eq(expectedAdvertiser->id));
//         EXPECT_THAT( actualAdvertiser->name,
//                      testing::Eq(expectedAdvertiser->name));
//
//         EXPECT_THAT( actualAdvertiser->status,
//                      testing::Eq(expectedAdvertiser->status));
//         EXPECT_THAT( actualAdvertiser->clientId,
//                      testing::Eq(expectedAdvertiser->clientId));
//
//         EXPECT_THAT( actualAdvertiser->createdAt,
//                      testing::Eq(expectedAdvertiser->createdAt));
//
//         // auto actulaDomainNames = CollectionUtil::getKeySetOfConcurrentMap<std::string, int>(*actualAdvertiser->domainNames);
//         // auto expectedDomainNames = CollectionUtil::getKeySetOfConcurrentMap<std::string, int>(*expectedAdvertiser->domainNames);
//         //
//         // EXPECT_THAT(CollectionUtil::areListsEqual(actulaDomainNames, expectedDomainNames), testing::Eq(true));
// }
//
// void AdvertiserCacheServiceTest::thenAllAdvertisersAreAsExpected()  {
//
//         auto actualAdvertisersMap = *this->service->allEntitiesMap;
//         EXPECT_TRUE (!this->service->allEntitiesMap->empty());
//
//

//
//         //BOOST_FOREACH_MAP
//         for(auto const& entry :  allAdvertisersExpected) {
//                 auto expectedAdvertiser = entry.second;
//                 auto pair = actualAdvertisersMap.find(expectedAdvertiser->getId());
//                 if (pair == actualAdvertisersMap.end()) {
//                         LOG(WARNING)<<"couldnt find this advertiser id in actual map : " <<expectedAdvertiser->getId();
//                         EXPECT_TRUE (false);//we didn't the expected advertiser in actual advertisers map, so we fail here
//                         continue;
//                 }
//                 auto actualAdvertiser = pair->second;
//                 thenBothAdvertisersAreEqual(actualAdvertiser, expectedAdvertiser);
//
//         }
// }
//
// void AdvertiserCacheServiceTest::givenDataInCache() {
//         auto advertiser = AdvertiserTestHelper::createSampleAdvertiser();
//         this->service->allEntitiesMap->insert(std::make_pair(advertiser->id, advertiser));
//
//         allAdvertisersExpected.insert(std::make_pair(advertiser->getId (), advertiser));
// }
//
// void AdvertiserCacheServiceTest::whenGetAllAsJsonIsCalled() {
//         this->allEntitiesInJsonReturned = this->servicecacheServiceServer->getAllAsJson ();
//         MLOG(3)<<"allEntitiesInJsonReturned : "<<allEntitiesInJsonReturned<<std::endl;
// }
//
// void AdvertiserCacheServiceTest::thenAllEntitiesJosnIsCreatedAsExpected() {
//
//         auto docReturned = parseJsonSafely(this->allEntitiesInJsonReturned);
//         std::unordered_map<int, std::shared_ptr<Advertiser>> allAdvertisersBuiltFromResponse;
//         for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//                 auto& advValue = (*docReturned)[i];
//                 std::shared_ptr<Advertiser> adv = Advertiser::fromJsonValue(advValue);
//                 MLOG(3) << "parsed this adv : "<<adv->toString();
//                 allAdvertisersBuiltFromResponse.insert(std::make_pair(adv->getId (), adv));
//         }
//
//         EXPECT_TRUE(allAdvertisersBuiltFromResponse.size() == allAdvertisersExpected.size());
//         EXPECT_TRUE(!allAdvertisersBuiltFromResponse.empty());
//

//
// //    BOOST_FOREACH_MAP
//         for(auto const& entry :  allAdvertisersExpected) {
//                 auto expectedadvertiser = entry.second;
//                 auto pair = allAdvertisersBuiltFromResponse.find(expectedadvertiser->getId());
//                 if (pair == allAdvertisersBuiltFromResponse.end()) {
//                         LOG(WARNING)<<"couldnt find this advertiser id in actual map : " <<expectedadvertiser->getId();
//                         EXPECT_TRUE (false);//we didn't the expected advertiser in actual advertisers map, so we fail here
//                         continue;
//                 }
//                 auto actualadvertiser = pair->second;
//                 thenBothAdvertisersAreEqual(actualadvertiser, expectedadvertiser);
//         }
// }
//
// TEST_F(AdvertiserCacheServiceTest, testParsingAdvertiserFromHttpUtilService) {
//
//         givenHttpServiceReturnsJsonData();
//
//         whenAdvertiserReloaCachesViaHtppIsIsCalled();
//
//         thenAllAdvertisersAreAsExpected();
// }
//
//
// TEST_F(AdvertiserCacheServiceTest, testGetAllEntitiesAsJson) {
//
//         givenDataInCache();
//
//         whenGetAllAsJsonIsCalled();
//
//         thenAllEntitiesJosnIsCreatedAsExpected();
// }
