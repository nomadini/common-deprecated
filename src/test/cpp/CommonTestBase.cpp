#include "CommonTestBase.h"
#include "BeanFactory.h"
#include "TargetGroupCacheService.h"
#include "TargetGroup.h"
#include "TargetGroupTestHelper.h"

CommonTestBase::CommonTestBase() {
        std::string appName = "CommonTestBase";
        std::string appVersion = "1";
        std::string logDirectory = "";
        std::string commonPropertyFileName = "common-test.properties";
        std::string propertyFileName = "common-test.properties";

        beanFactory = std::make_shared<BeanFactory>(appVersion);
        beanFactory->commonPropertyFileName = commonPropertyFileName;
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->appName = appName;
        beanFactory->initializeModules();

}

CommonTestBase::~CommonTestBase() {

}

void CommonTestBase::SetUp() {

}

void CommonTestBase::TearDown() {

}
