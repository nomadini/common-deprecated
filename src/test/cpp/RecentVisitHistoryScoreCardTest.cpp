//
// Created by Mahmoud Taabodi on 7/12/16.
//

#include "RecentVisitHistoryScoreCardTest.h"
#include "GUtil.h"

#include "EntityToModuleStateStats.h"
#include <string>
#include <memory>
#include "BeanFactory.h"
#include "TestsCommon.h"
#include "RecentVisitHistoryScoreCard.h"
#include <aerospike/as_config.h>
#include "AerospikeUtils.h"
#include <thread>
#include <algorithm>
#include <string>
#include <iostream>
#include <cctype>


#include <cmath>
#include <vector>
#include <iostream>
#include <boost/foreach.hpp>


RecentVisitHistoryScoreCardTest::RecentVisitHistoryScoreCardTest() {

}

RecentVisitHistoryScoreCardTest::~RecentVisitHistoryScoreCardTest() {

}

void RecentVisitHistoryScoreCardTest::SetUp() {

}

void RecentVisitHistoryScoreCardTest::TearDown() {

}

TEST_F(RecentVisitHistoryScoreCardTest, testRecentVisitHistoryScoreCardToJson) {

        auto model = std::make_shared<RecentVisitHistoryScoreCard>();

        auto tgModelEligibilityRecord  = std::make_shared<TgModelEligibilityRecord>();
        tgModelEligibilityRecord->targetGroupId = 1;
        tgModelEligibilityRecord->id = 2;
        tgModelEligibilityRecord->score = 3.24;
        tgModelEligibilityRecord->eligibile = "yes";
        auto scoreRecord = std::make_shared<ScoreRecord>();
        scoreRecord->timeScoredInUtc = 10;
        scoreRecord->scoreRecorded = 3.24;
        scoreRecord->scoreBase = 3.24;
        tgModelEligibilityRecord->scoreHistory->push_back(scoreRecord);

        auto tgScoreEligibilityRecord = std::make_shared<TgScoreEligibilityRecord>();
        tgScoreEligibilityRecord->modelIdToEligibilityRecords->insert(std::make_pair(1, tgModelEligibilityRecord));

        model->targetGroupIdToQualification->insert(std::make_pair(1, tgScoreEligibilityRecord));
        LOG(ERROR) << "model : " <<model->toJson();

        auto newModel = RecentVisitHistoryScoreCard::fromJson(model->toJson());
        LOG(ERROR) << "newModel : " <<newModel->toJson();

        EXPECT_THAT(newModel->toJson(), testing::Eq(model->toJson()));
}
