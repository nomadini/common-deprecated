// //
// // Created by mtaabodi on 7/13/2016.
// //
// #include "GUtil.h"
// #include "JsonUtil.h"
// #include "JsonArrayUtil.h"
// #include "JsonMapUtil.h"
// #include "StringUtil.h"
// #include <string>
// #include <memory>
// #include "DateTimeUtil.h"
// #include "CampaignCacheServiceTest.h"
// #include "Campaign.h"
// #include "CampaignCacheService.h"
// #include "CampaignTestHelper.h"
// #include "BeanFactory.h"
// #include "CollectionUtil.h"
// #include "HttpUtilServiceMock.h"
//
// CampaignCacheServiceTest::CampaignCacheServiceTest() {
//         this->service = beanFactory->campaignCacheService;
// }
//
// CampaignCacheServiceTest::~CampaignCacheServiceTest() {
//
// }
//
// std::string CampaignCacheServiceTest::createArrayOfCampaigns(std::shared_ptr<Campaign> campaignExpected) {
//         RapidJsonValueTypeNoRef crtObject(rapidjson::kObjectType);
//         auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
//         campaignExpected->addPropertiesToJsonValue (crtObject, doc);
//
//         doc->PushBack (crtObject, doc->GetAllocator ());
//         return JsonUtil::docToString (doc.get());
// }
//
// void CampaignCacheServiceTest::givenHttpServiceReturnsJsonData(){
//
//         auto campaignExpected = CampaignTestHelper::createSampleCampaign();
//         allCampaignsExpected.insert(std::make_pair(campaignExpected->getId (), campaignExpected));
//         //
//         std::string sampleJsonOfListOfCampaigns = createArrayOfCampaigns(campaignExpected);
//         MLOG(3)<<"sampleJsonOfListOfCampaigns : "<<sampleJsonOfListOfCampaigns;
//         ON_CALL(*httpUtilServiceMock,
//                 sendPostRequest (_,_,_))
//         .WillByDefault(Return(sampleJsonOfListOfCampaigns));
// }
//
// void CampaignCacheServiceTest::whenCampaignReloaCachesViaHtppIsIsCalled() {
//         this->service->reloadCachesViaHttp();
// }
//
// void CampaignCacheServiceTest::SetUp() {
//         this->httpUtilServiceMock = std::make_shared<HttpUtilServiceMock>();
//         this->service->httpUtilService = httpUtilServiceMock;
//         this->service->clearCaches();
//         this->allEntitiesExpected.clear();
// }
//
// void CampaignCacheServiceTest::TearDown() {
//
// }
//
// void CampaignCacheServiceTest::thenBothCampaignsAreEqual(std::shared_ptr<Campaign> actualCampaign, std::shared_ptr<Campaign> expectedCampaign) {
//         CampaignTestHelper::thenBothCampaignsAreEqual(actualCampaign, expectedCampaign);
// }
//
// void CampaignCacheServiceTest::thenAllCampaignsAreAsExpected()  {
//
//         std::unordered_map<int, std::shared_ptr<Campaign>> actualCampaignsMap;
//         EXPECT_TRUE (!this->service->allEntities->empty());
//
//         for (std::shared_ptr<Campaign> actualCampaign :  *this->service->allEntities) {
//                 actualCampaignsMap.insert(std::pair<int, std::shared_ptr<Campaign>>(actualCampaign->getId(), actualCampaign));
//         }
//
//         EXPECT_THAT(actualCampaignsMap.size(),
//                     testing::Eq(this->allEntitiesExpected.size()));
//
//

//
// //    BOOST_FOREACH_MAP
//         for(auto const& entry :  allCampaignsExpected) {
//                 auto expectedCampaign = entry.second;
//                 auto pair = actualCampaignsMap.find(expectedCampaign->getId());
//                 if (pair == actualCampaignsMap.end()) {
//                         LOG(WARNING)<<"couldnt find this campaign id in actual map : " <<expectedCampaign->getId();
//                         EXPECT_TRUE (false);//we didn't the expected campaign in actual campaigns map, so we fail here
//                         continue;
//                 }
//                 auto actualCampaign = pair->second;
//                 thenBothCampaignsAreEqual(actualCampaign, expectedCampaign);
//         }
// }
//
// void CampaignCacheServiceTest::givenDataInCache() {
//         auto campaign = CampaignTestHelper::createSampleCampaign();
//         this->service->allEntities->push_back(campaign);
//         allCampaignsExpected.insert(std::make_pair(campaign->getId(), campaign));
//
//         campaign = CampaignTestHelper::createSampleCampaign();
//         this->service->allEntities->push_back(campaign);
//         allCampaignsExpected.insert(std::make_pair(campaign->getId(), campaign));
// }
//
// void CampaignCacheServiceTest::whenGetAllAsJsonIsCalled() {
//         this->allEntitiesInJsonReturned = this->servicecacheServiceServer->getAllAsJson ();
//         MLOG(3)<<"allEntitiesInJsonReturned : "<<allEntitiesInJsonReturned<<std::endl;
// }
//
// void CampaignCacheServiceTest::thenAllEntitiesJosnIsCreatedAsExpected() {
//         auto docReturned = parseJsonSafely(this->allEntitiesInJsonReturned);
//         std::unordered_map<int, std::shared_ptr<Campaign>> allCampaignsBuiltFromResponse;
//         for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//                 auto& entityValue = (*docReturned)[i];
//                 std::shared_ptr<Campaign> entity = Campaign::fromJsonValue(entityValue);
//                 MLOG(3) << "parsed this entity : "<<entity->toString();
//                 allCampaignsBuiltFromResponse.insert(std::make_pair(entity->getId (), entity));
//         }
//
//         EXPECT_TRUE(allCampaignsBuiltFromResponse.size() == allCampaignsExpected.size());
//         EXPECT_TRUE(!allCampaignsBuiltFromResponse.empty());
//

//
// //    BOOST_FOREACH_MAP
//         for(auto const& entry :  allCampaignsExpected) {
//                 auto expectedCampaign = entry.second;
//                 auto pair = allCampaignsBuiltFromResponse.find(expectedCampaign->getId());
//                 if (pair == allCampaignsBuiltFromResponse.end()) {
//                         LOG(WARNING)<<"couldnt find this Campaign id in actual map : " <<expectedCampaign->getId();
//                         EXPECT_TRUE (false);//we didn't the expected Campaign in actual Campaigns map, so we fail here
//                         continue;
//                 }
//                 auto actualCampaign = pair->second;
//                 thenBothCampaignsAreEqual(actualCampaign, expectedCampaign);
//         }
// }
//
// TEST_F(CampaignCacheServiceTest, testParsingCampaignFromHttpUtilService) {
//
//         givenHttpServiceReturnsJsonData();
//
//         whenCampaignReloaCachesViaHtppIsIsCalled();
//
//         thenAllCampaignsAreAsExpected();
// }
//
//
// TEST_F(CampaignCacheServiceTest, testGetAllEntitiesAsJson) {
//
//         givenDataInCache();
//
//         whenGetAllAsJsonIsCalled();
//
//         thenAllEntitiesJosnIsCreatedAsExpected();
// }
