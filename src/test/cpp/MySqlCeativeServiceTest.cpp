//
// Created by mtaabodi on 7/13/2016.
//
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlCreativeServiceTest.h"
#include "MySqlCreativeService.h"
#include "Creative.h"
#include "MySqlDriver.h"
#include "EventLog.h"
#include "BeanFactory.h"
#include "CollectionUtil.h"

MySqlCreativeServiceTest::MySqlCreativeServiceTest() {
        // this->service= beanFactory->mySqlCreativeService;
}
MySqlCreativeServiceTest::~MySqlCreativeServiceTest() {

}

std::shared_ptr<Creative> MySqlCreativeServiceTest::givenCreative(){
        auto crv = std::make_shared<Creative>();
        crv->setName("abc");
        crv->setStatus("1a");
        crv->setDescription("abc is 123");
        crv->setAdvertiserId(21);

        crv->setAdTag(" tag ");
        crv->setPreviewUrl(" url ");
        crv->setSize(" size ");
        return crv;
}


void MySqlCreativeServiceTest::whenCreativeIsInserted(std::shared_ptr<Creative> crv){
        service->insert(crv);
}

void MySqlCreativeServiceTest::SetUp() {
        service->deleteAll ();
}

void MySqlCreativeServiceTest::TearDown() {

}

std::shared_ptr<Creative> MySqlCreativeServiceTest::thenCreativeIsReadById(int id) {

        std::shared_ptr<MySqlCreativeService> service;//= beanFactory->mySqlCreativeService;

        auto creative=service->readById (id);
        return creative;
}

void MySqlCreativeServiceTest::thenCreativesAreEqual(std::shared_ptr<Creative> crvReadFromDb, std::shared_ptr<Creative> crvInsretedInDb)  {

        EXPECT_THAT( crvReadFromDb->getName(),
                     testing::Eq(crvInsretedInDb->getName()));
        EXPECT_THAT( crvReadFromDb->getStatus(),
                     testing::Eq(crvInsretedInDb->getStatus()));
        EXPECT_THAT( crvReadFromDb->getDescription(),
                     testing::Eq(crvInsretedInDb->getDescription()));
        EXPECT_THAT( crvReadFromDb->getAdvertiserId(),
                     testing::Eq(crvInsretedInDb->getAdvertiserId()));
        EXPECT_THAT( crvReadFromDb->getAdTag(),
                     testing::Eq(crvInsretedInDb->getAdTag()));
        EXPECT_THAT( crvReadFromDb->getPreviewUrl(),
                     testing::Eq(crvInsretedInDb->getPreviewUrl()));
        EXPECT_THAT( crvReadFromDb->getSize(),
                     testing::Eq(crvInsretedInDb->getSize()));


}

TEST_F(MySqlCreativeServiceTest, testInsertingAndReadingCreative) {

        auto crv = givenCreative();
        whenCreativeIsInserted(crv);
        auto crvReadFromDb= thenCreativeIsReadById(crv->getId());
        thenCreativesAreEqual(crvReadFromDb, crv);
}
