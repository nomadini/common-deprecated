//
// Created by Ms.Rahmani on 7/12/2016.
//

#ifndef EXCHANGESIMULATOR_MYSQLCAMPAIGNSERVICETEST_H
#define EXCHANGESIMULATOR_MYSQLCAMPAIGNSERVICETEST_H

#include "TargetGroupTypeDefs.h"
class TargetGroup;
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "CampaignTypeDefs.h"
#include "MySqlCampaignService.h"
class MySqlCampaignServiceTest  : public ::testing::Test {

private:

public:

    void SetUp();

    void TearDown();

    std::shared_ptr<MySqlCampaignService> service;

    MySqlCampaignServiceTest();

    std::shared_ptr<Campaign> givenCampaign();

    void whenCampaignIsInserted(std::shared_ptr<Campaign> cmp);

    std::shared_ptr<Campaign> thenCampaignIsReadById(int id);

    void thenCampaignsAreEqual(std::shared_ptr<Campaign> cmpReadFromDb, std::shared_ptr<Campaign> cmpInsretedInDb);

    virtual ~MySqlCampaignServiceTest() ;

};


#endif //EXCHANGESIMULATOR_MYSQLCAMPAIGNSERVICETEST_H
