/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef EventLogCassandraServiceTests_H
#define EventLogCassandraServiceTests_H

#include "TargetGroupTypeDefs.h"
class TargetGroup;
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"

class EventLogCassandraServiceTest  : public ::testing::Test {

private:

public:

	EventLogCassandraServiceTest() ;

    virtual ~EventLogCassandraServiceTest() ;

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */
