// //
// // Created by mtaabodi on 7/13/2016.
// //
// #include "GUtil.h"
// #include "JsonUtil.h"
// #include "JsonArrayUtil.h"
// #include "JsonMapUtil.h"
// #include "StringUtil.h"
// #include <string>
// #include <memory>
// #include "DateTimeUtil.h"
// #include "TargetGroupGeoLocationCacheServiceTest.h"
// #include "TargetGroupGeoLocationCacheService.h"
// #include "EventLog.h"
// #include "BeanFactory.h"
// #include "CollectionUtil.h"
// #include "HttpUtilServiceMock.h"
// #include "RandomUtil.h"
// #include "FunctionUtil.h"
// TargetGroupGeoLocationCacheServiceTest::TargetGroupGeoLocationCacheServiceTest() {
//     this->service = beanFactory->targetGroupGeoLocationCacheService;
// }
//
// TargetGroupGeoLocationCacheServiceTest::~TargetGroupGeoLocationCacheServiceTest() {
//
// }
//
// std::string TargetGroupGeoLocationCacheServiceTest::createArrayOfTargetGroupGeoLocations(std::shared_ptr<TargetGroupGeoLocationList> targetGroupExpected) {
//     RapidJsonValueTypeNoRef crtObject(rapidjson::kObjectType);
//     auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
//     targetGroupExpected->addPropertiesToJsonValue (crtObject, doc);
//
//     doc->PushBack (crtObject , doc->GetAllocator ());
//     return JsonUtil::docToString (doc.get());
// }
//
// std::shared_ptr<TargetGroupGeoLocationList> TargetGroupGeoLocationCacheServiceTest::createSampleEntity() {
//   std::shared_ptr<TargetGroupGeoLocationList> entity = std::make_shared<TargetGroupGeoLocationList>();
//
//   entity->id = RandomUtil::sudoRandomNumber(10000);
//   entity->targetGroupId = RandomUtil::sudoRandomNumber(10000);
//   auto geoLocation = std::make_shared<GeoLocation>();
//   geoLocation->id= RandomUtil::sudoRandomNumber(10000);
//   geoLocation->city= "newyork";
//   geoLocation->state= "ny";
//   geoLocation->state= "country";
//   entity->geoLocationList.push_back(geoLocation);
//   entity->dateCreated = DateTimeUtil::getNowInMySqlFormat();
//   entity->dateModified =  DateTimeUtil::getNowInMySqlFormat();
//
//   return entity;
// }
//
// void TargetGroupGeoLocationCacheServiceTest::whenGetAllAsJsonIsCalled() {
//     this->allEntitiesInJsonReturned = this->servicecacheServiceServer->getAllAsJson ();
//     MLOG(3)<<"allEntitiesInJsonReturned : "<<allEntitiesInJsonReturned<<std::endl;
// }
//
// std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> TargetGroupGeoLocationCacheServiceTest::parseDtoReturnedToMap(std::string json) {
//
//   std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> actualTargetGroupsMap;
//   auto docReturned = parseJsonSafely(json);
//   for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//       auto& advValue = (*docReturned)[i];
//       std::shared_ptr<TargetGroupGeoLocationList> adv = TargetGroupGeoLocationList::fromJsonValue(advValue);
//       MLOG(3) << "parsed this std::shared_ptr<TargetGroupGeoLocationList> : "<<adv->toString();
//       actualTargetGroupsMap.insert(std::make_pair(adv->id, adv));
//   }
//   return actualTargetGroupsMap;
// }
//
// void TargetGroupGeoLocationCacheServiceTest::thenBothTargetGroupGeoLocationsAreEqual
// (std::shared_ptr<TargetGroupGeoLocationList> actualTargetGroupGeoLocation,
//    std::shared_ptr<TargetGroupGeoLocationList> expectedTargetGroupGeoLocation) {
//      MLOG(3) << "actualTargetGroupGeoLocation : "<<actualTargetGroupGeoLocation->toJson();
//      MLOG(3) << "expectedTargetGroupGeoLocation : "<<expectedTargetGroupGeoLocation->toJson();
//
//     EXPECT_THAT(actualTargetGroupGeoLocation->id,
//                  testing::Eq(expectedTargetGroupGeoLocation->id));
//
//     EXPECT_THAT(actualTargetGroupGeoLocation->targetGroupId,
//               testing::Eq(expectedTargetGroupGeoLocation->targetGroupId));
//
//     EXPECT_THAT(actualTargetGroupGeoLocation->dateCreated,
//                         testing::Eq(expectedTargetGroupGeoLocation->dateCreated));
//
//     EXPECT_THAT(actualTargetGroupGeoLocation->dateModified,
//                                             testing::Eq(expectedTargetGroupGeoLocation->dateModified));
//
//
//     auto actualGeoLocationLists = actualTargetGroupGeoLocation->geoLocationList;
//     auto expectedGeoLocationLists = expectedTargetGroupGeoLocation->geoLocationList;
//     auto actualMapOfGeoLocations = CollectionUtil::convertListToMap(actualGeoLocationLists);
//     auto expectedMapOfGeoLocations = CollectionUtil::convertListToMap(expectedGeoLocationLists);
//     boost::function< bool(std::shared_ptr<GeoLocation>, std::shared_ptr<GeoLocation>) > func =
//     boost::bind(&GeoLocation::equalFunction, _1, _2);
//     //ADD EXPECTATION HERE
//     EXPECT_TRUE(CollectionUtil::areMapsEqualWithFunc(actualMapOfGeoLocations, expectedMapOfGeoLocations, func));
//
// }
// void TargetGroupGeoLocationCacheServiceTest::assertBothMapsAreEqual(
//                       std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> actualTargetGroupsMap,
//                        std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> allTargetGroupCreativesExpected ) {
//                          EXPECT_THAT(actualTargetGroupsMap.size(),
//                                      testing::Eq(allTargetGroupCreativesExpected.size()));
//

//
//                          //    BOOST_FOREACH_MAP
//                          for(auto const& entry  :  allTargetGroupCreativesExpected) {
//                              auto expectedTgCreativeMap = entry.second;
//                              auto pair = actualTargetGroupsMap.find(expectedTgCreativeMap->id);
//                              if (pair == actualTargetGroupsMap.end()) {
//                                  LOG(WARNING)<<"couldnt find this tg map id in actual map : " <<expectedTgCreativeMap->id;
//                                  EXPECT_TRUE (false);//we didn't the expected creative in actual creatives map, so we fail here
//                                  continue;
//                              }
//                              auto actualTgCreativeMap = pair->second;
//                              thenBothTargetGroupGeoLocationsAreEqual(actualTgCreativeMap, expectedTgCreativeMap);
//                          }
// }
//
// void TargetGroupGeoLocationCacheServiceTest::givenHttpServiceReturnsJsonData(){
//
//     auto targetGroupExpected = createSampleEntity();
//     allTargetGroupGeoLocationsExpected.insert(std::make_pair(targetGroupExpected->id, targetGroupExpected));
//
//     std::string sampleJsonOfListOfTargetGroupGeoLocations = createArrayOfTargetGroupGeoLocations(targetGroupExpected);
//     MLOG(3)<<"sampleJsonOfListOfTargetGroupGeoLocations : "<<sampleJsonOfListOfTargetGroupGeoLocations;
//     ON_CALL(*httpUtilServiceMock,
//             sendPostRequest (_,_,_))
//             .WillByDefault(Return(sampleJsonOfListOfTargetGroupGeoLocations));
//
// }
//
// void TargetGroupGeoLocationCacheServiceTest::whenTargetGroupGeoLocationReloaCachesViaHtppIsIsCalled() {
//     this->service->reloadCachesViaHttp();
// }
//
// void TargetGroupGeoLocationCacheServiceTest::SetUp() {
//   this->httpUtilServiceMock = std::make_shared<HttpUtilServiceMock>();
//   this->service->httpUtilService = httpUtilServiceMock;
//   this->allTargetGroupGeoLocationsExpected.clear();
//   this->service->allTargetGroupGeoLocationMaps.clear();
//   this->service->clearCaches();
// }
//
// void TargetGroupGeoLocationCacheServiceTest::TearDown() {
//
// }
//
//
//
// void TargetGroupGeoLocationCacheServiceTest::thenAllTargetGroupGeoLocationsAreAsExpected()  {
//     MLOG(3)<<"thenAllTargetGroupGeoLocationsAreAsExpected ";
//     EXPECT_FALSE(this->allTargetGroupGeoLocationsExpected.empty());
//     assertBothMapsAreEqual(*this->service->getAllEntitiesMap(),
//     this->allTargetGroupGeoLocationsExpected );
//
// }
//
// void TargetGroupGeoLocationCacheServiceTest::givenDataInCache() {
//   auto targetGroup = createSampleEntity();
//
//   allTargetGroupGeoLocationsExpected.insert(std::make_pair(targetGroup->id, targetGroup));
//   this->service->allTargetGroupGeoLocationMaps.push_back(targetGroup);
//
//
//   targetGroup = TargetGroupGeoLocationCacheServiceTest::createSampleEntity();
//   this->service->allTargetGroupGeoLocationMaps.push_back(targetGroup);
//   allTargetGroupGeoLocationsExpected.insert(std::make_pair(targetGroup->id, targetGroup));
//
// }
//
// void TargetGroupGeoLocationCacheServiceTest::thenAllEntitiesJosnIsCreatedAsExpected() {
//   std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> actualTargetGroupsMap = parseDtoReturnedToMap(this->allEntitiesInJsonReturned);
//   EXPECT_TRUE(!actualTargetGroupsMap.empty());
//   assertBothMapsAreEqual(actualTargetGroupsMap, this->allTargetGroupGeoLocationsExpected );
//
// }
//
// TEST_F(TargetGroupGeoLocationCacheServiceTest, testParsingTargetGroupGeoLocationFromHttpUtilService) {
//
//     givenHttpServiceReturnsJsonData();
//
//     whenTargetGroupGeoLocationReloaCachesViaHtppIsIsCalled();
//
//     thenAllTargetGroupGeoLocationsAreAsExpected();
// }
//
//
// TEST_F(TargetGroupGeoLocationCacheServiceTest, testGetAllEntitiesAsJson) {
//
//     givenDataInCache();
//
//     whenGetAllAsJsonIsCalled();
//
//     thenAllEntitiesJosnIsCreatedAsExpected();
// }
