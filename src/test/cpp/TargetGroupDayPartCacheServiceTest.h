//
// Created by mtaabodi on 7/13/2016.
//

#ifndef TargetGroupDayPartCacheService_H
#define TargetGroupDayPartCacheService_H

class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
class TargetGroupDayPartCacheService;
#include "HttpUtilServiceMock.h"

class TargetGroupDayPartCacheServiceTest  : public ::testing::Test {

private:

public:

    TargetGroupDayPartCacheService* service;
    std::string allEntitiesInJsonReturned;

    std::string allEntitiesInJsonExpected;

    std::unordered_map<int , std::shared_ptr<TargetGroupDayPartTargetMap>> allTargetGroupDayPartsExpected;

    HttpUtilServiceMockPtr httpUtilServiceMock;

    void SetUp();

    void TearDown();

    std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> parseDtoReturnedToMap(std::string json);

    TargetGroupDayPartCacheServiceTest() ;

    void givenHttpServiceReturnsJsonData();

    void whenTargetGroupDayPartReloaCachesViaHtppIsIsCalled();

    void thenAllTargetGroupDayPartsAreAsExpected();

    std::shared_ptr<TargetGroupDayPartTargetMap> createSampleEntity();

    std::string createArrayOfTargetGroupDayParts(std::shared_ptr<TargetGroupDayPartTargetMap> targetGroupDayPartTargetMap);

    void assertBothMapsAreEqual(std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> actualTargetGroupsMap,
                                std::unordered_map<int, std::shared_ptr<TargetGroupDayPartTargetMap>> allTargetGroupCreativesExpected);

   void thenBothTargetGroupDayPartsAreEqual(std::shared_ptr<TargetGroupDayPartTargetMap> actualEntity,
                                            std::shared_ptr<TargetGroupDayPartTargetMap> expectedEntity);
    void givenDataInCache();

    void whenGetAllAsJsonIsCalled();

    void thenAllEntitiesJosnIsCreatedAsExpected();

    virtual ~TargetGroupDayPartCacheServiceTest() ;

};
#endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
