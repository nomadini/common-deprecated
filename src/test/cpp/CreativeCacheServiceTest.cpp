// //
// // Created by mtaabodi on 7/13/2016.
// //
// #include "GUtil.h"
// #include "JsonUtil.h"
// #include "JsonArrayUtil.h"
// #include "JsonMapUtil.h"
// #include "StringUtil.h"
// #include <string>
// #include <memory>
// #include "DateTimeUtil.h"
// #include "CreativeCacheServiceTest.h"
// #include "CreativeCacheService.h"
// #include "EventLog.h"
// #include "BeanFactory.h"
// #include "CollectionUtil.h"
// #include "Creative.h"
// #include "HttpUtilServiceMock.h"
// #include "CreativeTestHelper.h"
//
// CreativeCacheServiceTest::CreativeCacheServiceTest() {
//
//
// }
//
// CreativeCacheServiceTest::~CreativeCacheServiceTest() {
//
// }
//
// std::string CreativeCacheServiceTest::createArrayOfCreatives(std::shared_ptr<Creative> creativeExpected) {
//         RapidJsonValueTypeNoRef crtObject(rapidjson::kObjectType);
//         auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
//         creativeExpected->addPropertiesToJsonValue (crtObject, doc);
//
//         doc->PushBack (crtObject, doc->GetAllocator ());
//         return JsonUtil::docToString (doc.get());
// }
//
// void CreativeCacheServiceTest::givenHttpServiceReturnsJsonData(){
//
//         auto creativeExpected = CreativeTestHelper::createSampleCreative();
//         allCreativesExpected.insert(std::make_pair(creativeExpected->getId (), creativeExpected));
//
//         std::string sampleJsonOfListOfCreatives = createArrayOfCreatives(creativeExpected);
//         MLOG(3)<<"sampleJsonOfListOfCreatives : "<<sampleJsonOfListOfCreatives;
//         ON_CALL(*httpUtilServiceMock,
//                 sendPostRequest (_,_,_))
//         .WillByDefault(Return(sampleJsonOfListOfCreatives));
//
// }
//
// void CreativeCacheServiceTest::whenCreativeReloaCachesViaHtppIsIsCalled() {
//         this->creativeCacheService->reloadCachesViaHttp();
// }
//
// void CreativeCacheServiceTest::SetUp() {
//         this->httpUtilServiceMock = std::make_shared<HttpUtilServiceMock>();
//         this->creativeCacheService->httpUtilService = httpUtilServiceMock;
//         this->creativeCacheService->clearCaches();
//         this->allCreativesExpected.clear();
// }
//
// void CreativeCacheServiceTest::TearDown() {
//
// }
//
// void CreativeCacheServiceTest::thenBothCreativesAreEqual(std::shared_ptr<Creative> actualCreative, std::shared_ptr<Creative> expectedCreative) {
//         EXPECT_THAT( actualCreative->getId(),
//                      testing::Eq(expectedCreative->getId()));
//         EXPECT_THAT( actualCreative->getName(),
//                      testing::Eq(expectedCreative->getName()));
//         EXPECT_THAT( actualCreative->getStatus(),
//                      testing::Eq(expectedCreative->getStatus()));
//         EXPECT_THAT( actualCreative->getDescription(),
//                      testing::Eq(expectedCreative->getDescription()));
//         EXPECT_THAT( actualCreative->getAdvertiserId(),
//                      testing::Eq(expectedCreative->getAdvertiserId()));
//         EXPECT_THAT( actualCreative->getAdTag(),
//                      testing::Eq(expectedCreative->getAdTag()));
//         EXPECT_THAT( actualCreative->getPreviewUrl(),
//                      testing::Eq(expectedCreative->getPreviewUrl()));
//         EXPECT_THAT( actualCreative->getSize(),
//                      testing::Eq(expectedCreative->getSize()));
//
//         EXPECT_THAT( actualCreative->getCreatedAt (),
//                      testing::Eq(expectedCreative->getCreatedAt()));
//
//         EXPECT_THAT( actualCreative->getUpdatedAt (),
//                      testing::Eq(expectedCreative->getUpdatedAt()));
//
//         EXPECT_THAT( actualCreative->getLandingPageUrl (),
//                      testing::Eq(expectedCreative->getLandingPageUrl()));
//
//         EXPECT_THAT( actualCreative->getAdType (),
//                      testing::Eq(expectedCreative->getAdType()));
//
//         EXPECT_THAT( actualCreative->IsSecure(),
//                      testing::Eq(expectedCreative->IsSecure()));
//
//         // EXPECT_THAT(CollectionUtil::getCommonItems<std::string> (*actualCreative->getApis (), *expectedCreative->getApis ()).size (),
//         //             testing::Eq(actualCreative->getApis()->size()));
//
// }
//
// void CreativeCacheServiceTest::thenAllCreativesAreAsExpected()  {
//
//         std::unordered_map<int, std::shared_ptr<Creative>> actualCreativesMap;
//
//         auto docReturned = parseJsonSafely(this->allEntitiesInJsonReturned);
//         for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//                 auto& advValue = (*docReturned)[i];
//                 std::shared_ptr<Creative> adv = Creative::fromJsonValue(advValue);
//                 MLOG(3) << "parsed this creative : "<<adv->toString();
//                 actualCreativesMap.insert(std::make_pair(adv->getId (), adv));
//         }
//
//         EXPECT_THAT(actualCreativesMap.size(),
//                     testing::Eq(this->allCreativesExpected.size()));
//
//

//
// //    BOOST_FOREACH_MAP
//         for(auto const& entry :  allCreativesExpected) {
//                 auto expectedCreative = entry.second;
//                 auto pair = actualCreativesMap.find(expectedCreative->getId());
//                 if (pair == actualCreativesMap.end()) {
//                         LOG(WARNING)<<"couldnt find this creative id in actual map : " <<expectedCreative->getId();
//                         EXPECT_TRUE (false);//we didn't the expected creative in actual creatives map, so we fail here
//                         continue;
//                 }
//                 auto actualCreative = pair->second;
//                 thenBothCreativesAreEqual(actualCreative, expectedCreative);
//         }
// }
//
// void CreativeCacheServiceTest::givenDataInCache() {
//         auto creative = CreativeTestHelper::createSampleCreative();
//         this->service->allCreatives->push_back(creative);
//         allCreativesExpected.insert(std::make_pair(creative->getId (), creative));
//         creative = CreativeTestHelper::createSampleCreative();
//         this->service->allCreatives->push_back(creative);
//         allCreativesExpected.insert(std::make_pair(creative->getId (), creative));
// }
//
// void CreativeCacheServiceTest::whenGetAllAsJsonIsCalled() {
//         this->allEntitiesInJsonReturned = this->servicecacheServiceServer->getAllAsJson ();
//         MLOG(3)<<"allEntitiesInJsonReturned : "<<allEntitiesInJsonReturned<<std::endl;
// }
//
// void CreativeCacheServiceTest::thenAllEntitiesJosnIsCreatedAsExpected() {
//         auto docReturned = parseJsonSafely(this->allEntitiesInJsonReturned);
//         std::unordered_map<int, std::shared_ptr<Creative>> allCreativesBuiltFromResponse;
//         for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//                 auto& entityValue = (*docReturned)[i];
//                 std::shared_ptr<Creative> entity = Creative::fromJsonValue(entityValue);
//                 MLOG(3) << "parsed this entity : "<<entity->toString();
//                 allCreativesBuiltFromResponse.insert(std::make_pair(entity->getId (), entity));
//         }
//
//         EXPECT_TRUE(allCreativesBuiltFromResponse.size() == allCreativesExpected.size());
//         EXPECT_TRUE(!allCreativesBuiltFromResponse.empty());
//

//
// //    BOOST_FOREACH_MAP
//         for(auto const& entry :  allCreativesExpected) {
//                 auto expectedCreative = entry.second;
//                 auto pair = allCreativesBuiltFromResponse.find(expectedCreative->getId());
//                 if (pair == allCreativesBuiltFromResponse.end()) {
//                         LOG(WARNING)<<"couldnt find this Creative id in actual map : " <<expectedCreative->getId();
//                         EXPECT_TRUE (false);//we didn't the expected Creative in actual Creatives map, so we fail here
//                         continue;
//                 }
//                 auto actualCreative = pair->second;
//                 thenBothCreativesAreEqual(actualCreative, expectedCreative);
//         }
// }
//
// TEST_F(CreativeCacheServiceTest, testParsingCreativeFromHttpUtilService) {
//
//         givenHttpServiceReturnsJsonData();
//
//         whenCreativeReloaCachesViaHtppIsIsCalled();
//
//         thenAllCreativesAreAsExpected();
// }
//
//
// TEST_F(CreativeCacheServiceTest, testGetAllEntitiesAsJson) {
//
//         givenDataInCache();
//
//         whenGetAllAsJsonIsCalled();
//
//         thenAllEntitiesJosnIsCreatedAsExpected();
// }
