
//
// Created by mtaabodi on 7/13/2016.
//
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlAdvertiserServiceTest.h"
#include "MySqlAdvertiserService.h"
#include "Advertiser.h"
#include "MySqlDriver.h"
#include "EventLog.h"
#include "BeanFactory.h"
#include "CollectionUtil.h"

MySqlAdvertiserServiceTest::MySqlAdvertiserServiceTest() {
        // this->service= beanFactory->mySqlAdvertiserService;

}
MySqlAdvertiserServiceTest::~MySqlAdvertiserServiceTest() {

}

void MySqlAdvertiserServiceTest::SetUp() {
        service->deleteAll ();
}

void MySqlAdvertiserServiceTest::TearDown() {

}

std::shared_ptr<Advertiser> MySqlAdvertiserServiceTest::givenAdvertiser(){
        auto ad = std::make_shared<Advertiser>();
        ad->name="abc";
        ad->status="1a";
        ad->description="abc is 123";
        ad->clientId=21;

        return ad;
}


void MySqlAdvertiserServiceTest::whenAdvertiserIsInserted(std::shared_ptr<Advertiser> ad){
        service->insert(ad);
}


std::shared_ptr<Advertiser> MySqlAdvertiserServiceTest::thenAdvertiserIsReadById(int id){


        std::shared_ptr<MySqlAdvertiserService> service;//= beanFactory->mySqlAdvertiserService;

        std::shared_ptr<Advertiser> ser=service->readById(id);


        return ser;
}

void MySqlAdvertiserServiceTest::thenAdvertisersAreEqual(std::shared_ptr<Advertiser> adReadFromDb, std::shared_ptr<Advertiser> adInsretedInDb)  {


        EXPECT_THAT( adReadFromDb->getName(),
                     testing::Eq(adInsretedInDb->getName()));
        EXPECT_THAT( adReadFromDb->getStatus(),
                     testing::Eq(adInsretedInDb->getStatus()));
        EXPECT_THAT( adReadFromDb->getDescription(),
                     testing::Eq(adInsretedInDb->getDescription()));
        EXPECT_THAT( adReadFromDb->getClientId(),
                     testing::Eq(adInsretedInDb->getClientId()));

}



TEST_F(MySqlAdvertiserServiceTest, testInsertingAndReadingAdvertiser) {

        auto ad = givenAdvertiser();
        whenAdvertiserIsInserted(ad);
        auto adReadFromDb= thenAdvertiserIsReadById(ad->getId());
        thenAdvertisersAreEqual(adReadFromDb, ad);
}
