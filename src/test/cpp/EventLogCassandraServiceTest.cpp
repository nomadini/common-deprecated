
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "EventLogCassandraServiceTest.h"

#include "EventLog.h"
#include "BeanFactory.h"
#include "CassandraService.h"
#include "RandomUtil.h"
EventLogCassandraServiceTest::EventLogCassandraServiceTest() {
}
EventLogCassandraServiceTest::~EventLogCassandraServiceTest() {

}

TEST_F(EventLogCassandraServiceTest, testWritingAndReadingEvents) {

        std::string eventId = StringUtil::random_string (12) + StringUtil::toStr ("_") + StringUtil::toStr (DateTimeUtil::getNowInMicroSecond ());

        std::shared_ptr<EventLog> eventLog = std::make_shared<EventLog>(
                eventId, EventLog::EVENT_TYPE_BID, EventLog::TARGETING_TYPE_IGNORE_FOR_NOW);

        eventLog->eventTime = StringUtil::toStr(DateTimeUtil::getNowInMilliSecond());


        eventLog->targetGroupId = RandomUtil::sudoRandomNumber (3);
        eventLog->creativeId = RandomUtil::sudoRandomNumber (3);
        eventLog->campaignId = RandomUtil::sudoRandomNumber (3);
        eventLog->advertiserId = RandomUtil::sudoRandomNumber (3);
        eventLog->publisherId = RandomUtil::sudoRandomNumber (3);

        // eventLog->winBidPrice = StringUtil::random_string (3);

        std::shared_ptr<CassandraService<EventLog> > eventLogCassandraService;
        // = std::make_shared<CassandraService<EventLog> > (
        //         beanFactory->cassandraDriver,
        //         beanFactory->httpUtilService,
        //         beanFactory->entityToModuleStateStats);

        // eventLogCassandraService->deleteAll();
        // eventLogCassandraService->save(eventLog);

        std::vector<std::shared_ptr<EventLog> > eventsFromCassandra;
        //  = eventLogCassandraService->readEventLog(DateTimeUtil::getNowInYYYMMDDFormat ());

        EXPECT_THAT(eventsFromCassandra.size(), testing::Eq(1));
        auto eventLogFromDb = eventsFromCassandra.at(0);

        EXPECT_THAT(eventLogFromDb->eventTime, eventLog->eventTime);
        EXPECT_THAT(eventLogFromDb->eventType, eventLog->eventType);
        EXPECT_THAT(eventLogFromDb->eventId, eventLog->eventId);
        EXPECT_THAT(eventLogFromDb->targetGroupId, eventLog->targetGroupId);
        EXPECT_THAT(eventLogFromDb->creativeId, eventLog->creativeId);
        EXPECT_THAT(eventLogFromDb->campaignId, eventLog->campaignId);
        EXPECT_THAT(eventLogFromDb->advertiserId, eventLog->advertiserId);
        EXPECT_THAT(eventLogFromDb->publisherId, eventLog->publisherId);

        EXPECT_THAT(eventLogFromDb->winBidPrice, eventLog->winBidPrice);
        EXPECT_THAT(eventLogFromDb->platformCost, eventLog->platformCost);
        EXPECT_THAT(eventLogFromDb->userTimeZone, eventLog->userTimeZone);
        EXPECT_THAT(eventLogFromDb->userTimeZonDifferenceWithUTC, eventLog->userTimeZonDifferenceWithUTC);
        EXPECT_THAT(eventLogFromDb->device->getDeviceId(), eventLog->device->getDeviceId());
        EXPECT_THAT(eventLogFromDb->deviceUserAgent, eventLog->deviceUserAgent);
        EXPECT_THAT(eventLogFromDb->deviceIp, eventLog->deviceIp);
        EXPECT_THAT(eventLogFromDb->device->getDeviceId(), eventLog->device->getDeviceId());
        EXPECT_THAT(eventLogFromDb->device->getDeviceType(), eventLog->device->getDeviceType());

        EXPECT_THAT(eventLogFromDb->deviceCountry, eventLog->deviceCountry);
        EXPECT_THAT(eventLogFromDb->deviceCity, eventLog->deviceCity);
        EXPECT_THAT(eventLogFromDb->deviceZipcode, eventLog->deviceZipcode);
        EXPECT_THAT(eventLogFromDb->siteCategory, eventLog->siteCategory);



        EXPECT_THAT(eventLogFromDb->siteDomain, eventLog->siteDomain);
        EXPECT_THAT(eventLogFromDb->sitePage, eventLog->sitePage);
        EXPECT_THAT(eventLogFromDb->sitePublisherDomain, eventLog->sitePublisherDomain);
        EXPECT_THAT(eventLogFromDb->sitePublisherName, eventLog->sitePublisherName);

        EXPECT_THAT(eventLogFromDb->adSize, eventLog->adSize);
        EXPECT_THAT(eventLogFromDb->lastModuleRunInPipeline, eventLog->lastModuleRunInPipeline);
}
