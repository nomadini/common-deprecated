//
// Created by Mahmoud Taabodi on 7/12/16.
//

#include "AerospikeDriverAtomicWritesVisitHistoryTest.h"
#include "GUtil.h"

#include "EntityToModuleStateStats.h"
#include <string>
#include <memory>
#include "BeanFactory.h"
#include <aerospike/as_config.h>
#include "AerospikeUtils.h"
#include <thread>
#include <algorithm>
#include <string>
#include <iostream>
#include <cctype>



#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/polygon.hpp>

#include <boost/geometry/index/rtree.hpp>

#include <cmath>
#include <vector>
#include <iostream>
#include <boost/foreach.hpp>

#include "DeviceFeatureHistory.h"
#include "Device.h"
#include "Feature.h"
#include "DateTimeUtil.h"
#include "FeatureHistory.h"
#include "JsonArrayUtil.h"

#include "Poco/Runnable.h"
#include "Poco/ThreadPool.h"

AerospikeDriverAtomicWritesVisitHistoryTest::AerospikeDriverAtomicWritesVisitHistoryTest() {

}

AerospikeDriverAtomicWritesVisitHistoryTest::~AerospikeDriverAtomicWritesVisitHistoryTest() {

}

void AerospikeDriverAtomicWritesVisitHistoryTest::SetUp() {
        entityToModuleStateStats = new EntityToModuleStateStats();
}

void AerospikeDriverAtomicWritesVisitHistoryTest::TearDown() {

}

class Worker : public Poco::Runnable {
public:
std::shared_ptr<AerospikeDriver> driver;
std::shared_ptr<DeviceFeatureHistory> responseObject;

Worker() {
}
virtual void run() {

        for (int i = 0; i < 100; i++) {
                std::string valueWritten = driver->addValueAndRead("test",
                                                                   "testVisitHistory",
                                                                   "visithistoryKey",
                                                                   "vstBinSt3",
                                                                   responseObject->toJson(),
                                                                   //  "responseObject",
                                                                   10000);
                LOG(ERROR)<< "thread : valueWritten : "<<valueWritten;
                gicapods::Util::sleepMillis(10);
        }



}
};

TEST_F(AerospikeDriverAtomicWritesVisitHistoryTest, testWritingVisitHistoryInMultipleThreads) {
        std::string host="10.136.46.157";
        std::shared_ptr<AerospikeDriver> driver= std::make_shared<AerospikeDriver>(host, 3000, nullptr);
        driver->entityToModuleStateStats = entityToModuleStateStats;

        auto device = std::make_shared<Device>("nomadini12", "DESKTOP");
        auto responseObject = std::make_shared<DeviceFeatureHistory>(device);

        TimeType timeOfVisit = 1233;
        std::string featureName = "abc.com";

        std::string featureType = Feature::generalTopLevelDomain;
        auto feature = std::make_shared<Feature>(featureType, featureName, 0);
        auto featureHistory = std::make_shared<FeatureHistory>(feature, DateTimeUtil::getNowInMilliSecond());

        responseObject->getFeatures()->push_back(featureHistory);
        Poco::ThreadPool threadpool;

        for (int i = 0; i < 10; i++) {
                Worker work;
                work.driver = driver;
                work.responseObject = responseObject;
                threadpool.start(work);
        }
        //we started 10 threads that write to a bin atomically
        threadpool.joinAll(); // we should always join the threads


        std::string resultOfAllWrites = driver->addValueAndRead("test",
                                                                "testVisitHistory",
                                                                "visithistoryKey",
                                                                "vstBinSt3",
                                                                "",
                                                                //  "responseObject",
                                                                10000);
        LOG(ERROR) << "resultOfAllWrites : "<< resultOfAllWrites;
        std::vector<std::string> jsonRepresentation =
                StringUtil::split(resultOfAllWrites, driver->stringValueSeperator);


        std::vector<std::shared_ptr<DeviceFeatureHistory> > allFeatures;
        //we are parsing all the objects that we wrote
        for (auto json : jsonRepresentation) {
                LOG(ERROR) << "json : "<< json;
                if (!json.empty()) {
                        auto featureHistory = DeviceFeatureHistory::fromJson(json);
                        LOG(ERROR) << "object read : "<< DeviceFeatureHistory::fromJson(json);
                        allFeatures.push_back(featureHistory);
                }
        }


        //no we are cleaning up the bin by setting it to ""
        auto valueWritten = driver->setValueAndRead("test",
                                                    "testVisitHistory",
                                                    "visithistoryKey",
                                                    "vstBinSt3",
                                                    "",
                                                    10000);

        //now we add nothing but read the value from bean
        std::string valueWrittenAfterCleanUp = driver->addValueAndRead("test",
                                                                       "testVisitHistory",
                                                                       "visithistoryKey",
                                                                       "vstBinSt3",
                                                                       "",
                                                                       //  "responseObject",
                                                                       10000);
        LOG(ERROR) << "valueWrittenAfterCleanUp : "<< valueWrittenAfterCleanUp;
}
