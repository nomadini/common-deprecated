//
// Created by Mahmoud Taabodi on 7/12/16.
//

#include "RecencyModelTest.h"
#include "GUtil.h"

#include "EntityToModuleStateStats.h"
#include <string>
#include <memory>
#include "BeanFactory.h"
#include <aerospike/as_config.h>
#include "AerospikeUtils.h"
#include <thread>
#include <algorithm>
#include <string>
#include <iostream>
#include <cctype>


#include <cmath>
#include <vector>
#include <iostream>
#include <boost/foreach.hpp>


RecencyModelTest::RecencyModelTest() {

}

RecencyModelTest::~RecencyModelTest() {

}

void RecencyModelTest::SetUp() {

}

void RecencyModelTest::TearDown() {

}

TEST_F(RecencyModelTest, testRecencyModelToJson) {
        /*

           {
           "featureScores": [{
             "feature_1": 2.32
           }, {
             "feature_2": 2.32
           }]
           }
         */

        auto model = std::make_shared<RecencyModel>();
        model->featureScoreMap.insert(std::make_pair("abc.com", 12.02f));
        model->featureScoreMap.insert(std::make_pair("bbc.com", 13.04f));


        LOG(ERROR) << "model : " <<model->toJson();

        auto newModel = RecencyModel::fromJson(model->toJson());
        LOG(ERROR) << "newModel : " <<newModel->toJson();

}
