//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef COMMON_HistogramTestS_H
#define COMMON_HistogramTestS_H

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
class EntityToModuleStateStats;
class HistogramTest : public ::testing::Test {

private:

public:
HistogramTest();

virtual ~HistogramTest();

void SetUp();

void testHistogram();

void TearDown();
};

#endif //COMMON_HistogramTestS_H
