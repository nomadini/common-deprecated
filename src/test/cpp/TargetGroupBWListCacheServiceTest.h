// //
// // Created by mtaabodi on 7/13/2016.
// //
//
// #ifndef TargetGroupBWListCacheService_H
// #define TargetGroupBWListCacheService_H
//
//
//
// #include "TargetGroupTypeDefs.h"
// class TargetGroup;
// class MySqlDriver;
// #include "CollectionUtil.h"
// #include <memory>
// #include <string>
// #include <gtest/gtest.h>
// #include "TestsCommon.h"
// #include "TargetGroupTypeDefs.h"
// class TargetGroup;
// class TargetGroupBWListCacheService;
// #include "HttpUtilServiceMock.h"
// #include "MySqlBWListServiceMock.h"
//
// class TargetGroupBWListCacheServiceTest  : public ::testing::Test {
//
// private:
//
// public:
//
//     TargetGroupBWListCacheService* service;
//     std::string allEntitiesInJsonReturned;
//
//     std::string allEntitiesInJsonExpected;
//     std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> allEntitiesExpected;
//
//     HttpUtilServiceMockPtr httpUtilServiceMock;
//     MySqlBWListServiceMockPtr mySqlBWListService;
//     void assertBothMapsAreEqual(std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> actualEntitiesMap,
//                                 std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> expectedEntitiesMap );
//
//     std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> parseDtoReturnedToMap(std::string json);
//
//     void SetUp();
//
//     void TearDown();
//
//     std::shared_ptr<TargetGroupBWListMap> createSampleEntity();
//
//     TargetGroupBWListCacheServiceTest() ;
//
//     void givenHttpServiceReturnsJsonData();
//
//     void whenTargetGroupReloaCachesViaHtppIsIsCalled();
//
//     void thenAllTargetGroupsAreAsExpected();
//
//     std::string createArrayOfTargetGroups(std::shared_ptr<TargetGroupBWListMap> targetGroupExpected);
//
//     void thenBothTargetGroupsAreEqual(std::shared_ptr<TargetGroupBWListMap> actualTargetGroup, std::shared_ptr<TargetGroupBWListMap> expectedTargetGroup);
//
//     void givenDataInCache();
//
//     void whenGetAllAsJsonIsCalled();
//
//     void thenAllEntitiesJosnIsCreatedAsExpected();
//
//     virtual ~TargetGroupBWListCacheServiceTest() ;
//
// };
// #endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
