//
// Created by mtaabodi on 7/13/2016.
//
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "LatLonToPolygonConverterTest.h"
#include "RecencyModelCacheService.h"
#include "EventLog.h"
#include "BeanFactory.h"
#include "CollectionUtil.h"
#include "HttpUtilServiceMock.h"
#include "AdvertiserTestHelper.h"
#include "Advertiser.h"

LatLonToPolygonConverterTest::LatLonToPolygonConverterTest() {

        latLonToPolygonConverter = beanFactory->latLonToPolygonConverter.get();
}

LatLonToPolygonConverterTest::~LatLonToPolygonConverterTest() {

}


void LatLonToPolygonConverterTest::SetUp() {
}

void LatLonToPolygonConverterTest::TearDown() {

}


TEST_F(LatLonToPolygonConverterTest, testReloadingCaches) {

        beanFactory->
        latLonToPolygonConverter->
        circleToPolygon(-27.4575887, -58.99029, 100, 32);
}
