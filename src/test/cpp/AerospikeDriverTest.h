//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef AerospikeDriverTest_H
#define AerospikeDriverTest_H


#include "AerospikeDriver.h"
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
class EntityToModuleStateStats;
class AerospikeDriverTest : public ::testing::Test {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
AerospikeDriverTest();

virtual ~AerospikeDriverTest();

void SetUp();

int testCreatingPolyGonMap();
void testHistogram();

void TearDown();
};

#endif //COMMON_AEROSPIKEDRIVERTESTS_H
