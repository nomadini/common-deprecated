#ifndef GumboHtmlParser_H
#define GumboHtmlParser_H

#include <string>
#include <memory>
#include <vector>
#include "gumbo.h"
/*
   https://github.com/google/gumbo-parser
 */
class GumboHtmlParser;


class GumboHtmlParser {

public:
std::vector<std::string> extractLinks(std::string contents);
std::vector<std::string> extractLinksInner(GumboNode* node);
};

#endif
