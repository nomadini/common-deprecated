
#include <stdlib.h>

#include <fstream>
#include <iostream>
#include <string>

#include "gumbo.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "GumboHtmlParser.h"

std::vector<std::string> GumboHtmlParser::extractLinksInner(GumboNode* node) {
        std::vector<std::string> empty;
        if (node->type != GUMBO_NODE_ELEMENT) {
                return empty;
        }
        GumboAttribute* href = nullptr;
        std::vector<std::string> parentLink;
        if (node->v.element.tag == GUMBO_TAG_A &&
            (href = gumbo_get_attribute(&node->v.element.attributes, "href"))) {
                parentLink = CollectionUtil::convertToList(_toStr(href->value));
        }



        GumboVector* children = &node->v.element.children;
        std::vector<std::string> childrenLinks;
        for (unsigned int i = 0; i < children->length; ++i) {
                childrenLinks = extractLinksInner(static_cast<GumboNode*>(children->data[i]));
        }
        //IMPORTANT we don't need to release memory here , because
        //  gumbo_destroy_output(&kGumboDefaultOptions, output); would do it for us
        return CollectionUtil::combineTwoLists(childrenLinks, parentLink);
}

std::vector<std::string> GumboHtmlParser::extractLinks(std::string contents) {
        GumboOutput* output = gumbo_parse(contents.c_str());
        auto node = output->root;
        auto list = extractLinksInner(node);
        gumbo_destroy_output(&kGumboDefaultOptions, output);
        return list;
}
