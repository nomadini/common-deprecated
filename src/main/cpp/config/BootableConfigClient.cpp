/*
 * BootableConfigClientParser.h
 *
 *  Created on: Mar 4, 2015
 *      Author: mtaabodi
 */


#include "GUtil.h"
#include "BootableConfigClient.h"
#include "StringUtil.h"
#include "FileUtil.h"
#include "JsonUtil.h"
#include "JsonMapUtil.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>
#include "PocoSession.h"
#include "EntityToModuleStateStats.h"

gicapods::BootableConfigClient::BootableConfigClient(
								std::string remoteServerUrl,
								EntityToModuleStateStats* entityToModuleStateStats) {
								this->remoteServerUrl = remoteServerUrl;
								session = std::make_shared<PocoSession>(
																"BootableConfigClient",
																false,//keepAliveMode
																10000,
																10000,
																remoteServerUrl,
																entityToModuleStateStats);


}

gicapods::BootableConfigClient::~BootableConfigClient() {

}

std::shared_ptr<std::unordered_map<std::string, std::string> >
gicapods::BootableConfigClient::readPropertiesFromRemoteServer(
								std::string appName,
								std::string cluster,
								std::string host) {

								auto docResponse = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

								JsonUtil::addMemberToValue_FromPair(docResponse.get(), "appName", appName, value);
								JsonUtil::addMemberToValue_FromPair(docResponse.get(), "cluster", cluster, value);
								JsonUtil::addMemberToValue_FromPair(docResponse.get(), "host", host, value);

								auto request = PocoHttpRequest::createSimplePostRequest(
																remoteServerUrl,
																JsonUtil::valueToString (value));

								std::string responseBody;
								for (int i = 0; i < 5; i++) {
																auto response = session->sendRequest(*request);

																responseBody = response->body;

																if(!responseBody.empty()) {
																								LOG(INFO)<<"response from server : "<<responseBody;
																								auto loadedProps =
																																std::make_shared<std::unordered_map<std::string, std::string> >();
																								JsonMapUtil::readMapFromJsonString(
																																responseBody,
																																*loadedProps);

																								return loadedProps;
																}

																gicapods::Util::sleepViaBoost(_L_, 1);

								}
								EXIT("failed to get properties");

}
