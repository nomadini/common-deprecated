/*
 * BootableConfigClientParser.h
 *
 *  Created on: Mar 4, 2015
 *      Author: mtaabodi
 */

#ifndef BootableConfigClient_H
#define BootableConfigClient_H


class GUtil;
#include "Poco/Util/PropertyFileConfiguration.h"
#include "JsonTypeDefs.h"
#include <memory>
#include <string>
#include <unordered_map>
class PocoSession;
class EntityToModuleStateStats;
namespace gicapods {
class ConfigService;
class BootableConfigClient {

public:
std::shared_ptr<PocoSession> session;
std::string remoteServerUrl;
BootableConfigClient(std::string remoteServerUrl,
                     EntityToModuleStateStats* entityToModuleStateStats);

std::shared_ptr<std::unordered_map<std::string, std::string> > readPropertiesFromRemoteServer(std::string appName,
                                                                                              std::string cluster,
                                                                                              std::string host);

virtual ~BootableConfigClient();
};


}
#endif
