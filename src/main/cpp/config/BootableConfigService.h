/*
 * BootableConfigServiceParser.h
 *
 *  Created on: Mar 4, 2015
 *      Author: mtaabodi
 */

#ifndef BootableConfigService_H
#define BootableConfigService_H


class GUtil;
#include "Poco/Util/PropertyFileConfiguration.h"
#include "JsonTypeDefs.h"
#include <memory>
#include <string>
#include <unordered_map>
namespace gicapods {
class ConfigService;
class BootableConfigService {

private:
void addCustomProperties(
        std::string rawContentOfFile,
        std::shared_ptr<std::unordered_map<std::string, std::string> > allProps,
        std::string cluster,
        std::string host);

void addCommonProperties(
        std::shared_ptr<std::unordered_map<std::string, std::string> > allProps,
        std::string commonPropertyFileName);

void addGeneralProperties(
        std::shared_ptr<std::unordered_map<std::string, std::string> > allProps,
        std::string appName);
public:
//this class has its own instance of configService
std::unique_ptr<ConfigService> configService;
std::string commonPropertyFileName;
BootableConfigService(std::string commonPropertyFileName);

std::shared_ptr<std::unordered_map<std::string, std::string> > loadProperties(std::string appName,
                                                                              std::string cluster,
                                                                              std::string host);

void addPropertiesFrom(
        DocumentPtr doc,
        std::string propertyCategory,
        std::shared_ptr<std::unordered_map<std::string, std::string> > allGeneralProps,
        std::string propertyCategoryValue);

virtual ~BootableConfigService();
};


}
#endif
