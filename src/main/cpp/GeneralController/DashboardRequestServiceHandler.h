#ifndef DashboardRequestServiceHandler_H
#define DashboardRequestServiceHandler_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
namespace gicapods {
class ConfigService;
}

class DashboardRequestServiceHandler;



class DashboardRequestServiceHandler : public Poco::Net::HTTPRequestHandler {

public:

gicapods::ConfigService* configService;

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

DashboardRequestServiceHandler(gicapods::ConfigService* configService);

virtual ~DashboardRequestServiceHandler();
};

#endif
