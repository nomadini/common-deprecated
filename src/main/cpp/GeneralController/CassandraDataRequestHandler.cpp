
#include "CassandraDataRequestHandler.h"
#include "JsonUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "StringUtil.h"
#include "EntityToModuleStateStats.h"
#include "JsonArrayUtil.h"
#include "HttpUtilService.h"
#include "CassandraManagedType.h"
#include "DeviceSegmentHistory.h"
#include "EventLog.h"

#include "FeatureDeviceHistory.h"
#include "DeviceHistory.h"
#include "Feature.h"
#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"
#include "PixelDeviceHistory.h"
#include "GicapodsIdToExchangeIdsMap.h"
#include "AdHistory.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include "DeviceHistory.h"
#include "PixelDeviceHistory.h"
#include "Wiretap.h"
#include "HttpUtilService.h"
#include "DateTimeMacro.h"
CassandraDataRequestHandler::CassandraDataRequestHandler() {
}

void CassandraDataRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                                Poco::Net::HTTPServerResponse &response) {

        try {

                Poco::Timestamp now;
                LOG_EVERY_N(INFO, 1000)<< google::COUNTER<<"nth request.getURI () : "<<request.getURI ();
                auto requestBody = httpUtilService->getRequestBody(request);
                std::string responseBody;
                if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/")) {
                        responseBody = getValueFromCassandra(request, requestBody);
                } else if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/")) {
                        responseBody = writeValueWithCassandra(request, requestBody);
                }
                Poco::Timestamp::TimeDiff diff = now.elapsed (); // how long did it take?
                LOG_EVERY_N(INFO, 1000) << google::COUNTER<<"nth CassandraDataRequestHandler Latency  " << diff / 1000 << " milliseconds";
                LOG_EVERY_N(INFO, 100) << google::COUNTER<<"nth CassandraDataRequestHandler response  "
                                       << responseBody;
                std::ostream &ostr = response.send ();
                ostr << responseBody;
                return;
        } catch(...) {
                entityToModuleStateStats->addStateModuleForEntity ("#OfUnknownExceptionsInHandlingRequest",
                                                                   "CassandraDataRequestHandler",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);

                gicapods::Util::showStackTrace();
        }

        std::ostream &ostr = response.send ();
        ostr << "";
}

template<class T>
std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >
CassandraDataRequestHandler::getArraysOfObjectsFromRequest(std::string requestBody) {
        auto objects = JsonArrayUtil::convertStringArrayToArrayOfObject<T>(requestBody);
        std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > > generalObjects
                = std::make_shared<std::vector<std::shared_ptr<CassandraManagedType> > >();
        for (auto object : *objects) {
                generalObjects->push_back(std::static_pointer_cast<CassandraManagedType>(object));
        }
        return generalObjects;
}

std::string CassandraDataRequestHandler::writeValueWithCassandra(Poco::Net::HTTPServerRequest &request,
                                                                 std::string requestBody) {
        std::string responseBody;
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/DeviceFeatureHistory/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<DeviceFeatureHistory>(requestBody);
                cassandraServiceDeviceFeatureHistory->writeBatchData(objects);
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/FeatureDeviceHistory/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<FeatureDeviceHistory>(requestBody);
                cassandraServiceFeatureDeviceHistory->writeBatchData(objects);
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/DeviceSegmentHistory/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<DeviceSegmentHistory>(requestBody);
                cassandraServiceDeviceSegmentHistory->writeBatchData(objects);
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/IpToDeviceIdsMap/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<IpToDeviceIdsMap>(requestBody);
                cassandraServiceIpToDeviceIdsMap->writeBatchData(objects);
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/DeviceIdToIpsMap/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<DeviceIdToIpsMap>(requestBody);
                cassandraServiceDeviceIdToIpsMap->writeBatchData(objects);
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/GicapodsIdToExchangeIdsMap/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<GicapodsIdToExchangeIdsMap>(requestBody);
                cassandraServiceGicapodsIdToExchangeIdsMap->writeBatchData(objects);
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/AdHistory/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<AdHistory>(requestBody);
                cassandraServiceAdHistory->writeBatchData(objects);
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/PixelDeviceHistory/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<PixelDeviceHistory>(requestBody);
                cassandraServicePixelDeviceHistory->writeBatchData(objects);
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/EventLog/")) {
                auto objects = CassandraDataRequestHandler::getArraysOfObjectsFromRequest<EventLog>(requestBody);
                cassandraServiceEventLog->writeBatchData(objects);
        }  else {
                entityToModuleStateStats->addStateModuleForEntity(
                        "UNKNOWN_REQUEST_TYPE:" +  request.getURI (),
                        "CassandraDataRequestHandler",
                        "ALL",
                        EntityToModuleStateStats::exception);
                throwEx("unknwon request type : " + request.getURI ());
        }

        return responseBody;
}
std::string CassandraDataRequestHandler::getValueFromCassandra(Poco::Net::HTTPServerRequest &request,
                                                               std::string requestBody) {
        std::string responseBody;
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/DeviceFeatureHistory/")) {
                auto deviceFeatureHistory = DeviceFeatureHistory::fromJson(requestBody);
                auto responseFromCassandra = cassandraServiceDeviceFeatureHistory->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else

        if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/FeatureDeviceHistory/")) {
                auto deviceFeatureHistory = FeatureDeviceHistory::fromJson(requestBody);
                auto responseFromCassandra = cassandraServiceFeatureDeviceHistory->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/DeviceSegmentHistory/")) {
                auto deviceFeatureHistory = DeviceSegmentHistory::fromJson(requestBody);
                auto responseFromCassandra = cassandraServiceDeviceSegmentHistory->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/IpToDeviceIdsMap/")) {
                auto deviceFeatureHistory = IpToDeviceIdsMap::fromJson(requestBody);
                auto responseFromCassandra = cassandraServiceIpToDeviceIdsMap->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else

        if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/DeviceIdToIpsMap/")) {
                auto deviceFeatureHistory = DeviceIdToIpsMap::fromJson(requestBody);
                auto responseFromCassandra = cassandraServiceDeviceIdToIpsMap->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/GicapodsIdToExchangeIdsMap/")) {
                auto deviceFeatureHistory = GicapodsIdToExchangeIdsMap::fromJson(requestBody);
                auto responseFromCassandra = cassandraServiceGicapodsIdToExchangeIdsMap->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/AdHistory/")) {
                auto deviceFeatureHistory = AdHistory::fromJson(requestBody);
                auto responseFromCassandra = cassandraServiceAdHistory->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/PixelDeviceHistory/")) {
                auto deviceFeatureHistory = PixelDeviceHistory::fromJson(requestBody);
                auto responseFromCassandra = cassandraServicePixelDeviceHistory->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else if (StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/EventLog/")) {
                auto deviceFeatureHistory = EventLog::fromJson(requestBody);
                auto responseFromCassandra = cassandraServiceEventLog->cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(deviceFeatureHistory);
                if(responseFromCassandra != nullptr) {
                        responseBody = responseFromCassandra->toJson();
                }
        } else {
                entityToModuleStateStats->addStateModuleForEntity(
                        "UNKNOWN_REQUEST_TYPE:" +  request.getURI (),
                        "CassandraDataRequestHandler",
                        "ALL",
                        EntityToModuleStateStats::exception);

                throwEx("unknwon request type : " + request.getURI ());
        }


        return responseBody;
}
