#ifndef CassandraDataRequestHandler_H
#define CassandraDataRequestHandler_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
class EntityToModuleStateStats;
class HttpUtilService;
class CassandraManagedType;
#include "CassandraService.h"

#include "DeviceSegmentHistory.h"
#include "FeatureDeviceHistory.h"
#include "DeviceHistory.h"
#include "Feature.h"
#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"
#include "PixelDeviceHistory.h"
#include "GicapodsIdToExchangeIdsMap.h"
#include "AdHistory.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include "DeviceHistory.h"
#include "PixelDeviceHistory.h"
#include "Wiretap.h"
#include "DateTimeMacro.h"
#include "EventLog.h"
class CassandraDataRequestHandler;


class CassandraDataRequestHandler : public Poco::Net::HTTPRequestHandler {

public:

CassandraService<DeviceFeatureHistory>* cassandraServiceDeviceFeatureHistory;
CassandraService<FeatureDeviceHistory>* cassandraServiceFeatureDeviceHistory;

CassandraService<DeviceSegmentHistory>* cassandraServiceDeviceSegmentHistory;
CassandraService<IpToDeviceIdsMap>* cassandraServiceIpToDeviceIdsMap;
CassandraService<DeviceIdToIpsMap>* cassandraServiceDeviceIdToIpsMap;
CassandraService<GicapodsIdToExchangeIdsMap>* cassandraServiceGicapodsIdToExchangeIdsMap;
CassandraService<AdHistory>* cassandraServiceAdHistory;
CassandraService<EventLog>* cassandraServiceEventLog;
CassandraService<PixelDeviceHistory>* cassandraServicePixelDeviceHistory;

HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;

CassandraDataRequestHandler();
std::string getValueFromCassandra(
								Poco::Net::HTTPServerRequest &request,
								std::string responseBody);
std::string writeValueWithCassandra(
								Poco::Net::HTTPServerRequest &request,
								std::string responseBody);
void handleRequest(
								Poco::Net::HTTPServerRequest& request,
								Poco::Net::HTTPServerResponse& response);

template<class T>
static std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >
getArraysOfObjectsFromRequest(std::string requestBody);
};

#endif
