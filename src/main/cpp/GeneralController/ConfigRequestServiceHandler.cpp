
#include "ConfigRequestServiceHandler.h"
#include "JsonUtil.h"
#include <boost/exception/all.hpp>

#include "StringUtil.h"
#include "HttpUtil.h"
#include "JsonMapUtil.h"
#include "ConfigService.h"
#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "BootableConfigService.h"
#include "GUtil.h"


ConfigRequestServiceHandler::ConfigRequestServiceHandler(
        gicapods::ConfigService* configService,
        gicapods::BootableConfigService* bootableConfigService) {
        this->configService = configService;
        this->bootableConfigService = bootableConfigService;
}


void ConfigRequestServiceHandler::handleRequest(
        Poco::Net::HTTPServerRequest& request,
        Poco::Net::HTTPServerResponse& response) {
        std::ostream& ostr = response.send();
        try {
                MLOG(3) << "request.getURI() : " << request.getURI ();
                auto queryParams  = HttpUtil::getMapOfQueryParams(request);
                std::string functionName = HttpUtil::getRequiredParamFromQueryParameters("function", queryParams);

                if(StringUtil::equalsIgnoreCase(functionName, "get")) {

                        std::string proeprtyName = HttpUtil::getRequiredParamFromQueryParameters("name", queryParams);
                        std::string properyValue = configService->get(proeprtyName);
                        MLOG(3) << "properyValue : " << properyValue;
                        ostr << properyValue;
                        return;
                } else if(StringUtil::equalsIgnoreCase(functionName, "getAllLoadedProperties")) {
                        auto allProperties = configService->getAllLoadedProperties();
                        auto allPropertiesAsJson = JsonMapUtil::convertMapToJsonArray(*allProperties);
                        MLOG(3) << "allPropertiesAsJson : " << allPropertiesAsJson;
                        ostr << allPropertiesAsJson;
                        return;
                } else if(StringUtil::equalsIgnoreCase(functionName, "addProperty")) {
                        std::string proeprtyName = HttpUtil::getRequiredParamFromQueryParameters("name", queryParams);
                        std::string proeprtyValue = HttpUtil::getRequiredParamFromQueryParameters("value", queryParams);
                        configService->upsertProperty(proeprtyName, proeprtyValue);
                        ostr << "property " <<proeprtyName << " was added with value "<< proeprtyValue;
                        return;
                } else if(StringUtil::equalsIgnoreCase(functionName, "giveMePropertiesToBootUp")) {
                        std::string appName = HttpUtil::getRequiredParamFromQueryParameters("appName", queryParams);
                        std::string cluster = HttpUtil::getRequiredParamFromQueryParameters("cluster", queryParams);
                        std::string host = HttpUtil::getRequiredParamFromQueryParameters("host", queryParams);
                        auto bootableProps = bootableConfigService->loadProperties(appName, cluster, host);
                        auto propsAsJson = JsonMapUtil::convertMapToJsonArray(*bootableProps);
                        LOG(ERROR)
                                <<" appName : "<< appName
                                <<" cluster : "<< cluster
                                <<" host : "<< host;


                        LOG(ERROR) << "loading these props : "<< propsAsJson;
                        ostr << propsAsJson;
                        return;
                }


        } catch (std::exception const&  e) {
                LOG(ERROR) <<"error happening when handling request  " << boost::diagnostic_information(e);
        }

        catch (...) {
                LOG(ERROR)<<"unknown error happening when handling request";
        }
        ostr << "ERROR in PROCESSING";
        return;
}

ConfigRequestServiceHandler::~ConfigRequestServiceHandler() {
}
