#ifndef CommonRequestHandlerFactory_H
#define CommonRequestHandlerFactory_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"


#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
class EntityToModuleStateStats;
class CommonRequestHandlerFactory;
class DataMasterRequestHandlerFactory;
class EntityDeliveryInfoCacheService;
class MySqlMetricService;

namespace gicapods {
class ConfigService;
class BootableConfigService;
}



class CommonRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {



public:
EntityToModuleStateStats* entityToModuleStateStats;
DataMasterRequestHandlerFactory* dataMasterRequestHandlerFactory;
gicapods::ConfigService* configService;
gicapods::BootableConfigService* bootableConfigService;

CommonRequestHandlerFactory();

static std::shared_ptr<CommonRequestHandlerFactory> shared_instance(
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::ConfigService* configService,
        gicapods::BootableConfigService* bootableConfigService
        );

Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);

Poco::Net::HTTPRequestHandler* evalueteDataForceReloadHanlder(const Poco::Net::HTTPServerRequest &request);
Poco::Net::HTTPRequestHandler* evalueteConfigServiceHanlder(const Poco::Net::HTTPServerRequest &request);
Poco::Net::HTTPRequestHandler* evalueteDashboardServiceHanlder(const Poco::Net::HTTPServerRequest &request);
Poco::Net::HTTPRequestHandler* evalueteEntityToModuleStateStatsHandler(const Poco::Net::HTTPServerRequest &request);
Poco::Net::HTTPRequestHandler* evalueteDataMasterRequestHandler(const Poco::Net::HTTPServerRequest &request);
Poco::Net::HTTPRequestHandler* evalueteLogLevelManagerRequestHandler(const Poco::Net::HTTPServerRequest &request);
};

#endif
