#ifndef EntityDeliveryInfoCacheServiceRequestHandler_H
#define EntityDeliveryInfoCacheServiceRequestHandler_H

#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
class TargetGroupCacheService;
class EntityToModuleStateStats;

class EntityDeliveryInfoCacheServiceRequestHandler;
class EntityDeliveryInfoCacheService;

class EntityDeliveryInfoCacheServiceRequestHandler : public Poco::Net::HTTPRequestHandler {

public:
EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;
void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

EntityDeliveryInfoCacheServiceRequestHandler();

virtual ~EntityDeliveryInfoCacheServiceRequestHandler();
};

#endif
