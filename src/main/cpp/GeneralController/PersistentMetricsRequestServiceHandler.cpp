
#include "PersistentMetricsRequestServiceHandler.h"
#include "JsonUtil.h"
#include <boost/exception/all.hpp>

#include "StringUtil.h"
#include "HttpUtil.h"
#include "JsonMapUtil.h"
#include "ConverterUtil.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "ConfigService.h"
#include "CollectionUtil.h"
#include "JsonArrayUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "NetworkUtil.h"
#include "MySqlTargetGroupFilterCountDbRecordService.h"
#include "MySqlMetricService.h"
#include "MetricDbRecord.h"
#include "DateTimeUtil.h"
#include "ConverterUtil.h"
#include "EntityToModuleStateStats.h"
#include "MySqlTgBiddingPerformanceMetricDtoService.h"


PersistentMetricsRequestServiceHandler::PersistentMetricsRequestServiceHandler(
        MySqlMetricService* mySqlMetricService,
        MySqlTargetGroupFilterCountDbRecordService*  mySqlTargetGroupFilterCountDbRecordService,
        MySqlTgBiddingPerformanceMetricDtoService*  mySqlTgBiddingPerformanceMetricDtoService) {
        this->mySqlMetricService = mySqlMetricService;
        this->mySqlTargetGroupFilterCountDbRecordService = mySqlTargetGroupFilterCountDbRecordService;
        this->mySqlTgBiddingPerformanceMetricDtoService = mySqlTgBiddingPerformanceMetricDtoService;
}


void PersistentMetricsRequestServiceHandler::handleRequest(
        Poco::Net::HTTPServerRequest& request,
        Poco::Net::HTTPServerResponse& response) {
        std::ostream& ostr = response.send();

        MLOG(3) << "request.getURI() : " << request.getURI ();
        auto queryParams  = HttpUtil::getMapOfQueryParams(request);
        std::string functionName =
                HttpUtil::getRequiredParamFromQueryParameters("function", queryParams);
        MLOG(3) << "functionName : " << functionName;
        std::string responseStr;
        if (StringUtil::equalsIgnoreCase(
                    functionName,
                    "readMetricByModuleAndHostInLastNMinutes")) {
                responseStr = processModuleMetricDetails(queryParams);
        } else if (StringUtil::equalsIgnoreCase(
                           functionName,
                           "FilterCountReadAllByHostInLastNMinutes")) {
                responseStr = processFilterCountDetails(queryParams);
        } else if (StringUtil::equalsIgnoreCase(
                           functionName,
                           "FilterCountReadAllByHostInLastNMinutesWithPercentagesAbove")) {
                responseStr = processFilterCountReadAllByHostInLastNMinutesWithPercentagesAbove(queryParams);
        }

        else if (StringUtil::equalsIgnoreCase(
                         functionName,
                         "MetricsReadAllImportantOnes")) {
                responseStr = processImportantMetricsDetails(queryParams);
        }  else if (StringUtil::equalsIgnoreCase(
                            functionName,
                            "MetricsReadAllExceptionOnes")) {
                responseStr = processExceptionAndErrorMetricsDetails(queryParams);
        }  else if (StringUtil::equalsIgnoreCase(
                            functionName,
                            "readBidAndConfirmedWinsDetails")) {
                responseStr = readBidAndConfirmedWinsDetails(queryParams);
        }  else if (StringUtil::equalsIgnoreCase(
                            functionName,
                            "readAllMetricsByHost")) {
                responseStr = readAllMetricsByHost(queryParams);
        }
        MLOG(3)<<"responseStr : "<< responseStr;
        ostr << responseStr;
}

std::string PersistentMetricsRequestServiceHandler::
processImportantMetricsDetails(std::unordered_map<std::string, std::string> queryParams) {
        std::string lastNminuteStr =
                HttpUtil::getRequiredParamFromQueryParameters("lastNminute", queryParams);


        std::vector<std::shared_ptr<MetricDbRecord> > metrics =
                mySqlMetricService->readMetricsInLastNMinutes(
                        ConverterUtil::convertTo<int>(lastNminuteStr),
                        NetworkUtil::getHostName(),
                        "",
                        "",
                        EntityToModuleStateStats::important);
        std::string metricsJson = JsonArrayUtil::convertListToJson(metrics);
        MLOG(3)<<" lastNminuteStr :" << lastNminuteStr
               <<" metrics size :" << metrics.size()
               <<" hostname :" << NetworkUtil::getHostName()
               <<" metricsJson :" << metricsJson;

        return metricsJson;
}

std::string PersistentMetricsRequestServiceHandler::
readAllMetricsByHost(std::unordered_map<std::string, std::string> queryParams) {
        std::string lastNminuteStr =
                HttpUtil::getRequiredParamFromQueryParameters("lastNminute", queryParams);


        std::vector<std::shared_ptr<MetricDbRecord> > records =
                mySqlMetricService->readMetricsInLastNMinutes(
                        ConverterUtil::convertTo<int>(lastNminuteStr),
                        NetworkUtil::getHostName(),
                        "",
                        "",
                        "");

        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef parentValue(rapidjson::kObjectType);

        JsonArrayUtil::addMemberToValue_FromPair(
                doc.get(),
                "data",
                records,
                parentValue);
        std::string metricsJson =  JsonUtil::valueToString (parentValue);

        MLOG(3)<<" lastNminuteStr :" << lastNminuteStr
               <<" records size :" << records.size()
               <<" hostname :" << NetworkUtil::getHostName()
               <<" metricsJson :" << metricsJson;

        return metricsJson;
}

std::string PersistentMetricsRequestServiceHandler::
readBidAndConfirmedWinsDetails(std::unordered_map<std::string, std::string> queryParams) {
        std::string lastNminuteStr =
                HttpUtil::getRequiredParamFromQueryParameters("lastNminute", queryParams);
        NetworkUtil::getHostName();

        std::vector<std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder> >
        allBindWinDetails = mySqlTgBiddingPerformanceMetricDtoService->readAllDeliveryInfoPastDate(
                NetworkUtil::getHostName(),
                DateTimeUtil::getDateWithMinutePercision(ConverterUtil::convertTo<int>(lastNminuteStr) * -1)
                );

        std::string allBindWinDetailsJson = JsonArrayUtil::convertListToJson(allBindWinDetails);
        MLOG(3)<<" lastNminuteStr :" << lastNminuteStr
               <<" allBindWinDetails size :" << allBindWinDetails.size()
               <<" hostname :" << NetworkUtil::getHostName()
               <<" allBindWinDetailsJson :" << allBindWinDetailsJson;

        return allBindWinDetailsJson;
}

std::string PersistentMetricsRequestServiceHandler::
processExceptionAndErrorMetricsDetails(std::unordered_map<std::string, std::string> queryParams) {

        std::string lastNminuteStr =
                HttpUtil::getRequiredParamFromQueryParameters("lastNminute", queryParams);
        // std::string entity =
        //         HttpUtil::getRequiredParamFromQueryParameters("entity", queryParams);
        // std::string state =
        //         HttpUtil::getRequiredParamFromQueryParameters("state", queryParams);
        // std::string module =
        //         HttpUtil::getRequiredParamFromQueryParameters("module", queryParams);
        // std::string appName =
        //         HttpUtil::getRequiredParamFromQueryParameters("appName", queryParams);
        // std::string version =
        //         HttpUtil::getRequiredParamFromQueryParameters("version", queryParams);
        // std::string level =
        //         HttpUtil::getRequiredParamFromQueryParameters("level", queryParams);

        std::string entity, state, module;
        std::vector<std::shared_ptr<MetricDbRecord> > allExceptionMetrics =
                mySqlMetricService->readAllMetricsInLastNMinutesWithLevel(
                        ConverterUtil::convertTo<int>(lastNminuteStr),
                        NetworkUtil::getHostName(),
                        "", //we want to get any value for appName here
                        "",//we want to get any value for version here
                        entity,
                        state,
                        module,
                        EntityToModuleStateStats::exception);

        std::vector<std::shared_ptr<MetricDbRecord> > allErrorMetrics =
                mySqlMetricService->readAllMetricsInLastNMinutesWithLevel(
                        ConverterUtil::convertTo<int>(lastNminuteStr),
                        NetworkUtil::getHostName(),
                        "", //we want to get any value for appName here
                        "",//we want to get any value for version here
                        entity,
                        state,
                        module,
                        EntityToModuleStateStats::error);

        allExceptionMetrics = CollectionUtil::combineTwoLists(allExceptionMetrics,
                                                              allErrorMetrics);


        std::string metricsJson = JsonArrayUtil::convertListToJson(allExceptionMetrics);
        MLOG(3)<<" lastNminuteStr :" << lastNminuteStr
               <<" allExceptionMetrics size :" << allExceptionMetrics.size()
               <<" hostname :" << NetworkUtil::getHostName()
               <<" metricsJson :" << metricsJson;

        return metricsJson;
}

std::string PersistentMetricsRequestServiceHandler::processFilterCountDetails(
        std::unordered_map<std::string, std::string> queryParams) {
        std::string lastNminuteStr =
                HttpUtil::getRequiredParamFromQueryParameters("lastNminute", queryParams);
        std::string metricsJson = "NO";
        try {
                int minutes = ConverterUtil::convertTo<int>(lastNminuteStr);
                std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> > records =
                        mySqlTargetGroupFilterCountDbRecordService->
                        readAllByHostInLastNMinutes(
                                NetworkUtil::getHostName(),
                                minutes);

                auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
                RapidJsonValueTypeNoRef parentValue(rapidjson::kObjectType);

                JsonArrayUtil::addMemberToValue_FromPair(
                        doc.get(),
                        "data",
                        records,
                        parentValue);
                metricsJson =  JsonUtil::valueToString (parentValue);


                MLOG(3)<<" lastNminuteStr :" << lastNminuteStr
                       <<" metrics size :" << records.size()
                       <<" hostname :" << NetworkUtil::getHostName()
                       <<" metricsJson :" << metricsJson;
        } catch (std::exception& e) {
                gicapods::Util::showStackTrace(&e);
                LOG(ERROR) << "unknown exception";
        } catch (...) {
                LOG(ERROR) << "unknown exception";
        }
        return metricsJson;
}

std::string PersistentMetricsRequestServiceHandler::processFilterCountReadAllByHostInLastNMinutesWithPercentagesAbove(
        std::unordered_map<std::string, std::string> queryParams) {
        std::string lastNminuteStr =
                HttpUtil::getRequiredParamFromQueryParameters("lastNminute", queryParams);

        std::string lowestPercentageStr =
                HttpUtil::getRequiredParamFromQueryParameters("lowestPercentage", queryParams);
        std::string metricsJson = "NO";
        try {
                int minutes = ConverterUtil::convertTo<int>(lastNminuteStr);

                std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> > rawRecords =
                        mySqlTargetGroupFilterCountDbRecordService->
                        readAllByHostInLastNMinutes(
                                NetworkUtil::getHostName(),
                                minutes);

                std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> > records;
                int lowestPercentage = ConverterUtil::convertTo<int>(lowestPercentageStr);

                for (auto record : rawRecords) {
                        if (record->percentageOfFailures->getValue() > lowestPercentage) {
                                records.push_back(record);
                        }
                }
                auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
                RapidJsonValueTypeNoRef parentValue(rapidjson::kObjectType);

                JsonArrayUtil::addMemberToValue_FromPair(
                        doc.get(),
                        "data",
                        records,
                        parentValue);
                metricsJson =  JsonUtil::valueToString (parentValue);


                MLOG(3)<<" lastNminuteStr :" << lastNminuteStr
                       <<" metrics size :" << records.size()
                       <<" hostname :" << NetworkUtil::getHostName()
                       <<" metricsJson :" << metricsJson;
        } catch (std::exception& e) {
                gicapods::Util::showStackTrace(&e);
                LOG(ERROR) << "unknown exception";
        } catch (...) {
                LOG(ERROR) << "unknown exception";
        }
        return metricsJson;
}

std::string PersistentMetricsRequestServiceHandler::
processModuleMetricDetails(std::unordered_map<std::string, std::string> queryParams) {
        std::string module =
                HttpUtil::getRequiredParamFromQueryParameters("module", queryParams);
        std::string lastNminuteStr =
                HttpUtil::getRequiredParamFromQueryParameters("lastNminute", queryParams);
        auto metrics = mySqlMetricService->
                       readMetricByModuleAndHostInLastNMinutes(
                module,
                NetworkUtil::getHostName(),
                ConverterUtil::convertTo<int>(lastNminuteStr));
        std::string metricsJson = JsonArrayUtil::convertListToJson(metrics);
        MLOG(3)<<" module :" << module
               <<" lastNminuteStr :" << lastNminuteStr
               <<" metrics size :" << metrics.size()
               <<" hostname :" << NetworkUtil::getHostName()
               <<" metricsJson :" << metricsJson;

        return metricsJson;
}

PersistentMetricsRequestServiceHandler::~PersistentMetricsRequestServiceHandler() {
}
