



#include "StringUtil.h"
#include "GUtil.h"
#include "ConverterUtil.h"
#include "UnknownRequestHandler.h"
#include "CommonRequestHandlerFactory.h"
#include "EntityToModuleStateStatsHandler.h"
#include "EntityToModuleStateStats.h"
#include "LogLevelManagerRequestHandler.h"
#include "ForceReloadCachesHandler.h"
#include "DashboardRequestServiceHandler.h"
#include "ConfigRequestServiceHandler.h"
#include "DataClient.h"
#include "ConfigService.h"
#include "BootableConfigService.h"
#include "DoNothingRequestHandler.h"
#include "CassandraDataRequestHandler.h"
#include "DataMasterRequestHandlerFactory.h"
#include "DataMasterRequestHandlerFactory.h"
#include "LogLevelManagerRequestHandler.h"


CommonRequestHandlerFactory::CommonRequestHandlerFactory() {
}

Poco::Net::HTTPRequestHandler* CommonRequestHandlerFactory::createRequestHandler(
        const Poco::Net::HTTPServerRequest &request) {
        try {

                MLOG(3) << "request.getURI() : " << request.getURI ();
                Poco::Net::HTTPRequestHandler* handler;
                handler = evalueteDataForceReloadHanlder(request);
                if(handler != nullptr) { return handler; }
                handler = evalueteConfigServiceHanlder(request);
                if(handler != nullptr) { return handler; }
                handler = evalueteDashboardServiceHanlder(request);
                if(handler != nullptr) { return handler; }
                handler = evalueteEntityToModuleStateStatsHandler(request);
                if(handler != nullptr) { return handler; }
                handler = evalueteDataMasterRequestHandler(request);
                if(handler != nullptr) { return handler; }



                if (StringUtil::containsCaseInSensitive (request.getURI (), "/favicon.ico")) {
                        return new DoNothingRequestHandler(entityToModuleStateStats);
                }

        } catch (std::exception const &e) {
                LOG(ERROR) << "unknown error happening when handling request";
        }
        MLOG(3) << "found no handler for request with uri : " << request.getURI ();
        return new UnknownRequestHandler(entityToModuleStateStats);
}


Poco::Net::HTTPRequestHandler*
CommonRequestHandlerFactory::evalueteConfigServiceHanlder(const Poco::Net::HTTPServerRequest &request) {
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/configservice/") ||
            StringUtil::endsWith(request.getURI (), "/configservice")) {
                auto configRequestServiceHandler = new ConfigRequestServiceHandler(configService, bootableConfigService);
                return configRequestServiceHandler;
        }

        return nullptr;
}

Poco::Net::HTTPRequestHandler*
CommonRequestHandlerFactory::evalueteDashboardServiceHanlder(const Poco::Net::HTTPServerRequest &request) {
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/dashboard/") ||
            StringUtil::endsWith(request.getURI (), "/dashboard")) {
                auto dashboardRequestServiceHandler = new DashboardRequestServiceHandler(configService);
                return dashboardRequestServiceHandler;
        }

        return nullptr;

}

Poco::Net::HTTPRequestHandler*
CommonRequestHandlerFactory::evalueteEntityToModuleStateStatsHandler(
        const Poco::Net::HTTPServerRequest &request) {
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/entityToModuleStateStats/") ||
            StringUtil::endsWith(request.getURI (), "/entityToModuleStateStats")) {
                return new EntityToModuleStateStatsHandler(entityToModuleStateStats);

        }

        return nullptr;

}


Poco::Net::HTTPRequestHandler*
CommonRequestHandlerFactory::evalueteDataForceReloadHanlder(const Poco::Net::HTTPServerRequest &request) {
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/forcereloadcaches/") ||
            StringUtil::endsWith(request.getURI (), "/forcereloadcaches")) {
                throwEx("feature is not supported anymore");
                auto forceReloadCachesHandler = new ForceReloadCachesHandler();
                forceReloadCachesHandler->entityToModuleStateStats = entityToModuleStateStats;
                // forceReloadCachesHandler->dataClient = dataClient;
                return forceReloadCachesHandler;
        }

        return nullptr;

}


Poco::Net::HTTPRequestHandler*
CommonRequestHandlerFactory::evalueteDataMasterRequestHandler(const Poco::Net::HTTPServerRequest &request) {
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/data/api") ||
            StringUtil::endsWith(request.getURI (), "/data/api")) {

                auto requestHandler = dataMasterRequestHandlerFactory->createRequestHandler(request);
                auto unknown = dynamic_cast<UnknownRequestHandler *>(requestHandler);
                if( unknown == nullptr) {
                        return requestHandler;
                }
                //we got an UnknownRequestHandler so we return null

        }

        return nullptr;
}

Poco::Net::HTTPRequestHandler*
CommonRequestHandlerFactory::evalueteLogLevelManagerRequestHandler(
        const Poco::Net::HTTPServerRequest &request) {
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/loglevelmanager") ||
            StringUtil::endsWith(request.getURI (), "/loglevelmanager")) {
                return new LogLevelManagerRequestHandler();
        }

        return nullptr;
}


std::shared_ptr<CommonRequestHandlerFactory>
CommonRequestHandlerFactory::shared_instance(
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::ConfigService* configService,
        gicapods::BootableConfigService* bootableConfigService) {

        static auto instance = std::make_shared<CommonRequestHandlerFactory>();

        instance->entityToModuleStateStats = entityToModuleStateStats;
        instance->configService = configService;
        instance->bootableConfigService = bootableConfigService;

        return instance;
}
