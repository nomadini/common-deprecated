#ifndef ForceReloadCachesHandler_H
#define ForceReloadCachesHandler_H

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"

#include "SignalHandler.h"
class EntityToModuleStateStats;
#include "DataClient.h"
class ForceReloadCachesHandler;


class ForceReloadCachesHandler : public Poco::Net::HTTPRequestHandler {

public:
EntityToModuleStateStats* entityToModuleStateStats;
DataClient* dataClient;
ForceReloadCachesHandler();


void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response);
};
#endif
