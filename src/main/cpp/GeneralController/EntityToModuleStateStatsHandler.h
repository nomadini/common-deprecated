#ifndef EntityToModuleStateStatsHandler_H
#define EntityToModuleStateStatsHandler_H

#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
class TargetGroupCacheService;
class EntityToModuleStateStats;

class EntityToModuleStateStatsHandler;

class EntityToModuleStateStatsHandler: public Poco::Net::HTTPRequestHandler {

public:

   EntityToModuleStateStats* entityToModuleStateStats;

   void handleRequest(Poco::Net::HTTPServerRequest& request,
	                   Poco::Net::HTTPServerResponse& response);

    EntityToModuleStateStatsHandler(EntityToModuleStateStats* entityToModuleStateStats);

	virtual ~EntityToModuleStateStatsHandler() ;
};

#endif
