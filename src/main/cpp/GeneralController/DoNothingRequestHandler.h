#ifndef DoNothingRequestHandler_H
#define DoNothingRequestHandler_H

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"

#include "SignalHandler.h"
class EntityToModuleStateStats;


class DoNothingRequestHandler : public Poco::Net::HTTPRequestHandler {

public:
EntityToModuleStateStats* entityToModuleStateStats;
DoNothingRequestHandler(EntityToModuleStateStats* entityToModuleStateStats);


void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response);
};
#endif
