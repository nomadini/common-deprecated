
#include "EntityDeliveryInfoCacheServiceRequestHandler.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include <boost/exception/all.hpp>

#include "StringUtil.h"
#include "HttpUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "EntityDeliveryInfoCacheService.h"
#include "JsonMapUtil.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "ConcurrentHashMap.h"

EntityDeliveryInfoCacheServiceRequestHandler::EntityDeliveryInfoCacheServiceRequestHandler() {
}


void EntityDeliveryInfoCacheServiceRequestHandler::handleRequest(
        Poco::Net::HTTPServerRequest& request,
        Poco::Net::HTTPServerResponse& response) {
        std::ostream& ostr = response.send();
        try {

                auto requestBody = HttpUtil::getRequestBody(request);
                auto doc = parseJsonSafely(requestBody);
                std::string entityName = StringUtil::toLowerCase(JsonUtil::GetStringSafely(*doc,"entity"));
                if(StringUtil::equalsIgnoreCase(entityName, "campaign")) {
                        auto campaignJsonString = JsonMapUtil::convertMapValuesToJsonArrayString(
                                *entityDeliveryInfoCacheService->getAllCampaignRealTimeDeliveryInfoMap ());
                        ostr << campaignJsonString;
                        return;
                }

                if(StringUtil::equalsIgnoreCase(entityName, "targetgroup")) {
                        auto tgJsonString = JsonMapUtil::convertMapValuesToJsonArrayString(
                                *entityDeliveryInfoCacheService->getAllTgRealTimeDeliveryInfoMap ());
                        ostr << tgJsonString;
                        return;
                }
        } catch (std::exception const&  e) {
                LOG(ERROR) <<"error happening when handling request  " << boost::diagnostic_information(e);
        }

        catch (...) {
                LOG(ERROR)<<"unknown error happening when handling request";
        }
        ostr << "ERROR in PROCESSING";
        return;

}

EntityDeliveryInfoCacheServiceRequestHandler::~EntityDeliveryInfoCacheServiceRequestHandler() {
}
