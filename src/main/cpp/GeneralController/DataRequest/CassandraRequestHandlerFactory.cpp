#include "CassandraRequestHandlerFactory.h"
#include "StringUtil.h"
#include "DataRequestHandler.h"
#include "TempUtil.h"
#include "GUtil.h"
#include "UnknownRequestHandler.h"
#include "ConverterUtil.h"

#include "LogLevelManager.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "CommonRequestHandlerFactory.h"
#include "DeviceSegmentHistory.h"
#include "FeatureDeviceHistory.h"

#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "BeanFactory.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
#include "BeanFactory.h"
#include "MySqlTgPerformanceTrackService.h"
#include "MySqlEntityRealTimeDeliveryInfoService.h"
#include "DeviceIdToIpsMap.h"


#include "PixelDeviceHistory.h"
#include "GicapodsIdToExchangeIdsMap.h"
#include "AdHistory.h"
#include "EventLog.h"
#include "CassandraDataRequestHandler.h"

CassandraRequestHandlerFactory::CassandraRequestHandlerFactory() {

}


void CassandraRequestHandlerFactory::init() {

}

bool CassandraRequestHandlerFactory::isAppHealthy() {
        return true;
}


Poco::Net::HTTPRequestHandler
*CassandraRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {

        entityToModuleStateStats->addStateModuleForEntity ("IncomingRequests",
                                                           "CassandraRequestHandlerFactory",
                                                           "ALL");
        MLOG(3) << "request.getURI() : " << request.getURI ();

        if (
                StringUtil::containsCaseInSensitive (request.getURI (), "/readDataOptional/") ||
                StringUtil::containsCaseInSensitive (request.getURI (), "/writeBatchData/")
                ) {
                auto cassandraDataRequestHandler = new CassandraDataRequestHandler();

                cassandraDataRequestHandler->cassandraServiceDeviceFeatureHistory =
                        (CassandraService<DeviceFeatureHistory>*)beanFactory->deviceFeatureHistoryCassandraService.get();
                cassandraDataRequestHandler->cassandraServiceEventLog =
                        (CassandraService<EventLog>*)beanFactory->eventLogCassandraService.get();

                cassandraDataRequestHandler->cassandraServiceFeatureDeviceHistory =
                        (CassandraService<FeatureDeviceHistory>*)beanFactory->featureDeviceHistoryCassandraService.get();

                cassandraDataRequestHandler->cassandraServiceDeviceSegmentHistory =
                        (CassandraService<DeviceSegmentHistory>*)beanFactory->deviceSegmentHistoryCassandraService.get();
                cassandraDataRequestHandler->cassandraServiceIpToDeviceIdsMap =
                        (CassandraService<IpToDeviceIdsMap>*)beanFactory->ipToDeviceIdsMapCassandraService.get();
                cassandraDataRequestHandler->cassandraServiceDeviceIdToIpsMap =
                        (CassandraService<DeviceIdToIpsMap>*)beanFactory->deviceIdToIpsMapCassandraService.get();
                cassandraDataRequestHandler->cassandraServiceGicapodsIdToExchangeIdsMap =
                        (CassandraService<GicapodsIdToExchangeIdsMap>*)beanFactory->gicapodsIdToExchangeIdsMapCassandraService.get();

                cassandraDataRequestHandler->cassandraServiceAdHistory =
                        (CassandraService<AdHistory>*)beanFactory->adHistoryCassandraService.get();

                cassandraDataRequestHandler->cassandraServicePixelDeviceHistory
                        =   (CassandraService<PixelDeviceHistory>*)beanFactory->pixelDeviceHistoryCassandraService.get();

                cassandraDataRequestHandler->httpUtilService = beanFactory->httpUtilService.get();
                cassandraDataRequestHandler->entityToModuleStateStats =
                        beanFactory->entityToModuleStateStats.get();

                return cassandraDataRequestHandler;
        }

        return nullptr;
}
