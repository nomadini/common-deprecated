#ifndef DataMasterRequestHandlerFactory_H
#define DataMasterRequestHandlerFactory_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include <unordered_map>
class TargetGroupCacheService;
class CampaignCacheService;
class CreativeCacheService;

class EntityToModuleStateStats;
#include "Poco/Logger.h"
#include "Poco/LogStream.h"

#include <boost/thread.hpp>
#include <thread>
#include <unordered_map>
#include <memory>

#include "AerospikeDriver.h"
#include "CacheService.h"
#include "EntityProviderService.h"
class CassandraDriverInterface;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class TgFilterMeasuresService;
class BeanFactory;
class MySqlTgPerformanceTrackService;
class MySqlEntityRealTimeDeliveryInfoService;
class BeanFactory;
class MySqlMetricService;
class DataMasterRequestHandlerFactory;
class CommonRequestHandlerFactory;


class DataMasterRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {

void setupCommandsToInterfaces();

public:
bool isAppHealthy();

EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<std::unordered_map<std::string, CacheServiceInterface*> > entityToFetcherServiceMap;
std::shared_ptr<std::unordered_map<std::string, EntityProviderServiceInterface*> > entityToProviderServiceMap;

MySqlTgPerformanceTrackService* mySqlTgPerformanceTrackService;
CommonRequestHandlerFactory* commonRequestHandlerFactory;
MySqlEntityRealTimeDeliveryInfoService* mySqlEntityRealTimeDeliveryInfoService;
TgFilterMeasuresService* tgFilterMeasuresService;
BeanFactory* beanFactory;
void init();
DataMasterRequestHandlerFactory();
Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request);
};

#endif
