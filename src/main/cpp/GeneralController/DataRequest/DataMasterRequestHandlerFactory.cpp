#include "DataMasterRequestHandlerFactory.h"
#include "StringUtil.h"
#include "DataRequestHandler.h"
#include "TempUtil.h"
#include "GUtil.h"
#include "UnknownRequestHandler.h"
#include "ConverterUtil.h"

#include "LogLevelManager.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "CommonRequestHandlerFactory.h"
#include "DeviceSegmentHistory.h"
#include "FeatureDeviceHistory.h"

#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "BeanFactory.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
#include "BeanFactory.h"
#include "MySqlTgPerformanceTrackService.h"
#include "MySqlEntityRealTimeDeliveryInfoService.h"
#include "DeviceIdToIpsMap.h"


#include "PixelDeviceHistory.h"
#include "GicapodsIdToExchangeIdsMap.h"
#include "AdHistory.h"
#include "EventLog.h"
#include "CassandraDataRequestHandler.h"

DataMasterRequestHandlerFactory::DataMasterRequestHandlerFactory() {
        entityToFetcherServiceMap = std::make_shared<std::unordered_map<std::string, CacheServiceInterface*> >();
        entityToProviderServiceMap = std::make_shared<std::unordered_map<std::string, EntityProviderServiceInterface*> >();

}


void DataMasterRequestHandlerFactory::init() {
        setupCommandsToInterfaces();
}
bool DataMasterRequestHandlerFactory::isAppHealthy() {
        return true;
}


Poco::Net::HTTPRequestHandler
*DataMasterRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {

        entityToModuleStateStats->addStateModuleForEntity ("IncomingRequests",
                                                           "DataMasterRequestHandlerFactory",
                                                           "ALL");
        MLOG(3) << "request.getURI() : " << request.getURI ();

        //TODO : write a test for this
        if (!isAppHealthy ()) {
                entityToModuleStateStats->addStateModuleForEntity ("NOT_HEALTHY",
                                                                   "DataMasterRequestHandlerFactory",
                                                                   "ALL");

                //we won't serv this request to let other systems know something is wrong
                LOG(INFO) << "App not serving request...Not healthy";
                return new UnknownRequestHandler (entityToModuleStateStats);
        }
        MLOG(3) << "healthy app ";
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/data/api")) {
                MLOG(3) << "dataRequestHandler is chosen... ";
                entityToModuleStateStats->addStateModuleForEntity ("ServingData",
                                                                   "DataMasterRequestHandlerFactory",
                                                                   "ALL");


                auto dataRequestHandler = new DataRequestHandler (
                        entityToModuleStateStats,
                        entityToFetcherServiceMap,
                        entityToProviderServiceMap);
                dataRequestHandler->tgFilterMeasuresService = tgFilterMeasuresService;
                dataRequestHandler->mySqlTgPerformanceTrackService = mySqlTgPerformanceTrackService;
                dataRequestHandler->mySqlEntityRealTimeDeliveryInfoService = mySqlEntityRealTimeDeliveryInfoService;
                dataRequestHandler->httpUtilService = beanFactory->httpUtilService.get();

                return dataRequestHandler;

        }

        auto commonRequestHandler = commonRequestHandlerFactory->createRequestHandler(request);
        if (commonRequestHandler != nullptr) {
                return commonRequestHandler;
        }

        entityToModuleStateStats->addStateModuleForEntity ("NotServingData",
                                                           "DataMasterRequestHandlerFactory",
                                                           "ALL");

        MLOG(3) << "found no handler for request with uri : " << request.getURI ();
        return new UnknownRequestHandler(entityToModuleStateStats);
}

void DataMasterRequestHandlerFactory::setupCommandsToInterfaces() {
        //some apps don't have all cahces, like adserver , so it makes sense to be nullptr

        for (auto entityProviderService : beanFactory->allEntityProviderService) {
                if (entityProviderService != nullptr) {
                        auto key = StringUtil::toLowerCase(entityProviderService->getEntityName());
                        MLOG(1) << "registering " << key<< " as CacheService";
                        entityToProviderServiceMap->insert(
                                std::make_pair(
                                        key,
                                        entityProviderService));
                }
        }

        for (auto entityFetcherHttpService : beanFactory->allCacheServices) {
                if (entityFetcherHttpService != nullptr) {
                        auto key = StringUtil::toLowerCase(entityFetcherHttpService->getEntityName());
                        MLOG(1) << "registering " << key<< " as entityFetcherHttpService";
                        entityToFetcherServiceMap->insert(
                                std::make_pair(
                                        key,
                                        entityFetcherHttpService));
                }
        }

}
