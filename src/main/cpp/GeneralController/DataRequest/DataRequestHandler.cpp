
#include "DataRequestHandler.h"
#include "JsonUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "StringUtil.h"
#include "DataMasterRequestHandlerFactory.h"
#include "HttpUtil.h"
#include "GUtil.h"
#include "FunctionUtil.h"
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include "ClientDealCacheService.h"
#include "DealCacheService.h"
#include "TargetGroupSegmentCacheService.h"
#include "SegmentCacheService.h"
#include "ModelCacheService.h"
#include "TgGeoFeatureCollectionMapCacheService.h"
#include "EntityProviderService.h"
#include "DataMasterRequestHandlerFactory.h"
#include "DealCacheService.h"
#include "TargetGroupCacheService.h"
#include "CampaignCacheService.h"
#include "CreativeCacheService.h"

#include "EntityToModuleStateStats.h"

#include "TargetGroupBWListCacheService.h"

#include "TargetGroupCreativeCacheService.h"
#include "TargetGroupGeoSegmentCacheService.h"
#include "TargetGroupDayPartCacheService.h"
#include "TargetGroupGeoLocationCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "GlobalWhiteListedCacheService.h"
#include "GlobalWhiteListedModelCacheService.h"
#include "TgBiddingPerformanceFilterMeasures.h"
#include "ClientDealCacheService.h"
#include "Client.h"
#include "JsonArrayUtil.h"
#include "TargetGroupOfferCacheService.h"
#include "OfferPixelMapCacheService.h"
#include "CampaignOfferCacheService.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
#include "MySqlTgPerformanceTrackService.h"
#include "GUtil.h"
#include "MySqlEntityRealTimeDeliveryInfoService.h"
#include "TargetGroup.h"
#include "EntityProviderService.h"
#include "CacheService.h"
/*
   this handler serves data to client apps
   it is embedded in DataMaster, and it can also be mebedded in other apps
 */
DataRequestHandler::DataRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<std::unordered_map<std::string, CacheServiceInterface*> > entityToFetcherServiceMap,
        std::shared_ptr<std::unordered_map<std::string, EntityProviderServiceInterface*> > entityToProviderServiceMap) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->entityToFetcherServiceMap = entityToFetcherServiceMap;
        this->entityToProviderServiceMap = entityToProviderServiceMap;
}

std::string DataRequestHandler::findKeyFromRequestBody(
        std::string requestBody,
        std::string& entity,
        std::string& function) {

        try {
                auto doc = parseJsonSafely(requestBody);
                entity = StringUtil::toLowerCase(JsonUtil::GetStringSafely(*doc,"entity"));
                function = StringUtil::toLowerCase(JsonUtil::GetStringSafely(*doc,"function"));
                LOG(INFO)<<"processing function "<< function<<" on entity "<< entity;
        } catch(...) {
                entityToModuleStateStats->addStateModuleForEntity ("#ErrorInParsingTheRequest",
                                                                   "DataRequestHandler",
                                                                   "ALL");
        }

        std::string key = entity+function;
        MLOG(3) << "entity : " << entity << "  function : " << function
                << " , Key : "<< key;
        return key;
}

void DataRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                       Poco::Net::HTTPServerResponse &response) {

        try {

                auto requestBody = httpUtilService->getRequestBody(request);

                entityToModuleStateStats->addStateModuleForEntity ("numberOfDataRequestProcessed",
                                                                   "DataRequestHandler",
                                                                   "ALL");
                try {
                        Poco::Timestamp now;
                        std::string entity = "";
                        std::string function = "";

                        findKeyFromRequestBody(requestBody, entity, function);

                        std::string interfaceHandlerKey = entity;
                        MLOG(INFO) << "interfaceHandlerKey : "<<interfaceHandlerKey;
                        int numberOfEntities = 0;
                        std::string responseBody;

                        if(StringUtil::containsCaseInSensitive(function, "readHashOfAll")) {
                                auto pairPtr = entityToProviderServiceMap->find(entity);
                                if(pairPtr != entityToProviderServiceMap->end()) {
                                        responseBody = pairPtr->second->processReadHashOfAll();
                                } else {
                                        EXIT("couldnt find a service for " +  entity  +  " and " + function);
                                }
                        } else if(StringUtil::containsCaseInSensitive(function, "readall")) {
                                auto pairPtr = entityToProviderServiceMap->find(entity);
                                if(pairPtr != entityToProviderServiceMap->end()) {
                                        responseBody = pairPtr->second->processReadAllCommand(requestBody, numberOfEntities);
                                } else {
                                        EXIT("couldnt find a service for " +  entity  +  " and " + function);
                                }
                        } else if (StringUtil::containsCaseInSensitive(function, "readallbyclient")) {
                                auto pairPtr = entityToFetcherServiceMap->find(interfaceHandlerKey);
                                if(pairPtr != entityToFetcherServiceMap->end()) {
                                        auto serviceInterface = pairPtr->second;

                                        responseBody = serviceInterface->processReadAllByClient(requestBody, numberOfEntities);
                                        LOG(INFO) << "loaded "<<numberOfEntities<<" entity for request : entity : " << entity;
                                        entityToModuleStateStats->addStateModuleForEntity ("PROCESSING_ENTITY:" + entity + "_function:" + function,
                                                                                           "DataRequestHandler",
                                                                                           "ALL");

                                        MLOG(3) << "responseBody : " << responseBody;

                                } else {
                                        entityToModuleStateStats->addStateModuleForEntity ("NotFoundServiceToProcess",
                                                                                           "DataRequestHandler",
                                                                                           "ALL",
                                                                                           EntityToModuleStateStats::exception);

                                        entityToModuleStateStats->addStateModuleForEntity ("#NoBadKey_key:" + keyOfRequest,
                                                                                           "DataRequestHandler",
                                                                                           "ALL",
                                                                                           EntityToModuleStateStats::exception);
                                        throwEx ("coudn't find a service to process the request, keyOfRequest : " + keyOfRequest);
                                }

                        }



                        Poco::Timestamp::TimeDiff diff = now.elapsed (); // how long did it take?

                        MLOG(3) << "DataMaster Latency  " << diff / 1000 << " milliseconds";
                        MLOG(3) << "response " << responseBody;
                        std::ostream &ostr = response.send ();
                        ostr << responseBody;

                } catch (std::exception const &e) {
                        LOG(ERROR) << "error happening when handling request  " << boost::diagnostic_information (e);
                        entityToModuleStateStats->addStateModuleForEntity ("#OfExceptionsInHandlingRequest",
                                                                           "DataRequestHandler",
                                                                           "ALL");

                } catch (...) {
                        LOG(ERROR) << "unknown error happening when handling request";

                        entityToModuleStateStats->addStateModuleForEntity ("#OfUnknownExceptionsInHandlingRequest",
                                                                           "DataRequestHandler",
                                                                           "ALL");

                }

        }
        catch(...) {
                gicapods::Util::showStackTrace();
        }

        std::ostream &ostr = response.send ();
        ostr << "";
        return;
}


std::string DataRequestHandler::processReadAllForTargetGroupIdToPerformanceMetricMap(std::string requestBody, int& numberOfEntities) {
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures> > targetGroupIdToPerformanceMetricMap =
                *tgFilterMeasuresService->targetGroupIdToFilterMeasuresMap;

        std::vector<std::shared_ptr<TgBiddingPerformanceFilterMeasures> > allMetrics;

        for(auto entry : targetGroupIdToPerformanceMetricMap) {
                allMetrics.push_back(entry.second);
        }

        //throw linking error!!
        // std::string json = JsonArrayUtil::convertListToJson(allMetrics);
        // return json;
        return "";
}

std::string DataRequestHandler::processReadAllTargetGroupTrackingPerformances(std::string requestBody, int& numberOfEntities) {
        NULL_CHECK(mySqlTgPerformanceTrackService);
        Poco::DateTime startDate = DateTimeUtil::getDateWithHourPercision(-1);
        Poco::DateTime endDate = DateTimeUtil::getDateWithMinutePercision(-2); //so that we don't get affected by the race between the bidder and pacer in saving the latest measures

        std::vector<std::shared_ptr<OverallBidWinContainerPerTargetGroup> > allTracking = mySqlTgPerformanceTrackService->readAllTargetGroupPerformanceTracksBetweenDates(startDate, endDate);
        numberOfEntities = allTracking.size();
        return JsonArrayUtil::convertListToJson(allTracking);
}
