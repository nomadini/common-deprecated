#ifndef LogLevelManagerRequestHandler_H
#define LogLevelManagerRequestHandler_H

#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
class TargetGroupCacheService;
class EntityToModuleStateStats;

class LogLevelManagerRequestHandler;

class LogLevelManagerRequestHandler : public Poco::Net::HTTPRequestHandler {

public:

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

LogLevelManagerRequestHandler();

virtual ~LogLevelManagerRequestHandler();
};

#endif
