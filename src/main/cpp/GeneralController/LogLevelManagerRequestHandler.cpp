
#include "LogLevelManagerRequestHandler.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include <boost/exception/all.hpp>

#include "StringUtil.h"
#include "HttpUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"

LogLevelManagerRequestHandler::LogLevelManagerRequestHandler() {
}


void LogLevelManagerRequestHandler::handleRequest(
        Poco::Net::HTTPServerRequest& request,
        Poco::Net::HTTPServerResponse& response) {
        std::ostream& ostr = response.send();
        try {
                MLOG(3) << "request.getURI() : " << request.getURI ();
                auto queryParams  = HttpUtil::getMapOfQueryParams(request);
                std::string functionName = HttpUtil::getRequiredParamFromQueryParameters("function", queryParams);

                if(StringUtil::equalsIgnoreCase(functionName, "enableAll")) {

                        // std::string proeprtyName = HttpUtil::getRequiredParamFromQueryParameters("name", queryParams);
                        // std::string properyValue = configService->get(proeprtyName);
                        // MLOG(3) << "properyValue : " << properyValue;
                        LogLevelManager::shared_instance()->enableAll();
                        return;
                } else if(StringUtil::equalsIgnoreCase(functionName, "getLogsToStatusMapAsJson")) {
                        ostr << LogLevelManager::shared_instance()->getLogsToStatusMapAsJson();
                        return;
                } else if(StringUtil::equalsIgnoreCase(functionName, "disableAll")) {
                        LogLevelManager::shared_instance()->disableAll();
                        ostr << "disabledAll";
                        return;
                }

        } catch (std::exception const&  e) {
                LOG(ERROR) <<"error happening when handling request  " << boost::diagnostic_information(e);
        }

        catch (...) {
                LOG(ERROR)<<"unknown error happening when handling request";
        }
        ostr << "ERROR in PROCESSING";
        return;

}

LogLevelManagerRequestHandler::~LogLevelManagerRequestHandler() {
}
