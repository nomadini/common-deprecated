
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"

#include "EntityToModuleStateStats.h"
#include "ForceReloadCachesHandler.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include <string>
#include <memory>
#include <boost/exception/all.hpp>

ForceReloadCachesHandler::ForceReloadCachesHandler() {

}

void ForceReloadCachesHandler::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response) {

								try {
																entityToModuleStateStats->addStateModuleForEntity ("ServingDataByUnknown",
																																																																			"ForceReloadCachesHandler",
																																																																			"ALL");

																std::string requestBody = HttpUtil::getRequestBody(request);

																LOG(INFO)<<"calling reload function pointer";
																if (dataClient != nullptr) {
																								dataClient->reloadDataFromDataMaster(); //we are calling the function pointer
																}

																std::ostream& ostr = response.send();
																ostr << "dataReloadedWithSuccess";
																return;
								} catch (std::exception const&  e) {
																LOG(ERROR) <<"error happening when handling request  " << boost::diagnostic_information(e);
								}

								catch (...) {
																LOG(ERROR)<<"unknown error happening when handling request";
								}

								std::ostream& ostr = response.send();
								ostr << "dataReloadedWithFailure";
								return;
}
