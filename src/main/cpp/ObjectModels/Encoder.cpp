
#include "GUtil.h"
#include "Poco/Base64Decoder.h"
#include "Poco/Base64Encoder.h"
#include "Encoder.h"
#include "JsonUtil.h"
#include <string>
#include <memory>


Encoder::Encoder() : Object(__FILE__) {

}


std::string Encoder::encode(std::string encodeMe) {
								std::ostringstream str;
								Poco::Base64Encoder encoder(str);
								encoder << encodeMe;
								encoder.close();
								return str.str();
}

std::string Encoder::decode(std::string stringToDecode) {
								std::istringstream istr(stringToDecode);
								Poco::Base64Decoder decoder(istr);
								std::string s;
								decoder >> s;
								assertAndThrow(!decoder.fail());
								assertAndThrow(decoder.good());
								return s;

}

std::string Encoder::toString() {
								return this->toJson();
}

std::string Encoder::toJson() {
								std::string json;
								rapidjson::StringBuffer s;
								rapidjson::Writer<rapidjson::StringBuffer> writer(s);
								writer.StartObject();
								/*writer.String("id");
								   writer.String((char*) id.c_str());
								   writer.String("bidfloor");*/
								/*if (banner != NULL) {
								   writer.String(("banner"));
								   writer.String((char*) banner->toJson().c_str());*/
								writer.EndObject();
								return json;
}

Encoder::~Encoder() {

}
