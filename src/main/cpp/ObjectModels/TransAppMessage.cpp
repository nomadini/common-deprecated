

#include "AdEntry.h"
#include "GUtil.h"
#include "TransAppMessage.h"
#include "JsonUtil.h"
#include "AtomicLong.h"

#include "Device.h"

TransAppMessage::TransAppMessage()  : Object(__FILE__) {


        bidPrice = 0;
        deviceLat = 0;
        deviceLon = 0;
        chosenTargetGroupId = 0;
        chosenCreativeId = 0;
        version = "";
        sourceAppName= "";
        destinationAppName= "";
        transactionId= "";
        userTimeZonDifferenceWithUTC = 0;
}
TransAppMessage::~TransAppMessage() {

}

std::string TransAppMessage::toJson() {

        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "version", version, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "sourceAppName", sourceAppName, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "destinationAppName", destinationAppName, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "transactionId", transactionId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "chosenTargetGroupId", chosenTargetGroupId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "chosenCreativeId", chosenCreativeId, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "bidPrice", bidPrice, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "userTimeZone", userTimeZone, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "userTimeZonDifferenceWithUTC", userTimeZonDifferenceWithUTC, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceUserAgent", deviceUserAgent, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceIp", deviceIp, value);
        NULL_CHECK(device);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceId", device->getDeviceId(), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceType", device->getDeviceType(), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceCountry", deviceCountry, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceCity", deviceCity, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceState", deviceState, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceZipcode", deviceZipcode, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceIpAddress", deviceIpAddress, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "siteDomain", siteDomain, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "sitePage", sitePage, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "sitePublisherDomain", sitePublisherDomain, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "sitePublisherName", sitePublisherName, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceLat", deviceLat, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceLon", deviceLon, value);


        return JsonUtil::valueToString (value);

}

std::shared_ptr<TransAppMessage> TransAppMessage::fromJson(std::string jsonString) {
        std::shared_ptr<TransAppMessage> msg = std::make_shared<TransAppMessage>();

        auto document = parseJsonSafely(jsonString);
        msg->version = JsonUtil::GetStringSafely (*document, "version");
        msg->sourceAppName = JsonUtil::GetStringSafely (*document, "sourceAppName");
        msg->destinationAppName = JsonUtil::GetStringSafely (*document, "destinationAppName");
        msg->transactionId = JsonUtil::GetStringSafely (*document, "transactionId");

        msg->chosenTargetGroupId = JsonUtil::GetIntSafely (*document, "chosenTargetGroupId");
        msg->chosenCreativeId = JsonUtil::GetIntSafely (*document, "chosenCreativeId");


        msg->bidPrice = JsonUtil::GetDoubleSafely (*document, "bidPrice");
        msg->userTimeZone = JsonUtil::GetStringSafely (*document, "userTimeZone");
        msg->userTimeZonDifferenceWithUTC = JsonUtil::GetIntSafely (*document, "userTimeZonDifferenceWithUTC");

        msg->deviceUserAgent = JsonUtil::GetStringSafely (*document, "deviceUserAgent");
        msg->deviceIp = JsonUtil::GetStringSafely (*document, "deviceIp");

        msg->device = std::make_shared<Device>(
                JsonUtil::GetStringSafely (*document, "deviceId"),
                JsonUtil::GetStringSafely (*document, "deviceType"));
        msg->deviceCountry = JsonUtil::GetStringSafely (*document, "deviceCountry");
        msg->deviceCity = JsonUtil::GetStringSafely (*document, "deviceCity");
        msg->deviceState = JsonUtil::GetStringSafely (*document, "deviceState");
        msg->deviceZipcode = JsonUtil::GetStringSafely (*document, "deviceZipcode");
        msg->deviceIpAddress = JsonUtil::GetStringSafely (*document, "deviceIpAddress", "");
        msg->siteDomain = JsonUtil::GetStringSafely (*document, "siteDomain");
        msg->sitePage = JsonUtil::GetStringSafely (*document, "sitePage");
        msg->sitePublisherDomain = JsonUtil::GetStringSafely (*document, "sitePublisherDomain");
        msg->sitePublisherName = JsonUtil::GetStringSafely (*document, "sitePublisherName");


        msg->deviceLat = JsonUtil::GetDoubleSafely (*document, "deviceLat");
        msg->deviceLon = JsonUtil::GetDoubleSafely (*document, "deviceLon");

        //TODO : std::vector<std::string> siteCategory;
        //TODO : std::shared_ptr<AdHistory> adHistory;
        return msg;
}
