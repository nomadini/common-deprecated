
#include "StringUtil.h"
#include "GUtil.h"
#include "IpToDeviceIdsMap.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include "DateTimeUtil.h"
#include "Device.h"
#include "JsonUtil.h"
#include "CassandraService.h"

#include "JsonArrayUtil.h"

IpToDeviceIdsMap::IpToDeviceIdsMap() : Object(__FILE__) {

}
IpToDeviceIdsMap::~IpToDeviceIdsMap() {

}
std::string IpToDeviceIdsMap::toString() {
								return this->toJson();

}

std::string IpToDeviceIdsMap::toJson() {

								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);

								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

void IpToDeviceIdsMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								JsonUtil::addMemberToValue_FromPair(doc, "ip", ip, value);
								JsonArrayUtil::addMemberToValue_FromPair(
																doc,
																"devices",
																devices,
																value
																);
}

std::shared_ptr<IpToDeviceIdsMap> IpToDeviceIdsMap::fromJsonValue(RapidJsonValueType value) {
								auto ipToDeviceIdsMap = std::make_shared<IpToDeviceIdsMap>();
								ipToDeviceIdsMap->ip = JsonUtil::GetStringSafely (value, "ip");
								JsonArrayUtil::getArrayFromValueMemeber(
																value,
																"devices",
																ipToDeviceIdsMap->devices);
								return ipToDeviceIdsMap;
}

std::shared_ptr<IpToDeviceIdsMap> IpToDeviceIdsMap::fromJson(std::string jsonModel) {
								auto document = parseJsonSafely(jsonModel);
								return fromJsonValue(*document);
}
