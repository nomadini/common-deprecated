/*
 * DeviceIdToIpsMapCassandraService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef DeviceIdToIpsMapCassandraService_H_
#define DeviceIdToIpsMapCassandraService_H_

class CassandraDriverInterface;

#include "Object.h"
namespace gicapods { class ConfigService; }

#include "DeviceIdToIpsMap.h"
class CassandraDriverInterface;
#include "HttpUtilService.h"
class EntityToModuleStateStats;
#include "CassandraService.h"
class AsyncThreadPoolService;

class DeviceIdToIpsMapCassandraService;



class DeviceIdToIpsMapCassandraService : public CassandraService<DeviceIdToIpsMap>, public Object {
private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;


AsyncThreadPoolService* asyncThreadPoolService;

public:
DeviceIdToIpsMapCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);



};




#endif /* GICAPODS_GICAPODSSERVER_SRC_CASSANDRA_IPDEVICEIDMAPCASSANDRASERVICE_H_ */
