#ifndef DeviceIdToIpsMap_H
#define DeviceIdToIpsMap_H


class StringUtil;

#include "JsonTypeDefs.h"

#include <memory>
#include <string>
#include <vector>
#include <set>
#include "cassandra.h"
#include "CassandraManagedType.h"

class Device;
class DeviceIdToIpsMap;
#include "Object.h"


class DeviceIdToIpsMap : public CassandraManagedType, public Object {

public:
std::shared_ptr<Device> device;
std::set<std::string> ips;


DeviceIdToIpsMap();

std::string toString();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
static std::shared_ptr<DeviceIdToIpsMap> fromJson(std::string json);
static std::shared_ptr<DeviceIdToIpsMap> fromJsonValue(RapidJsonValueType value);
virtual ~DeviceIdToIpsMap();
};


#endif
