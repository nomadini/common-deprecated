
#include "StringUtil.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "CrossWalkedDeviceMap.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Device.h"
#include "DateTimeUtil.h"
#include "CassandraService.h"
#include "JsonArrayUtil.h"


CrossWalkedDeviceMap::CrossWalkedDeviceMap() : Object(__FILE__) {

}

CrossWalkedDeviceMap::~CrossWalkedDeviceMap() {
}

std::string CrossWalkedDeviceMap::toString() {
								return this->toJson();

}

std::string CrossWalkedDeviceMap::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

void CrossWalkedDeviceMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {


								JsonUtil::addMemberToValue_FromPair(doc, "sourceDeviceId", sourceDevice->getDeviceId(), value);
								JsonUtil::addMemberToValue_FromPair(doc, "sourceDeviceType", sourceDevice->getDeviceType(), value);

								JsonArrayUtil::addMemberToValue_FromPair(
																doc,
																"linkedDevices",
																linkedDevices,
																value
																);
}

std::shared_ptr<CrossWalkedDeviceMap> CrossWalkedDeviceMap::fromJsonValue(RapidJsonValueType value) {
								auto crossWalkedDeviceMap = std::make_shared<CrossWalkedDeviceMap>();
								crossWalkedDeviceMap->sourceDevice =
																std::make_shared<Device>(
																								JsonUtil::GetStringSafely (value, "sourceDeviceId"),
																								JsonUtil::GetStringSafely (value, "sourceDeviceType"));
								JsonArrayUtil::getArrayFromValueMemeber(
																value,
																"linkedDevices",
																crossWalkedDeviceMap->linkedDevices);
								return crossWalkedDeviceMap;
}

std::shared_ptr<CrossWalkedDeviceMap> CrossWalkedDeviceMap::fromJson(std::string jsonModel) {
								auto document = parseJsonSafely(jsonModel);
								return fromJsonValue(*document);

}
