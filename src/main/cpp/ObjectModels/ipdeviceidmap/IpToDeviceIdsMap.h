#ifndef IpToDeviceIdsMap_H
#define IpToDeviceIdsMap_H


class StringUtil;

#include "JsonTypeDefs.h"

#include <memory>
#include <string>
#include <vector>
#include <set>
#include "cassandra.h"
#include "CassandraManagedType.h"

class Device;
#include "Object.h"
class IpToDeviceIdsMap;



class IpToDeviceIdsMap : public CassandraManagedType, public Object {

public:

std::string ip;
std::vector<std::shared_ptr<Device> > devices;


IpToDeviceIdsMap();

std::string toString();

std::string toJson();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<IpToDeviceIdsMap> fromJson(std::string json);
static std::shared_ptr<IpToDeviceIdsMap> fromJsonValue(RapidJsonValueType value);
virtual ~IpToDeviceIdsMap();
};


#endif
