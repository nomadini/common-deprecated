/*
 * IpToDeviceIdsMapCassandraService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef IpToDeviceIdsMapCassandraService_H_
#define IpToDeviceIdsMapCassandraService_H_
#include "Object.h"
class CassandraDriverInterface;


namespace gicapods { class ConfigService; }

#include "IpToDeviceIdsMap.h"
class CassandraDriverInterface;
#include "HttpUtilService.h"
class EntityToModuleStateStats;
#include "CassandraService.h"
class AsyncThreadPoolService;


class IpToDeviceIdsMapCassandraService;



class IpToDeviceIdsMapCassandraService : public CassandraService<IpToDeviceIdsMap>, public Object {
private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;

AsyncThreadPoolService* asyncThreadPoolService;

public:
IpToDeviceIdsMapCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);



};



#endif /* GICAPODS_GICAPODSSERVER_SRC_CASSANDRA_IPDEVICEIDMAPCASSANDRASERVICE_H_ */
