
#include "DeviceIdToIpsMapCassandraService.h"
#include "ConfigService.h"

DeviceIdToIpsMapCassandraService::DeviceIdToIpsMapCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService)  :
        CassandraService(cassandraDriver,
                         entityToModuleStateStats,
                         
                         asyncThreadPoolService,
                         configService), Object(__FILE__) {
        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}
