
#include "Poco/Net/MailMessage.h"
#include "Poco/Net/MailRecipient.h"
#include "Poco/Net/SMTPClientSession.h"
#include "Poco/Net/StringPartSource.h"
#include "Poco/Path.h"
#include "Poco/Exception.h"
#include "EmailSender.h"
#include "EntityToModuleStateStats.h"
#include "GUtil.h"
#include <iostream>


using Poco::Net::MailMessage;
using Poco::Net::MailRecipient;
using Poco::Net::SMTPClientSession;
using Poco::Net::StringPartSource;
using Poco::Path;
using Poco::Exception;

#include <Poco/Net/MailMessage.h>
#include <Poco/Net/MailRecipient.h>
#include <Poco/Net/SMTPClientSession.h>
#include <Poco/Net/NetException.h>
#include <Poco/Net/SecureSMTPClientSession.h>
#include <Poco/Net/InvalidCertificateHandler.h>
#include <Poco/Net/AcceptCertificateHandler.h>
#include <Poco/Net/SSLManager.h>
#include <Poco/Net/SecureStreamSocket.h>
#include <Poco/Net/MailRecipient.h>
#include <Poco/Net/ConsoleCertificateHandler.h>
#include <iostream>

using Poco::Net::InvalidCertificateHandler;
using Poco::Net::AcceptCertificateHandler;
using Poco::Net::Context;
using Poco::Net::SSLManager;
using Poco::Net::SecureStreamSocket;
using Poco::Net::SocketAddress;
using Poco::Net::SecureSMTPClientSession;
using Poco::Net::SMTPClientSession;
using Poco::SharedPtr;
using Poco::Net::MailMessage;
using Poco::Net::MailRecipient;
using Poco::Net::ConsoleCertificateHandler;
using namespace std;

EmailSender::EmailSender(std::string mailhost,
                         std::string username,
                         std::string password,
                         int port)  : Object(__FILE__) {
        this->username = username;
        this->password = password;
        this->mailhost = mailhost;
        this->port = port;
}

// smtp.gmail.com as host, 465 as port
void EmailSender::sendEmail(std::string sender,
                            std::string recipient,
                            std::string messageContent,
                            std::string subject)
{
        try{

                Poco::SharedPtr<Poco::Net::InvalidCertificateHandler> pCert = new Poco::Net::AcceptCertificateHandler(false);
                Poco::Net::Context::Ptr pContext = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", "",
                                                                          Poco::Net::Context::VERIFY_NONE, 9,
                                                                          false,
                                                                          "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
                Poco::Net::SSLManager::instance().initializeClient(0, pCert, pContext);

                //sSmtpServer is smtp.gmail.com and nSmtpPort is 465
                //"smtp.mail.yahoo.com", 465 : this works
                Poco::Net::SecureStreamSocket pSSLSocket(pContext);
                pSSLSocket.connect(Poco::Net::SocketAddress("smtp.mail.yahoo.com", 465));
                Poco::Net::SMTPClientSession pSession_(pSSLSocket);
                pSession_.login(Poco::Net::SMTPClientSession::AUTH_LOGIN, username, password);

                MailMessage message;
                message.setSender(sender);
                message.addRecipient(MailRecipient(MailRecipient::PRIMARY_RECIPIENT, recipient));
                // std::string logo(reinterpret_cast<const char*>(PocoLogo), sizeof(PocoLogo));
                // message.setContentType("text/html");
                message.addContent(new StringPartSource(messageContent));

                // message.addAttachment("logo", new StringPartSource(logo, "image/gif"));
                message.setSubject(subject);
                pSession_.sendMessage(message);
                pSession_.close();

        }
        catch (Exception& exc)
        {
                LOG(ERROR) <<"ERROR : "<< exc.displayText() << std::endl;
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_SENDING_EMAIL",
                        "EmailSender",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception
                        );
                gicapods::Util::showStackTrace();

        }
}

EmailSender::~EmailSender() {

}
