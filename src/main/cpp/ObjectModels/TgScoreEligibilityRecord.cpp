
#include <atomic>
#include <memory>
#include <string>
#include <unordered_map>
#include "JsonTypeDefs.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "TgScoreEligibilityRecord.h"
#include "TgModelEligibilityRecord.h"
#include "DateTimeUtil.h"

TgScoreEligibilityRecord::TgScoreEligibilityRecord() : Object(__FILE__) {

        this->modelIdToEligibilityRecords =
                std::make_shared<std::unordered_map<int, std::shared_ptr<TgModelEligibilityRecord> > > ();
        this->targetGroupId = 0;
}

TgScoreEligibilityRecord::~TgScoreEligibilityRecord() {
}

void TgScoreEligibilityRecord::addPropertiesToJsonValue(RapidJsonValueType parentValue, DocumentType* doc) {

        RapidJsonValueTypeNoRef arrayValue(rapidjson::kArrayType);
        arrayValue.SetArray();
        for (auto& kvPair : *modelIdToEligibilityRecords) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kObjectType);
                kvPair.second->addPropertiesToJsonValue(objValue, doc);
                arrayValue.PushBack (objValue, doc->GetAllocator());
        }

        RapidJsonValueTypeNoRef nameOfArrayValue ("tgScoreEligibility", doc->GetAllocator ());
        parentValue.AddMember (nameOfArrayValue, arrayValue, doc->GetAllocator ());

        assertAndThrow(targetGroupId != 0);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, parentValue);
}

std::shared_ptr<TgScoreEligibilityRecord> TgScoreEligibilityRecord::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TgScoreEligibilityRecord> tgScoreEligibilityRecord =
                std::make_shared<TgScoreEligibilityRecord>();
        tgScoreEligibilityRecord->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        assertAndThrow(tgScoreEligibilityRecord->targetGroupId != 0);
        std::vector<std::shared_ptr<TgModelEligibilityRecord> > allModelRecords =
                JsonArrayUtil::getArrayOfObjectsFromMemberInValue<TgModelEligibilityRecord>(value, "tgScoreEligibility");

        for(auto modelRecord : allModelRecords) {
                tgScoreEligibilityRecord->modelIdToEligibilityRecords->insert(
                        std::make_pair(modelRecord->id, modelRecord));
        }
        return tgScoreEligibilityRecord;
}

std::string TgScoreEligibilityRecord::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

std::shared_ptr<TgScoreEligibilityRecord> TgScoreEligibilityRecord::fromJson(std::string jsonString) {
        auto doc = JsonUtil::parseJsonSafely(jsonString);
        return TgScoreEligibilityRecord::fromJsonValue(*doc);
}
