#include "Device.h"
#include "CassandraDriverInterface.h"
#include "GUtil.h"
#include "RealTimeEntityCacheService.h"

#include "AeroCacheService.h"
#include "CassandraService.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "EntityToModuleStateStats.h"
#include "HttpUtilService.h"

template <class T>
RealTimeEntityCacheService<T>::RealTimeEntityCacheService(
        int inMemoryCacheSize,
        EntityToModuleStateStats* entityToModuleStateStats)  : Object(__FILE__) {

        inMemoryCache = std::make_shared<gicapods::SizeAndTimeBasedLRUCache<std::string, std::shared_ptr<T> > > (
                inMemoryCacheSize, 10, T::getName());
        inMemoryCache->entityToModuleStateStats = entityToModuleStateStats;
        this->entityToModuleStateStats = entityToModuleStateStats;
}

template <class T>
RealTimeEntityCacheService<T>::~RealTimeEntityCacheService() {

}

template <class T>
std::shared_ptr<T> RealTimeEntityCacheService<T>::readDataOptional(
        std::shared_ptr<T> keyObject) {

        std::shared_ptr<T> finalValue = nullptr;

        if (isPartOfCacheSample(keyObject)) {

                auto inMemoryValue = inMemoryCache->get(aeroTraits::getKey(keyObject.get()));
                if (inMemoryValue != boost::none) {
                        entityToModuleStateStats->
                        addStateModuleForEntity("IN_MEMORY_CACHE_HIT",
                                                "RealTimeEntityCacheService" + realTimeEntityAerospikeCacheService->getEntityName(),
                                                "ALL",
                                                EntityToModuleStateStats::important);
                        finalValue = *inMemoryValue;
                        return finalValue;
                }

                entityToModuleStateStats->
                addStateModuleForEntity("IN_MEMORY_CACHE_MISS",
                                        "RealTimeEntityCacheService" + realTimeEntityAerospikeCacheService->getEntityName(),
                                        "ALL",
                                        EntityToModuleStateStats::important);


                finalValue = readAndPopulateFromRemoteCache(keyObject);
        } else {
                entityToModuleStateStats->
                addStateModuleForEntity("BYPASS_REMOTE_CACHE_ATTEMPT",
                                        "RealTimeEntityCacheService" + realTimeEntityAerospikeCacheService->getEntityName(),
                                        "ALL");
                finalValue = readFromMainSource(keyObject);
        }
        entityToModuleStateStats->
        addStateModuleForEntity("NUMBER_OF_ATTEMPTS",
                                "RealTimeEntityCacheService" + realTimeEntityAerospikeCacheService->getEntityName(),
                                "ALL");
        return finalValue;
}

template <class T>
std::shared_ptr<T>  RealTimeEntityCacheService<T>::
readAndPopulateFromRemoteCache(std::shared_ptr<T> keyObject) {
        std::shared_ptr<T> finalValue = nullptr;

        int cachedResult = 0;
        std::shared_ptr<T> cachedValue = realTimeEntityAerospikeCacheService->
                                         readDataOptional(keyObject, cachedResult);
        if (cachedValue == nullptr) {

                if(cachedResult == AerospikeDriver::CACHE_MISS) {
                        entityToModuleStateStats->
                        addStateModuleForEntity("REMOTE_CACHE_MISS",
                                                "RealTimeEntityCacheService" + realTimeEntityAerospikeCacheService->getEntityName(),
                                                "ALL",
                                                EntityToModuleStateStats::important);

                        finalValue = readFromMainSource(keyObject);
                        //we write the value now, when its missed
                        writeKeyValueInCache(keyObject, finalValue);
                } else {
                        //data was in cache but it was empty...
                        //we dont read from main source again
                        entityToModuleStateStats->
                        addStateModuleForEntity("REMOTE_CACHE_HIT_WITH_EMPTY_DATA",
                                                "RealTimeEntityCacheService" + realTimeEntityAerospikeCacheService->getEntityName(),
                                                "ALL",
                                                EntityToModuleStateStats::important);
                        finalValue = nullptr;
                }
        } else {
                entityToModuleStateStats->
                addStateModuleForEntity("REMOTE_CACHE_HIT",
                                        "RealTimeEntityCacheService" + realTimeEntityAerospikeCacheService->getEntityName(),
                                        "ALL",
                                        EntityToModuleStateStats::important);

                finalValue = cachedValue;
        }

        return finalValue;
}

template <class T>
void RealTimeEntityCacheService<T>::
writeKeyValueInCache(std::shared_ptr<T> keyObject,std::shared_ptr<T> finalValue) {
        realTimeEntityAerospikeCacheService->putDataInCache(keyObject, finalValue);
        inMemoryCache->put(aeroTraits::getKey(keyObject.get()), finalValue);
}

template <class T>
std::shared_ptr<T> RealTimeEntityCacheService<T>::readFromMainSource(std::shared_ptr<T> keyObject) {
        return realTimeEntityCassandraService->readDataOptional(keyObject);
}

template <class T>
bool RealTimeEntityCacheService<T>::isPartOfCacheSample(std::shared_ptr<T> keyObject) {
        return true;
}

#include "DeviceSegmentHistory.h"
template class RealTimeEntityCacheService<DeviceSegmentHistory>;

#include "AdHistory.h"
template class RealTimeEntityCacheService<AdHistory>;
