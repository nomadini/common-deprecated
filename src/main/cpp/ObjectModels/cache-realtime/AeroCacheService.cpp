#include "Device.h"
#include "CassandraDriverInterface.h"
#include "GUtil.h"
#include "AeroCacheService.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "AerospikeDriver.h"


template <class T>
AeroCacheService<T>::AeroCacheService()  : Object(__FILE__) {
}

template <class T>
AeroCacheService<T>::~AeroCacheService() {
}

template <class T>
std::shared_ptr<T> AeroCacheService<T>::readDataOptional(std::shared_ptr<T> keyObject,
                                                         int& cachedResult) {
        NULL_CHECK(aeroSpikeDriver);
        std::string value = aeroSpikeDriver->get_cache(aeroTraits::getNamespaceName(),
                                                       aeroTraits::getSetName(),
                                                       aeroTraits::getKey(keyObject.get()),
                                                       aeroTraits::getBinName(),
                                                       cachedResult);
        if (value.empty()) {
                return nullptr;
        } else {
                return aeroTraits::getObjectFromStr(value);
        }
}

template <class T>
std::string AeroCacheService<T>::getEntityName() {
        return aeroTraits::getEntityName();
}

template <class T>
void AeroCacheService<T>::putDataInCache(std::shared_ptr<T> keyObject) {
        NULL_CHECK(keyObject);
        aeroSpikeDriver->set_cache(aeroTraits::getNamespaceName(),
                                   aeroTraits::getSetName(),
                                   aeroTraits::getKey(keyObject.get()),
                                   aeroTraits::getBinName(),
                                   aeroTraits::getBinValue(keyObject.get()),
                                   aeroTraits::getRecentyInMinute() * 60);
}

template <class T>
void AeroCacheService<T>::putDataInCache(std::shared_ptr<T> keyObject,
                                         std::shared_ptr<T> valueObject) {
        NULL_CHECK(keyObject);
        if (valueObject == nullptr) {
                //we set "" as the bin value if the object was nullptr
                //so that we can cache the keys that we don't have data for them
                aeroSpikeDriver->set_cache(aeroTraits::getNamespaceName(),
                                           aeroTraits::getSetName(),
                                           aeroTraits::getKey(keyObject.get()),
                                           aeroTraits::getBinName(),
                                           "",
                                           aeroTraits::getRecentyInMinute() * 60);
        } else {
                aeroSpikeDriver->set_cache(aeroTraits::getNamespaceName(),
                                           aeroTraits::getSetName(),
                                           aeroTraits::getKey(keyObject.get()),
                                           aeroTraits::getBinName(),
                                           aeroTraits::getBinValue(keyObject.get()),
                                           aeroTraits::getRecentyInMinute() * 60);
        }

}

#include "DeviceSegmentHistory.h"
template class AeroCacheService<DeviceSegmentHistory>;

#include "AdHistory.h"
template class AeroCacheService<AdHistory>;

#include "EventLog.h"
template class AeroCacheService<EventLog>;

#include "CrossWalkedDeviceMap.h"
template class AeroCacheService<CrossWalkedDeviceMap>;

#include "Feature.h"
template class AeroCacheService<Feature>;

#include "EventLock.h"
template class AeroCacheService<EventLock>;

#include "PlacesCachedResult.h"
template class AeroCacheService<PlacesCachedResult>;
