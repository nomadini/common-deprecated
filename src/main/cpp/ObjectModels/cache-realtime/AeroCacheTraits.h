/*
 * AeroCacheService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef AeroCacheTraits_H_
#define AeroCacheTraits_H_


#include "AerospikeDriver.h"


#include "Feature.h"
#include "AdHistory.h"
#include "DeviceSegmentHistory.h"
#include "StringUtil.h"
#include "EventLock.h"
#include "Device.h"
#include "CrossWalkedDeviceMap.h"
template <class T>
class AeroCacheTraits
{

public:
static std::string getEntityName()
{
        EXIT("not implemented");
}
static std::string getNamespaceName()
{
        EXIT("not implemented");
}
static std::string getSetName()
{
        EXIT("not implemented");
}

static std::string getBinName()
{
        EXIT("not implemented");
}

static std::string getKey(T* value)
{
        EXIT("not implemented");
}
static std::string getBinValue(T* value)
{
        EXIT("not implemented");
}

static std::shared_ptr<T> getObjectFromStr(std::string str) {
        EXIT("not implemented");
}

static int getRecentyInMinute()
{

        int value = 10;
        assertAndThrow (value != AS_RECORD_DEFAULT_TTL &&
                        value != AS_RECORD_NO_EXPIRE_TTL);

        return value;
}

};


template <>
class AeroCacheTraits <DeviceSegmentHistory> {
public:
static std::string getEntityName()
{
        return "DeviceSegmentHistory";
}
static std::string getNamespaceName()
{
        return "DeviceSegmentHistory";
}
static std::string getSetName()
{
        return "DeviceSegmentHistory";
}

static std::string getBinName()
{
        return "key1";
}

static std::string getKey(DeviceSegmentHistory* value)
{
        NULL_CHECK(value);
        return value->device->getDeviceId();
}

static std::string getBinValue(DeviceSegmentHistory* value)
{
        if (value == nullptr) {
                return "";
        }
        return value->toJson();
}
static std::shared_ptr<DeviceSegmentHistory> getObjectFromStr(std::string str) {
        auto deviceSegmentHistory = DeviceSegmentHistory::fromJson(str);
        return deviceSegmentHistory;
}

static int getRecentyInMinute()
{

        int value = 60 * 24;//one day
        assertAndThrow (value != AS_RECORD_DEFAULT_TTL &&
                        value != AS_RECORD_NO_EXPIRE_TTL);

        return value;
}
};


template <>
class AeroCacheTraits <AdHistory> {
public:
static std::string getEntityName()
{
        return "AdHistory";
}
static std::string getNamespaceName()
{
        return "AdHistory";
}
static std::string getSetName()
{
        return "AdHistory";
}

static std::string getBinName()
{
        return "key1";
}

static std::string getKey(AdHistory* value)
{
        NULL_CHECK(value);
        return value->device->getDeviceId();
}

static std::string getBinValue(AdHistory* value)
{
        if (value == nullptr) {
                return "";
        }
        return value->toJson();
}
static std::shared_ptr<AdHistory> getObjectFromStr(std::string str) {
        auto adHistory = AdHistory::fromJson(str);
        return adHistory;
}

static int getRecentyInMinute()
{

        int value = 10; //ten minutes
        assertAndThrow (value != AS_RECORD_DEFAULT_TTL &&
                        value != AS_RECORD_NO_EXPIRE_TTL);

        return value;
}
};

#include "EventLog.h"
template <>
class AeroCacheTraits <EventLog> {
public:

static std::string getEntityName()
{
        return "EventLog";
}
static std::string getNamespaceName()
{
        return "EventLog";
}
static std::string getSetName()
{
        return "EventLog";
}

static std::string getBinName()
{
        return "key1";
}

static std::string getKey(EventLog* value)
{
        if (value == nullptr) {
                EXIT("EventLog is null");
        }
        return value->eventId;
}

static std::string getBinValue(EventLog* value)
{
        if (value == nullptr) {
                EXIT("EventLog is null");
        }
        return value->toJson();
}
static std::shared_ptr<EventLog> getObjectFromStr(std::string str) {
        auto eventLog = EventLog::fromJson(str);
        return eventLog;
}

static int getRecentyInMinute()
{

        int value = 10;
        assertAndThrow (value != AS_RECORD_DEFAULT_TTL &&
                        value != AS_RECORD_NO_EXPIRE_TTL);

        return value;
}
};

#include "Feature.h"
template <>
class AeroCacheTraits <Feature> {
public:

static std::string getEntityName()
{
        return "Feature";
}
static std::string getNamespaceName()
{
        return "Feature";
}
static std::string getSetName()
{
        return "Feature";
}

static std::string getBinName()
{
        return "key1";
}

static std::string getKey(Feature* value)
{
        if (value == nullptr) {
                EXIT("Feature is null");
        }
        std::string featureKeyInMap = value->type + "-" + value->name;
        return featureKeyInMap;
}

static std::string getBinValue(Feature* value)
{
        if (value == nullptr) {
                EXIT("Feature is null");
        }
        return value->toJson();
}
static std::shared_ptr<Feature> getObjectFromStr(std::string str) {
        auto feature = Feature::fromJson(str);
        return feature;
}

static int getRecentyInMinute()
{

        int value = 60 * 24 * 365; //one year
        assertAndThrow (value != AS_RECORD_DEFAULT_TTL &&
                        value != AS_RECORD_NO_EXPIRE_TTL);

        return value;
}
};

#include "EventLock.h"
template <>
class AeroCacheTraits <EventLock> {
public:

static std::string getEntityName()
{
        return "EventLock";
}
static std::string getNamespaceName()
{
        return "EventLock";
}
static std::string getSetName()
{
        return "EventLock";
}

static std::string getBinName()
{
        return "key1";
}

static std::string getKey(EventLock* value)
{
        if (value == nullptr) {
                EXIT("EventLock is null");
        }
        std::string key = value->eventId + "-" + value->eventType+"-lock";
        return key;
}

static std::string getBinValue(EventLock* value)
{
        if (value == nullptr) {
                EXIT("EventLock is null");
        }
        return value->toJson();
}
static std::shared_ptr<EventLock> getObjectFromStr(std::string str) {
        auto eventLock = EventLock::fromJson(str);
        return eventLock;
}

static int getRecentyInMinute()
{

        int value = 4;
        assertAndThrow (value != AS_RECORD_DEFAULT_TTL &&
                        value != AS_RECORD_NO_EXPIRE_TTL);

        return value;
}
};

#include "PlacesCachedResult.h"
template <>
class AeroCacheTraits <PlacesCachedResult> {
public:

static std::string getEntityName()
{
        return "PlacesCachedResult";
}
static std::string getNamespaceName()
{
        return "PlacesCachedResult";
}
static std::string getSetName()
{
        return "PlacesCachedResult";
}

static std::string getBinName()
{
        return "key1";
}

static std::string getKey(PlacesCachedResult* value)
{
        if (value == nullptr) {
                EXIT("PlacesCachedResult is null");
        }
        std::string key = value->mgrs10m + "-" + _toStr(value->maxNumebrOfWantedFeatures);
        return key;
}

static std::string getBinValue(PlacesCachedResult* value)
{
        if (value == nullptr) {
                EXIT("PlacesCachedResult is null");
        }
        return value->toJson();
}
static std::shared_ptr<PlacesCachedResult> getObjectFromStr(std::string str) {
        return PlacesCachedResult::fromJson(str);
}

static int getRecentyInMinute()
{

        int value = 60 * 24 * 3; //3 days
        assertAndThrow (value != AS_RECORD_DEFAULT_TTL &&
                        value != AS_RECORD_NO_EXPIRE_TTL);

        return value;
}
};


template <>
class AeroCacheTraits <CrossWalkedDeviceMap> {
public:

static std::string getEntityName()
{
        return "CrossWalkedDeviceMap";
}
static std::string getNamespaceName()
{
        return "CrossWalkedDeviceMap";
}
static std::string getSetName()
{
        return "CrossWalkedDeviceMap";
}

static std::string getBinName()
{
        return "key1";
}

static std::string getKey(CrossWalkedDeviceMap* value)
{
        if (value == nullptr) {
                EXIT("CrossWalkedDeviceMap is null");
        }
        std::string key = value->sourceDevice->getDeviceId();
        return key;
}

static std::string getBinValue(CrossWalkedDeviceMap* value)
{
        if (value == nullptr) {
                EXIT("CrossWalkedDeviceMap is null");
        }
        return value->toJson();
}
static std::shared_ptr<CrossWalkedDeviceMap> getObjectFromStr(std::string str) {
        return CrossWalkedDeviceMap::fromJson(str);
}

static int getRecentyInMinute()
{

        int value = 60 * 24 * 7; //7 days
        assertAndThrow (value != AS_RECORD_DEFAULT_TTL &&
                        value != AS_RECORD_NO_EXPIRE_TTL);

        return value;
}
};


#endif
