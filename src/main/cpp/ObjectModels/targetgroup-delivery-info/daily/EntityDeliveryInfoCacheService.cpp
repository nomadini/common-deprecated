#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "EntityDeliveryInfoCacheService.h"
#include "JsonArrayUtil.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonMapUtil.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashMap.h"

EntityDeliveryInfoCacheService::EntityDeliveryInfoCacheService(
        EntityToModuleStateStats* entityToModuleStateStats)  : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->allTgRealTimeDeliveryInfoMap = std::make_shared<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo > >(entityToModuleStateStats);
        this->allCampaignRealTimeDeliveryInfoMap = std::make_shared<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo > >(entityToModuleStateStats);
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo> > EntityDeliveryInfoCacheService::getAllTgRealTimeDeliveryInfoMap() {
        return allTgRealTimeDeliveryInfoMap;
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo> > EntityDeliveryInfoCacheService::getAllCampaignRealTimeDeliveryInfoMap() {
        return allCampaignRealTimeDeliveryInfoMap;
}

bool EntityDeliveryInfoCacheService::isRealTimeInfoOldForTg(int targetGroupId) {
        auto tgRealTimeDeliveryInfo = allTgRealTimeDeliveryInfoMap->getOptional(targetGroupId);
        if (tgRealTimeDeliveryInfo == nullptr) {
                return true;
        }
        return false;
}

std::string EntityDeliveryInfoCacheService::processReadAllByClient(std::string requestBody, int sizeOfMap) {
        auto responseToClient  = JsonMapUtil::convertMapValuesToJsonArrayString (*allTgRealTimeDeliveryInfoMap);
        MLOG(3)<<"sending response to requester : "<<std::endl<<
                responseToClient;
        return responseToClient;
}
