#ifndef ObjectGlobalMapContainer_H
#define ObjectGlobalMapContainer_H

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "AtomicBoolean.h"
#include "DateTimeUtil.h"

class EntityToModuleStateStats;

class ObjectGlobalMapContainer {

public:
TimeType lastTimeThreadRan;

static std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> > > globalMap;;

static std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> > > getMap();
static TimeType getWhenAppStarted();
static bool checkMemoryLeakUponCreationProperty;

std::shared_ptr<gicapods::AtomicBoolean> isAllowedToRun;
std::shared_ptr<gicapods::AtomicBoolean> threadIsFinished;


static long getMaxLimitOfObjects();
static void setMaxLimitOfObjects(long maxLimitOfObjects);
static long maxLimitOfObjects;

static long getMaxLimitOfMapEntries();
static void setMaxLimitOfMapEntries(long maxLimitOfMapEntries);
static long maxLimitOfMapEntries;
static void checkMemoryLeakUponCreation(std::string className, long numberOfObjectsCreated);

void checkMemoryLeakStatusThread();

void checkMemoryLeakStatus();

ObjectGlobalMapContainer();
virtual ~ObjectGlobalMapContainer();

};



#endif //COMMON_Object_H
