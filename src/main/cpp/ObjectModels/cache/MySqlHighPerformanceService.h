//
// Created by Mahmoud Taabodi on 7/17/16.
//

#ifndef MySqlHighPerformanceService_H
#define MySqlHighPerformanceService_H

#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Object.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "MySqlDriver.h"
#include "GUtil.h"
#include "ConcurrentHashMap.h"
#include <boost/exception/all.hpp>
#include "ConcurrentQueueFolly.h"
namespace gicapods { template <typename K, typename V> class ConcurrentHashMap; }
class EntityToModuleStateStats;
#include "MySqlHighPerfTraits.h"
#include "MySqlService.h"

/*
   this class helps us queue thousands of entities
   in a short amount of time and write the unique ones in mysql
   we use it with MysqlBadDeviceIdService, in order to write all the bad devices
   in mysql
 */
template<class K, class V>
class MySqlHighPerformanceService {

public:

std::shared_ptr<ConcurrentQueueFolly<V> > entityCreatedQueueManager;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, V> > entityCreatedMapWrapper;
EntityToModuleStateStats* entityToModuleStateStats;
MySqlService<K, V>* mySqlService;
int queueLimit;
typedef MySqlHighPerfTraits<V> Traits;


MySqlHighPerformanceService(
        MySqlDriver* driver,
        EntityToModuleStateStats* entityToModuleStateStats,
        int queueLimit) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        entityCreatedQueueManager = std::make_shared<ConcurrentQueueFolly<V> >();
        entityCreatedMapWrapper = std::make_shared<gicapods::ConcurrentHashMap<std::string, V> >(
                this->entityToModuleStateStats
                );

        this->queueLimit = queueLimit;
        assertAndThrow(this->queueLimit >= 10);
}

~MySqlHighPerformanceService() {

}


void queueForPersistence(std::shared_ptr<V> entity) {
        if(!entityCreatedMapWrapper->exists(Traits::getKey(entity))) {
                if (entityCreatedMapWrapper->size() > 1000) {
                        MLOG(3)<<"clearing the entity map ";
                        entityCreatedMapWrapper->clear();
                        entityToModuleStateStats->addStateModuleForEntity(
                                "CLEAR_FULL_ENTITY_MAP",
                                "MySqlHighPerformanceService",
                                "ALL"
                                );
                }
                MLOG(3)<<"inserting a entity in map and queue , "<<
                        entity->toString();
                entityToModuleStateStats->addStateModuleForEntity(
                        "INSERT_ENTITY_IN_QUEUE",
                        "MySqlHighPerformanceService",
                        Traits::getKey(entity)
                        );

                entityCreatedMapWrapper->put(Traits::getKey(entity), entity);
                entityCreatedQueueManager->enqueueEntity(entity);

                entityToModuleStateStats->addStateModuleForEntity(
                        "ENTITY_QUEUED_FOR_MYSQL_INSERTION",
                        "MySqlHighPerformanceService",
                        Traits::getKey(entity));

                if (entityCreatedQueueManager->getEntityQueue()->unsafe_size() > queueLimit) {
                        //if queue size is more than 100, we invoke the insert manually
                        entityToModuleStateStats->addStateModuleForEntity(
                                "ENTITY_QUEUED_OVER_LIMIT_INSERTION_MANUALLY",
                                "MySqlHighPerformanceService",
                                "ALL");

                        insertFromQueue();
                }

        } else {
                entityToModuleStateStats->addStateModuleForEntity(
                        "ENTITY_ALREADY_IN_MAP",
                        "MySqlHighPerformanceService",
                        Traits::getKey(entity)
                        );
                MLOG(2)<<"entity is already in map , "<<Traits::getKey(entity);
        }

}

//we should call this method from a thread that runs every minute....

void insertFromQueue() {
        while (!entityCreatedQueueManager->getEntityQueue()->empty()) {
                try {
                        boost::optional<std::shared_ptr<V> > entity = entityCreatedQueueManager->dequeueEntity ();
                        if (entity == boost::none) {
                                continue;
                        }
                        MLOG(3) << "inserting entity : " << (*entity)->toJson();
                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "INSERT_ENTITY_IN_MYSQL",
                                "MySqlHighPerformanceService",
                                Traits::getKey(*entity));

                        mySqlService->insertIfNotExisting (Traits::getKey(*entity), *entity);

                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "INSERT_ENTITY_IN_MYSQL",
                                "MySqlHighPerformanceService",
                                "ALL",
                                EntityToModuleStateStats::important);

                } catch (std::exception const &e) {
                        LOG(ERROR) << "error happening building models  " << boost::diagnostic_information (e);
                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "EXCEPTION_IN_INSERT_ENTITY_IN_MYSQL",
                                "MySqlHighPerformanceService",
                                "ALL",
                                EntityToModuleStateStats::exception);


                } catch (...) {
                        LOG(ERROR) << "unknown error happening putting msg in kafka queue ";
                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "UNKNOWN_EXCEPTION_IN_INSERT_ENTITY_IN_MYSQL",
                                "MySqlHighPerformanceService",
                                "ALL",
                                EntityToModuleStateStats::exception);
                }
        }
}

};

#endif //COMMON_MySqlHighPerformanceService_H
