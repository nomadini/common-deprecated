//
// Created by Mahmoud Taabodi on 7/17/16.
//

#ifndef EntityProviderService_H
#define EntityProviderService_H

#include <memory>
#include <string>
#include <vector>
#include <set>

#include "HttpUtilService.h"
#include "DataProvider.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "ConcurrentHashMap.h"
#include "Object.h"
class EntityToModuleStateStats;

/*
   provides a general interface to all templated EntityProviderService<T> classes
   for making it easy to work with them
 */
class EntityProviderServiceInterface {
public:

virtual void reloadCaches() = 0;
virtual void clearCaches() = 0;
virtual std::string getEntityName() = 0;
virtual std::string processReadAllCommand(std::string requestBody,
                                          int& numberOfEntities) = 0;
virtual std::string processReadHashOfAll() = 0;
};

template<class T>
class EntityProviderService : public EntityProviderServiceInterface {

std::string entityName;
private:
//these are the data containers that this cache provides out of the box
std::shared_ptr<std::unordered_map<int, std::shared_ptr<T> > > allEntitiesMap;
std::shared_ptr<std::vector<std::shared_ptr<T> > > allEntities;


public:
std::string hashOfLoadedDataFromDb;
TimeType lastTimeDataWasReloadedFromDbInSeconds;
int minReloadDataFromDbIntervalInSeconds;
DataProvider<T>* dataProvider;
EntityToModuleStateStats* entityToModuleStateStats;

EntityProviderService(
        DataProvider<T>* dataProvider,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) {
        NULL_CHECK(entityToModuleStateStats);
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->dataProvider = dataProvider;
        allEntitiesMap = std::make_shared<std::unordered_map<int, std::shared_ptr<T> > >();

        allEntities = std::make_shared<std::vector<std::shared_ptr<T> > >();
        this->entityName = T::getEntityName();
        minReloadDataFromDbIntervalInSeconds = 30;
        //we set the value to some time in the past to make the first run , reload data from db
        lastTimeDataWasReloadedFromDbInSeconds = DateTimeUtil::getNowInSecond() - 1000;
}


std::string getEntityName() {
        return entityName;
}


std::string processReadAllCommand(std::string requestBody,
                                  int& numberOfEntities) {
        auto json = getAllAsJson();
        numberOfEntities = allEntitiesMap->size();
        return json;
}


std::string processReadHashOfAll() {
        return hashOfLoadedDataFromDb;
}


std::string getAllAsJson() {
        auto map = *allEntitiesMap;
        entityToModuleStateStats->addStateModuleForEntityWithValue (getEntityName() + "_map_size",
                                                                    map.size(),
                                                                    "EntityProviderService_Entities_Loaded",
                                                                    "ALL");
        auto responseToClient  = JsonMapUtil::convertMapValuesToJsonArrayString (map);
        MLOG(3)<<"sending response to client : "<<std::endl<<
                responseToClient;
        return responseToClient;
}


std::shared_ptr<std::vector<std::shared_ptr<T> > > parseResponse(std::string reponseInJson) {
        /*
           we have a method  JsonArrayUtil::convertStringArrayToArrayOfObject<T>(reponseInJson) here.
           but we dont use it here becuase in that case, we have to include every single T.h in JsonArrayUtil.cpp
         */
        std::shared_ptr<std::vector<std::shared_ptr<T> > >
        objectsParsed = std::make_shared<std::vector<std::shared_ptr<T> > >();

        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++) // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertAndThrow(obj.IsObject ());
                auto entity = T::fromJsonValue (obj);
                objectsParsed->push_back (entity);
        }

        return objectsParsed;
}


~EntityProviderService() {

}

/*
   this method exits the program in case of any error
   only DataMaster should call it
 */

void reloadCaches() {
        auto nowInSeconds = DateTimeUtil::getNowInSecond();
        auto timeDiffInSeconds = nowInSeconds - lastTimeDataWasReloadedFromDbInSeconds;
        if (timeDiffInSeconds < minReloadDataFromDbIntervalInSeconds) {
                MLOG(2)<<"aborting the reloading because, data was reloaded recently"
                       <<" timeDiffInSeconds "
                       << timeDiffInSeconds
                       <<", minReloadDataFromDbIntervalInSeconds "
                       <<minReloadDataFromDbIntervalInSeconds;
                return;
        }

        try {
                MLOG(3) << "loading entity : "<< getEntityName() << " from mysql";

                std::vector<std::shared_ptr<T> > allEntitiesFromDB =
                        dataProvider->readAllEntities ();
                if (allEntitiesFromDB.empty()) {
                        LOG(ERROR) << "loaded 0 entties for ......" << getEntityName();
                }

                std::string oldJsonData = getAllAsJson();
                clearCaches ();

                populateMapsAndLists(allEntitiesFromDB);
                std::string newJsonData = getAllAsJson();
                auto newHash = StringUtil::hashStringBySha1(newJsonData);
                if(!StringUtil::equalsCaseSensitive(newHash, hashOfLoadedDataFromDb)) {
                        LOG(ERROR) << getEntityName() << " Data Changed : hashes are different..";
                        // LOG(ERROR)<<"newHash : "<<newHash<< " , hashOfLoadedDataFromDb : "<<hashOfLoadedDataFromDb;
                        // LOG(ERROR) << "diff of strings : "<<StringUtil::getDifference(oldJsonData, newJsonData);
                } else {
                        LOG(ERROR) << getEntityName() << " : No Data Changed ";
                        // LOG(ERROR) <<"..the same hash is calculated : "<< hashOfLoadedDataFromDb;
                }
                hashOfLoadedDataFromDb = newHash;
        } catch(std::exception const &e) {

                entityToModuleStateStats->
                addStateModuleForEntity("ERROR_IN_GETTING_DATA_FROM_DB",
                                        "EntityProviderService" + getEntityName(),
                                        EntityToModuleStateStats::all,
                                        EntityToModuleStateStats::exception
                                        );

                gicapods::Util::showStackTrace(&e);
                //this method should be called only by DataMaster
                //and by exiting here we make sure that we dont load junk data, aka incomplete data into the system
                exit(1);
        }

        lastTimeDataWasReloadedFromDbInSeconds = DateTimeUtil::getNowInSecond();
}


void populateMapsAndLists(std::vector<std::shared_ptr<T> > allEntitiesFromDB) {
        LOG(INFO) << entityName << " : "<< allEntitiesFromDB.size () << " were loaded.";

        for(auto entity :  allEntitiesFromDB) {
                MLOG(2) << "entity loaded  " << entity->toJson();
                this->allEntitiesMap->insert (std::make_pair (entity->id, entity));
                this->allEntities->push_back(entity);
        }
        LOG(INFO) << "size of allEntitiesMap is " << allEntitiesMap->size ();
        assertAndThrow(allEntitiesFromDB.size () == allEntitiesMap->size());
        entityToModuleStateStats->addStateModuleForEntity (getEntityName() + "_AllEntitiesMap_SIZE_AFTER_POPULATION:" +
                                                           StringUtil::toStr(allEntitiesMap->size()),
                                                           "EntityProviderService",
                                                           "ALL");

        //this is  a strict check for now, to make sure all the entities are loaded for testing
        //and all the caches are populated, TODO : later remove it
        if(this->allEntitiesMap->empty() || this->allEntities->empty()) {
                LOG(WARNING) << "warning in populating entity : " << getEntityName()
                             <<" all entities map is empty...no data populated!";

                entityToModuleStateStats->addStateModuleForEntity (getEntityName() + "_AllEntitiesMap_IS_EMPTY_AFTER_POPULATION",
                                                                   "EntityProviderService",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                assertAndWarn(!this->allEntitiesMap->empty());
        }
}


void clearCaches() {
        this->allEntitiesMap->clear ();
        this->allEntities->clear();
}
};

#endif //COMMON_CACHESERVICE_H
