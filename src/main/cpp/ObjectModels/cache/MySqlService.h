//
// Created by Mahmoud Taabodi on 7/17/16.
//

#ifndef MySqlService_H
#define MySqlService_H
#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <cppconn/resultset.h>
#include "MySqlDriver.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "ResultSetHolder.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>

#include "EntityToModuleStateStats.h"

template<class K, class T>
class MySqlService : public Object {

public:
MySqlDriver* driver;
EntityToModuleStateStats* entityToModuleStateStats;
virtual std::shared_ptr<T> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) = 0;
virtual std::string getSelectAllQueryStatement() = 0;
virtual std::string getReadByIdSqlStatement(K id) = 0;
virtual std::string getInsertObjectSqlStatement(std::shared_ptr<T> object) = 0;

MySqlService(MySqlDriver* driver,
             EntityToModuleStateStats* entityToModuleStateStats)  : Object(__FILE__) {
        this->driver = driver;
        this->entityToModuleStateStats = entityToModuleStateStats;
}

~MySqlService() {

}


void insert(std::shared_ptr<T> object) {

        MLOG(2) << "inserting new object in db : " << object->toJson ();
        std::string query = getInsertObjectSqlStatement(object);

        driver->executedUpdateStatement (query);
        object->id = driver->getLastInsertedId ();

        MLOG(2) << "object id inserted in db is  : " << object->id;

}


std::vector<std::shared_ptr<T> > readAll() {

        std::vector<std::shared_ptr<T> > allObjects;

        std::string query = getSelectAllQueryStatement();

        auto res = driver->executeQuery(query);
        while (res->next ()) {
                allObjects.push_back (mapResultSetToObject(res));
        }


        return allObjects;
}

// vector<tuple<int,double,char>> vt =
//     { std::make_tuple(1,0.1,'a'), std::make_tuple(2,4.2,'b') };

std::vector<std::shared_ptr<T> > readAllByColumns(
        std::vector<std::tuple<std::string, std::string, std::string> > allParams) {
        std::vector<std::shared_ptr<T> > allObjects;
        std::string query = "SELECT "
                            " id,"
                            " updated_at "
                            " FROM campaign ";

        std::string whereClause = " where ";
        int numberOfParams = allParams.size();
        int numberOfParamsProcessed = 0;
        for(auto param : allParams) {

                whereClause += std::get<0>(param) + " = ";

                if (StringUtil::equalsIgnoreCase(std::get<2>(param), "string")) {
                        whereClause += "'";
                }

                whereClause += std::get<1>(param);

                if (StringUtil::equalsIgnoreCase(std::get<2>(param), "string")) {
                        whereClause += "'";
                }

                numberOfParamsProcessed++;
                if (numberOfParamsProcessed == numberOfParams) {
                        //do nothing...
                } else {
                        whereClause += " AND ";
                }
        }
        std::string finalQuery = query + whereClause;
        LOG(ERROR) << "finalQuery : " <<finalQuery;

        return allObjects;
}


std::shared_ptr<T>  readById(K id) {
        auto obj = readByIdOptional(id);
        if (obj == nullptr) {
                obj = createSharedInstance();
        }
        return obj;
}


std::shared_ptr<T>  readByIdOptional(K id) {
        std::shared_ptr<T> obj = nullptr;

        std::string query = getReadByIdSqlStatement(id);

        auto res = driver->executeQuery (query);
        while (res->next ()) {
                obj = mapResultSetToObject(res);
        }

        return obj;
}

void insertIfNotExisting(K key, std::shared_ptr<T> value) {

        if (readByIdOptional(key) == nullptr) {
                insert(value);
        } else {
                NULL_CHECK(entityToModuleStateStats);
                entityToModuleStateStats->addStateModuleForEntity("ALREADY_EXISTS_IN_MYSQL",
                                                                  "MySqlService",
                                                                  "ALL");//TODO : add trait to add name here
        }

}


std::shared_ptr<T> createSharedInstance() {
        return std::make_shared<T>();
}

};

#endif //COMMON_MySqlService_H
