/*
 * AeroCacheService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef MySqlHighPerfTraits_H_
#define MySqlHighPerfTraits_H_


#include "DeviceSegmentHistory.h"
#include "Feature.h"
#include "Segment.h"

template <class T>
class MySqlHighPerfTraits
{

public:
static std::string getKey(std::shared_ptr<T> value)
{
        throwEx("not implemented");
}

};

template <>
class MySqlHighPerfTraits <Segment> {
public:
static std::string getKey(std::shared_ptr<Segment> value)
{
        return value->getUniqueName();
}
};

template <>
class MySqlHighPerfTraits <Feature> {
public:
static std::string getKey(std::shared_ptr<Feature> value)
{
        return value->name;
}
};


#endif
