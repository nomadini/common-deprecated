#include "GUtil.h"
#include "MySqlHourlyCompressor.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include "JsonArrayUtil.h"
#include "MySqlMetricService.h"

template<class T>
MySqlHourlyCompressor<T>::MySqlHourlyCompressor(
        MySqlDriver* driver,
        MySqlDriver* desitantionDriver) : Object(__FILE__) {
        NULL_CHECK(driver);
        NULL_CHECK(desitantionDriver);
        this->driver = driver;
        this->desitantionDriver = desitantionDriver;

        compressImpressionTableName = traits.getCompressedTableName();

        coreFieldsToRead = traits.getCoreFieldsToRead();


        coreFieldsForInserts = traits.getCoreFieldsToInsert();


        groupByFields = traits.getGroupByFields();


        coreReplacementFields = traits.getCoreReplacementFields();

}

template<class T>
void MySqlHourlyCompressor<T>::deleteCurrentHourOfImpressedInfo() {
        auto query = " delete from " + traits.getCompressedTableName() +
                     " where created_at >= '" + DateTimeUtil::getNowPlusHoursInMySqlFormat(-1) + "'";
        desitantionDriver->executedUpdateStatement (query);
}
template<class T>
void MySqlHourlyCompressor<T>::compressThread() {
        // LOG(ERROR)<< "disabling for now";
        // return;
        // while(true) {
        //read last 12 hour of data
        try {
                std::set<std::string>  alreadyCompressedDates = readAllCurrentDatesAlreadyCompressed();
                MLOG(10) << "alreadyCompressedDates : "<< JsonArrayUtil::convertListToJson(alreadyCompressedDates);

                //this is an expensive call...takes about 30 seconds
                std::vector<std::shared_ptr<T> > entities =
                        readLastNHoursOfDataAsCompressed(-3, alreadyCompressedDates);
                MLOG(10) << entities.size() << " entities to insert into compressed table ";
                //we delete, current hour, and re insert it , to get the latest data
                deleteCurrentHourOfImpressedInfo();
                deleteOlderThanNHoursAgoOfOriginalInfo(3);
                insertInCompressedTable(entities);
        } catch(std::exception &e) {
                gicapods::Util::showStackTrace(&e);
        }
        //we set it to 10 seconds for debugging
        //         gicapods::Util::sleepViaBoost(_L_, 10);
        // }
}
template<class T>
std::set<std::string> MySqlHourlyCompressor<T>::readAllCurrentDatesAlreadyCompressed() {
        auto query = " select distinct created_at from " + traits.getCompressedTableName();
        auto res = desitantionDriver->executeQuery (query);

        std::set<std::string> allDateTimes;
        while (res->next ()) {
                auto dateCompressed = DateTimeUtil::dateTimeToStr(MySqlDriver::parseDateTime(res, "created_at"));
                allDateTimes.insert (dateCompressed);
        }
        return allDateTimes;
}

template<class T>
std::vector<std::shared_ptr<T> > MySqlHourlyCompressor<T>::readLastNHoursOfDataAsCompressed(
        int hourStartPeriod,
        std::set<std::string> alreadyCompressedDates
        ) {
        auto query = " SELECT " + coreFieldsToRead +
                     " FROM "+ traits.getTableName() +" where " +
                     " created_at >= '" + DateTimeUtil::getNowPlusHoursInMySqlFormat(hourStartPeriod) + "'"
                     " and created_at <= '" + DateTimeUtil::getNowPlusHoursInMySqlFormat(0) + "'"
                     +" group by " + groupByFields +
                     "  , YEAR(created_at), MONTH(created_at), DAY(created_at), HOUR(created_at) ";

        auto res = driver->executeQuery (query);

        std::vector<std::shared_ptr<T> > entities;
        while (res->next ()) {
                auto impEvent = mapResultSetToObject(res);
                auto hourToInsertDataFor = DateTimeUtil::dateTimeToStr(impEvent->createdAt);
                if (!CollectionUtil::containsInList(hourToInsertDataFor, alreadyCompressedDates)) {
                        MLOG(10) << "inserting data compressed for hour : "<<hourToInsertDataFor;
                        entities.push_back (impEvent);
                } else {
                        MLOG(10) << "ignoring data compressed for hour : "<<hourToInsertDataFor;
                }


        }
        return entities;
}


template<class T>
std::shared_ptr<T> MySqlHourlyCompressor<T>::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        auto entity = getResultFromObject(res);
        //we truncate the dates to hour, in order to know up until what hours we have compressed, later
        entity->createdAt =
                DateTimeUtil::truncateDateToHourPercision(entity->createdAt);
        entity->updatedAt =
                DateTimeUtil::truncateDateToHourPercision(entity->updatedAt);

        MLOG(10) << "reading "<< traits.getTableName() <<" from mysql " << entity->toJson ();
        return entity;
}
template<class T>
void MySqlHourlyCompressor<T>::insertInCompressedTable(std::vector<std::shared_ptr<T> > entities) {
        if (entities.empty()) {
                return;
        }
        MLOG(10) << "coreFieldsForInserts : " <<
                coreFieldsForInserts;
        std::string queryStr =
                "INSERT INTO " + compressImpressionTableName +
                " ("
                + coreFieldsForInserts +
                " ) VALUES __VALUES_OF_ALLROW__  ;";

        int maxSizeOfBatchForInflux = 100;
        int totalEventsInBatches = 0;
        for (int totalIndex=0; totalIndex < entities.size(); ) {

                std::vector<std::string > batch;
                while (batch.size() < maxSizeOfBatchForInflux && totalIndex < entities.size()) {

                        if (totalIndex < entities.size()) {
                                auto oneRecordValues = convertRecordToValueString(entities.at(totalIndex));
                                batch.push_back(oneRecordValues);
                        }
                        totalIndex++;
                }

                auto properAllValues = StringUtil::joinArrayAsString(batch, ",");
                queryStr = StringUtil::replaceString (queryStr, "__VALUES_OF_ALLROW__", properAllValues);
                desitantionDriver->executedUpdateStatement (queryStr);
                totalEventsInBatches += batch.size();
        }

        LOG_EVERY_N(INFO, 1) << "recording " << totalEventsInBatches << " metrics in compressed table" << traits.getCompressedTableName();


}

template<class T>
std::string MySqlHourlyCompressor<T>::convertRecordToValueString(std::shared_ptr<T> entity) {
        throwEx("implemnet this function");
}

template<class T>
std::shared_ptr<T> MySqlHourlyCompressor<T>::getResultFromObject(std::shared_ptr<ResultSetHolder> res) {
        throwEx("implement this function");
}

template<class T>
void MySqlHourlyCompressor<T>::deleteOlderThanNHoursAgoOfOriginalInfo(int nHoursToDelete) {
        //this is a no-op by defaul
}

template<>
void MySqlHourlyCompressor<MetricDbRecord>::deleteOlderThanNHoursAgoOfOriginalInfo(int nHoursToDelete) {
        // we delete all the original metrics that are older than n hours agos
        auto query =
                "DELETE FROM "+ traits.getTableName()
                +" where " +
                " created_at <= '" + DateTimeUtil::getNowPlusHoursInMySqlFormat(-1 * nHoursToDelete) + "'";

        driver->executedUpdateStatement (query);
}

template<>
std::shared_ptr<MetricDbRecord> MySqlHourlyCompressor<MetricDbRecord>::getResultFromObject(std::shared_ptr<ResultSetHolder> res) {
        return MySqlMetricService::mapResultSetToObject(res);
}

template<>
std::string MySqlHourlyCompressor<MetricDbRecord>::
convertRecordToValueString(std::shared_ptr<MetricDbRecord> entity) {

        return MySqlMetricService::convertRecordToValueString(entity);
}

template<class T>
MySqlHourlyCompressor<T>::~MySqlHourlyCompressor() {
}

#include "MetricDbRecord.h"
template class MySqlHourlyCompressor<MetricDbRecord>;
