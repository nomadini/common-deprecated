
#include "Object.h"
#include "EntityToModuleStateStats.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "AtomicLong.h"
#include "CollectionUtil.h"
#include "ObjectGlobalMapContainer.h"

std::set<std::string> Object::highRateClasses;

Object::Object(std::string nameOfObject) {
        if (highRateClasses.empty()) {
                highRateClasses.insert("GlobalWhiteListEntry");
                highRateClasses.insert("GeoFeature");
                highRateClasses.insert("Feature");
        }
        assertAndThrow(!nameOfObject.empty());
        this->nameOfObject = nameOfObject;
        increment();
}

std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> > >
Object::getMap() {
        return ObjectGlobalMapContainer::getMap();
}
Object::~Object() {
        decrement();
}

void Object::increment() {
        std::string className = nameOfObject;
        tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> >::accessor accessor;
        auto found = getMap()->find(accessor, className);
        if (!found) {
                getMap()->insert(accessor, className);
                accessor->second = std::make_shared<gicapods::AtomicLong>();
        }

        accessor->second->increment();

        ObjectGlobalMapContainer::checkMemoryLeakUponCreation(className, accessor->second->getValue());
}

void Object::decrement() {
        std::string className = nameOfObject;
        try {

                tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> >::accessor accessor;
                if (getMap() != nullptr //this might happen when app is shutting down
                    && getMap()->find(accessor, className)) {
                        accessor->second->decrement();
                        // LOG_EVERY_N(ERROR, 1) << "classname : "<< className << " value decremented to : "<<accessor->second->getValue();
                        return;
                }

                //  throwEx("cannot decrement an unallocated object for class : " + className);
                LOG(ERROR)<<"cannot decrement an unallocated object for class : " <<className;
        } catch(...) {
                LOG(ERROR)<<"error in desctructor, cannot decrement an unallocated object for class : " <<className;
        }
}

void Object::printStats() {
        int maxNumberOfObjects = 500;
        for (auto pair : *getMap()) {
                if (pair.second->getValue() >= maxNumberOfObjects) {
                        LOG(ERROR) << "classname : "<< pair.first << " value : "<<pair.second->getValue();
                        // entityToModuleStateStats->addStateModuleForEntity("LEAK_SPOTTED-For-" + pair.first
                        //                                                   + " " +_toStr(maxNumberOfObjects),
                        //                                                   "Object",
                        //                                                   "ALL",
                        //                                                   EntityToModuleStateStats::warning);
                }
        }
}
