#include "RecentVisitHistoryScoreCard.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "TgScoreEligibilityRecord.h"

RecentVisitHistoryScoreCard::RecentVisitHistoryScoreCard () : Object(__FILE__) {
								this->targetGroupIdToQualification = std::make_shared<std::unordered_map<int, std::shared_ptr<TgScoreEligibilityRecord> > >();

}

RecentVisitHistoryScoreCard::~RecentVisitHistoryScoreCard() {

}

void RecentVisitHistoryScoreCard::addPropertiesToJsonValue(RapidJsonValueType parentValue, DocumentType* doc) {
								RapidJsonValueTypeNoRef arrayValue(rapidjson::kArrayType);
								arrayValue.SetArray();
								for (auto& kvPair : *targetGroupIdToQualification) {
																RapidJsonValueTypeNoRef objValue (rapidjson::kObjectType);
																kvPair.second->addPropertiesToJsonValue(objValue, doc);
																arrayValue.PushBack (objValue, doc->GetAllocator());
								}

								RapidJsonValueTypeNoRef nameOfArrayValue ("targetGroupIdToQualification", doc->GetAllocator ());

								parentValue.AddMember (nameOfArrayValue, arrayValue, doc->GetAllocator ());
}

std::string RecentVisitHistoryScoreCard::toJson() {
								auto doc = JsonUtil::createDcoumentAsObjectDoc();
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}

std::shared_ptr<RecentVisitHistoryScoreCard> RecentVisitHistoryScoreCard::fromJsonValue(RapidJsonValueType value) {

								auto model = std::make_shared<RecentVisitHistoryScoreCard>();

								std::vector< std::shared_ptr<TgScoreEligibilityRecord> > allModelRecords =
																JsonArrayUtil::getArrayOfObjectsFromMemberInValue
																<TgScoreEligibilityRecord>(value, "targetGroupIdToQualification");

								for(auto modelRecord : allModelRecords) {
																model->targetGroupIdToQualification->insert(
																								std::make_pair(modelRecord->targetGroupId, modelRecord));
								}

								return model;

}

std::shared_ptr<RecentVisitHistoryScoreCard> RecentVisitHistoryScoreCard::fromJson(std::string jsonString) {
								if (jsonString.empty()) {
																auto obj = std::make_shared<RecentVisitHistoryScoreCard>();
																return obj;
								}
								auto doc = JsonUtil::parseJsonSafely(jsonString);
								return RecentVisitHistoryScoreCard::fromJsonValue(*doc);
}
