#include "StringHolder.h"
#include "JsonUtil.h"

#include "GUtil.h"
StringHolder::StringHolder ()  : Object(__FILE__) {
}

StringHolder::~StringHolder() {

}


void StringHolder::addPropertiesToJsonValue(RapidJsonValueType valueArg, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair (doc, "value", value, valueArg);
}

std::shared_ptr<StringHolder> StringHolder::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<StringHolder> feature =
																std::make_shared<StringHolder>();
								feature->value = JsonUtil::GetStringSafely(value, "value");
								return feature;
}

std::shared_ptr<StringHolder> StringHolder::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

std::string StringHolder::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}
