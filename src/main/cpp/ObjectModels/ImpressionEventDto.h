#ifndef ImpressionEventDto_H
#define ImpressionEventDto_H

class DateTimeUtil;

#include "Object.h"

class ImpressionEventDto;
/*
   this class is used as part of MySqlDataMover functionality to
   move the impressions that are created hourly by spark job to the impression_compressed table in mysql
 */
class ImpressionEventDto : public Object {

private:

public:
ImpressionEventDto();

virtual ~ImpressionEventDto();
};


#endif
