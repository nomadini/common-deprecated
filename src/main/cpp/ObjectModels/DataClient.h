//
// Created by Mahmoud Taabodi on 7/15/16.
//

#ifndef DataClient_H
#define DataClient_H


#include <memory>
#include <string>
#include <vector>
#include <set>
class EntityToModuleStateStats;
#include "Object.h"
#include <boost/thread.hpp>
#include <thread>

class DataClient : public Object {

public:

DataClient();
virtual ~DataClient();

virtual void reloadDataFromDataMaster()=0;

private:

};
#endif //BIDDER_DATARELOADSERVICE_H
