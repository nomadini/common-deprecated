#include "GUtil.h"
#include "Status.h"

#include <memory>
#include <string>
#include <vector>
#include <set>
#include "StringUtil.h"


std::string gicapods::Status::getDescription() {
        switch (value) {
        case STOP_PROCESSING:
                return "STOP_PROCESSING";
        case CONTINUE_PROCESSING:
                return "CONTINUE_PROCESSING";
        }
        throwEx("status value is not valid "  + StringUtil::toStr(value));
        return "UNKNOWN_DESCRIPTION";//just for clang
}


gicapods::Status::Status() {
        value = CONTINUE_PROCESSING;
}

gicapods::Status::~Status() {

}

std::string gicapods::Status::getUpdatedAt() {
        return updatedAt;
}
