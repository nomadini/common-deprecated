#ifndef MySqlOfferService_h
#define MySqlOfferService_h




#include "Offer.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "Object.h"
#include <cppconn/resultset.h>
class ResultSetHolder;
class MySqlOfferService;



class MySqlOfferService : public DataProvider<Offer>, public Object {

public:
static std::string SELECT_STATEMENT;

MySqlDriver* driver;

MySqlOfferService(MySqlDriver* driver);

virtual void deleteAllOffersAndAssociatedPixelsAndModels();

virtual std::shared_ptr<Offer> readOfferByName(std::string offerName);

virtual std::shared_ptr<Offer> getOfferFromResultSet(std::shared_ptr<ResultSetHolder> res);

virtual std::shared_ptr<Offer> readOffer(std::string offerId);

virtual std::shared_ptr<Offer> readOfferByAdvertiserId(std::string advertiserId);

virtual void persistOffer(std::shared_ptr<Offer> offer);

virtual std::unordered_map<int, std::shared_ptr<Offer> > readAllOffers();

std::vector<std::shared_ptr<Offer> > readAllEntities();

virtual ~MySqlOfferService();

};

#endif
