#ifndef PIXEL_DEVICE_HISTORY_CASSANDRA_SERVICE
#define PIXEL_DEVICE_HISTORY_CASSANDRA_SERVICE



namespace gicapods { class ConfigService; }
#include "PixelDeviceHistory.h"
class CassandraDriverInterface;
#include "HttpUtilService.h"
class EntityToModuleStateStats;

class AsyncThreadPoolService;

class PixelDeviceHistoryCassandraService;
#include "Object.h"
#include "CassandraService.h"



class PixelDeviceHistoryCassandraService : public CassandraService<PixelDeviceHistory>, public Object {
private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;


AsyncThreadPoolService* asyncThreadPoolService;

public:
PixelDeviceHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

void createPixelDeviceHistoryTable(CassSession *session);

/**
 * this function returns all the feature histories for a specific device
 * that are newer than time parameter
 */
std::shared_ptr<PixelDeviceHistory> readDeviceHistoryOfPixel(std::string pixelId,
                                                             int pixelHitRecencyInSecond);



// PixelDeviceHistory* readData(PixelDeviceHistory* value);

virtual ~PixelDeviceHistoryCassandraService();
};


#endif /* PIXEL_DEVICE_HISTORY_CASSANDRA_SERVICE */
