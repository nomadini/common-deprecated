
#include "Pixel.h"
#include "JsonUtil.h"

Pixel::Pixel() : Object(__FILE__) {
								id = 0;
								advertiserId = 0;

}

std::string Pixel::toString() {
								return toJson ();

}
std::string Pixel::getEntityName() {
								return "Pixel";
}
std::shared_ptr<Pixel> Pixel::fromJsonValue(RapidJsonValueType value) {
								auto pixel = std::make_shared<Pixel>();

								pixel->id = JsonUtil::GetIntSafely(value, "id");
								pixel->name =JsonUtil::GetStringSafely(value, "name");
								pixel->description =JsonUtil::GetStringSafely(value, "description");
								pixel->uniqueKey =JsonUtil::GetStringSafely(value, "uniqueKey");
								pixel->advertiserId = JsonUtil::GetIntSafely(value,"advertiserId");

								pixel->dateCreated  = JsonUtil::GetStringSafely(value,"dateCreated");
								pixel->dateModified = JsonUtil::GetStringSafely(value,"dateModified");

								return pixel;
}


void Pixel::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
								JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
								JsonUtil::addMemberToValue_FromPair (doc, "description", description, value);
								JsonUtil::addMemberToValue_FromPair (doc, "uniqueKey", uniqueKey, value);
								JsonUtil::addMemberToValue_FromPair (doc, "advertiserId", advertiserId, value);

								JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
								JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
}

std::string Pixel::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}


Pixel::~Pixel() {

}
