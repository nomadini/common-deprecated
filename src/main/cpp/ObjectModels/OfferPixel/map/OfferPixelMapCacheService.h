#ifndef OfferPixelMapCacheService_h
#define OfferPixelMapCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include "Poco/LRUCache.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "DataProvider.h"

class HttpUtilService;
class MySqlOfferPixelMapService;
class OfferPixelMap;
class OfferPixelMapCacheService;

#include "Object.h"
class OfferPixelMapCacheService : public CacheService<OfferPixelMap>, public Object {

public:

OfferPixelMapCacheService(MySqlOfferPixelMapService* mySqlOfferPixelMapService,
                          HttpUtilService* httpUtilService,
                          std::string dataMasterUrl,
                          EntityToModuleStateStats* entityToModuleStateStats,
                          std::string appName);

std::shared_ptr<std::unordered_map<int, std::shared_ptr<std::vector<int> > > > pixelIdToOfferIds;
std::shared_ptr<std::unordered_map<int, std::shared_ptr<std::vector<int> > > > offerIdToPixelIds;

void clearOtherCaches();
void populateOtherMapsAndLists(std::vector<std::shared_ptr<OfferPixelMap> > allEntities);

std::vector<int> findOfferIdsMappedToPixelId(int pixelId);
std::vector<int> findPixelIdsMappedToOfferId(int offerId);
};


#endif
