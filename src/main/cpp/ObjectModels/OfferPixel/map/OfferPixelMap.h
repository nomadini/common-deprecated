#ifndef OfferPixelMap_H
#define OfferPixelMap_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "Poco/LRUCache.h"
#include "JsonTypeDefs.h"

class OfferPixelMap;

#include "Object.h"
class OfferPixelMap : public Object {

public:

int id;
int pixelId;
int offerId;

std::string dateCreated;
std::string dateModified;

OfferPixelMap();

std::string toString();

std::string toJson();

static std::string getEntityName();
virtual ~OfferPixelMap();
static std::shared_ptr<OfferPixelMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};




#endif
