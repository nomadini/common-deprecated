#include "GUtil.h"

#include "MySqlDriver.h"
#include "ConverterUtil.h"
#include "MySqlOfferPixelMapService.h"
#include "MySqlDriver.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include "OfferPixelMap.h"
#include "StringUtil.h"

MySqlOfferPixelMapService::MySqlOfferPixelMapService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}

MySqlOfferPixelMapService::~MySqlOfferPixelMapService() {

}

std::vector<std::shared_ptr<OfferPixelMap> > MySqlOfferPixelMapService::readAllEntities() {
        std::vector<std::shared_ptr<OfferPixelMap> > all;
        std::string query = "SELECT"
                            "`id`,"
                            " `offer_id`,"
                            " `pixel_id`,"
                            " `created_at`,"
                            " `updated_at`"
                            " FROM `offer_pixel_map`";
        auto res = driver->executeQuery (query);
        while (res->next ()) {
                auto offerPixelMap = getOfferPixelMapFromResultSet (res);
                all.push_back(offerPixelMap);

        }
        return all;
}

std::shared_ptr<OfferPixelMap> MySqlOfferPixelMapService::getOfferPixelMapFromResultSet(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<OfferPixelMap> offerPixelMap = std::make_shared<OfferPixelMap>();
        offerPixelMap->id = res->getInt ("id");
        offerPixelMap->pixelId = res->getInt ("pixel_id");
        offerPixelMap->offerId = res->getInt ("offer_id");

        offerPixelMap->dateCreated = MySqlDriver::getString( res, "created_at");
        offerPixelMap->dateModified = MySqlDriver::getString( res, "updated_at");
        return offerPixelMap;
}

void MySqlOfferPixelMapService::persistOfferPixelMappings(int offerId, int pixelId) {

        std::string queryStr =
                "INSERT INTO offer_pixel_map"
                "("
                "offer_id,"
                "pixel_id)  "
                "VALUES"
                "('__offer_id__',"
                "'__pixel_id__');";
        MLOG(3) << "persistOfferPixelMappings in db offerId  " << offerId << ", pixelId : " << pixelId;
        assertAndThrow(offerId != 0);
        assertAndThrow(pixelId != 0);
        queryStr = StringUtil::replaceString (queryStr, "__offer_id__",
                                              StringUtil::toStr (offerId));
        queryStr = StringUtil::replaceString (queryStr, "__pixel_id__",
                                              StringUtil::toStr(pixelId));

        driver->executedUpdateStatement (queryStr);
}

std::set<int> MySqlOfferPixelMapService::readPixelIdsForOffer(int id) {
        std::set<int> pixelIds;
        std::string query = "SELECT `offer_pixel_map`.`ID`," //1
                            " `offer_pixel_map`.`offer_id`," //2
                            " `offer_pixel_map`.`pixel_id` " //3
                            " FROM `offer_pixel_map` where offer_id = '__offer_id__'";


        query = StringUtil::replaceString (query, "__offer_id__", StringUtil::toStr (id));


        auto res = driver->executeQuery (query);


        while (res->next ()) {

                int offerId = res->getInt ("offer_id");
                int pixelId = res->getInt ("pixel_id");

                MLOG(3) << "values loaded for offer_pixel_map : "
                        " offerId :  " << offerId <<
                        " pixelId : " << pixelId;

                pixelIds.insert (pixelId);
        }

        return pixelIds;
}
