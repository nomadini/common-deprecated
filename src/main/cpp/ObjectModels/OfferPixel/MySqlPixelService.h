#ifndef MySqlPixelService_h
#define MySqlPixelService_h




class Pixel;
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "Object.h"
#include <cppconn/resultset.h>
class MySqlPixelService;


class MySqlPixelService : public DataProvider<Pixel>, public Object {

public:

MySqlDriver* driver;

MySqlPixelService(MySqlDriver* driver);

std::vector<std::shared_ptr<Pixel> > readAllEntities();
std::shared_ptr<Pixel> getPixelFromResultSet(std::shared_ptr<ResultSetHolder> res);

virtual ~MySqlPixelService();

};

#endif
