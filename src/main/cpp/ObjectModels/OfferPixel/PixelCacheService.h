#ifndef PixelCacheService_h
#define PixelCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include "Poco/LRUCache.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
#include "DataProvider.h"

class HttpUtilService;
class MySqlPixelService;
class Pixel;
class PixelCacheService;

#include "Object.h"
class PixelCacheService : public CacheService<Pixel>, public Object {

public:

PixelCacheService(MySqlPixelService* mySqlPixelService,
                  HttpUtilService* httpUtilService,
                  std::string dataMasterUrl,
                  EntityToModuleStateStats* entityToModuleStateStats,
                  std::string appName);

std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> > > keyToPixelMap;
std::shared_ptr<tbb::concurrent_hash_map<int, std::shared_ptr<Pixel> > > idToPixelMap;

std::shared_ptr<Pixel> getPixelByKey(std::string uniqueKey);

void clearOtherCaches();
void populateOtherMapsAndLists(std::vector<std::shared_ptr<Pixel> > allEntities);
};


#endif
