#ifndef OfferTestHelper_H
#define OfferTestHelper_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Object.h"
class OfferTestHelper;

class OfferTestHelper {

public:

OfferTestHelper();

static std::set<int> getRandomOfferIds(int size);

virtual ~OfferTestHelper();
};

#endif
