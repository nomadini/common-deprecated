#ifndef PixelDeviceHistory_H
#define PixelDeviceHistory_H


class Device;
#include "DateTimeMacro.h"
#include "JsonTypeDefs.h"
#include "CassandraManagedType.h"
#include <unordered_map>
#include <memory>

#include "Object.h"
class PixelDeviceHistory : public CassandraManagedType, public Object {

private:

public:
std::string pixelUniqueKey;

std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<Device> > > devicesHittingPixel;

PixelDeviceHistory();

std::string toString();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
static std::shared_ptr<PixelDeviceHistory> fromJson(std::string json);
static std::shared_ptr<PixelDeviceHistory> fromJsonValue(RapidJsonValueType value);

virtual ~PixelDeviceHistory();

};


#endif
