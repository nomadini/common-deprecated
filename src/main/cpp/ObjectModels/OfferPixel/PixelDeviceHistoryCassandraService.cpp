#include "CassandraDriverInterface.h"
#include "CollectionUtil.h"

#include "GUtil.h"
#include "PixelDeviceHistory.h"
#include "FeatureDeviceHistory.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "CassandraDriverInterface.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "CassandraDriver.h"
#include "ConfigService.h"
#include "Device.h"

PixelDeviceHistoryCassandraService::PixelDeviceHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService)  :
        CassandraService(cassandraDriver,
                         entityToModuleStateStats,

                         asyncThreadPoolService,
                         configService), Object(__FILE__) {
        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}

/**
 * this function returns all the feature histories for a specific device
 * that are newer than time parameter
 */
std::shared_ptr<PixelDeviceHistory> PixelDeviceHistoryCassandraService::
readDeviceHistoryOfPixel(std::string pixelUniqueKey, int pixelHitRecencyInSecond) {
        MLOG(3)<<"readDeviceHistoryOfPixel , pixelUniqueKey :  "<<pixelUniqueKey
               <<", pixelHitRecencyInSecond : "<<pixelHitRecencyInSecond;

        std::shared_ptr<PixelDeviceHistory> pixelDeviceHistory = std::make_shared<PixelDeviceHistory>();

        CassError rc = CASS_OK;
        CassStatement* statement = NULL;
        CassFuture* future = NULL;
        auto dateToPickupInSecond = DateTimeUtil::getNowInSecond() - pixelHitRecencyInSecond;
        std::string queryStr = StringUtil::toStr(
                "SELECT deviceId, deviceType, timeofvisit FROM gicapods_mdl.pixelDeviceHistory "
                " where pixelId = '") + pixelUniqueKey + StringUtil::toStr("'")
                               +  " and timeofvisit > "
                               + StringUtil::toStr(dateToPickupInSecond * 1000) + " ALLOW FILTERING";

        MLOG(3)<<"query to readDeviceHistoryOfPixel : "<<queryStr;
        const char* query = queryStr.c_str();

        statement = cass_statement_new(query, 0);

        future = cass_session_execute(cassandraDriver->getSession(), statement);
        cass_future_wait_timed(future, 10);

        rc = cass_future_error_code(future);
        if (rc != CASS_OK) {
                print_error(future, queryStr, entityToModuleStateStats);
        } else {
                const CassResult* result = cass_future_get_result(future);

                CassIterator* rowIterator = cass_iterator_from_result(result);
                pixelDeviceHistory->pixelUniqueKey = pixelUniqueKey;

                while (cass_iterator_next(rowIterator)) {

                        TimeType timeOfKey = (long) cassandraDriver->getLongValueFromRowAtColumn(rowIterator, 2);
                        TimeType timeOfKeyInSec = (timeOfKey/1000);

                        auto deviceId = cassandraDriver->getStringValueFromRowAtColumn(rowIterator, 0);
                        auto deviceTypeStr = cassandraDriver->getStringValueFromRowAtColumn(rowIterator, 1);
                        auto deviceRead = std::make_shared<Device> (
                                deviceId,
                                deviceTypeStr
                                );
                        pixelDeviceHistory->devicesHittingPixel->insert(
                                std::pair<std::string, std::shared_ptr<Device> >(
                                        StringUtil::toStr(timeOfKey), deviceRead));
                        // } else {
                        //         entityToModuleStateStats->
                        //         addStateModuleForEntity("TO_OLD_OF_PIXEL_DEVICE_HISTORY",
                        //                                 "PixelDeviceHistoryCassandraService",
                        //                                 "pix" + pixelUniqueKey);
                        // }
                }
                cass_iterator_free(rowIterator);
                cass_result_free(result);
                cass_future_free(future);
                cass_statement_free(statement);

                if (pixelDeviceHistory->devicesHittingPixel->size() == 0) {
                        LOG(WARNING)<<"query to read pixelDeviceHistory : "<<queryStr;
                        LOG(WARNING)<<"size of devicesHittingPixel returned for this pixel  "<<pixelDeviceHistory->pixelUniqueKey<<" is 0";
                }

                return pixelDeviceHistory;
        }
        return pixelDeviceHistory;
}
//
// PixelDeviceHistory* PixelDeviceHistoryCassandraService::readData(PixelDeviceHistory* value) {
//         throwEx("not implemented");
// }

PixelDeviceHistoryCassandraService::~PixelDeviceHistoryCassandraService() {

}
