
#ifndef PIXEL_DEVICE_HISTORY_CASSANDRA_SERVICE_MOCK
#define PIXEL_DEVICE_HISTORY_CASSANDRA_SERVICE_MOCK
class CassandraDriverInterface;



#include "PixelDeviceHistory.h"
#include "FeatureDeviceHistory.h"
#include "ModelRequest.h"
#include "gmock/gmock.h"

class PixelDeviceHistoryCassandraServiceMock;


class PixelDeviceHistoryCassandraServiceMock : public PixelDeviceHistoryCassandraService {

public:


// MOCK_METHOD1(createPixelDeviceHistoryTable, void (CassSession* session));
//
// MOCK_METHOD2(fetchPixelDeviceHistoryFromRow,
//              std::shared_ptr<PixelDeviceHistory>(const CassRow* row, std::shared_ptr<ModelRequest> modelContext));
//
// MOCK_METHOD2(readDeviceHistoryOfPixel, std::shared_ptr<PixelDeviceHistory>(std::string pixelId, int pixelRecencyInSecond));

};


#endif /* PIXEL_DEVICE_HISTORY_CASSANDRA_SERVICE_MOCK */
