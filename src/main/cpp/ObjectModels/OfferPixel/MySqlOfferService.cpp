#include "GUtil.h"


#include "Offer.h"
#include "MySqlDriver.h"
#include "ConverterUtil.h"
#include "MySqlOfferService.h"
#include "MySqlDriver.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include "ResultSetHolder.h"


std::string MySqlOfferService::SELECT_STATEMENT =
        " SELECT"
        "`ID`,"
        " `NAME`,"
        " `advertiser_id`,"
        " `audience_type`,"
        " `created_at`,"
        " `updated_at`"
        " FROM `offer`";

MySqlOfferService::MySqlOfferService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}

void MySqlOfferService::deleteAllOffersAndAssociatedPixelsAndModels() {

        MLOG(3) << "deleteAllOffersAndAssociatedPixelsAndModels ";
        driver->deleteAll("offer ");
}

std::shared_ptr<Offer> MySqlOfferService::readOfferByName(std::string offerName) {

        auto offer = std::make_shared<Offer> ();
        std::string query = SELECT_STATEMENT +  _toStr("where NAME = '__NAME__'");

        query = StringUtil::replaceString (query, "__NAME__", offerName);


        auto res = driver->executeQuery (query);


        while (res->next ()) {
                offer = getOfferFromResultSet (res);
        }

        return offer;
}

std::shared_ptr<Offer> MySqlOfferService::getOfferFromResultSet(std::shared_ptr<ResultSetHolder> res) {
        auto offer = std::make_shared<Offer> ();
        int id = res->getInt ("ID");
        std::string offerName = MySqlDriver::getString( res, "NAME");
        int advertiserId = res->getInt ("ADVERTISER_ID");

        std::string audienceType = MySqlDriver::getString( res, "audience_type");
        std::string dateCreated = MySqlDriver::getString( res, "created_at");
        std::string dateModified = MySqlDriver::getString( res, "updated_at");

        MLOG(3) <<
                "values loaded for offer id :  " << id << " ,"
                " offerName :  " << offerName << " , "
                " advertiserId :  " << advertiserId << " ,"
                " audienceType :  " << audienceType << " ,"
                " dateCreated :  " << dateCreated << " ,"
                " dateModified :  " << dateModified << "  ";

        offer->id = id;
        offer->name = offerName;
        offer->advertiserId = advertiserId;
        offer->audienceType = audienceType;
        offer->dateCreated = dateCreated;
        offer->dateModified = dateModified;

        return offer;
}

std::shared_ptr<Offer> MySqlOfferService::readOffer(std::string offerId) {

        std::string query =
                SELECT_STATEMENT +  _toStr("where ID = '__ID__'");

        query = StringUtil::replaceString (query, "__ID__", offerId);
        MLOG(3) << "query : " << query;


        auto res = driver->executeQuery (query);
        while (res->next ()) {

                auto offer = getOfferFromResultSet (res);
                return offer;
        }


}

std::shared_ptr<Offer> MySqlOfferService::readOfferByAdvertiserId(std::string advertiserId) {

        std::string query =
                SELECT_STATEMENT +  _toStr("where ADVERTISER_ID = '__ADVERTISER_ID__'");


        query = StringUtil::replaceString (query, "__ADVERTISER_ID__", advertiserId);


        auto res = driver->executeQuery (query);


        while (res->next ()) {
                std::shared_ptr<Offer> offer  = std::make_shared<Offer>();
                offer = getOfferFromResultSet (res);
                return offer;
        }

        throwEx("no offer for adv id : " + advertiserId );

}

void MySqlOfferService::persistOffer(std::shared_ptr<Offer> offer) {
        std::string queryStr =
                "INSERT INTO offer"
                " ("
                " NAME,"
                " ADVERTISER_ID,"
                " `audience_type`,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '__NAME__',"
                " '__ADVERTISER_ID__',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";

        MLOG(3) << "inserting new offer in db : " << offer->toJson ();

        Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__NAME__",
                                              offer->name);
        queryStr = StringUtil::replaceString (queryStr, "__ADVERTISER_ID__",
                                              StringUtil::toStr(offer->advertiserId));
        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        offer->id = driver->getLastInsertedId ();


        //reading the offer back, because we need the ID that mysql created for us
        std::shared_ptr<Offer> offerFromMySql = readOfferByName (offer->name);
}

std::unordered_map<int, std::shared_ptr<Offer> > MySqlOfferService::readAllOffers() {
        std::unordered_map<int, std::shared_ptr<Offer> > offerMap;
        std::string query = SELECT_STATEMENT;


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                // getInt(1) returns the first column
                auto offer = getOfferFromResultSet (res);
                offerMap.insert (std::make_pair(offer->id, offer));
        }


        if (offerMap.size () == 0) {
                LOG(WARNING) << "no offer were loaded ";
        }
        return offerMap;
}


std::vector<std::shared_ptr<Offer> > MySqlOfferService::readAllEntities() {
        std::vector<std::shared_ptr<Offer> > allOffers;
        std::string query = SELECT_STATEMENT;


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                auto offer = getOfferFromResultSet (res);
                allOffers.push_back (offer);
        }


        return allOffers;
}



MySqlOfferService::~MySqlOfferService() {

}
