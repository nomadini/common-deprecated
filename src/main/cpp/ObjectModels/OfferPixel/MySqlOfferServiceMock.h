#ifndef MySqlOfferService_Mock_h
#define MySqlOfferService_Mock_h

#include "Offer.h"
#include "MySqlOfferService.h"

class MySqlOfferServiceMock;


class MySqlOfferServiceMock : public MySqlOfferService {

	public:

    MySqlOfferServiceMock(MySqlDriver* driver) : MySqlOfferService(driver) {

    }

    MOCK_METHOD1(readOffer, std::shared_ptr<Offer> (std::string offerId));


	virtual ~MySqlOfferServiceMock() {

	}

};


#endif
