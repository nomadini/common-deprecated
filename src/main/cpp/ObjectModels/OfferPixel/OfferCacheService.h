#ifndef OfferCacheService_h
#define OfferCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include "Poco/LRUCache.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
#include "DataProvider.h"

class HttpUtilService;
class Offer;
class OfferCacheService;

#include "Object.h"
class OfferCacheService : public CacheService<Offer>, public Object {

public:

OfferCacheService(HttpUtilService* httpUtilService,
                  std::string dataMasterUrl,
                  EntityToModuleStateStats* entityToModuleStateStats,
                  std::string appName);

std::shared_ptr<tbb::concurrent_hash_map<int, std::shared_ptr<Offer> > > idToOfferMap;

void clearOtherCaches();
void populateOtherMapsAndLists(std::vector<std::shared_ptr<Offer> > allEntities);
};


#endif
