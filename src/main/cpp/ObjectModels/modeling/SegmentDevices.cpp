
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "StringUtil.h"
#include "SegmentDevices.h"
#include <boost/foreach.hpp>
#include "TestsCommon.h"
#include "DateTimeUtil.h"
#include "Device.h"

SegmentDevices::SegmentDevices()   : Object(__FILE__) {
        dateCreatedInMillis = DateTimeUtil::getNowDayInMilliSecond();
}

void SegmentDevices::setDevice(std::string deviceId, std::string deviceType){

        auto device = std::make_shared<Device> (
                deviceId,
                deviceType
                );
        this->devices.push_back(device);
}

std::string SegmentDevices::toString() {
        return toJson();
}

std::string SegmentDevices::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}


void SegmentDevices::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair(doc, "segment", segment,value);
        JsonArrayUtil::addMemberToValue_FromArrayOfObjects(doc, value, "devices", devices);
        JsonUtil::addMemberToValue_FromPair(doc, "dateCreatedInMillis", dateCreatedInMillis, value);
}


std::shared_ptr<SegmentDevices> SegmentDevices::fromJson(std::string jsonModel) {
        auto doc = parseJsonSafely(jsonModel);
        auto segmentDevices = std::make_shared<SegmentDevices> ();
        segmentDevices->segment = Segment::fromJsonValue((*doc)["segment"]);
        segmentDevices->dateCreatedInMillis = JsonUtil::GetLongSafely(*doc, "dateCreatedInMillis"),

        JsonArrayUtil::getArrayFromValueMemeber(
                *doc, "devices", segmentDevices->devices);
        return segmentDevices;
}

std::shared_ptr<SegmentDevices> SegmentDevices::fromJsonValue(RapidJsonValueType value) {
        auto segmentDevices = std::make_shared<SegmentDevices> ();
        segmentDevices->segment = Segment::fromJsonValue((value)["segment"]);
        segmentDevices->dateCreatedInMillis = JsonUtil::GetLongSafely(value, "dateCreatedInMillis"),
        JsonArrayUtil::getArrayFromValueMemeber(
                value, "devices", segmentDevices->devices);
        return segmentDevices;
}

SegmentDevices::~SegmentDevices() {

}
