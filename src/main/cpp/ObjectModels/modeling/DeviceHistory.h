//
// Created by Mahmoud Taabodi on 11/29/15.
//

#ifndef GICAPODS_DEVICEHISTORY_H
#define GICAPODS_DEVICEHISTORY_H

#include <string>
#include <memory>
#include "JsonTypeDefs.h"
#include "DateTimeMacro.h"
class Device;
#include "Object.h"
class DeviceHistory : public Object {
private:

public:
std::shared_ptr<Device> device;
std::string deviceType;
TimeType timeOfVisit;
std::string deviceInfo;      //this is for other info about this device, it should be in json format


DeviceHistory(std::shared_ptr<Device> device);
virtual ~DeviceHistory();

static std::shared_ptr<DeviceHistory> fromJsonValue(RapidJsonValueType value);
std::string toJson();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif //GICAPODS_DEVICEHISTORY_H
