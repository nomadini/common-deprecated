#include "RecencyModel.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "ConverterUtil.h"

RecencyModel::RecencyModel () : Object(__FILE__) {
								id = 0;
								scoreBase = 0;
								advertiserId = 0;

}

RecencyModel::~RecencyModel() {

}

std::string RecencyModel::getEntityName() {
								return "RecencyModel";
}
void RecencyModel::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								RapidJsonValueTypeNoRef featureScoresArrayContent (rapidjson::kArrayType);

								JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
								JsonUtil::addMemberToValue_FromPair (doc, "scoreBase", scoreBase, value);

								for (auto &keyValue : featureScoreMap) {
																RapidJsonValueTypeNoRef featureScoresPair (rapidjson::kObjectType);
																JsonUtil::addMemberToValue_FromPair (doc, "score", keyValue.second, featureScoresPair);
																JsonUtil::addMemberToValue_FromPair (doc, "name", keyValue.first, featureScoresPair);
																featureScoresArrayContent.PushBack (featureScoresPair, doc->GetAllocator ());
								}
								JsonUtil::addMemberToValue_FromPair (doc, "featureScores", featureScoresArrayContent, value);
}


std::string RecencyModel::toJson() {
								auto doc = JsonUtil::createDcoumentAsObjectDoc();
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}

std::shared_ptr<RecencyModel> RecencyModel::fromJsonValue(RapidJsonValueType value) {
								auto model = std::make_shared<RecencyModel>();
								model->scoreBase = JsonUtil::GetDoubleSafely (value, "scoreBase");
								model->id = JsonUtil::GetIntSafely (value, "id");
								model->featureScoreMap = parseFeatureScores(value);

								return model;
}

std::unordered_map<std::string, float > RecencyModel::parseFeatureScores(RapidJsonValueType value) {

								std::unordered_map<std::string, float >  map;
								if(value.HasMember("featureScores")) {
																RapidJsonValueType featureScoresValue = (value)["featureScores"];

																assertAndThrow(featureScoresValue.IsArray ());
																// rapidjson uses SizeType instead of size_t.
																for (rapidjson::SizeType i = 0; i < featureScoresValue.Size (); i++) {
																								RapidJsonValueType featureScoresPair = featureScoresValue[i];
																								std::string nameOfFeature = JsonUtil::getValueOfMember<std::string>(featureScoresPair, "name");
																								RapidJsonValueType scoreValue = featureScoresPair["score"];
																								float scoreOfFeature = 0;
																								if (scoreValue.IsDouble()) {
																																scoreOfFeature = JsonUtil::getValueOfMember<float>(featureScoresPair, "score");
																								} else {
																																scoreOfFeature = ConverterUtil::convertTo<double>(JsonUtil::getValueOfMember<std::string>(featureScoresPair, "score"));
																								}
																								map.insert(std::make_pair(nameOfFeature, scoreOfFeature));
																}

								} else {
																throwEx("featureScores is required");
								}
								return map;
}
std::shared_ptr<RecencyModel> RecencyModel::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);

}
