#ifndef RecencyModel_h
#define RecencyModel_h


#include <atomic>
#include <memory>
#include <string>
#include <unordered_map>
#include "JsonTypeDefs.h"
#include "Object.h"
#include "Poco/DateTime.h"
/*
   its a model that user enters that will be used to score a device
   RecencyModel looks like this :
   {"modelId":12, "scoreBase": 12.0, "featureScores":[{"score":12.020000457763672,"name":"abc.com"},{"score":13.039999961853028,"name":"bbc.com"}]}

 */
class RecencyModel : public Object {
public:
int id;
int advertiserId;
float scoreBase;
std::string name;

Poco::DateTime createdAt;
Poco::DateTime updatedAt;

std::unordered_map<std::string, float > featureScoreMap;

RecencyModel();
virtual ~RecencyModel();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);


std::string toJson();

static std::string getEntityName();
static std::shared_ptr<RecencyModel> fromJson(std::string jsnString);
static std::shared_ptr<RecencyModel> fromJsonValue(RapidJsonValueType value);
static std::unordered_map<std::string, float > parseFeatureScores(RapidJsonValueType value);
};

#endif
