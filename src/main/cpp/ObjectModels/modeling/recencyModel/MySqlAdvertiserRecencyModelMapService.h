#ifndef MySqlAdvertiserRecencyModelMapService_h
#define MySqlAdvertiserRecencyModelMapService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "AdvertiserRecencyModelMap.h"
#include "Object.h"
class MySqlAdvertiserRecencyModelMapService : public Object {

public:

MySqlDriver* driver;

MySqlAdvertiserRecencyModelMapService(MySqlDriver* driver);

std::vector<std::shared_ptr<AdvertiserRecencyModelMap> > readAll();

std::vector<std::shared_ptr<AdvertiserRecencyModelMap> > readAllActiveOnes();

std::shared_ptr<AdvertiserRecencyModelMap> read(int id);

void update(std::shared_ptr<AdvertiserRecencyModelMap> obj);

void insert(std::shared_ptr<AdvertiserRecencyModelMap> obj);

virtual void deleteAll();

virtual ~MySqlAdvertiserRecencyModelMapService();

};


#endif
