#include "GUtil.h"
#include "ModelUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlRecencyModelService.h"
#include "MySqlDriver.h"
#include "TempUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include <shogun/lib/common.h>

std::string MySqlRecencyModelService::commonReadModelQuery =
        "SELECT "
        " `id`,"
        " `name`,"
        " `advertiser_id`,"
        " `description`,"
        " `scoreBase`,"
        " `feature_score_map`,"
        " `updated_at`,"
        " `created_at`"
        " FROM `recency_model` ";



MySqlRecencyModelService::MySqlRecencyModelService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<RecencyModel> > MySqlRecencyModelService::readAllEntities() {
        std::vector<std::shared_ptr<RecencyModel> > allModels;
        std::string query = MySqlRecencyModelService::commonReadModelQuery + StringUtil::toStr (" ;");
        //each target group has a vector which has 168 numbers which represents each hour of the week


        auto res = driver->executeQuery (query);
        while (res->next ()) {
                auto model = getModelFromResultSet (res);
                allModels.push_back (model);
        }


        return allModels;
}

std::shared_ptr<RecencyModel> MySqlRecencyModelService::getModelFromResultSet(std::shared_ptr<ResultSetHolder> res) {
        assertAndThrow(res != NULL);
        std::shared_ptr<RecencyModel> model = std::make_shared <RecencyModel> ();

        model->name =  res->getString("name");
        model->advertiserId = res->getInt("advertiser_id");
        model->id  = res->getInt("id");
        model->scoreBase  = res->getDouble("scoreBase");

        model->createdAt = DateTimeUtil::parseDateTime(res->getString ("created_at"));
        model->updatedAt = DateTimeUtil::parseDateTime(res->getString ("updated_at"));
        std::string featureScoreMap = MySqlDriver::getString( res, "feature_score_map");
        MLOG(3)<< "parsing featureScoreMap : "<< featureScoreMap;
        auto document = parseJsonSafely(featureScoreMap);
        model->featureScoreMap = RecencyModel::parseFeatureScores(*document);

        return model;

}

void MySqlRecencyModelService::deleteAll() {
        driver->deleteAll("model");
}

MySqlRecencyModelService::~MySqlRecencyModelService() {

}
