#ifndef RecencyModelCacheService_h
#define RecencyModelCacheService_h


#include "RecencyModel.h"
#include <string>
#include <memory>
#include <unordered_map>

#include "MySqlRecencyModelService.h"
#include "HttpUtilService.h"
#include "CacheService.h"
class EntityToModuleStateStats;
#include "Object.h"
#include "EntityProviderService.h"
class RecencyModelCacheService;



class RecencyModelCacheService : public CacheService<RecencyModel>, public Object {

public:

RecencyModelCacheService(MySqlRecencyModelService* mySqlModelService,
                         HttpUtilService* httpUtilService,
                         std::string dataMasterUrl,
                         EntityToModuleStateStats* entityToModuleStateStats,
                         std::string appName);

virtual ~RecencyModelCacheService();
};


#endif
