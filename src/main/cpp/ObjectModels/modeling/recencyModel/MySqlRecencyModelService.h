#ifndef MySqlRecencyModelService_h
#define MySqlRecencyModelService_h


#include "RecencyModel.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "Object.h"
class MySqlRecencyModelService : public DataProvider<RecencyModel>, public Object {

public:

static std::string commonReadModelQuery;

MySqlDriver* driver;

MySqlRecencyModelService(MySqlDriver* driver);

std::vector<std::shared_ptr<RecencyModel> > readAllEntities();

void deleteAll();
std::shared_ptr<RecencyModel> getModelFromResultSet(std::shared_ptr<ResultSetHolder> res);

virtual ~MySqlRecencyModelService();
};


#endif
