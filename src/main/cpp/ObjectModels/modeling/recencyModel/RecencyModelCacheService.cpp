#include "RecencyModel.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "RecencyModelCacheService.h"
#include "MySqlRecencyModelService.h"
#include "JsonArrayUtil.h"

RecencyModelCacheService::RecencyModelCacheService(
        MySqlRecencyModelService* mySqlModelServiceArg,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<RecencyModel>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {

}

RecencyModelCacheService::~RecencyModelCacheService() {

}
