
#include "RandomUtil.h"
#include "TestsCommon.h"

#include "DateTimeUtil.h"
#include "SegmentTestHelper.h"
#include "StringUtil.h"

std::shared_ptr<Segment> SegmentTestHelper::createSampleSegment() {

        //creating the sample segment
        std::shared_ptr<Segment> segment = std::make_shared<Segment>(DateTimeUtil::getNowInMilliSecond());
        segment->id = RandomUtil::sudoRandomNumber (1000);
        segment->modelId = RandomUtil::sudoRandomNumber (1000);
        segment->advertiserId = RandomUtil::sudoRandomNumber (1000);
        segment->setUniqueName("segment" + StringUtil::toStr(RandomUtil::sudoRandomNumber (1000)));
        return segment;
}

void SegmentTestHelper::areEqual(std::shared_ptr<Segment> segment1, std::shared_ptr<Segment> segment2) {

        EXPECT_THAT(segment1->modelId,
                    testing::Eq (segment2->modelId));
        EXPECT_THAT(segment1->advertiserId,
                    testing::Eq (segment2->advertiserId));
        EXPECT_THAT(segment1->getUniqueName(),
                    testing::Eq (segment2->getUniqueName()));
}
