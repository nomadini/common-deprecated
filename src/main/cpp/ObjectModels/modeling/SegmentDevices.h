#ifndef SegmentDevices_H
#define SegmentDevices_H


#include "JsonTypeDefs.h"
#include <string>
#include <memory>
class Device;
#include <gtest/gtest.h>
#include "Object.h"
#include "Segment.h"
#include "CassandraManagedType.h"

/**
   this class holds all the device id and types that belong to a segment
   if dateCreated is set, it means they are for a specific date, if not, it means
   they are for different dates
 */
class SegmentDevices : public CassandraManagedType, public Object {

private:

public:

std::shared_ptr<Segment> segment;
std::vector<std::shared_ptr<Device> > devices;
long dateCreatedInMillis;

SegmentDevices();
std::string toString();

std::string toJson();

static std::shared_ptr<SegmentDevices> fromJson(std::string jsonModel);
static std::shared_ptr<SegmentDevices> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
virtual ~SegmentDevices();
void setDevice(std::string deviceId, std::string deviceType);

};

#endif
