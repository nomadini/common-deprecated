
#include "FeatureToFeatureHistory.h"
#include "CassandraDriverInterface.h"
#include "gmock/gmock.h"
#include "FeatureToFeatureHistoryCassandraService.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "JsonArrayUtil.h"
#include "StatisticsUtil.h"
#include "FeatureHistory.h"
#include "ConfigService.h"
#include "CassandraDriver.h"
#include "FeatureHistory.h"


FeatureToFeatureHistoryCassandraService::FeatureToFeatureHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService)  :
        CassandraService<FeatureToFeatureHistory>(cassandraDriver,
                                                  entityToModuleStateStats,

                                                  asyncThreadPoolService,
                                                  configService), Object(__FILE__) {
        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}


void FeatureToFeatureHistoryCassandraService::deleteAll() {
        cassandraDriver->deleteAll (
                "gicapods_mdl.featuretofeaturehistory"
                );
}


void FeatureToFeatureHistoryCassandraService::fetchFeatureHistory(CassIterator* rowIterator,
                                                                  std::shared_ptr<FeatureToFeatureHistory> featureDeviceHistories,
                                                                  TimeType timeOfVisitFromDB) {
        std::shared_ptr<FeatureHistory> devHistory = std::make_shared<FeatureHistory>(
                std::make_shared<Feature>(
                        cassandraDriver->getStringValueFromRowByName(rowIterator, "feature"),
                        cassandraDriver->getStringValueFromRowByName(rowIterator, "featureType"),
                        0),
                cassandraDriver->getLongValueFromRowByName(rowIterator, "timeofvisit")
                );

        featureDeviceHistories->featureHistories.push_back(devHistory);
}


std::shared_ptr<FeatureToFeatureHistory> FeatureToFeatureHistoryCassandraService::readFeatureHistoryOfFeature(
        std::string feature,
        std::string featureType,
        TimeType time,
        int limit) {

        int numberOfDeviceHistoriesFetchedForThisFeature = 0;
        std::string queryStr =
                "SELECT feature, featureType, timeofvisit from "
                " gicapods_mdl.featuretofeaturehistory "
                " where featurekey = '__FEATURE__' and featurekeytype='__featureType__' ";

        queryStr = StringUtil::replaceString(queryStr, "__FEATURE__", feature);
        queryStr = StringUtil::replaceString(queryStr, "__featureType__", featureType);
        MLOG(3)<<"queryStr to read feature device history : "<<queryStr;
        const char* query = queryStr.c_str();

        auto statement = cass_statement_new(query, 0);

        auto future = cass_session_execute(cassandraDriver->getSession(), statement);
        cass_future_wait(future);

        auto rc = cass_future_error_code(future);
        std::shared_ptr<FeatureToFeatureHistory> featureDeviceHistories =
                std::make_shared<FeatureToFeatureHistory>(
                        std::make_unique<Feature>(featureType, feature, 0));

        if (rc != CASS_OK) {
                print_error(future, queryStr, entityToModuleStateStats);
        } else {
                const CassResult* result = cass_future_get_result(future);

                CassIterator* rowIterator = cass_iterator_from_result(result);

                while (cass_iterator_next(rowIterator)) {

                        long timeOfVisitFromDB =
                                cassandraDriver->getLongValueFromRowAtColumn(rowIterator, 2);

                        if (timeOfVisitFromDB > time) {
                                fetchFeatureHistory(rowIterator, featureDeviceHistories,
                                                    timeOfVisitFromDB);
                                numberOfDeviceHistoriesFetchedForThisFeature++;
                                if (numberOfDeviceHistoriesFetchedForThisFeature >= limit) {
                                        break;
                                }
                        } else {
                                MLOG(3)<<"feature device history is too old to be fetched, timeOfVisitFromDB "
                                        " :  "<<timeOfVisitFromDB
                                       <<" , time : "<<time;
                        }

                }

                cass_iterator_free(rowIterator);
                cass_result_free(result);
        }

        cass_future_free(future);
        cass_statement_free(statement);
        MLOG(3)<<"  "<<featureDeviceHistories->featureHistories.size()
               <<" devices visited this feature  "<<featureDeviceHistories->feature->getName();
        return featureDeviceHistories;
}


/**
 * reads all devices that are the visitors of these features
 */
std::vector<std::shared_ptr<FeatureToFeatureHistory> > FeatureToFeatureHistoryCassandraService::readFeatureHistoryOfFeatures(
        int limit, TimeType time, std::set<std::string> featuresWanted, std::string featureType) {

        std::vector<std::shared_ptr<FeatureToFeatureHistory> > allFeatureDeviceHistories;
        for (auto feature : featuresWanted) {
                std::shared_ptr<FeatureToFeatureHistory> featureFeatureHistory = readFeatureHistoryOfFeature(feature,
                                                                                                             featureType,
                                                                                                             time,
                                                                                                             limit);
                MLOG(3)<<"featureFeatureHistory size :  "<<featureFeatureHistory->featureHistories.size()
                       <<" feature   "<<feature;
                if (!featureFeatureHistory->featureHistories.empty() &&
                    featureFeatureHistory->feature->isValid()) {
                        allFeatureDeviceHistories.push_back(featureFeatureHistory);
                } else {
                        LOG(ERROR)<<" featureFeatureHistory is invalid or empty ,feature : "<<feature;
                }


        }
        return allFeatureDeviceHistories;
}
