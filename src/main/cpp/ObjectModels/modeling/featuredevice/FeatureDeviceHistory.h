#ifndef FeatureDeviceHistory_H
#define FeatureDeviceHistory_H



#include <memory>
#include <string>
#include <vector>
#include "cassandra.h"
#include "JsonTypeDefs.h"
#include "DeviceHistory.h"
#include "CassandraManagedType.h"


class Feature;
#include "Object.h"
class FeatureDeviceHistory : public CassandraManagedType, public Object {

private:

public:

std::unique_ptr<Feature> feature;
std::vector<std::shared_ptr<DeviceHistory> > devicehistories;

FeatureDeviceHistory(std::unique_ptr<Feature> feature);

std::string toString();

std::string toJson();

static std::shared_ptr<FeatureDeviceHistory> fromJson(std::string json);
static std::shared_ptr<FeatureDeviceHistory> fromJsonValue(RapidJsonValueType value);
virtual ~FeatureDeviceHistory();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

};


#endif
