#ifndef FeatureToFeatureHistory_H
#define FeatureToFeatureHistory_H




class FeatureHistory;
#include <memory>
#include <string>
#include <vector>
#include "cassandra.h"
#include "JsonTypeDefs.h"
#include "CassandraManagedType.h"

class Feature;
#include "Object.h"
class FeatureToFeatureHistory : public CassandraManagedType, public Object {

private:

public:

std::unique_ptr<Feature> feature;
std::vector<std::shared_ptr<FeatureHistory> > featureHistories;

FeatureToFeatureHistory(std::unique_ptr<Feature> feature);

std::string toString();

std::string toJson();

static std::shared_ptr<FeatureToFeatureHistory> fromJson(std::string json);
static std::shared_ptr<FeatureToFeatureHistory> fromJsonValue(RapidJsonValueType value);
virtual ~FeatureToFeatureHistory();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

};


#endif
