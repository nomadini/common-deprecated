

#include "GUtil.h"


#include "DeviceHistory.h"
#include "FeatureDeviceHistory.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "Feature.h"
#include "Device.h"
#include "DateTimeUtil.h"
#include "CassandraService.h"

FeatureDeviceHistory::FeatureDeviceHistory(
								std::unique_ptr<Feature> feature) : Object(__FILE__) {
								this->feature = std::move(feature);

}

FeatureDeviceHistory::~FeatureDeviceHistory() {

}

std::string FeatureDeviceHistory::toString() {
								return this->toJson();
}

std::string FeatureDeviceHistory::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

								addPropertiesToJsonValue(value, doc.get());

								return JsonUtil::valueToString(value);
}

std::shared_ptr<FeatureDeviceHistory> FeatureDeviceHistory::fromJsonValue(RapidJsonValueType value) {
								auto object = std::make_shared<FeatureDeviceHistory>(
																std::make_unique<Feature>(
																								JsonUtil::GetStringSafely (value, "type"),
																								JsonUtil::GetStringSafely (value, "feature"),
																								0)
																);
								JsonArrayUtil::getArrayFromValueMemeber(
																value,
																"devicehistories",
																object->devicehistories);
								return object;
}

std::shared_ptr<FeatureDeviceHistory> FeatureDeviceHistory::fromJson(std::string jsonModel) {

								auto document = parseJsonSafely(jsonModel);
								return fromJsonValue(*document);
}

void FeatureDeviceHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								RapidJsonValueTypeNoRef featureValue(rapidjson::kObjectType);
								feature->addPropertiesToJsonValue(featureValue, doc);
								JsonUtil::addMemberToValue_FromPair(doc, "feature", featureValue, value);

								JsonArrayUtil::addMemberToValue_FromPair(
																doc,
																"devicehistories",
																devicehistories,
																value
																);
}
