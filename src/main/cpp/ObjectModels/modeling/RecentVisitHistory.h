#ifndef RecentVisitHistory_H
#define RecentVisitHistory_H





#include "JsonTypeDefs.h"


#include "cassandra.h"
#include "AtomicLong.h"
#include "DateTimeMacro.h"
#include <unordered_map>

class Device;
class Feature;
#include "Object.h"
class RecentVisitHistory : public Object {
public:
std::string featureName;
std::string featureType;
TimeType timeSeenInMillis;

std::string toJson();
std::string getName();
RecentVisitHistory();
static std::shared_ptr<RecentVisitHistory> fromJson(std::string json);

static std::shared_ptr<RecentVisitHistory> fromJsonValue(RapidJsonValueType value);

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

virtual ~RecentVisitHistory();

};

#endif
