#include "GUtil.h"

#include "ModelUtil.h"
#include "ModelResult.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlModelResultService.h"
#include "MySqlDriver.h"
#include "TempUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include <shogun/lib/common.h>


std::string MySqlModelResultService::commonReadModelQuery =
        "SELECT "
        " id,"
        " model_request_id,"
        " num_neg_devices_used,"
        " num_pos_devices_used,"
        " feature_avg_num_history_used,"
        " negative_feature_used,"
        " positive_feature_used,"
        " feature_score_map,"
        " top_feature_score_map,"
        " red_flags,"
        " process_result,"
        " created_at,"
        " updated_at"
        " FROM `modelresult` ";



MySqlModelResultService::MySqlModelResultService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}



std::vector<std::shared_ptr<ModelResult> > MySqlModelResultService::readAllEntities() {
        std::vector<std::shared_ptr<ModelResult> > allModels;
        std::string query = MySqlModelResultService::commonReadModelQuery + StringUtil::toStr (" ;");
        //each target group has a vector which has 168 numbers which represents each hour of the week


        auto res = driver->executeQuery (query);
        while (res->next ()) {
                auto model = getModelFromResultSet (res);
                allModels.push_back (model);
        }


        return allModels;
}

std::shared_ptr<ModelResult> MySqlModelResultService::getModelFromResultSet(std::shared_ptr<ResultSetHolder> res) {
        assertAndThrow(res != NULL);
        std::shared_ptr<ModelResult> model = std::make_shared <ModelResult> ();

        model->id  = res->getInt("id");
        model->modelRequestId = res->getInt("model_request_id");

        model->numberOfNegativeDevicesUsed = res->getInt ("num_neg_devices_used");
        model->numberOfPositiveDevicesUsed = res->getInt ("num_pos_devices_used");

        std::string negativeFeaturesUsed = MySqlDriver::getString( res, "negative_feature_used");

        model->processResult = MySqlDriver::getString( res, "process_result");
        std::string redFlagsStr = MySqlDriver::getString( res, "red_flags");
        model->redFlags = parseJsonArrayFromString (_L_,redFlagsStr);

        std::string featureScoreMap = MySqlDriver::getString( res, "feature_score_map");
        std::string topFeatureScoreMap = MySqlDriver::getString( res, "top_feature_score_map");

        std::string positiveFeaturesUsed = MySqlDriver::getString( res, "positive_feature_used");

        std::vector<std::string> negFeats = parseJsonArrayFromString (_L_,negativeFeaturesUsed);
        model->negativeFeaturesUsed = CollectionUtil::convertListToSet<std::string> (negFeats);

        std::vector<std::string> posFeats = parseJsonArrayFromString (_L_,positiveFeaturesUsed);
        model->positiveFeaturesUsed = CollectionUtil::convertListToSet<std::string> (posFeats);

        model->featureScoreMap = getMapOfStringDoubleFromString (featureScoreMap);
        model->topFeatureScoreMap = getMapOfStringDoubleFromString (topFeatureScoreMap);

        model->createdAt = DateTimeUtil::parseDateTime(res->getString ("created_at"));
        model->updatedAt = DateTimeUtil::parseDateTime(res->getString ("updated_at"));

        model->validate ();
        return model;

}


std::string MySqlModelResultService::replaceStringsInModelQuery(std::string queryStr, std::shared_ptr<ModelResult> model) {
        assertAndThrow(model->modelRequestId >= 0);
        queryStr = strictReplace (queryStr, "_model_request_id",
                                  StringUtil::toStr (model->modelRequestId));
        queryStr = StringUtil::replaceString (queryStr, "__id",
                                              StringUtil::toStr (model->id));

        queryStr = strictReplace (queryStr, "_num_neg_devices_used",
                                  StringUtil::toStr (model->numberOfNegativeDevicesUsed));

        queryStr = strictReplace (queryStr, "_num_pos_devices_used",
                                  StringUtil::toStr (model->numberOfPositiveDevicesUsed));

        queryStr = strictReplace (queryStr, "_feature_avg_num_history_used",
                                  ModelUtil::convertFeatureIndexMapToJson (
                                          model->featureToAvgNumberOfHistoryUsed));

        queryStr = strictReplace (queryStr, "_negative_feature_used",
                                  JsonArrayUtil::convertListToJson (
                                          CollectionUtil::convertSetToList<std::string>
                                                  (model->negativeFeaturesUsed)));

        queryStr = strictReplace (queryStr, "__red_flags",
                                  JsonArrayUtil::convertListToJson (model->redFlags));

        queryStr = strictReplace (queryStr, "__process_result", model->processResult);

        queryStr = strictReplace (queryStr, "_positive_feature_used",
                                  JsonArrayUtil::convertListToJson (
                                          CollectionUtil::convertSetToList<std::string>
                                                  (model->positiveFeaturesUsed)));


        if (model->topFeatureScoreMap.empty ()) {
                LOG(WARNING)<<"topFeatureScoreMap is empty.";
        }

        std::string topFeatureScoreMapJson = model->convertFeatureScoreMapToJsonDocument (
                model, StringUtil::toStr (
                        "topFeatureScoreMap"));
        assertAndThrow(JsonUtil::isProperJson (topFeatureScoreMapJson));
        queryStr = strictReplace (queryStr, "_top_feature_score_map",
                                  topFeatureScoreMapJson);

        queryStr = strictReplace (queryStr, "_updated_at",
                                  DateTimeUtil::getNowInMySqlFormat ());


        assertAndWarn(!model->featureScoreMap.empty ());
        std::string featureScoreMapJson = model->convertFeatureScoreMapToJsonDocument (
                model, StringUtil::toStr (
                        "featureScoreMap"));
        assertAndThrow(JsonUtil::isProperJson (featureScoreMapJson));
        queryStr = strictReplace (queryStr, "__feature_score_map",
                                  featureScoreMapJson);

        return queryStr;

}

std::shared_ptr<ModelResult> MySqlModelResultService::upsertModel(std::shared_ptr<ModelResult> model) {

        //a model must have the unqiue combination of name and advertiser id. its enforced in db too
        if (readModelByRequestId (model->modelRequestId) != nullptr) {
                return updateTheModel (model);
        } else {
                return insert (model);
        }

}

std::shared_ptr<ModelResult> MySqlModelResultService::readModelByRequestId(int modelRequestId) {

        std::string query = MySqlModelResultService::commonReadModelQuery + " where model_request_id = __ID__ ;";
        query = strictReplace (query, "__ID__", StringUtil::toStr (modelRequestId));
        auto res = driver->executeQuery (query);
        while (res->next ()) {
                return getModelFromResultSet (res);

        }
        return nullptr;
}

std::string MySqlModelResultService::strictReplace(std::string main, std::string old, std::string newStr) {
        return StringUtil::replaceString(main, old, newStr, true);
}

std::vector<int> MySqlModelResultService::parseJsonArrayOfIntegersFromString(std::string arrayInString) {
        MLOG(3) << "parseJsonArrayOfIntegersFromString : arrayInString : " << arrayInString;
        if (arrayInString.empty ()) {
                MLOG(3) << "returning empty list of strings, because input is empty";
                std::vector<int> empty;
                return empty;
        }
        std::vector<int> values;
        JsonArrayUtil::getArrayOfObjectsFromJsonString (arrayInString, values);
        return values;
}


std::vector<std::string> MySqlModelResultService::parseJsonArrayFromString(std::string line, std::string arrayInString) {
        MLOG(3) << "line : " <<line<< " , parseJsonArrayFromString : arrayInString : " << arrayInString;
        if (arrayInString.empty ()) {
                MLOG(3) << "returning empty list of strings, because input is empty";
                std::vector<std::string> empty;
                return empty;
        }
        std::vector<std::string> values;
        JsonArrayUtil::getArrayOfObjectsFromJsonString (arrayInString, values);
        return values;

}

std::unordered_map<std::string, double> MySqlModelResultService::getMapOfStringDoubleFromString(std::string mapDoubleString) {
        MLOG(3) << "getMapOfStringDoubleFromString : mapDoubleString :  " << mapDoubleString;
        std::unordered_map<std::string, double> map;
        if (mapDoubleString.empty()) {
                return map;
        }
        JsonMapUtil::read_Map_From_Value(*parseJsonSafely(mapDoubleString), map);
        return map;
}

std::unordered_map<std::string, int> MySqlModelResultService::getMapOfStringIntFromString(std::string mapInString) {
        MLOG(3) << "getMapOfStringIntFromString : mapInString :  " << mapInString;
        std::unordered_map<std::string, int> map;
        if (mapInString.empty()) {
                return map;
        }
        JsonMapUtil::read_Map_From_Value<std::string, int> (*parseJsonSafely(mapInString), map);
        return map;
}

std::shared_ptr<ModelResult> MySqlModelResultService::updateTheModel(std::shared_ptr<ModelResult> model) {
        std::string queryStr =
                " UPDATE modelresult SET "
                " model_request_id = _model_request_id,"
                " num_neg_devices_used = _num_neg_devices_used,"
                " num_pos_devices_used = _num_pos_devices_used,"
                " feature_avg_num_history_used = '_feature_avg_num_history_used',"
                " negative_feature_used = '_negative_feature_used',"
                " positive_feature_used = '_positive_feature_used',"
                " feature_score_map = '__feature_score_map',"
                " top_feature_score_map = '_top_feature_score_map',"
                " process_result = '__process_result',"
                " red_flags = '__red_flags',"
                " updated_at  = '_updated_at' "
                " where model_request_id = _model_request_id";

        MLOG(3) << "update The ModelResult in db : " << model->toJson ();

        queryStr = replaceStringsInModelQuery (queryStr, model);
        MLOG(3) << "queryStr : " << queryStr;


        driver->executedUpdateStatement (queryStr);

        return model;
}

void MySqlModelResultService::deleteAll() {
        driver->deleteAll("model");
}

std::shared_ptr<ModelResult> MySqlModelResultService::insert(std::shared_ptr<ModelResult> model) {
        std::string queryStr =
                "INSERT INTO "
                " modelresult "
                "( "
                " model_request_id,"
                " num_neg_devices_used,"
                " num_pos_devices_used,"
                " feature_avg_num_history_used,"
                " negative_feature_used,"
                " positive_feature_used,"
                " feature_score_map,"
                " top_feature_score_map,"
                " process_result,"
                " red_flags,"
                " updated_at"
                " )"
                " VALUES"
                " ( "
                " _model_request_id,"
                " _num_neg_devices_used,"
                " _num_pos_devices_used,"
                " '_feature_avg_num_history_used',"
                " '_negative_feature_used',"
                " '_positive_feature_used',"
                " '__feature_score_map',"
                " '_top_feature_score_map',"
                " '__process_result',"
                " '__red_flags',"
                " '_updated_at'"
                " ); ";
        MLOG(3) << "inserting new model in db : " << model->toJson ();
        queryStr = replaceStringsInModelQuery (queryStr, model);
        MLOG(3) << "model persist queryStr : " << queryStr;


        driver->executedUpdateStatement (queryStr);
        model->id = driver->getLastInsertedId ();
        MLOG(3) << "mode with id :  " << model->id << " was inserted";

        return model;
}


MySqlModelResultService::~MySqlModelResultService() {

}
