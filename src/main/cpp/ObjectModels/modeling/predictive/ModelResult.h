//
// Created by Mahmoud Taabodi on 11/19/15.
//

#ifndef ModelResult_H
#define ModelResult_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <set>
#include "JsonTypeDefs.h"
#include "Object.h"
#include "Poco/DateTime.h"

class ModelResult : public std::enable_shared_from_this<ModelResult>, public Object {

private:

public:

static constexpr const char *PROCESS_RESULT_STARTED = "started";
static constexpr const char *PROCESS_RESULT_CREATED = "created";
static constexpr const char *PROCESS_RESULT_FAILED = "failed";

int id;
int modelRequestId;
int numberOfNegativeDevicesUsed;
int numberOfPositiveDevicesUsed;

Poco::DateTime createdAt;
Poco::DateTime updatedAt;

std::unordered_map<std::string, int> featureToAvgNumberOfHistoryUsed;
std::set<std::string> negativeFeaturesUsed;
std::set<std::string> positiveFeaturesUsed;

std::vector<std::string> redFlags;
std::string processResult;

//featureScoreMap is only for reference and debugging
std::unordered_map<std::string, double> featureScoreMap;
//we score based on topFeatureScoreMap
std::unordered_map<std::string, double> topFeatureScoreMap;

virtual void validate();

ModelResult();

virtual std::string toString();

virtual std::string toJson();
std::string toshortString();

static std::shared_ptr<ModelResult> fromJson(std::string json);

std::string convertFeatureScoreMapToJsonDocument(std::shared_ptr<ModelResult> model, std::string nameOfMap);

double getScoreOfTopFeatures(std::string feature);

static std::string getEntityName();
virtual ~ModelResult();

int getId();

static std::shared_ptr<ModelResult> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
