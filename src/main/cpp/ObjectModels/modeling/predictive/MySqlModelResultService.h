#ifndef MySqlModelResultService_h
#define MySqlModelResultService_h


#include "ModelResult.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"

class MySqlModelResultService;
#include "Object.h"



class MySqlModelResultService : public DataProvider<ModelResult>, public Object {

public:

static std::string commonReadModelQuery;

MySqlDriver* driver;

MySqlModelResultService(MySqlDriver* driver);
std::shared_ptr<ModelResult> readModelByRequestId(int modelRequestId);

std::vector<std::shared_ptr<ModelResult> > readAllEntities();

std::vector<std::string> parseJsonArrayFromString(std::string line, std::string arrayInString);

std::unordered_map<std::string, double> getMapOfStringDoubleFromString(std::string mapDoubleString);

std::unordered_map<std::string, int> getMapOfStringIntFromString(std::string mapInString);


std::shared_ptr<ModelResult> getModelFromResultSet(std::shared_ptr<ResultSetHolder> res);

std::shared_ptr<ModelResult> upsertModel(std::shared_ptr<ModelResult> model);
std::shared_ptr<ModelResult> updateTheModel(std::shared_ptr<ModelResult> model);

std::shared_ptr<ModelResult> insert(std::shared_ptr<ModelResult> model);

void deleteAll();

std::string strictReplace(std::string main, std::string old, std::string newStr);

std::string replaceStringsInModelQuery(std::string queryStr, std::shared_ptr<ModelResult> model);

std::vector<int> parseJsonArrayOfIntegersFromString(std::string arrayInString);

virtual ~MySqlModelResultService();

};


#endif
