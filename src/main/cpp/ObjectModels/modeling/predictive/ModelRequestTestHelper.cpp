
#include "ModelRequestTestHelper.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "OfferTestHelper.h"

ModelRequestTestHelper::ModelRequestTestHelper() : Object(__FILE__) {
}

ModelRequestTestHelper::~ModelRequestTestHelper() {

}

std::shared_ptr<ModelRequest> ModelRequestTestHelper::createSeedModelRequest() {

								std::shared_ptr<ModelRequest> req(new ModelRequest());

								req->name="hossein-seedModel";
								req->modelType=ModelRequest::seedModel;
								req->seedWebsites.insert(StringUtil::toStr("www.cnn.com"));
								req->segmentSeedName = "cnn-visitor-today";
								req->numberOfNegativeDevicesToBeUsed = 100;
								req->numberOfPositiveDevicesToBeUsed  = 100;
								req->maxNumberOfDeviceHistoryPerFeature = 100;
								req->numberOfTopFeatures =  10;
								req->cutOffScore = 2;
								req->maxNumberOfNegativeFeaturesToPick = 100;
								req->featureRecencyInSecond = 3600;

								req->algorithmToModelBasedOn = StringUtil::toStr("alpha");
								req->dateOfRequest = DateTimeUtil::getNowInMySqlFormat();
								req->advertiserId = 1001;

								return req;
}

std::shared_ptr<ModelRequest> ModelRequestTestHelper::createPixelModelRequest() {

								std::shared_ptr<ModelRequest> req(new ModelRequest());

								req->name="hassan-pixelModel";
								req->modelType=ModelRequest::pixelModel;
								req->positiveOfferIds = OfferTestHelper::getRandomOfferIds(10);
								req->negativeOfferIds = OfferTestHelper::getRandomOfferIds(10);//have a test case that fixes
								req->segmentSeedName = "cnn-visitor-today";
								req->advertiserId = 1000;
								req->numberOfNegativeDevicesToBeUsed = 100;
								req->numberOfPositiveDevicesToBeUsed  = 100;
								req->numberOfTopFeatures =  10;
								req->cutOffScore = 2;

								req->featureRecencyInSecond = 3600;
								req->pixelHitRecencyInSecond  = 1000;

								req->algorithmToModelBasedOn = StringUtil::toStr("alpha");
								req->dateOfRequest = DateTimeUtil::getNowInMySqlFormat();
								req->maxNumberOfDeviceHistoryPerFeature = 100;
								return req;
}

std::shared_ptr<ModelRequest> ModelRequestTestHelper::createBuiltPixelModel() {
								auto model = ModelRequestTestHelper::createPixelModelRequest();
								model->numberOfNegativeDevicesToBeUsed =101;
								model->numberOfPositiveDevicesToBeUsed =101;

								return model;
}


std::shared_ptr<ModelRequest> ModelRequestTestHelper::createBuiltPixelModelWithFeatures(
								std::unordered_map<std::string, double> featureScoreMap) {
								auto model = ModelRequestTestHelper::createPixelModelRequest();
								model->numberOfNegativeDevicesToBeUsed =101;
								model->numberOfPositiveDevicesToBeUsed =101;

								model->modelResult->featureScoreMap = featureScoreMap;
								model->modelResult->topFeatureScoreMap = featureScoreMap;

								return model;
}
std::shared_ptr<ModelRequest> ModelRequestTestHelper::createBuiltSeedModel() {
								auto model = ModelRequestTestHelper::createSeedModelRequest();
								model->seedWebsites.insert("sample.com");
								model->negativeFeaturesRequested.insert("sample-feature.com");
								model->maxNumberOfDeviceHistoryPerFeature =102;
								model->maxNumberOfNegativeFeaturesToPick=102;
								return model;
}
