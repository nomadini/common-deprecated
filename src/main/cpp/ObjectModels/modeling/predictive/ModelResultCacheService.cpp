#include "ModelResult.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "ModelResultCacheService.h"
#include "MySqlModelResultService.h"
#include "JsonArrayUtil.h"

ModelResultCacheService::ModelResultCacheService(
        MySqlModelResultService* mySqlModelResultServiceArg,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<ModelResult>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {
        modelRequestIdToResultMap = std::make_shared<gicapods::ConcurrentHashMap<int,
                                                                                 ModelResult> > ();
}

void ModelResultCacheService::clearOtherCaches() {

        modelRequestIdToResultMap->clear();

}

void ModelResultCacheService::populateOtherMapsAndLists(
        std::vector<std::shared_ptr<ModelResult> > allEntities) {
        for(auto result : allEntities) {
                if(result->modelRequestId > 0) {
                        //some models might not have any result. we don't want to put the in the map
                        MLOG(2)<<" popupating modelRequestIdToResultMap with "<< result->toJson();
                        modelRequestIdToResultMap->put(result->modelRequestId, result);
                } else {
                        LOG(WARNING)<<"model result with no request id : "<<result->toJson();
                }
        }
}

ModelResultCacheService::~ModelResultCacheService() {

}
