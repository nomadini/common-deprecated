#ifndef ModelRequestTestHelper_H
#define ModelRequestTestHelper_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "ModelRequest.h"
#include "Object.h"
class ModelRequestTestHelper;

class ModelRequestTestHelper : public Object {

public:
ModelRequestTestHelper();
static std::shared_ptr<ModelRequest> createSeedModelRequest();
static std::shared_ptr<ModelRequest> createPixelModelRequest();
static std::shared_ptr<ModelRequest> createBuiltSeedModel();
static std::shared_ptr<ModelRequest> createBuiltPixelModel();
static std::shared_ptr<ModelRequest> createBuiltPixelModelWithFeatures(std::unordered_map<std::string, double> featureScoreMap);

virtual ~ModelRequestTestHelper();
};

#endif
