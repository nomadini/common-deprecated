#include "GUtil.h"

#include "ModelUtil.h"
#include "ModelRequest.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlModelService.h"
#include "MySqlDriver.h"
#include "TempUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include <shogun/lib/common.h>

std::string MySqlModelService::commonReadModelQuery =
        "SELECT "
        " `name`,"
        " `advertiser_id`,"
        " `id`,"
        " `seed_web_sites`,"
        " `algo`,"
        " `segment_name_seed`,"
        " `description`,"
        " `feature_recency_in_sec`,"
        " `rebuild_model_on_next_run`,"
        " `approved_for_scoring`,"
        " `negative_features_requested`,"
        " `date_of_request`,"
        " `max_number_of_history_per_device`,"
        " `updated_at`,"
        " `created_at`,"
        " `positive_offer_ids`, "
        " `negative_offer_ids`,"

        " `model_type`,"

        " `cut_off_score`,"
        " `cut_off_type`,"
        " `pixel_hit_recency_in_seconds`,"
        " `max_number_of_device_history_per_feature`, "
        " `max_number_of_negative_feature_to_pick`, "

        " `number_of_positive_device_to_be_used_for_modeling`, "
        " `number_of_negative_device_to_be_used_for_modeling` ,"

        " `parent_model_request_id`, "
        " `description`, "
        " `date_of_request_completion` "
        " FROM `model` ";



MySqlModelService::MySqlModelService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}



std::vector<std::shared_ptr<ModelRequest> > MySqlModelService::readAllEntities() {
        std::vector<std::shared_ptr<ModelRequest> > allModels;
        std::string query = MySqlModelService::commonReadModelQuery + StringUtil::toStr (" ;");
        //each target group has a vector which has 168 numbers which represents each hour of the week


        auto res = driver->executeQuery (query);
        while (res->next ()) {
                auto model = getModelFromResultSet (res);
                allModels.push_back (model);
        }


        return allModels;
}

std::shared_ptr<ModelRequest> MySqlModelService::getModelFromResultSet(std::shared_ptr<ResultSetHolder> res) {
        assertAndThrow(res != NULL);
        std::shared_ptr<ModelRequest> model = std::make_shared <ModelRequest> ();


        std::string name = MySqlDriver::getString( res, "name");
        int advertiserId = res->getInt("advertiser_id");
        int id  = res->getInt("id");
        std::string seedWebsites = MySqlDriver::getString( res, "seed_web_sites");
        std::string algorithm = MySqlDriver::getString( res, "algo");
        std::string segmentSeedName = MySqlDriver::getString( res, "segment_name_seed");
        std::string description = MySqlDriver::getString( res, "description");
        int featureRecencyInSecond = res->getInt ("feature_recency_in_sec");

        std::string negativeFeaturesRequested = MySqlDriver::getString( res, "negative_features_requested");

        int parentModelRequestId = res->getInt("parent_model_request_id");

        std::string dateModifiedStr = MySqlDriver::getString( res, "updated_at");
        std::string modelType = MySqlDriver::getString( res, "model_type");

        std::string dateRequestStr = MySqlDriver::getString(  res, "date_of_request");
        int pixelHitRecencyInSecond = res->getInt ("pixel_hit_recency_in_seconds");
        int rebuildModelOnNextRunInt = res->getInt ("rebuild_model_on_next_run");
        int approvedForScoringInt = res->getInt ("approved_for_scoring");
        std::string positiveOfferIds = MySqlDriver::getString( res, "positive_offer_ids");
        std::string negativeOfferIds = MySqlDriver::getString( res, "negative_offer_ids");

        int maxNumberOfDeviceHistoryPerFeature = res->getInt ("max_number_of_device_history_per_feature");
        int maxNumberOfNegativeFeaturesToPick = res->getInt ("max_number_of_negative_feature_to_pick");

        int numberOfPositiveDevicesToBeUsed =
                res->getInt ("number_of_positive_device_to_be_used_for_modeling");
        int maxNumberOfHistoryPerDevice =
                res->getInt ("max_number_of_history_per_device");

        int numberOfNegativeDevicesToBeUsed =
                res->getInt ("number_of_negative_device_to_be_used_for_modeling");

        std::string createdAt = MySqlDriver::getString( res, "created_at");
        std::string dateOfRquestCompletion = MySqlDriver::getString( res, "date_of_request_completion");


        double cutOffScore = res->getDouble ("cut_off_score");
        std::string cutOffType = res->getString ("cut_off_type");

        MLOG(3) << "values loaded for model name :  " << name << ", "
                " seedWebsites :  " << seedWebsites << ", algorithm :  " << algorithm
                << ", segemntSeedName :  " << segmentSeedName << ","
                "dateRequestStr :  " << dateRequestStr <<
                ", featureRecencyInSecond :  " << featureRecencyInSecond << ","

                << ", modelType :  " << modelType << " "
                " pixelHitRecencyInSecond :  " << pixelHitRecencyInSecond << ", positiveOfferIds:  " <<
                positiveOfferIds
                << ", negativeOfferIds :  " << negativeOfferIds << " "
                " seedWebsites :  " << seedWebsites <<
                ", maxNumberOfDeviceHistoryPerFeature:  " << maxNumberOfDeviceHistoryPerFeature << ", "
                " maxNumberOfNegativeFeaturesToPick :  " << maxNumberOfNegativeFeaturesToPick << " , id :" << id;

        model->id = id;
        model->name = name;
        model->seedWebsites = CollectionUtil::convertListToSet<std::string>(parseJsonArrayFromString (_L_,seedWebsites));
        model->algorithmToModelBasedOn = algorithm;
        model->segmentSeedName = segmentSeedName;
        model->dateOfRequest = dateRequestStr;

        model->featureRecencyInSecond = featureRecencyInSecond;
        model->rebuildModelOnNextRun = (rebuildModelOnNextRunInt == 1 ? true : false);
        model->approvedForScoring = (approvedForScoringInt == 1 ? true : false);
        model->negativeFeaturesRequested = CollectionUtil::convertListToSet<std::string> (parseJsonArrayFromString (_L_,negativeFeaturesRequested));



        model->parentModelRequestId = parentModelRequestId;
        model->modelType = modelType;


        model->pixelHitRecencyInSecond = pixelHitRecencyInSecond;
        model->positiveOfferIds = CollectionUtil::convertListToSet<int>(parseJsonArrayOfIntegersFromString (positiveOfferIds));
        model->negativeOfferIds = CollectionUtil::convertListToSet<int>(parseJsonArrayOfIntegersFromString (negativeOfferIds));

        model->seedWebsites =
                CollectionUtil::convertListToSet<std::string>(parseJsonArrayFromString (_L_,seedWebsites));
        model->maxNumberOfDeviceHistoryPerFeature = maxNumberOfDeviceHistoryPerFeature;
        model->maxNumberOfNegativeFeaturesToPick = maxNumberOfNegativeFeaturesToPick;

        model->maxNumberOfHistoryPerDevice = maxNumberOfHistoryPerDevice;
        model->numberOfPositiveDevicesToBeUsed = numberOfPositiveDevicesToBeUsed;
        model->numberOfNegativeDevicesToBeUsed = numberOfNegativeDevicesToBeUsed;

        model->advertiserId = advertiserId;

        model->cutOffScore = cutOffScore;
        model->cutOffType = cutOffType;
        model->createdAt  =  createdAt;
        model->dateOfRquestCompletion = dateOfRquestCompletion;
        model->description = description;


        model->validate ();
        return model;

}


std::string MySqlModelService::replaceStringsInModelQuery(std::string queryStr, std::shared_ptr<ModelRequest> model) {

        queryStr = strictReplace (queryStr, "__ALGO__",
                                  StringUtil::toStr (model->algorithmToModelBasedOn));
        queryStr = strictReplace (queryStr, "__ADV_ID__",
                                  StringUtil::toStr (model->advertiserId));

        queryStr = strictReplace (queryStr, "__NAME__",
                                  model->name);

        queryStr = strictReplace (queryStr, "__SEGMENT_SEED_NAME__",
                                  model->segmentSeedName);

        queryStr = strictReplace (queryStr, "__DATE_OF_REQUEST__",
                                  model->dateOfRequest);



        queryStr = strictReplace (queryStr, "__FEATURE_RECENCY_IN_SEC__",
                                  StringUtil::toStr (model->featureRecencyInSecond));



        queryStr = strictReplace (queryStr, "__NEGATIVE_FEATURES_REQUESTED__",
                                  JsonArrayUtil::convertListToJson (
                                          CollectionUtil::convertSetToList<std::string>(
                                                  model->negativeFeaturesRequested)));


        queryStr = strictReplace (queryStr, "__DATE_OF_REQUEST_COMPLETION__",
                                  DateTimeUtil::getNowInMySqlFormat ());

        queryStr = strictReplace (queryStr, "__description__", model->description);

        queryStr = strictReplace (queryStr, "__updated_at__",
                                  DateTimeUtil::getNowInMySqlFormat ());            //TODO : check this , if it works with date time in mysql

        queryStr = strictReplace (queryStr, "__MODEL_TYPE__",
                                  model->modelType);


        queryStr = strictReplace (queryStr, "__PIXEL_HIT_RECENCY_IN_SECONDS__",
                                  StringUtil::toStr (model->pixelHitRecencyInSecond));

        queryStr = strictReplace (queryStr, "__rebuildModelOnNextRun__",
                                  model->rebuildModelOnNextRun == true ? _toStr (1) : _toStr(0));

        queryStr = strictReplace (queryStr, "__approvedForScoring__",
                                  model->approvedForScoring == true ? _toStr (1) : _toStr(0));

        queryStr = strictReplace (queryStr, "__POSITIVE_OFFER_IDS__",
                                  JsonArrayUtil::convertListToJson (
                                          CollectionUtil::convertSetToList<int>(model->positiveOfferIds)));
        queryStr = strictReplace (queryStr, "__NEGATIVE_OFFER_IDS__",
                                  JsonArrayUtil::convertListToJson (
                                          CollectionUtil::convertSetToList<int>(model->negativeOfferIds)));

        queryStr = strictReplace (queryStr, "__SEED_WEB_SITES__",
                                  JsonArrayUtil::convertListToJson (
                                          CollectionUtil::convertSetToList<std::string>(model->seedWebsites)));

        queryStr = strictReplace (queryStr, "__MAX_NUMBER_OF_DEVICE_HISTORY_PER_FEATURE__",
                                  StringUtil::toStr (model->maxNumberOfDeviceHistoryPerFeature));
        queryStr = strictReplace (queryStr, "__max_number_of_negative_feature_to_pick__",
                                  StringUtil::toStr (model->maxNumberOfNegativeFeaturesToPick));


        queryStr = strictReplace (queryStr, "__max_number_of_history_per_device__",
                                  StringUtil::toStr (model->maxNumberOfHistoryPerDevice));

        queryStr = strictReplace (queryStr, "__number_of_positive_device_to_be_used_for_modeling__",
                                  StringUtil::toStr (model->numberOfPositiveDevicesToBeUsed));
        queryStr = strictReplace (queryStr, "__number_of_negative_device_to_be_used_for_modeling__",
                                  StringUtil::toStr (model->numberOfNegativeDevicesToBeUsed));

        queryStr = strictReplace (queryStr, "__CUT_OFF_SCORE__", StringUtil::toStr (model->cutOffScore));
        queryStr = strictReplace (queryStr, "__cut_off_type__", model->cutOffType);
        queryStr = strictReplace (queryStr, "__parent_model_request_id__", _toStr(model->parentModelRequestId));

        return queryStr;

}

std::shared_ptr<ModelRequest> MySqlModelService::fetchModelBySegmentSeedNameAndAdvertiserIdOptional(
        std::string segmentSeedName,
        int advertiserId) {
        assertAndThrow(!segmentSeedName.empty ());
        assertAndThrow(advertiserId > 0);

        std::string query = MySqlModelService::commonReadModelQuery + StringUtil::toStr (
                " where segment_name_seed = '__segmentSeedName__' AND "
                " ADVERTISER_ID = __ADV_ID__ ;");
        std::shared_ptr<ModelRequest> model = nullptr;
        //each target group has a vector which has 168 numbers which represents each hour of the week
        query = strictReplace (query, "__segmentSeedName__", segmentSeedName);

        query = strictReplace (query, "__ADV_ID__", StringUtil::toStr (advertiserId));

        MLOG(3) << "query : " << query;


        auto res = driver->executeQuery (query);


        while (res->next ()) {
                model = getModelFromResultSet (res);
        }


        return model;
}


std::shared_ptr<ModelRequest> MySqlModelService::readModelById(int modelId) {

        std::string query = MySqlModelService::commonReadModelQuery + " where id = __ID__ ;";
        query = strictReplace (query, "__ID__", StringUtil::toStr (modelId));
        auto res = driver->executeQuery (query);
        while (res->next ()) {
                return getModelFromResultSet (res);

        }
        return nullptr;
}

std::string MySqlModelService::strictReplace(std::string main, std::string old, std::string newStr) {
        return StringUtil::replaceString(main, old, newStr, true);
}

std::shared_ptr<ModelRequest> MySqlModelService::readModelByNameAndAdvertiserId(std::string name, int advertiserId) {
        assertAndThrow(!name.empty ());
        assertAndThrow(advertiserId > 0);

        std::string query = MySqlModelService::commonReadModelQuery + StringUtil::toStr (
                " where NAME = '__NAME__' AND "
                " ADVERTISER_ID = __ADV_ID__ ;");
        //each target group has a vector which has 168 numbers which represents each hour of the week
        query = strictReplace (query, "__NAME__", name);

        query = strictReplace (query, "__ADV_ID__", StringUtil::toStr (advertiserId));

        MLOG(3) << "query : " << query;


        auto res = driver->executeQuery (query);


        while (res->next ()) {
                return getModelFromResultSet (res);
        }

        return nullptr;

}


std::vector<int> MySqlModelService::parseJsonArrayOfIntegersFromString(std::string arrayInString) {
        MLOG(3) << "parseJsonArrayOfIntegersFromString : arrayInString : " << arrayInString;
        if (arrayInString.empty ()) {
                MLOG(3) << "returning empty list of strings, because input is empty";
                std::vector<int> empty;
                return empty;
        }
        std::vector<int> values;
        JsonArrayUtil::getArrayOfObjectsFromJsonString (arrayInString, values);
        return values;
}


std::vector<std::string> MySqlModelService::parseJsonArrayFromString(std::string line, std::string arrayInString) {
        MLOG(3) << "line : " <<line<< " , parseJsonArrayFromString : arrayInString : " << arrayInString;
        if (arrayInString.empty ()) {
                MLOG(3) << "returning empty list of strings, because input is empty";
                std::vector<std::string> empty;
                return empty;
        }
        std::vector<std::string> values;
        JsonArrayUtil::getArrayOfObjectsFromJsonString (arrayInString, values);
        return values;

}

std::unordered_map<std::string, double> MySqlModelService::getMapOfStringDoubleFromString(std::string mapDoubleString) {
        MLOG(3) << "getMapOfStringDoubleFromString : mapDoubleString :  " << mapDoubleString;
        std::unordered_map<std::string, double> map;
        if (mapDoubleString.empty()) {
                return map;
        }
        JsonMapUtil::read_Map_From_Value(*parseJsonSafely(mapDoubleString), map);
        return map;
}

std::unordered_map<std::string, int> MySqlModelService::getMapOfStringIntFromString(std::string mapInString) {
        MLOG(3) << "getMapOfStringIntFromString : mapInString :  " << mapInString;
        std::unordered_map<std::string, int> map;
        if (mapInString.empty()) {
                return map;
        }
        JsonMapUtil::read_Map_From_Value<std::string, int> (*parseJsonSafely(mapInString), map);
        return map;
}


std::shared_ptr<ModelRequest> MySqlModelService::upsertModel(std::shared_ptr<ModelRequest> model) {

        //a model must have the unqiue combination of name and advertiser id. its enforced in db too
        auto modelInDb = readModelByNameAndAdvertiserId (model->name, model->advertiserId);
        if ( modelInDb != nullptr) {
                model->id = modelInDb->id;
                return updateTheModel (model);
        } else {
                return insert (model);
        }

}

std::shared_ptr<ModelRequest> MySqlModelService::updateTheModel(std::shared_ptr<ModelRequest> model) {
        std::string queryStr =
                "UPDATE model SET "
                " ALGO = '__ALGO__', "
                " SEED_WEB_SITES = '__SEED_WEB_SITES__',"
                " SEGMENT_NAME_SEED = '__SEGMENT_SEED_NAME__',"
                " DATE_OF_REQUEST = '__DATE_OF_REQUEST__',"
                " DATE_OF_REQUEST_COMPLETION = '__DATE_OF_REQUEST_COMPLETION__' ,"
                " description = '__description__' ,"
                " FEATURE_RECENCY_IN_SEC = __FEATURE_RECENCY_IN_SEC__,"

                " rebuild_model_on_next_run = __rebuildModelOnNextRun__ ,"
                " approved_for_scoring = __approvedForScoring__ ,"
                " NEGATIVE_FEATURES_REQUESTED = '__NEGATIVE_FEATURES_REQUESTED__',"
                " updated_at  = '__updated_at__',"
                " MODEL_TYPE  = '__MODEL_TYPE__',   "

                " PIXEL_HIT_RECENCY_IN_SECONDS   = '__PIXEL_HIT_RECENCY_IN_SECONDS__'  , "//19
                " POSITIVE_OFFER_IDS= '__POSITIVE_OFFER_IDS__'  ,"//20
                " NEGATIVE_OFFER_IDS= '__NEGATIVE_OFFER_IDS__'  ,"//21

                " seed_web_sites= '__SEED_WEB_SITES__'  ,"//22
                " MAX_NUMBER_OF_DEVICE_HISTORY_PER_FEATURE= '__MAX_NUMBER_OF_DEVICE_HISTORY_PER_FEATURE__'  ,"//23
                " max_number_of_negative_feature_to_pick= '__max_number_of_negative_feature_to_pick__', "//24


                " max_number_of_history_per_device = '__max_number_of_history_per_device__',"//25
                " number_of_positive_device_to_be_used_for_modeling = '__number_of_positive_device_to_be_used_for_modeling__',"//25
                " number_of_negative_device_to_be_used_for_modeling = '__number_of_negative_device_to_be_used_for_modeling__' ,"//26

                " date_of_request = CURRENT_TIMESTAMP(), "
                " cut_off_type = '__cut_off_type__', "
                " parent_model_request_id = __parent_model_request_id__, "
                " CUT_OFF_SCORE ='__CUT_OFF_SCORE__' "

                "  WHERE NAME = '__NAME__' AND ADVERTISER_ID = __ADV_ID__";


        MLOG(3) << "updateTheModel in db : " << model->toJson ();
        ModelRequest::verifyModelHasRequiredFields (model, false);

        queryStr = replaceStringsInModelQuery (queryStr, model);
        //MLOG(3) << "queryStr : " << queryStr;


        driver->executedUpdateStatement (queryStr);

        return model;
}

std::vector<std::shared_ptr<ModelRequest> > MySqlModelService::readModelRequestsOlderThan(int hours) {
        std::vector<std::shared_ptr<ModelRequest> > staleModels;
        assertAndThrow(hours > 0);
        std::string query = MySqlModelService::commonReadModelQuery + StringUtil::toStr (
                " where DATE_OF_REQUEST < DATE_SUB(UTC_TIMESTAMP(), INTERVAL __HOUR__ HOUR)");

        MLOG(3) << "readModelRequestsOlderThan";
        //each target group has a vector which has 168 numbers which represents each hour of the week
        query = strictReplace (query, "__HOUR__", StringUtil::toStr (hours));

        MLOG(3) << "query : " << query;


        auto res = driver->executeQuery (query);


        while (res->next ()) {
                auto model = getModelFromResultSet (res);
                staleModels.push_back (model);
        }



        return staleModels;
}

void MySqlModelService::deleteAll() {
        driver->deleteAll("model");
}

std::shared_ptr<ModelRequest> MySqlModelService::insert(std::shared_ptr<ModelRequest> model) {
        ModelRequest::verifyModelHasRequiredFields (model, false);
        std::string queryStr =
                "INSERT INTO "
                " model "
                "( ALGO,"
                " NAME,"
                " ADVERTISER_ID,"
                " SEGMENT_NAME_SEED,"
                " DATE_OF_REQUEST,"
                " date_of_request_completion,"
                " description,"

                " FEATURE_RECENCY_IN_SEC,"

                " rebuild_model_on_next_run,"
                " approved_for_scoring,"
                " NEGATIVE_FEATURES_REQUESTED,"
                " updated_at,"
                " MODEL_TYPE,"
                " PIXEL_HIT_RECENCY_IN_SECONDS,"//19
                " POSITIVE_OFFER_IDS ,"//20
                " NEGATIVE_OFFER_IDS,"//21
                " seed_web_sites,"//22
                " MAX_NUMBER_OF_DEVICE_HISTORY_PER_FEATURE,"//23
                " max_number_of_negative_feature_to_pick, "//24
                " max_number_of_history_per_device, "//24
                " number_of_positive_device_to_be_used_for_modeling, "//25
                " number_of_negative_device_to_be_used_for_modeling, "//26

                " cut_off_type, "//27
                " parent_model_request_id, "//27
                " CUT_OFF_SCORE "
                " )"
                " VALUES"
                " ('__ALGO__',"
                "'__NAME__',"
                " __ADV_ID__,"
                "'__SEGMENT_SEED_NAME__',"
                "'__DATE_OF_REQUEST__',"
                "'__DATE_OF_REQUEST_COMPLETION__',"
                "'__description__',"

                "'__FEATURE_RECENCY_IN_SEC__',"

                "'__rebuildModelOnNextRun__',"
                "'__approvedForScoring__',"
                "'__NEGATIVE_FEATURES_REQUESTED__',"
                "'__updated_at__',"
                "'__MODEL_TYPE__',"
                "'__PIXEL_HIT_RECENCY_IN_SECONDS__',"
                "'__POSITIVE_OFFER_IDS__',"
                "'__NEGATIVE_OFFER_IDS__',"
                "'__SEED_WEB_SITES__',"
                "'__MAX_NUMBER_OF_DEVICE_HISTORY_PER_FEATURE__',"
                "'__max_number_of_negative_feature_to_pick__',"
                "'__max_number_of_history_per_device__',"
                "'__number_of_positive_device_to_be_used_for_modeling__',"
                "'__number_of_negative_device_to_be_used_for_modeling__',"

                "'__cut_off_type__',"
                "'__parent_model_request_id__',"
                " '__CUT_OFF_SCORE__' "
                " ); ";
        MLOG(3) << "inserting new model in db : " << model->toJson ();
        queryStr = replaceStringsInModelQuery (queryStr, model);
        MLOG(3) << "model persist queryStr : " << queryStr;


        driver->executedUpdateStatement (queryStr);
        model->id = driver->getLastInsertedId ();
        MLOG(3) << "mode with id :  " << model->id << " was inserted";

        return model;
}


MySqlModelService::~MySqlModelService() {

}

std::shared_ptr<MySqlModelService> MySqlModelService::getInstance(
        gicapods::ConfigService* configService) {

        static std::shared_ptr<MySqlModelService> ins =
                std::make_shared<MySqlModelService>(
                        MySqlDriver::getInstance(configService).get());
        return ins;
}
