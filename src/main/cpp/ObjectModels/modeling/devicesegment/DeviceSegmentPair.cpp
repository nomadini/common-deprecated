
#include "JsonUtil.h"
#include "StringUtil.h"
#include "DeviceSegmentPair.h"
#include "Device.h"
#include "DateTimeUtil.h"
#include "CassandraService.h"

DeviceSegmentPair::DeviceSegmentPair(std::shared_ptr<Device> device,
                                     std::shared_ptr<Segment> segment) : Object(__FILE__) {
        this->device = device;
        this->segment = segment;

}

DeviceSegmentPair::~DeviceSegmentPair() {

}

std::string DeviceSegmentPair::toString() {
        return toJson();
}


void DeviceSegmentPair::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        RapidJsonValueTypeNoRef segmentValue(rapidjson::kObjectType);
        this->segment->addPropertiesToJsonValue(segmentValue,  doc);
        JsonUtil::assertJsonTypeAndThrow(value, "object");
        JsonUtil::addMemberToValue_FromPair(doc, "segment", segmentValue, value);

        JsonUtil::addMemberToValue_FromPair(doc, "deviceId", device->getDeviceId(), value);
        JsonUtil::addMemberToValue_FromPair(doc, "deviceType", device->getDeviceType(), value);
}

std::string DeviceSegmentPair::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());

        return JsonUtil::valueToString(value);
}

std::shared_ptr<DeviceSegmentPair> DeviceSegmentPair::fromJson(std::string jsonModel) {
        auto document = parseJsonSafely(jsonModel);

        auto segment =  std::make_shared<Segment>(DateTimeUtil::getNowInMilliSecond());
        //        segment->
        // deviceSegment->segmentId = JsonUtil::GetStringSafely (*document, "segmentId");

        auto deviceSegment = std::make_shared<DeviceSegmentPair>(
                std::make_shared<Device>(
                        JsonUtil::GetStringSafely (*document, "deviceId"),
                        JsonUtil::GetStringSafely (*document, "deviceType")),
                segment);

        return deviceSegment;
}

std::shared_ptr<DeviceSegmentPair> DeviceSegmentPair::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<DeviceSegmentPair> adEntry = std::make_shared<DeviceSegmentPair>(
                std::make_shared<Device>(
                        JsonUtil::GetStringSafely (value, "deviceId"),
                        JsonUtil::GetStringSafely (value, "deviceType")),
                Segment::fromJsonValue(value["segment"])
                );

        return adEntry;
}
