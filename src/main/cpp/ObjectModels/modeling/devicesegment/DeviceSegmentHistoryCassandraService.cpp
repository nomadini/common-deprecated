#include "Device.h"
#include "CassandraDriverInterface.h"
#include "GUtil.h"
#include "DeviceSegmentHistoryCassandraService.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "ConfigService.h"

DeviceSegmentHistoryCassandraService::DeviceSegmentHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService)  :
        CassandraService(cassandraDriver,
                         entityToModuleStateStats,
                         
                         asyncThreadPoolService,
                         configService), Object(__FILE__) {
        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void DeviceSegmentHistoryCassandraService::deleteAll() {
        cassandraDriver->deleteAll ("deviceSegments");
}

DeviceSegmentHistoryCassandraService::~DeviceSegmentHistoryCassandraService() {

}
