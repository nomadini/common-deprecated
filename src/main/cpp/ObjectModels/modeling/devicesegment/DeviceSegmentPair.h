#ifndef DeviceSegmentPair_H
#define DeviceSegmentPair_H


#include "JsonTypeDefs.h"

class StringUtil;
#include <string>
#include <memory>

#include <gtest/gtest.h>
class Device;

#include "cassandra.h"
#include "Segment.h"
#include "Object.h"
class DeviceSegmentPair;


class DeviceSegmentPair : public Object {

public:

std::shared_ptr<Device> device;
std::shared_ptr<Segment> segment;

DeviceSegmentPair(std::shared_ptr<Device> device,
                  std::shared_ptr<Segment> segment);

std::string toString();

std::string toJson();

static std::shared_ptr<DeviceSegmentPair> fromJson(std::string jsonModel);

virtual ~DeviceSegmentPair();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

static std::shared_ptr<DeviceSegmentPair> fromJsonValue(RapidJsonValueType value);
};

#endif
