#include "Device.h"
#include "CassandraDriverInterface.h"
#include "GUtil.h"
#include "SegmentToDeviceCassandraService.h"
#include "ConfigService.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "CassandraDriver.h"
#include "DateTimeUtil.h"
#include "SegmentDevices.h"


SegmentToDeviceCassandraService::SegmentToDeviceCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService)  :
        CassandraService(cassandraDriver,
                         entityToModuleStateStats,

                         asyncThreadPoolService,
                         configService), Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
}
