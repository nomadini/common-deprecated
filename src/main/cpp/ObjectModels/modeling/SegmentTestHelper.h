#ifndef SegmentTestHelper_h
#define SegmentTestHelper_h

#include <gtest/gtest.h>


#include "Segment.h"


class SegmentTestHelper;





#include "Object.h"
class SegmentTestHelper {

private:

public:
static void areEqual(std::shared_ptr<Segment> tg1, std::shared_ptr<Segment> tg2);
static std::shared_ptr<Segment> createSampleSegment();
};

#endif
