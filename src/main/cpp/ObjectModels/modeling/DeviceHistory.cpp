
#include "DeviceHistory.h"
#include "Device.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "ConverterUtil.h"
#include <string>
#include <memory>

DeviceHistory::DeviceHistory(std::shared_ptr<Device> deviceArg) : Object(__FILE__) {
        this->device = deviceArg;
}
DeviceHistory::~DeviceHistory() {

}

std::shared_ptr<DeviceHistory> DeviceHistory::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<DeviceHistory> adEntry = std::make_shared<DeviceHistory>(
                std::make_shared<Device>(
                        JsonUtil::GetStringSafely (value, "deviceId"),
                        JsonUtil::GetStringSafely (value, "deviceType"))
                );

        adEntry->timeOfVisit = ConverterUtil::convertTo<TimeType>(JsonUtil::GetStringSafely (value, "timeOfVisit"));
        adEntry->deviceInfo = JsonUtil::GetStringSafely (value, "deviceInfo");


        return adEntry;
}

void DeviceHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair(doc, "deviceId", device->getDeviceId(), value);

        JsonUtil::addMemberToValue_FromPair(doc, "deviceType", device->getDeviceType(), value);

        JsonUtil::addMemberToValue_FromPair(doc,"timeOfVisit", StringUtil::toStr(timeOfVisit), value);

        JsonUtil::addMemberToValue_FromPair(doc, "deviceInfo",deviceInfo, value);

}


std::string DeviceHistory::toJson() {

        assertAndThrow(timeOfVisit > 0);
        assertAndThrow(!device->getDeviceId().empty());
        assertAndThrow(!device->getDeviceType().empty());

        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());

        return JsonUtil::valueToString(value);

}
