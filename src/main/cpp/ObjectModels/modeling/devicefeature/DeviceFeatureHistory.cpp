
#include "GUtil.h"
#include "CollectionUtil.h"

#include "JsonMapUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "DeviceFeatureHistory.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "Device.h"
#include "Feature.h"
#include "DateTimeUtil.h"

#include "ConverterUtil.h"
#include "CassandraService.h"

DeviceFeatureHistory::DeviceFeatureHistory(
								std::shared_ptr<Device> device) : Object(__FILE__) {
								this->device = device;
								positiveHistoryValue = false;
								featureRecencyInSecondToPickUp = 0;
								features = std::make_shared<std::vector<std::shared_ptr<FeatureHistory> > > ();
}

DeviceFeatureHistory::~DeviceFeatureHistory() {

}


std::string DeviceFeatureHistory::toString() {
								return this->toJson();
}

std::shared_ptr<std::unordered_map<std::string, int> >
DeviceFeatureHistory::consutructFeatureVisitCountsMap() {
								if (featureToVisitCounts != nullptr) {
																throwEx("aleardy constructed");
								}
								featureToVisitCounts =
																std::make_shared<std::unordered_map<std::string, int> > ();
								for(auto feature: *features) {
																auto pair = featureToVisitCounts->find(feature->feature->getName());
																if (pair == featureToVisitCounts->end()) {
																								featureToVisitCounts->insert(std::make_pair(feature->feature->getName(), 1));
																} else {
																								pair->second++;
																								MLOG(2)<< "feature "<<feature->feature->getName() <<", count is "<< pair->second;
																}

								}
								return featureToVisitCounts;
}

std::string DeviceFeatureHistory::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

								addPropertiesToJsonValue(value, doc.get());

								return JsonUtil::valueToString(value);
}



void DeviceFeatureHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								JsonUtil::addMemberToValue_FromPair(
																doc,
																"deviceId",
																device->getDeviceId(), value);
								JsonUtil::addMemberToValue_FromPair(
																doc,
																"deviceType",
																device->getDeviceType(),
																value);


								JsonUtil::addMemberToValue_FromPair(
																doc,
																"positiveHistoryValue",
																StringUtil::toStr(positiveHistoryValue), value);

								JsonUtil::addMemberToValue_FromPair(
																doc,
																"featureRecencyInSecondToPickUp",
																StringUtil::toStr(featureRecencyInSecondToPickUp), value);

								JsonArrayUtil::addMemberToValue_FromPair(doc, "featureHistory", *features, value);
}


std::shared_ptr<DeviceFeatureHistory> DeviceFeatureHistory::fromJson(std::string json) {
								auto document = parseJsonSafely(json);
								return fromJsonValue(*document);
}


std::shared_ptr<DeviceFeatureHistory> DeviceFeatureHistory::fromJsonValue(RapidJsonValueType value) {
								auto deviceFeatureHistory =
																std::make_shared<DeviceFeatureHistory>(
																								std::make_shared<Device>(
																																JsonUtil::GetStringSafely (value, "deviceId"),
																																JsonUtil::GetStringSafely (value, "deviceType")));


								deviceFeatureHistory->positiveHistoryValue =
																ConverterUtil::convertTo<bool>(JsonUtil::GetStringSafely(value, "positiveHistoryValue"));
								deviceFeatureHistory->featureRecencyInSecondToPickUp =
																ConverterUtil::convertTo<TimeType>(JsonUtil::GetStringSafely(value, "featureRecencyInSecondToPickUp"));

								JsonArrayUtil::getArrayFromValueMemeber(value, "featureHistory", *deviceFeatureHistory->features);
								return deviceFeatureHistory;
}


std::shared_ptr<std::vector<std::shared_ptr<FeatureHistory> > >  DeviceFeatureHistory::getFeatures() {
								return features;
}

std::shared_ptr<std::vector<std::shared_ptr<FeatureHistory> > > DeviceFeatureHistory::getFeaturesHappenedMoreThanNTimes(int n) {
								std::unordered_map<std::string, int> featureToNumberOfTimesSeen;
								std::unordered_map<std::string, std::shared_ptr<FeatureHistory> > featureToObjectMap;
								auto featuresToPick =
																std::make_shared<std::vector<std::shared_ptr<FeatureHistory> > > ();
								for(auto fh : *features) {
																auto pair = featureToNumberOfTimesSeen.find(fh->feature->name);
																if (pair == featureToNumberOfTimesSeen.end()) {
																								featureToNumberOfTimesSeen.insert(std::make_pair(fh->feature->name, 1));
																} else {
																								pair->second++;
																}
								}

								for(auto fh : *features) {
																auto pair = featureToObjectMap.find(fh->feature->name);
																if (pair == featureToObjectMap.end()) {
																								featureToObjectMap.insert(std::make_pair(fh->feature->name, fh));
																}
								}
								MLOG(2)<<"size of featureToObjectMap is "<< featureToObjectMap.size();
								for (auto featureTimePair : featureToNumberOfTimesSeen) {
																if (featureTimePair.second >= n) {
																								auto fhFoundPair = featureToObjectMap.find(featureTimePair.first);
																								assertAndThrow(fhFoundPair != featureToObjectMap.end());
																								featuresToPick->push_back(
																																fhFoundPair->second);
																}
								}

								MLOG(2)<<"size of featuresToPick is "<<featuresToPick->size();
								return featuresToPick;
}

void DeviceFeatureHistory::removeFeaturesFromHistory(std::unordered_set<std::string> featuresToRemoveSet) {


								//we build map of features to remove
								std::unordered_map<std::string, std::string>  featuresToRemoveToFeatureMap;
								for(auto featureToRemove: featuresToRemoveSet) {
																featuresToRemoveToFeatureMap.insert(std::make_pair(featureToRemove, featureToRemove));
								}

								//first we build featureToObjectMap
								std::unordered_map<std::string, std::shared_ptr<FeatureHistory> > featureToObjectMap;

								for(auto fh : *features) {
																auto pair = featureToObjectMap.find(fh->feature->name);
																if (pair == featureToObjectMap.end()) {
																								featureToObjectMap.insert(std::make_pair(fh->feature->name, fh));
																}
								}

								//we go thru old map, if the to-be-removed feature doesn't exist
								//then we copy them to clean feature vector
								auto cleanFeatures =
																std::make_shared<std::vector<std::shared_ptr<FeatureHistory> > > ();

								std::unordered_map<std::string, std::shared_ptr<FeatureHistory> > cleanFeatureToObjectMap;
								for(auto fh : featureToObjectMap) {
																auto pair = featuresToRemoveToFeatureMap.find(fh.second->feature->name);
																if (pair == featuresToRemoveToFeatureMap.end()) {
																								//bad feature doesn't exist in current map
																								//we insert it in new cleanFeatureToObjectMap
																								cleanFeatures->push_back(fh.second);
																} else {
																								//bad feature exist in current map
																								//we ignore it
																								MLOG(2) <<"cleaning out feature : "<<fh.second->feature->name;

																}
								}
								MLOG(2)<<" removed "<<features->size() - cleanFeatures->size();
								features = cleanFeatures;
}

std::shared_ptr<DeviceFeatureHistory> DeviceFeatureHistory::getCleanHistoryClone(int minFeatureTime) {
								auto cleanDevHist = std::make_shared<DeviceFeatureHistory>(device);

								cleanDevHist->positiveHistoryValue = positiveHistoryValue;
								cleanDevHist->featureRecencyInSecondToPickUp = featureRecencyInSecondToPickUp;
								cleanDevHist->features =
																getFeaturesHappenedMoreThanNTimes(minFeatureTime);
								return cleanDevHist;
}
