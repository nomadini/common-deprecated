/*
 * DeviceFeatureHistoryCassandraService.h
 *
 *  Created on: Jul 12, 2015
 *      Author: mtaabodi
 */

#ifndef DEVICE_FEATURE_HISTORY_CASSANDRA_SERVICE_H_
#define DEVICE_FEATURE_HISTORY_CASSANDRA_SERVICE_H_

class CassandraDriverInterface;

namespace gicapods { class ConfigService; }
class DeviceHistory;
#include "gmock/gmock.h"//move me and my mock to a different cpp file
#include "HttpUtilService.h"
#include "DeviceFeatureHistory.h"
#include "DeviceHistory.h"
#include "Device.h"
#include "CassandraService.h"
class EntityToModuleStateStats;
#include "CassandraService.h"
class AsyncThreadPoolService;

#include "Object.h"
class DeviceFeatureHistoryCassandraService
        : public CassandraService<DeviceFeatureHistory>, public Object {
private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;


AsyncThreadPoolService* asyncThreadPoolService;

public:
DeviceFeatureHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

std::shared_ptr<DeviceFeatureHistory> readFeatureHistoryOfDevice(
        std::shared_ptr<Device> device,
        TimeType featureRecencyInSecond,
        std::set<std::string> featureTypesRequested,
        int maxNumberOfHistoryPerDevice);

void deleteAll();
};

class DeviceFeatureHistoryCassandraServiceMock : public DeviceFeatureHistoryCassandraService {

public:

//
// MOCK_METHOD2_T(readFeatureHistoryOfDevice, std::shared_ptr<DeviceFeatureHistory>
//                        (std::shared_ptr<DeviceHistory> deviceHistory,
//                        TimeType featureRecencyInSecond,
//                        std::set<std::string> featureTypesRequested));
};


#endif /* DEVICE_FEATURE_HISTORY_CASSANDRA_SERVICE_H_ */
