
#include "DeviceFeatureHistory.h"

#include "DeviceFeatureHistoryTestHelper.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "Device.h"
#include "FeatureHistory.h"
#include "FeatureHistory.h"
#include "Feature.h"


std::shared_ptr<DeviceFeatureHistory> DeviceFeatureHistoryTestHelper::
createNewDeviceFeatureHistoryFromActionTakersPool(std::string seedWebsite) {

								std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
																std::make_shared<DeviceFeatureHistory>(
																								std::make_shared<Device>(Device::createStandardDeviceId(), ""));


								/**
								 * adding the history of visiting the seed website
								 */
								auto feature = std::make_shared<Feature>(Feature::generalTopLevelDomain, seedWebsite, 0);
								auto featureHistory = std::make_shared<FeatureHistory>(feature, DateTimeUtil::getNowInMilliSecond());
								deviceFeatureHistory->getFeatures()->push_back(featureHistory);

								/**
								 * adding a few number of important websites to the history of this browser
								 */
								int randomNumeberOfHistory = 5;
								for (int i=0; i< randomNumeberOfHistory; i++) {
																std::string important =
																								StringUtil::toStr("important-feature-") +
																								StringUtil::toStr(seedWebsite)+ StringUtil::toStr("-")+StringUtil::toStr(i);
																auto feature = std::make_shared<Feature>(Feature::generalTopLevelDomain, important, 0);
																auto featureHistory = std::make_shared<FeatureHistory>(feature, DateTimeUtil::getNowInMilliSecond());
																deviceFeatureHistory->getFeatures()->push_back(featureHistory);
								}


								return deviceFeatureHistory;
}

std::shared_ptr<DeviceFeatureHistory> DeviceFeatureHistoryTestHelper::createNewDeviceFeatureHistory() {

								std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
																std::make_shared<DeviceFeatureHistory>(
																								std::make_shared<Device>(Device::createStandardDeviceId(), "DESKTOP"));

								int randomNumeberOfHistory = 10;
								for (int i=0; i< randomNumeberOfHistory; i++) {
																auto feature = std::make_shared<Feature>(Feature::generalTopLevelDomain, StringUtil::random_string("non-related-feature", 2), 0);
																auto featureHistory = std::make_shared<FeatureHistory>(feature, DateTimeUtil::getNowInMilliSecond());
																deviceFeatureHistory->getFeatures()->push_back(featureHistory);
								}


								return deviceFeatureHistory;
}
