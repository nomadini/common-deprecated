/*
 * DeviceFeatureHistoryCassandraService.h
 *
 *  Created on: Jul 12, 2015
 *      Author: mtaabodi
 */

#include "CassandraDriverInterface.h"
#include "CollectionUtil.h"
#include "JsonArrayUtil.h"
#include "Feature.h"
#include "GUtil.h"
#include "DeviceFeatureHistory.h"
#include "FeatureDeviceHistory.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "StatisticsUtil.h"
#include "EntityToModuleStateStats.h"
#include "ConfigService.h"


DeviceFeatureHistoryCassandraService::DeviceFeatureHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService)  :
        CassandraService<DeviceFeatureHistory>(cassandraDriver,
                                               entityToModuleStateStats,

                                               asyncThreadPoolService,
                                               configService), Object(__FILE__) {
        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}


void DeviceFeatureHistoryCassandraService::deleteAll() {
        cassandraDriver->deleteAll ("gicapods_mdl.devicehistory");
}

std::shared_ptr<DeviceFeatureHistory> DeviceFeatureHistoryCassandraService::
readFeatureHistoryOfDevice(
        std::shared_ptr<Device> device,
        TimeType featureRecencyInSecond,
        std::set<std::string> featureTypesRequested,
        int maxNumberOfHistoryPerDevice) {
        assertAndThrow(!featureTypesRequested.empty());
        assertAndThrow(featureRecencyInSecond > 0);
        MLOG(3)<< "readFeatureHistoryOfDevice : featureTypesRequested "<<
                JsonArrayUtil::convertSetToJson(featureTypesRequested);
        auto deviceFeatureHistoryAsKey =
                std::make_shared<DeviceFeatureHistory>(device);
        deviceFeatureHistoryAsKey->featureRecencyInSecondToPickUp = featureRecencyInSecond;
        auto returnedDataTotalFeatures = readDataOptional(deviceFeatureHistoryAsKey);
        if (returnedDataTotalFeatures == nullptr) {
                entityToModuleStateStats->addStateModuleForEntity("NO_FEATURE_HISTORY_FOR_DEVICE_FOUND",
                                                                  "DeviceFeatureHistoryCassandraService",
                                                                  "ALL");
                return deviceFeatureHistoryAsKey;
        } else {

                entityToModuleStateStats->addStateModuleForEntity("FEATURE_HISTORY_FOR_DEVICE_FOUND",
                                                                  "DeviceFeatureHistoryCassandraService",
                                                                  "ALL");
                auto returnedDataSpecificFeatures =
                        std::make_shared<DeviceFeatureHistory>(device);

                int numberOfFeaturesPicked = 0;
                for(auto feature: *returnedDataTotalFeatures->getFeatures()) {
                        if (CollectionUtil::valueExistInSet(featureTypesRequested,
                                                            feature->feature->type)) {
                                if (numberOfFeaturesPicked <= maxNumberOfHistoryPerDevice) {
                                        returnedDataSpecificFeatures->getFeatures()->push_back(feature);
                                        MLOG(3)<<" adding feature "<< feature->feature->getName();
                                        numberOfFeaturesPicked++;
                                }
                        } else {
                                MLOG(3)<<" ignoring feature "<< feature->feature->getName()
                                       <<" because type is not requested : "<< feature->feature->getType();
                        }
                }
                return returnedDataSpecificFeatures;
        }
}
