#ifndef ObjectSetHolder_H
#define ObjectSetHolder_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "JsonTypeDefs.h"

#include "Object.h"

template<class T>
class ObjectSetHolder : public Object {
public:
std::shared_ptr<std::set<std::shared_ptr<T> > > values;

ObjectSetHolder();
virtual ~ObjectSetHolder();

};



#endif
