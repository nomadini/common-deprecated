#ifndef Encoder_H
#define Encoder_H

#include <memory>
#include <string>
#include <vector>
#include "Object.h"
class Encoder : public Object {

private:

public:

Encoder();
static std::string encode(std::string encodeMe);

static std::string decode(std::string stringToDecode);

std::string toString();
std::string toJson();
virtual ~Encoder();

};



/*

   EncoderPtrList EncoderList;
   for (EncoderPtrList::iterator it = EncoderList.begin();it != EncoderPtrList.end(); ++it)
   {

   }
 */

#endif
