#ifndef PixelClusterResult_H
#define PixelClusterResult_H


#include "Object.h"
#include <string>

#include "Poco/DateTime.h"
#include "JsonTypeDefs.h"

class PixelClusterResult : public Object {

private:

public:

int pixelId;
int id;

double score;
std::string cluster;
std::string clusterType;
std::string scoreType;

Poco::DateTime hiteDate;
Poco::DateTime createdAt;
Poco::DateTime updatedAt;

PixelClusterResult();
virtual ~PixelClusterResult();

static std::shared_ptr<PixelClusterResult> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

std::string toJson();

};

#endif
