#ifndef BWList_H
#define BWList_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "BWEntry.h"
#include "JsonTypeDefs.h"
#include "Object.h"

class BWList;

class BWList : public Object {

private:
int id;
std::string listType;
std::string name;
int clientId;
std::shared_ptr<std::vector<std::shared_ptr<BWEntry> > > bwEntries;
std::string createdAt;
std::string updatedAt;
public:

static std::string WHITE_LIST_TYPE;
static std::string BLACK_LIST_TYPE;
std::string toJson();
BWList();
virtual ~BWList();

void setId(int id);
void setListType(std::string listType);
void setName(std::string name);
void setClientId(int clientId);
void setCreatedAt(std::string createdAt);
void setUpdatedAt(std::string updatedAt);
void setBwEntries(std::shared_ptr<std::vector<std::shared_ptr<BWEntry> > > bwEntries);

int getId();
std::string getListType();
std::string getName();
int getClientId();
std::string getCreatedAt();
std::string getUpdatedAt();
std::shared_ptr<std::vector<std::shared_ptr<BWEntry> > > getBwEntries();



static std::shared_ptr<BWList> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
