#ifndef BWEntry_H
#define BWEntry_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "JsonTypeDefs.h"
#include "Object.h"

class BWEntry;



class BWEntry : public Object {

private:
int id;
int bwListId;
std::string domainName;
std::string createdAt;
std::string updatedAt;
public:

void setId(int id);

void setBwListId(int id);

void setDomainName(std::string domainName);

void setCreatedAt(std::string createdAt);

void setUpdatedAt(std::string updatedAt);

int getId();

int getBwListId();

std::string getDomainName();

std::string getCreatedAt();

std::string getUpdatedAt();

BWEntry();

std::string toJson();

virtual ~BWEntry();

static std::shared_ptr<BWEntry> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
