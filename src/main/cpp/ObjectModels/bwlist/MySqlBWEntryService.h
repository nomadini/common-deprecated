#ifndef MySqlBWEntryService_h
#define MySqlBWEntryService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "BWEntry.h"
#include "Object.h"
class MySqlBWEntryService;




class MySqlBWEntryService : public Object {

public:

MySqlDriver* driver;

MySqlBWEntryService(MySqlDriver* driver);

std::vector<std::shared_ptr<BWEntry> > readAll();

std::shared_ptr<std::vector<std::shared_ptr<BWEntry> > > readAllByBWListId(int id);

virtual std::shared_ptr<BWEntry> read(int id);

virtual void update(std::shared_ptr<BWEntry> obj);

virtual void insert(std::shared_ptr<BWEntry> obj);

virtual void deleteAll();

virtual ~MySqlBWEntryService();

};


#endif
