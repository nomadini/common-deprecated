#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlBWListService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlBWEntryService.h"
#include "MySqlDriver.h"
#include "MySqlTargetGroupBWListMapService.h"
#include "MySqlDriver.h"

#include <boost/foreach.hpp>

MySqlBWListService::MySqlBWListService(MySqlDriver* driver)   : Object(__FILE__) {
        this->driver = driver;
        this->mySqlBWEntryService = std::make_shared<MySqlBWEntryService>(driver);
}

MySqlBWListService::~MySqlBWListService() {

}

std::vector<std::shared_ptr<BWList> > MySqlBWListService::readAll() {

}

std::shared_ptr<BWList> MySqlBWListService::readByName(std::string name) {
        std::shared_ptr<BWList> bwList  = std::make_shared<BWList> ();
        std::string query = "SELECT "
                            " id, "
                            " name,"
                            " list_type, "
                            " client_id,"
                            " created_at, "
                            " updated_at "
                            " FROM bwlist where name = '__NAME__'";
        query = StringUtil::replaceString (query, "__NAME__", name);


        auto res = driver->executeQuery (query);


        while (res->next ()) {

                bwList->setId(res->getInt (1));
                bwList->setName(res->getString (2));
                auto listType = res->getString (3);
                if (!StringUtil::equalsIgnoreCase(listType, BWList::WHITE_LIST_TYPE) &&
                    !StringUtil::equalsIgnoreCase(listType, BWList::BLACK_LIST_TYPE)) {
                        EXIT("wrong list type for " + _toStr(bwList->getId()) + _toStr(", type : ") + listType);
                }

                bwList->setListType(listType);
                bwList->setClientId(res->getInt (4));
                bwList->setCreatedAt(res->getString (5));
                bwList->setUpdatedAt(res->getString (6));
                MLOG(3) << "reading bwList from db : " << bwList->toJson ();
        }

        assertAndThrow(!bwList->getName().empty ());
        return bwList;
}

std::shared_ptr<BWList> MySqlBWListService::readWithBwEntries(int id) {

        auto bwList = read (id);
        bwList->setBwEntries(mySqlBWEntryService->readAllByBWListId (id));
        return bwList;
}

std::shared_ptr<BWList> MySqlBWListService::read(int id) {


        std::string query = "SELECT "
                            " id, "
                            " name,"
                            " list_type, "
                            " client_id,"
                            " created_at, "
                            " updated_at "
                            " FROM `bwlist` where id = __ID__";
        MLOG(3)<< "MySqlBWListService :  read by id : " << id;
        query = StringUtil::replaceString (query, "__ID__", StringUtil::toStr (id));


        auto res = driver->executeQuery (query);

        std::shared_ptr<BWList> bwList  = std::make_shared<BWList> ();
        while (res->next ()) {

                bwList->setId(res->getInt (1));
                bwList->setName(res->getString (2));
                bwList->setListType(res->getString (3));
                bwList->setClientId(res->getInt (4));
                bwList->setCreatedAt(res->getString (5));
                bwList->setUpdatedAt(res->getString (6));
                MLOG(3) << "reading bwList from db : " << bwList->toJson ();
        }


        return bwList;

}

void MySqlBWListService::update(std::shared_ptr<BWList> obj) {

}

void MySqlBWListService::insert(std::shared_ptr<BWList> obj) {

        MLOG(3) << "inserting new BWList in db : " << obj->toJson ();
        std::string queryStr =
                "INSERT INTO `bwlist`"
                " ("
                " NAME,"
                " ADVERTISER_ID,"
                " list_type,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '__NAME__',"
                " '__ADVERTISER_ID__',"
                " '__listType__',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__NAME__",
                                              obj->getName());
        queryStr = StringUtil::replaceString (queryStr, "__ADVERTISER_ID__",
                                              StringUtil::toStr (obj->getClientId()));
        queryStr = StringUtil::replaceString (queryStr, "__listType__", obj->getListType());

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);

        MLOG(3) << "queryStr : " << queryStr;


        driver->executedUpdateStatement (queryStr);
        obj->setId(driver->getLastInsertedId ());


}

void MySqlBWListService::deleteAll() {
        driver->deleteAll("bwlist");
}
