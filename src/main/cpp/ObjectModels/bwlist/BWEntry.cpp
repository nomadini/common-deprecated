#include "BWEntry.h"
#include "JsonUtil.h"
#include "StringUtil.h"

BWEntry::BWEntry()  : Object(__FILE__) {
        id= 0;
        bwListId= 0;
}

BWEntry::~BWEntry() {

}

std::shared_ptr<BWEntry> BWEntry::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<BWEntry> bwList = std::make_shared<BWEntry>();

        bwList->setId(JsonUtil::GetIntSafely(value, "id"));
        bwList->setDomainName(JsonUtil::GetStringSafely(value, "domainName"));
        bwList->setBwListId(JsonUtil::GetIntSafely(value,"bwListId"));

        return bwList;
}

void BWEntry::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc, "domainName", domainName, value);
        JsonUtil::addMemberToValue_FromPair(doc, "bwListId", bwListId, value);
}

std::string BWEntry::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}


void BWEntry::setId(int id) {
        this->id = id;
}

void BWEntry::setBwListId(int id) {
        this->bwListId = id;
}

void BWEntry::setDomainName(std::string domainName) {
        this->domainName = StringUtil::toLowerCase (domainName);
}

void BWEntry::setCreatedAt(std::string createdAt) {
        this->createdAt = createdAt;
}

void BWEntry::setUpdatedAt(std::string updatedAt) {
        this->updatedAt = updatedAt;
}

int BWEntry::getId() {
        return id;
}

int BWEntry::getBwListId() {
        return bwListId;
}

std::string BWEntry::getDomainName() {
        return domainName;
}

std::string BWEntry::getCreatedAt() {
        return createdAt;
}

std::string BWEntry::getUpdatedAt() {
        return updatedAt;
}
