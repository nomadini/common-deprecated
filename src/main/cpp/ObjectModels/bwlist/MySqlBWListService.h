#ifndef MySqlBWListService_h
#define MySqlBWListService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "BWList.h"
#include "BWEntry.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include <tbb/concurrent_hash_map.h>
#include "MySqlBWEntryService.h"
#include "Object.h"
class MySqlBWListService;




class MySqlBWListService : public Object {

public:

MySqlDriver* driver;

std::shared_ptr<MySqlBWEntryService> mySqlBWEntryService;

MySqlBWListService(MySqlDriver* driver);

std::vector<std::shared_ptr<BWList> > readAll();

virtual std::shared_ptr<BWList> read(int id);

virtual std::shared_ptr<BWList> readByName(std::string name);

virtual std::shared_ptr<BWList> readWithBwEntries(int id);

virtual void update(std::shared_ptr<BWList> obj);

virtual void insert(std::shared_ptr<BWList> obj);

virtual void deleteAll();

virtual ~MySqlBWListService();

};


#endif
