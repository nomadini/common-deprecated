//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef CallStats_H
#define CallStats_H


#include <memory>
#include <string>
class EntityToModuleStateStats;
#include "AtomicLong.h"
#include "Object.h"
class CallStats :  public Object {

private:

public:
CallStats();

std::shared_ptr<gicapods::AtomicLong> totalCalls;
std::shared_ptr<gicapods::AtomicLong> totalMicroseconds;

virtual ~CallStats();
};

#endif //BIDDER_FILTER_H
