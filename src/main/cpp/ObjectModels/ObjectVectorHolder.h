#ifndef ObjectVectorHolder_H
#define ObjectVectorHolder_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "JsonTypeDefs.h"

#include "Object.h"

template<class T>
class ObjectVectorHolder : public Object {
public:
std::shared_ptr<std::vector<std::shared_ptr<T> > > values;

ObjectVectorHolder();
virtual ~ObjectVectorHolder();

};



#endif
