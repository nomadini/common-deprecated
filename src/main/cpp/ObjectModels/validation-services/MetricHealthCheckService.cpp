//
// Created by Mahmoud Taabodi on 3/6/16.
//


#include "AtomicDouble.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "CollectionUtil.h"
#include <tbb/concurrent_hash_map.h>
#include <thread>
#include "MetricHealthCheckService.h"
#include "MetricValidateInfo.h"
#include "CountMetricValidateInfo.h"
#include "EntityToModuleStateStats.h"
#include "PercentageMetricValidateInfo.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include "MySqlMetricService.h"
#include "MetricValidateInfo.h"
#include "AtomicBoolean.h"
#include "MetricEmailSender.h"

#include "MetricValidationResult.h"
#include "MetricDbRecord.h"
#include "ComparatorService.h"
#include "MetricValidationResult.h"

bool MetricHealthCheckService::validateThereIsNoExceptionMetrics(
        std::vector<std::shared_ptr<MetricDbRecord> > allExceptionMetrics) {

        if (!allExceptionMetrics.empty()) {
                for (auto metric : allExceptionMetrics) {
                        LOG(ERROR)<<"error metric found : "<< metric->toJson();
                }
                return false;
        }

        return true;
}

bool MetricHealthCheckService::determinesHealthOfPerformanceMetrics(
        std::string hostname,
        std::string appName,
        std::string version) {

        std::vector<std::shared_ptr<MetricDbRecord> > allMetrics =
                mySqlMetricService->readMetricsInLastNMinutes(
                        5,
                        hostname,
                        appName,
                        version,
                        EntityToModuleStateStats::important);

        std::string entity, state, module;
        std::vector<std::shared_ptr<MetricDbRecord> > allExceptionMetrics =
                mySqlMetricService->readAllMetricsInLastNMinutesWithLevel(
                        LOOK_BACK_WINDOW_FOR_METRICS_VALIDATION_IN_MINUTE,
                        "", //we want to get any value for host here
                        "", //we want to get any value for appName here
                        "",//we want to get any value for version here
                        entity,//we want to get any value for version here
                        state,//we want to get any value for version here
                        module,//we want to get any value for version here
                        EntityToModuleStateStats::exception);

        std::vector<std::shared_ptr<MetricDbRecord> > allWarningMetrics =
                mySqlMetricService->readAllMetricsInLastNMinutesWithLevel(
                        LOOK_BACK_WINDOW_FOR_METRICS_VALIDATION_IN_MINUTE,
                        "", //we want to get any value for host here
                        "", //we want to get any value for appName here
                        "",//we want to get any value for version here
                        entity,//we want to get any value for version here
                        state,//we want to get any value for version here
                        module,//we want to get any value for version here
                        EntityToModuleStateStats::warning);

        std::vector<std::shared_ptr<MetricDbRecord> > allErrorMetrics =
                mySqlMetricService->readAllMetricsInLastNMinutesWithLevel(
                        LOOK_BACK_WINDOW_FOR_METRICS_VALIDATION_IN_MINUTE,
                        "", //we want to get any value for host here
                        "", //we want to get any value for appName here
                        "",//we want to get any value for version here
                        entity,//we want to get any value for version here
                        state,//we want to get any value for version here
                        module,//we want to get any value for version here
                        EntityToModuleStateStats::error);

        allExceptionMetrics = CollectionUtil::combineTwoLists(allExceptionMetrics, allErrorMetrics);


        std::vector<std::shared_ptr<MetricDbRecord> > allCorrectMetrics =
                mySqlMetricService->readAllMetricsInLastNMinutesWithLevel(
                        LOOK_BACK_WINDOW_FOR_METRICS_VALIDATION_IN_MINUTE,
                        "", //we want to get any value for host here
                        "", //we want to get any value for appName here
                        "",//we want to get any value for version here
                        entity,//we want to get any value for version here
                        state,//we want to get any value for version here
                        module,//we want to get any value for version here
                        EntityToModuleStateStats::correct);

        std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<MetricDbRecord> > >
        mapOfValidateInfoNameToMetricDbRecord =
                std::make_shared<tbb::concurrent_hash_map<std::string, std::shared_ptr<MetricDbRecord> > > ();
        for (auto metricDbRecord : allMetrics) {
                std::string metricValidateInfoName = createAppSpecificNameForMetric(metricDbRecord,
                                                                                    hostname,
                                                                                    appName,
                                                                                    version);
                tbb::concurrent_hash_map<std::string, std::shared_ptr<MetricDbRecord> >::accessor accessor;
                LOG(INFO) << "metricValidateInfoName read to validate : "<< metricValidateInfoName;
                mapOfValidateInfoNameToMetricDbRecord->insert(accessor, metricValidateInfoName);
                accessor->second = metricDbRecord;
        }

        LOG(INFO) << "#metrics to validate : "<< allMetricValidateInfos.size();
        std::vector<std::shared_ptr<MetricValidationResult> > invalidOutsideThresholdMetrics;
        std::vector<std::shared_ptr<MetricValidationResult> > invalidNoValueMetrics;
        std::vector<std::string> seriousExceptionMetrics;
        std::vector<std::string> warningMetrics;
        std::vector<std::string> correctMetrics;

        std::vector<std::shared_ptr<MetricValidationResult> > validMetrics;


        for (auto metricDbRecord : allExceptionMetrics) {
                std::string metricValidateInfoName = createAppSpecificNameForMetric(metricDbRecord,
                                                                                    hostname,
                                                                                    appName,
                                                                                    version);
                seriousExceptionMetrics.push_back(metricValidateInfoName);
        }

        for (auto metricDbRecord : allCorrectMetrics) {
                std::string metricValidateInfoName = createAppSpecificNameForMetric(metricDbRecord,
                                                                                    hostname,
                                                                                    appName,
                                                                                    version);
                correctMetrics.push_back(metricValidateInfoName);
        }

        for (auto metricDbRecord : allWarningMetrics) {
                std::string metricValidateInfoName = createAppSpecificNameForMetric(metricDbRecord,
                                                                                    hostname,
                                                                                    appName,
                                                                                    version);
                warningMetrics.push_back(metricValidateInfoName);
        }

        for (auto metricValidateInfo : allMetricValidateInfos) {
                auto
                metricValidationResult =
                        metricValidateInfo->validate(mapOfValidateInfoNameToMetricDbRecord);
                metricValidationResult->metricUniqueName =  metricValidateInfo->metricUniqueName;
                if (StringUtil::equalsIgnoreCase(metricValidationResult->result, "failure")) {

                        if (StringUtil::equalsIgnoreCase(metricValidationResult->failureType, "no-value-recorded")) {
                                invalidNoValueMetrics.push_back(metricValidationResult);
                        } else if (StringUtil::equalsIgnoreCase(metricValidationResult->failureType, "outside-threshold")) {
                                invalidOutsideThresholdMetrics.push_back(metricValidationResult);
                        } else {
                                throwEx("error : bad failureType : " + metricValidationResult->failureType);
                        }

                } else if (StringUtil::equalsIgnoreCase(metricValidationResult->result, "success")) {
                        validMetrics.push_back(metricValidationResult);

                } else {
                        throwEx("error : bad result type : " + metricValidationResult->result);
                }

        }
        std::string spentListOfTargetGroups = comparatorService->getSpentAmountListOfTargetGroups(seriousExceptionMetrics);
        // spentAmountChecker->getProblematicListOfTargetGroups();
        metricEmailSender->sendEmail(
                invalidOutsideThresholdMetrics,
                invalidNoValueMetrics,
                seriousExceptionMetrics,
                correctMetrics,
                warningMetrics,
                validMetrics,
                spentListOfTargetGroups);

        if (invalidNoValueMetrics.empty() &&
            invalidOutsideThresholdMetrics.empty()) {
                return true;
        } else {
                return false;
        }
}

void MetricHealthCheckService::runHealthCheck() {
        int numberOfRuns = 0;
        while(true) {
                try {

                        //we retrieve metrics for any app, any hostname and any version
                        auto performanceHealth = determinesHealthOfPerformanceMetrics("", "", "");
                        // auto exceptionHealth = validateThereIsNoExceptionMetrics();

                        comparatorService->runComparisons();

                        auto exceptionHealth = true;
                        if (performanceHealth && exceptionHealth) {
                                overallMetricsHealth->setValue(true);
                        } else {
                                overallMetricsHealth->setValue(false);
                        }
                } catch(std::exception &e) {
                        gicapods::Util::showStackTrace(&e);
                }


                numberOfRuns++;
                if (numberOfRuns < 30) {
                        gicapods::Util::sleepViaBoost(_L_, secondsToSleepAfterCheckInFirstHalfAnHour);
                } else {
                        gicapods::Util::sleepViaBoost(_L_, secondsToSleepAfterCheckAfterFirstHalfAnHour);
                }

        }
}
void MetricHealthCheckService::startHealthCheckingThread() {
        std::thread healthCheckThread(&MetricHealthCheckService::runHealthCheck,
                                      this);
        healthCheckThread.detach();
}

std::string MetricHealthCheckService::createAppSpecificNameForMetric(std::shared_ptr<MetricDbRecord> metricDbRecord,
                                                                     std::string hostname,
                                                                     std::string appName,
                                                                     std::string version) {
        std::string baseName = metricDbRecord->state + "-" +
                               metricDbRecord->module + "-" +
                               metricDbRecord->entity + "-" +
                               metricDbRecord->appName;
        return baseName;

}

void MetricHealthCheckService::addCounterValidatorInfo(std::string metricUniqueName,
                                                       int minCountThreshold,
                                                       int maxCountThreshold) {
        allMetricValidateInfos.push_back(
                std::make_shared<CountMetricValidateInfo>(
                        metricUniqueName, minCountThreshold, maxCountThreshold));
}

MetricHealthCheckService::~MetricHealthCheckService() {

}
MetricHealthCheckService::MetricHealthCheckService() : Object(__FILE__) {
        LOOK_BACK_WINDOW_FOR_METRICS_VALIDATION_IN_MINUTE = 10;
        secondsToSleepAfterCheckInFirstHalfAnHour = 180;
        secondsToSleepAfterCheckAfterFirstHalfAnHour = 1800;

        overallMetricsHealth = std::make_shared<gicapods::AtomicBoolean>();
        addCounterValidatorInfo("NEW_BROWSER_SEEN-NomadiniDeviceIdCookieReaderModule-ALL-ActionRecorder", 10, 10000);
        addCounterValidatorInfo("ADDING_ACTION_TAKER_DEVICE_SEGMENT-PixelActionTakerSegmentWriterModule-ALL-ActionRecorder", 10, 10000);
        addCounterValidatorInfo("KNOWN_BROWSER_SEEN-NomadiniDeviceIdCookieReaderModule-ALL-ActionRecorder", 20, 10000);
        addCounterValidatorInfo("ADDING_ACTION_TAKER_DEVICE_SEGMENT-PixelActionTakerSegmentWriterModule-ALL-ActionRecorder", 20, 10000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-CassandraServiceDirectDb-DeviceSegment-ALL-ActionRecorder", 20, 1000);
        addCounterValidatorInfo("MSG_PUT_TO_KAFKA_QUEUE-EnqueueForScoringModule-ALL-Bidder", 1000, 20000);
        addCounterValidatorInfo("pushed_to_queue_for_write-CrossWalkUpdaterModule-ALL-Bidder", 20, 10000);
        addCounterValidatorInfo("pushed_to_queue_for_write-DeviceHistoryUpdaterModule-ALL-Bidder", 20, 10000);
        addCounterValidatorInfo("pushed_to_queue_for_write-FeatureDeviceHistoryUpdaterModule-ALL-Bidder", 20, 10000);
        addCounterValidatorInfo("#GoodBids-BidRequestHandler-ALL-Bidder",20, 10000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-ValueBatchUpdateTask-EventLog-ALL-AdServer", 20, 1000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-ValueBatchUpdateTask-AdHistory-ALL-AdServer", 20, 1000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-ValueBatchUpdateTask-DeviceFeatureHistory-ALL-Bidder", 20, 1000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-ValueBatchUpdateTask-DeviceIdToIpsMap-ALL-DataMaster", 20, 1000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-ValueBatchUpdateTask-FeatureDeviceHistory-ALL-Bidder", 20, 1000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-ValueBatchUpdateTask-IpToDeviceIdsMap-ALL-DataMaster", 20, 10000);
        addCounterValidatorInfo("TRACE_SUCCESS_HAPPENED-ValueTraceTask-DeviceFeatureHistory-ALL-Bidder", 10, 10000);
        addCounterValidatorInfo("TRACE_SUCCESS_HAPPENED-ValueTraceTask-DeviceIdToIpsMap-ALL-Bidder", 10, 10000);
        addCounterValidatorInfo("TRACE_SUCCESS_HAPPENED-ValueTraceTask-FeatureDeviceHistory-ALL-Bidder", 10, 10000);
        addCounterValidatorInfo("TRACE_SUCCESS_HAPPENED-ValueTraceTask-IpToDeviceIdsMap-ALL-Bidder", 10, 10000);
        addCounterValidatorInfo("MAPPED_INITIAL_UNKNOW_USER_WITH_SUCCESS-GicapodsIdToExchangeIdMapModule-ALL-Bidder", -1, 10); //this should not happen alot at all
        addCounterValidatorInfo("BIDDING_TO_TRY_MATCHING_PIXEL-GicapodsIdToExchangeIdMapModule-ALL-Bidder", 10, 1000); //this should not happen too much but it should happen
        addCounterValidatorInfo("IGNORE_UNKNOWN_DESKTOP_USER-GicapodsIdToExchangeIdMapModule-ALL-Bidder", 100, 1000); //this should not happen all the time
        addCounterValidatorInfo("READ_MSG_FROM_KAFKA-KafkaSubscriber-ALL-Scorer", 1000, 100000);
        addCounterValidatorInfo("ADDING_DEVICE_SEGMENT-DeviceSegmentPersistenceModule-ALL-Scorer", 20, 100000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-CassandraServiceDirectDb-DeviceSegment-ALL-Scorer", 20, 10000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-CassandraServiceDirectDb-IpToDeviceIdsMap-ALL-DataMaster", 20, 1000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-CassandraServiceDirectDb-DeviceFeatureHistory-ALL-Bidder", 20, 1000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-CassandraServiceDirectDb-DeviceIdToIpsMap-ALL-DataMaster", 20, 1000);
        addCounterValidatorInfo("BATCH_UPDATE_SUCCESS-CassandraServiceDirectDb-FeatureDeviceHistory-ALL-Bidder", 20, 1000);
        addCounterValidatorInfo("#ImpressionRequestsProcessed-ImpressionServHandler-ALL-AdServer", 20, 1000);
}
