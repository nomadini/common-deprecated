#include "GUtil.h"
#include "MetricEmailSender.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include <cmath>
#include "EmailSender.h"
#include "MetricValidationResult.h"
MetricEmailSender::MetricEmailSender() : Object(__FILE__){

}

MetricEmailSender::~MetricEmailSender() {

}

void MetricEmailSender::sendEmail(
        std::vector<std::shared_ptr<MetricValidationResult> > invalidOutsideThresholdMetrics,
        std::vector<std::shared_ptr<MetricValidationResult> > invalidNoValueMetrics,
        std::vector<std::string> seriousExceptionMetrics,
        std::vector<std::string> correctMetrics,
        std::vector<std::string> warningMetrics,
        std::vector<std::shared_ptr<MetricValidationResult> > validMetrics,
        std::string spentListOfTargetGroups) {
        std::string emailMessage;

        emailMessage += "Serious Exceptions in Apps, Investigate ASAP (exception metrics)";
        emailMessage += "\n\n";
        for (auto metricName : seriousExceptionMetrics) {
                std::string oneMessage = " " + metricName + "\n";
                emailMessage +=oneMessage;
        }

        emailMessage += "\n\n";
        emailMessage += "Non-working features (No-Value-Found  Metrics)";
        emailMessage += "\n\n";
        for (auto metricValidateInfo : invalidNoValueMetrics) {
                std::string oneMessage = " " + metricValidateInfo->metricUniqueName +
                                         "_value :" +   metricValidateInfo->resultMeasure
                                         + "\n";
                emailMessage +=oneMessage;
        }

        emailMessage += "\n\n";
        emailMessage += "Semi-Working Features (Failing Metrics)";
        emailMessage += "\n\n";
        for (auto metricValidateInfo : invalidOutsideThresholdMetrics) {
                std::string oneMessage = " " + metricValidateInfo->metricUniqueName +
                                         "_value :" +   metricValidateInfo->resultMeasure
                                         + "\n";
                emailMessage +=oneMessage;
        }

        emailMessage += "\n\n";
        emailMessage += "WARNING, Investigate and Fix later. (Warning Metrics)";
        emailMessage += "\n\n";
        for (auto metricName : warningMetrics) {
                std::string oneMessage = " " + metricName + "\n";
                emailMessage +=oneMessage;
        }

        emailMessage +="\n\n";
        emailMessage += "Working Features (Passing Metrics)";
        emailMessage += "\n\n";
        for (auto metricValidateInfo : validMetrics) {
                std::string oneMessage = " " + metricValidateInfo->metricUniqueName +
                                         "_value :" +   metricValidateInfo->resultMeasure
                                         + "\n";
                emailMessage +=oneMessage;
        }
        emailMessage += "\n\n";
        emailMessage += "Correct Metrics.......";
        emailMessage += "\n\n";
        for (auto metricName : correctMetrics) {
                std::string oneMessage = " " + metricName + "\n";
                emailMessage +=oneMessage;
        }

        emailMessage +=spentListOfTargetGroups;
        LOG(ERROR) << "# invalidNoValueMetrics  : "<< invalidNoValueMetrics.size();
        LOG(ERROR) << "# invalidOutsideThresholdMetrics metrics  : "<< invalidOutsideThresholdMetrics.size();
        LOG(ERROR) << "# validMetrics metrics  : "<< validMetrics.size();


        emailSender->sendEmail("m_taabodi@yahoo.com",
                               "m_taabodi@yahoo.com",
                               emailMessage,
                               "metrics status report");

}
