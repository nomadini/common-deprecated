//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef MetricHealthCheckService_H
#define MetricHealthCheckService_H

#include "AtomicDouble.h"
class EntityToModuleStateStats;
class MySqlMetricService;
class MetricValidateInfo;
class ComparatorService;
class MetricDbRecord;
class MetricEmailSender;
#include "Object.h"
#include "AtomicBoolean.h"
#include "ConcurrentHashMap.h"

#include <tbb/concurrent_hash_map.h>
class MetricHealthCheckService;


class MetricHealthCheckService : public Object {
public:
int LOOK_BACK_WINDOW_FOR_METRICS_VALIDATION_IN_MINUTE;
MySqlMetricService* mySqlMetricService;
EntityToModuleStateStats* entityToModuleStateStats;
int secondsToSleepAfterCheckInFirstHalfAnHour;
int secondsToSleepAfterCheckAfterFirstHalfAnHour;

std::vector<std::shared_ptr<MetricValidateInfo> > allMetricValidateInfos;
std::shared_ptr<gicapods::AtomicBoolean> overallMetricsHealth;
std::unique_ptr<ComparatorService> comparatorService;
std::unique_ptr<MetricEmailSender> metricEmailSender;
MetricHealthCheckService();

bool determinesHealthOfPerformanceMetrics(
        std::string hostname,
        std::string appName,
        std::string version);

bool validateThereIsNoExceptionMetrics(std::vector<std::shared_ptr<MetricDbRecord> > allExceptionMetrics);

void runHealthCheck();

void startHealthCheckingThread();

void addCounterValidatorInfo(std::string metricUniqueName,
                             int minCountThreshold,
                             int maxCountThreshold);
std::string createAppSpecificNameForMetric(std::shared_ptr<MetricDbRecord> metric,
                                           std::string hostname,
                                           std::string appName,
                                           std::string version);
virtual ~MetricHealthCheckService();
};
#endif //MetricHealthCheckService_H
