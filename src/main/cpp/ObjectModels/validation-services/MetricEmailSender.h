#ifndef MetricEmailSender_H_
#define MetricEmailSender_H_

#include <memory>
#include <string>
#include <vector>
class EntityToModuleStateStats;
class MetricValidationResult;
class EmailSender;
namespace gicapods { class ConfigService; }
#include "Object.h"
class MetricEmailSender;



class MetricEmailSender : public std::enable_shared_from_this<MetricEmailSender>, public Object {

public:

EntityToModuleStateStats* entityToModuleStateStats;

EmailSender* emailSender;
MetricEmailSender();

virtual ~MetricEmailSender();

void sendEmail(
        std::vector<std::shared_ptr<MetricValidationResult> > invalidOutsideThresholdMetrics,
        std::vector<std::shared_ptr<MetricValidationResult> > invalidNoValueMetrics,
        std::vector<std::string> seriousExceptionMetrics,
        std::vector<std::string> correctMetrics,
        std::vector<std::string> warningMetrics,
        std::vector<std::shared_ptr<MetricValidationResult> > validMetrics,
        std::string spentListOfTargetGroups);

private:

};

#endif
