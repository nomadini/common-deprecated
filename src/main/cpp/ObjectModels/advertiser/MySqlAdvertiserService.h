#ifndef MySqlAdvertiserService_h
#define MySqlAdvertiserService_h


#include "Advertiser.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "Object.h"
class MySqlAdvertiserService;




class MySqlAdvertiserService : public DataProvider<Advertiser>, public Object {

public:
MySqlDriver* driver;

MySqlAdvertiserService(MySqlDriver* driver);

virtual ~MySqlAdvertiserService();

void parseTheAttribute(std::shared_ptr<Advertiser> obj, std::string attributesFromDB);

std::vector<std::shared_ptr<Advertiser>> readAllAdvertisers();

void insert(std::shared_ptr<Advertiser> Advertiser);

std::shared_ptr<Advertiser> readById(int id);

virtual void deleteAll();

std::vector<std::shared_ptr<Advertiser>> readAllEntities();

std::shared_ptr<Advertiser> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
};

#endif
