#ifndef Advertiser_H
#define Advertiser_H

#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
class Advertiser;
#include "Object.h"


class Advertiser : public Object {

private:

public:
int id;
std::string name;
std::string description;
std::string status;

int clientId;

//an advertiser can have multiple domain names
std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > domainNames;

Poco::DateTime createdAt;
Poco::DateTime updatedAt;
static std::string getEntityName();
Advertiser();

void validate();

std::string toString();

std::string toJson();

virtual ~Advertiser();

int getId();
std::string getSlug();
std::string getName();
std::string getStatus();
std::string getDescription();
int getClientId();
Poco::DateTime getCreatedAt();
Poco::DateTime getUpdatedAt();
std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > getDomainNames();

void setId(int id);
void setName(std::string name);
void setStatus(std::string status);
void setDescription(std::string description);
void setClientId(int clientId);

void setCreatedAt(Poco::DateTime dateCreated);
void setUpdatedAt(Poco::DateTime dateModified);

void setDomainNames(std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > domainNames);

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<Advertiser> fromJsonValue(RapidJsonValueType value);
};

#endif
