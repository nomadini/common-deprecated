//
// Created by Mahmoud Taabodi on 4/15/16.
//

#ifndef COMMON_ADVERTISERTESTHELPER_H
#define COMMON_ADVERTISERTESTHELPER_H


#include "Advertiser.h"
#include "Object.h"
class AdvertiserTestHelper : public Object {

public:
static std::shared_ptr<Advertiser> createSampleAdvertiser();

};

#endif //COMMON_ADVERTISERTESTHELPER_H
