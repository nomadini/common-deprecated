#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "GlobalWhiteListedCacheService.h"
#include "MySqlGlobalWhiteListService.h"
#include "JsonArrayUtil.h"
#include "BooleanObject.h"

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, BooleanObject> > GlobalWhiteListedCacheService::getAllWhiteListedDomains() {
        return allWhiteListedDomains;
}

GlobalWhiteListedCacheService::GlobalWhiteListedCacheService(
        MySqlGlobalWhiteListService* mySqlGlobalWhiteListService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) :
        CacheService<GlobalWhiteListEntry>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {
        assertAndThrow(entityToModuleStateStats != NULL);
        this->allWhiteListedDomains = std::make_shared<gicapods::ConcurrentHashMap<std::string, BooleanObject> > ();
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void GlobalWhiteListedCacheService::clearOtherCaches() {
        getAllWhiteListedDomains ()->clear ();
}

void GlobalWhiteListedCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<GlobalWhiteListEntry> > allEntities) {
        for(std::shared_ptr<GlobalWhiteListEntry> entry :  allEntities) {
                auto boolean = std::make_shared<BooleanObject>();
                allWhiteListedDomains->put (StringUtil::toLowerCase(entry->getDomainName()), boolean);
        }
}

GlobalWhiteListedCacheService::~GlobalWhiteListedCacheService() {

}
