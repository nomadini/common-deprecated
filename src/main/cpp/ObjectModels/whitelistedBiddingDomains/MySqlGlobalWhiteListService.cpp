#include "GUtil.h"

#include "MySqlGlobalWhiteListService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"

MySqlGlobalWhiteListService::MySqlGlobalWhiteListService(MySqlDriver* driver) : Object(__FILE__) {
        assertAndThrow(driver != NULL);
        this->driver = driver;
}

std::vector<std::shared_ptr<GlobalWhiteListEntry>> MySqlGlobalWhiteListService::readAllEntities() {
        std::vector<std::shared_ptr<GlobalWhiteListEntry>> allEntities;
        std::string query = " SELECT ID, DOMAIN_NAME,created_at, updated_at"
                            " FROM global_whitlisted_domains";

        auto res = driver->executeQuery (query);
        //removed the join on advertiser here, because it
        //would have made setting up data for the tests more difficult

        while (res->next ()) {
                std::shared_ptr<GlobalWhiteListEntry> entry = std::make_shared<GlobalWhiteListEntry>();
                entry->setId(res->getInt(1));
                entry->setDomainName(res->getString(2));
                entry->setCreatedAt(res->getString(3));
                entry->setUpdatedAt(res->getString(4));
                allEntities.push_back(entry);
        }

        return allEntities;
}

void MySqlGlobalWhiteListService::deleteAll() {
        driver->deleteAll("global_whitlisted_domains");
}

void MySqlGlobalWhiteListService::insert(std::string domain) {
        std::string queryStr =
                " INSERT INTO `global_whitlisted_domains`"
                " ("
                " `DOMAIN_NAME`,"
                " `created_at`,"
                " `updated_at`)"
                " VALUES"
                " ( "
                " '__DOMAIN_NAME__',"
                " '__created_at__',"
                " '__updated_at__');";

        queryStr = StringUtil::replaceString (queryStr, "__DOMAIN_NAME__",
                                              domain);
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);

        driver->executedUpdateStatement (queryStr);

}

MySqlGlobalWhiteListService::~MySqlGlobalWhiteListService() {
}
