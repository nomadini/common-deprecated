#include "GlobalWhiteListEntry.h"
#include "JsonUtil.h"
#include "StringUtil.h"

GlobalWhiteListEntry::GlobalWhiteListEntry()  : Object(__FILE__) {
        id = 0;
}

GlobalWhiteListEntry::~GlobalWhiteListEntry() {

}

std::shared_ptr<GlobalWhiteListEntry> GlobalWhiteListEntry::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<GlobalWhiteListEntry> bwList = std::make_shared<GlobalWhiteListEntry>();

        bwList->setId(JsonUtil::GetIntSafely(value, "id"));
        bwList->setDomainName(JsonUtil::GetStringSafely(value, "domainName"));


        return bwList;
}

void GlobalWhiteListEntry::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc, "domainName", domainName, value);

}

std::string GlobalWhiteListEntry::getEntityName() {
        return "GlobalWhiteListEntry";
}
std::string GlobalWhiteListEntry::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}


void GlobalWhiteListEntry::setId(int id) {
        this->id = id;
}

void GlobalWhiteListEntry::setDomainName(std::string domainName) {
        this->domainName = StringUtil::toLowerCase (domainName);
}

void GlobalWhiteListEntry::setCreatedAt(std::string createdAt) {
        this->createdAt = createdAt;
}

void GlobalWhiteListEntry::setUpdatedAt(std::string updatedAt) {
        this->updatedAt = updatedAt;
}

int GlobalWhiteListEntry::getId() {
        return id;
}

std::string GlobalWhiteListEntry::getDomainName() {
        return domainName;
}

std::string GlobalWhiteListEntry::getCreatedAt() {
        return createdAt;
}

std::string GlobalWhiteListEntry::getUpdatedAt() {
        return updatedAt;
}
