#ifndef GlobalWhiteListEntry_H
#define GlobalWhiteListEntry_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "JsonTypeDefs.h"

//this class determines if a domain is eligible for bidding or not
class GlobalWhiteListEntry;
#include "Object.h"


class GlobalWhiteListEntry : public Object {

private:
std::string domainName;
std::string createdAt;
std::string updatedAt;
public:
int id;     //used in CacheService

void setId(int id);

void setDomainName(std::string domainName);

void setCreatedAt(std::string createdAt);

void setUpdatedAt(std::string updatedAt);

int getId();

std::string getDomainName();

std::string getCreatedAt();

std::string getUpdatedAt();

GlobalWhiteListEntry();

std::string toJson();

virtual ~GlobalWhiteListEntry();

static std::string getEntityName();
static std::shared_ptr<GlobalWhiteListEntry> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
