
#ifndef MySqlGlobalWhiteListService_H
#define MySqlGlobalWhiteListService_H



#include "MySqlDriver.h"
#include <cppconn/resultset.h>
class DateTimeUtil;

#include <tbb/concurrent_hash_map.h>
#include <memory>
#include "DataProvider.h"
#include "GlobalWhiteListEntry.h"
class MySqlGlobalWhiteListService;
#include "Object.h"


class MySqlGlobalWhiteListService : public DataProvider<GlobalWhiteListEntry>, public Object {

public:
MySqlDriver* driver;

MySqlGlobalWhiteListService(MySqlDriver* driver);

void deleteAll();

void insert(std::string domain);

virtual ~MySqlGlobalWhiteListService();

std::vector<std::shared_ptr<GlobalWhiteListEntry>> readAllEntities();
};

#endif
