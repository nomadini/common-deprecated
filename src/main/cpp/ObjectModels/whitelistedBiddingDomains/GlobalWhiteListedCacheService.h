
#ifndef GlobalWhiteListedCacheService_H
#define GlobalWhiteListedCacheService_H


class StringUtil;
#include "GlobalWhiteListEntry.h"
#include <boost/foreach.hpp>
class GlobalWhiteListedCacheService;
#include "MySqlGlobalWhiteListService.h"
#include "Object.h"
#include "MySqlGlobalWhiteListService.h"
class DateTimeUtil;

#include <tbb/concurrent_hash_map.h>
#include <memory>
#include "HttpUtilService.h"
class EntityToModuleStateStats;
class BooleanObject;
#include "EntityProviderService.h"
#include "CacheService.h"
#include "ConcurrentHashMap.h"

class GlobalWhiteListedCacheService;



class GlobalWhiteListedCacheService : public CacheService<GlobalWhiteListEntry>, public Object {

public:
MySqlGlobalWhiteListService* mySqlGlobalWhiteListService;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, BooleanObject> > allWhiteListedDomains;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, BooleanObject> > getAllWhiteListedDomains();

EntityToModuleStateStats* entityToModuleStateStats;

GlobalWhiteListedCacheService(MySqlGlobalWhiteListService* mySqlGlobalWhiteListService,
                              HttpUtilService* httpUtilService,
                              std::string dataMasterUrl,
                              EntityToModuleStateStats* entityToModuleStateStats,
                              std::string appName);

virtual ~GlobalWhiteListedCacheService();

virtual void clearOtherCaches();

virtual void populateOtherMapsAndLists(std::vector<std::shared_ptr<GlobalWhiteListEntry> > allEntities);

};

#endif
