#ifndef MySqlGeoSegmentService_h
#define MySqlGeoSegmentService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "GeoSegment.h"

class MySqlGeoSegmentService;




class MySqlGeoSegmentService {

public:

    MySqlDriver* driver;

    MySqlGeoSegmentService(MySqlDriver* driver);

    std::vector<std::shared_ptr<GeoSegment>> readAll();

    std::vector<std::shared_ptr<GeoSegment>> readAllByListId(int geoSegmentListId);

    std::shared_ptr<GeoSegment> read(int id);

    void update(std::shared_ptr<GeoSegment> obj);

    void insert(std::shared_ptr<GeoSegment> obj);

    virtual void deleteAll();

    virtual ~MySqlGeoSegmentService();

};


#endif
