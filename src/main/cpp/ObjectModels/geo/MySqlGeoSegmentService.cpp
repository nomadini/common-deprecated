#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlGeoSegmentService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

MySqlGeoSegmentService::MySqlGeoSegmentService(MySqlDriver* driver) {
        this->driver = driver;
}

MySqlGeoSegmentService::~MySqlGeoSegmentService() {

}

std::vector<std::shared_ptr<GeoSegment>> MySqlGeoSegmentService::readAllByListId(int geoSegmentListId) {
        std::vector<std::shared_ptr<GeoSegment>> all;
        std::string queryStr = "SELECT "
                               " ID ,"
                               " SEGMENT_NAME,"
                               " LAT,"
                               " LON,"
                               " SEGMENT_RADIUS_IN_MILE,"
                               " GEO_SEGMENT_LIST_ID,"
                               " created_at,"
                               " updated_at "
                               "FROM geosegment where GEO_SEGMENT_LIST_ID = _GEO_SEGMENT_LIST_ID";



        queryStr = StringUtil::replaceString (queryStr, "_GEO_SEGMENT_LIST_ID", StringUtil::toStr (geoSegmentListId));
        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                MLOG(3) << "geolocation obj loaded id :  " << res->getInt (1);         // getInt(1) returns the first colum
                std::shared_ptr<GeoSegment> obj  = std::make_shared<GeoSegment> ();
                obj->id = res->getInt ("ID");         //this is done for the converting to map function
                obj->segmentName = MySqlDriver::getString( res, 2);
                obj->lat = res->getDouble (3);
                obj->lon = res->getDouble (4);
                obj->segmentRadiusInMile = res->getDouble (5);
                obj->geoSegmentListId = res->getInt (6);
                all.push_back (obj);

        }


        return all;


}

std::vector<std::shared_ptr<GeoSegment>> MySqlGeoSegmentService::readAll() {
        std::vector<std::shared_ptr<GeoSegment>> all;
        std::string query =   "SELECT "
                            " ID ,"
                            " SEGMENT_NAME,"
                            " LAT,"
                            " LON,"
                            " SEGMENT_RADIUS_IN_MILE,"
                            " GEO_SEGMENT_LIST_ID,"
                            " created_at,"
                            " updated_at "
                            "FROM geosegment;";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                MLOG(3) << "geolocation obj loaded id : " << res->getInt (1);         // getInt(1) returns the first colum
                std::shared_ptr<GeoSegment> obj =  std::make_shared<GeoSegment> ();
                obj->id = res->getInt (1);         //this is done for the converting to map function
                obj->segmentName = MySqlDriver::getString( res, 2);
                obj->lat = res->getDouble (3);
                obj->lon = res->getDouble (4);
                obj->segmentRadiusInMile = res->getDouble (5);
                obj->geoSegmentListId = res->getInt (6);
                all.push_back (obj);

        }

        return all;
}

std::shared_ptr<GeoSegment> MySqlGeoSegmentService::read(int id) {

        throwEx("not implemented");
}

void MySqlGeoSegmentService::update(std::shared_ptr<GeoSegment> obj) {
        throwEx("not implemented");
}

void MySqlGeoSegmentService::insert(std::shared_ptr<GeoSegment> obj) {

        MLOG(3) << "inserting new GeoSegment in db : " << obj->toJson ();
        std::string queryStr =
                "INSERT INTO geosegment"
                " ("
                " SEGMENT_NAME,"
                " LAT,"
                " LON,"
                " SEGMENT_RADIUS_IN_MILE,"
                " GEO_SEGMENT_LIST_ID,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '_SEGMENT_NAME',"
                " '_lat',"
                " '_lon',"
                " '_segmentRadiusInMile',"
                " '_geoSegmentListId',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "_SEGMENT_NAME",
                                              obj->segmentName);
        queryStr = StringUtil::replaceString (queryStr, "_lat", StringUtil::toStr (obj->lat));
        queryStr = StringUtil::replaceString (queryStr, "_lon", StringUtil::toStr (obj->lon));
        queryStr = StringUtil::replaceString (queryStr, "_segmentRadiusInMile",
                                              StringUtil::toStr (obj->segmentRadiusInMile));
        queryStr = StringUtil::replaceString (queryStr, "_geoSegmentListId",
                                              StringUtil::toStr (obj->geoSegmentListId));

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);

        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        obj->id = driver->getLastInsertedId ();

}

void MySqlGeoSegmentService::deleteAll() {
        driver->deleteAll("geosegment");
}
