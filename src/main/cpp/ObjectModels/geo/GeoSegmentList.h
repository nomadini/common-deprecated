#ifndef GeoSegmentList_H
#define GeoSegmentList_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "GeoSegment.h"
#include "JsonTypeDefs.h"
#include "Object.h"


class GeoSegmentList;


class GeoSegmentList : public Object {

public:
int id;
int advertiserId;
std::string name;
std::vector<std::shared_ptr<GeoSegment> > geoSegments;
std::string status;
std::string createdAt;
std::string updatedAt;

std::string toJson();
std::string toString();

GeoSegmentList();
virtual ~GeoSegmentList();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<GeoSegmentList> fromJsonValue(RapidJsonValueType value);
};

#endif
