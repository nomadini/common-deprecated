#include "GeoSegment.h"
#include "JsonUtil.h"
#include <string>
#include <memory>
#include "CollectionUtil.h"
GeoSegment::GeoSegment()  : Object(__FILE__) {
        lat = 0;
        lon = 0;
        segmentRadiusInMile = 0;
        geoSegmentListId = 0;
        id = 0;
}


std::string GeoSegment::toString() {
        return this->toJson ();
}

std::string GeoSegment::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "segmentName", segmentName, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "lat", lat, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "lon", lon, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "segmentRadiusInMile", segmentRadiusInMile, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "geoSegmentListId", geoSegmentListId, value);

        return JsonUtil::valueToString (value);
}

GeoSegment::~GeoSegment() {

}
