/*
 * TagPlace.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef TagPlace_H_
#define TagPlace_H_


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include "JsonTypeDefs.h"


#include "Poco/DateTime.h"
#include "Object.h"

class TagPlace;


class TagPlace : public Object {

public:
int id;
int placeId;
int tagId;

TagPlace();

std::string toString();

std::string toJson();

static std::shared_ptr<TagPlace> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<TagPlace> fromJson(std::string jsonString);
virtual ~TagPlace();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
