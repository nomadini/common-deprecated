#ifndef TgGeoFeatureCollectionMapCacheService_h
#define TgGeoFeatureCollectionMapCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>

#include "TgGeoFeatureCollectionMap.h"
class TargetGroupCacheService;
#include "MySqlCreativeService.h"
#include "MySqlTgGeoFeatureCollectionMapService.h"
#include "MySqlTgGeoFeatureCollectionMapService.h"
#include "HttpUtilService.h"
#include "DataProvider.h"
#include "CacheService.h"
#include "EntityProviderService.h"
class EntityToModuleStateStats;
#include <unordered_set>
#include "Object.h"
class TgGeoFeatureCollectionMapCacheService;



class TgGeoFeatureCollectionMapCacheService : public CacheService<TgGeoFeatureCollectionMap>, public Object {

private:

EntityToModuleStateStats* entityToModuleStateStats;
public:

MySqlTgGeoFeatureCollectionMapService* mySqlTgGeoFeatureCollectionMapService;

MySqlCreativeService* mySqlCreativeService;

TargetGroupCacheService* targetGroupCacheService;

std::shared_ptr<std::unordered_set<std::string> > allTgGeoFeatureCollectionKeys;
std::shared_ptr<std::unordered_set<int> > targetGroupIdsWithGeoCollectionAssignments;

TgGeoFeatureCollectionMapCacheService(MySqlTgGeoFeatureCollectionMapService* mySqlTgGeoFeatureCollectionMapService,
                                      HttpUtilService* httpUtilService,
                                      std::string dataMasterUrl,
                                      EntityToModuleStateStats* entityToModuleStateStats,std::string appName);

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<TgGeoFeatureCollectionMap> > allEntities);

virtual ~TgGeoFeatureCollectionMapCacheService();

};


#endif
