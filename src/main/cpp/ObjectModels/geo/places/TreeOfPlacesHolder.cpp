#include "TreeOfPlacesHolder.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

#include "EntityToModuleStateStats.h"
#include "JsonArrayUtil.h"
#include "Place.h"

TreeOfPlacesHolder::TreeOfPlacesHolder(
								std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Tag> > > placeIdsToTags
								) : Object(__FILE__) {
								treeOfPlaces = std::make_shared<TreeOfPlacesType>();
								this->placeIdsToTags = placeIdsToTags;
}

int TreeOfPlacesHolder::getSize() {
								// get shared access
								boost::shared_lock<boost::shared_mutex> lock(_access);

								return treeOfPlaces->size();
}

std::vector<BoxToPlaceValue> TreeOfPlacesHolder::getIntersectingPlaces(double deviceLat, double deviceLon, int maxNumebrOfWantedFeatures) {
								// get shared access
								boost::shared_lock<boost::shared_mutex> lock(_access);

								std::vector<BoxToPlaceValue> intersectingResultSet;
								Point here(deviceLat, deviceLon);
								treeOfPlaces->query(boost::geometry::index::intersects(here), std::back_inserter(intersectingResultSet));
								return intersectingResultSet;
}

std::vector<BoxToPlaceValue> TreeOfPlacesHolder::getNearestPlaces(double deviceLat, double deviceLon, int maxNumebrOfWantedFeatures) {
								// get shared access
								boost::shared_lock<boost::shared_mutex> lock(_access);

								std::vector<BoxToPlaceValue> nearestResultSet;
								Point here(deviceLat, deviceLon);
								MLOG(2) << "treeOfPlaces size : "<< treeOfPlaces->size();
								treeOfPlaces->query(bgi::nearest(here, maxNumebrOfWantedFeatures), std::back_inserter(nearestResultSet));
								return nearestResultSet;
}

TreeOfPlacesHolder::~TreeOfPlacesHolder() {

}

void TreeOfPlacesHolder::addToTree(std::shared_ptr<Place> place) {

								// get upgradable access
								boost::upgrade_lock<boost::shared_mutex> lock(_access);
								// get exclusive access
								boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
								// now we have exclusive access

								treeOfPlaces->insert(std::make_pair(bg::return_envelope<Box>(*place->polygon), place));
								auto tagHolder = placeIdsToTags->getOptional(place->id);
								if (tagHolder == nullptr) {
																LOG_EVERY_N(ERROR, 20000) <<google::COUNTER<<"th palce with id " << place->id << " doesnt have any tags placeIdsToTags size :"<<placeIdsToTags->size();
								} else {
																place->tags = *tagHolder->values;
								}

}
