#include "GUtil.h"


#include "Place.h"
#include "MySqlPlaceService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"
#include "Tag.h"
#include "TreeOfPlacesHolder.h"

MySqlPlaceService::MySqlPlaceService(MySqlDriver* driver)
        : MySqlService(driver, nullptr), Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<Place> > MySqlPlaceService::readAllEntities() {
        return this->readAll();


}

void MySqlPlaceService::readIntoMap(std::shared_ptr<TreeOfPlacesHolder> treeOfPlacesHolder) {

        std::string query = getSelectAllQueryStatement();

        auto res = driver->executeQuery(query);
        while (res->next ()) {
                auto place = mapResultSetToObject(res);
                if (place->polygonStr.empty()) {
                        LOG(ERROR) << "place has no polygon, delete it from db. place : "<< place->toJson();
                } else {
                        treeOfPlacesHolder->addToTree(place);
                        LOG_EVERY_N(INFO, 100000)<<google::COUNTER<< "th place was added to tree";
                }

        }

}

std::string MySqlPlaceService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     " PLACE.`ID`, "
                                                     " PLACE.`DESCR`, "
                                                     " PLACE.`RADIUS`, "
                                                     " PLACE.`LATITUDE`, "
                                                     " PLACE.`LONGITUDE`,"
                                                     " PLACE.`ADDRESS`,"
                                                     " PLACE.`CITY`,"
                                                     " PLACE.`STATE`,"
                                                     " PLACE.`POSTAL_CODE`,"
                                                     " PLACE.`POLYGON`"
                                                     " FROM `PLACE` LIMIT " + _toStr(maxNumberOfPlacesToLoad->getValue());

        return selectAllQueryStatement;
}

std::string MySqlPlaceService::getInsertObjectSqlStatement(std::shared_ptr<Place> deal) {
        throwEx("not supported");
}

std::shared_ptr<Place> MySqlPlaceService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<Place> obj = std::make_shared<Place> ();
        obj->id = res->getInt ("ID");
        obj->lat = res->getDouble ("LATITUDE");
        obj->lon = res->getDouble ("LONGITUDE");
        obj->radius = res->getDouble ("RADIUS");
        obj->address = res->getString ("ADDRESS");
        obj->city = res->getString ("CITY");
        obj->postalCode = res->getString ("POSTAL_CODE");
        obj->description = MySqlDriver::getString(res, "DESCR");
        obj->polygonStr = MySqlDriver::getString(res, "POLYGON");
        if (!obj->polygonStr.empty()) {
                obj->polygon = obj->constructPolygon(obj->extractPointsFromFeatureInJson(obj->polygonStr));
        }

        MLOG(3) << "reading deal from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlPlaceService::getReadByIdSqlStatement(int id) {
        std::string query =
                " SELECT "
                " PLACE.`ID`, "
                " PLACE.`DESCR`, "
                " PLACE.`RADIUS`, "
                " PLACE.`LATITUDE`, "
                " PLACE.`LONGITUDE`,"
                " PLACE.`ADDRESS`,"
                " PLACE.`CITY`,"
                " PLACE.`STATE`,"
                " PLACE.`POSTAL_CODE`,"
                " PLACE.`POLYGON`"
                " FROM `PLACE`"
                " WHERE ID = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

void MySqlPlaceService::deleteAll() {
        throwEx("not supported");
}

MySqlPlaceService::~MySqlPlaceService() {
}
