#include "Tag.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"
Tag::Tag() : Object(__FILE__) {
								id = 0;
								parentId = 0;


}

std::string Tag::toString() {
								return toJson();

}

std::shared_ptr<Tag> Tag::fromJson(std::string jsonString) {
								std::shared_ptr<Tag> tag = std::make_shared<Tag>();
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

std::shared_ptr<Tag> Tag::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<Tag> tag = std::make_shared<Tag>();
								tag->id  = JsonUtil::GetIntSafely(value, "id");
								tag->parentId  = JsonUtil::GetIntSafely(value, "parentId");
								tag->name  = JsonUtil::GetStringSafely(value, "name");
								tag->description  = JsonUtil::GetStringSafely(value, "description");
								return tag;
}

void Tag::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
								JsonUtil::addMemberToValue_FromPair(doc, "parentId", parentId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "name", name, value);
								JsonUtil::addMemberToValue_FromPair(doc, "description", description, value);
}

std::string Tag::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);

}

Tag::~Tag() {

}
