#ifndef MySqlTgGeoFeatureCollectionMapService_h
#define MySqlTgGeoFeatureCollectionMapService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "TgGeoFeatureCollectionMap.h"
#include "MySqlService.h"
#include "DataProvider.h"
#include "Object.h"
class MySqlTgGeoFeatureCollectionMapService;


class MySqlTgGeoFeatureCollectionMapService :
        public MySqlService<int, TgGeoFeatureCollectionMap>,
        public DataProvider<TgGeoFeatureCollectionMap>, public Object {

public:

MySqlDriver* driver;

MySqlTgGeoFeatureCollectionMapService(MySqlDriver* driver);

std::shared_ptr<TgGeoFeatureCollectionMap> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getSelectAllQueryStatement();
std::string getReadByIdSqlStatement(int id);
std::string getInsertObjectSqlStatement(std::shared_ptr<TgGeoFeatureCollectionMap> geoFeature);
std::vector<std::shared_ptr<TgGeoFeatureCollectionMap>> readAllEntities();
virtual ~MySqlTgGeoFeatureCollectionMapService();

};


#endif
