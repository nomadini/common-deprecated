//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlTagPlaceService_h
#define MySqlTagPlaceService_h



#include "Object.h"
#include "TagPlace.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlService.h"
#include "AtomicLong.h"

class MySqlTagPlaceService;


class MySqlTagPlaceService : public DataProvider<TagPlace>, public MySqlService<int, TagPlace>, public Object {

public:

MySqlDriver* driver;

MySqlTagPlaceService(MySqlDriver* driver);

virtual ~MySqlTagPlaceService();
std::shared_ptr<gicapods::AtomicLong> maxNumberOfPlacesToLoad;
std::string getSelectAllQueryStatement();
std::shared_ptr<TagPlace> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<TagPlace> campaign);
std::string getReadByIdSqlStatement(int id);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<TagPlace> > readAllEntities();
virtual void deleteAll();
};

#endif
