#include "TagPlace.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

TagPlace::TagPlace() : Object(__FILE__) {
								placeId = 0;
								tagId = 0;
								id = 0;

}

std::string TagPlace::toString() {
								return toJson();

}

std::shared_ptr<TagPlace> TagPlace::fromJson(std::string jsonString) {
								std::shared_ptr<TagPlace> deal = std::make_shared<TagPlace>();
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

std::shared_ptr<TagPlace> TagPlace::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<TagPlace> deal = std::make_shared<TagPlace>();
								deal->id  = JsonUtil::GetIntSafely(value, "id");
								deal->tagId  = JsonUtil::GetIntSafely(value, "tagId");
								deal->placeId  = JsonUtil::GetIntSafely(value, "placeId");
								return deal;
}

void TagPlace::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
								JsonUtil::addMemberToValue_FromPair(doc, "tagId", tagId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "placeId", placeId, value);
}

std::string TagPlace::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);

}

TagPlace::~TagPlace() {

}
