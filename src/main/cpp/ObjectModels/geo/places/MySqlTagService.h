//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlTagService_h
#define MySqlTagService_h



#include "Object.h"
#include "Tag.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlService.h"

class MySqlTagService;


class MySqlTagService : public DataProvider<Tag>, public MySqlService<int, Tag>, public Object {

public:

MySqlDriver* driver;

MySqlTagService(MySqlDriver* driver);

virtual ~MySqlTagService();

std::string getSelectAllQueryStatement();
std::shared_ptr<Tag> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<Tag> campaign);
std::string getReadByIdSqlStatement(int id);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<Tag> > readAllEntities();
virtual void deleteAll();
};

#endif
