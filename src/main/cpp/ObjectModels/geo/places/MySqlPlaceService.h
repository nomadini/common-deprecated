//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlPlaceService_h
#define MySqlPlaceService_h



#include "Object.h"
#include "Place.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>

#include <memory>
#include "DataProvider.h"
#include "MySqlService.h"

class MySqlPlaceService;
class TreeOfPlacesHolder;
class Tag;


class MySqlPlaceService : public DataProvider<Place>, public MySqlService<int, Place>, public Object {

public:

MySqlDriver* driver;

std::shared_ptr<gicapods::AtomicLong> maxNumberOfPlacesToLoad;

MySqlPlaceService(MySqlDriver* driver);

virtual ~MySqlPlaceService();

std::string getSelectAllQueryStatement();
std::shared_ptr<Place> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<Place> campaign);
std::string getReadByIdSqlStatement(int id);
void readIntoMap(std::shared_ptr<TreeOfPlacesHolder> treeOfPlacesHolder);
//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<Place> > readAllEntities();
virtual void deleteAll();
};

#endif
