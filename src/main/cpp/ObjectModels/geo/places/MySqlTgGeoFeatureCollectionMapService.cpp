#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlTgGeoFeatureCollectionMapService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

MySqlTgGeoFeatureCollectionMapService::MySqlTgGeoFeatureCollectionMapService(MySqlDriver* driver)
        : MySqlService(driver, nullptr), Object(__FILE__) {
        this->driver = driver;
}

MySqlTgGeoFeatureCollectionMapService::~MySqlTgGeoFeatureCollectionMapService() {

}

std::shared_ptr<TgGeoFeatureCollectionMap> MySqlTgGeoFeatureCollectionMapService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<TgGeoFeatureCollectionMap> obj = std::make_shared<TgGeoFeatureCollectionMap> ();
        obj->id = res->getInt ("id");
        obj->targetGroupId = res->getInt ("targetgroup_id");
        obj->geoFeatureCollectionId = res->getInt ("geocollection_id");
        obj->tgGeoCollectionKey = StringUtil::toStr(obj->targetGroupId) +"g"+ StringUtil::toStr(obj->geoFeatureCollectionId);
        obj->createdAt = MySqlDriver::parseDateTime(res, "created_at");
        obj->updatedAt = MySqlDriver::parseDateTime(res, "updated_at");

        MLOG(3) << "reading campaign from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlTgGeoFeatureCollectionMapService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = "SELECT "
                                                     " id,"
                                                     " targetgroup_id,"
                                                     " geocollection_id,"
                                                     " created_at, "
                                                     " updated_at "
                                                     " FROM `targetgroup_geo_segment_collection_map`";
        return selectAllQueryStatement;
}

std::string MySqlTgGeoFeatureCollectionMapService::getReadByIdSqlStatement(int id) {
        std::string query = "SELECT "
                            " id,"
                            " targetgroup_id,"
                            " geocollection_id,"
                            " created_at, "
                            " updated_at "
                            " FROM targetgroup_geo_segment_collection_map where id = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

std::string MySqlTgGeoFeatureCollectionMapService::getInsertObjectSqlStatement(std::shared_ptr<TgGeoFeatureCollectionMap> geoFeature) {
        std::string queryStr =
                " INSERT INTO `targetgroup_geo_segment_collection_map`"
                " ("
                " targetgroup_id,"
                " geocollection_id,"
                " created_at,"
                " updated_at)"
                " VALUES"
                " ( "
                " '__targetgroup_id__',"
                " '__geocollection_id__',"
                " '__created_at__',"
                " '__updated_at__');";

        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr(geoFeature->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__geocollection_id__",
                                              StringUtil::toStr(geoFeature->geoFeatureCollectionId));
        queryStr = StringUtil::replaceString (queryStr, "__created_at__",
                                              DateTimeUtil::dateTimeToStr(geoFeature->createdAt));
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__",
                                              DateTimeUtil::dateTimeToStr(geoFeature->updatedAt));
        return queryStr;
}

std::vector<std::shared_ptr<TgGeoFeatureCollectionMap>> MySqlTgGeoFeatureCollectionMapService::readAllEntities() {
        return this->readAll();
}
