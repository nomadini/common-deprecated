/*
 * TreeOfPlacesHolder.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef TreeOfPlacesHolder_H_
#define TreeOfPlacesHolder_H_


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include "Place.h"
#include "AtomicBoolean.h"

#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"
#include "AtomicLong.h"
#include <boost/thread.hpp>
class EntityToModuleStateStats;

class TreeOfPlacesHolder : public Object {
private:
std::shared_ptr<TreeOfPlacesType> treeOfPlaces;
boost::shared_mutex _access;

public:

TreeOfPlacesHolder(
        std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Tag> > > placeIdsToTags
        );
std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Tag> > > placeIdsToTags;
void addToTree(std::shared_ptr<Place> place);

std::vector<BoxToPlaceValue> getNearestPlaces(double deviceLat, double deviceLon, int maxNumebrOfWantedFeatures);
std::vector<BoxToPlaceValue> getIntersectingPlaces(double deviceLat, double deviceLon, int maxNumebrOfWantedFeatures);
int getSize();

virtual ~TreeOfPlacesHolder();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
