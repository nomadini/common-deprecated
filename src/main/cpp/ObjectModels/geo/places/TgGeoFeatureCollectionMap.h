#ifndef TgGeoFeatureCollectionMap_H
#define TgGeoFeatureCollectionMap_H

#include <memory>
#include <string>
#include <vector>
#include "Poco/DateTime.h"
#include "JsonTypeDefs.h"

class DateTimeUtil;
#include "Object.h"
class TgGeoFeatureCollectionMap;


class TgGeoFeatureCollectionMap : public Object {

private:

public:

int id;
int targetGroupId;
int geoFeatureCollectionId;
std::string tgGeoCollectionKey;

Poco::DateTime createdAt;
Poco::DateTime updatedAt;

TgGeoFeatureCollectionMap();
static std::string getEntityName();

std::string toString();
std::string toJson();
static std::shared_ptr<TgGeoFeatureCollectionMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

virtual ~TgGeoFeatureCollectionMap();

};


#endif
