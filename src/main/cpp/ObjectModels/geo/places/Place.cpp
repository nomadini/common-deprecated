#include "Place.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

#include "JsonArrayUtil.h"
#include "Tag.h"

Place::Place() : Object(__FILE__) {
								id = 0;
								radius = 0;
								lon = 0;
								lat = 0;
}

std::string Place::toString() {
								return toJson();

}

std::shared_ptr<Place> Place::fromJson(std::string jsonString) {
								std::shared_ptr<Place> place = std::make_shared<Place>();
								auto doc = parseJsonSafely(jsonString);
								return fromJsonValue(*doc);
}

std::shared_ptr<Place> Place::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<Place> place = std::make_shared<Place>();
								place->id  = JsonUtil::GetIntSafely(value, "id");
								place->radius  = JsonUtil::GetDoubleSafely(value, "radius");
								place->lon  = JsonUtil::GetDoubleSafely(value, "lon");
								place->lat  = JsonUtil::GetDoubleSafely(value, "lat");
								place->description  = JsonUtil::GetStringSafely(value, "description");
								place->address  = JsonUtil::GetStringSafely(value, "address");
								place->city  = JsonUtil::GetStringSafely(value, "city");
								place->state  = JsonUtil::GetStringSafely(value, "state");
								place->postalCode  = JsonUtil::GetStringSafely(value, "postalCode");
								place->polygonStr  = JsonUtil::GetStringSafely(value, "polygon");
								JsonArrayUtil::getArrayFromValueMemeber(value, "tags", place->tags);
								place->polygon = place->constructPolygon(place->extractPointsFromFeatureInJson(place->polygonStr));
								return place;
}

void Place::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
								JsonUtil::addMemberToValue_FromPair(doc, "radius", radius, value);
								JsonUtil::addMemberToValue_FromPair(doc, "lon", lon, value);
								JsonUtil::addMemberToValue_FromPair(doc, "lat", lat, value);
								JsonUtil::addMemberToValue_FromPair(doc, "description", description, value);
								JsonUtil::addMemberToValue_FromPair(doc, "address", address, value);
								JsonUtil::addMemberToValue_FromPair(doc, "city", city, value);
								JsonUtil::addMemberToValue_FromPair(doc, "state", state, value);
								JsonUtil::addMemberToValue_FromPair(doc, "postalCode", postalCode, value);
								JsonUtil::addMemberToValue_FromPair(doc, "polygon", polygonStr, value);
								JsonArrayUtil::addMemberToValue_FromPair(
																doc,
																"tags",
																tags,
																value);
}

std::string Place::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);

}

std::shared_ptr<Polygon> Place::constructPolygon(std::vector<std::pair<float,float> > points) {
								auto polygon = std::make_shared<Polygon> ();
								for (std::pair<float, float> pair : points )
								{
																polygon->outer().push_back(Point(pair.first, pair.second));
								}

								return polygon;
}

std::vector<std::pair<float, float> > Place::extractPointsFromFeatureInJson(std::string featuresInJson) {

								auto doc = JsonUtil::parseJsonSafely(featuresInJson);
								RapidJsonValueTypeNoRef inner1;
								RapidJsonValueTypeNoRef inner2;
								assertAndThrow((*doc).HasMember("geometry"));
								assertAndThrow((*doc).HasMember("type"));
								JsonArrayUtil::getNthValueOfArray((*doc)["geometry"], 0, inner1);

								std::vector<std::pair<float,float> > allPairs;
								std::string type = (*doc)["type"].GetString();
								if (!StringUtil::equalsIgnoreCase(type, "Polygon")) {
																//TODO : fix this later
																// LOG(WARNING) << "type of geometry is "<< type<<"..ignoring for now.fix later";
																return allPairs;
								}

								// JsonArrayUtil::getNthValueOfArray(inner1, 0, inner2);

								for (rapidjson::SizeType i = 0; i < inner1.Size (); i++)         // rapidjson uses SizeType instead of size_t.
								{
																std::pair<float, float> pair;
																auto &obj = inner1[i];
																if (!obj.IsArray()) {
																								JsonUtil::printTypeAndValueOfJsonElement(obj);
																}
																if (!obj[0].IsDouble()) {
																								JsonUtil::printTypeAndValueOfJsonElement(obj);
																								JsonUtil::printTypeAndValueOfJsonElement(obj[0]);
																}
																if (!obj[1].IsDouble()) {
																								JsonUtil::printTypeAndValueOfJsonElement(obj);

																								JsonUtil::printTypeAndValueOfJsonElement(obj[1]);
																}

																assertAndThrow(obj.IsArray());
																assertAndThrow(obj[0].IsDouble());
																assertAndThrow(obj[1].IsDouble());
																pair.first = obj[0].GetDouble();
																pair.second = obj[1].GetDouble();
																// MLOG(3)<<" pair is : "<< pair.first << " , " << pair.second;
																allPairs.push_back(pair);
								}
								return allPairs;
}
Place::~Place() {

}
