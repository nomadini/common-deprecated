/*
 * PlaceFinderModule.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef PlaceFinderModule_H_
#define PlaceFinderModule_H_


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include "Place.h"
#include "AtomicBoolean.h"
class EntityToModuleStateStats;
class TreeOfPlacesHolder;

class PlaceFinderModule : public Object {

public:

PlaceFinderModule(std::shared_ptr<TreeOfPlacesHolder> treeOfPlacesHolder);
std::shared_ptr<TreeOfPlacesHolder> treeOfPlacesHolder;
EntityToModuleStateStats* entityToModuleStateStats;

std::vector<std::shared_ptr<Place> >
findIntersectingFeatures(double deviceLat, double deviceLon, int maxNumebrOfWantedFeatures);

std::vector<std::shared_ptr<Place> > findNearestFeatures(
        double deviceLat,
        double deviceLon,
        int maxNumebrOfWantedFeatures);

virtual ~PlaceFinderModule();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
