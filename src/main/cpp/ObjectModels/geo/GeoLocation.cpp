#include "GeoLocation.h"
#include "JsonUtil.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
GeoLocation::GeoLocation() {
        id = 0;
        country = "";
        state = "";
        city = "";
}

std::string GeoLocation::toString() {
        return this->toJson ();
}

void GeoLocation::setCountry(std::string country) {
        assertAndThrow(!country.empty());
        country = StringUtil::toLowerCase(country);
        if (StringUtil::containsCaseInSensitive(country, "United States")) {
                this->country =  "usa";
        }
        if (this->country.empty()) {
                throwEx("country cannot be empty");
        }
}

void GeoLocation::setState(std::string state) {
        assertAndThrow(!state.empty());
        state = StringUtil::toLowerCase(state);
        this->state = state;
}
void GeoLocation::setCity(std::string city) {
        if (city.empty()) {
                this->city = "unknown";
        } else {
                city = StringUtil::toLowerCase(city);
        }
        this->city = city;
}

std::string GeoLocation::getCountry() {
        return country;
}

std::string GeoLocation::getState() {
        return state;
}

std::string GeoLocation::getCity() {
        return city;
}

std::string GeoLocation::getEntityName() {
        return "GeoLocation";
}

void GeoLocation::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "state", state, value);

        JsonUtil::addMemberToValue_FromPair (doc, "country", country, value);
        JsonUtil::addMemberToValue_FromPair (doc, "city", city, value);
}

std::shared_ptr<GeoLocation> GeoLocation::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<GeoLocation> geoLocation = std::make_shared<GeoLocation>();
        geoLocation->id = JsonUtil::GetIntSafely(value, "id");
        geoLocation->city = JsonUtil::GetStringSafely(value, "city");

        geoLocation->state  =JsonUtil::GetStringSafely(value,"state");
        geoLocation->country = JsonUtil::GetStringSafely(value,"country");

        return geoLocation;
}

std::string GeoLocation::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

bool GeoLocation::equalFunction(std::shared_ptr<GeoLocation> v1, std::shared_ptr<GeoLocation> v2) {
        auto res1 = StringUtil::equalsIgnoreCase(v1->city, v2->city);
        auto res2 = StringUtil::equalsIgnoreCase(v1->country, v2->country);
        auto res3 = StringUtil::equalsIgnoreCase(v1->state, v2->state);
        auto overall = res1 && res2 && res3;
        if(!overall) {
                MLOG(3)<<"rest1 : " <<res1 << " , res2" << res2 << " , res3"<< res3;
        }
        return overall;
}

GeoLocation::~GeoLocation() {

}
