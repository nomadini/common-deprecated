#ifndef GeoLocation_H
#define GeoLocation_H

#include <string>
#include <memory>
#include <vector>
#include <unordered_map>
#include "JsonTypeDefs.h"


class GeoLocation;





class GeoLocation {

private:

std::string country;

std::string state;
std::string city;
public:
int id;

GeoLocation();

std::string toString();

std::string toJson();

virtual ~GeoLocation();
void setCountry(std::string country);
void setState(std::string state);
void setCity(std::string city);

std::string getCountry();
std::string getState();
std::string getCity();


void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<GeoLocation> fromJsonValue(RapidJsonValueType value);
static std::string getEntityName();
static bool equalFunction(std::shared_ptr<GeoLocation> v1, std::shared_ptr<GeoLocation> v2);
};
#endif
