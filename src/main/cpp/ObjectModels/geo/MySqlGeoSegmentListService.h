#ifndef MySqlGeoSegmentListService_h
#define MySqlGeoSegmentListService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "GeoSegmentList.h"
#include "GeoSegment.h"
#include "MySqlGeoSegmentService.h"
#include "HttpUtilService.h"

class MySqlGeoSegmentListService;




class MySqlGeoSegmentListService {

public:

std::shared_ptr<std::unordered_map<int, std::shared_ptr<GeoSegmentList> > > allGeoSegmentListMap;
std::shared_ptr<std::vector<std::shared_ptr<GeoSegmentList> > > allGeoSegmentList;


MySqlDriver* driver;
std::shared_ptr<MySqlGeoSegmentService> mySqlGeoSegmentService;

std::string dataMasterUrl;

HttpUtilService* httpUtilService;


MySqlGeoSegmentListService(MySqlDriver* driver,
                           HttpUtilService* httpUtilService,
                           std::string dataMasterUrl);

std::vector<std::shared_ptr<GeoSegmentList> > readAll();

std::shared_ptr<GeoSegmentList> read(int id);

void update(std::shared_ptr<GeoSegmentList> obj);

void insert(std::shared_ptr<GeoSegmentList> obj);

virtual void deleteAll();

virtual ~MySqlGeoSegmentListService();

std::shared_ptr<std::vector<std::shared_ptr<GeoSegmentList> > > getAllGeoSegmentLists();

std::shared_ptr<std::unordered_map<int, std::shared_ptr<GeoSegmentList> > > getAllGeoSegmentListMap();

};


#endif
