
#include "GeoSegmentList.h"
#include "JsonUtil.h"

GeoSegmentList::GeoSegmentList() : Object(__FILE__) {
        id = 0;
        advertiserId = 0;
}

GeoSegmentList::~GeoSegmentList() {

}

void GeoSegmentList::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "advertiserId", advertiserId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "status", status, value);
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", createdAt, value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", updatedAt, value);
}
std::shared_ptr<GeoSegmentList> GeoSegmentList::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<GeoSegmentList> geoSegmentList = std::make_shared<GeoSegmentList>();
        geoSegmentList->id = JsonUtil::GetIntSafely(value, "id");
        geoSegmentList->advertiserId  =JsonUtil::GetIntSafely(value,"advertiserId");
        geoSegmentList->name = JsonUtil::GetStringSafely(value,"name");
        geoSegmentList->status = JsonUtil::GetStringSafely(value,"status");
        geoSegmentList->createdAt = JsonUtil::GetStringSafely(value,"createdAt");
        geoSegmentList->updatedAt = JsonUtil::GetStringSafely(value,"updatedAt");
        return geoSegmentList;
}

std::string GeoSegmentList::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}
std::string GeoSegmentList::toString() {
        return toJson();
}
