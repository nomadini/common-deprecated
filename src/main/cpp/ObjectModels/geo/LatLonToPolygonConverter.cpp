#include "LatLonToPolygonConverter.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "StringUtil.h"
#include "GUtil.h"
#include <cmath>

#include <iostream>
#include <GeographicLib/UTMUPS.hpp>
#include <GeographicLib/MGRS.hpp>
using namespace GeographicLib;
# define M_PI           3.14159265358979323846  /* pi */
LatLonToPolygonConverter::LatLonToPolygonConverter(
        EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;
}

LatLonToPolygonConverter::~LatLonToPolygonConverter() {

}

double LatLonToPolygonConverter::toRadians(double angleInDegrees) {
        return angleInDegrees * M_PI / 180;
}

double LatLonToPolygonConverter::toDegrees(double angleInRadians) {
        return angleInRadians * 180 / M_PI;
}

std::tuple<double, double> LatLonToPolygonConverter::offset(double lonArg, double latArg, int distance, double bearing) {
        double lat1 = toRadians(latArg);
        double lon1 = toRadians(lonArg);
        double dByR = distance / 6378137; // distance divided by 6378137 (radius of the earth) wgs84
        double lat = asin(
                sin(lat1) * cos(dByR) +
                cos(lat1) * sin(dByR) * cos(bearing));

        double lon = lon1 + atan2(
                sin(bearing) * sin(dByR) * cos(lat1),
                cos(dByR) - sin(lat1) * sin(lat));

        std::tuple<double, double> tuple =
                std::make_tuple(toDegrees(lon), toDegrees(lat));
        return tuple;
}

std::string LatLonToPolygonConverter::circleToPolygon(double lon, double lat, int radius, int numberOfSegments) {
        int n = numberOfSegments;
        std::vector<std::tuple<double, double> > flatCoordinates;
        std::vector<std::vector<std::tuple<double, double> > > coordinates;
        for (int i = 0; i < n; ++i) {
                // flatCoordinates.push.apply(flatCoordinates, offset(center, radius, 2 * M_PI * i / n));
                flatCoordinates.push_back(offset(lon, lat, radius, 2 * M_PI * i / n));
        }

        flatCoordinates.push_back(flatCoordinates[0]);
        flatCoordinates.push_back(flatCoordinates[1]);


        for (int i = 0, j = 0; j < flatCoordinates.size(); j += 2) {
                // coordinates[i++] = flatCoordinates.slice(j, j + 2);
                std::tuple<double, double> fC = flatCoordinates.at(j);
                std::tuple<double, double> sC = flatCoordinates.at(j+2);

                std::vector<std::tuple<double, double> > oneCordinate;
                oneCordinate.push_back(fC);
                oneCordinate.push_back(sC);
                LOG(INFO) <<"[ "<<  std::get<0>(fC) <<", "<< std::get<1>(fC)<< " ]";
                LOG(INFO) <<"[ "<<  std::get<0>(sC) <<", "<< std::get<1>(sC)<< " ]";
                coordinates.push_back(oneCordinate);
        }

        return "";
        // return {
        //   type: 'Polygon',
        //   coordinates: [coordinates]
        // };
}
