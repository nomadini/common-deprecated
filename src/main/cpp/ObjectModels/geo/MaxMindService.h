//
// Created by Mahmoud Taabodi on 2/14/16.
//

#ifndef EXCHANGESIMULATOR_MAXMINDSERVICE_H
#define EXCHANGESIMULATOR_MAXMINDSERVICE_H

#include <maxminddb.h>

#include <memory>
#include <string>
#include <vector>
#include <set>

class MaxMindService;

class MaxMindService {

public:
std::shared_ptr<MMDB_s> mmdb;
MaxMindService();
virtual ~MaxMindService();

void openDatabase(std::string fname);
void closeDatabase();

std::string dup_entry_string_or_bail(MMDB_entry_data_s entry_data);

bool readIpInfo(const std::string& ipAddress, MMDB_lookup_result_s& result);

std::string getCity(MMDB_lookup_result_s& result);

double getLatitude(MMDB_lookup_result_s& result);

double getLongitude(MMDB_lookup_result_s& result);

int getMetroCode(MMDB_lookup_result_s& result);

std::string getTimeZone(MMDB_lookup_result_s& result);

std::string getPostalCode(MMDB_lookup_result_s& result);

std::string getStateName(MMDB_lookup_result_s& result);

std::string getStateCode(MMDB_lookup_result_s& result);

std::string getCountryCode(MMDB_lookup_result_s& result);

};
#endif //EXCHANGESIMULATOR_MAXMINDSERVICE_H
