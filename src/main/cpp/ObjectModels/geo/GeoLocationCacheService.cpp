#include "GeoLocationCacheService.h"
#include <boost/foreach.hpp>
#include "GeoLocation.h"
#include "JsonArrayUtil.h"
#include "GeoLocationCacheService.h"
#include "StringUtil.h"
#include "MySqlGeoLocationService.h"
GeoLocationCacheService::GeoLocationCacheService(
        MySqlGeoLocationService* mySqlGeoLocationService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) :
        CacheService<GeoLocation>
                (httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {

        this->entityToModuleStateStats = entityToModuleStateStats;

        this->mapOfStateNamesToGeoLocations =
                std::make_shared<gicapods::ConcurrentHashMap<std::string, GeoLocation > >();

        this->mapOfCountryNamesToGeoLocations =
                std::make_shared<gicapods::ConcurrentHashMap<std::string, GeoLocation > >();
}

std::string GeoLocationCacheService::getEntityName() {
        return "GeoLocation";
}

void GeoLocationCacheService::clearOtherCaches() {
        mapOfStateNamesToGeoLocations->clear ();
}

void GeoLocationCacheService::populateOtherMapsAndLists(
        std::vector<std::shared_ptr<GeoLocation> > geolocations) {

        for(auto entity :  geolocations) {
                MLOG(10) << "loading GeoLocation : "<< entity->toJson();
                mapOfStateNamesToGeoLocations->put(StringUtil::toLowerCase(entity->getState()), entity);
                mapOfCountryNamesToGeoLocations->put(StringUtil::toLowerCase(entity->getCountry()), entity);
        }

        MLOG(10) << mapOfStateNamesToGeoLocations->size () << " geo locations were loaded....";
}

GeoLocationCacheService::~GeoLocationCacheService() {

}
