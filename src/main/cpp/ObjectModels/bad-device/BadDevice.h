#ifndef BadDevice_H
#define BadDevice_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "Object.h"
#include "JsonTypeDefs.h"


class BadDevice;


class BadDevice : public Object {

private:

public:


int id;
std::string deviceId;      //this is the unique name that each badDevice is defined by
std::string deviceType;
int numberOfTimesSeenInPeriod;
std::string dateCreated;

BadDevice();

std::string toString();

std::string toJson();
static std::string getEntityName();
virtual ~BadDevice();

static std::shared_ptr<BadDevice> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};



#endif
