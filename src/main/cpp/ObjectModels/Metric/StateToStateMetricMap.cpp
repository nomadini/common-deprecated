//
// Created by Mahmoud Taabodi on 3/6/16.
//


#include "AtomicDouble.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include <tbb/concurrent_hash_map.h>

#include "StateToStateMetricMap.h"
#include "StateMetric.h"

StateToStateMetricMap::StateToStateMetricMap() : Object(__FILE__) {

        this->statesToStateMetrics =
                std::make_shared<gicapods::ConcurrentHashMap<std::string, StateMetric > > ();
}

void StateToStateMetricMap::clearRates() {
        statesToStateMetrics->clear ();
}

void StateToStateMetricMap::addStateToMap(std::string state,
                                          std::string metricLevel,
                                          double value,
                                          std::shared_ptr<gicapods::ConcurrentHashMap<std::string, StateMetric > > map) {
        assertAndThrow(map != nullptr);
        auto stateMetric = map->getOptional (state);
        if (stateMetric != nullptr) {

                stateMetric->value->setValue(stateMetric->value->getValue() + value);
        } else {
                assertAndThrow(map != nullptr);
                auto stateMetric = std::make_shared<StateMetric> (state, metricLevel);
                stateMetric->value->setValue (value);
                map->put(state, stateMetric);


        }
}

void StateToStateMetricMap::update(
        std::string state,
        std::string metricLevel,
        double value) {
        //MLOG(3)<<" update : state : " <<  state << " with value : "<< value;
        addStateToMap (state, metricLevel, value, statesToStateMetrics);
}

double StateToStateMetricMap::getOneMinuteStateValue(std::string state) {
        assertAndThrow (statesToStateMetrics != NULL);
        auto stateMetric = statesToStateMetrics->getOptional (state);
        if (stateMetric != nullptr) {
                return stateMetric->value->getValue();
        }
        return 0;
        // throw std::logic_error("didn't find this state : " + state);
}

bool StateToStateMetricMap::hasState(std::string state) {
        assertAndThrow (statesToStateMetrics != NULL);
        auto stateMetric = statesToStateMetrics->getOptional (state);
        if (stateMetric != nullptr) {
                return true;
        }
        return false;
}

double StateToStateMetricMap::getOneMinuteStatePerc(std::string state) {

        double totalValues = 0; //save this in a AtomicDouble value in object state
        tbb::concurrent_hash_map<std::string,  std::shared_ptr<StateMetric> >::iterator iter;
        auto map = statesToStateMetrics->getCopyOfMap();
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {
                std::shared_ptr<StateMetric> stateMetric = iter->second;
                totalValues += stateMetric->value->getValue();
        }

        assertAndThrow (statesToStateMetrics != NULL);
        auto stateMetric = statesToStateMetrics->getOptional (state);
        if (stateMetric != nullptr) {
                double valueOfState = stateMetric->value->getValue ();
                if (totalValues == 0) {
                        return 0;
                        // throw std::logic_error("totalValues == 0 for state " + state);
                }
                return valueOfState * 100/ totalValues;
        }

        return 0;
        // throw std::logic_error("didn't find this state : " + state);
}


std::vector<std::string> StateToStateMetricMap::getAllPossibleStatesInLastOneMinue() {
        std::vector<std::string> allStates;
        tbb::concurrent_hash_map<std::string,  std::shared_ptr<StateMetric> >::iterator iter;
        auto map = statesToStateMetrics->getCopyOfMap();
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {
                allStates.push_back(iter->first);
        }
        MLOG(3)<<" AllPossibleStatesInLastOneMinute : " <<  JsonArrayUtil::convertListToJson (allStates);
        return allStates;
}

std::vector<std::string> StateToStateMetricMap::getAllPossibleStates() {
        return getAllPossibleStatesInLastOneMinue();
}
