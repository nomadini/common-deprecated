//
// Created by Mahmoud Taabodi on 7/15/16.
//

#ifndef InfluxDbClient_H
#define InfluxDbClient_H



#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <boost/thread.hpp>
class EntityToModuleStateStats;
class MySqlMetricService;
class MetricDbRecord;
#include "AtomicBoolean.h"
#include "PocoSession.h"
#include "AtomicDouble.h"
#include "ConfigService.h"
class InfluxDbClient;


class InfluxDbClient : public Object {

public:
std::shared_ptr<PocoSession> session;

std::string influxHost;
EntityToModuleStateStats* entityToModuleStateStats;
InfluxDbClient(EntityToModuleStateStats* entityToModuleStateStats,
               gicapods::ConfigService* configService);
static std::string replaceIllegalCharacters(std::string& candidate);
void recordMetricsInInfluxDb(std::string allInfluxdbRecords, std::string db);
virtual ~InfluxDbClient();
};

#endif //BIDDER_ENTITYTOMODULESTATESTATSPERSISTENCESERVICE_H
