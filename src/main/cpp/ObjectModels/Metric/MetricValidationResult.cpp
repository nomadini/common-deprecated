//
// Created by Mahmoud Taabodi on 7/3/16.
//

#include "JsonUtil.h"
#include "GUtil.h"
#include "MetricValidationResult.h"

MetricValidationResult::MetricValidationResult() : Object(__FILE__) {
}

std::string MetricValidationResult::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc ();
        RapidJsonValueTypeNoRef valueObject(rapidjson::kObjectType);
        JsonUtil::addMemberToValue_FromPair (doc.get(), "metricUniqueName", metricUniqueName, valueObject);
        JsonUtil::addMemberToValue_FromPair (doc.get(), "result", result, valueObject);
        JsonUtil::addMemberToValue_FromPair (doc.get(), "failureType", failureType, valueObject);
        JsonUtil::addMemberToValue_FromPair (doc.get(), "resultMeasure", resultMeasure, valueObject);

        return JsonUtil::valueToString (valueObject);
}


MetricValidationResult::~MetricValidationResult() {

}
