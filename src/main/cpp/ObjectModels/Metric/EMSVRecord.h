#ifndef EMSVRecord_h
#define EMSVRecord_h

#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "Object.h"
#include "ConcurrentHashMap.h"
#include "AtomicBoolean.h"

class EMSVRecord : public Object {
public:
EMSVRecord();
virtual ~EMSVRecord();
std::string entity;
std::string module;
std::string metricLevel;
std::string state;
double value;
};

#endif
