//
// Created by Mahmoud Taabodi on 7/3/16.
//

#ifndef COMMON_METRICDBRECORD_H
#define COMMON_METRICDBRECORD_H
#include "Object.h"
#include "AtomicDouble.h"
class DateTimeUtil;


#include <tbb/concurrent_hash_map.h>
#include <memory>
#include "Poco/DateTime.h"

#include "JsonTypeDefs.h"
class MetricDbRecord;



class MetricDbRecord : public Object {
public:
int id;

std::string hostName;
std::string appName;
std::string appVersion;
std::string domain;     //module name, or filter name or something else
std::string module;
std::string state;
std::string entity;
std::string level;

double value;
Poco::DateTime timeOfRecord;
Poco::DateTime createdAt;
Poco::DateTime updatedAt;
MetricDbRecord();

std::string getName();
std::string toJson();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};


#endif //COMMON_METRICDBRECORD_H
