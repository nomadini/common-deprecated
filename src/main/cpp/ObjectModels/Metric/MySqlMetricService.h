//
// Created by User on 6/12/2016.
//

#ifndef EXCHANGESIMULATOR_MYSQLMETRICSERVICE_H
#define EXCHANGESIMULATOR_MYSQLMETRICSERVICE_H
#include "Object.h"
#include <tbb/concurrent_hash_map.h>
class EntityToModuleStateStats;
#include <cppconn/resultset.h>
#include <memory>
#include <vector>

class ResultSetHolder;
class AtomicDouble;
class DateTimeUtil;
class GUtil;

class MySqlDriver;
class MetricDbRecord;

class MySqlMetricService;



/**
 * this class has writes metrics to mysql table called module_states;
 */
class MySqlMetricService : public Object {

public:

MySqlDriver* driver;
static std::string coreFields;

MySqlMetricService(MySqlDriver* driver);

static std::string convertRecordToValueString(std::shared_ptr<MetricDbRecord> metricDbRecord);
void insert(std::vector<std::shared_ptr<MetricDbRecord> > metricDbRecords);

void insertLimited(std::vector<std::shared_ptr<MetricDbRecord> > metricDbRecords);

std::shared_ptr<MetricDbRecord> readMetricByStateModuleEntity(
        std::string state,
        std::string module,
        std::string entity);

std::vector<std::shared_ptr<MetricDbRecord> > readMetricByModuleAndHostInLastNMinutes(
        std::string module,
        std::string host,
        int lastNminute);

std::vector<std::shared_ptr<MetricDbRecord> > readMetricsInLastNMinutes(int lastNminute,
                                                                        std::string hostname,
                                                                        std::string appName,
                                                                        std::string version,
                                                                        std::string level);

std::vector<std::shared_ptr<MetricDbRecord> >
readAllMetricsInLastNMinutesWithLevel(
        int lastNminute,
        std::string hostname,
        std::string appName,
        std::string version,
        std::string entity,
        std::string state,
        std::string module,
        std::string metricLevel);

void deleteAll();
static std::shared_ptr<MetricDbRecord> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
virtual ~MySqlMetricService();
private:

};

#endif //EXCHANGESIMULATOR_MYSQLMETRICSERVICE_H
