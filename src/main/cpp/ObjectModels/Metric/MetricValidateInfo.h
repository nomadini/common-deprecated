//
// Created by Mahmoud Taabodi on 7/3/16.
//

#ifndef MetricValidateInfo_H
#define MetricValidateInfo_H
#include "Object.h"
#include "AtomicDouble.h"
class DateTimeUtil;
class GUtil;

class MetricDbRecord;
class MetricValidationResult;
#include <tbb/concurrent_hash_map.h>
#include <memory>


class MetricValidateInfo;



class MetricValidateInfo : public Object {
public:


//this comes from state, module and entity and appName
std::string metricUniqueName;
MetricValidateInfo();
virtual std::shared_ptr<MetricValidationResult> validate(std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<MetricDbRecord> > >
                                                         mapOfValidateInfoNameToMetricDbRecord) = 0;
std::string toJson();
virtual ~MetricValidateInfo();
};


#endif //COMMON_METRICDBRECORD_H
