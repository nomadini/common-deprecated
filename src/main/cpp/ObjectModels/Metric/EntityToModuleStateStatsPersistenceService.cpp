//
// Created by Mahmoud Taabodi on 7/15/16.
//

#include "EntityToModuleStateStatsPersistenceService.h"
#include "EntityToModuleStateStats.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "StateToStateMetricMap.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlMetricService.h"
#include "MySqlMetricService.h"
#include "MetricDbRecord.h"
#include "CollectionUtil.h"
#include "MetricDbRecord.h"
#include "NetworkUtil.h"
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include <thread>
#include "MetricDbRecord.h"
#include "StateMetric.h"
#include "HttpUtil.h"
#include "ModulesToStatePercentages.h"
#include "ConfigService.h"

std::string EntityToModuleStateStatsPersistenceService::appName;
std::string EntityToModuleStateStatsPersistenceService::appVersion;
std::string EntityToModuleStateStatsPersistenceService::hostName;

EntityToModuleStateStatsPersistenceService::EntityToModuleStateStatsPersistenceService(
        EntityToModuleStateStats* entityToModuleStateStats,
        MySqlMetricService* mySqlMetricService,
        gicapods::ConfigService* configService,
        std::string appName,
        std::string appVersion) : Object(__FILE__) {

        this->entityToModuleStateStats = entityToModuleStateStats;
        this->mySqlMetricService = mySqlMetricService;
        this->appName = appName;
        this->appVersion = appVersion;
        this->hostName = NetworkUtil::getHostName();
        dequeueThreadIsRunning = std::make_shared<gicapods::AtomicBoolean>();
        stopConsumingThread = std::make_shared<gicapods::AtomicBoolean>();
        maxSizeOfBatchForInflux = configService->getAsInt("maxSizeOfBatchForInflux");
        session = std::make_shared<PocoSession>(
                "EntityPersistenceService",
                configService->getAsBooleanFromString("influxdbCallKeepAliveMode"),
                configService->getAsInt("influxdbKeepAliveTimeoutInMillis"),
                configService->getAsInt("influxdbCallTimeoutInMillis"),
                configService->get("influxdbHostUrl"),
                entityToModuleStateStats);
}

EntityToModuleStateStatsPersistenceService::~EntityToModuleStateStatsPersistenceService() {
        while(dequeueThreadIsRunning->getValue() ==  true)  {
                //try stopping the thread before exiting
                stopConsumingThread->setValue(true);
                gicapods::Util::sleepViaBoost(_L_, 2);  //until consumer thread goes out
        };
}

void EntityToModuleStateStatsPersistenceService::replaceIllegalCharacters(std::string& candidate) {
        candidate.erase(std::remove(candidate.begin(), candidate.end(), '\t'), candidate.end());
        candidate.erase(std::remove(candidate.begin(), candidate.end(), '\n'), candidate.end());

        // std::vector<std::string> badCharacters = {"'", ",", ")", "(", ":", ";", "_"," "};
        //https://docs.influxdata.com/influxdb/v1.3/write_protocols/line_protocol_tutorial/#special-characters-and-keywords
        //more relaxed
        std::vector<std::string> badCharacters = {"'", ",", " ", "=", "\"", "-"};
        for (auto badCharacter : badCharacters) {
                candidate = StringUtil::replaceStringCaseInsensitive(candidate, badCharacter, "");
        }
}

std::shared_ptr<MetricDbRecord> EntityToModuleStateStatsPersistenceService::buildMetricDbRecord(std::string entity,
                                                                                                std::string module,
                                                                                                std::string state,
                                                                                                std::shared_ptr<gicapods::AtomicDouble> stateValue,
                                                                                                std::string metricLevel) {
        replaceIllegalCharacters(entity);
        replaceIllegalCharacters(module);
        replaceIllegalCharacters(state);
        replaceIllegalCharacters(metricLevel);
        replaceIllegalCharacters(hostName);
        replaceIllegalCharacters(appName);
        replaceIllegalCharacters(appVersion);

        if (entity.empty()) {
                EXIT(" enitty is empty for module " +  module  +  ", state : " +state);
        }
        if (module.empty()) {
                EXIT(" module is empty for entity " +  entity  +  ", state : " +state);
        }
        if (state.empty()) {
                EXIT(" state is empty for entity " +  entity  +  ", module : " +module);
        }

        assertAndThrow(!metricLevel.empty());
        assertAndThrow(!hostName.empty());
        assertAndThrow(!appName.empty());
        assertAndThrow(!appVersion.empty());

        //TODO shorten this back to 100 this after a few months
        if (entity.size() >= 100) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "EntityTooLong_checkThesLogs",
                        "EntityToModuleStateStatsPersistenceService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                LOG(ERROR)<<"entity is too long : " <<  entity;
                return nullptr;
        }
        if (module.size() >= 100) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "moduleTooLong_checkThesLogs",
                        "EntityToModuleStateStatsPersistenceService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                LOG(ERROR)<<"module is too long : " <<  module;
                return nullptr;
        }

        //TODO shorten this back to 150 this after a few months
        if (state.size() >= 150) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "stateTooLong_checkThesLogs",
                        "EntityToModuleStateStatsPersistenceService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                LOG(ERROR)<<"state is too long : " <<  state;
                return nullptr;
        }
        if (stateValue->getValue() > 500000) {
                LOG(ERROR) << "module : " << module
                           << ", state : "<< state
                           << ", entity : "<< entity
                           <<", value is high : "<< stateValue->getValue();
        }
        assertAndThrow(metricLevel.size() < 40);
        assertAndThrow(hostName.size() < 40);
        assertAndThrow(appName.size() < 40);
        assertAndThrow(appVersion.size() < 40);

        std::shared_ptr<MetricDbRecord> metric = std::make_shared<MetricDbRecord>();
        metric->appName =  appName;
        metric->appVersion = appVersion;
        metric->hostName = hostName;
        metric->module= module;
        metric->state= state;
        metric->entity= entity;
        metric->level = metricLevel;
        Poco::DateTime now;
        metric->timeOfRecord = now;
        metric->createdAt = now;
        metric->updatedAt = now;

        metric->value = stateValue->getValue();
        metric->domain = module;

        return metric;
}

std::string EntityToModuleStateStatsPersistenceService::toInfluxRecord(
        std::shared_ptr<MetricDbRecord> record,
        std::string measurementName) {

        std::string recordStr =
                measurementName +
                ","+"hostName="+record->hostName
                +","+"appName="+ record->appName
                +","+"appVersion="+record->appVersion
                +","+"domain="+record->domain
                +","+"state="+record->state
                +","+"entity="+record->entity
                +","+"level="+record->level
                +" "+//this space separates the tags from fields
                "timeOfRecord=\""+DateTimeUtil::dateTimeToStr(record->timeOfRecord)+"\""
                +","+"created_at_utc_millis="+_toStr(DateTimeUtil::getNowInMilliSecond())
                +","+"count="+_toStr(record->value);

        return recordStr;
}

void EntityToModuleStateStatsPersistenceService::recordMetricsInInfluxDb(
        std::vector<std::shared_ptr<MetricDbRecord> > allMetrics) {
        std::string allInfluxdbRecords;
        std::string influxHost = "http://10.136.46.157";
        std::string url = influxHost + ":8086/write?db=app_metrics";

        for(int i=0; i < allMetrics.size(); i++) {
                allInfluxdbRecords += toInfluxRecord(allMetrics.at(i), allMetrics.at(i)->module) + "\n";

                if (StringUtil::equalsIgnoreCase(allMetrics.at(i)->level, EntityToModuleStateStats::exception)) {
                        //creating a ExceptionService metric for reporting here
                        allInfluxdbRecords += toInfluxRecord(allMetrics.at(i), "ExceptionService") + "\n";

                } else if (StringUtil::equalsIgnoreCase(allMetrics.at(i)->level, EntityToModuleStateStats::important)) {
                        //creating a ImportantMetricService metric for reporting here
                        allInfluxdbRecords += toInfluxRecord(allMetrics.at(i),  "ImportantMetricService") + "\n";
                }
        }

        MLOG(20)<<"allInfluxdbRecords :\n"<< allInfluxdbRecords;

        auto request = PocoHttpRequest::createSimplePostRequest(
                url,
                allInfluxdbRecords);
        for (int i=0; i< 5; i++) {
                auto response = session->sendRequest(
                        *request);

                if (response->statusCode != 204) {
                        LOG(INFO)<<" request body failed"<<std::endl<<"startOfRequest_"<<request->body<<"_endOfRequest";
                        LOG(ERROR)<<"response->statusCode " << response->statusCode <<
                                "request with error : "<<
                                allInfluxdbRecords << std::endl;
                        entityToModuleStateStats->addStateModuleForEntity("ERROR_WRITING_DATA_INFLUX_"+
                                                                          _toStr(response->statusCode),
                                                                          "EntityToModuleStateStatsPersistenceService",
                                                                          EntityToModuleStateStats::all,
                                                                          EntityToModuleStateStats::exception);
                        if (i == 4) {
                                //this was last time ...exiting now
                                EXIT("exiting because of error in saving metrics");
                        }
                        //sleeping before next try
                        gicapods::Util::sleepViaBoost(_L_, 3);
                } else {
                        //response is Ok, breaking out of loop
                        break;
                }


        }

}

void EntityToModuleStateStatsPersistenceService::persistStats() {
        persistStats(entityToModuleStateStats);
}

void EntityToModuleStateStatsPersistenceService::persistStats(EntityToModuleStateStats* entityToModuleStateStats) {

        try {

                auto allStates = entityToModuleStateStats->getAllPossibleStates();
                auto states = JsonArrayUtil::convertListToJson(allStates);
                MLOG(20)<<"all possible states : "<<states;

                std::vector<std::shared_ptr<MetricDbRecord> > allMetrics = getMetricsFromStates(entityToModuleStateStats);
                LOG(INFO) << "recording " << allMetrics.size() << " metrics";
                mySqlMetricService->insert(allMetrics);


                if (!allMetrics.empty()) {
                        int totalEventsInBatches = 0;
                        for (int totalIndex=0; totalIndex < allMetrics.size(); ) {

                                std::vector<std::shared_ptr<MetricDbRecord> > batch;
                                while (batch.size() < maxSizeOfBatchForInflux && totalIndex < allMetrics.size()) {

                                        if (totalIndex < allMetrics.size()) {
                                                batch.push_back(allMetrics.at(totalIndex));
                                        }
                                        totalIndex++;
                                }

                                MLOG(4) << "recording " << batch.size() << " sized batch of metrics in influxdb";
                                totalEventsInBatches += batch.size();
                                recordMetricsInInfluxDb(batch);
                        }
                        LOG_EVERY_N(INFO, 1) << "recording " << totalEventsInBatches << " metrics in influxdb";
                }
        } catch (...) {
                gicapods::Util::showStackTrace();
                EXIT("exception in saving metrics");
        }

}

std::vector<std::shared_ptr<MetricDbRecord> > EntityToModuleStateStatsPersistenceService::getMetricsFromStates(
        EntityToModuleStateStats* entityToModuleStateStats) {
        std::vector<std::shared_ptr<MetricDbRecord> > allMetrics;
        int numberOfPermutations = 0;
        auto map = entityToModuleStateStats->entityToFilterStatePercentages->moveMap();
        for (auto&& pair : *map) {
                auto entity = pair.first;
                auto moduleStatePercentages = *(pair.second->moduleToStatePercentage->moveMap());
                for (auto&& modulePair : moduleStatePercentages) {
                        auto module = modulePair.first;
                        auto stateToStateMetric = *(modulePair.second->statesToStateMetrics->moveMap());
                        for (auto&& stateValuePair : stateToStateMetric) {
                                numberOfPermutations++;
                                auto state = stateValuePair.first;
                                std::shared_ptr<StateMetric> stateMetric = stateValuePair.second;
                                if (stateMetric > 0) {
                                        auto metric = buildMetricDbRecord(entity,
                                                                          module,
                                                                          state,
                                                                          stateMetric->value,
                                                                          stateMetric->metricLevel);
                                        if (metric != nullptr && metric->value > 0) {
                                                allMetrics.push_back (metric);

                                                stateMetric->value->setValue(0);
                                        }
                                } else {
                                        //we don't persist an state with value = 0
                                        // the only reason we don't delete this kind of
                                        //state is because tbb concurrent_hash_map has unsafe
                                        //delete operations
                                }


                        }
                }

        }

        MLOG(20)<<" found in "<< numberOfPermutations<< " permutations";
        return allMetrics;
}


void EntityToModuleStateStatsPersistenceService::thread() {

        int numberOfRuns = 0;
        while(stopConsumingThread->getValue() == false) {
                dequeueThreadIsRunning->setValue(true);
                try {

                        isMetricPersistenceInProgress  = true;
                        entityToModuleStateStats->addStateModuleForEntity ("persistStats",
                                                                           "BidderAsyncJobsService",
                                                                           "ALL");
                        persistStats();
                } catch (std::exception const &e) {
                        //TODO : go red and show in dashboard that this is red
                        gicapods::Util::showStackTrace();
                        LOG(ERROR) << "error happening when persistStats  " << boost::diagnostic_information (e);
                }

                isMetricPersistenceInProgress  = false;
                //this has to be 60, because we want have minutely data
                gicapods::Util::sleepViaBoost (_L_, 60);
        }
        dequeueThreadIsRunning->setValue(false);
        isMetricPersistenceInProgress  = false;
}

void EntityToModuleStateStatsPersistenceService::startThread() {
        std::thread persistStatsThread (&EntityToModuleStateStatsPersistenceService::thread, this);
        persistStatsThread.detach ();
}
