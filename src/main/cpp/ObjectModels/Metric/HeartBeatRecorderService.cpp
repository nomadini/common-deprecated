//
// Created by Mahmoud Taabodi on 7/15/16.
//

#include "HeartBeatRecorderService.h"
#include "EntityToModuleStateStats.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "StateToStateMetricMap.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlMetricService.h"
#include "MySqlMetricService.h"
#include "MetricDbRecord.h"
#include "CollectionUtil.h"
#include "MetricDbRecord.h"
#include "NetworkUtil.h"
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include <thread>
#include "MetricDbRecord.h"
#include "StateMetric.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include "ModulesToStatePercentages.h"
#include "ConfigService.h"

HeartBeatRecorderService::HeartBeatRecorderService(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) : Object(__FILE__) {
        dequeueThreadIsRunning = std::make_shared<gicapods::AtomicBoolean>();
        stopConsumingThread = std::make_shared<gicapods::AtomicBoolean>();
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->appName = appName;
        this->appStartedInSecond = DateTimeUtil::getNowInSecond();
}

HeartBeatRecorderService::~HeartBeatRecorderService() {
        while(dequeueThreadIsRunning->getValue() ==  true)  {
                //try stopping the thread before exiting
                stopConsumingThread->setValue(true);
                gicapods::Util::sleepViaBoost(_L_, 2);  //until consumer thread goes out
        };
}


void HeartBeatRecorderService::thread() {

        while(stopConsumingThread->getValue() == false) {
                dequeueThreadIsRunning->setValue(true);


                entityToModuleStateStats->addStateModuleForEntity (appName + "_is_alive",
                                                                   "HeartBeatRecorderService",
                                                                   "ALL");

                long minuteAppIsUp = (DateTimeUtil::getNowInSecond() - this->appStartedInSecond) / 60;
                entityToModuleStateStats->addStateModuleForEntityWithValue (appName + "_Uptime",
                                                                            minuteAppIsUp, //in minutes
                                                                            "HeartBeatRecorderService",
                                                                            "ALL");

                gicapods::Util::sleepViaBoost (_L_, 60);
        }
        dequeueThreadIsRunning->setValue(false);
}

void HeartBeatRecorderService::startThread() {
        std::thread threadHandle (&HeartBeatRecorderService::thread, this);
        threadHandle.detach ();
}
