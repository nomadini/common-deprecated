#ifndef ImportantStats_h
#define ImportantStats_h

class ModulesToStatePercentages;
#include <memory>
#include <string>
#include <vector>
#include "Object.h"
class ImportantStats;

class ImportantStats {

public:

static std::string getName(std::string state);

ImportantStats();

virtual ~ImportantStats();
};


#endif
