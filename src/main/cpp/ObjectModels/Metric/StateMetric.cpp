//
// Created by Mahmoud Taabodi on 3/6/16.
//


#include "AtomicDouble.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "CollectionUtil.h"
#include <tbb/concurrent_hash_map.h>

#include "StateMetric.h"

StateMetric::StateMetric(std::string name, std::string metricLevel) : Object(__FILE__) {
        this->value = std::make_shared<gicapods::AtomicDouble> ();
        this->name = name;
        this->metricLevel = metricLevel;
}

StateMetric::~StateMetric() {

}
