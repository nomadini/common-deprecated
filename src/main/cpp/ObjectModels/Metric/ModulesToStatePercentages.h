//
// Created by Mahmoud Taabodi on 3/6/16.
//
#ifndef ModulesToStatePercentages_H
#define ModulesToStatePercentages_H

#include <tbb/concurrent_hash_map.h>
#include <memory>
#include "AtomicDouble.h"
#include "ConcurrentHashMap.h"
#include "Object.h"
class DateTimeUtil;
class GUtil;

class StateToStateMetricMap;

class ModulesToStatePercentages;

class ModulesToStatePercentages : public Object {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, StateToStateMetricMap > > moduleToStatePercentage;
ModulesToStatePercentages();

double getOneMinuteStatePercForModule(std::string state, std::string module);

/**
 * values
 */

double getValueOfStateModule(std::string state, std::string module);
bool hasStateModule(std::string state, std::string module);

std::vector<std::string> getAllPossibleStatesForModule(std::string module);

std::vector<std::string> getAllPossibleStates();

std::vector<std::string> getAllModulesInLastOneMintue();

std::vector<std::string> getAllStatesForModule(std::string module);


};

#endif
