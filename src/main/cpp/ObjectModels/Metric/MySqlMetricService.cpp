//
// Created by User on 6/12/2016.
//

#include "MySqlMetricService.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include "AtomicDouble.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "CollectionUtil.h"
#include "MySqlDriver.h"
#include "ResultSetHolder.h"
#include "MetricDbRecord.h"

std::string MySqlMetricService::coreFields =
        " id,            "
        " hostname,"
        " appname,"
        " appInstanceUniqueName,"
        " value,"
        "module,"
        "state,"
        "entity,"
        "level,"
        " timeOfRecord, "
        " created_at, "
        " updated_at ";

MySqlMetricService::MySqlMetricService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<MetricDbRecord>> MySqlMetricService::readAllMetricsInLastNMinutesWithLevel(
        int lastNminute,
        std::string hostname,
        std::string appName,
        std::string version,
        std::string entity,
        std::string state,
        std::string module,
        std::string metricLevel) {
        std::vector<std::shared_ptr<MetricDbRecord>> allMetrics;

        std::string query = "SELECT "
                            " hostname,"
                            " appname,"
                            " appInstanceUniqueName,"
                            " module,"
                            " state,"
                            " entity,"
                            " sum(value) AS value "
                            " FROM metric where "
                            " (LOWER( state ) like  '%__metricLevel__%'  "
                            " OR LOWER( level ) like  '%__metricLevel__%') "
                            " and hostname like '%__hostname__%' "
                            " and appname like '%__appname__%' "
                            " and state like '%__state__%' "
                            " and entity like '%__entity__%' "
                            " and module like '%__module__%' "
                            " and appInstanceUniqueName like '%__appInstanceUniqueName__%' "
                            " and timeOfRecord >= '__timeOfRecord__' "
                            " group by module , state, entity , appname, appInstanceUniqueName, hostname having value > 0";

        query = StringUtil::replaceString (query, "__metricLevel__", metricLevel);
        query = StringUtil::replaceString (query, "__hostname__", hostname);
        query = StringUtil::replaceString (query, "__appname__", appName);
        query = StringUtil::replaceString (query, "__appInstanceUniqueName__", version);
        query = StringUtil::replaceString (query, "__module__", module);
        query = StringUtil::replaceString (query, "__state__", state);
        query = StringUtil::replaceString (query, "__entity__", entity);

        query = StringUtil::replaceString (query, "__timeOfRecord__",
                                           DateTimeUtil::
                                           getNowPlusSecondsInMySqlFormat(lastNminute * -60));


        MLOG(3)<< "query is : "<<query;
        auto res = driver->executeQuery (query);

        while (res->next ()) {
                auto metricDbRecord = mapResultSetToObject(res);
                MLOG(4) << "reading Metric from db : " << metricDbRecord->toJson ();
                allMetrics.push_back(metricDbRecord);
        }


        return allMetrics;

}

std::vector<std::shared_ptr<MetricDbRecord>>
MySqlMetricService::readMetricByModuleAndHostInLastNMinutes(
        std::string module,
        std::string host,
        int lastNminute) {
        std::vector<std::shared_ptr<MetricDbRecord>> allMetrics;

        std::string query = "SELECT "
                            " sum(value) AS value,"
                            " module,"
                            " state,"
                            " entity,"
                            " MAX(timeOfRecord) as timeOfRecord, "
                            " hostname "
                            " FROM metric where hostname like  '%__hostname__%' and "
                            " module like '%__module__%' "
                            " and timeOfRecord >= '__timeOfRecord__' "
                            " group by module , state, entity, hostname ";
        query = StringUtil::replaceString (query, "__hostname__", host);
        query = StringUtil::replaceString (query, "__module__", module);
        query = StringUtil::replaceString (query, "__timeOfRecord__",
                                           DateTimeUtil::getNowPlusSecondsInMySqlFormat(lastNminute * -60));

        MLOG(3)<< "query is : "<<query;
        auto res = driver->executeQuery (query);


        while (res->next ()) {
                auto metricDbRecord = mapResultSetToObject(res);
                MLOG(4) << "reading Metric from db : " << metricDbRecord->toJson ();
                allMetrics.push_back(metricDbRecord);
        }


        return allMetrics;
}


std::vector<std::shared_ptr<MetricDbRecord>> MySqlMetricService::readMetricsInLastNMinutes(
        int lastNminute,
        std::string hostname,
        std::string appName,
        std::string version,
        std::string level) {

        std::vector<std::shared_ptr<MetricDbRecord>> allMetrics;

        std::string query = "SELECT "
                            " hostname,"
                            " appname,"
                            " appInstanceUniqueName,"
                            " sum(value) AS value,"
                            " module,"
                            " state,"
                            " entity,"
                            " level,"
                            " MAX(timeOfRecord) as timeOfRecord "
                            " FROM metric where hostname like  '%__hostname__%' "
                            " and appname like '%__appname__%' and "
                            " appInstanceUniqueName like '%__version__%' and " //TODO :
                            " level like '%__level__%' "
                            " and timeOfRecord >= '__timeOfRecord__' "
                            " group by module , state, entity, hostname, appname, level, appInstanceUniqueName ";
        query = StringUtil::replaceString (query, "__hostname__", hostname);
        query = StringUtil::replaceString (query, "__appname__", appName);
        query = StringUtil::replaceString (query, "__version__", version);
        query = StringUtil::replaceString (query, "__level__", level);
        query = StringUtil::replaceString (query, "__timeOfRecord__",
                                           DateTimeUtil::getNowPlusSecondsInMySqlFormat(lastNminute * -60));

        MLOG(10)<< "query is : "<<query;
        auto res = driver->executeQuery (query);


        while (res->next ()) {
                auto metricDbRecord = mapResultSetToObject(res);
                MLOG(4) << "reading Metric from db : " << metricDbRecord->toJson ();
                allMetrics.push_back(metricDbRecord);
        }


        return allMetrics;
}

std::shared_ptr<MetricDbRecord> MySqlMetricService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<MetricDbRecord> metricDbRecord = std::make_shared<MetricDbRecord> ();
        //we put a lot of try catch here, some of the queries don't have all the fields
        try { metricDbRecord->id = res->getInt ("id"); } catch(...) {}
        try { metricDbRecord->hostName = MySqlDriver::getString( res, "hostname"); } catch(...) {}
        try { metricDbRecord->appName = MySqlDriver::getString( res, "appname"); } catch(...) {}
        try { metricDbRecord->appVersion = MySqlDriver::getString( res, "appInstanceUniqueName"); } catch(...) {}
        try { metricDbRecord->value = res->getDouble ("value"); } catch(...) {}

        try { metricDbRecord->module = MySqlDriver::getString( res, "module"); } catch(...) {}
        try { metricDbRecord->state = MySqlDriver::getString( res, "state"); } catch(...) {}
        try { metricDbRecord->entity = MySqlDriver::getString( res, "entity"); } catch(...) {}

        try { metricDbRecord->level = MySqlDriver::getString( res, "level"); } catch(...) {}
        try { metricDbRecord->timeOfRecord = MySqlDriver::parseDateTime(res, "timeOfRecord"); } catch(...) {}
        try { metricDbRecord->createdAt = MySqlDriver::parseDateTime(res, "created_at"); } catch(...) {}
        try { metricDbRecord->updatedAt = MySqlDriver::parseDateTime(res, "updated_at"); } catch(...) {}

        MLOG(10) << "reading metricDbRecord from mysql " << metricDbRecord->toJson ();
        return metricDbRecord;
}


std::shared_ptr<MetricDbRecord> MySqlMetricService::readMetricByStateModuleEntity(
        std::string state,
        std::string module,
        std::string entity) {

        std::shared_ptr<MetricDbRecord> metricDbRecord = std::make_shared<MetricDbRecord> ();

        std::string query = "SELECT "
                            + coreFields+
                            " FROM metric where state='__state__' and module = '__module__' and entity ='__entity__' ";
        query = StringUtil::replaceString (query, "__state__", state);
        query = StringUtil::replaceString (query, "__module__", module);
        query = StringUtil::replaceString (query, "__entity__", entity);


        auto res = driver->executeQuery (query);

        int count = 0;
        while (res->next ()) {
                metricDbRecord = mapResultSetToObject(res);
                MLOG(4) << "reading Metric from db : " << metricDbRecord->toJson ();
                count++;
        }


        return metricDbRecord;
}



/*
   this is a bactch insert operations
 */
void MySqlMetricService::insert(std::vector<std::shared_ptr<MetricDbRecord>> metricDbRecords) {
        if (metricDbRecords.empty()) {
                return;
        }
        insertLimited(metricDbRecords);

        std::size_t const half_size = metricDbRecords.size() / 2;

        // fill
        std::vector<std::shared_ptr<MetricDbRecord>> split_lo(metricDbRecords.begin(), metricDbRecords.begin() + half_size);
        std::vector<std::shared_ptr<MetricDbRecord>> split_hi(metricDbRecords.begin() + half_size, metricDbRecords.end());

        insertLimited(split_lo);
        insertLimited(split_hi);

}

void MySqlMetricService::insertLimited(std::vector<std::shared_ptr<MetricDbRecord>> metricDbRecords) {
        if (metricDbRecords.empty()) {
                return;
        }
        std::string queryStr =
                "INSERT INTO metric"
                " ("
                " hostname,"
                " appname,"
                " appInstanceUniqueName,"
                " value,"
                "module,"
                "state,"
                "entity,"
                "level,"
                "domain,"
                "created_at,"
                "updated_at,"
                " timeOfRecord) VALUES __VALUES_OF_ALLROW__  ;";


        std::vector<std::string> allValues;
        for(auto metricDbRecord :  metricDbRecords) {
                auto oneRecordValues = convertRecordToValueString(metricDbRecord);
                allValues.push_back(oneRecordValues);
                // MLOG(3) << "oneRecordValues : " << oneRecordValues;
        }

        auto properAllValues = StringUtil::joinArrayAsString(allValues, ",");
        // MLOG(3) << "properAllValues : " << properAllValues;
        queryStr = StringUtil::replaceString (queryStr, "__VALUES_OF_ALLROW__", properAllValues);

        driver->executedUpdateStatement (queryStr);

}

std::string MySqlMetricService::convertRecordToValueString(std::shared_ptr<MetricDbRecord> metricDbRecord) {

        std::string oneRowValue =
                " "
                " ("
                " '__hostname__',"
                " '__appname__',"
                " '__appInstanceUniqueName__',"
                " '__value__',"
                " '__module__',"
                " '__state__',"
                " '__entity__',"
                " '__level__',"
                " '__domain__',"
                " '__created_at__',"
                " '__updated_at__',"
                " '__timeOfRecord__'"
                " ) ";


        oneRowValue = StringUtil::replaceString (oneRowValue, "__domain__", metricDbRecord->domain);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__hostname__", metricDbRecord->hostName);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__appname__", metricDbRecord->appName);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__appInstanceUniqueName__", metricDbRecord->appVersion);

        oneRowValue = StringUtil::replaceString (oneRowValue, "__value__", StringUtil::toStr (metricDbRecord->value));


        oneRowValue = StringUtil::replaceString (oneRowValue, "__module__", metricDbRecord->module);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__state__", metricDbRecord->state);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__entity__",metricDbRecord->entity);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__level__", metricDbRecord->level);


        oneRowValue = StringUtil::replaceString (oneRowValue, "__created_at__", DateTimeUtil::dateTimeToStr(metricDbRecord->createdAt));
        oneRowValue = StringUtil::replaceString (oneRowValue, "__updated_at__", DateTimeUtil::dateTimeToStr(metricDbRecord->updatedAt));
        oneRowValue = StringUtil::replaceString (oneRowValue, "__timeOfRecord__", DateTimeUtil::dateTimeToStr(metricDbRecord->timeOfRecord));

        return oneRowValue;
}

void MySqlMetricService::deleteAll() {
        driver->deleteAll("metric");
}

MySqlMetricService::~MySqlMetricService() {

}
