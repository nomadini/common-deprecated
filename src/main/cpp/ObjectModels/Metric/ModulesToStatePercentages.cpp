
#include "ModulesToStatePercentages.h"
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "CollectionUtil.h"
#include "JsonArrayUtil.h"
#include "StateToStateMetricMap.h"

ModulesToStatePercentages::ModulesToStatePercentages() : Object(__FILE__) {
        moduleToStatePercentage = std::make_shared <gicapods::ConcurrentHashMap < std::string, StateToStateMetricMap > > ();
}

double ModulesToStatePercentages::getValueOfStateModule(std::string state, std::string module) {
        auto moduleValue = moduleToStatePercentage->getOptional(module);
        if (moduleValue != nullptr) {
                return moduleValue->getOneMinuteStateValue (state);
        }

        return 0;
        // throw std::logic_error("didn't find this module : " + module);
}

bool ModulesToStatePercentages::hasStateModule(std::string state, std::string module) {
        auto moduleValue = moduleToStatePercentage->getOptional(module);
        if (moduleValue != nullptr) {
                return moduleValue->hasState(state);
        }
        return false;
}

double ModulesToStatePercentages::getOneMinuteStatePercForModule(std::string state, std::string module) {
        auto moduleValue = moduleToStatePercentage->getOptional(module);
        if (moduleValue != nullptr) {
                return moduleValue->getOneMinuteStatePerc (state);
        }

        return 0;
        // throw std::logic_error("didn't find this module : " + module);
}

std::vector<std::string> ModulesToStatePercentages::getAllPossibleStatesForModule(std::string module) {
        auto moduleValue = moduleToStatePercentage->getOptional(module);
        if (moduleValue != nullptr) {
                return moduleValue->getAllPossibleStatesInLastOneMinue ();
        }
        std::vector<std::string> empty;
        return empty;
}

std::vector<std::string> ModulesToStatePercentages::getAllPossibleStates() {
        std::set<std::string> setOfStates;
        auto map = moduleToStatePercentage->getCopyOfMap();
        for (auto iter =
                     map->begin ();
             iter != map->end ();
             ++iter) {
                std::string module = iter->first;
                std::vector<std::string> states = iter->second->getAllPossibleStates ();
                for(std::string oneState :  states) {
                        setOfStates.insert(oneState);
                }
        }
        auto list = CollectionUtil::convertSetToList(setOfStates);
        MLOG(3) << "getAllPossibleStates in json :  " << JsonArrayUtil::convertListToJson(list);

        return list;

}
