#ifndef EntityToModuleStateStats_h
#define EntityToModuleStateStats_h

class ModulesToStatePercentages;
#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "Object.h"
namespace gicapods {
template <class K, class V>
class ConcurrentHashMap;
}
#include "AtomicBoolean.h"
#include "AtomicLong.h"
#include <tbb/concurrent_queue.h>

namespace gicapods {
class ConfigService;
}

class EMSVRecord;
class EntityToModuleStateStats;

class EntityToModuleStateStats {

public:
std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<EMSVRecord> > > stateQueue;
static constexpr const char* important = "IMPORTANT";
static constexpr const char* exception = "EXCEPTION";
/*
   correct is the opposite of exception, it
   indicates that the metric is in good and correct state
 */
static constexpr const char* correct = "CORRECT";
static constexpr const char* error = "ERROR";
static constexpr const char* warning = "WARNING";
static constexpr const char* performance = "PERFORMANCE";
static constexpr const char* debug = "DEBUG";
static constexpr const char* trace = "TRACE";
static constexpr const char* all = "ALL";

std::shared_ptr<gicapods::AtomicBoolean> stateDequeueThreadIsRunning;
std::shared_ptr<gicapods::AtomicBoolean> stopStateConsumingThread;
std::shared_ptr<gicapods::AtomicBoolean> isStateRecordingDisabled;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong > > comboKeyToLastInsertedInSecondMap;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ModulesToStatePercentages > > entityToFilterStatePercentages;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ModulesToStatePercentages > > getEntityToFilterStatePercentages();

static std::shared_ptr<EntityToModuleStateStats> globalEntityToModuleStateStats;

EntityToModuleStateStats();

void consumeFromQueue();
void addStateModuleForEntity(std::string state, std::string module, std::string entityId);

void addStateModuleForEntity(std::string state, std::string module, std::string entityId, std::string metricLevel);

void addStateModuleForEntityWithValue(std::string state, double value, std::string module, std::string entityId, std::string metricLevel);
void addStateModuleForEntityWithValue(std::string state, double value, std::string module, std::string entityId);


/**
 * values AKA counts
 */
double getValueOfStateModuleForEntity(std::string state, std::string module, std::string entityId);

bool hasStateModuleEntity(std::string state, std::string module, std::string entity);

std::vector<std::string> getAllPossibleStatesForModuleAndEntity(std::string module, std::string entityId);

std::vector<std::string> getAllPossibleStatesForModule(std::string module);

std::vector<std::string> getAllPossibleStates();

std::vector<std::string> getAllEntities();

std::vector<std::string> getAllModulesForEntity(std::string entity);
std::vector<std::string> getAllStatesForModule(std::string module);


std::vector<std::string> getAllModulesInLastOneMintue();

std::vector<std::string> getAllModules();

void addStateModuleForEntityPerMinute(
        std::string state, std::string module, std::string entityId);
void addStateModuleForEntityWithValuePerMinute(
        std::string state,
        double value,
        std::string module,
        std::string entityId);

void clearAllMetrics();

void dequeueStates();
virtual ~EntityToModuleStateStats();

static std::shared_ptr<EntityToModuleStateStats> getInstance(gicapods::ConfigService* configService);
};


#endif
