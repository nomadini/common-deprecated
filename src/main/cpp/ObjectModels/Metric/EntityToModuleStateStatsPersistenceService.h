//
// Created by Mahmoud Taabodi on 7/15/16.
//

#ifndef BIDDER_ENTITYTOMODULESTATESTATSPERSISTENCESERVICE_H
#define BIDDER_ENTITYTOMODULESTATESTATSPERSISTENCESERVICE_H



#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <boost/thread.hpp>
class EntityToModuleStateStats;
class MySqlMetricService;
class MetricDbRecord;
#include "AtomicBoolean.h"
#include "PocoSession.h"
#include "AtomicDouble.h"
#include "ConfigService.h"
class EntityToModuleStateStatsPersistenceService;


class EntityToModuleStateStatsPersistenceService : public Object {

public:

MySqlMetricService* mySqlMetricService;
EntityToModuleStateStats* entityToModuleStateStats;
static std::string appName;
static std::string appVersion;
static std::string hostName;
int maxSizeOfBatchForInflux;
bool isMetricPersistenceInProgress;
std::shared_ptr<PocoSession> session;
std::shared_ptr<gicapods::AtomicBoolean> stopConsumingThread;
std::shared_ptr<gicapods::AtomicBoolean> dequeueThreadIsRunning;
EntityToModuleStateStatsPersistenceService(EntityToModuleStateStats* entityToModuleStateStats,
                                           MySqlMetricService* mySqlMetricService,
                                           gicapods::ConfigService* configService,
                                           std::string appName,
                                           std::string appVersion);

void thread();
std::vector<std::shared_ptr<MetricDbRecord> > getMetricsFromStates(EntityToModuleStateStats* entityToModuleStateStats);
void persistStats(EntityToModuleStateStats* entityToModuleStateStats);
void persistStats();
void startThread();

void recordMetricsInInfluxDb(std::vector<std::shared_ptr<MetricDbRecord> > allMetrics);
std::string toInfluxRecord(std::shared_ptr<MetricDbRecord> metric, std::string measurementName);

static void replaceIllegalCharacters(std::string& candidate);
std::shared_ptr<MetricDbRecord> buildMetricDbRecord(std::string entity,
                                                    std::string module,
                                                    std::string state,
                                                    std::shared_ptr<gicapods::AtomicDouble> stateValue,
                                                    std::string metricLevel);
virtual ~EntityToModuleStateStatsPersistenceService();
};

#endif //BIDDER_ENTITYTOMODULESTATESTATSPERSISTENCESERVICE_H
