//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef COMMON_STATEPERCENTAGEStateMetric_H
#define COMMON_STATEPERCENTAGEStateMetric_H

#include "AtomicDouble.h"
class DateTimeUtil;
#include "Object.h"

#include <tbb/concurrent_hash_map.h>

class StateMetric;



/**
   this class holds the state with all the metrics
 */
class StateMetric : public Object {

public:

std::string name;
std::string metricLevel;
std::shared_ptr<gicapods::AtomicDouble> value;


StateMetric(std::string name, std::string metricLevel);

virtual ~StateMetric();

};
#endif //COMMON_STATEPERCENTAGEStateMetric_H
