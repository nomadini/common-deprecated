#include "Device.h"
#include "CassandraDriverInterface.h"
#include "CollectionUtil.h"

#include "GUtil.h"
#include "DeviceFeatureHistory.h"
#include "StringUtil.h"
#include "ActionTakerCassandraService.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"

#include "CassandraDriver.h"
#include "Segment.h"
#include "HttpUtilService.h"
#include "EntityToModuleStateStats.h"

ActionTakerCassandraService::ActionTakerCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats)  : Object(__FILE__) {
        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void ActionTakerCassandraService::saveActionTakersForOffer(std::string offerId,
                                                           std::string deviceId,
                                                           std::string timeOfInsert) {
        MLOG(3)<<"creating an action taker for offerId :  "<<offerId<<" , deviceId :"<<deviceId;

        CassError rc = CASS_OK;
        std::string queryStr =
                "UPDATE  gicapods_mdl.actiontakers SET timeipuamap [TIME_VARIABLE]= '__DEVICE_ID__' where offerid='OFFER_ID_VARIABLE';";

        queryStr = StringUtil::replaceString(queryStr, "TIME_VARIABLE",
                                             StringUtil::toStr(timeOfInsert));

        queryStr = StringUtil::replaceString(queryStr,
                                             "__DEVICE_ID__", deviceId);

        queryStr = StringUtil::replaceString(queryStr,
                                             "OFFER_ID_VARIABLE", offerId);

        MLOG(3)<<"query to create action taker data :  "<<queryStr;

        const char* query = queryStr.c_str();
        //use the generic cassandra services for this
        // cassandraDriver->execute_query(query);
}

std::vector<std::shared_ptr<Device> > ActionTakerCassandraService::readActionTakerDataFrom(
        const std::string& offerId) {
        MLOG(3)<<"reading action taker data for offer : "<<offerId;

        std::vector<std::shared_ptr<Device> > allDevices;

        CassError rc = CASS_OK;
        CassStatement* statement = NULL;
        CassFuture* future = NULL;

        std::string queryStr = StringUtil::toStr(
                "SELECT timeipuamap FROM examples.actiontakers WHERE offerid = '")
                               + (offerId) + StringUtil::toStr("';");

        const char* query = queryStr.c_str();

        statement = cass_statement_new(query, 0);

        future = cass_session_execute(cassandraDriver->getSession(), statement);
        cass_future_wait(future);

        rc = cass_future_error_code(future);
        if (rc != CASS_OK) {
                print_error(future, queryStr, entityToModuleStateStats);
        } else {
                const CassResult* result = cass_future_get_result(future);

                if (cass_result_row_count(result) > 0) {
                        const CassRow* row = cass_result_first_row(result);

                        CassIterator* iterator = cass_iterator_from_map(
                                cass_row_get_column(row, 0));

                        while (cass_iterator_next(iterator)) {

                                cass_int64_t key;
                                const char* value;
                                size_t value_length;
                                cass_value_get_int64(cass_iterator_get_map_key(iterator), &key);
                                cass_value_get_string(cass_iterator_get_map_value(iterator),
                                                      &value, &value_length);

                                std::string deviceId(value, value_length);
                                allDevices.push_back(std::make_shared<Device>(deviceId, ""));

                        }

                        cass_iterator_free(iterator);
                }

                cass_result_free(result);
        }

        cass_future_free(future);
        cass_statement_free(statement);
        MLOG(3)<<" "<<allDevices.size()<<" devices was read as action takers of offer :"<<offerId;

        return allDevices;
}
