#include "ObjectVectorHolder.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"

#include "GUtil.h"
#include <string>
#include <memory>
template <class T>
ObjectVectorHolder<T>::ObjectVectorHolder() : Object(__FILE__) {
        values = std::make_shared<std::vector<std::shared_ptr<T> > > ();
}

template <class T>
ObjectVectorHolder<T>::~ObjectVectorHolder() {

}

#include "Segment.h"
#include "GeoLocation.h"
#include "TargetGroup.h"
#include "RecencyModel.h"
#include "Tag.h"
template class ObjectVectorHolder<Segment>;
template class ObjectVectorHolder<GeoLocation>;
template class ObjectVectorHolder<TargetGroup>;
template class ObjectVectorHolder<RecencyModel>;
template class ObjectVectorHolder<Tag>;
