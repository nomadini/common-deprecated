#include "CassandraDriverInterface.h"
#include "AdEntry.h"
#include "AdHistory.h"
#include "DateTimeUtil.h"
#include "AdHistoryCassandraService.h"
#include "ConfigService.h"
AdHistoryCassandraService::AdHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService) :
        CassandraService(cassandraDriver,
                         entityToModuleStateStats,

                         asyncThreadPoolService,
                         configService), Object("AdHistoryCassandraService") {
        assertAndThrow(cassandraDriver != NULL);
        assertAndThrow(httpUtilService != NULL);
        assertAndThrow(entityToModuleStateStats != NULL);

        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}


void AdHistoryCassandraService::saveAdInteraction(std::shared_ptr<Device> device,
                                                  std::shared_ptr<AdEntry> adEntry) {
        auto adHistory = std::make_shared<AdHistory>(device);
        adHistory->listOfAdEntries.push_back(adEntry);
        pushToWriteBatchQueue(adHistory);
}

void AdHistoryCassandraService::deleteAll() {
        cassandraDriver->deleteAll ("adhistory");
}

AdHistoryCassandraService::~AdHistoryCassandraService() {

}
