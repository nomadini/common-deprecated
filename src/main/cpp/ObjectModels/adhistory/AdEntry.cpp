


#include "GUtil.h"
#include "AdEntry.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include "EventLog.h"
#include "DateTimeUtil.h"
#include <string>
#include <memory>

AdEntry::AdEntry(std::string transactionId, std::string adInteractionType)  : Object("AdEntry") {
								targetGroupId = 0;
								creativeId = 0;
								publisherId = 0;
								segmentId = 0;
								timeAdShownInMillis = DateTimeUtil::getNowInMilliSecond();
								this->transactionId = transactionId;
								assertAndThrow(!this->transactionId.empty());
								if (!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_IMPRESSION, adInteractionType) &&
												!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_CLICK, adInteractionType) &&
												!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_CONVERSION, adInteractionType)) {
																throwEx("invalid adInteractionType : " + adInteractionType);
								}

								this->adInteractionType = adInteractionType;
}

std::string AdEntry::toString() {
								return this->toJson();
}

std::shared_ptr<AdEntry> AdEntry::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return AdEntry::fromJsonValue(*document);
}

void AdEntry::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "targetGroupId", targetGroupId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "creativeId", creativeId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "adInteractionType", adInteractionType, value);
								JsonUtil::addMemberToValue_FromPair(doc, "publisherId", publisherId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "segmentId", segmentId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "transactionId", transactionId, value);

								long timeAdShownInMillisLong = (long) timeAdShownInMillis;
								JsonUtil::addMemberToValue_FromPair(doc, "timeAdShownInMillis", timeAdShownInMillisLong, value);
}

std::shared_ptr<AdEntry> AdEntry::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<AdEntry> adEntry = std::make_shared<AdEntry>(
																JsonUtil::GetStringSafely (value, "transactionId"),
																JsonUtil::GetStringSafely (value, "adInteractionType")
																);

								adEntry->targetGroupId = JsonUtil::GetIntSafely (value, "targetGroupId");
								adEntry->creativeId = JsonUtil::GetIntSafely (value, "creativeId");
								adEntry->publisherId = JsonUtil::GetIntSafely (value, "publisherId");
								adEntry->segmentId = JsonUtil::GetIntSafely (value, "segmentId");
								adEntry->timeAdShownInMillis  = JsonUtil::GetLongSafely (value, "timeAdShownInMillis");

								return adEntry;
}


std::string AdEntry::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

AdEntry::~AdEntry() {

}
