

#include "AdEntry.h"
#include "Device.h"
#include "GUtil.h"
#include "AdHistory.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

AdHistory::AdHistory(std::shared_ptr<Device> deviceArg) : Object("AdHistory") {
								this->device = deviceArg;

}

AdHistory::~AdHistory() {

}

std::string AdHistory::toString() {
								return this->toJson();
}

void AdHistory::createMapOfTgIdToAdEntries(bool forceCreated) {
								if (!forceCreated) {
																LOG(WARNING)<<"targetGroupIdToAdEntriesMap has already been created";
																return;
								}
								for (auto adEntry : listOfAdEntries) {
																auto ptr = targetGroupIdToAdEntriesMap.find(adEntry->targetGroupId);
																if (ptr != targetGroupIdToAdEntriesMap.end()) {
																								ptr->second.push_back(adEntry);
																} else {
																								std::vector<std::shared_ptr<AdEntry> > adEntries;
																								adEntries.push_back(adEntry);
																								targetGroupIdToAdEntriesMap.insert(
																																std::pair<int, std::vector<std::shared_ptr<AdEntry> > >(
																																								adEntry->targetGroupId, adEntries));
																}
								}
								MLOG(10)<<"created the targetGroupIdToAdEntriesMap , size is :  "<<targetGroupIdToAdEntriesMap.size();
}

std::string AdHistory::getName() {
								return "AdHistory";
}

std::vector<std::shared_ptr<AdEntry> > AdHistory::getAdEntriesByTgId(int targetGroupId) {
								if(targetGroupIdToAdEntriesMap.size() == 0
											&& !listOfAdEntries.empty()) {
																createMapOfTgIdToAdEntries(true);

								}
								auto ptr = targetGroupIdToAdEntriesMap.find(targetGroupId);
								if (ptr != targetGroupIdToAdEntriesMap.end()) {
																return ptr->second;
								}
								std::vector<std::shared_ptr<AdEntry> > empty;
								return empty;
}

std::shared_ptr<AdHistory> AdHistory::fromJsonValue(RapidJsonValueType value) {
								auto device = std::make_shared<Device> (
																JsonUtil::GetStringSafely(value, "deviceId"),
																JsonUtil::GetStringSafely(value, "deviceType")
																);
								std::shared_ptr<AdHistory> adHistory = std::make_shared<AdHistory>(device);

								JsonArrayUtil::getArrayFromValueMemeber(
																value,
																"adentries",
																adHistory->listOfAdEntries);

								adHistory->createMapOfTgIdToAdEntries(true);

								return adHistory;

}
std::shared_ptr<AdHistory> AdHistory::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

std::string AdHistory::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

								/*
								 * adhistory object looks like this in db
								 * { "deviceId":"1234",
								 *    "deviceType:"desktop",
								 *     "adentries" : [
								 *      {  },
								 *      {  },
								 *
								 *     ]
								 *
								 *  }
								 *
								 **/

								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

void AdHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								JsonUtil::addMemberToValue_FromPair(doc, "deviceId", device->getDeviceId(), value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceType", device->getDeviceType(), value);
								JsonArrayUtil::addMemberToValue_FromArrayOfObjects(doc, value, "adentries", listOfAdEntries);
}
