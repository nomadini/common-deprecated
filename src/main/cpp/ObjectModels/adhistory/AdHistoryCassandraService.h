/*
 * AdHistoryCassandraService.h
 *
 *  Created on: Jul 18, 2015
 *      Author: mtaabodi
 */

#ifndef AdHistoryCassandraService_H_
#define AdHistoryCassandraService_H_

class CassandraDriverInterface;
#include "AdEntry.h"
#include "AdHistory.h"
#include "Object.h"
class DateTimeUtil;


namespace gicapods { class ConfigService; }
#include "HttpUtilService.h"
#include "CassandraService.h"
class EntityToModuleStateStats;
class Device;


class AsyncThreadPoolService;
class AdHistoryCassandraService;

#include "CassandraService.h"



class AdHistoryCassandraService : public CassandraService<AdHistory>, public Object {

private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;
public:


AdHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

virtual ~AdHistoryCassandraService();

void saveAdInteraction(std::shared_ptr<Device> device, std::shared_ptr<AdEntry> adEntry);

virtual void deleteAll();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_CASSANDRA_ADHISTORYCASSANDRASERVICE_H_ */
