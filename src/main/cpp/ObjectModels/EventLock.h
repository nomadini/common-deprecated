#ifndef EventLock_H
#define EventLock_H


#include "Status.h"
#include "JsonUtil.h"

#include <memory>
#include <string>
class EventLock {

private:

public:

std::string eventType;
std::string eventId;

EventLock(
        std::string eventId,
        std::string eventType);

virtual ~EventLock();

std::string toJson();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

static std::shared_ptr<EventLock> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<EventLock> fromJson(std::string json);
};




#endif
