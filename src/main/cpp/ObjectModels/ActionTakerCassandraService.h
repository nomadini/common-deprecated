/*
 * ActionTakerCassandraService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef ActionTakerCassandraService_H_
#define ActionTakerCassandraService_H_

class Device;
class CassandraDriverInterface;


class DeviceFeatureHistory;
#include "HttpUtilService.h"
class EntityToModuleStateStats;

class ActionTakerCassandraService;
#include "Object.h"


class ActionTakerCassandraService : public Object {

private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;
public:

ActionTakerCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats);

virtual std::vector <std::shared_ptr<Device> > readActionTakerDataFrom(const std::string &offerId);

virtual void saveActionTakersForOffer(std::string offerId, std::string deviceId, std::string timeOfInsert);

};

#endif
