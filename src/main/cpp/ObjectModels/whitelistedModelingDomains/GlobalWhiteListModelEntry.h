#ifndef GlobalWhiteListModelEntry_H
#define GlobalWhiteListModelEntry_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "JsonTypeDefs.h"

class GlobalWhiteListModelEntry;
#include "Object.h"


//this class determines if a domain is eligible for sampling for modeling or not
class GlobalWhiteListModelEntry : public Object {

private:

public:
int id;     //used in CacheService
std::string domainName;
std::string domainType;
std::string status;
std::string createdAt;
std::string updatedAt;

void setId(int id);


void setDomainName(std::string domainName);

void setCreatedAt(std::string createdAt);

void setUpdatedAt(std::string updatedAt);

int getId();

std::string getDomainName();

std::string getCreatedAt();

std::string getUpdatedAt();

GlobalWhiteListModelEntry();

std::string toJson();

virtual ~GlobalWhiteListModelEntry();

static std::string getEntityName();
static std::shared_ptr<GlobalWhiteListModelEntry> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
