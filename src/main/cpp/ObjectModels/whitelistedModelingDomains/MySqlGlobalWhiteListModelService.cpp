#include "GUtil.h"

#include "MySqlGlobalWhiteListModelService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"

MySqlGlobalWhiteListModelService::MySqlGlobalWhiteListModelService(MySqlDriver* driver) : Object(__FILE__) {
        assertAndThrow(driver != NULL);
        this->driver = driver;
}

std::vector<std::shared_ptr<GlobalWhiteListModelEntry>> MySqlGlobalWhiteListModelService::readAllEntities() {
        std::vector<std::shared_ptr<GlobalWhiteListModelEntry>> allEntities;
        std::string query = " SELECT "
                            " id, "
                            " domain_name,"
                            " domain_type,"
                            " status,"
                            " created_at,"
                            " updated_at"
                            " FROM global_whitlisted_domains";


        auto res = driver->executeQuery (query);

        bool nextResult = res->next ();

        while (res->next ()) {
                std::shared_ptr<GlobalWhiteListModelEntry> entry =
                        std::make_shared<GlobalWhiteListModelEntry>();
                entry->setId(res->getInt("id"));
                entry->setDomainName(res->getString("domain_name"));
                entry->domainType = MySqlDriver::getString( res, "domain_type");
                entry->status = MySqlDriver::getString( res, "status");
                entry->setCreatedAt(res->getString("created_at"));
                entry->setUpdatedAt(res->getString("updated_at"));
                allEntities.push_back(entry);
        }


        return allEntities;
}

void MySqlGlobalWhiteListModelService::deleteAll() {
        driver->deleteAll("global_whitlisted_domains");
}

void MySqlGlobalWhiteListModelService::insert(std::shared_ptr<GlobalWhiteListModelEntry> entry) {
        std::string queryStr =
                " INSERT INTO `global_whitlisted_domains`"
                " ("
                " domain_name,"
                " domain_type,"
                " status,"
                " `created_at`,"
                " `updated_at`)"
                " VALUES"
                " ( "
                " '__DOMAIN_NAME__',"
                " '__created_at__',"
                " '__updated_at__');";
        MLOG(3) << "inserting new model entry in db : " << entry->toJson();



        queryStr = StringUtil::replaceString (queryStr, "__domain_name__",
                                              entry->domainName);
        queryStr = StringUtil::replaceString (queryStr, "__domain_type__",
                                              entry->domainType);
        queryStr = StringUtil::replaceString (queryStr, "__status__",
                                              entry->status);
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);

        driver->executedUpdateStatement (queryStr);
        entry->id = driver->getLastInsertedId ();
}

MySqlGlobalWhiteListModelService::~MySqlGlobalWhiteListModelService() {
}
