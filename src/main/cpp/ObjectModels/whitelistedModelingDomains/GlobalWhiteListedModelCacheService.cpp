#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "GlobalWhiteListedModelCacheService.h"
#include "MySqlGlobalWhiteListModelService.h"
#include "JsonArrayUtil.h"
#include "BooleanObject.h"

GlobalWhiteListedModelCacheService::GlobalWhiteListedModelCacheService(
        MySqlGlobalWhiteListModelService* mySqlGlobalWhiteListService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) :
        CacheService<GlobalWhiteListModelEntry>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {
        this->allWhiteListedDomains = std::make_shared<gicapods::ConcurrentHashMap<std::string, BooleanObject> > ();
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void GlobalWhiteListedModelCacheService::clearOtherCaches() {
        allWhiteListedDomains->clear ();
}

void GlobalWhiteListedModelCacheService::populateOtherMapsAndLists(
        std::vector<std::shared_ptr<GlobalWhiteListModelEntry> > allEntities) {
        for(std::shared_ptr<GlobalWhiteListModelEntry> entry :  allEntities) {
                auto boolean = std::make_shared<BooleanObject>();
                allWhiteListedDomains->put(StringUtil::toLowerCase(entry->getDomainName()), boolean);
        }
}

GlobalWhiteListedModelCacheService::~GlobalWhiteListedModelCacheService() {

}
