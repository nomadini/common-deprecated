
#ifndef MySqlGlobalWhiteListModelService_H
#define MySqlGlobalWhiteListModelService_H



#include "MySqlDriver.h"
#include <cppconn/resultset.h>
class DateTimeUtil;

#include <tbb/concurrent_hash_map.h>
#include <memory>
#include "DataProvider.h"
#include "GlobalWhiteListModelEntry.h"
class MySqlGlobalWhiteListModelService;
#include "Object.h"


class MySqlGlobalWhiteListModelService : public DataProvider<GlobalWhiteListModelEntry>, public Object{

public:
MySqlDriver* driver;

MySqlGlobalWhiteListModelService(MySqlDriver* driver);

void deleteAll();

void insert(std::shared_ptr<GlobalWhiteListModelEntry> entry);

virtual ~MySqlGlobalWhiteListModelService();

std::vector<std::shared_ptr<GlobalWhiteListModelEntry>> readAllEntities();
};

#endif
