
#ifndef GlobalWhiteListedModelCacheService_H
#define GlobalWhiteListedModelCacheService_H


class StringUtil;
#include "GlobalWhiteListModelEntry.h"
#include <boost/foreach.hpp>
class GlobalWhiteListedModelCacheService;
#include "MySqlGlobalWhiteListModelService.h"

#include "MySqlGlobalWhiteListModelService.h"
class DateTimeUtil;

#include <tbb/concurrent_hash_map.h>
#include <memory>
#include "HttpUtilService.h"
class EntityToModuleStateStats;
class BooleanObject;
#include "EntityProviderService.h"
#include "CacheService.h"
#include "Object.h"
#include "ConcurrentHashMap.h"

class GlobalWhiteListedModelCacheService;



class GlobalWhiteListedModelCacheService : public CacheService<GlobalWhiteListModelEntry>, public Object {

public:
MySqlGlobalWhiteListModelService* mySqlGlobalWhiteListService;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, BooleanObject> > allWhiteListedDomains;
EntityToModuleStateStats* entityToModuleStateStats;

GlobalWhiteListedModelCacheService(MySqlGlobalWhiteListModelService* mySqlGlobalWhiteListService,
                                   HttpUtilService* httpUtilService,
                                   std::string dataMasterUrl,
                                   EntityToModuleStateStats* entityToModuleStateStats,
                                   std::string appName);

virtual ~GlobalWhiteListedModelCacheService();

virtual void clearOtherCaches();

virtual void populateOtherMapsAndLists(std::vector<std::shared_ptr<GlobalWhiteListModelEntry> > allEntities);
};

#endif
