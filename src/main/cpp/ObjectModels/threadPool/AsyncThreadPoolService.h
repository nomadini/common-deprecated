#ifndef AsyncThreadPoolService_H
#define AsyncThreadPoolService_H



#include <memory>
#include <string>
#include "TargetGroupFilterStatistic.h"
class EntityToModuleStateStats;
#include <tbb/concurrent_queue.h>
#include "Poco/ThreadPool.h"
#include "NomadiniTask.h"
#include "AtomicBoolean.h"

class AsyncThreadPoolService;

#include "Object.h"
class AsyncThreadPoolService : public Object {

private:

public:

EntityToModuleStateStats* entityToModuleStateStats;

std::shared_ptr<gicapods::AtomicBoolean> isAllowedToRun;
std::shared_ptr<gicapods::AtomicBoolean> threadIsFinished;
//determines how often we submit tasks in queue to the threadPool
int submitTasksIntervalInSecond;
std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<NomadiniTask> > > queueOfTasks;

//these tasks will be submitted to the thread pool after their initial delay has passed
std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<NomadiniTask> > > queueOfTasksWithDelay;
std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<NomadiniTask> > > queueOfRunningTasks;

std::shared_ptr<Poco::ThreadPool> threadPool;

AsyncThreadPoolService();
void submitTask(std::shared_ptr<NomadiniTask> task);
void submitTaskWithDelay(std::shared_ptr<NomadiniTask> task);
void runTasks();
void submitOneTask(std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<NomadiniTask> > > sourceQueueOfTasks);
void deleteDoneTasks();
void runTasksInSeperateThread();
virtual ~AsyncThreadPoolService();
};

#endif
