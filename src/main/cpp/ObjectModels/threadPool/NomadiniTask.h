/*
 * NomadiniTask.h
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef NomadiniTask_H_
#define NomadiniTask_H_




#include "Poco/NotificationQueue.h"
class EntityToModuleStateStats;
#include "AtomicBoolean.h"
#include "Poco/Runnable.h"

class NomadiniTask;
#include "Object.h"


class NomadiniTask : public Poco::Runnable, public Object {
public:


std::shared_ptr<gicapods::AtomicBoolean> taskIsStarted;
long timeFinishedInMillis;
long timeCreatedInMillis;
EntityToModuleStateStats* entityToModuleStateStats;

NomadiniTask(EntityToModuleStateStats* entityToModuleStateStats);



virtual void run();

//this is the method that child classes have to implement
virtual void runTask() = 0;

virtual bool isReadyToBeSubmitted();

virtual ~NomadiniTask();
private:

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_SVMSGDWORKER_H_ */
