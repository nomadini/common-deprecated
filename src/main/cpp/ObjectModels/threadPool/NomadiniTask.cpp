



#include "GUtil.h"
#include "NomadiniTask.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "EntityToModuleStateStats.h"
#include <string>
#include <memory>
#include <boost/exception/all.hpp>

NomadiniTask::NomadiniTask(EntityToModuleStateStats* entityToModuleStateStats)
        : Object(__FILE__) {

        this->taskIsStarted  = std::make_shared<gicapods::AtomicBoolean>(false);
        this->entityToModuleStateStats  = entityToModuleStateStats;
        timeFinishedInMillis = 0;
        timeCreatedInMillis = DateTimeUtil::getNowInMilliSecond();
}

void NomadiniTask::run() {
        this->taskIsStarted->setValue(true);
        try {
                runTask();
        } catch(...) {
                entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION_IN_RUNNING_TASK",
                                                                   "NomadiniTask",
                                                                   "ALL");
                gicapods::Util::showStackTrace();

        }
        this->timeFinishedInMillis = DateTimeUtil::getNowInMilliSecond();
}

//determines if a task is eligible for submition to the threadPool
//can be overridden by subclasses
bool NomadiniTask::isReadyToBeSubmitted() {
        return !taskIsStarted->getValue();
}
NomadiniTask::~NomadiniTask() {

}
