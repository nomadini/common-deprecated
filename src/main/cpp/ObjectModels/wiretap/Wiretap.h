#ifndef Wiretap_H
#define Wiretap_H

#include "JsonTypeDefs.h"

#include <set>
#include <unordered_map>
#include <memory>
#include <string>
#include <vector>
class Device;
#include "CassandraManagedType.h";
#include "DateTimeUtil.h"
#include "Object.h"
class Wiretap;




class Wiretap : public CassandraManagedType, public Object {

private:

public:
std::string eventId;
TimeType eventTime;
std::string appName;
std::string appVersion;
std::string appHost;
std::string moduleName;
std::string request;
std::string response;

Wiretap();
std::string toString();
static std::string getName();

static std::shared_ptr<Wiretap> fromJson(std::string jsonString);
static std::shared_ptr<Wiretap> fromJsonValue(RapidJsonValueType value);

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
virtual ~Wiretap();
};

#endif
