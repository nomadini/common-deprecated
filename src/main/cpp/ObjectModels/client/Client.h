#ifndef Client_H
#define Client_H
#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
class Client;


class Client : public Object {

public:
int id;
std::string status;
std::string name;

Poco::DateTime createdAt;
Poco::DateTime updatedAt;
Client();

void validate();

std::string toString();

std::string toJson();

virtual ~Client();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<Client> fromJsonValue(RapidJsonValueType value);
static std::string getEntityName();
};

#endif
