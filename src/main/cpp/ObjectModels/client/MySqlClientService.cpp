#include "MySqlClientService.h"
#include "MySqlDriver.h"

#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "MySqlDriver.h"
#include "Client.h"
MySqlClientService::MySqlClientService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<Client> > MySqlClientService::readAllEntities() {

        auto all =  readAllClients();
        MLOG(3)<<"clients read from db are : "<< all.size();
        return all;
}

std::vector<std::shared_ptr<Client> > MySqlClientService::readAllClients() {


        std::vector<std::shared_ptr<Client> > allObjects;
        std::string query  = "SELECT "
                             " `id`, "

                             " `name`,"
                             " `status`,"
                             " `created_at`,"
                             " `updated_at` "
                             " FROM `client`";

        auto res = driver->executeQuery (query);

        while (res->next ()) {
                std::shared_ptr<Client> obj = std::make_shared<Client> ();
                obj->id = res->getInt ("id");
                obj->name = MySqlDriver::getString( res, "name");
                obj->status = MySqlDriver::getString( res, "status");
                obj->createdAt = MySqlDriver::parseDateTime(res, "created_at");
                obj->updatedAt = MySqlDriver::parseDateTime(res, "updated_at");
                MLOG(3)<<"reading client : " << obj->toJson();
                allObjects.push_back (obj);
        }

        return allObjects;
}


void MySqlClientService::deleteAll() {
        driver->deleteAll("client");
}

void MySqlClientService::insert(std::shared_ptr<Client> client) {
        std::string queryStr;

        queryStr =
                " INSERT INTO `client`"
                " ( "
                " `name`,"
                " `status`,"
                " `created_at`,"
                " `updated_at`)"
                " VALUES"
                " ("
                " '__COMPANY_ID__',"
                " '__NAME__',"
                " '__STATUS__',"
                " '__created_at__',"
                " '__updated_at__');";


        queryStr = StringUtil::replaceString (queryStr, "__NAME__", client->name);
        queryStr = StringUtil::replaceString (queryStr, "__STATUS__", client->status);


        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();
        MLOG(3) << "date to insert in mysql is : " << date;


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        driver->executedUpdateStatement (queryStr);
        client->id = driver->getLastInsertedId ();

}

MySqlClientService::~MySqlClientService() {
}
