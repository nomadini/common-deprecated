
#include "BooleanObject.h"

BooleanObject::BooleanObject() : Object(__FILE__) {
        value = false;
}

BooleanObject::~BooleanObject() {

}

bool BooleanObject::getValue() {
        return value;
}

void BooleanObject::setValue(bool val) {
        this->value = val;
}
