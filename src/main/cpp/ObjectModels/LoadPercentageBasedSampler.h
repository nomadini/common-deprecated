//
// Created by Mahmoud Taabodi on 11/24/15.
//

#ifndef GICAPODS_RANDOMLoadPercentageBasedSampler_H
#define GICAPODS_RANDOMLoadPercentageBasedSampler_H

#include "AdEntry.h"

#include <memory>
#include <string>
#include "AtomicLong.h"
#include "AtomicDouble.h"
class LoadPercentageBasedSampler;
class RandomUtil;
#include "Object.h"



class LoadPercentageBasedSampler : public Object {

private:

int sampleRate;
int overallPercentage;
std::string samplerName;
std::shared_ptr<gicapods::AtomicLong> numberOfTotalCalls;
std::shared_ptr<gicapods::AtomicLong> numberOfFoundInSample;
std::shared_ptr<gicapods::AtomicDouble> actualSamplerRate;
public:
std::shared_ptr<RandomUtil> sampleRandomizer;
LoadPercentageBasedSampler(int actualSamplerRate,
                           std::string samplerName);

bool isPartOfSample();

virtual ~LoadPercentageBasedSampler();

};

#endif //GICAPODS_RANDOMLoadPercentageBasedSampler_H
