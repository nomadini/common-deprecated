#ifndef FeatureToFeatureHistoryUpdaterModule_H
#define FeatureToFeatureHistoryUpdaterModule_H
#include "Object.h"
#include "Feature.h"
#include <memory>
#include <string>

#include <boost/optional.hpp>
class FeatureToFeatureHistory;
class Device;

class FeatureToFeatureHistoryCassandraService;


class FeatureGuardService;
class EntityToModuleStateStats;

class LoadPercentageBasedSampler;
namespace gicapods { template <typename K, typename V> class ConcurrentHashMap; }

class FeatureToFeatureHistoryUpdaterModule : public Object {

private:

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, Feature> > mapOfInterestingFeatures;
std::unique_ptr<LoadPercentageBasedSampler> randomFeatureSampler;
FeatureGuardService* featureGuardService;
FeatureToFeatureHistoryCassandraService* featureToFeatureHistoryCassandraService;
EntityToModuleStateStats* entityToModuleStateStats;


FeatureToFeatureHistoryUpdaterModule(
        FeatureToFeatureHistoryCassandraService* featureToFeatureHistoryCassandraService);

std::string getName();

virtual void process(
        std::string featureKeyCategory,
        std::string featureKeyValue,
        std::string featureCategory,
        std::string featureValue
        );

bool isInterestingFeature(std::string featureValue);

void prepFeatureValueForWrite(
        std::string featureKeyCategory,
        std::string featureKeyValue,
        std::string featureCategory,
        std::string featureValue);

std::shared_ptr<FeatureToFeatureHistory> createEntityForWrite(
        std::string featureKeyCategory,
        std::string featureKeyValue,
        std::string featureCategory,
        std::string featureValue);

virtual ~FeatureToFeatureHistoryUpdaterModule();
};

#endif
