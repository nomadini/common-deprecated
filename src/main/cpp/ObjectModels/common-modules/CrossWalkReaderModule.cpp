#include "CrossWalkReaderModule.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceIdToIpsMap.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"

#include "IpToDeviceIdsMapCassandraService.h"
#include "DeviceIdToIpsMapCassandraService.h"
#include "Device.h"
#include "AeroCacheService.h"
#include "CrossWalkedDeviceMap.h"

CrossWalkReaderModule::CrossWalkReaderModule() : Object(__FILE__) {

}

std::string CrossWalkReaderModule::getName() {
        return "CrossWalkReaderModule";
}

std::vector<std::shared_ptr<Device> >
CrossWalkReaderModule::readNearByDevices(std::shared_ptr<Device> device) {

        auto crossWalkedDeviceMap = std::make_shared<CrossWalkedDeviceMap>();
        crossWalkedDeviceMap->sourceDevice = device;

        auto crossWalkedDeviceMapCachedResult =
                tryFetchResultFromCache(crossWalkedDeviceMap);
        if (crossWalkedDeviceMapCachedResult != nullptr) {
                return crossWalkedDeviceMapCachedResult->linkedDevices;
        }


        auto deviceIdToIpsMapKey = std::make_shared<DeviceIdToIpsMap> ();
        deviceIdToIpsMapKey->device = device;

        auto deviceIdToIpsMap =
                deviceIdToIpsMapCassandraService->readDataOptionalOverHttp(deviceIdToIpsMapKey, 1000);

        std::vector<std::shared_ptr<Device> > allDevices;
        if (deviceIdToIpsMap == nullptr) {
                entityToModuleStateStats->addStateModuleForEntity("DEVICE_ID_NOT_LINKED_TO_ANY_IPS",
                                                                  "CrossWalkReaderModule",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::debug);

                cacheDeviceLinks(crossWalkedDeviceMap, allDevices);
                return allDevices;
        }

        if (deviceIdToIpsMap->ips.size() > 8) {
                entityToModuleStateStats->addStateModuleForEntity("DEVICE_ID_LINKED_TO_TOO_MANY_IPS",
                                                                  "CrossWalkReaderModule",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::debug);

                cacheDeviceLinks(crossWalkedDeviceMap, allDevices);
                return allDevices;
        }

        allDevices = getAllLinkedDevices(deviceIdToIpsMap);
        cacheDeviceLinks(crossWalkedDeviceMap, allDevices);
        return allDevices;
}


void CrossWalkReaderModule::cacheDeviceLinks(
        std::shared_ptr<CrossWalkedDeviceMap> crossWalkedDeviceMap, std::vector<std::shared_ptr<Device> > allDevices) {
        crossWalkedDeviceMap->linkedDevices = allDevices;
        writeKeyValueInCache(crossWalkedDeviceMap);
}


std::vector<std::shared_ptr<Device> > CrossWalkReaderModule::getAllLinkedDevices(std::shared_ptr<DeviceIdToIpsMap> deviceIdToIpsMap) {
        std::vector<std::shared_ptr<Device> > allDevices;

        for (auto ip : deviceIdToIpsMap->ips) {
                auto ipToDeviceIdsMap = std::make_shared<IpToDeviceIdsMap>();
                ipToDeviceIdsMap->ip = ip;
                ipToDeviceIdsMap =
                        ipToDeviceIdsMapCassandraService->readDataOptionalOverHttp(ipToDeviceIdsMap, 1000);
                if (ipToDeviceIdsMap->devices.size() > 8) {
                        entityToModuleStateStats->addStateModuleForEntity("IP_LINKED_TO_TOO_MANY_DEVICE_IDS",
                                                                          "CrossWalkReaderModule",
                                                                          EntityToModuleStateStats::all,
                                                                          EntityToModuleStateStats::debug);
                        continue;
                }

                MLOG(3)<<"found "<<ipToDeviceIdsMap->devices.size() << " neighbor devices for ip : "<< ip;

                for(auto deviceNearByFound : ipToDeviceIdsMap->devices) {
                        allDevices.push_back(deviceNearByFound);
                }
        }

        return allDevices;

}

std::shared_ptr<CrossWalkedDeviceMap> CrossWalkReaderModule::
tryFetchResultFromCache(std::shared_ptr<CrossWalkedDeviceMap> crossWalkedDeviceMap) {
        int cacheResultStatus = 0;
        auto crossWalkedDeviceMapCachedResult =
                crossWalkedDeviceMapAerospikeCacheService->
                readDataOptional(crossWalkedDeviceMap, cacheResultStatus);
        if (cacheResultStatus != AerospikeDriver::CACHE_MISS) {

                entityToModuleStateStats->addStateModuleForEntity("CACHED_VALUE_FOUND",
                                                                  "CrossWalkReaderModule",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::important);
                return crossWalkedDeviceMapCachedResult;

        }

        entityToModuleStateStats->addStateModuleForEntity("CACHED_VALUE_MISSED",
                                                          "CrossWalkReaderModule",
                                                          EntityToModuleStateStats::all,
                                                          EntityToModuleStateStats::important);


        return nullptr;
}

void CrossWalkReaderModule::
writeKeyValueInCache(
        std::shared_ptr<CrossWalkedDeviceMap> finalValue) {
        crossWalkedDeviceMapAerospikeCacheService->putDataInCache(finalValue);

}

CrossWalkReaderModule::~CrossWalkReaderModule() {

}
