#ifndef RealTimeFeatureRegistryCacheUpdaterService_H
#define RealTimeFeatureRegistryCacheUpdaterService_H


#include <memory>
#include <string>
class IpToDeviceIdsMapCassandraService;
class DeviceIdToIpsMapCassandraService;
#include "TargetGroupFilterStatistic.h"
class EntityToModuleStateStats;
#include "IpToDeviceIdsMap.h"
#include "SizeBasedLRUCache.h"
#include "MySqlHighPerformanceService.h"
#include "DeviceIdToIpsMap.h"
#include <boost/optional.hpp>

class Device;
class MySqlFeatureRegistryService;
class MySqlFeatureUnderReviewService;
#include "Object.h"
template <class T>
class AeroCacheService;
class CrossWalkedDeviceMap;
class Feature;

namespace gicapods {
template <class K, class V>
class LRUCache;
}

/*

   this service has one task :
   reading the latest updated features in featureRegistry and populating the aerospike cache
 */

class RealTimeFeatureRegistryCacheUpdaterService : public Object {

private:

public:

MySqlFeatureUnderReviewService* mySqlFeatureUnderReviewService;
MySqlFeatureRegistryService* mySqlFeatureRegistryService;
EntityToModuleStateStats* entityToModuleStateStats;
AeroCacheService<Feature>* featuresAerospikeCacheService;

RealTimeFeatureRegistryCacheUpdaterService();

std::string getName();

std::string toString();

void updateFeaturesUnderReview();

void populateAerospikeFeatureCache();

virtual ~RealTimeFeatureRegistryCacheUpdaterService();
};



#endif
