#ifndef SizeBasedLRUCache_H
#define SizeBasedLRUCache_H


#include <memory>
#include <string>
#include <boost/optional.hpp>
#include "Poco/Exception.h"
#include "Poco/LRUCache.h"
#include "Poco/Bugcheck.h"
#include "Poco/Delegate.h"
#include "EntityToModuleStateStats.h"
#include "Object.h"

namespace gicapods {

template<class K, class V>
class SizeBasedLRUCache : public Object {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
std::string entityName;
std::shared_ptr<Poco::LRUCache<K, V> > internalCache;


SizeBasedLRUCache(
        int cacheSize,
        std::string entityName) : Object(__FILE__) {
        internalCache = std::make_shared<Poco::LRUCache<K, V> >(cacheSize);
        this->entityName = entityName;
}

virtual ~SizeBasedLRUCache() {

}

bool containKey(K key) {
        return internalCache->has(key);
}

boost::optional<V> get(K key) {
        if(containKey(key)) {
                boost::optional<V> value =  *(internalCache->get(key));
                entityToModuleStateStats->
                addStateModuleForEntity("CACHE_HIT",
                                        "SizeBasedLRUCache-" +entityName,
                                        "ALL");

                return value;
        }
        entityToModuleStateStats->
        addStateModuleForEntity("CACHE_MISS",
                                "SizeBasedLRUCache-" + entityName,
                                "ALL");

        return boost::none;
}

void put(K key, V value) {
        return internalCache->add(key, value);
}

void remove(K key) {
        return internalCache->remove(key);
}


};
}

#endif
