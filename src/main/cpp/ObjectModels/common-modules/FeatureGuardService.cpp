#include "FeatureGuardService.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceIdToIpsMap.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"

#include "IpToDeviceIdsMapCassandraService.h"
#include "DeviceIdToIpsMapCassandraService.h"
#include "Device.h"
#include "AeroCacheService.h"
#include "StringUtil.h"
#include "JsonArrayUtil.h"
#include "Feature.h"
#include "SizeBasedLRUCache.h"
#include "MySqlFeatureUnderReviewService.h"
#include "MySqlFeatureRegistryService.h"

FeatureGuardService::FeatureGuardService() : Object(__FILE__) {

}

std::string FeatureGuardService::getName() {
        return "FeatureGuardService";
}

bool FeatureGuardService::isAllowedToRecord(std::string featureCategory,
                                            std::string featureValue,
                                            bool submitFeatureForReviewValue) {

        //TODO remove this line. for now everything is allowed until I am done with testing modeling
        return true;

        //we still want to record MGRS100, and be able to have them in feature_registry table
        if (StringUtil::equalsIgnoreCase(featureCategory, Feature::MGRS100)) {
                //we don't want to record any MGRS100 in our feature_registry table
                //because they are too many..
                //maybe having a blacklist of MGRS100 is better than a whitelist
                //so we return true here, without submitting the feature for review
                return true;
        }

        boost::optional<std::shared_ptr<Feature> > featureObj =
                fetchFeatureFromInMemoryMap(featureCategory, featureValue);

        if (featureObj == boost::none) {
                //feature is not found in  in memory cache

                featureObj = fetchFeatureFromInRemoteCache(featureCategory, featureValue);
                if (featureObj == boost::none) {
                        //feature is not found in aero spike either
                        if (submitFeatureForReviewValue) {
                                entityToModuleStateStats->addStateModuleForEntity("FEATURE_SUBMITTED_FOR_REVIEW",
                                                                                  "FeatureGuardService",
                                                                                  "ALL");
                                //in some calls, we might choose to submit feature for review
                                submitFeatureForReview(featureCategory,
                                                       featureValue);
                        }
                        return false;
                }


                //we found the found the feature in remote cache, now we put the feature in memory now
                entityToModuleStateStats->addStateModuleForEntity("FEATURE_WAS_FOUND_IN_REMOTE_CACHE",
                                                                  "FeatureGuardService",
                                                                  "ALL");
                std::string featureKeyInMap = featureCategory + "-" + featureValue;
                featuresCache->put(featureKeyInMap, *featureObj);

        }

        //feature is found from aerospike cache, now we check the status of it
        MLOG(10)<<"feature : "<<(*featureObj)->toJson();

        if (StringUtil::equalsIgnoreCase((*featureObj)->getStatus(), "Active")) {
                entityToModuleStateStats->addStateModuleForEntity("FEATURE_IS_APPROVED",
                                                                  "FeatureGuardService",
                                                                  "ALL");
                return true;
        }

        entityToModuleStateStats->addStateModuleForEntity("FEATURE_IS_DISAPPROVED",
                                                          "FeatureGuardService",
                                                          "ALL");

        return false;
}

boost::optional<std::shared_ptr<Feature> > FeatureGuardService::fetchFeatureFromInRemoteCache(
        std::string featureCategory,
        std::string featureValue) {

        std::shared_ptr<Feature> featureObj = std::make_unique<Feature>(featureCategory,
                                                                        featureValue,
                                                                        0);

        int cacheResultStatus = 0;
        auto featureInCache = featuresAerospikeCacheService->readDataOptional(featureObj, cacheResultStatus);
        if (featureInCache == nullptr) {
                return boost::none;
        }

        boost::optional<std::shared_ptr<Feature> > cachedValue = featureInCache;
        return cachedValue;
}

boost::optional<std::shared_ptr<Feature> > FeatureGuardService::fetchFeatureFromInMemoryMap(
        std::string featureCategory,
        std::string featureValue) {

        std::string featureKeyInMap = featureCategory + "-" + featureValue;

        auto featueInCache = featuresCache->get(featureKeyInMap);
        return featueInCache;
}

void FeatureGuardService::submitFeatureForReview(
        std::string featureCategory,
        std::string featureValue) {
        std::shared_ptr<Feature> featureObj =
                std::make_shared<Feature> (featureCategory, featureValue, 0);

        MLOG(3) << "features submitted for review : " << featureObj->toJson();

        mySqlHighPerformanceFeatureUnderReviewService->queueForPersistence(featureObj);
}

FeatureGuardService::~FeatureGuardService() {

}
