#ifndef DeviceHistoryUpdaterModule_H
#define DeviceHistoryUpdaterModule_H
#include "Object.h"
#include <memory>
#include <string>
#include "DeviceFeatureHistoryCassandraService.h";
class Device;
class EntityToModuleStateStats;

class FeatureGuardService;
#include "DeviceIdService.h"
#include "LoadPercentageBasedSampler.h"
class DeviceHistoryUpdaterModule : public Object {

private:
public:
DeviceIdService* deviceIdService;
FeatureGuardService* featureGuardService;
std::unique_ptr<LoadPercentageBasedSampler> deviceHistorySizeSampler;

DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
EntityToModuleStateStats* entityToModuleStateStats;

std::string getName();

virtual void process(std::string featureType, std::string featureValue, std::shared_ptr<Device> device);

DeviceHistoryUpdaterModule(
        DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService);

void prepFeatureForWrite(
        std::string featureType,
        std::string featureValue,
        std::shared_ptr<Device> device);

bool isAValidAndSensibleDevice(std::shared_ptr<Device> device);

virtual ~DeviceHistoryUpdaterModule();

};


#endif
