#ifndef CrossWalkUpdaterModule_H
#define CrossWalkUpdaterModule_H

#include "Object.h"
#include <memory>
#include <string>
class IpToDeviceIdsMapCassandraService;
class DeviceIdToIpsMapCassandraService;
#include "TargetGroupFilterStatistic.h"
class EntityToModuleStateStats;
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"


class CrossWalkUpdaterModule : public Object {

private:

public:
IpToDeviceIdsMapCassandraService* ipToDeviceIdsMapCassandraService;
DeviceIdToIpsMapCassandraService* deviceIdToIpsMapCassandraService;
EntityToModuleStateStats* entityToModuleStateStats;

CrossWalkUpdaterModule();

std::string getName();

std::string toString();

void recordIpToDeviceAssociations(std::string ipAddress, std::shared_ptr<Device> device);

virtual ~CrossWalkUpdaterModule();

};



#endif
