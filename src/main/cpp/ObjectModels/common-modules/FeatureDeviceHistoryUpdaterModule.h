#ifndef FeatureDeviceHistoryUpdaterModule_H
#define FeatureDeviceHistoryUpdaterModule_H
#include "Object.h"
#include <memory>
#include <string>

#include <boost/optional.hpp>
class FeatureDeviceHistory;
class Device;

class FeatureDeviceHistoryCassandraService;


class FeatureGuardService;
class EntityToModuleStateStats;

class LoadPercentageBasedSampler;
namespace gicapods { template <typename K, typename V> class ConcurrentHashMap; }

class FeatureDeviceHistoryUpdaterModule : public Object {

private:

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, FeatureDeviceHistory> > mapOfInterestingFeatures;
std::unique_ptr<LoadPercentageBasedSampler> randomFeatureSampler;
FeatureGuardService* featureGuardService;
FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService;
EntityToModuleStateStats* entityToModuleStateStats;


FeatureDeviceHistoryUpdaterModule(
        FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService);

std::string getName();

virtual void process(
        std::string featureCategory,
        std::string featureValue,
        std::shared_ptr<Device> device);

bool isInterestingFeature(std::string featureValue);

void prepFeatureValueForWrite(
        std::string featureCategory,
        std::string featureValue,
        std::shared_ptr<Device> device);

std::shared_ptr<FeatureDeviceHistory> createEntityForWrite(
        std::string featureCategory,
        std::string featureValue,
        std::shared_ptr<Device> device);

virtual ~FeatureDeviceHistoryUpdaterModule();
};

#endif
