#include "FeatureDeviceHistoryUpdaterModule.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashMap.h"

#include "Feature.h"
#include "FeatureDeviceHistory.h"

#include <boost/optional.hpp>
#include "FeatureDeviceHistoryCassandraService.h"
#include "ConcurrentHashMap.h"
#include "DeviceHistory.h"
#include "LoadPercentageBasedSampler.h"
#include "FeatureGuardService.h"


FeatureDeviceHistoryUpdaterModule::
FeatureDeviceHistoryUpdaterModule(
        FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService) : Object(__FILE__) {
        this->featureDeviceHistoryCassandraService = featureDeviceHistoryCassandraService;

}


std::string FeatureDeviceHistoryUpdaterModule::getName() {
        return "FeatureDeviceHistoryUpdaterModule";
}


void FeatureDeviceHistoryUpdaterModule::process(
        std::string featureCategory,
        std::string featureValue,
        std::shared_ptr<Device> device) {

        assertAndThrow(!featureValue.empty());
        if (!isInterestingFeature(featureValue)) {
                entityToModuleStateStats->addStateModuleForEntity("NOT_PART_OF_INTERESTING_FEATURES",
                                                                  "FeatureDeviceHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);

                if (randomFeatureSampler->isPartOfSample()) {
                        entityToModuleStateStats->addStateModuleForEntity("PART_OF_NEGATIVE_SAMPLE",
                                                                          "FeatureDeviceHistoryUpdaterModule",
                                                                          EntityToModuleStateStats::all);
                        prepFeatureValueForWrite(featureCategory,
                                                 featureValue,
                                                 device);
                }
        } else {
                entityToModuleStateStats->addStateModuleForEntity("PART_OF_INTERESTING_FEATURES",
                                                                  "FeatureDeviceHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);
                prepFeatureValueForWrite(featureCategory,
                                         featureValue,
                                         device);
        }
}


bool FeatureDeviceHistoryUpdaterModule::isInterestingFeature(std::string featureValue) {
        //we have a list of interesting seed websites that we want to know
        //who has visited them, in order not to save millions of records
        // for visitors of every website that doesn't interest us,
        // we use this map that gets populated from interesting_domains_to_model in database
        return mapOfInterestingFeatures->exists(featureValue);
}


void FeatureDeviceHistoryUpdaterModule::prepFeatureValueForWrite(
        std::string featureCategory,
        std::string featureValue,
        std::shared_ptr<Device> device) {


        if (!featureGuardService->isAllowedToRecord(
                    featureCategory,
                    featureValue,
                    true /*submitFeatureForReview*/)) {

                entityToModuleStateStats->addStateModuleForEntity("featureIsNotAllowedToBeRecorded:" + featureCategory,
                                                                  "FeatureDeviceHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);
                return;
        }

        auto featureDeviceHistory = createEntityForWrite(featureCategory,
                                                         featureValue,
                                                         device);

        featureDeviceHistoryCassandraService->pushToWriteBatchQueue(featureDeviceHistory);
        entityToModuleStateStats->addStateModuleForEntity("writing_feature_type:" + featureCategory,
                                                          "FeatureDeviceHistoryUpdaterModule",
                                                          EntityToModuleStateStats::all);
}



std::shared_ptr<FeatureDeviceHistory>
FeatureDeviceHistoryUpdaterModule::createEntityForWrite(
        std::string featureCategory,
        std::string featureValue,
        std::shared_ptr<Device> device) {
        auto devHistory = std::make_shared<DeviceHistory>(device);
        devHistory->timeOfVisit = DateTimeUtil::getNowInMilliSecond();

        auto featureObject = std::make_unique<Feature>(featureCategory, featureValue, 0);
        auto featureDeviceHistory = std::make_shared<FeatureDeviceHistory>(
                std::move(featureObject));

        featureDeviceHistory->devicehistories.push_back(devHistory);
        return featureDeviceHistory;
}

FeatureDeviceHistoryUpdaterModule::~FeatureDeviceHistoryUpdaterModule() {

}
