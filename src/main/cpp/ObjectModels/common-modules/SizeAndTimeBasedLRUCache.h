#ifndef SizeAndTimeBasedLRUCache_H
#define SizeAndTimeBasedLRUCache_H


#include <memory>
#include <string>
#include <boost/optional.hpp>
#include "Poco/Exception.h"
#include "Poco/ExpireLRUCache.h"
#include "Poco/Bugcheck.h"
#include "Poco/Delegate.h"
#include "Object.h"
#include "DateTimeUtil.h"
#include "AtomicLong.h"
#include "EntityToModuleStateStats.h"
#include "StringUtil.h"
#include "JsonArrayUtil.h"

class EntityToModuleStateStats;

namespace gicapods {

template<class K, class V>
class SizeAndTimeBasedLRUCache : public Object {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
std::string entityName;
std::shared_ptr<Poco::ExpireLRUCache<K, V> > internalCache;

SizeAndTimeBasedLRUCache(
        int cacheSize,
        int cacheExpiryInMinutes,
        std::string entityName) : Object(__FILE__) {
        Timestamp::TimeDiff expireTimeInMilliSec = cacheExpiryInMinutes * 60 * 1000;
        internalCache =
                std::make_shared<Poco::ExpireLRUCache<K, V> >(
                        cacheSize,
                        expireTimeInMilliSec
                        );
        this->entityName = entityName;
}

virtual ~SizeAndTimeBasedLRUCache() {

}

bool containKey(K key) {
        return internalCache->has(key);
}

boost::optional<V> get(K key) {
        if(containKey(key)) {
                boost::optional<V> value =  *(internalCache->get(key));
                entityToModuleStateStats->
                addStateModuleForEntity("CACHE_HIT",
                                        "SizeAndTimeBasedLRUCache-" +entityName,
                                        "ALL");

                return value;
        }
        entityToModuleStateStats->
        addStateModuleForEntity("CACHE_MISS",
                                "SizeAndTimeBasedLRUCache-" + entityName,
                                "ALL");

        return boost::none;
}

void put(K key, V value) {
        return internalCache->add(key, value);
}

void remove(K key) {
        return internalCache->remove(key);
}
};
}

#endif
