#include "FeatureToFeatureHistoryUpdaterModule.h"
#include "DateTimeUtil.h"
#include "FeatureToFeatureHistoryCassandraService.h"
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashMap.h"

#include "Feature.h"
#include "FeatureToFeatureHistory.h"

#include <boost/optional.hpp>
#include "FeatureToFeatureHistoryCassandraService.h"
#include "ConcurrentHashMap.h"
#include "DeviceHistory.h"
#include "LoadPercentageBasedSampler.h"
#include "FeatureHistory.h"
#include "FeatureToFeatureHistory.h"
#include "FeatureGuardService.h"


FeatureToFeatureHistoryUpdaterModule::
FeatureToFeatureHistoryUpdaterModule(
        FeatureToFeatureHistoryCassandraService* featureToFeatureHistoryCassandraService) : Object(__FILE__) {
        this->featureToFeatureHistoryCassandraService = featureToFeatureHistoryCassandraService;

}


std::string FeatureToFeatureHistoryUpdaterModule::getName() {
        return "FeatureToFeatureHistoryUpdaterModule";
}


void FeatureToFeatureHistoryUpdaterModule::process(
        std::string featureKeyCategory,
        std::string featureKeyValue,
        std::string featureCategory,
        std::string featureValue) {

        assertAndThrow(!featureValue.empty());
        if (!isInterestingFeature(featureValue)) {
                entityToModuleStateStats->addStateModuleForEntity("NOT_PART_OF_INTERESTING_FEATURES",
                                                                  "FeatureToFeatureHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);

                if (randomFeatureSampler->isPartOfSample()) {
                        entityToModuleStateStats->addStateModuleForEntity("PART_OF_NEGATIVE_SAMPLE",
                                                                          "FeatureToFeatureHistoryUpdaterModule",
                                                                          EntityToModuleStateStats::all);
                        prepFeatureValueForWrite(featureKeyCategory,
                                                 featureKeyValue,
                                                 featureCategory,
                                                 featureValue);
                }
        } else {
                entityToModuleStateStats->addStateModuleForEntity("PART_OF_INTERESTING_FEATURES",
                                                                  "FeatureToFeatureHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);
                prepFeatureValueForWrite(featureKeyCategory,
                                         featureKeyValue,
                                         featureCategory,
                                         featureValue);
        }
}


bool FeatureToFeatureHistoryUpdaterModule::isInterestingFeature(std::string featureValue) {
        //we have a list of interesting seed websites that we want to know
        //who has visited them, in order not to save millions of records
        // for visitors of every website that doesn't interest us,
        // we use this map that gets populated from interesting_domains_to_model in database
        return mapOfInterestingFeatures->exists(featureValue);
}


void FeatureToFeatureHistoryUpdaterModule::prepFeatureValueForWrite(
        std::string featureKeyCategory,
        std::string featureKeyValue,
        std::string featureCategory,
        std::string featureValue) {

        auto featureDeviceHistory = createEntityForWrite(
                featureKeyCategory,
                featureKeyValue,
                featureCategory,
                featureValue);

        featureToFeatureHistoryCassandraService->pushToWriteBatchQueue(featureDeviceHistory);
        entityToModuleStateStats->addStateModuleForEntity("writing_feature_type:" + featureKeyCategory,
                                                          "FeatureToFeatureHistoryUpdaterModule",
                                                          EntityToModuleStateStats::all);
}



std::shared_ptr<FeatureToFeatureHistory>
FeatureToFeatureHistoryUpdaterModule::createEntityForWrite(
        std::string featureKeyCategory,
        std::string featureKeyValue,
        std::string featureCategory,
        std::string featureValue) {

        auto featureKeyObject = std::make_unique<Feature>(featureKeyCategory, featureKeyValue, 0);

        auto featureObject = std::make_shared<Feature>(featureCategory, featureValue, 0);
        auto fh = std::make_shared<FeatureHistory>(featureObject, DateTimeUtil::getNowInMilliSecond());
        auto ffh = std::make_shared<FeatureToFeatureHistory>(std::move(featureKeyObject));
        ffh->featureHistories.push_back(fh);
        return ffh;
}

FeatureToFeatureHistoryUpdaterModule::~FeatureToFeatureHistoryUpdaterModule() {

}
