#include "CrossWalkUpdaterModule.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceIdToIpsMap.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"

#include "IpToDeviceIdsMapCassandraService.h"
#include "DeviceIdToIpsMapCassandraService.h"

CrossWalkUpdaterModule::CrossWalkUpdaterModule() : Object(__FILE__) {

}

std::string CrossWalkUpdaterModule::getName() {
        return "CrossWalkUpdaterModule";
}

void CrossWalkUpdaterModule::recordIpToDeviceAssociations(std::string ipAddress, std::shared_ptr<Device> device) {

        auto ipToDeviceIdsMap = std::make_shared<IpToDeviceIdsMap>();
        ipToDeviceIdsMap->ip = ipAddress;
        ipToDeviceIdsMap->devices.push_back(device);

        auto deviceIdToIpsMap = std::make_shared<DeviceIdToIpsMap> ();
        deviceIdToIpsMap->device = device;
        deviceIdToIpsMap->ips.insert (ipAddress);

        ipToDeviceIdsMapCassandraService->pushToWriteBatchQueue(ipToDeviceIdsMap);
        deviceIdToIpsMapCassandraService->pushToWriteBatchQueue(deviceIdToIpsMap);

        entityToModuleStateStats->addStateModuleForEntity("pushed_to_queue_for_write",
                                                          "CrossWalkUpdaterModule",
                                                          EntityToModuleStateStats::all);


}

CrossWalkUpdaterModule::~CrossWalkUpdaterModule() {

}
