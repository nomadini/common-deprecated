#ifndef IntWrapper_h
#define IntWrapper_h


#include <atomic>
#include <memory>
#include <string>
#include "Object.h"
#include "AtomicLong.h"
#include "JsonTypeDefs.h"
//use AtomicLong instead of this class
class IntWrapper;

class IntWrapper : public Object {

public:

std::shared_ptr<gicapods::AtomicLong> value;

IntWrapper ();
IntWrapper (int v);
int getValue();
void setValue(int v);
void increment();

virtual ~IntWrapper();

std::string toJson();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

static std::shared_ptr<IntWrapper> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<IntWrapper> fromJson(std::string json);
};
#endif
