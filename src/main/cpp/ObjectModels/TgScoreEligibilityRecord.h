#ifndef TgScoreEligibilityRecord_h
#define TgScoreEligibilityRecord_h


#include <atomic>
#include <memory>
#include <string>
#include <unordered_map>
#include "JsonTypeDefs.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "TgModelEligibilityRecord.h"
#include "Object.h"
#include "DateTimeUtil.h"
#include "Object.h"

class TgScoreEligibilityRecord : public Object {
public:

int targetGroupId;
std::shared_ptr<std::unordered_map<int, std::shared_ptr<TgModelEligibilityRecord> > > modelIdToEligibilityRecords;
TgScoreEligibilityRecord();
virtual ~TgScoreEligibilityRecord();
void addPropertiesToJsonValue(RapidJsonValueType parentValue, DocumentType* doc);

static std::shared_ptr<TgScoreEligibilityRecord> fromJsonValue(RapidJsonValueType value);
std::string toJson();

static std::shared_ptr<TgScoreEligibilityRecord> fromJson(std::string jsonString);
};

#endif
