#ifndef BeanFactory_H
#define BeanFactory_H


#include <memory>
#include <string>
#include <vector>
#include <set>
#include <tbb/concurrent_hash_map.h>
#include <unordered_set>
#include <unordered_map>

class MaxMindService;
class MySqlPlaceService;
class HeartBeatRecorderService;
class RecentVisitHistoryScorer;
class MySqlDriver;
class MySqlTargetGroupFilterCountDbRecordService;
class HttpUtilService;
class MySqlTargetGroupRecencyModelMapService;
class TargetGroupRecencyModelMapCacheService;
class MySqlRecencyModelService;
class KafkaProducer;
class LatLonToMGRSConverter;
template<class T>
class MySqlHourlyCompressor;

class MySqlTgBiddingPerformanceMetricDtoService;
class DeviceIdService;
class MySqlPixelClusterResultService;
class AdHistoryCassandraService;
class EntityToModuleStateStats;
class MySqlTgPerformanceTrackService;
class RecencyModelCacheService;
class CassandraServiceQueueManager;

class MySqlCampaignService;
class MySqlTargetGroupService;

class EntityDeliveryInfoCacheService;
class ObjectGlobalMapContainer;


class TargetGroupCacheService;
class CampaignCacheService;
class SegmentCacheService;
class TargetGroupSegmentCacheService;

class CreativeCacheService;
class TargetGroupBWListCacheService;
class TargetGroupCreativeCacheService;
class TargetGroupMgrsSegmentCacheService;
class RealTimeFeatureRegistryCacheUpdaterService;
class TargetGroupGeoSegmentCacheService;
class TargetGroupDayPartCacheService;
class TargetGroupGeoLocationCacheService;
class GlobalWhiteListedCacheService;
class TgGeoFeatureCollectionMapCacheService;
class ModelCacheService;
class ModelResultCacheService;
class MySqlTagService;
class OfferPixelMap;

class TargetGroupRecencyModelMap;
class LatLonToPolygonConverter;
class GlobalWhiteListedModelCacheService;
class ClientDealCacheService;
class DealCacheService;
class MySqlPixelService;
class MySqlOfferService;
class ClientDeal;
class MySqlBWEntryService;
class TargetGroupFilterStatistic;
class CampaignOffer;
class TargetGroupOffer;
class EntityToModuleStateStatsPersistenceService;

class AsyncThreadPoolService;
class DateTimeService;
class AerospikeDriverInterface;
class CassandraDriverInterface;
class MySqlMetricService;
class MySqlModelService;
class MySqlModelResultService;
class InfluxDbClient;
class MySqlSegmentService;
class MySqlAdvertiserModelMappingService;
class PixelDeviceHistoryCassandraService;

class EntityToModuleStateStatsPersistenceService;
class MySqlCampaignService;
class MySqlAdvertiserService;
class MySqlClientService;

class MySqlBWListService;
class MySqlTargetGroupBWListMapService;
class MySqlTargetGroupSegmentMapService;

class MySqlTgBiddingPerformanceMetricDtoService;
class MySqlTgPerformanceTrackService;

class MySqlEntityRealTimeDeliveryInfoService;
class MySqlTopMgrsSegmentService;
class MySqlCreativeService;
class TargetGroupCreativeMap;
class TargetGroupMgrsSegmentMap;
class MySqlTgGeoFeatureCollectionMapService;
class MySqlTargetGroupCreativeMapService;
class MySqlTargetGroupMgrsSegmentMapService;
class MySqlTargetGroupGeoSegmentListMapService;
class MySqlTargetGroupDayPartTargetMapService;
class MySqlGeoLocationService;
class MySqlTargetGroupGeoLocationService;
class MySqlGlobalWhiteListService;
class MySqlGlobalWhiteListModelService;
class MySqlDealService;
class MySqlClientDealService;
class MySqlBadDeviceService;
class MySqlModelService;
class AdHistory;
class DeviceSegmentHistory;
class MySqlTagPlaceService;
class GicapodsIdToExchangeIdsMapCassandraService;
class AdhistoryCassandraService;
class DeviceFeatureHistory;
class DeviceGeoFeatureHistory;
#include "AtomicBoolean.h";
#include "DeviceFeatureHistoryCassandraService.h";
#include "FeatureDeviceHistoryCassandraService.h";
#include "FeatureToFeatureHistoryCassandraService.h";
class IpToDeviceIdsMapCassandraService;
class DeviceIdToIpsMapCassandraService;
class GicapodsIdToExchangeIdsMapCassandraService;
class Feature;
class Client;
class ModelScore;
class DeviceSegmentHistoryCassandraService;
class MySqlEntityRealTimeDeliveryInfoService;
class EventLog;

class Creative;
class EmailSender;
class SegmentToDeviceCassandraService;
template<typename Type>
class RealTimeEntityCacheService;

template<typename Type>
class MySqlDataMover;

template<typename Type>
class CacheService;

template<typename Type>
class CassandraService;

template<typename Type1, typename Type2>
class MySqlHighPerformanceService;

template<typename Type>
class AeroCacheService;
class EventLock;
class MetricHealthCheckService;
class CrossWalkedDeviceMap;
class AdInteractionRecordingModule;
class BadDevice;
class Client;
class Offer;
class Pixel;

class CampaignRealTimeInfo;
class Segment;
class TargetGroup;
class BidEventLog;
class ModelResult;
class OfferSegmentCacheService;
class TargetGroupOfferCacheService;
class CampaignOfferCacheService;
class OfferPixelMapCacheService;
class MySqlCampaignOfferService;
class MySqlTargetGroupOfferService;
class FeatureGuardService;
class MySqlOfferPixelMapService;
class Pixel;
class CrossWalkUpdaterModule;
class OfferSegment;
class MySqlOfferSegmentService;
class CrossWalkReaderModule;
class CacheServiceInterface;
class PlacesCachedResult;
class OfferCacheService;
class GeoLocation;
class GeoLocationCacheService;
class MySqlFeatureUnderReviewService;
class MySqlFeatureRegistryService;
class MetricDbRecord;
class Campaign;
class TargetGroupBWListMap;
class TargetGroupDayPartTargetMap;;
class TargetGroupGeoLocationList;
class TargetGroupGeoLocation;
class GlobalWhiteListEntry;;
class TgGeoFeatureCollectionMap;
class ModelRequest;;
class TargetGroupSegmentMap;;
class Deal;
class TargetGroupSegmentGroupMap;
class PixelCacheService;
class WiretapCassandraService;
class ImpressionEventDto;
class SegmentGroup;
class SegmentGroupSegmentMap;
class Advertiser;
namespace gicapods {
class ConfigService;
class BootableConfigService;

template <class K, class V>
class ConcurrentHashMap;

template<class T>
class ConcurrentHashSet;

};

#include <boost/thread.hpp>
#include "Object.h"
class BeanFactory;
class EntityProviderServiceInterface;

template<class T>
class EntityProviderService;

class CacheServiceInterface;


class BeanFactory {

public:
void addCachesToList();
std::vector<CacheServiceInterface*> allCacheServices;
std::vector<EntityProviderServiceInterface*> allEntityProviderService;
std::shared_ptr<EntityToModuleStateStats> entityToModuleStateStats;
std::unique_ptr<gicapods::ConfigService> configService;

int dequeueAsyncTasksIntervalInSeconds;
std::string dataMasterUrl;
std::string propertyFileName;
std::string commonPropertyFileName;
std::string appName;
std::string appVersion;


std::shared_ptr<gicapods::AtomicLong> queueSizeWaitingForAck;

std::shared_ptr<gicapods::AtomicBoolean> bidderIsStopped;
std::shared_ptr<gicapods::AtomicBoolean> asyncThreadIsStopped;
std::shared_ptr<gicapods::AtomicBoolean> stopConsumingThread;
std::shared_ptr<gicapods::AtomicBoolean> dequeueThreadIsRunning;

std::unique_ptr<LatLonToPolygonConverter> latLonToPolygonConverter;
std::unique_ptr<MySqlOfferSegmentService> mySqlOfferSegmentService;
std::unique_ptr<MySqlPixelClusterResultService> mySqlPixelClusterResultService;
std::unique_ptr<MySqlTargetGroupRecencyModelMapService> mySqlTargetGroupRecencyModelMapService;
std::unique_ptr<TargetGroupRecencyModelMapCacheService> targetGroupRecencyModelMapCacheService;
std::shared_ptr<MySqlHighPerformanceService<std::string, Feature> > mySqlHighPerformanceFeatureUnderReviewService;

std::shared_ptr<HeartBeatRecorderService> heartBeatRecorderService;

std::unique_ptr<MySqlFeatureUnderReviewService> mySqlFeatureUnderReviewService;
std::unique_ptr<ObjectGlobalMapContainer> objectGlobalMapContainer;
std::unique_ptr<MySqlFeatureRegistryService> mySqlFeatureRegistryService;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts;
std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedDevices;
std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedIps;
std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedMgrs10ms;

std::shared_ptr<std::unordered_set<int> > targetGroupIdsWithGeoCollectionAssignments;
std::shared_ptr<std::unordered_set<std::string> > targetGroupToGeoCollectionKeys;
std::shared_ptr<std::unordered_set<int> > argetGroupIdsWithGeoCollectionAssignments;

std::unique_ptr<InfluxDbClient> influxDbClient;
std::unique_ptr<CrossWalkUpdaterModule> crossWalkUpdaterModule;
std::unique_ptr<CrossWalkReaderModule> crossWalkReaderModule;
std::unique_ptr<RealTimeFeatureRegistryCacheUpdaterService> realTimeFeatureRegistryCacheUpdaterService;
std::shared_ptr<MySqlHourlyCompressor<MetricDbRecord> > mySqlMetricCompressor;
std::unique_ptr<EmailSender> emailSender;
std::unique_ptr<MySqlTagService> mySqlTagService;
std::unique_ptr<MySqlTagPlaceService> mySqlTagPlaceService;
std::unique_ptr<gicapods::BootableConfigService> bootableConfigService;
std::shared_ptr<MySqlDataMover<ImpressionEventDto> > mySqlImpressionMover;
std::unique_ptr<MaxMindService> maxMindService;
std::unique_ptr<MySqlPlaceService> mySqlPlaceService;

std::unique_ptr<DeviceIdService> deviceIdService;

std::unique_ptr<MySqlDriver> mySqlDriverMangoGeo;
std::unique_ptr<MySqlDriver> mySqlDriverAlpha1;
std::shared_ptr<MySqlDriver> mySqlDriverMango;
std::shared_ptr<HttpUtilService> httpUtilService;
std::unique_ptr<AdInteractionRecordingModule> adInteractionRecordingModule;

std::unique_ptr<AeroCacheService<PlacesCachedResult> > placesCachedResultAeroCacheService;
std::unique_ptr<AeroCacheService<EventLock> > eventLockPersistenceService;
std::unique_ptr<AeroCacheService<EventLog> > realTimeEventLogCacheService;
std::unique_ptr<AeroCacheService<CrossWalkedDeviceMap> > crossWalkedDeviceMapAerospikeCacheService;
std::unique_ptr<AeroCacheService<Feature> > featuresAerospikeCacheService;
std::unique_ptr<FeatureGuardService> featureGuardService;

std::unique_ptr<MetricHealthCheckService> metricHealthCheckService;


std::unique_ptr<TargetGroupOfferCacheService> targetGroupOfferCacheService;
std::unique_ptr<CampaignOfferCacheService> campaignOfferCacheService;
std::unique_ptr<MySqlCampaignOfferService> mySqlCampaignOfferService;
std::unique_ptr<MySqlTargetGroupOfferService> mySqlTargetGroupOfferService;
std::shared_ptr<MySqlSegmentService> mySqlSegmentService;
std::unique_ptr<AerospikeDriverInterface> aeroSpikeDriver;
std::unique_ptr<DateTimeService> dateTimeService;
std::unique_ptr<MySqlTgBiddingPerformanceMetricDtoService> mySqlTgBiddingPerformanceMetricDtoService;
std::unique_ptr<MySqlEntityRealTimeDeliveryInfoService> mySqlEntityRealTimeDeliveryInfoService;
std::unique_ptr<LatLonToMGRSConverter> latLonToMGRSConverter;

std::unique_ptr<AsyncThreadPoolService> asyncThreadPoolService;
std::unique_ptr<MySqlTgPerformanceTrackService> mySqlTgPerformanceTrackService;
std::unique_ptr<CassandraDriverInterface> cassandraDriver;
std::unique_ptr<CassandraServiceQueueManager> cassandraServiceQueueManager;
std::unique_ptr<MySqlTargetGroupService> mySqlTargetGroupService;
std::unique_ptr<MySqlMetricService> mySqlMetricService;
std::unique_ptr<EntityDeliveryInfoCacheService> entityDeliveryInfoCacheService;
std::unique_ptr<CacheService<BadDevice> > badDeviceCacheService;
std::unique_ptr<CacheService<Advertiser> > advertiserCacheService;

std::unique_ptr<CacheService<Client> > clientCacheService;
std::shared_ptr<EntityProviderService<SegmentGroup> > segmentGroupEntityProviderService;
std::shared_ptr<EntityProviderService<SegmentGroupSegmentMap> > segmentGroupSegmentMapEntityProviderService;
std::unique_ptr<EntityProviderService<OfferSegment> > offerSegmentEntityProviderService;
std::unique_ptr<EntityProviderService<ModelResult> > modelResultEntityProviderService;
std::unique_ptr<EntityProviderService<GeoLocation> > geoLocationEntityProviderService;
std::unique_ptr<EntityProviderService<Client> > clientEntityProviderService;
std::unique_ptr<EntityProviderService<Advertiser> > advertiserEntityProviderService;
std::unique_ptr<EntityProviderService<Campaign> > campaignEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroup> > targetGroupEntityProviderService;
std::unique_ptr<EntityProviderService<Segment> > segmentEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupBWListMap> > targetGroupBWListEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupCreativeMap> > targetGroupCreativeEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupMgrsSegmentMap> > targetGroupMgrsSegmentEntityProviderService;
std::unique_ptr<EntityProviderService<Creative> > creativeEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupDayPartTargetMap> > targetGroupDayPartEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupGeoLocationList> > targetGroupGeoLocationEntityProviderService;
std::unique_ptr<EntityProviderService<GlobalWhiteListEntry> > globalWhiteListedEntityProviderService;
std::unique_ptr<EntityProviderService<TgGeoFeatureCollectionMap> > tgGeoFeatureCollectionMapEntityProviderService;
std::unique_ptr<EntityProviderService<ModelRequest> > generalModelEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupSegmentMap> > targetGroupSegmentEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupSegmentGroupMap> > targetGroupSegmentGroupMapEntityProviderService;
std::unique_ptr<EntityProviderService<Offer> > offerEntityProviderService;
std::unique_ptr<EntityProviderService<Deal> > dealEntityProviderService;
std::unique_ptr<EntityProviderService<ClientDeal> > clientDealEntityProviderService;
std::unique_ptr<EntityProviderService<OfferPixelMap> > offerPixelMapEntityProviderService;
std::unique_ptr<EntityProviderService<Pixel> > pixelEntityProviderService;
std::unique_ptr<EntityProviderService<CampaignOffer> > campaignOfferEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupOffer> > targetGroupOfferEntityProviderService;
std::unique_ptr<EntityProviderService<TargetGroupRecencyModelMap> > targetGroupRecencyModelMapEntityProviderService;

TargetGroupCacheService* targetGroupCacheService;
std::unique_ptr<CampaignCacheService> campaignCacheService;
SegmentCacheService* segmentCacheService;
std::unique_ptr<TargetGroupSegmentCacheService> targetGroupSegmentCacheService;
std::unique_ptr<CreativeCacheService> creativeCacheService;
std::unique_ptr<TargetGroupBWListCacheService> targetGroupBWListCacheService;
std::unique_ptr<TargetGroupCreativeCacheService> targetGroupCreativeCacheService;
std::unique_ptr<TargetGroupMgrsSegmentCacheService> targetGroupMgrsSegmentCacheService;
std::unique_ptr<TargetGroupGeoSegmentCacheService> targetGroupGeoSegmentCacheService;
std::unique_ptr<TargetGroupDayPartCacheService> targetGroupDayPartCacheService;
std::unique_ptr<TargetGroupGeoLocationCacheService> targetGroupGeoLocationCacheService;
std::unique_ptr<GlobalWhiteListedCacheService> globalWhiteListedCacheService;
std::unique_ptr<TgGeoFeatureCollectionMapCacheService> tgGeoFeatureCollectionMapCacheService;
std::unique_ptr<ModelCacheService> modelCacheService;
std::unique_ptr<ModelResultCacheService> modelResultCacheService;
std::unique_ptr<OfferCacheService> offerCacheService;
std::unique_ptr<OfferSegmentCacheService> offerSegmentCacheService;

std::unique_ptr<MySqlOfferPixelMapService> mySqlOfferPixelMapService;
std::unique_ptr<PixelCacheService> pixelCacheService;
std::unique_ptr<OfferPixelMapCacheService> offerPixelMapCacheService;
std::unique_ptr<GlobalWhiteListedModelCacheService> globalWhiteListedModelCacheService;
std::unique_ptr<ClientDealCacheService> clientDealCacheService;
std::unique_ptr<DealCacheService> dealCacheService;
std::unique_ptr<CacheService<CampaignRealTimeInfo> > campaignRealTimeInfoCacheService;
std::unique_ptr<MySqlPixelService> mySqlPixelService;
std::unique_ptr<MySqlOfferService> mySqlOfferService;
std::unique_ptr<MySqlAdvertiserModelMappingService> mySqlAdvertiserModelMappingService;


std::unique_ptr<MySqlBWEntryService> mySqlBWEntryService;
std::unique_ptr<EntityToModuleStateStatsPersistenceService> entityToModuleStateStatsPersistenceService;
std::unique_ptr<MySqlCampaignService> mySqlCampaignService;
std::unique_ptr<MySqlAdvertiserService> mySqlAdvertiserService;
std::unique_ptr<MySqlClientService> mySqlClientService;

std::unique_ptr<MySqlBWListService> mySqlBWListService;
std::unique_ptr<MySqlTargetGroupBWListMapService> mySqlTargetGroupBWListMapService;
std::unique_ptr<MySqlTargetGroupSegmentMapService> mySqlTargetGroupSegmentMapService;
std::unique_ptr<MySqlCreativeService> mySqlCreativeService;
std::unique_ptr<MySqlTopMgrsSegmentService> mySqlTopMgrsSegmentService;
std::unique_ptr<MySqlTgGeoFeatureCollectionMapService> mySqlTgGeoFeatureCollectionMapService;
std::unique_ptr<MySqlTargetGroupCreativeMapService> mySqlTargetGroupCreativeMapService;
std::unique_ptr<MySqlTargetGroupMgrsSegmentMapService> mySqlTargetGroupMgrsSegmentMapService;
std::unique_ptr<MySqlTargetGroupGeoSegmentListMapService> mySqlTargetGroupGeoSegmentListMapService;
std::unique_ptr<MySqlTargetGroupDayPartTargetMapService> mySqlTargetGroupDayPartTargetMapService;
std::unique_ptr<MySqlGeoLocationService> mySqlGeoLocationService;
std::unique_ptr<GeoLocationCacheService> geoLocationCacheService;
std::unique_ptr<MySqlTargetGroupGeoLocationService> mySqlTargetGroupGeoLocationService;
std::unique_ptr<MySqlGlobalWhiteListService> mySqlGlobalWhiteListService;
std::unique_ptr<MySqlGlobalWhiteListModelService> mySqlGlobalWhiteListModelService;
std::unique_ptr<MySqlDealService> mySqlDealService;
std::unique_ptr<MySqlClientDealService> mySqlClientDealService;
std::unique_ptr<MySqlBadDeviceService> mySqlBadDeviceService;
std::shared_ptr<MySqlModelService> mySqlModelService;
std::unique_ptr<MySqlModelResultService> mySqlModelResultService;

std::unique_ptr<RealTimeEntityCacheService<AdHistory> > adhistoryCacheService;
std::unique_ptr<AeroCacheService<AdHistory> > realTimeAdHistoryAerospikeCacheService;

std::unique_ptr<KafkaProducer> kafkaProducer;
std::unique_ptr<AdHistoryCassandraService> adHistoryCassandraService;
std::unique_ptr<WiretapCassandraService> wiretapCassandraService;
std::unique_ptr<GicapodsIdToExchangeIdsMapCassandraService> gicapodsIdToExchangeIdsMapCassandraService;
std::unique_ptr<PixelDeviceHistoryCassandraService> pixelDeviceHistoryCassandraService;

std::unique_ptr<CassandraService<EventLog> > eventLogCassandraService;
std::unique_ptr<CassandraService<BidEventLog> > bidEventLogCassandraService;
std::unique_ptr<CassandraService<ModelScore> > modelScoreCassandraService;

std::unique_ptr<AeroCacheService<DeviceSegmentHistory> > realTimeDeviceSegementHistoryAerospikeCacheService;
std::unique_ptr<RealTimeEntityCacheService<DeviceSegmentHistory> > realTimeEntityCacheServiceDeviceSegmentHistory;

std::unique_ptr<DeviceSegmentHistoryCassandraService> deviceSegmentHistoryCassandraService;
std::unique_ptr<DeviceFeatureHistoryCassandraService> deviceFeatureHistoryCassandraService;
std::unique_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService;
std::unique_ptr<FeatureToFeatureHistoryCassandraService> featureToFeatureHistoryCassandraService;
std::unique_ptr<IpToDeviceIdsMapCassandraService> ipToDeviceIdsMapCassandraService;
std::unique_ptr<DeviceIdToIpsMapCassandraService> deviceIdToIpsMapCassandraService;
std::unique_ptr<SegmentToDeviceCassandraService> segmentToDeviceCassandraService;

std::unique_ptr<MySqlTargetGroupFilterCountDbRecordService> mySqlTargetGroupFilterCountDbRecordService;
std::unique_ptr<MySqlRecencyModelService> mySqlRecencyModelService;
std::unique_ptr<RecencyModelCacheService> recencyModelCacheService;

BeanFactory(std::string appVersion);
void initializeModules();
virtual ~BeanFactory();

};

#endif
