#include "ServiceFactory.h"

#include "CommonRequestHandlerFactory.h"
#include "DataMasterRequestHandlerFactory.h"
#include "BeanFactory.h"
#include "ConverterUtil.h"
#include "ConfigService.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "DataReloadService.h"

ServiceFactory::ServiceFactory(EntityToModuleStateStats* entityToModuleStateStats,
                               gicapods::ConfigService* configService,
                               gicapods::BootableConfigService* bootableConfigService,
                               std::vector<CacheServiceInterface*> allCacheServices,
                               std::vector<EntityProviderServiceInterface*> allEntityProviderService) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->configService = configService;
        this->bootableConfigService = bootableConfigService;
        this->allEntityProviderService = allEntityProviderService;
        this->allCacheServices = allCacheServices;
}

void ServiceFactory::initializeModules() {

        commonRequestHandlerFactory = CommonRequestHandlerFactory::shared_instance(
                entityToModuleStateStats,
                configService,
                bootableConfigService
                );

        dataMasterRequestHandlerFactory = std::make_unique<DataMasterRequestHandlerFactory> ();
        dataMasterRequestHandlerFactory->entityToModuleStateStats = entityToModuleStateStats;

        dataMasterRequestHandlerFactory->commonRequestHandlerFactory = commonRequestHandlerFactory.get();
        dataMasterRequestHandlerFactory->init();

        commonRequestHandlerFactory->dataMasterRequestHandlerFactory =
                dataMasterRequestHandlerFactory.get();


        auto isReloadProcessHealthy = std::make_shared<gicapods::AtomicBoolean>(true);

        dataReloadService = std::make_shared<DataReloadService>(
                allCacheServices,
                allEntityProviderService
                );
        dataReloadService->isReloadProcessHealthy = isReloadProcessHealthy;
        dataReloadService->entityToModuleStateStats = entityToModuleStateStats;


        auto interruptedFlag = std::make_shared<gicapods::AtomicBoolean>(false);
        auto reloadCachesIntervalInSecond =
                ConverterUtil::convertTo<int> (configService->get ("reloadCachesIntervalInSecond"));
        dataReloadService->interruptedFlag = interruptedFlag;
        dataReloadService->reloadCachesIntervalInSecond = reloadCachesIntervalInSecond;

}

ServiceFactory::~ServiceFactory() {

}
