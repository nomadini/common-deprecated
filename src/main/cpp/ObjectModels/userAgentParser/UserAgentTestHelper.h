#ifndef UserAgentTestHelper_H
#define UserAgentTestHelper_H



#include <string>
#include <memory>
#include <vector>
class StringUtil;
class UserAgentTestHelper {
public:
static std::string getRandomUserAgentFromFiniteSet();
static std::shared_ptr <std::vector<std::string> > getUserAgentSamples();
};



#endif
