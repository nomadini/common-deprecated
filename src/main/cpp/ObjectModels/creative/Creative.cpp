#include "GUtil.h"
#include "Creative.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "CollectionUtil.h"
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"


std::string Creative::CONTENT_TYPE_DISPLAY = "display";
std::string Creative::CONTENT_TYPE_VIDEO = "video";

std::string Creative::AD_TYPE_JAVASCRIPT = "javascript";
std::string Creative::AD_TYPE_IFRAME = "iframe";

Creative::Creative()  : Object(__FILE__) {
        secure = false;
        id = 0;
        advertiserId = 0;

        this->apis = std::make_shared<std::unordered_map<std::string, bool> >();
        this->attributes = std::make_shared<std::unordered_map<std::string, bool> >();
}

std::string Creative::toString() {
        return this->toJson ();
}

std::string Creative::getEntityName() {
        return "Creative";
}
std::shared_ptr<Creative> Creative::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<Creative> creative = std::make_shared<Creative>();

        creative->setId(JsonUtil::GetIntSafely(value, "id"));
        creative->setName(JsonUtil::GetStringSafely(value, "name"));

        creative->setDescription(JsonUtil::GetStringSafely(value,"description"));
        creative->setAdvertiserId(JsonUtil::GetIntSafely(value,"advertiserId"));
        creative->setStatus(JsonUtil::GetStringSafely(value,"status"));
        creative->setAdTag(JsonUtil::GetStringSafely(value,"adTag"));

        creative->setPreviewUrl (JsonUtil::GetStringSafely(value,"previewUrl"));
        creative->setSize (JsonUtil::GetStringSafely(value,"size"));
        creative->setCreatedAt (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt")));
        creative->setUpdatedAt (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt")));
        creative->setLandingPageUrl (JsonUtil::GetStringSafely(value,"landingPageUrl"));
        creative->setAdType (JsonUtil::GetStringSafely(value,"adType"));
        creative->creativeContentType = JsonUtil::GetStringSafely(value,"creativeContentType");
        creative->setSecure (JsonUtil::GetBooleanSafely (value,"secure"));

        auto arrayOfApis = JsonArrayUtil::getArrayOfStringsFromMemberInValue (value, "apis");
        for (std::string api :  arrayOfApis) {
                creative->apis->insert(std::make_pair(StringUtil::toUpperCase (api), false));
        }

        auto arrayOfAttributes = JsonArrayUtil::getArrayOfStringsFromMemberInValue (value, "attributes");

        for (std::string attribute :  arrayOfAttributes) {
                creative->attributes->insert(std::make_pair(attribute, false));
        }

        return creative;
}

void Creative::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "status", status, value);
        JsonUtil::addMemberToValue_FromPair (doc, "description", description, value);
        JsonUtil::addMemberToValue_FromPair (doc, "advertiserId", advertiserId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "creativeContentType", creativeContentType, value);
        JsonUtil::addMemberToValue_FromPair (doc, "adTag", adTag, value);
        JsonUtil::addMemberToValue_FromPair (doc, "previewUrl", previewUrl, value);
        JsonUtil::addMemberToValue_FromPair (doc, "size", size, value);
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "landingPageUrl", landingPageUrl, value);
        JsonUtil::addMemberToValue_FromPair (doc, "adType", adType, value);
        JsonUtil::addMemberToObject_From_String_Boolean_Pair (doc, "secure", secure, value);

        std::vector<std::string> apisVector;
        for (auto&& attPair : *apis) {
                apisVector.push_back(attPair.first);
        }
        JsonArrayUtil::addMemberToValue_FromPair (doc,"apis", apisVector, value);

        std::vector<std::string> attributesVector;
        for (auto&& attPair : *attributes) {
                attributesVector.push_back(attPair.first);
        }
        JsonArrayUtil::addMemberToValue_FromPair (doc,"attributes", attributesVector, value);

}
std::string Creative::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

Creative::~Creative() {

}



void Creative::validate() {


        // for (auto apiPair, *this->apis) {
        //         if (!StringUtil::equalsCaseSensitive (StringUtil::toUpperCase(apiPair.first), "VPAID1.0") &&
        //             !StringUtil::equalsCaseSensitive (StringUtil::toUpperCase(apiPair.first), "VPAID2.0") &&
        //             !StringUtil::equalsCaseSensitive (StringUtil::toUpperCase(apiPair.first), "MRAID-1") &&
        //             !StringUtil::equalsCaseSensitive (StringUtil::toUpperCase(apiPair.first), "ORMMA") &&
        //             !StringUtil::equalsCaseSensitive (StringUtil::toUpperCase(apiPair.first), "MRAID-2")) {
        //                 throwEx ("unknown api " + StringUtil::toStr (apiPair.first));
        //         }
        // }

}


int Creative::getId() {
        return id;
}


std::string Creative::getName() {
        return name;
}

std::string Creative::getStatus() {
        return status;
}

std::string Creative::getDescription() {
        return description;
}

int Creative::getAdvertiserId() {
        return advertiserId;
}

std::string Creative::getAdTag() {
        return adTag;
}


std::string Creative::getLandingPageUrl() {
        return landingPageUrl;
}

std::string Creative::getPreviewUrl() {
        return previewUrl;
}

std::string Creative::getSize() {
        return size;
}

Poco::DateTime Creative::getCreatedAt() {
        return createdAt;
}

Poco::DateTime Creative::getUpdatedAt() {
        return updatedAt;
}

std::string Creative::getAdType() {
        if (adType.empty ()) {
                throwEx("ad type cannot be empty");
        }

        if (!StringUtil::equalsCaseSensitive (this->adType, StringUtil::toLowerCase ("XHTML_TEXT_AD")) &&
            !StringUtil::equalsCaseSensitive (this->adType, StringUtil::toLowerCase ("XHTML_BANNER_AD")) &&
            !StringUtil::equalsCaseSensitive (this->adType, StringUtil::toLowerCase ("JAVASCRIPT")) &&
            !StringUtil::equalsCaseSensitive (this->adType, StringUtil::toLowerCase ("IFRAME"))) {
                throwEx("ad type is wrong " + StringUtil::toStr (this->adType));
        }
        return adType;
}

bool Creative::IsSecure() {
        return secure;
}


void Creative::setId(int id) {
        this->id = id;
}


void Creative::setName(std::string name) {
        this->name = name;
}

void Creative::setStatus(std::string status) {
        this->status = StringUtil::toLowerCase (status);
}

void Creative::setDescription(std::string description) {
        this->description = description;
}

void Creative::setAdvertiserId(int advertiserId) {
        this->advertiserId = advertiserId;
}

void Creative::setAdTag(std::string adTag) {
        this->adTag = adTag;
}

void Creative::setLandingPageUrl(std::string landingPageUrl) {
        this->landingPageUrl = landingPageUrl;
}

void Creative::setPreviewUrl(std::string previewUrl) {
        this->previewUrl = previewUrl;
}

void Creative::setSize(std::string size) {
        this->size = StringUtil::toLowerCase (size);
}

void Creative::setCreatedAt(Poco::DateTime createdAt) {
        this->createdAt = createdAt;
}

void Creative::setUpdatedAt(Poco::DateTime updatedAt) {
        this->updatedAt = updatedAt;
}


void Creative::setAdType(std::string adType) {
        if (adType.empty ()) {
                throwEx("ad type cannot be empty");
        }
        this->adType = StringUtil::toLowerCase (adType);
}

void Creative::setSecure(bool secure) {
        this->secure = secure;
}
