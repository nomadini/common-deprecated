//
// Created by Mahmoud Taabodi on 2/14/16.
//

#ifndef COMMON_CREATIVECACHESERVICE_MOCK_H
#define COMMON_CREATIVECACHESERVICE_MOCK_H

#include <string>
#include <memory>
#include <unordered_map>

#include "CreativeTypeDefs.h"
class Creative;
class MySqlDriver;
#include "MySqlCreativeService.h"
#include "HttpUtilService.h"
#include "CacheService.h"
#include "CreativeCacheService.h"
class CreativeCacheService;
#include "gmock/gmock.h"

class EntityToModuleStateStats;

class CreativeCacheServiceMock;



class CreativeCacheServiceMock : public CreativeCacheService {

public:
CreativeCacheServiceMock(
        MySqlCreativeService* mySqlCreativeService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName);

MOCK_METHOD1(processReadAll, std::string(std::string));

};

#endif //COMMON_CREATIVECACHESERVICE_H
