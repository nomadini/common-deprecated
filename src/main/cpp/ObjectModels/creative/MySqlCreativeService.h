#ifndef MySqlCreativeService_h
#define MySqlCreativeService_h


#include "Object.h"
#include "CreativeTypeDefs.h"
#include <cppconn/resultset.h>
#include "CreativeTestHelper.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"

class MySqlCreativeService;




class MySqlCreativeService : public DataProvider<Creative>, public Object {

public:

MySqlDriver* driver;

MySqlCreativeService(MySqlDriver* driver);

virtual ~MySqlCreativeService();

void parseTheAttribute(std::shared_ptr<Creative> obj, std::string attributesFromDB);

std::vector<std::shared_ptr<Creative>> readAllCreatives();

void insert(std::shared_ptr<Creative> creative);

std::shared_ptr<Creative> readById(int id);

virtual void deleteAll();

std::shared_ptr<Creative> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::vector<std::shared_ptr<Creative>> readAllEntities();

};

#endif
