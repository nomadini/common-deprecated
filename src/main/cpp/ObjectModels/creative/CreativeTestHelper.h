/*
 * CreativeTestHelper.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_CREATIVETESTHELPER_H_
#define GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_CREATIVETESTHELPER_H_
#include "Object.h"

class StringUtil;

#include "CreativeTypeDefs.h"
class Creative;

class CreativeTestHelper {

public:
static std::shared_ptr<Creative> createSampleCreative();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_CREATIVETESTHELPER_H_ */
