#include "MySqlCreativeService.h"
#include "Creative.h"
#include "MySqlDriver.h"
#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <boost/foreach.hpp>
MySqlCreativeService::MySqlCreativeService(MySqlDriver* driver)  : Object(__FILE__) {
        this->driver = driver;
}


std::shared_ptr<Creative> MySqlCreativeService::readById(int id) {
        std::shared_ptr<Creative> creative = std::make_shared<Creative> ();

        std::string query = "SELECT "
                            " id, "
                            " name,"
                            " advertiser_id,"
                            " description,"
                            " status,"
                            " ad_tag,"
                            " landing_page_url,"
                            " api ,"
                            " ad_type, "
                            " creative_content_type, "
                            " landing_page_url,"
                            " preview_url,"
                            " size,"
                            " is_secure,"
                            " attributes, " //attributes are saved as something like this ["Audio_Ad_Auto_Play", "Audio_Ad_User_Initiated", "Expandable_Automatic"]
                            " advertiser_domain_name,"
                            " created_at,"
                            " updated_at "
                            " FROM creative where id = __ID__";

        query = StringUtil::replaceString (query, "__ID__", StringUtil::toStr(id));


        auto res = driver->executeQuery (query);


        while (res->next ()) {
                creative = mapResultSetToObject(res);
                MLOG(3) << "reading creative from db : " << creative->toJson ();
        }

        assertAndThrow(!creative->getName().empty ());
        return creative;
}

std::vector<std::shared_ptr<Creative> > MySqlCreativeService::readAllEntities() {
        return readAllCreatives();
}

std::vector<std::shared_ptr<Creative> > MySqlCreativeService::readAllCreatives() {

        std::vector<std::shared_ptr<Creative> > allObjects;
        std::string query = "SELECT "
                            " id, "
                            " name,"
                            " advertiser_id,"
                            " description,"
                            " status,"
                            " ad_tag,"
                            " landing_page_url,"
                            " api ,"
                            " ad_type, "
                            " creative_content_type, "
                            " preview_url,"
                            " size,"
                            " is_secure,"
                            " attributes, "              //attributes are saved as something like this ["Audio_Ad_Auto_Play", "Audio_Ad_User_Initiated", "Expandable_Automatic"]
                            " advertiser_domain_name,"
                            " created_at,"
                            " updated_at "
                            " FROM creative ";


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                std::shared_ptr<Creative> obj = mapResultSetToObject(res);
                MLOG(3) << "reading creative : " << obj->toJson();         // getInt(1) returns the first column

                allObjects.push_back (obj);

        }

        return allObjects;

}

std::shared_ptr<Creative> MySqlCreativeService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {

        std::shared_ptr<Creative> obj = std::make_shared<Creative> ();
        try {
                obj->setId(res->getInt ("id"));
                obj->setName(res->getString ("name"));
                obj->setAdvertiserId(res->getInt ("advertiser_id"));
                obj->setDescription(res->getString ("description"));
                obj->setStatus(res->getString ("status"));
                obj->setAdTag(res->getString ("ad_tag"));
                obj->setLandingPageUrl(res->getString ("landing_page_url"));
                std::string apisStr(res->getString ("api"));
                auto adType = res->getString ("ad_type");
                if (StringUtil::equalsIgnoreCase(adType, Creative::AD_TYPE_IFRAME)) {
                        obj->setAdType(Creative::AD_TYPE_IFRAME);
                } else if (StringUtil::equalsIgnoreCase(adType, Creative::AD_TYPE_JAVASCRIPT)) {
                        obj->setAdType(Creative::AD_TYPE_JAVASCRIPT);
                } else {
                        throwEx("wrong adType : " + adType);
                }

                auto creativeContentType = res->getString ("creative_content_type");
                if (StringUtil::equalsIgnoreCase(creativeContentType, Creative::CONTENT_TYPE_DISPLAY)) {
                        obj->creativeContentType = Creative::CONTENT_TYPE_DISPLAY;
                } else if (StringUtil::equalsIgnoreCase(creativeContentType, Creative::CONTENT_TYPE_VIDEO)) {
                        obj->creativeContentType = Creative::CONTENT_TYPE_VIDEO;
                } else {
                        throwEx("wrong creativeContentType : " + creativeContentType);
                }

                obj->setPreviewUrl(res->getString ("preview_url"));
                obj->setSize(res->getString ("size"));
                std::string secure(res->getString ("is_secure"));
                std::string attributesFromDB = MySqlDriver::getString( res, "attributes");

                std::string advertiserDomainNames = MySqlDriver::getString( res, "advertiser_domain_name"); //this might not be set
                if(!advertiserDomainNames.empty()) {
                        auto domains = StringUtil::tokenizeString(advertiserDomainNames, ",");
                        for (auto domain : domains) {
                                obj->advertiserDomainNames.push_back(domain);
                        }
                }

                obj->setCreatedAt(DateTimeUtil::parseDateTime(res->getString ("created_at")));
                obj->setUpdatedAt(DateTimeUtil::parseDateTime(res->getString ("updated_at")));

                if (StringUtil::equalsIgnoreCase (secure, "true")) {
                        obj->setSecure(true);
                } else if (StringUtil::equalsIgnoreCase (secure, "false")) {
                        throwEx("bad value for secure field : " + secure);
                } else {
                        obj->setSecure(false);
                }


                parseTheAttribute (obj, attributesFromDB);

                MLOG(3)<<"reading apis : " << apisStr;
                std::vector<std::string> apiVector;
                if (StringUtil::equalsIgnoreCase(apisStr, "null") ||
                    apisStr.empty()) {
                        apisStr = "[]";
                }
                JsonArrayUtil::getArrayOfObjectsFromJsonString (apisStr, apiVector);


                for(std::string api :  apiVector) {
                        obj->apis->insert(std::make_pair(StringUtil::toUpperCase (api), false));
                }


                obj->validate ();
        } catch(...) {
                gicapods::Util::showStackTrace();
                throwEx("error in converting result set to creative : id : " + StringUtil::toStr(obj->getId()));
        }
        return obj;
}

void MySqlCreativeService::deleteAll() {
        driver->deleteAll("creative");
}

void MySqlCreativeService::parseTheAttribute(std::shared_ptr<Creative> obj, std::string attributesFromDB) {
        if (attributesFromDB.empty () || StringUtil::equalsIgnoreCase(attributesFromDB, "null")) {
                attributesFromDB = "[]";
        }
        MLOG(3) << "attributesFromDB read from db : " << attributesFromDB;
        std::vector<std::string> allAttributes;
        JsonArrayUtil::getArrayOfObjectsFromJsonString (attributesFromDB, allAttributes);
        MLOG(3) << "these are crt attributes id :  " << obj->getId() << ", attr : "
                << JsonArrayUtil::convertListToJson (allAttributes);
        for (std::string attribute : allAttributes) {
                obj->attributes->insert(std::make_pair(attribute, false));
        }
}

void MySqlCreativeService::insert(std::shared_ptr<Creative> creative) {

        std::string queryStr =
                " INSERT INTO creative"
                " ("
                " name,"
                " status,"
                " description,"
                " advertiser_id,"
                " landing_page_url,"
                " preview_url,"
                " size,"
                " is_secure,"
                " attributes,"
                " api,"
                " ad_type, "
                " creative_content_type, "
                " ad_tag,"
                " created_at,"
                " updated_at "
                " )"
                " VALUES"
                " ("
                " '__NAME__',"
                " '__STATUS__',"
                " '__DESCR__',"
                " __ADVERTISER_ID__,"
                " '__LANDING_PAGE_URL__',"
                " '__PREVIEW_URL__',"
                " '__SIZE__',"
                " '__IS_SECURE__',"
                " '__ATTRIBUTES__',"
                " '__api__',"
                " '__ad_type__',"
                " '__creative_content_type__',"
                " '__ad_tag__',"
                " '__created_at__',"
                " '__updated_at__');";

        MLOG(3) << "inserting new creative in db : " << creative->toJson ();
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();

        queryStr = StringUtil::replaceString (queryStr, "__NAME__",
                                              creative->getName());
        queryStr = StringUtil::replaceString (queryStr, "__STATUS__",
                                              creative->getStatus());
        queryStr = StringUtil::replaceString (queryStr, "__DESCR__",
                                              creative->getDescription());

        queryStr = StringUtil::replaceString (queryStr, "__ADVERTISER_ID__",
                                              StringUtil::toStr (creative->getAdvertiserId()));


        creative->setAdTag(StringUtil::replaceString (creative->getAdTag(), "'", "\\'"));

        queryStr = StringUtil::replaceString (queryStr, "__ad_tag__",
                                              StringUtil::toStr (creative->getAdTag()));


        queryStr = StringUtil::replaceString (queryStr, "__LANDING_PAGE_URL__",
                                              StringUtil::toStr (creative->getLandingPageUrl()));
        queryStr = StringUtil::replaceString (queryStr, "__PREVIEW_URL__",
                                              StringUtil::toStr (creative->getPreviewUrl()));

        queryStr = StringUtil::replaceString (queryStr, "__SIZE__",
                                              StringUtil::toStr (creative->getSize()));

        queryStr = StringUtil::replaceString (queryStr, "__DESCR__",
                                              creative->getDescription());
        queryStr = StringUtil::replaceString (queryStr, "__IS_SECURE__",
                                              StringUtil::toStr (creative->IsSecure ()));


        queryStr = StringUtil::replaceString (queryStr, "__ATTRIBUTES__",
                                              JsonArrayUtil::convertListToJson (CollectionUtil::getMapKeySet<std::string, bool>(*creative->attributes)));

        queryStr = StringUtil::replaceString (queryStr, "__api__",
                                              JsonArrayUtil::convertListToJson (CollectionUtil::getMapKeySet<std::string, bool>(*creative->apis)));

        queryStr = StringUtil::replaceString (queryStr, "__ad_type__", creative->getAdType());
        queryStr = StringUtil::replaceString (queryStr, "__creative_content_type", creative->creativeContentType);

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        driver->executedUpdateStatement (queryStr);
        creative->setId(driver->getLastInsertedId ());

}

MySqlCreativeService::~MySqlCreativeService() {
}
