#ifndef Creative_H
#define Creative_H



#include "Object.h"
#include <unordered_map>
#include <memory>
#include <string>
#include <vector>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "CreativeTypeDefs.h"

class Creative : public Object {

private:

public:

static std::string CONTENT_TYPE_DISPLAY;
static std::string CONTENT_TYPE_VIDEO;

static std::string AD_TYPE_JAVASCRIPT;
static std::string AD_TYPE_IFRAME;

int id;   //keep this public because of CacheService

std::string name;
int advertiserId;
std::string status;
std::string description;

std::string adTag;
std::string landingPageUrl;
std::string previewUrl;
std::string size;

Poco::DateTime createdAt;
Poco::DateTime updatedAt;

std::vector<std::string> advertiserDomainNames;

std::shared_ptr<std::unordered_map<std::string, bool> > apis;
std::shared_ptr<std::unordered_map<std::string, bool> > attributes;    //these are attributes in english names based that we show in UI
std::string adType;
std::string creativeContentType;
bool secure;

static std::vector<std::string> validApis;

Creative();

std::string toString();

std::string toJson();

virtual ~Creative();

void validate();

static std::string getEntityName();
int getId();
std::string getName();
std::string getStatus();
std::string getDescription();
int getAdvertiserId();
std::string getAdTag();
std::string getLandingPageUrl();
std::string getPreviewUrl();
std::string getSize();
Poco::DateTime getCreatedAt();
Poco::DateTime getUpdatedAt();
std::string getAdType();
bool IsSecure();


void setId(int id);
void setName(std::string name);
void setStatus(std::string status);
void setDescription(std::string descr);
void setAdvertiserId(int advertiserId);
void setAdTag(std::string adTag);
void setLandingPageUrl(std::string landingPageUrl);
void setPreviewUrl(std::string previewUrl);
void setSize(std::string size);
void setCreatedAt(Poco::DateTime dateCreated);
void setUpdatedAt(Poco::DateTime dateModified);

void setAdType(std::string adType);
void setSecure(bool secure);

static std::shared_ptr<Creative> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};


#endif
