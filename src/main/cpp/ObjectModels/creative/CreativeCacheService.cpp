//
// Created by Mahmoud Taabodi on 2/14/16.
//

#include "CreativeCacheService.h"
#include "MySqlCreativeService.h"
#include <boost/foreach.hpp>
#include "HttpUtilService.h"
#include "JsonUtil.h"
#include <vector>
#include "JsonArrayUtil.h"
#include "Creative.h"
#include "MySqlCreativeService.h"
#include "HttpUtilService.h"
#include "CacheService.h"
CreativeCacheService::CreativeCacheService(
        MySqlCreativeService* mySqlCreativeService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<Creative>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        allCreatives = std::make_shared<std::vector<std::shared_ptr<Creative> > > ();
}


void CreativeCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<Creative> > allCreatives) {
        for(std::shared_ptr<Creative> crt :  allCreatives) {
                getAllCreatives ()->push_back (crt);
        }
}

void CreativeCacheService::clearOtherCaches() {
        getAllCreatives ()->clear ();

}

std::vector<std::string> CreativeCacheService::getListOfSizes(std::vector<std::shared_ptr<Creative> > list) {
        std::vector<std::string> allSizes;
        for (auto crt : list) {
                allSizes.push_back (crt->getSize());
        }
        return allSizes;
}

std::shared_ptr<std::vector<std::shared_ptr<Creative> > > CreativeCacheService::getAllCreatives() {
        return allCreatives;
}
