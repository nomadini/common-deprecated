/*
 * MySqlCompressableEntityTraits.h
 *
 *  Created on: Jan 15, 2017
 *      Author: mtaabodi
 */

#ifndef MySqlCompressableEntityTraits_H_
#define MySqlCompressableEntityTraits_H_

#include "GUtil.h"
#include "StringUtil.h"
#include "MetricDbRecord.h"
template <class T>
class MySqlCompressableEntityTraits
{

public:

std::string getTableName();
std::string getCompressedTableName();
std::string getCoreFieldsToRead();
std::string getCoreFieldsToInsert();
std::string getGroupByFields();
std::string getCoreReplacementFields();
};

template <>
class MySqlCompressableEntityTraits<MetricDbRecord> {
public:
std::string getTableName() {
        return "metric";
}
std::string getCompressedTableName() {
        return "metric_compressed";
}

std::string getCoreReplacementFields() {
        return " '__hostname__', "
               " '__appname__', "
               " '__appInstanceUniqueName__',"
               " '__value__', "
               " '__module__', "
               " '__state__',"
               " '__entity__',"
               " '__level__',"
               " '__domain__',"
               " '__created_at__',"
               " '__updated_at__',"
               " '__timeOfRecord__'";


}
std::string getGroupByFields() {
        return " hostname, "
               " appname, "
               " appInstanceUniqueName,"
               " module, "
               " state,"
               " entity,"
               " domain,"
               " level ";
}
std::string getCoreFieldsToInsert() {
        return " `hostname`, "
               " `appname`, "
               " `appInstanceUniqueName`,"
               " `value`, "
               " `module`, "
               " `state`,"
               " `entity`,"
               " `level`,"
               " `domain`,"
               " `created_at`,"
               " `updated_at`,"
               " `timeOfRecord` ";


}
std::string getCoreFieldsToRead() {
        return " `hostname`, "
               " `appname`, "
               " `appInstanceUniqueName`,"
               " sum(value) as `value`, "
               " `module`, "
               " `state`,"
               " `entity`,"
               " `level`,"
               " `domain`,"
               " MAX(`created_at`) AS `created_at`,"
               " MAX(`updated_at`) AS `updated_at`,"
               " MAX(`timeOfRecord`) AS `timeOfRecord` ";
}
};

#endif
