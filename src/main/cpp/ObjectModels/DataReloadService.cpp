//
//
// Created by Mahmoud Taabodi on 7/15/16.
//

#include "DataReloadService.h"
#include <boost/exception/all.hpp>
#include <mutex>  // For std::unique_lock
#include <boost/thread.hpp>
#include <thread>

#include "CollectionUtil.h"
#include "CacheService.h"


DataReloadService::DataReloadService(
        std::vector<CacheServiceInterface*> allCacheServices,
        std::vector<EntityProviderServiceInterface*> allEntityProviderService
        )  : Object(__FILE__) {
        this->allCacheServices = allCacheServices;
        this->allEntityProviderService = allEntityProviderService;
}

DataReloadService::~DataReloadService() {

}


void DataReloadService::reloadDataViaHttp() {
        MLOG(10) << "start of relaoding data...locking mutex";

        try {

                for (auto entitFetcherService : allCacheServices) {
                        //TODO : add an item in dashboard about when this thread was called, last time ,
                        // and how many data we have in bidder now
                        if (entitFetcherService != nullptr) {

                                if (!entitiesToReload.empty()) {
                                        if (!CollectionUtil::containsInList(
                                                    entitFetcherService->getEntityName(),
                                                    entitiesToReload
                                                    )) {
                                                //skipping the unwanted entity
                                                MLOG(1)<<"skipping the unwanted entity: "<<
                                                        entitFetcherService->getEntityName();
                                                continue;
                                        } else {
                                                MLOG(1)<<"reloading caches via http : "<<
                                                        entitFetcherService->getEntityName();

                                                entitFetcherService->reloadCachesViaHttp ();
                                        }
                                } else {
                                        MLOG(1)<<"reloading caches via http : "<<
                                                entitFetcherService->getEntityName();

                                        entitFetcherService->reloadCachesViaHttp ();
                                }

                        }
                }



                isReloadProcessHealthy->setValue(true);
        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity("ERROR_IN_LOADING_DATA_VIA_HTTP",
                                                                  "DataReloadService",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::exception);
                gicapods::Util::showStackTrace();
                LOG(ERROR) << "error happening when reloading caches  " << boost::diagnostic_information (e)
                           << "clearing all caches now";
                isReloadProcessHealthy->setValue(false);
                for (auto entitFetcherService : allCacheServices) {
                        entitFetcherService->clearCaches();
                        entitFetcherService->clearOtherCaches();
                }
        }
}


void DataReloadService::reloadData() {

        try {
                for (auto entityToProviderService : allEntityProviderService) {
                        //TODO : add an item in dashboard about when this thread was called, last time ,
                        // and how many data we have in bidder now
                        if (entityToProviderService != nullptr) {
                                MLOG(1)<<"reloading caches directly : "<< entityToProviderService->getEntityName();
                                entityToProviderService->reloadCaches ();
                        }
                }

                isReloadProcessHealthy->setValue(true);
        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity("ERROR_IN_LOADING_DATA_FROM_DB",
                                                                  "DataReloadService",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::exception);

                gicapods::Util::showStackTrace();
                LOG(ERROR) << "error happening when reloading caches  " << boost::diagnostic_information (e)
                           << "clearing all caches now";
                isReloadProcessHealthy->setValue(false);
                for (auto entityToProviderService : allEntityProviderService) {
                        entityToProviderService->clearCaches();
                }
        }
}


void DataReloadService::thread() {

        assertAndThrow(interruptedFlag != nullptr);
        assertAndThrow(reloadCachesIntervalInSecond > 10);
        while (!interruptedFlag->getValue()) {
                try {
                        isReloadingDataInProgress = true;
                        reloadDataViaHttp();
                } catch (std::exception const&  e) {
                        gicapods::Util::showStackTrace();
                        LOG(ERROR) << "error happening when reloadCaches  " << boost::diagnostic_information (e);
                }
                isReloadingDataInProgress = false;
                MLOG (1) << "sleeping for : " << reloadCachesIntervalInSecond << " seconds";
                gicapods::Util::sleepViaBoost (_L_, reloadCachesIntervalInSecond);
        }
        isReloadingDataInProgress = false;
}

void DataReloadService::startThread() {
        std::thread startReloadingCacheThread (&DataReloadService::thread, this);
        startReloadingCacheThread.detach ();
}



std::string DataReloadService::getName() {
        return "DataReloadService";
}
