#ifndef STATUS_H
#define STATUS_H




#define STOP_PROCESSING 100
#define CONTINUE_PROCESSING 110

#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Object.h"
namespace gicapods {
class Status {

private:

public:
int value;
std::string updatedAt;
std::string getDescription();
std::string getUpdatedAt();
Status();
virtual ~Status();

};


}
#endif
