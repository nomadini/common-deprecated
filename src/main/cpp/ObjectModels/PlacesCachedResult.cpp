
#include "GUtil.h"
#include "PlacesCachedResult.h"
#include "JsonArrayUtil.h"
#include "JsonUtil.h"
PlacesCachedResult::PlacesCachedResult(
								std::string mgrs10m,
								int maxNumebrOfWantedFeatures) {
								this->mgrs10m = mgrs10m;
								this->maxNumebrOfWantedFeatures = maxNumebrOfWantedFeatures;
}

PlacesCachedResult::~PlacesCachedResult() {
}


std::string PlacesCachedResult::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}

void PlacesCachedResult::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair (doc, "mgrs10m", mgrs10m, value);
								JsonUtil::addMemberToValue_FromPair (doc, "maxNumebrOfWantedFeatures", maxNumebrOfWantedFeatures, value);
								JsonArrayUtil::addMemberToValue_FromPair(doc, "places", places, value);
}

std::shared_ptr<PlacesCachedResult> PlacesCachedResult::fromJsonValue(RapidJsonValueType value) {

								std::shared_ptr<PlacesCachedResult> result = std::make_shared<PlacesCachedResult>(
																JsonUtil::GetStringSafely(value, "mgrs10m"),
																JsonUtil::GetIntSafely(value, "maxNumebrOfWantedFeatures"));


								JsonArrayUtil::getArrayFromValueMemeber(value, "places", result->places);
								return result;
}

std::shared_ptr<PlacesCachedResult> PlacesCachedResult::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}
