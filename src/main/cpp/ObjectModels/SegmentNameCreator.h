/*
 * SegmentNameCreator.h
 *
 *  Created on: Aug 1, 2015
 *      Author: mtaabodi
 */

#ifndef SegmentNameCreator_H_
#define SegmentNameCreator_H_

#include <memory>
#include <string>
class SegmentNameCreator;

#include "Object.h"
class SegmentNameCreator : public Object {

public:

SegmentNameCreator();

virtual ~SegmentNameCreator();

static std::string createName(
								std::string segmentSeedName,
								std::string segmentLevel,
								std::string segmentType);
};



#endif /* SegmentNameCreator_H_ */
