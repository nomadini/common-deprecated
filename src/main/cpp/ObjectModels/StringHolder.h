#ifndef StringHolder_h
#define StringHolder_h


#include <atomic>
#include <memory>
#include <string>
#include "Object.h"
#include "AtomicLong.h"
#include "JsonTypeDefs.h"
//use AtomicLong instead of this class
class StringHolder;

class StringHolder : public Object {

public:

std::string value;

StringHolder();
virtual ~StringHolder();

std::string toJson();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

static std::shared_ptr<StringHolder> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<StringHolder> fromJson(std::string json);
};
#endif
