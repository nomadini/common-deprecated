#ifndef CampaignCacheService_h
#define CampaignCacheService_h


#include "CampaignTypeDefs.h"

#include <string>
#include <memory>
#include <unordered_map>

class TargetGroupCacheService;
#include "Object.h"
class CampaignCacheService;
class MySqlDriver;
class MySqlCampaignService;
#include "HttpUtilService.h"
#include "CacheService.h"
#include "CampaignTypeDefs.h"
#include "EntityProviderService.h"
class EntityToModuleStateStats;

class CampaignCacheService;



class CampaignCacheService : public CacheService<Campaign> {



public:

TargetGroupCacheService* targetGroupCacheService;

CampaignCacheService(MySqlCampaignService* mySqlCampaignService,
                     TargetGroupCacheService* targetGroupCacheService,
                     HttpUtilService* httpUtilService,
                     std::string dataMasterUrl,
                     EntityToModuleStateStats* entityToModuleStateStats,std::string appName);

void addCampaignToCache(std::shared_ptr<Campaign> cmp);

std::shared_ptr<Campaign> findCampaignByTgId(int id);

double getCampaignCpmByTgId(int targetGroupId);

};


#endif
