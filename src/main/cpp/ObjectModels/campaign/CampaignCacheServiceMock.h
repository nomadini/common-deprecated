#ifndef CampaignCacheServiceMock_h
#define CampaignCacheServiceMock_h

class CampaignCacheService;
#include "gmock/gmock.h"
#include "CampaignCacheService.h"
class EntityToModuleStateStats;

class CampaignCacheServiceMock;



class CampaignCacheServiceMock : public CampaignCacheService {

public:

CampaignCacheServiceMock(MySqlCampaignService* mySqlCampaignService,
                         TargetGroupCacheService* targetGroupCacheService,
                         HttpUtilService* httpUtilService,
                         std::string dataMasterUrl,
                         EntityToModuleStateStats* entityToModuleStateStats,std::string appName);
MOCK_METHOD1(processReadAll, std::string(std::string));
};


#endif
