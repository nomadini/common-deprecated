#include "GUtil.h"
#include "MySqlCampaignOfferService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"
#include "CampaignOffer.h"
MySqlCampaignOfferService::MySqlCampaignOfferService(MySqlDriver* driver) :
        MySqlService(driver, nullptr),Object(__FILE__) {
        this->driver = driver;
        allFields =
                " `id`, "
                " `campaign_id`, "
                " `offer_id`, "
                " `created_at`,"
                " `updated_at` ";
}

std::vector<std::shared_ptr<CampaignOffer> > MySqlCampaignOfferService::readAllEntities() {
        return this->readAll();
}
std::string MySqlCampaignOfferService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     + allFields +
                                                     " FROM `campaign_offer_map`";
        return selectAllQueryStatement;
}

std::string MySqlCampaignOfferService::getInsertObjectSqlStatement(std::shared_ptr<CampaignOffer> campaign) {
        std::string queryStr =
                " INSERT INTO `campaign_offer_map`"
                " ("
                + allFields +
                ")"
                " VALUES"
                " ( "
                " __campaign_id__,"
                " __offer_id__,"
                " '__created_at__',"
                " '__updated_at__');";

        queryStr = StringUtil::replaceString (queryStr, "__campaign_id__", _toStr(campaign->campaignId));
        queryStr = StringUtil::replaceString (queryStr, "__offer_id__", _toStr(campaign->offerId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);
        return queryStr;
}

std::shared_ptr<CampaignOffer> MySqlCampaignOfferService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<CampaignOffer> obj = std::make_shared<CampaignOffer> ();
        obj->id  = res->getInt ("id");
        obj->campaignId  = res->getInt ("campaign_id");
        obj->offerId  = res->getInt ("offer_id");
        obj->createdAt = DateTimeUtil::parseDateTime(res->getString ("created_at"));
        obj->updatedAt = DateTimeUtil::parseDateTime(res->getString ("updated_at"));

        MLOG(3) << "reading campaign_offer_map from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlCampaignOfferService::getReadByIdSqlStatement(int id) {
        std::string query = "SELECT "
                            " `id`,"
                            + allFields +
                            " FROM campaign_offer_map where id = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

void MySqlCampaignOfferService::deleteAll() {
        driver->deleteAll("campaign_offer_map");
}

MySqlCampaignOfferService::~MySqlCampaignOfferService() {
}
