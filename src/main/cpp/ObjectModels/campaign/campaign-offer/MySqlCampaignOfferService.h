//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlCampaignOfferService_h
#define MySqlCampaignOfferService_h

#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlService.h"
#include "Object.h"
class CampaignOffer;
class MySqlCampaignOfferService;


class MySqlCampaignOfferService :
        public DataProvider<CampaignOffer>, public MySqlService<int, CampaignOffer>, public Object {

public:

MySqlDriver* driver;

MySqlCampaignOfferService(MySqlDriver* driver);
virtual ~MySqlCampaignOfferService();

std::string getSelectAllQueryStatement();
std::string allFields;
std::shared_ptr<CampaignOffer> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<CampaignOffer> campaign);
std::string getReadByIdSqlStatement(int id);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<CampaignOffer> > readAllEntities();
virtual void deleteAll();
};

#endif
