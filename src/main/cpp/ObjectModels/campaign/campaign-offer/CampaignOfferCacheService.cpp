#include "CampaignOffer.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "TargetGroupCacheService.h"
#include "CampaignOfferCacheService.h"
#include "TargetGroup.h"
#include "MySqlCampaignOfferService.h"
#include "JsonArrayUtil.h"
#include "CampaignOffer.h"

CampaignOfferCacheService::CampaignOfferCacheService(
        MySqlCampaignOfferService* mySqlCampaignOfferService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<CampaignOffer>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {
}
