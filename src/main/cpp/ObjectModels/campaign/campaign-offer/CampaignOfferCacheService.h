#ifndef CampaignOfferCacheService_h
#define CampaignOfferCacheService_h

#include <string>
#include <memory>
#include <unordered_map>

class CampaignOfferCacheService;
class MySqlDriver;
class MySqlCampaignOfferService;
#include "HttpUtilService.h"
#include "CacheService.h"
class CampaignOffer;
class EntityToModuleStateStats;
#include "Object.h"
#include "EntityProviderService.h"
class CampaignOfferCacheService;



class CampaignOfferCacheService : public CacheService<CampaignOffer>, public Object {
public:

CampaignOfferCacheService(MySqlCampaignOfferService* mySqlCampaignOfferService,
                          HttpUtilService* httpUtilService,
                          std::string dataMasterUrl,
                          EntityToModuleStateStats* entityToModuleStateStats,std::string appName);
};


#endif
