/*
 * CampaignTestHelper.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_CAMPAIGNTESTHELPER_H_
#define GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_CAMPAIGNTESTHELPER_H_

#include "Object.h"
#include "CampaignTypeDefs.h"
#include "TestsCommon.h"
class CampaignTestHelper {

public:
CampaignTestHelper();
static std::shared_ptr<Campaign> createSampleCampaign();
static void thenBothCampaignsAreEqual(std::shared_ptr<Campaign> actualCampaign, std::shared_ptr<Campaign> expectedCampaign);
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_CAMPAIGNTESTHELPER_H_ */
