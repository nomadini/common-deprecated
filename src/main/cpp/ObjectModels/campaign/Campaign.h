#ifndef Campaign_h
#define Campaign_h


class StringUtil;
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "Object.h"
#include "CampaignTypeDefs.h"
class AdvertiserCostModel;
class Campaign : public Object {

private:
public:
std::string name;
int advertiserId;
std::string description;
std::string status;
long maxImpression;
long dailyMaxImpression;
double maxBudget;
double dailyMaxBudget;
double cpm;
std::shared_ptr<AdvertiserCostModel> advCostModel;
std::shared_ptr<AdvertiserCostModel> platformCostModel;
Poco::DateTime startDate;
Poco::DateTime endDate;

Poco::DateTime createdAt;
Poco::DateTime updatedAt;


int id;   //keep this public because of CacheService

static std::string getEntityName();
Campaign();

void validate();

std::string toString();

std::string toJson();

virtual ~Campaign();


int getId();
std::string getSlug();
std::string getName();
std::string getStatus();
std::string getDescription();
int getAdvertiserId();

long getMaxImpression();
long getDailyMaxImpression();
double getMaxBudget();
double getDailyMaxBudget();
double getCpm();

Poco::DateTime getStartDate();
Poco::DateTime getEndDate();

Poco::DateTime getCreatedAt();
Poco::DateTime getUpdatedAt();


void setId(int id);
void setSlug(std::string slug);
void setName(std::string name);
void setStatus(std::string status);
void setDescription(std::string description);
void setAdvertiserId(int advertiserId);
void setMaxImpression(long maxImpression);
void setDailyMaxImpression(long dailyMaxImpression);
void setMaxBudget(double maxBudget);
void setDailyMaxBudget(double dailyMaxBudget);
void setCpm(double cpm);

void setStartDate(Poco::DateTime startDate);
void setEndDate(Poco::DateTime endDate);

void setCreatedAt(Poco::DateTime dateCreated);
void setUpdatedAt(Poco::DateTime dateModified);

static std::shared_ptr<Campaign> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
