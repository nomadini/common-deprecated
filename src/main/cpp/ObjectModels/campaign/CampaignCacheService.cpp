#include "Campaign.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "TargetGroupCacheService.h"
#include "CampaignCacheService.h"
#include "TargetGroup.h"
#include "MySqlCampaignService.h"
#include "JsonArrayUtil.h"

CampaignCacheService::CampaignCacheService(
        MySqlCampaignService* mySqlCampaignService,
        TargetGroupCacheService* targetGroupCacheService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<Campaign>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {
        NULL_CHECK(targetGroupCacheService);

        this->targetGroupCacheService = targetGroupCacheService;
}

std::shared_ptr<Campaign> CampaignCacheService::findCampaignByTgId(int id) {
        auto tg = targetGroupCacheService->findByEntityId (id);

        auto campaignPtr = findByEntityId(tg->getCampaignId());
        return campaignPtr;
}

double CampaignCacheService::getCampaignCpmByTgId(int targetGroupId) {
        auto campaign = findCampaignByTgId(targetGroupId);
        return campaign->getCpm();
}
