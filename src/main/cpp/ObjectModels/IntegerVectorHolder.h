#ifndef IntegerVectorHolder_h
#define IntegerVectorHolder_h


#include <atomic>
#include <memory>
#include <string>
#include "Object.h"
#include "AtomicLong.h"
//use AtomicLong instead of this class
class IntegerVectorHolder : public Object {

public:

std::shared_ptr<std::vector<int> >  values;

IntegerVectorHolder ();
virtual ~IntegerVectorHolder();
};
#endif
