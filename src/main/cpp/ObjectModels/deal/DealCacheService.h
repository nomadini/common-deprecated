#ifndef DealCacheService_h
#define DealCacheService_h


#include "Deal.h"
#include "Object.h"
#include <string>
#include <memory>
#include <unordered_map>

class MySqlDriver;
#include "MySqlDealService.h"
#include "HttpUtilService.h"
#include "Deal.h"
#include "CacheService.h"
class EntityToModuleStateStats;

class DealCacheService;



class DealCacheService : public CacheService<Deal>, public Object {

public:
MySqlDealService* mySqlDealService;
EntityToModuleStateStats* entityToModuleStateStats;

DealCacheService(MySqlDealService* mySqlDealService,
                 HttpUtilService* httpUtilService,
                 std::string dataMasterUrl,
                 EntityToModuleStateStats* entityToModuleStateStats,
                 std::string appName);

virtual ~DealCacheService();
};


#endif
