#include "Deal.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

Deal::Deal() : Object(__FILE__) {
								id = 0;
								exchangeId = 0;

}

std::string Deal::toString() {
								return toJson();

}
std::string Deal::getEntityName() {
								return "Deal";
}
std::shared_ptr<Deal> Deal::fromJson(std::string jsonString) {
								std::shared_ptr<Deal> deal = std::make_shared<Deal>();
								auto document = parseJsonSafely(jsonString);
								deal->id  = JsonUtil::GetIntSafely(*document, "id");
								deal->exchangeId  = JsonUtil::GetIntSafely(*document, "exchangeId");
								deal->tpDealId  = JsonUtil::GetStringSafely(*document, "tpDealId");
								deal->description  = JsonUtil::GetStringSafely(*document, "description");
								deal->createdAt  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(*document, "createdAt"));
								return deal;
}

std::shared_ptr<Deal> Deal::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<Deal> deal = std::make_shared<Deal>();
								deal->id  = JsonUtil::GetIntSafely(value, "id");
								deal->exchangeId  = JsonUtil::GetIntSafely(value, "exchangeId");
								deal->tpDealId  = JsonUtil::GetStringSafely(value, "tpDealId");
								deal->description  = JsonUtil::GetStringSafely(value, "description");
								deal->createdAt  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "createdAt"));
								return deal;
}

void Deal::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
								JsonUtil::addMemberToValue_FromPair(doc, "exchangeId", exchangeId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "tpDealId", tpDealId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "description", description, value);
								JsonUtil::addMemberToValue_FromPair(doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
}

std::string Deal::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);

}

Deal::~Deal() {

}
