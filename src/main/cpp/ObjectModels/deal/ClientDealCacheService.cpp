#include "ClientDeal.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "TargetGroupCacheService.h"
#include "ClientDealCacheService.h"
#include "MySqlClientDealService.h"
#include "JsonArrayUtil.h"
#include "MySqlClientDealService.h"
#include "HttpUtilService.h"
#include "ClientDeal.h"
ClientDealCacheService::ClientDealCacheService(MySqlClientDealService* mySqlClientDealService,
                                               HttpUtilService* httpUtilService,
                                               std::string dataMasterUrl,
                                               EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<ClientDeal>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),

        Object(__FILE__) {
}


ClientDealCacheService::~ClientDealCacheService() {

}
