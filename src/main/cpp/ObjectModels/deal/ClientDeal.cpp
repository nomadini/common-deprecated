#include "ClientDeal.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"


ClientDeal::ClientDeal()  : Object(__FILE__) {
								id = 0;
								dealId = 0;
								clientId = 0;
}

std::string ClientDeal::toString() {
								return toJson();

}
std::string ClientDeal::getEntityName() {
								return "ClientDeal";
}
std::shared_ptr<ClientDeal> ClientDeal::fromJson(std::string jsonString) {
								std::shared_ptr<ClientDeal> deal = std::make_shared<ClientDeal>();
								auto document = parseJsonSafely(jsonString);
								deal->id  = JsonUtil::GetIntSafely(*document, "id");
								deal->dealId  = JsonUtil::GetIntSafely(*document, "dealId");
								deal->clientId  = JsonUtil::GetIntSafely(*document, "clientId");
								deal->description  = JsonUtil::GetStringSafely(*document, "description");
								deal->status  = JsonUtil::GetStringSafely(*document, "status");
								deal->createdAt  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(*document, "createdAt"));
								deal->lastModified  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(*document, "lastModified"));
								return deal;
}
std::shared_ptr<ClientDeal> ClientDeal::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<ClientDeal> deal = std::make_shared<ClientDeal>();
								deal->id  = JsonUtil::GetIntSafely(value, "id");
								deal->dealId  = JsonUtil::GetIntSafely(value, "dealId");
								deal->clientId  = JsonUtil::GetIntSafely(value, "clientId");
								deal->description  = JsonUtil::GetStringSafely(value, "description");
								deal->status  = JsonUtil::GetStringSafely(value, "status");
								deal->createdAt  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "createdAt"));
								deal->lastModified  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "lastModified"));
								return deal;
}

void ClientDeal::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
								JsonUtil::addMemberToValue_FromPair(doc, "dealId", dealId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "clientId", clientId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "description", description, value);
								JsonUtil::addMemberToValue_FromPair(doc, "status", status, value);
								JsonUtil::addMemberToValue_FromPair(doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
								JsonUtil::addMemberToValue_FromPair(doc, "lastModified", DateTimeUtil::dateTimeToStr(lastModified), value);
}

std::string ClientDeal::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);

}

ClientDeal::~ClientDeal() {

}
