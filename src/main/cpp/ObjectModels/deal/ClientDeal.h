/*
 * ClientDeal.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef ClientDeal_H_
#define ClientDeal_H_

#include "Object.h"


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include "JsonTypeDefs.h"
#include "Poco/DateTime.h"

class ClientDeal : public Object {

public:


int id;
int dealId;
int clientId;
std::string description;
std::string status;
Poco::DateTime createdAt;
Poco::DateTime lastModified;

ClientDeal();

std::string toString();

std::string toJson();

static std::shared_ptr<ClientDeal> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

static std::shared_ptr<ClientDeal> fromJson(std::string jsonString);

static std::string getEntityName();
virtual ~ClientDeal();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
