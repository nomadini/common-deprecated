//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlClientDealService_h
#define MySqlClientDealService_h


#include "Object.h"

#include "ClientDeal.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlService.h"

class MySqlClientDealService;


class MySqlClientDealService : public DataProvider<ClientDeal>,
        public MySqlService<int, ClientDeal>, public Object {

public:

MySqlDriver* driver;

MySqlClientDealService(MySqlDriver* driver);

virtual ~MySqlClientDealService();

std::string getSelectAllQueryStatement();
std::shared_ptr<ClientDeal> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<ClientDeal> campaign);
std::string getReadByIdSqlStatement(int id);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<ClientDeal>> readAllEntities();
virtual void deleteAll();
};

#endif
