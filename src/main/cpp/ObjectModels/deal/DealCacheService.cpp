#include "Deal.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "TargetGroupCacheService.h"
#include "DealCacheService.h"
#include "MySqlDealService.h"
#include "JsonArrayUtil.h"

DealCacheService::DealCacheService(MySqlDealService* mySqlDealService,
                                   HttpUtilService* httpUtilService,
                                   std::string dataMasterUrl,
                                   EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<Deal>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__)
{

}

DealCacheService::~DealCacheService() {

}
