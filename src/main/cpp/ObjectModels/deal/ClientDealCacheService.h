#ifndef ClientDealCacheService_h
#define ClientDealCacheService_h


#include "Object.h"
#include <string>
#include <memory>
#include <unordered_map>

class MySqlDriver;
class MySqlClientDealService;
class HttpUtilService;
class ClientDeal;
#include "EntityProviderService.h"
#include "CacheService.h"
class EntityToModuleStateStats;

class ClientDealCacheService;



class ClientDealCacheService : public CacheService<ClientDeal>, public Object {

public:

ClientDealCacheService(MySqlClientDealService* mySqlClientDealService,
                       HttpUtilService* httpUtilService,
                       std::string dataMasterUrl,
                       EntityToModuleStateStats* entityToModuleStateStats,std::string appName);

virtual ~ClientDealCacheService();
};


#endif
