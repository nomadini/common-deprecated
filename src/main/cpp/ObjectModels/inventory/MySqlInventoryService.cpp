#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlInventoryService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "Inventory.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"
MySqlInventoryService::MySqlInventoryService(MySqlDriver* driver,
                                             HttpUtilService* httpUtilService,
                                             std::string dataMasterUrl) : Object(__FILE__){
        this->driver = driver;
        this->dataMasterUrl = dataMasterUrl;
        this->httpUtilService = httpUtilService;
        this->allInventoryMap = std::make_shared<std::unordered_map<int, std::shared_ptr<Inventory> > > ();

}

MySqlInventoryService::~MySqlInventoryService() {

}


std::shared_ptr<std::unordered_map<int, std::shared_ptr<Inventory> > > MySqlInventoryService::readAll() {
        std::shared_ptr<std::unordered_map<int, std::shared_ptr<Inventory> > > inventoryMap =
                std::make_shared<std::unordered_map<int, std::shared_ptr<Inventory> > > ();

        std::string query = "SELECT "
                            "`inventory`.`ID`,"
                            " `inventory`.`NAME`,"
                            " `inventory`.`CATEGORY`,"
                            " `inventory`.`TYPE`,"
                            " `inventory`.`DAILY_LIMIT`,"
                            " `inventory`.`STATUS`"
                            " FROM `inventory`;";


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                MLOG(3) << "inventoryMap obj loaded : id  " << res->getString (1);         // getInt(1) returns the first column
                std::shared_ptr<Inventory> inv  = std::make_shared<Inventory> ();

                inv->id = res->getInt (1);

                inv->name = MySqlDriver::getString( res, 2);
                inv->category = MySqlDriver::getString( res, 3);
                inv->type = MySqlDriver::getString( res, 4);
                inv->dailyLimit = res->getInt (5);
                inv->status = MySqlDriver::getString( res, 6);
                inventoryMap->insert (std::make_pair (inv->id, inv));
        }

        return inventoryMap;
}

std::shared_ptr<Inventory> MySqlInventoryService::read(int id) {
        throwEx("error in mysql");
}

void MySqlInventoryService::update(std::shared_ptr<Inventory> obj) {

}

void MySqlInventoryService::insert(std::shared_ptr<Inventory> obj) {

        MLOG(3) << "inserting new Inventory in db : " << obj->toJson ();
        std::string queryStr =
                "INSERT INTO inventory"
                " ("
                " NAME,"
                " STATUS,"
                " TYPE,"
                " CATEGORY,"
                " DAILY_LIMIT,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '__NAME__',"
                " '__STATUS__',"
                " '__TYPE__',"
                " '__CATEGORY__',"
                " '__DAILY_LIMIT__',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__NAME__",
                                              obj->name);
        queryStr = StringUtil::replaceString (queryStr, "__STATUS__",
                                              obj->status);
        queryStr = StringUtil::replaceString (queryStr, "__TYPE__", obj->type);
        queryStr = StringUtil::replaceString (queryStr, "__CATEGORY__", obj->category);
        queryStr = StringUtil::replaceString (queryStr, "__DAILY_LIMIT__", StringUtil::toStr (obj->dailyLimit));
        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        obj->id = driver->getLastInsertedId ();


}

void MySqlInventoryService::deleteAll() {
        driver->deleteAll("inventory");
}


std::shared_ptr<std::unordered_map<int, std::shared_ptr<Inventory> > > MySqlInventoryService::getAllInventoryMap() {
        return allInventoryMap;
}

void MySqlInventoryService::addInventory(std::shared_ptr<Inventory> inv) {
        MySqlInventoryService::getAllInventoryMap ()->insert (std::make_pair (inv->id, inv));

}
