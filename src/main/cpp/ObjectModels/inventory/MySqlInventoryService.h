#ifndef MySqlInventoryService_h
#define MySqlInventoryService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "Inventory.h"
#include "HttpUtilService.h"
#include "Object.h"
class MySqlInventoryService;




class MySqlInventoryService : public Object {

public:
std::shared_ptr<std::unordered_map<int, std::shared_ptr<Inventory>> > allInventoryMap;

MySqlDriver* driver;

std::string dataMasterUrl;

HttpUtilService* httpUtilService;


MySqlInventoryService(MySqlDriver* driver,
                      HttpUtilService* httpUtilService,
                      std::string dataMasterUrl);

std::shared_ptr<std::unordered_map<int, std::shared_ptr<Inventory>>> readAll();

std::shared_ptr<Inventory> read(int id);

void update(std::shared_ptr<Inventory> obj);

void insert(std::shared_ptr<Inventory> obj);

virtual void deleteAll();

virtual ~MySqlInventoryService();

std::shared_ptr<std::unordered_map<int, std::shared_ptr<Inventory>> > getAllInventoryMap();

void addInventory(std::shared_ptr<Inventory> inv);
};


#endif
