#ifndef Inventory_h
#define Inventory_h


#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "Object.h"
class Inventory;





class Inventory : public Object {


public:
int id;
std::string name;
std::string status;
std::string type;
std::string category;
//active or inactive
int dailyLimit;    //this can be used to give advertisers
//ability to say that I want to run this many impressions
//on this inventory maximum

std::string dateCreated;
std::string dateModified;

std::string toJson();
Inventory();
virtual ~Inventory();

};

#endif
