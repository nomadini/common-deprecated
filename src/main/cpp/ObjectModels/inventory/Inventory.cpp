#include "GUtil.h"
#include "JsonUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Inventory.h"

Inventory::Inventory() : Object(__FILE__) {
}
std::string Inventory::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "name", name, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "type", type, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "status", status, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "dailyLimit", dailyLimit, value);
        return JsonUtil::valueToString (value);
}
