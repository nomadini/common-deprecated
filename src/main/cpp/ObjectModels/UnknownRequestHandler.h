#ifndef UnknownRequestHandler_H
#define UnknownRequestHandler_H

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"

#include "SignalHandler.h"
class EntityToModuleStateStats;


class UnknownRequestHandler : public Poco::Net::HTTPRequestHandler {

public:
EntityToModuleStateStats* entityToModuleStateStats;
UnknownRequestHandler(EntityToModuleStateStats* entityToModuleStateStats);


void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response);
};
#endif
