#include "GUtil.h"

#include "MySqlFeatureRegistryService.h"
#include "MySqlDriver.h"
#include "Feature.h"
#include "MySqlFeatureUnderReviewService.h"
MySqlFeatureRegistryService::MySqlFeatureRegistryService(
        MySqlDriver* driver,
        EntityToModuleStateStats* entityToModuleStateStats)  : Object(__FILE__) {
        this->driver = driver;
}


std::vector<std::shared_ptr<Feature> > MySqlFeatureRegistryService::readAllFeatures(std::string timeUpdated) {
        std::vector<std::shared_ptr<Feature> > allFeatures;
        std::string query =
                "SELECT id,"
                " `name`,"
                " `type`, "
                " `status`, "
                " `iab_category` from feature_registry where updated_at > '__updated_at__'";
        query = StringUtil::replaceString (query, "__updated_at__", timeUpdated);
        auto res = driver->executeQuery (query);

        while (res->next ()) {
                allFeatures.push_back(MySqlFeatureUnderReviewService::mapResultSetToObjectStatic(res));
        }

        return allFeatures;
}

void MySqlFeatureRegistryService::writeFeatures(std::vector<std::shared_ptr<Feature> > allFeaturesToWrite) {
        if(allFeaturesToWrite.empty()) {
                return;
        }

        std::string queryStr =
                "INSERT INTO feature_registry"
                " ("
                " `name`,"
                " `type`, "
                " `status`, "
                " `iab_category`"
                " ) VALUES __VALUES_OF_ALLROW__  ;";


        std::vector<std::string> allValues;
        for(auto feature :  allFeaturesToWrite) {
                auto oneRecordValues = convertRecordToValueString(feature);
                allValues.push_back(oneRecordValues);
                // MLOG(3) << "oneRecordValues : " << oneRecordValues;
        }

        auto properAllValues = StringUtil::joinArrayAsString(allValues, ",");
        // MLOG(3) << "properAllValues : " << properAllValues;
        queryStr = StringUtil::replaceString (queryStr, "__VALUES_OF_ALLROW__", properAllValues);

        driver->executedUpdateStatement (queryStr);
}


std::string MySqlFeatureRegistryService::convertRecordToValueString(std::shared_ptr<Feature> feature) {

        std::string oneRowValue =
                " "
                " ("
                " '__name__',"
                " '__type__', "
                " '__status__', "
                " '__iab_category__'"
                " ) ";


        oneRowValue = StringUtil::replaceString (oneRowValue, "__name__", feature->name);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__type__", feature->type);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__status__", feature->status);

        // oneRowValue = StringUtil::replaceString (oneRowValue, "__created_at__", DateTimeUtil::dateTimeToStr(metricDbRecord->createdAt));
        // oneRowValue = StringUtil::replaceString (oneRowValue, "__updated_at__", DateTimeUtil::dateTimeToStr(metricDbRecord->updatedAt));

        return oneRowValue;
}

MySqlFeatureRegistryService::~MySqlFeatureRegistryService() {
}
