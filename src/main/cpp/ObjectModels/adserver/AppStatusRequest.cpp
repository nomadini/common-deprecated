#include "GUtil.h"
#include "AppStatusRequest.h"
#include "JsonUtil.h"


AppStatusRequest::AppStatusRequest()   : Object(__FILE__) {
}

std::string AppStatusRequest::toJson() {
        std::string json;
        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "status", status, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "healthDetails", healthDetails, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "reason", reason, value);

        return JsonUtil::valueToString(value);

}

std::shared_ptr<AppStatusRequest> AppStatusRequest::fromJson(std::string json) {

        auto doc = parseJsonSafely(json);

        std::shared_ptr<AppStatusRequest> request = std::make_shared<AppStatusRequest>();
        /*
           its valid that status and reason are not present for request, and not the responses
         */
        if (doc->HasMember("status")) {
                request->status = JsonUtil::GetStringSafely(*doc, "status");
        }
        if (doc->HasMember("reason")) {
                request->reason = JsonUtil::GetStringSafely(*doc, "reason");
        }
        if (doc->HasMember("healthDetails")) {
                request->healthDetails = JsonUtil::GetStringSafely(*doc, "healthDetails");
        }

        return request;
}

AppStatusRequest::~AppStatusRequest() {
}
