
#include "GUtil.h"
#include "AdServerStatus.h"
#include "JsonUtil.h"


AdServerStatus::AdServerStatus()  : Object(__FILE__) {
        numberOfImpressionsShownInLastMinute = 0;

        numberOfClicksInLastMinute = 0;
}

std::string AdServerStatus::toJson() {
        std::string json;
        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "numberOfImpressionsShownInLastMinute",
                                            numberOfImpressionsShownInLastMinute, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "numberOfClicksInLastMinute",
                                            numberOfClicksInLastMinute, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "lastTimeImpressionWasShown",
                                            lastTimeImpressionWasShown, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "lastTimeARequestWasProcessed",
                                            lastTimeARequestWasProcessed, value);


        MLOG(3) <<"AdServerStatus in json :  " << JsonUtil::valueToString(value);
        return JsonUtil::valueToString(value);

}

std::shared_ptr<AdServerStatus> AdServerStatus::fromJson(std::string json) {

        auto doc = parseJsonSafely(json);
        std::shared_ptr<AdServerStatus> thiss (new AdServerStatus ());
        if (doc->HasMember ("numberOfImpressionsShownInLastMinute")) {
                thiss->numberOfImpressionsShownInLastMinute = JsonUtil::GetIntSafely (*doc, "numberOfImpressionsShownInLastMinute");
        }
        if (doc->HasMember ("numberOfClicksInLastMinute")) {
                thiss->numberOfClicksInLastMinute = JsonUtil::GetIntSafely (*doc, "numberOfClicksInLastMinute");
        }

        if (doc->HasMember ("lastTimeImpressionWasShown")) {
                thiss->lastTimeImpressionWasShown = JsonUtil::GetStringSafely (*doc, "lastTimeImpressionWasShown");
        }
        if (doc->HasMember ("lastTimeARequestWasProcessed")) {
                thiss->lastTimeARequestWasProcessed = JsonUtil::GetStringSafely (*doc, "lastTimeARequestWasProcessed");
        }
        return thiss;
}

AdServerStatus::~AdServerStatus() {
}
