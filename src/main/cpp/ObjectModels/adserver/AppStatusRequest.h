/*
 * AppStatusRequest.h
 *
 *  Created on: Aug 24, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_BIDDER_AdServerStatusENGINE_AdServerStatusREQUEST_H_
#define GICAPODS_GICAPODSSERVER_SRC_BIDDER_AdServerStatusENGINE_AdServerStatusREQUEST_H_


#include <memory>
#include <string>
#include "Object.h"

/**
 * a AdServerStatus request has the realtime info of an entity (for now we only have tg realtime)
 * plus, the entityId and entityType
 *
 */
class AppStatusRequest;



class AppStatusRequest : public Object {

public:
std::string reason;

std::string status;

std::string healthDetails;

AppStatusRequest();

std::string toJson();

static std::shared_ptr<AppStatusRequest> fromJson(std::string json);

virtual ~AppStatusRequest();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_AdServerStatusENGINE_AdServerStatusREQUEST_H_ */
