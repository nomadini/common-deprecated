//
// Created by Mahmoud Taabodi on 7/15/16.
//

#ifndef BIDDER_DATARELOADSERVICE_H
#define BIDDER_DATARELOADSERVICE_H

#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
class EntityToModuleStateStats;

#include "Status.h"
#include "EntityProviderService.h"
#include "CacheService.h"
#include "AtomicBoolean.h"
class BeanFactory;
#include <boost/thread.hpp>
#include <thread>

class DataReloadService;


class DataReloadService : public Object {

public:
int reloadCachesIntervalInSecond;
std::shared_ptr<gicapods::AtomicBoolean> interruptedFlag;
std::shared_ptr<gicapods::AtomicBoolean> isReloadProcessHealthy;

std::vector<std::string> entitiesToReload;

bool isReloadingDataInProgress;
EntityToModuleStateStats* entityToModuleStateStats;
std::vector<CacheServiceInterface*> allCacheServices;
std::vector<EntityProviderServiceInterface*> allEntityProviderService;
DataReloadService(
        std::vector<CacheServiceInterface*> allCacheServices,
        std::vector<EntityProviderServiceInterface*> allEntityProviderService
        );
virtual ~DataReloadService();

void reloadData();
void reloadDataViaHttp();

std::string getName();


void thread();
void startThread();
private:

};
#endif //BIDDER_DATARELOADSERVICE_H
