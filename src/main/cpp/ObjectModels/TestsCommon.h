//
// Created by Mahmoud Taabodi on 2/13/16.
//
#ifndef TestsCommon_H
#define TestsCommon_H

#include "gmock/gmock.h"
#include "gtest/gtest.h"

using ::testing::NiceMock;
using ::testing::AtLeast;
using ::testing::Return;
using ::testing::AtLeast;
using ::testing::Eq;
using ::testing::Mock;

using ::testing::AllOf;
using ::testing::Field;
using ::testing::Pointee;
using ::testing::PrintToString;
using ::testing::ElementsAre;
using ::testing::Matcher;
using ::testing::AnyOf;

using testing::_;
using testing::A;
using testing::AllOf;
using testing::AnyOf;
using testing::Assign;
using testing::ContainerEq;
using testing::DoAll;
using testing::DoDefault;
using testing::DoubleEq;
using testing::ElementsAre;
using testing::ElementsAreArray;
using testing::EndsWith;
using testing::Eq;
using testing::Field;
using testing::FloatEq;
using testing::Ge;
using testing::Gt;
using testing::HasSubstr;
using testing::IgnoreResult;
using testing::Invoke;
using testing::InvokeArgument;
using testing::InvokeWithoutArgs;
using testing::IsNull;
using testing::Le;
using testing::Lt;
using testing::Matcher;
using testing::MatcherCast;
using testing::NanSensitiveDoubleEq;
using testing::NanSensitiveFloatEq;
using testing::Ne;
using testing::Not;
using testing::NotNull;
using testing::Pointee;
using testing::Property;
using testing::Ref;
using testing::ResultOf;
using testing::Return;
using testing::ReturnNull;
using testing::ReturnRef;
using testing::SetArgPointee;
using testing::SetArrayArgument;
using testing::StartsWith;
using testing::StrCaseEq;
using testing::StrCaseNe;
using testing::StrEq;
using testing::StrNe;
using testing::Truly;
using testing::TypedEq;
using testing::WithArg;
using testing::WithArgs;
using testing::WithoutArgs;
using testing::Throw;
#endif //COMMON_TESTSCOMMON_H_H
