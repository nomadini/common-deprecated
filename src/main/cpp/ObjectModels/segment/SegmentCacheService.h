#ifndef SegmentCacheService_h
#define SegmentCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include <boost/foreach.hpp>
#include "HttpUtilService.h"
#include "EntityProviderService.h"
#include "CacheService.h"
#include "Object.h"
class EntityToModuleStateStats;
class SegmentCacheService;

namespace gicapods {
class ConfigService;
}


class SegmentCacheService : public CacheService<Segment>, public Object {

private:

public:

std::shared_ptr <std::unordered_map<std::string, std::shared_ptr<Segment> > > allNameToSegmentsMap;

EntityToModuleStateStats* entityToModuleStateStats;

SegmentCacheService(HttpUtilService* httpUtilService,
                    std::string dataMasterUrl,
                    EntityToModuleStateStats* entityToModuleStateStats,
                    std::string appName);

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<Segment> > allEntities);

static std::shared_ptr<SegmentCacheService> getInstance(gicapods::ConfigService* configService);
};


#endif
