#include "GUtil.h"
#include "OfferSegment.h"
#include "MySqlOfferSegmentService.h"
#include "MySqlDriver.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "ConcurrentQueueFolly.h"
#include "ConcurrentHashMap.h"
#include "MySqlModelService.h"

std::string MySqlOfferSegmentService::coreSelectSql =
        "SELECT "
        "`offer_segment_map`.`id`,"
        "`offer_segment_map`.`offer_id`,"
        "`offer_segment_map`.`segment_id`";


MySqlOfferSegmentService::MySqlOfferSegmentService(
        MySqlDriver* driver) : Object(__FILE__) {

        this->driver = driver;
}

std::shared_ptr<OfferSegment>
MySqlOfferSegmentService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<OfferSegment> segment = std::make_shared<OfferSegment>();
        int id = res->getInt("id");
        int offerId = res->getInt("offer_id");
        int segmentId = res->getInt("segment_id");

        segment->id = id;
        segment->offerId = offerId;
        segment->segmentId = segmentId;
        return segment;
}

std::vector<std::shared_ptr<OfferSegment> > MySqlOfferSegmentService::readAllEntities() {
        return this->readAll();
}

std::vector <std::shared_ptr<OfferSegment> > MySqlOfferSegmentService::readAll() {

        std::vector <std::shared_ptr<OfferSegment> > segments;
        std::string query = coreSelectSql +
                            "FROM `offer_segment_map`";

        auto res = driver->executeQuery(query);

        while (res->next()) {

                auto segment = mapResultSetToObject(res);
                segments.push_back(segment);
        }



        return segments;
}

MySqlOfferSegmentService::~MySqlOfferSegmentService() {

}
