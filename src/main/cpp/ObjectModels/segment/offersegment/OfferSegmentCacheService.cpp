
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "OfferSegmentCacheService.h"
#include "SegmentCacheService.h"
#include "JsonArrayUtil.h"

OfferSegmentCacheService::OfferSegmentCacheService(HttpUtilService* httpUtilService,
                                                   std::string dataMasterUrl,
                                                   EntityToModuleStateStats* entityToModuleStateStats,
                                                   std::string appName)
        : CacheService<OfferSegment>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName), Object(__FILE__) {
        this->offerIdToSegments =
                std::make_shared<std::unordered_map<int, std::shared_ptr<std::vector<std::shared_ptr<Segment> > > > >();
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void OfferSegmentCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<OfferSegment> > offerSegments) {

        for(std::shared_ptr<OfferSegment> of : offerSegments) {

                auto pair = offerIdToSegments->find(of->segmentId);
                auto segment = segmentCacheService->findByEntityId(of->segmentId);

                if (pair == offerIdToSegments->end()) {
                        auto vector = std::make_shared<std::vector<std::shared_ptr<Segment> > >();
                        vector->push_back(segment);
                        offerIdToSegments->insert (
                                std::make_pair (of->offerId,
                                                vector));

                } else {
                        pair->second->push_back(segment);
                }
        }
}

void OfferSegmentCacheService::clearOtherCaches() {
        MLOG(10) << "clearing OfferSegmentCacheService ";
        this->offerIdToSegments->clear ();
}
