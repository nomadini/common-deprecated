#ifndef OfferSegment_H
#define OfferSegment_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "Object.h"
class OfferSegment : public Object {

public:
int id;
int offerId;
int segmentId;

OfferSegment();

std::string toJson();
static std::string getEntityName();

virtual ~OfferSegment();

static std::shared_ptr<OfferSegment> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};



#endif
