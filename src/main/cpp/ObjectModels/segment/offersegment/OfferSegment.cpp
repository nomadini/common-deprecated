#include "OfferSegment.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>


OfferSegment::OfferSegment()  : Object(__FILE__) {
        id = 0;
        offerId = 0;
        segmentId = 0;
}

void OfferSegment::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "offerId", offerId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "segmentId", segmentId, value);
}

std::string OfferSegment::getEntityName() {
        return "OfferSegment";
}

std::shared_ptr<OfferSegment> OfferSegment::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<OfferSegment> offerSegment =
                std::make_shared<OfferSegment>();
        offerSegment->id = JsonUtil::GetIntSafely(value, "id");
        offerSegment->offerId  =JsonUtil::GetIntSafely(value,"offerId");
        offerSegment->segmentId = JsonUtil::GetIntSafely(value,"segmentId");

        return offerSegment;
}

std::string OfferSegment::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

OfferSegment::~OfferSegment() {

}
