#ifndef SegmentGroup_H
#define SegmentGroup_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "Object.h"
class SegmentGroup : public Object {

private:

public:
std::string name;
std::string descr;

int id;
int advertiserId;

SegmentGroup();

std::string toString();

std::string toJson();

static std::string getEntityName();

virtual ~SegmentGroup();

static std::shared_ptr<SegmentGroup> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};



#endif
