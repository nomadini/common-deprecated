#include "SegmentGroup.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>



SegmentGroup::SegmentGroup()  : Object(__FILE__) {
        id = 0;
        advertiserId = 0;

}

std::string SegmentGroup::toString() {
        return toJson ();
}

void SegmentGroup::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "advertiserId", advertiserId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "descr", descr, value);
}

std::shared_ptr<SegmentGroup> SegmentGroup::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<SegmentGroup> sg = std::make_shared<SegmentGroup>(
                );
        sg->id = JsonUtil::GetIntSafely(value, "id");
        sg->advertiserId  =JsonUtil::GetIntSafely(value,"advertiserId");
        sg->name = JsonUtil::GetStringSafely(value,"name");
        sg->descr = JsonUtil::GetStringSafely(value,"descr");
        return sg;
}

std::string SegmentGroup::getEntityName() {
        return "SegmentGroup";
}
std::string SegmentGroup::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

SegmentGroup::~SegmentGroup() {

}
