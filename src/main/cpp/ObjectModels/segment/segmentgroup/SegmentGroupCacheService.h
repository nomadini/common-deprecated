#ifndef SegmentGroupCacheService_h
#define SegmentGroupCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include <boost/foreach.hpp>
#include "HttpUtilService.h"
#include "EntityProviderService.h"
#include "CacheService.h"
#include "SegmentGroup.h"
#include "Object.h"
class EntityToModuleStateStats;
class SegmentGroupCacheService;

namespace gicapods {
class ConfigService;
}


class SegmentGroupCacheService : public CacheService<SegmentGroup>, public Object {

private:

public:

std::shared_ptr <std::unordered_map<std::string, std::shared_ptr<SegmentGroup> > > allNameToSegmentGroupsMap;

EntityToModuleStateStats* entityToModuleStateStats;

SegmentGroupCacheService(HttpUtilService* httpUtilService,
                         std::string dataMasterUrl,
                         EntityToModuleStateStats* entityToModuleStateStats,
                         std::string appName);

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<SegmentGroup> > allEntities);

static std::shared_ptr<SegmentGroupCacheService> getInstance(gicapods::ConfigService* configService);
};


#endif
