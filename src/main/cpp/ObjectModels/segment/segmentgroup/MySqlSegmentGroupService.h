#ifndef MySqlSegmentGroupService_h
#define MySqlSegmentGroupService_h




#include "SegmentGroup.h"
#include <cppconn/resultset.h>
#include <tbb/concurrent_hash_map.h>
#include "DataProvider.h"
#include "MySqlService.h"
class EntityToModuleStateStats;
class MySqlDriver;
#include "Object.h"
namespace gicapods {
template<class K, class V> class ConcurrentHashMap;
class ConfigService;
}

class MySqlSegmentGroupService : public DataProvider<SegmentGroup>, public Object {

public:

static std::string coreSelectSql;

MySqlDriver* driver;
EntityToModuleStateStats* entityToModuleStateStats;

MySqlSegmentGroupService(MySqlDriver* driver,
                         EntityToModuleStateStats* entityToModuleStateStatsArg);

std::shared_ptr<SegmentGroup> readSegmentGroup(std::string name,
                                               int advertiserId);

std::vector<std::shared_ptr<SegmentGroup> > readAll();

virtual ~MySqlSegmentGroupService();

void insertIfNotExisting(std::shared_ptr<SegmentGroup> segment);

bool segmentExists(std::string name,
                   int advertiserId);


void update(std::shared_ptr<SegmentGroup> segment);

void insert(std::shared_ptr<SegmentGroup> segment);

void deleteAll();

std::shared_ptr<SegmentGroup> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);

std::vector<std::shared_ptr<SegmentGroup> > readAllEntities();

static std::shared_ptr<MySqlSegmentGroupService> getInstance(gicapods::ConfigService* configService);
};


#endif
