#ifndef Segment_H
#define Segment_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "Object.h"
class Segment : public Object {

private:
std::string uniqueName;    //this is the unique name that each segment is defined by
std::string type;

public:

static constexpr const char* ActionTakerSegmentName = "ActionTaker";
static constexpr const char* ModelBasedSegmentName= "ModelBased";
static constexpr const char* CrossWalkedSegmentType= "ModelBased-CrossWalked";
bool valid;
int id;
int modelId;
int advertiserId;
long dateCreatedInMillis;

Segment(long dateCreatedInMillis);

std::string toString();

std::string toJson();

void validate();

std::string getUniqueName();
std::string getType();
static std::string getEntityName();

void setUniqueName(std::string uniqueName);
void setType(std::string type);

virtual ~Segment();

static std::shared_ptr<Segment> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};



#endif
