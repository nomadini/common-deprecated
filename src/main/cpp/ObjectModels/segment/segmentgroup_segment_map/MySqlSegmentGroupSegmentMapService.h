#ifndef MySqlSegmentGroupSegmentMapService_h
#define MySqlSegmentGroupSegmentMapService_h




#include "SegmentGroupSegmentMap.h"
#include <cppconn/resultset.h>
#include <tbb/concurrent_hash_map.h>
#include "DataProvider.h"
#include "MySqlService.h"
class EntityToModuleStateStats;
class MySqlDriver;
#include "Object.h"
namespace gicapods {
template<class K, class V> class ConcurrentHashMap;
class ConfigService;
}

class MySqlSegmentGroupSegmentMapService : public DataProvider<SegmentGroupSegmentMap>, public Object {

public:

static std::string coreSelectSql;

MySqlDriver* driver;
EntityToModuleStateStats* entityToModuleStateStats;

MySqlSegmentGroupSegmentMapService(MySqlDriver* driver,
                                   EntityToModuleStateStats* entityToModuleStateStatsArg);

std::vector<std::shared_ptr<SegmentGroupSegmentMap> > readAll();

virtual ~MySqlSegmentGroupSegmentMapService();

void insert(std::shared_ptr<SegmentGroupSegmentMap> segment);

void deleteAll();

std::shared_ptr<SegmentGroupSegmentMap> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);

std::vector<std::shared_ptr<SegmentGroupSegmentMap> > readAllEntities();

static std::shared_ptr<MySqlSegmentGroupSegmentMapService> getInstance(gicapods::ConfigService* configService);
};


#endif
