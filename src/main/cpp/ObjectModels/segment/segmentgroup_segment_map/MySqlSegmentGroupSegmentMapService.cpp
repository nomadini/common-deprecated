#include "GUtil.h"
#include "SegmentGroupSegmentMap.h"
#include "MySqlSegmentGroupSegmentMapService.h"
#include "MySqlDriver.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "ConcurrentQueueFolly.h"
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashMap.h"
#include "ConfigService.h"
#include "MySqlDriver.h"
#include "MySqlModelService.h"

std::string MySqlSegmentGroupSegmentMapService::coreSelectSql =
        "SELECT `id`,"
        "`segment_id`,"
        "`segment_group_id`";


MySqlSegmentGroupSegmentMapService::MySqlSegmentGroupSegmentMapService(MySqlDriver* driver,
                                                                       EntityToModuleStateStats* entityToModuleStateStatsArg) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStatsArg;
        this->driver = driver;
}


std::shared_ptr<SegmentGroupSegmentMap> MySqlSegmentGroupSegmentMapService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<SegmentGroupSegmentMap> segment = std::make_shared<SegmentGroupSegmentMap>();
        int id = res->getInt("id");
        int segmentId = res->getInt("segment_id");
        int segmentGroupId = res->getInt("segment_group_id");

        MLOG(3) << "values loaded for segment id :  " << id << ","
                " segmentId :  " << segmentId << ", "
                " segmentGroupId :  " << segmentGroupId;

        segment->id = id;
        segment->segmentId = segmentId;
        segment->segmentGroupId = segmentGroupId;

        return segment;
}

std::vector<std::shared_ptr<SegmentGroupSegmentMap> > MySqlSegmentGroupSegmentMapService::readAllEntities() {
        return this->readAll();
}

std::vector <std::shared_ptr<SegmentGroupSegmentMap> > MySqlSegmentGroupSegmentMapService::readAll() {

        std::vector <std::shared_ptr<SegmentGroupSegmentMap> > segments;
        std::string query = coreSelectSql +
                            "FROM `segment_group_to_segment_map`";


        auto res = driver->executeQuery(query);

        while (res->next()) {

                auto segment = mapResultSetToObject(res);
                segments.push_back(segment);
        }



        return segments;
}


void MySqlSegmentGroupSegmentMapService::insert(std::shared_ptr<SegmentGroupSegmentMap> segment) {
        std::string queryStr =
                "INSERT INTO segment_group_to_segment_map"
                "( "
                " segment_id,"
                " segment_group_id"
                " )  "
                "VALUES"
                "("
                " __segment_id__,"
                " __segment_group_id__"
                ");";

        MLOG(3) << "inserting new segment in db : " << segment->toJson();

        queryStr = StringUtil::replaceString(queryStr, "__segment_id__",
                                             StringUtil::toStr(segment->segmentId));
        queryStr = StringUtil::replaceString(queryStr, "__segment_group_id__",
                                             StringUtil::toStr(segment->segmentGroupId));
        MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        segment->id = driver->getLastInsertedId ();

}

void MySqlSegmentGroupSegmentMapService::deleteAll() {
        driver->deleteAll("segment_group_to_segment_map");
}

std::shared_ptr<MySqlSegmentGroupSegmentMapService> MySqlSegmentGroupSegmentMapService::getInstance(
        gicapods::ConfigService* configService
        ) {
        static std::shared_ptr<MySqlSegmentGroupSegmentMapService> ins =
                std::make_shared<MySqlSegmentGroupSegmentMapService>(
                        MySqlDriver::getInstance(configService).get(),
                        EntityToModuleStateStats::getInstance(configService).get()
                        );
        return ins;
}

MySqlSegmentGroupSegmentMapService::~MySqlSegmentGroupSegmentMapService() {

}
