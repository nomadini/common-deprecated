#ifndef SegmentGroupSegmentMapCacheService_h
#define SegmentGroupSegmentMapCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include <boost/foreach.hpp>
#include "HttpUtilService.h"
#include "EntityProviderService.h"
#include "SegmentGroupSegmentMap.h"
#include "CacheService.h"
#include "Object.h"
class EntityToModuleStateStats;
class SegmentGroupSegmentMapCacheService;

namespace gicapods {
class ConfigService;
}


class SegmentGroupSegmentMapCacheService : public CacheService<SegmentGroupSegmentMap>, public Object {

private:

public:

std::shared_ptr <std::unordered_map<std::string, std::shared_ptr<SegmentGroupSegmentMap> > > allNameToSegmentGroupSegmentMapsMap;

EntityToModuleStateStats* entityToModuleStateStats;

SegmentGroupSegmentMapCacheService(HttpUtilService* httpUtilService,
                                   std::string dataMasterUrl,
                                   EntityToModuleStateStats* entityToModuleStateStats,
                                   std::string appName);

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<SegmentGroupSegmentMap> > allEntities);

static std::shared_ptr<SegmentGroupSegmentMapCacheService> getInstance(gicapods::ConfigService* configService);
};


#endif
