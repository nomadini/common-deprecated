
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "SegmentGroupSegmentMapCacheService.h"
#include "ConfigService.h"
#include "HttpUtilService.h"
#include "JsonArrayUtil.h"

SegmentGroupSegmentMapCacheService::SegmentGroupSegmentMapCacheService(HttpUtilService* httpUtilService,
                                                                       std::string dataMasterUrl,
                                                                       EntityToModuleStateStats* entityToModuleStateStats,
                                                                       std::string appName)
        : CacheService<SegmentGroupSegmentMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName), Object(__FILE__) {
        this->allNameToSegmentGroupSegmentMapsMap= std::make_shared<std::unordered_map<std::string, std::shared_ptr<SegmentGroupSegmentMap> > >();
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void SegmentGroupSegmentMapCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<SegmentGroupSegmentMap> > segments) {

        for(std::shared_ptr<SegmentGroupSegmentMap> segment : segments) {
                // allNameToSegmentGroupSegmentMapsMap->insert (std::make_pair (segment->getUniqueName(), segment));

        }
}

void SegmentGroupSegmentMapCacheService::clearOtherCaches() {
        MLOG(10) << "clearing SegmentGroupSegmentMapCacheService ";
        this->allNameToSegmentGroupSegmentMapsMap->clear ();

}

std::shared_ptr<SegmentGroupSegmentMapCacheService>
SegmentGroupSegmentMapCacheService::getInstance(
        gicapods::ConfigService* configService) {
        static auto instance =
                std::make_shared<SegmentGroupSegmentMapCacheService>(
                        HttpUtilService::getInstance(configService).get(),
                        configService->get("dataMasterUrl"),
                        EntityToModuleStateStats::getInstance(configService).get(),
                        configService->get("appName")
                        );

        return instance;
}
