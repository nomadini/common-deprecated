#include "GUtil.h"

#include "SegmentNameCreator.h"
SegmentNameCreator::SegmentNameCreator() : Object(__FILE__) {

}

std::string SegmentNameCreator::createName(
								std::string segmentSeedName,
								std::string segmentLevel,
								std::string segmentType) {

								return "N_" + segmentSeedName +
															"_L" + segmentLevel +
															"_T" + segmentType +
															"_V1";

}

SegmentNameCreator::~SegmentNameCreator() {

}
