#include "GUtil.h"
#include "PocoHttpResponse.h"
#include "JsonMapUtil.h"
#include "HttpUtil.h"
#include "ConfigService.h"
#include "StringUtil.h"

PocoHttpResponse::PocoHttpResponse() {
        statusCode = -1;
}

std::string PocoHttpResponse::getRequiredCookieValue(std::string cookieName) {
        auto pair = responseCookies.find(cookieName);
        if( pair == responseCookies.end()) {
                throwEx(cookieName + " is not found");
        }
        return pair->second;
}

PocoHttpResponse::~PocoHttpResponse() {

}
