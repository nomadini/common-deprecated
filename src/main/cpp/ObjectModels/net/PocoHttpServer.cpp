#include "GUtil.h"
#include "PocoHttpServer.h"
#include "JsonMapUtil.h"
#include "HttpUtil.h"
#include "ConfigService.h"
#include "Poco/ThreadPool.h"
PocoHttpServer::PocoHttpServer() {
}

std::shared_ptr<Poco::Net::HTTPServer>
PocoHttpServer::createHttpServer(
        gicapods::ConfigService* configService,
        Poco::Net::HTTPRequestHandlerFactory* requestHandlerFactory,
        unsigned short port) {
        return createCommonHttpServer(configService, requestHandlerFactory, port);
}

std::shared_ptr<Poco::Net::HTTPServer>
PocoHttpServer::createCommonHttpServer(
        gicapods::ConfigService* configService,
        Poco::Net::HTTPRequestHandlerFactory* requestHandlerFactory,
        unsigned short port) {
        // set-up a server socket
        Poco::Net::ServerSocket svs (port);
        // set-up a HTTPServer instance
        Poco::Net::HTTPServerParams *params = new Poco::Net::HTTPServerParams ();
        params->setKeepAlive(configService->getAsBooleanFromString("httpServerKeepAlive"));

        // int timeoutInSeconds = 10000;
        Poco::Timespan timespan(configService->getAsBooleanFromString("httpServerTimeoutInSeconds"), 0);
        params->setMaxKeepAliveRequests(configService->getAsBooleanFromString("httpServerMaxKeepAliveRequests"));
        params->setKeepAliveTimeout(configService->getAsBooleanFromString("httpServerKeepAliveTimeoutInSeconds"));
        params->setMaxThreads(configService->getAsInt("appServerMaxThreads"));
        params->setMaxQueued(configService->getAsInt("appServerMaxQueued"));
        int minCapacity = configService->getAsInt("appThreadPoolMinCapacity");
        int maxCapacity = configService->getAsInt("appThreadPoolMaxCapacity");
        int idleTime = configService->getAsInt("appThreadPoolIdleTimeInSeconds");
        int stackSize = configService->getAsInt("appThreadPoolStackSize");
        MLOG(10) << "starting entityDeliveryInfoFetcher instance";
        auto threadPool = new Poco::ThreadPool(
                minCapacity,
                maxCapacity,
                idleTime,
                stackSize
                );

        auto httpServer = std::make_shared<Poco::Net::HTTPServer>(requestHandlerFactory, *threadPool, svs, params);
        return httpServer;

}

std::shared_ptr<Poco::Net::HTTPServer>
PocoHttpServer::createHttpServer(
        gicapods::ConfigService* configService,
        Poco::Net::HTTPRequestHandlerFactory* requestHandlerFactory) {

        unsigned short port = (unsigned short) configService->getAsInt("httpPort");
        LOG(ERROR) << "server binding to port " << port;
        return createCommonHttpServer(configService, requestHandlerFactory, port);
}


void PocoHttpServer::printServerInfo(std::shared_ptr<Poco::Net::HTTPServer> server) {
        LOG_EVERY_N(INFO, 1) <<
                "currentConnections : " <<server->currentConnections()<<
                ", currentThreads : " <<server->currentThreads()<<
                ",maxConcurrentConnections : " <<server->maxConcurrentConnections()<<
                ",maxThreads : " <<server->maxThreads()<<
                ",queuedConnections : " <<server->queuedConnections()<<
                ",refusedConnections : " <<server->refusedConnections()<<
                ", totalConnections : " <<server->totalConnections();
}

PocoHttpServer::~PocoHttpServer() {

}
