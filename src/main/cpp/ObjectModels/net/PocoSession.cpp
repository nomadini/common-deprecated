#include "GUtil.h"
#include "PocoSession.h"
#include "JsonMapUtil.h"
#include "HttpUtil.h"
#include "ConfigService.h"
#include "StringUtil.h"

PocoSession::PocoSession(
        std::string nameOfSession,
        bool keepAliveMode,
        int keepAliveTimeoutInMillis,
        int httpSessionTimeoutInMillis,
        std::string hostUrlArg,
        EntityToModuleStateStats* entityToModuleStateStats){
        assertAndThrow(StringUtil::containsCaseInSensitive(hostUrlArg, "http://") ||
                       StringUtil::containsCaseInSensitive(hostUrlArg, "https://"));
        this->httpSessionTimeoutInMillis = httpSessionTimeoutInMillis;
        this->keepAliveTimeoutInMillis = keepAliveTimeoutInMillis;
        this->hostUrl = hostUrlArg;
        this->nameOfSession = nameOfSession;
        this->keepAliveMode = keepAliveMode;
        this->entityToModuleStateStats = entityToModuleStateStats;
        globalSession = initSession();
}

std::shared_ptr<Poco::Net::HTTPClientSession> PocoSession::initSession() {

        // LOG_EVERY_N(INFO, 1000)<< "initSession : hostUrl : "<<hostUrl;
        Poco::URI hostUri(hostUrl);
        Poco::Timespan timespan(0, httpSessionTimeoutInMillis * 1000);

        auto session =
                std::make_shared<Poco::Net::HTTPClientSession> (hostUri.getHost (),
                                                                hostUri.getPort ());

        session->setTimeout(timespan);
        if (keepAliveMode) {

                Poco::Timespan timeoutKeepAliveInSeconds(0, keepAliveTimeoutInMillis * 1000 );
                session->setKeepAlive(keepAliveMode);
                session->setKeepAliveTimeout(timeoutKeepAliveInSeconds);
        }

        return session;
}

std::shared_ptr<PocoHttpResponse> PocoSession::sendRequest(PocoHttpRequest& request) {

        auto session = initSession();
        // auto session = globalSession; using this crashes
        MLOG_EVERY_N(INFO, 1000)<< google::COUNTER<<  "sending request to "<<request.url;
        // LOG_EVERY_N (INFO, 10000) << "request boday :   " << requestBody;
        auto pocoHttpResponse = std::make_shared<PocoHttpResponse>();
        try {
                Poco::Net::HTTPRequest req (Poco::Net::HTTPRequest::HTTP_POST, request.url,
                                            Poco::Net::HTTPMessage::HTTP_1_1);

                //to make keep-alive work , you must set the content-length
                req.setContentLength((int) request.body.length());
                req.setContentType(request.contentType);

                std::ostream &ostr = session->sendRequest (req);
                ostr << request.body.c_str ();

                Poco::Net::HTTPResponse res;
                std::istream &rs = session->receiveResponse (res);
                std::string responseBody ((std::istreambuf_iterator<char> (rs)),
                                          (std::istreambuf_iterator<char> ()));

                MLOG_EVERY_N(INFO, 1000)<< google::COUNTER<< "th resposne from server :  " << responseBody;

                // LOG_EVERY_N(INFO, 1000) << "getKeepAlive() : "<< res.getKeepAlive();
                // assertAndThrow(keepAliveMode == res.getKeepAlive());

                pocoHttpResponse->body = responseBody;
                pocoHttpResponse->statusCode = res.getStatus();

                int responseStatusCode = res.getStatus();
                entityToModuleStateStats->addStateModuleForEntity("ResponseStatus_" + StringUtil::toStr(responseStatusCode),
                                                                  nameOfSession+"PocoSession",
                                                                  EntityToModuleStateStats::all);
        } catch(Poco::Exception& e) {
                MLOG_EVERY_N(INFO, 1000) << google::COUNTER<< "error : "<<e.displayText() + "_errorCode" + StringUtil::toStr(e.code());
                entityToModuleStateStats->addStateModuleForEntity(e.displayText() + "_errorCode" + StringUtil::toStr(e.code()),
                                                                  nameOfSession+"PocoSession",
                                                                  EntityToModuleStateStats::all);
        }

        return pocoHttpResponse;
}

std::string PocoSession::getServerUrl() {
        return hostUrl;
}

void PocoSession::close() {

}

PocoSession::~PocoSession() {

}
