#ifndef PocoSession_H
#define PocoSession_H

#include "Poco/URI.h"
namespace gicapods { class ConfigService; }
#include <memory>
#include <unordered_map>
#include <string>
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Encoder.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/URI.h"
#include "AtomicLong.h"
#include "PocoHttpRequest.h"
#include "PocoHttpResponse.h"
#include "EntityToModuleStateStats.h"

#include <boost/thread.hpp>
class PocoSession {

public:

int httpSessionTimeoutInMillis;
int keepAliveTimeoutInMillis;
bool keepAliveMode;
EntityToModuleStateStats* entityToModuleStateStats;
std::string hostUrl;
std::string nameOfSession;
std::shared_ptr<Poco::Net::HTTPClientSession> globalSession;
PocoSession(
        std::string nameOfSession,
        bool keepAliveMode,
        int keepAliveTimeoutInMillis,
        int httpSessionTimeoutInMillis,
        std::string hostUrlArg,
        EntityToModuleStateStats* entityToModuleStateStats);

std::shared_ptr<PocoHttpResponse> sendRequest(PocoHttpRequest& request);

std::shared_ptr<Poco::Net::HTTPClientSession> initSession();

std::string getServerUrl();
void close();
virtual ~PocoSession();

};

#endif
