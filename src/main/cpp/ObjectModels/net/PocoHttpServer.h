#ifndef PocoHttpServer_H
#define PocoHttpServer_H

#include "Poco/URI.h"
namespace gicapods { class ConfigService; }
#include <memory>
#include <unordered_map>
#include <string>
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Encoder.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/URI.h"
#include "ConfigService.h"
#include "AtomicLong.h"
#include "NomadiniHttpServerInterface.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include <boost/thread.hpp>
#include "Poco/Net/HTTPServerParams.h"

#include "Poco/Net/HTTPRequestHandlerFactory.h"
class PocoHttpServer : public NomadiniHttpServerInterface {

public:

PocoHttpServer();

static std::shared_ptr<Poco::Net::HTTPServer>
createHttpServer(
        gicapods::ConfigService* configService,
        Poco::Net::HTTPRequestHandlerFactory* requestHandlerFactory,
        unsigned short port);

static std::shared_ptr<Poco::Net::HTTPServer>
createHttpServer(gicapods::ConfigService* configService,
                 Poco::Net::HTTPRequestHandlerFactory* requestHandlerFactory);

static std::shared_ptr<Poco::Net::HTTPServer>
createCommonHttpServer(
        gicapods::ConfigService* configService,
        Poco::Net::HTTPRequestHandlerFactory* requestHandlerFactory,
        unsigned short port);

static void printServerInfo(std::shared_ptr<Poco::Net::HTTPServer> server);
virtual ~PocoHttpServer();

};

#endif
