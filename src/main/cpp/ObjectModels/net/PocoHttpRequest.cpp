#include "GUtil.h"
#include "PocoHttpRequest.h"
#include "JsonMapUtil.h"
#include "HttpUtil.h"
#include "ConfigService.h"
#include "StringUtil.h"

std::string PocoHttpRequest::BINARY_CONTENT = "application/octet-stream";
std::string PocoHttpRequest::TEXT_CONTENT = "text/plain";

PocoHttpRequest::PocoHttpRequest(std::string url,
                                 std::string methodType,
                                 std::string contentType,
                                 std::string body,
                                 std::unordered_map<std::string, std::string> requestHeaders){
        this->url = url;
        this->methodType = methodType;
        this->contentType = contentType;
        this->body = body;
        this->requestHeaders = requestHeaders;
}

std::shared_ptr<PocoHttpRequest> PocoHttpRequest::
createSimplePostRequest(std::string url, std::string body)
{
        std::unordered_map<std::string, std::string> requestHeaders;
        auto request = std::make_shared<PocoHttpRequest>(
                url,
                "POST",
                PocoHttpRequest::TEXT_CONTENT,
                body,
                requestHeaders
                );
        return request;
}

PocoHttpRequest::~PocoHttpRequest() {

}
