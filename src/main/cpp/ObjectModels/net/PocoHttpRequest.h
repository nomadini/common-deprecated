#ifndef PocoHttpRequest_H
#define PocoHttpRequest_H

#include "Poco/URI.h"
namespace gicapods { class ConfigService; }
#include <memory>
#include <unordered_map>
#include <string>
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Encoder.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/URI.h"
#include "AtomicLong.h"
#include "EntityToModuleStateStats.h"

#include <boost/thread.hpp>
class PocoHttpRequest {

public:

std::string url;
std::string methodType;
std::string body;
std::string contentType;

static std::string BINARY_CONTENT;
static std::string TEXT_CONTENT;

std::unordered_map<std::string, std::string> requestHeaders;
PocoHttpRequest(std::string url,
                std::string methodType,
                std::string contentType,
                std::string body,
                std::unordered_map<std::string, std::string> requestHeaders);

static std::shared_ptr<PocoHttpRequest>
createSimplePostRequest(std::string url, std::string body);

virtual ~PocoHttpRequest();

};

#endif
