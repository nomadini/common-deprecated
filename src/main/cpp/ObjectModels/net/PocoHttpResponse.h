#ifndef PocoHttpResponse_H
#define PocoHttpResponse_H

#include "Poco/URI.h"
namespace gicapods { class ConfigService; }
#include <memory>
#include <unordered_map>
#include <string>
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Encoder.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/URI.h"
#include "AtomicLong.h"
#include "EntityToModuleStateStats.h"

class PocoHttpResponse {

public:

int statusCode;
std::string body;
std::unordered_map<std::string, std::string> responseHeaders;
std::unordered_map<std::string, std::string> responseCookies;

PocoHttpResponse();
std::string getRequiredCookieValue(std::string cookieName);
virtual ~PocoHttpResponse();

};

#endif
