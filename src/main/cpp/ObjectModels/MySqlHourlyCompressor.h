#ifndef MySqlHourlyCompressor_h
#define MySqlHourlyCompressor_h

#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "MySqlService.h"
#include "MySqlCompressableEntityTraits.h"

#include "Object.h"
template<class T>
class MySqlHourlyCompressor : public Object {

private:
std::string coreFieldsToRead;
std::string coreFieldsForInserts;
std::string groupByFields;
std::string coreReplacementFields;
public:

std::string compressImpressionTableName;

MySqlCompressableEntityTraits<T> traits;

MySqlDriver* driver;
MySqlDriver* desitantionDriver;
MySqlHourlyCompressor(MySqlDriver* driver,
                      MySqlDriver* desitantionDriver);

virtual ~MySqlHourlyCompressor();

std::string replaceFieldsForInserts(std::string queryStr, std::shared_ptr<T> entity);

std::vector<std::shared_ptr<T> > readLastNHoursOfDataAsCompressed(int hour, std::set<std::string> alreadyCompressedDates);

std::shared_ptr<T> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<T> > readAllEntities();

void compressThread();

std::shared_ptr<T> getResultFromObject(std::shared_ptr<ResultSetHolder> res);

std::set<std::string> readAllCurrentDatesAlreadyCompressed();

void deleteCurrentHourOfImpressedInfo();

void deleteOlderThanNHoursAgoOfOriginalInfo(int nHoursToDelete);

void insertInCompressedTable(std::vector<std::shared_ptr<T> > entities);

std::string convertRecordToValueString(std::shared_ptr<T> entity);

};

#endif
