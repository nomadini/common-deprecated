#ifndef Feature_H
#define Feature_H


#include <memory>
#include <string>
#include <vector>
#include "JsonTypeDefs.h"
#include "Object.h"
#include <boost/optional.hpp>
#include "Object.h"

class Feature;


class Feature : public Object {

private:

public:
std::string type;
std::string name;
std::string status;  //UNKNOWN, ACTIVE, BLOCKED , INACTIVE
int level;
int id;  //mysqlId
bool valid;

static std::string generalTopLevelDomain;
static std::string MGRS100;
static std::string PLACE_TAG;
static std::string IAB_CAT;
//used on reading from cassandra where multiple types might be in DeviceFeatureHistory object
static std::string UNSURE_TYPE;
static std::string UNKNOWN_STATUS;
static std::string ACTIVE_STATUS;
static std::string BLOCKED_STATUS;
static std::string INACTIVE_STATUS;

void setNameAndLevel(std::string name, int level);

std::string getName();
std::string getType();
std::string getStatus();

int getLevel();

Feature(std::string type, std::string name, int level);
Feature(/*Needs by JsonMapUtil */);

std::string toString();

void validate();

bool isValid();

bool equals(Feature* feature);

std::string toJson();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

virtual ~Feature();
static std::set<std::string> getAllFeatureTypes();
static std::shared_ptr<Feature> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<Feature> fromJson(std::string json);
};


#endif
