
#include "AdEntry.h"
#include "GUtil.h"
#include "LoadPercentageBasedSampler.h"
#include "RandomUtil.h"
#include "AtomicLong.h"
LoadPercentageBasedSampler::LoadPercentageBasedSampler(
        int sampleRate,
        std::string samplerName) : Object(__FILE__) {

        assertAndThrow(sampleRate >= 0 &&
                       sampleRate <= 100);
        this->sampleRate = sampleRate;
        this->samplerName = samplerName;
        this->sampleRandomizer = std::make_shared<RandomUtil> (100);
        numberOfTotalCalls = std::make_shared<gicapods::AtomicLong>();
        numberOfFoundInSample = std::make_shared<gicapods::AtomicLong>();
        actualSamplerRate = std::make_shared<gicapods::AtomicDouble>();
}

bool LoadPercentageBasedSampler::isPartOfSample() {

        if (sampleRate == 100) {
                return true;
        } else if (sampleRate == 0) {
                return false;
        }

        long randomNumber =
                this->sampleRandomizer->randomNumberV2();
        numberOfTotalCalls->increment();
        int deviation = std::abs(actualSamplerRate->getValue() - sampleRate);
        if (deviation > 10 && numberOfTotalCalls->getValue() >= 1000) {
                LOG_EVERY_N(ERROR, 1000)<<"Sampler " <<samplerName<< " not working as desired"
                                        << " , deviation : " <<deviation
                                        << " , actualSamplerRate : "<<actualSamplerRate->getValue()
                                        << " , sampleRate  : "<< sampleRate
                                        << " , overallPercentage  : "<< overallPercentage
                                        <<" , numberOfFoundInSample : "<< numberOfFoundInSample->getValue()
                                        <<" , numberOfTotalCalls : " << numberOfTotalCalls->getValue();
        }

        if (randomNumber <= sampleRate && sampleRate != 0) {
                numberOfFoundInSample->increment();
                actualSamplerRate->setValue(
                        (numberOfFoundInSample->getValue() * 100)
                        / numberOfTotalCalls->getValue());

                return true;
        } else {
                return false;
        }



}
LoadPercentageBasedSampler::~LoadPercentageBasedSampler() {

}
