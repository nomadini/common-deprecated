#ifndef DeviceVisitCountHistory_h
#define DeviceVisitCountHistory_h
#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include "AtomicLong.h"
class DeviceVisitCountHistory;


class DeviceVisitCountHistory : public Object {
public:
DeviceVisitCountHistory();
virtual ~DeviceVisitCountHistory();

std::shared_ptr<gicapods::AtomicLong> numberOfVisitsByAllBiddersInPeriod;
std::shared_ptr<gicapods::AtomicLong> numberOfVisitsBythisBidder;

};

#endif
