/*
 * DeviceTestHelper.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_DEVICETESTHELPER_H_
#define GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_DEVICETESTHELPER_H_


class StringUtil;
class Device;

#include <string>
#include <vector>

class DeviceTestHelper {

public:
static std::vector<std::string> allUsIps;
static std::string getUsBasedIp();
static std::string getGeneralIp();
static std::string getRandomIpAddressFromFiniteSet();

};




#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_DEVICETESTHELPER_H_ */
