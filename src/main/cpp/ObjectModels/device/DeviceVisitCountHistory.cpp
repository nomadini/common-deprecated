#include "DeviceVisitCountHistory.h"
#include "DateTimeUtil.h"
#include "AtomicLong.h"
DeviceVisitCountHistory::DeviceVisitCountHistory() : Object(__FILE__) {
        numberOfVisitsByAllBiddersInPeriod = std::make_shared<gicapods::AtomicLong>(-1);
        numberOfVisitsBythisBidder  = std::make_shared<gicapods::AtomicLong>();
}

DeviceVisitCountHistory::~DeviceVisitCountHistory() {

}
