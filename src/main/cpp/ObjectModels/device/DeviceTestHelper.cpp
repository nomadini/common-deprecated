#include "StringUtil.h"
#include "DeviceTestHelper.h"
#include "RandomUtil.h"
#include "GUtil.h"

std::vector<std::string> DeviceTestHelper::allUsIps;

std::string DeviceTestHelper::getRandomIpAddressFromFiniteSet() {
								if(RandomUtil::sudoRandomNumber(20000) <= 20) {
																return DeviceTestHelper::getGeneralIp();
								}

								//we want to get a a little bit of random ness in our us ips
								std::vector<std::string> ipParts =
																StringUtil::split(DeviceTestHelper::getUsBasedIp(), ".");
								return ipParts.at(0) + "." + ipParts.at(1) + "."
								       //this is just to diversify the ip addresses
															+ StringUtil::toStr(RandomUtil::sudoRandomNumber(50)) +
															+"." +
								       //this is just to diversify the ip addresses
															StringUtil::toStr(RandomUtil::sudoRandomNumber(20));



}

std::string DeviceTestHelper::getGeneralIp() {
								//this method should not be static or should be treated carefully
								std::string rand;
								//new york ip address 72.229.28.185
								//from 3.... to 261... belong to USA  == > http://www.nirsoft.net/countryip/us.html
								// rand.append(StringUtil::toStr(RandomUtil::sudoRandomNumber(216 + 3))); //1000

								rand.append(StringUtil::toStr(RandomUtil::sudoRandomNumber(259) + 2));
								rand.append(".");
								rand.append(StringUtil::toStr(RandomUtil::sudoRandomNumber(100))); //10
								rand.append(".");
								rand.append(StringUtil::toStr(RandomUtil::sudoRandomNumber(50))); //100
								rand.append(".");
								rand.append(StringUtil::toStr(RandomUtil::sudoRandomNumber(200))); //100


								MLOG(2)<<"rand ip is : "<<rand;
								return rand;
}

std::string DeviceTestHelper::getUsBasedIp() {
								if (allUsIps.empty()) {

																allUsIps.push_back("97.13.19.192");
																allUsIps.push_back("19.75.39.152");
																allUsIps.push_back("40.53.2.37");
																allUsIps.push_back("159.42.6.177");
																allUsIps.push_back("192.24.33.123");
																allUsIps.push_back("204.60.45.90");
																allUsIps.push_back("139.93.11.111");
																allUsIps.push_back("97.1.43.38");
																allUsIps.push_back("67.64.42.65");
																allUsIps.push_back("54.56.3.3");
																allUsIps.push_back("35.63.48.117");
																allUsIps.push_back("155.84.44.127");
																allUsIps.push_back("99.49.1.2");
																allUsIps.push_back("139.70.20.144");
																allUsIps.push_back("72.45.42.41");
																allUsIps.push_back("159.56.34.127");
																allUsIps.push_back("143.60.19.196");
																allUsIps.push_back("6.53.19.79");
																allUsIps.push_back("17.19.36.72");
																allUsIps.push_back("55.44.3.140");
																allUsIps.push_back("108.50.29.195");
																allUsIps.push_back("63.46.22.78");
																allUsIps.push_back("45.35.38.85");
																allUsIps.push_back("16.37.45.76");
																allUsIps.push_back("18.37.37.170");
																allUsIps.push_back("129.22.17.16");
																allUsIps.push_back("152.34.48.160");
																allUsIps.push_back("108.65.27.6");
																allUsIps.push_back("205.74.5.75");
																allUsIps.push_back("140.98.13.112");
																allUsIps.push_back("162.27.48.64");
																allUsIps.push_back("69.41.8.42");
																allUsIps.push_back("139.77.5.33");
																allUsIps.push_back("104.94.12.199");
																allUsIps.push_back("172.4.20.108");
																allUsIps.push_back("167.23.2.199");
																allUsIps.push_back("67.67.14.67");
																allUsIps.push_back("34.69.8.42");
																allUsIps.push_back("6.82.8.45");
																allUsIps.push_back("52.39.40.11");
																allUsIps.push_back("99.73.1.105");
																allUsIps.push_back("147.79.24.55");
																allUsIps.push_back("162.48.16.86");
																allUsIps.push_back("137.5.26.40");
																allUsIps.push_back("169.15.13.181");
																allUsIps.push_back("66.13.45.126");
																allUsIps.push_back("44.54.32.147");
																allUsIps.push_back("19.47.19.144");
																allUsIps.push_back("155.52.22.131");
																allUsIps.push_back("166.92.15.165");
																allUsIps.push_back("6.93.47.148");
																allUsIps.push_back("162.88.35.78");
																allUsIps.push_back("63.64.22.8");
																allUsIps.push_back("162.2.27.191");
																allUsIps.push_back("98.4.17.87");
																allUsIps.push_back("3.20.10.35");
																allUsIps.push_back("56.48.42.109");
																allUsIps.push_back("130.3.43.152");
																allUsIps.push_back("137.90.29.138");
																allUsIps.push_back("19.59.48.169");
																allUsIps.push_back("72.86.29.131");

																allUsIps.push_back("216.70.8.2");
																allUsIps.push_back("48.2.22.40");
																allUsIps.push_back("56.38.39.181");
																allUsIps.push_back("11.52.44.25");
																allUsIps.push_back("3.39.2.133");
																allUsIps.push_back("98.86.34.35");
																allUsIps.push_back("65.91.48.35");
																allUsIps.push_back("204.91.42.156");
																allUsIps.push_back("159.39.10.126");
																allUsIps.push_back("131.45.19.130");
																allUsIps.push_back("156.65.10.2");
																allUsIps.push_back("184.87.7.64");
																allUsIps.push_back("130.52.21.137");
																allUsIps.push_back("170.86.2.183");
																allUsIps.push_back("166.95.48.199");
																allUsIps.push_back("143.28.42.65");
																allUsIps.push_back("128.58.7.145");
																allUsIps.push_back("149.29.12.76");
																allUsIps.push_back("63.35.19.45");
																allUsIps.push_back("132.96.41.90");

																allUsIps.push_back("192.5.12.100");
																allUsIps.push_back("69.23.44.171");
																allUsIps.push_back("66.80.2.71");
																allUsIps.push_back("153.54.17.17");
																allUsIps.push_back("64.47.41.13");
																allUsIps.push_back("208.3.46.147");
																allUsIps.push_back("71.99.29.145");
																allUsIps.push_back("167.83.3.192");
																allUsIps.push_back("173.60.31.41");
																allUsIps.push_back("152.4.45.187");

																allUsIps.push_back("206.63.11.18");
																allUsIps.push_back("216.14.25.139");
																allUsIps.push_back("72.69.12.61");

																allUsIps.push_back("134.84.28.28");
																allUsIps.push_back("8.63.33.68");
																allUsIps.push_back("99.37.28.64");
																allUsIps.push_back("13.82.32.135");
																allUsIps.push_back("209.93.34.152");
																allUsIps.push_back("7.64.8.14");
																allUsIps.push_back("143.79.16.119");
																allUsIps.push_back("56.27.22.46");
																allUsIps.push_back("168.27.7.94");
																allUsIps.push_back("66.64.9.197");
																allUsIps.push_back("13.34.30.37");
																allUsIps.push_back("206.82.16.140");
																allUsIps.push_back("11.49.48.85");
																allUsIps.push_back("136.2.40.66");
																allUsIps.push_back("66.33.2.170");
																allUsIps.push_back("134.9.34.72");
																allUsIps.push_back("104.58.14.189");
																allUsIps.push_back("63.83.34.199");
																allUsIps.push_back("63.10.2.180");
																allUsIps.push_back("18.98.47.127");

																allUsIps.push_back("3.60.36.107");
																allUsIps.push_back("129.75.6.55");
																allUsIps.push_back("107.53.13.130");
																allUsIps.push_back("66.16.9.86");
																allUsIps.push_back("209.30.45.136");
																allUsIps.push_back("44.50.49.197");
								}

								return allUsIps.at (RandomUtil::sudoRandomNumber (allUsIps.size() - 1));
}
