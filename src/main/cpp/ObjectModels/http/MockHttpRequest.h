//
// Created by Mahmoud Taabodi on 5/8/16.
//

#ifndef BIDDER_MOCKHTTPREQUEST_H
#define BIDDER_MOCKHTTPREQUEST_H


#include "Poco/Net/Net.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponseImpl.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/AutoPtr.h"
#include <istream>
#include <memory>
#include "Poco/Net/SocketAddress.h"

#include "gmock/gmock.h"
class MockHttpRequest : public Poco::Net::HTTPServerRequest {

public:
Poco::Net::SocketAddress _clientAddress;
std::shared_ptr<std::istringstream> requestStream;


//    MOCK_METHOD0(stream, std::istream& ());

MockHttpRequest(std::string requestBody);
/// Creates the HTTPServerRequest

~MockHttpRequest();
/// Destroys the HTTPServerRequest.

virtual std::istream& stream();

virtual bool expectContinue() const;
//    /// Returns true if the client expects a
//    /// 100 Continue response.
//
virtual const Poco::Net::SocketAddress& clientAddress() const;
//    /// Returns the client's address.
//
virtual const Poco::Net::SocketAddress& serverAddress() const;
//    /// Returns the server's address.
//
virtual const Poco::Net::HTTPServerParams& serverParams() const;
//    /// Returns a reference to the server parameters.
//
virtual Poco::Net::HTTPServerResponse& response() const;
//    /// Returns a reference to the associated response.


};


#endif //BIDDER_MOCKHTTPREQUEST_H
