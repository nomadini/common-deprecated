//
// Created by Mahmoud Taabodi on 5/8/16.
//

#include "MockHttpResponse.h"


MockHttpResponse::MockHttpResponse() {
    responseStream = std::make_shared<std::ostringstream>();
}

MockHttpResponse::~MockHttpResponse() {

}
void MockHttpResponse::sendContinue() {

}
std::ostream& MockHttpResponse::send() {
    return *responseStream;
}

void MockHttpResponse::sendFile(const std::string& path, const std::string& mediaType) {

}
void MockHttpResponse::sendBuffer(const void* pBuffer, std::size_t length) {

}

void MockHttpResponse::redirect(const std::string& uri, HTTPStatus status) {

}

void MockHttpResponse::requireAuthentication(const std::string& realm) {

}

bool MockHttpResponse::sent() const {

}

std::string MockHttpResponse::getResponseBody() {

    return  responseStream->str();
}
/// Returns true if the response (header) has been sent.