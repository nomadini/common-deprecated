#ifndef ModelScore_H
#define ModelScore_H


#include "AdEntry.h"
#include "JsonTypeDefs.h"

#include <set>
#include <unordered_map>
#include <memory>
#include <string>
#include <vector>
class Device;

#include "CassandraManagedType.h";
#include "Object.h"
class ModelScore;




class ModelScore : public CassandraManagedType, public Object {

private:

public:

int modelId;
double score;
ModelScore();
std::string toString();
static std::string getName();

static std::shared_ptr<ModelScore> fromJson(std::string jsonString);
static std::shared_ptr<ModelScore> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
virtual ~ModelScore();
};
/*

   std::shared_ptr<ModelScore>List ModelScoreList;
   for (std::shared_ptr<ModelScore>List::iterator it = ModelScoreList.begin();it != std::shared_ptr<ModelScore>List.end(); ++it)
   {

   }
 */

#endif
