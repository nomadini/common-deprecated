

#include "AdEntry.h"
#include "Device.h"
#include "GUtil.h"
#include "ModelScore.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

ModelScore::ModelScore() : Object("ModelScore") {
}

ModelScore::~ModelScore() {

}

std::string ModelScore::toString() {
								return this->toJson();
}

std::string ModelScore::getName() {
								return "ModelScore";
}


std::shared_ptr<ModelScore> ModelScore::fromJsonValue(RapidJsonValueType value) {

								std::shared_ptr<ModelScore> obj = std::make_shared<ModelScore>();
								obj->modelId = JsonUtil::GetIntSafely(value, "modelId");
								obj->score = JsonUtil::GetDoubleSafely(value, "score");
								return obj;

}
std::shared_ptr<ModelScore> ModelScore::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

std::string ModelScore::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

void ModelScore::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								JsonUtil::addMemberToValue_FromPair(doc, "modelId", modelId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "score", score, value);
}
