#ifndef PlacesCachedResult_H
#define PlacesCachedResult_H


#include "Place.h"
#include "Status.h"
#include "JsonUtil.h"

#include <memory>
#include <string>
class PlacesCachedResult {

private:

public:
/* begin of key fields */
std::string mgrs10m;
int maxNumebrOfWantedFeatures;
/* end of key fields */

std::vector<std::shared_ptr<Place> > places;

PlacesCachedResult(std::string mgrs10m,
                   int maxNumebrOfWantedFeatures);

virtual ~PlacesCachedResult();

std::string toJson();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

static std::shared_ptr<PlacesCachedResult> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<PlacesCachedResult> fromJson(std::string json);
};




#endif
