/*
 * EventLog.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef EventLogCassandraTraits_H_
#define EventLogCassandraTraits_H_


#include <memory>
#include <string>
#include <vector>
#include "JsonTypeDefs.h"
#include "CassandraManagedType.h"
#include "Object.h"
#include "BidEventLog.h"
#include "EventLog.h"

#include "CassandraDriverInterface.h"
class EventLogCassandraTraits : public Object {

public:
static std::string getAllFields();
static std::string getQuestionMarks();

static void extractRowData(
        CassIterator *rowIterator,
        EventLog* requestObject,
        std::shared_ptr<EventLog>& eventLog);
static void extractRowDataForBidEventLog(
        CassIterator *rowIterator,
        EventLog* requestObject,
        std::shared_ptr<BidEventLog>& eventLog);
static void commonExtract(
        CassIterator *rowIterator,
        EventLog* requestObject,
        EventLog* eventLog);

static void bindValuesToCassStatementForBatchWrite(
        EventLog* event,
        CassStatement* statement);
virtual ~EventLogCassandraTraits();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
