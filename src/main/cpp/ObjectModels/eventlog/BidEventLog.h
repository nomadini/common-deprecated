/*
 * BidEventLog.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef BidEventLog_H_
#define BidEventLog_H_


#include <memory>
#include <string>
#include <vector>
#include "JsonTypeDefs.h"
#include "CassandraManagedType.h"

class Device;

class BidEventLog;
#include "EventLog.h"
#include "Object.h"
/*
   BidEventLog is used to store bid and no_bid events in a separate column family
   in cassandra, it extends EventLog class, but has no field on its own yet.
   there might be a case in future that BidEventLog has a field that we don't need it
   for regular impression, click, conversion events in eventLog column family in cassandra
 */
class BidEventLog : public EventLog, public Object {

public:

BidEventLog(std::string transactionId,
            std::string eventType,
            std::string targetingTypes);

std::string toString();

std::string toJson();

static std::shared_ptr<BidEventLog> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<BidEventLog> fromJson(std::string jsonString);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

virtual ~BidEventLog();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
