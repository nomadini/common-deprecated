#include "BidEventLog.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "EventLog.h"
#include "Device.h"

BidEventLog::BidEventLog(
								std::string transactionIdArg,
								std::string eventTypeArg,
								std::string targetingTypes) :
								EventLog(transactionIdArg, eventTypeArg, targetingTypes), Object(__FILE__) {
}

std::string BidEventLog::toString() {
								return toJson();

}

std::shared_ptr<BidEventLog> BidEventLog::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

void BidEventLog::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								EventLog::addPropertiesToJsonValue(value, doc);
}

std::string BidEventLog::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}

std::shared_ptr<BidEventLog> BidEventLog::fromJsonValue(RapidJsonValueType value) {
								std::string targetingTypes="[]";
								if (value.HasMember("targetingTypes")) {
																targetingTypes = JsonUtil::GetStringSafely(value,"targetingTypes");
								}
								auto bidEventLog = std::make_shared<BidEventLog>(
																JsonUtil::GetStringSafely(value, "eventId"),
																JsonUtil::GetStringSafely(value, "eventType"),
																targetingTypes
																);

								EventLog::populateFieldsFromJsonValue((EventLog*) bidEventLog.get(), value);

								return bidEventLog;
}


BidEventLog::~BidEventLog() {

}
