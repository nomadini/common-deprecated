#include "EventLog.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "Device.h"

std::string EventLog::EVENT_TYPE_CLICK = "click";
std::string EventLog::EVENT_TYPE_BID = "bid";
std::string EventLog::EVENT_TYPE_NO_BID = "no_bid";
std::string EventLog::EVENT_TYPE_IMPRESSION = "impression";
std::string EventLog::EVENT_TYPE_CONVERSION = "conversion";
std::string EventLog::TARGETING_TYPE_IGNORE_FOR_NOW = "ignore";
std::string EventLog::EVENT_TYPE_ACTION = "action";
std::string EventLog::EVENT_TYPE_MAPPING = "mapping";

int EventLog::SCRUB_TYPE_PERFECT_EVENT = 410;
int EventLog::SCRUB_TYPE_REPEATED_IMPRESSION = 411;
int EventLog::SCRUB_TYPE_REPEATED_CLICK = 412;

EventLog::EventLog(
								std::string transactionIdArg,
								std::string eventTypeArg,
								std::string targetingTypes) : Object(__FILE__) {
								clientId = 0;
								pixelId = 0;
								advertiserId = 0;
								campaignId = 0;
								targetGroupId = 0;
								creativeId = 0;
								winBidPrice = 0;
								bidPrice = 0;
								partnerId = 0;

								publisherId = 0;
								platformCost = 0;
								advertiserCost = 0;
								userTimeZonDifferenceWithUTC = 0;
								deviceLat = 0;
								deviceLon = 0;
								biddingOnlyForMatchingPixel = false;
								geoSegmentListId = 0;

								scrubType = SCRUB_TYPE_PERFECT_EVENT;
								assertAndThrow(!transactionIdArg.empty());
								this->eventId = transactionIdArg;
								this->eventType = eventTypeArg;

								if (!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_BID, eventType) &&
												!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_NO_BID, eventType) &&
												!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_CLICK, eventType) &&
												!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_CONVERSION, eventType) &&
												!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_ACTION, eventType) &&
												!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_MAPPING, eventType) &&
												!StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_IMPRESSION, eventType)) {
																EXIT("wrong event type being used :" + eventType);
								}

								this->eventTime = DateTimeUtil::getNowInMilliSecond();
								this->targetingTypes = targetingTypes;


}

std::string EventLog::toString() {
								return toJson();

}

std::shared_ptr<EventLog> EventLog::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

void EventLog::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "eventId", eventId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "sourceEventId", eventId, value);

								JsonUtil::addMemberToValue_FromPair(doc, "eventTime", eventTime, value);
								JsonUtil::addMemberToValue_FromPair(doc, "eventType", eventType, value);
								JsonUtil::addMemberToValue_FromPair(doc, "targetGroupId", targetGroupId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "creativeId", creativeId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "campaignId", campaignId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "clientId", clientId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "pixelId", pixelId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "advertiserId",advertiserId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "publisherId", publisherId, value);

								JsonUtil::addMemberToValue_FromPair(doc, "winBidPrice", winBidPrice, value);
								JsonUtil::addMemberToValue_FromPair(doc, "bidPrice", bidPrice, value);
								JsonUtil::addMemberToValue_FromPair(doc, "platformCost", platformCost, value);
								JsonUtil::addMemberToValue_FromPair(doc, "advertiserCost", advertiserCost, value);
								JsonUtil::addMemberToValue_FromPair(doc, "userTimeZone", userTimeZone, value);
								JsonUtil::addMemberToValue_FromPair(
																doc,
																"userTimeZonDifferenceWithUTC",
																userTimeZonDifferenceWithUTC, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceId", device->getDeviceId(), value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceType", device->getDeviceType(), value);
								JsonUtil::addMemberToValue_FromPair(doc, "impressionType", impressionType, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceUserAgent", deviceUserAgent, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceIp", deviceIp, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceLat", deviceLat, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceLon", deviceLon, value);
								JsonUtil::addMemberToValue_FromPair(doc, "scrubType", scrubType, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceCountry", deviceCountry, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceState", deviceState, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceCity", deviceCity, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceZipcode", deviceZipcode, value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceIpAddress", deviceIpAddress, value);
								JsonUtil::addMemberToValue_FromPair(doc, "siteCategory", siteCategory, value);
								JsonUtil::addMemberToValue_FromPair(doc, "siteDomain", siteDomain, value);
								JsonUtil::addMemberToValue_FromPair(doc, "sitePage", sitePage, value);
								JsonUtil::addMemberToValue_FromPair(doc, "sitePublisherDomain", sitePublisherDomain, value);
								JsonUtil::addMemberToValue_FromPair(doc, "sitePublisherName", sitePublisherName, value);
								JsonUtil::addMemberToValue_FromPair(doc, "adSize", adSize, value);
								JsonUtil::addMemberToValue_FromPair(doc, "lastModuleRunInPipeline", lastModuleRunInPipeline, value);
								JsonUtil::addBooleanMemberToValue_FromPair(doc, "biddingOnlyForMatchingPixel", biddingOnlyForMatchingPixel, value);
								JsonUtil::addMemberToValue_FromPair(doc, "exchangeName", exchangeName, value);
								JsonUtil::addMemberToValue_FromPair(doc, "mgrs1km", mgrs1km, value);
								JsonUtil::addMemberToValue_FromPair(doc, "mgrs100m", mgrs100m, value);
								JsonUtil::addMemberToValue_FromPair(doc, "mgrs10m", mgrs10m, value);
								JsonUtil::addMemberToValue_FromPair(doc, "targetingTypes", targetingTypes, value);
								JsonUtil::addMemberToValue_FromPair(doc, "appHostname", appHostname, value);
								JsonUtil::addMemberToValue_FromPair(doc, "appVersion", appVersion, value);
								JsonUtil::addMemberToValue_FromPair(doc, "bidderCallbackServletUrl", bidderCallbackServletUrl, value);
								JsonArrayUtil::addMemberToValue_FromPair(doc, "chosenSegmentIdsList", chosenSegmentIdsList, value);
								JsonArrayUtil::addMemberToValue_FromPair(doc, "allSegmentIdsFoundList", allSegmentIdsFoundList, value);
								JsonUtil::addMemberToValue_FromPair(doc, "partnerId", partnerId, value);
}

std::string EventLog::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}

std::shared_ptr<EventLog> EventLog::fromJsonValue(RapidJsonValueType value) {
								std::string targetingTypes="[]";
								if (value.HasMember("targetingTypes")) {
																targetingTypes = JsonUtil::GetStringSafely(value,"targetingTypes");
								}

								auto eventLog = std::make_shared<EventLog>(
																JsonUtil::GetStringSafely(value, "eventId"),
																JsonUtil::GetStringSafely(value, "eventType"),
																targetingTypes
																);

								populateFieldsFromJsonValue(eventLog.get(), value);
								return eventLog;
}


void EventLog::populateFieldsFromJsonValue(EventLog* eventLog, RapidJsonValueType value) {

								eventLog->eventTime  = JsonUtil::GetStringSafely(value, "sourceEventId");
								eventLog->eventTime  = JsonUtil::GetStringSafely(value, "eventTime");
								eventLog->targetGroupId  = JsonUtil::GetIntSafely(value, "targetGroupId");
								eventLog->creativeId  = JsonUtil::GetIntSafely(value, "creativeId");
								eventLog->campaignId  = JsonUtil::GetIntSafely(value, "campaignId");
								eventLog->clientId  = JsonUtil::GetIntSafely(value, "clientId");
								eventLog->pixelId  = JsonUtil::GetIntSafely(value, "pixelId");
								eventLog->advertiserId  = JsonUtil::GetIntSafely(value, "advertiserId");
								eventLog->publisherId  = JsonUtil::GetIntSafely(value, "publisherId");

								eventLog->winBidPrice  = JsonUtil::GetDoubleSafely(value, "winBidPrice");
								eventLog->bidPrice  = JsonUtil::GetDoubleSafely(value, "bidPrice");
								eventLog->platformCost  = JsonUtil::GetDoubleSafely(value, "platformCost");
								eventLog->advertiserCost  = JsonUtil::GetDoubleSafely(value, "advertiserCost");
								eventLog->userTimeZone  = JsonUtil::GetStringSafely(value, "userTimeZone");
								eventLog->userTimeZonDifferenceWithUTC  = JsonUtil::GetIntSafely(value, "userTimeZonDifferenceWithUTC");
								eventLog->partnerId  = JsonUtil::GetIntSafely(value, "partnerId");

								eventLog->deviceUserAgent  = JsonUtil::GetStringSafely(value, "deviceUserAgent");
								eventLog->deviceIp  = JsonUtil::GetStringSafely(value, "deviceIp");

								eventLog->device = std::make_shared<Device>(
																JsonUtil::GetStringSafely(value, "deviceId"),
																JsonUtil::GetStringSafely(value, "deviceType"));

								eventLog->impressionType = JsonUtil::GetStringSafely(value, "impressionType");
								eventLog->deviceLat  = JsonUtil::GetDoubleSafely(value, "deviceLat");
								eventLog->deviceLon  = JsonUtil::GetDoubleSafely(value, "deviceLon");
								eventLog->scrubType  = JsonUtil::GetIntSafely(value, "scrubType");

								eventLog->deviceCountry  = JsonUtil::GetStringSafely(value, "deviceCountry");
								eventLog->deviceCity  = JsonUtil::GetStringSafely(value, "deviceCity");
								eventLog->deviceState  = JsonUtil::GetStringSafely(value, "deviceState");
								eventLog->deviceZipcode  = JsonUtil::GetStringSafely(value, "deviceZipcode");
								eventLog->deviceIpAddress  = JsonUtil::GetStringSafely(value, "deviceIpAddress", "");


								eventLog->siteCategory  = JsonUtil::GetStringSafely(value, "siteCategory");
								eventLog->siteDomain  = JsonUtil::GetStringSafely(value, "siteDomain");
								eventLog->sitePage  = JsonUtil::GetStringSafely(value, "sitePage");
								eventLog->sitePublisherDomain  = JsonUtil::GetStringSafely(value, "sitePublisherDomain");
								eventLog->sitePublisherName  = JsonUtil::GetStringSafely(value, "sitePublisherName");
								eventLog->adSize  = JsonUtil::GetStringSafely(value, "adSize");
								eventLog->lastModuleRunInPipeline  = JsonUtil::GetStringSafely(value, "lastModuleRunInPipeline");
								eventLog->biddingOnlyForMatchingPixel =  JsonUtil::GetBooleanSafely(value, "biddingOnlyForMatchingPixel");
								eventLog->exchangeName = JsonUtil::GetStringSafely(value, "exchangeName");
								eventLog->mgrs1km = JsonUtil::GetStringSafely (value, "mgrs1km");
								eventLog->mgrs100m = JsonUtil::GetStringSafely (value, "mgrs100m");
								eventLog->mgrs10m = JsonUtil::GetStringSafely (value, "mgrs10m");

								eventLog->appHostname = JsonUtil::GetStringSafely (value, "appHostname");
								eventLog->appVersion = JsonUtil::GetStringSafely (value,"appVersion");
								eventLog->bidderCallbackServletUrl = JsonUtil::GetStringSafely (value,"bidderCallbackServletUrl");
								JsonArrayUtil::getArrayFromValueMemeber(value,"chosenSegmentIdsList", eventLog->chosenSegmentIdsList);
								JsonArrayUtil::getArrayFromValueMemeber(value,"allSegmentIdsFoundList", eventLog->allSegmentIdsFoundList);

}

EventLog::~EventLog() {

}

std::string EventLog::createRandomTransactionId() {
								return StringUtil::random_string (12) + StringUtil::toStr ("_")
															+ StringUtil::toStr (DateTimeUtil::getNowInMicroSecond ());
}
