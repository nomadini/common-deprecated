#include "EventLog.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "EventLogCassandraTraits.h"
#include "Device.h"
#include "JsonArrayUtil.h"

EventLogCassandraTraits::~EventLogCassandraTraits() {

}

std::string EventLogCassandraTraits::getQuestionMarks() {
								std::string marks;
								auto fieldsSize = StringUtil::tokenizeString(getAllFields(), ",").size();
								for (int i=0; i < fieldsSize; i++) {
																if (i == fieldsSize - 1) {
																								marks +="?";
																} else {
																								marks +="?,";
																}

								}

								return marks;
}
std::string EventLogCassandraTraits::getAllFields() {

								static std::string allFields =
																"eventId,"
																"eventTime,"
																"eventTimeInHourPercision,"
																"event_type,"
																"partner_id,"
																"scrub_type,"
																"sourceEventId,"
																"targetgroup_id,"
																"creative_id,"
																"campaign_id,"
																"advertiser_id,"
																"client_id,"
																"pixel_id,"
																"publisher_id,"
																"segment_id,"
																"geosegmentlist_id,"
																"exchangeCostInCpm,"
																"bidPriceInCpm,"
																"platformCostInCpm,"
																"advertiserCostInCpm,"
																"userTimezone,"
																"userTimeZoneDifferenceWithUTC,"
																"nomadiniDeviceId,"
																"deviceUserAgent,"
																"deviceIp,"
																"deviceId,"
																"latitude,"
																"longitude,"
																"deviceCountry,"
																"deviceState,"
																"deviceCity,"
																"deviceZipcode,"
																"deviceIpAddress,"
																"deviceType,"
																"siteCategory,"
																"siteDomain,"
																"sitePage,"
																"sitePublisherDomain,"
																"sitePublisherName,"
																"adSize,"
																"lastModuleRunInPipeline,"
																"impressionType,"
																"mgrs1km,"
																"mgrs100m,"
																"mgrs10m,"
																"targetingTypes,"
																"appHostname,"
																"appVersion,"
																"chosenSegmentIdsList,"
																"allSegmentIdsFoundList,"
																"count";

								return allFields;
}

void EventLogCassandraTraits::bindValuesToCassStatementForBatchWrite(
								EventLog* event,
								CassStatement* statement) {
								NULL_CHECK(event);
								NULL_CHECK(statement);

								cass_statement_bind_string_by_name(statement, "eventId", event->eventId.c_str());
								cass_statement_bind_int64_by_name(statement, "eventTime", DateTimeUtil::getNowInMilliSecond());
								cass_statement_bind_int64_by_name(statement, "eventTimeInHourPercision", DateTimeUtil::getNowHourInMilliSecond());
								cass_statement_bind_string_by_name(statement, "event_type", event->eventType.c_str());
								cass_statement_bind_int32_by_name(statement, "partner_id", event->partnerId);
								cass_statement_bind_int32_by_name(statement, "scrub_type", event->scrubType);
								cass_statement_bind_string_by_name(statement, "sourceEventId", event->sourceEventId.c_str());
								cass_statement_bind_int64_by_name(statement, "client_id", event->clientId);
								cass_statement_bind_int32_by_name(statement, "pixel_id", event->pixelId);
								cass_statement_bind_int64_by_name(statement, "targetGroup_id", event->targetGroupId);
								cass_statement_bind_int64_by_name(statement, "creative_id", event->creativeId);
								cass_statement_bind_int64_by_name(statement, "campaign_id", event->campaignId);
								cass_statement_bind_int64_by_name(statement, "advertiser_id", event->advertiserId);
								cass_statement_bind_int64_by_name(statement, "publisher_id", event->publisherId);
								cass_statement_bind_int64_by_name(statement, "segment_id", 0); //deprecated
								cass_statement_bind_int64_by_name(statement, "geosegmentlist_id", event->geoSegmentListId);
								cass_statement_bind_double_by_name(statement, "exchangeCostInCpm", event->winBidPrice);
								cass_statement_bind_double_by_name(statement, "bidPriceInCpm", event->bidPrice);
								cass_statement_bind_double_by_name(statement, "platformCostInCpm", event->platformCost);
								cass_statement_bind_double_by_name(statement, "advertiserCostInCpm", event->advertiserCost);
								cass_statement_bind_string_by_name(statement, "userTimeZone", event->userTimeZone.c_str());
								cass_statement_bind_string_by_name(statement, "usertimezonedifferencewithutc", StringUtil::toStr(event->userTimeZonDifferenceWithUTC).c_str());
								cass_statement_bind_string_by_name(statement, "nomadiniDeviceId", event->device->getDeviceId().c_str());
								cass_statement_bind_string_by_name(statement, "deviceUserAgent", event->deviceUserAgent.c_str());
								cass_statement_bind_string_by_name(statement, "deviceIp", event->deviceIp.c_str());
								cass_statement_bind_string_by_name(statement, "deviceId", event->device->getDeviceId().c_str());
								cass_statement_bind_string_by_name(statement, "deviceType", event->device->getDeviceType().c_str());
								cass_statement_bind_string_by_name(statement, "latitude", StringUtil::toStr(event->deviceLat).c_str());
								cass_statement_bind_string_by_name(statement, "longitude", StringUtil::toStr(event->deviceLon).c_str());
								cass_statement_bind_string_by_name(statement, "deviceCountry",event->deviceCountry.c_str());
								cass_statement_bind_string_by_name(statement, "deviceState", event->deviceState.c_str());
								cass_statement_bind_string_by_name(statement, "deviceCity",  event->deviceCity.c_str());
								cass_statement_bind_string_by_name(statement, "deviceZipcode", event->deviceZipcode.c_str());
								cass_statement_bind_string_by_name(statement, "deviceIpAddress", event->deviceIpAddress.c_str());
								cass_statement_bind_string_by_name(statement, "siteCategory", event->siteCategory.c_str());
								cass_statement_bind_string_by_name(statement, "siteDomain", event->siteDomain.c_str());
								cass_statement_bind_string_by_name(statement, "sitePage", event->sitePage.c_str());
								cass_statement_bind_string_by_name(statement, "sitePublisherDomain", event->sitePublisherDomain.c_str());
								cass_statement_bind_string_by_name(statement, "sitePublisherName", event->sitePublisherName.c_str());
								cass_statement_bind_string_by_name(statement, "adSize", event->adSize.c_str());
								cass_statement_bind_string_by_name(statement, "lastModuleRunInPipeline", event->lastModuleRunInPipeline.c_str());
								cass_statement_bind_string_by_name(statement, "impressionType", event->impressionType.c_str());
								cass_statement_bind_string_by_name(statement, "mgrs1km", event->mgrs1km.c_str());
								cass_statement_bind_string_by_name(statement, "mgrs100m", event->mgrs100m.c_str());
								cass_statement_bind_string_by_name(statement, "mgrs10m", event->mgrs10m.c_str());
								cass_statement_bind_string_by_name(statement, "targetingTypes", event->targetingTypes.c_str());
								cass_statement_bind_string_by_name(statement, "appHostname", "AdServer");
								cass_statement_bind_string_by_name(statement, "appVersion", "0-SNAPSHOT");
								cass_statement_bind_string_by_name(statement, "chosenSegmentIdsList", JsonArrayUtil::convertListToJson(event->chosenSegmentIdsList).c_str());
								cass_statement_bind_string_by_name(statement, "allSegmentIdsFoundList", JsonArrayUtil::convertListToJson(event->allSegmentIdsFoundList).c_str());
								cass_statement_bind_int64_by_name(statement, "count", 1);
}

void EventLogCassandraTraits::extractRowDataForBidEventLog(
								CassIterator *rowIterator,
								EventLog* requestObject,
								std::shared_ptr<BidEventLog>& eventLog) {
								if (eventLog == nullptr) {
																eventLog = std::make_shared<BidEventLog>(
																								CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "eventId"),
																								CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "event_type"),
																								CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "targetingTypes"));
								}

								EventLogCassandraTraits::commonExtract(
																rowIterator,
																requestObject,
																(EventLog*) eventLog.get());
}

void EventLogCassandraTraits::extractRowData(
								CassIterator *rowIterator,
								EventLog* requestObject,
								std::shared_ptr<EventLog>& eventLog) {

								if (eventLog == nullptr) {
																eventLog = std::make_shared<EventLog>(
																								CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "eventId"),
																								CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "event_type"),
																								CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "targetingTypes"));
								}

								EventLogCassandraTraits::commonExtract(
																rowIterator,
																requestObject,
																eventLog.get());

}

void EventLogCassandraTraits::commonExtract(
								CassIterator *rowIterator,
								EventLog* requestObject,
								EventLog* eventLog) {

								eventLog->eventTime = CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "eventTime");
								// eventLog->eventTimeInHourPercision = CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "eventTimeInHourPercision");
								eventLog->sourceEventId = CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "sourceEventId");

								eventLog->scrubType = CassandraDriverInterface::getIntegerValueFromRowByName (rowIterator, "scrub_type");
								eventLog->clientId = CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "client_id");
								eventLog->partnerId = CassandraDriverInterface::getIntegerValueFromRowByName (rowIterator, "partner_id");
								eventLog->pixelId = CassandraDriverInterface::getIntegerValueFromRowByName (rowIterator, "pixel_id");
								eventLog->targetGroupId = CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "targetGroup_id");
								eventLog->creativeId = CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "creative_id");
								eventLog->campaignId = CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "campaign_id");
								eventLog->advertiserId = CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "advertiser_id");
								eventLog->publisherId = CassandraDriverInterface::getLongValueFromRowByName (rowIterator,  "publisher_id");
								eventLog->geoSegmentListId = CassandraDriverInterface::getLongValueFromRowByName (rowIterator,  "nomadiniDeviceId");
								eventLog->winBidPrice = CassandraDriverInterface::getDoubleValueFromRowByName (rowIterator,  "exchangeCostInCpm");
								eventLog->bidPrice = CassandraDriverInterface::getDoubleValueFromRowByName (rowIterator,  "bidPriceInCpm");
								eventLog->platformCost = CassandraDriverInterface::getDoubleValueFromRowByName (rowIterator,  "platformCostInCpm");
								eventLog->advertiserCost = CassandraDriverInterface::getDoubleValueFromRowByName (rowIterator,  "advertiserCostInCpm");
								eventLog->userTimeZone = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "userTimeZone");
								eventLog->userTimeZonDifferenceWithUTC = CassandraDriverInterface::getLongValueFromRowByName (rowIterator,  "userTimeZonDifferenceWithUTC");
								eventLog->deviceUserAgent = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceUserAgent");
								eventLog->deviceIp = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceIp");
								eventLog->device = std::make_shared<Device>(
																CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceId"),
																CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceType"));

								eventLog->deviceCountry = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceCountry");
								eventLog->deviceState = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceState");
								eventLog->deviceCity = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceCity");
								eventLog->deviceZipcode = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceZipcode");
								eventLog->deviceIpAddress = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "deviceIpAddress");
								eventLog->siteCategory = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "siteCategory");
								eventLog->siteDomain = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "siteDomain");
								eventLog->sitePage = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "sitePage");
								eventLog->sitePublisherDomain = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "sitePublisherDomain");
								eventLog->sitePublisherName = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "sitePublisherName");

								eventLog->adSize = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "adSize");
								eventLog->lastModuleRunInPipeline = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "lastModuleRunInPipeline");
								eventLog->impressionType = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "impressionType");

								eventLog->mgrs1km = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "mgrs1km");
								eventLog->mgrs100m = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "mgrs100m");
								eventLog->mgrs10m = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "mgrs10m");
								eventLog->targetingTypes = CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "targetingTypes");
								JsonArrayUtil::getArrayOfObjectsFromJsonString(
																CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "chosenSegmentIdsList"), eventLog->chosenSegmentIdsList);

								JsonArrayUtil::getArrayOfObjectsFromJsonString(
																CassandraDriverInterface::getStringValueFromRowByName (rowIterator,  "allSegmentIdsFoundList"), eventLog->allSegmentIdsFoundList);

}
