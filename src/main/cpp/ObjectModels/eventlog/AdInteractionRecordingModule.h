#ifndef AdInteractionRecordingModule_H
#define AdInteractionRecordingModule_H


#include <memory>
#include <string>
#include "CassandraServiceInterface.h"
#include <tbb/concurrent_queue.h>
#include "Object.h"
class EventLog;
class InfluxDbClient;
class CassandraServiceInterface;
class AdInteractionRecordingModule : public Object {

private:

public:

EntityToModuleStateStats* entityToModuleStateStats;
CassandraServiceInterface* eventLogCassandraService;
InfluxDbClient* influxDbClient;

std::string adserverHostname;
std::string adserverVersion;

std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<EventLog> > > queueOfEventLogs;

AdInteractionRecordingModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        CassandraServiceInterface* eventLogCassandraService,
        std::string adserverHostname,
        std::string adserverVersion);

virtual std::string getName();

void enqueueEventForPersistence(std::shared_ptr<EventLog>  eventLog);

void recordEvents();

void writeEventsToInfluxDb(std::vector<std::shared_ptr<EventLog> > events);

std::string toInfluxRecord(std::shared_ptr<EventLog> record,
                           std::string measurementName);

virtual ~AdInteractionRecordingModule();

};




#endif
