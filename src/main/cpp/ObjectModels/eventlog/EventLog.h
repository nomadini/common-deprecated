/*
 * EventLog.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_
#define GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_


#include <memory>
#include <string>
#include <vector>
#include "JsonTypeDefs.h"
#include "CassandraManagedType.h"

class Device;

class EventLog;
#include "Object.h"


/*
   -- each event has a unique eventId that we can query by
   -- values of eventType can be bid, conversion, impression, click

   -- TODO : remove eventId from cassandra event table
 */
class EventLog : public CassandraManagedType, public Object {

public:

static std::string EVENT_TYPE_CLICK;
static std::string EVENT_TYPE_BID;
static std::string EVENT_TYPE_NO_BID;
static std::string EVENT_TYPE_IMPRESSION;
static std::string EVENT_TYPE_CONVERSION;
static std::string EVENT_TYPE_ACTION;
static std::string EVENT_TYPE_MAPPING;
static std::string TARGETING_TYPE_IGNORE_FOR_NOW;

static int SCRUB_TYPE_PERFECT_EVENT;
static int SCRUB_TYPE_REPEATED_IMPRESSION;
static int SCRUB_TYPE_REPEATED_CLICK;

int partnerId;
int pixelId;
std::string eventId;
std::string eventTime;
std::string eventType;  //type of event eg. bid, no_bid, impression, click
std::string sourceEventId;
std::string targetingTypes;//reasons behind bidding and showing this impression, as a json list

/*
   these ids are long, because impression_compressed table has them in long
   we keep these long for consistency
 */
long clientId;
long targetGroupId;
long creativeId;
long campaignId;
long advertiserId;
long publisherId;
long geoSegmentListId;

int scrubType;

/**
   this is the price that we have won the bid with
 */
double winBidPrice;
/**
   this is the price that we bid with
 */
double bidPrice;
/**
   this is the price that we charge client for using our platform
 */
double platformCost;

double advertiserCost;

std::string userTimeZone;
int userTimeZonDifferenceWithUTC;

/** ID Struct **/
std::string deviceUserAgent;
std::string deviceIp;

std::shared_ptr<Device> device;
double deviceLat;
double deviceLon;

std::string deviceCountry;
std::string deviceState;
std::string deviceCity;
std::string deviceZipcode;
std::string deviceIpAddress;
std::string impressionType; //video, in-app, web

/** Site Struct **/
std::string siteCategory;
std::string siteDomain;
std::string sitePage;
std::string sitePublisherDomain;
std::string sitePublisherName;
std::string adSize;
std::string lastModuleRunInPipeline;
bool biddingOnlyForMatchingPixel;
std::string exchangeName;

std::string mgrs1km;
std::string mgrs100m;
std::string mgrs10m;


std::string appHostname;
std::string appVersion;

std::string bidderCallbackServletUrl;
std::vector<int> chosenSegmentIdsList;
std::vector<int> allSegmentIdsFoundList;

EventLog(std::string transactionId,
         std::string eventType,
         std::string targetingTypes
         );

std::string toString();

std::string toJson();

static void populateFieldsFromJsonValue(EventLog* eventLog, RapidJsonValueType value);
static std::shared_ptr<EventLog> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<EventLog> fromJson(std::string jsonString);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

virtual ~EventLog();
static std::string createRandomTransactionId();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
