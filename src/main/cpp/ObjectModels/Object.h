#ifndef COMMON_Object_H
#define COMMON_Object_H

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"

class EntityToModuleStateStats;
class ObjectGlobalMapContainer;

class Object {
private:
std::string nameOfObject;

public:
static std::set<std::string> highRateClasses;

ObjectGlobalMapContainer* ObjectGlobalMapContainer;

Object(std::string nameOfObject);

virtual ~Object();

void increment();
void decrement();
static void printStats();
static std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> > > getMap();
};



#endif //COMMON_Object_H
