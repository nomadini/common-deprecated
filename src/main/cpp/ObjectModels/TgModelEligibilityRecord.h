#ifndef TgModelEligibilityRecord_h
#define TgModelEligibilityRecord_h


#include <atomic>
#include <memory>
#include <string>
#include <unordered_map>
#include "JsonTypeDefs.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "ScoreRecord.h"
#include "DateTimeUtil.h"
#include "Object.h"
/**
   documents the score of a tg versus a model that was calculated
 */
class TgModelEligibilityRecord : public Object {
public:
int targetGroupId;
int id;
float score;
float scoreDiffComparedToScoreBase;
std::string eligibile;
std::shared_ptr<std::vector<std::shared_ptr<ScoreRecord> > > scoreHistory;
TgModelEligibilityRecord() : Object(__FILE__){
        this->scoreHistory = std::make_shared<std::vector<std::shared_ptr<ScoreRecord> > > ();
        this->targetGroupId = 0;
        this->id = 0;
        this->score = 0;
        this->scoreDiffComparedToScoreBase = 0;
}

virtual ~TgModelEligibilityRecord(){
}

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "score", score, value);
        JsonUtil::addMemberToValue_FromPair (doc, "scoreDiffComparedToScoreBase", scoreDiffComparedToScoreBase, value);
        JsonUtil::addMemberToValue_FromPair (doc, "eligibile", eligibile, value);
        JsonArrayUtil::addMemberToValue_FromPair (doc, "scoreHistory", *scoreHistory, value);
}

static std::shared_ptr<TgModelEligibilityRecord> fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TgModelEligibilityRecord> scoreRecord = std::make_shared<TgModelEligibilityRecord>();

        scoreRecord->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        scoreRecord->id = JsonUtil::GetIntSafely(value, "id");
        scoreRecord->score = JsonUtil::GetDoubleSafely(value, "score");
        scoreRecord->scoreDiffComparedToScoreBase = JsonUtil::GetDoubleSafely(value, "scoreDiffComparedToScoreBase");
        scoreRecord->eligibile = JsonUtil::GetStringSafely(value, "eligibile");
        auto allScoreHistory = JsonArrayUtil::getArrayOfObjectsFromMemberInValue<ScoreRecord>(value, "scoreHistory");
        for (auto history : allScoreHistory) {
                scoreRecord->scoreHistory->push_back(history);
        }


        return scoreRecord;
}

std::string toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

static std::shared_ptr<TgModelEligibilityRecord> fromJson(std::string jsonString) {
        auto doc = JsonUtil::parseJsonSafely(jsonString);
        return TgModelEligibilityRecord::fromJsonValue(*doc);
}

};
#endif
