
#include "GicapodsIdToExchangeIdsMapCassandraService.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "CassandraDriverInterface.h"
#include "DateTimeUtil.h"
#include "ConfigService.h"

GicapodsIdToExchangeIdsMapCassandraService::GicapodsIdToExchangeIdsMapCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService)  :
        CassandraService(cassandraDriver,
                         entityToModuleStateStats,
                         
                         asyncThreadPoolService,
                         configService), Object(__FILE__) {
        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}


std::shared_ptr<GicapodsIdToExchangeIdsMap> GicapodsIdToExchangeIdsMapCassandraService::readGicapodsIdMappedToExchangeId(
        std::string partnerName,
        std::string partnerId) {
        assertAndThrow(!partnerId.empty());
        auto gicapodsIdToExchangeIdsMap = std::make_shared<GicapodsIdToExchangeIdsMap>();
        CassError rc = CASS_OK;
        CassStatement *statement = NULL;
        CassFuture *future = NULL;

        std::string queryStr = StringUtil::toStr (
                "SELECT nomadiniDeviceId, google, rubicon FROM gicapods.nomadiniDeviceIdToExchangeIdsMap "
                " where __partnerName__ = '__partnerId__'");

        queryStr = StringUtil::replaceString (queryStr, "__partnerId__", partnerId);
        queryStr = StringUtil::replaceString (queryStr, "__partnerName__", partnerName);


        const char *query = queryStr.c_str ();

        statement = cass_statement_new (query, 0);

        future = cass_session_execute (cassandraDriver->getSession (), statement);
        cass_future_wait_timed (future, 10);

        rc = cass_future_error_code (future);
        if (rc != CASS_OK) {
                cass_future_free (future);
                throwEx("error running query " + queryStr);
        } else {
                const CassResult *result = cass_future_get_result (future);

                CassIterator *rowIterator = cass_iterator_from_result (result);

                while (cass_iterator_next (rowIterator)) {
                        std::string nomadiniDeviceId =
                                cassandraDriver->getStringValueFromRowByName (rowIterator, "nomadiniDeviceId");

                        gicapodsIdToExchangeIdsMap->nomadiniDeviceId = nomadiniDeviceId;

                        std::string exchangeAId =
                                cassandraDriver->getStringValueFromRowByName (rowIterator, "google");
                        gicapodsIdToExchangeIdsMap->exchangeNameToExchangeId.insert(std::make_pair("google", exchangeAId));

                        std::string exchangeBId =
                                cassandraDriver->getStringValueFromRowByName (rowIterator, "rubicon");
                        gicapodsIdToExchangeIdsMap->exchangeNameToExchangeId.insert(std::make_pair("rubicon", exchangeAId));


                }

                cass_iterator_free (rowIterator);
                cass_result_free (result);
                cass_future_free (future);
                cass_statement_free (statement);
        }


        return gicapodsIdToExchangeIdsMap;
}
void GicapodsIdToExchangeIdsMapCassandraService::addThisExchangeIdToMap(
        std::string nomadiniDeviceId, std::string exchangeId, std::string exchangeName) {

        assertAndThrow(!nomadiniDeviceId.empty());
        assertAndThrow(!exchangeId.empty());
        assertAndThrow(!exchangeName.empty());
        CassError rc = CASS_OK;
        CassFuture *close_future = NULL;

        CassStatement *statement = NULL;
        CassFuture *future = NULL;


        std::string queryStr = "update gicapods.nomadiniDeviceIdToExchangeIdsMap "
                               " set __EXCHANGE_NAME__ = '__EXCHANGE_ID__' , dateCreated = '__dateCreated__' where nomadiniDeviceId = '__nomadiniDeviceId__'";

        queryStr = StringUtil::replaceString (queryStr,
                                              "__nomadiniDeviceId__", nomadiniDeviceId);
        queryStr = StringUtil::replaceString (queryStr, "__EXCHANGE_ID__", exchangeId);
        queryStr = StringUtil::replaceString (queryStr, "__EXCHANGE_NAME__", exchangeName);
        queryStr = StringUtil::replaceString(queryStr, "__dateCreated__",
                                             StringUtil::toStr(DateTimeUtil::getNowInYYYMMDDFormat()));

        const char *query = queryStr.c_str ();
        MLOG(3) << "queryStr to insert exchangeId map :" << queryStr;
        statement = cass_statement_new (query, 0);
        future = cass_session_execute (cassandraDriver->getSession (), statement);
        rc = cass_future_error_code(future);
        if (rc != CASS_OK) {
                const char* message;
                size_t message_length;
                cass_future_error_message(future, &message, &message_length);
                LOG_EVERY_N(ERROR, 100)<<google::COUNTER<<"th CasssandraDriver : Error: " <<  StringUtil::toStr(message);
        }

        cass_future_free (future);
        cass_statement_free (statement);

}
