/*
 * GicapodsIdToExchangeIdsMapCassandraService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef GicapodsIdToExchangeIdsMapCassandraService_H_
#define GicapodsIdToExchangeIdsMapCassandraService_H_

class CassandraDriverInterface;

#include "Object.h"
namespace gicapods { class ConfigService; }

#include "IpToDeviceIdsMap.h"
class CassandraDriverInterface;
#include "HttpUtilService.h"
class EntityToModuleStateStats;
#include "GicapodsIdToExchangeIdsMap.h"
#include "CassandraService.h"

class AsyncThreadPoolService;

class GicapodsIdToExchangeIdsMapCassandraService;



class GicapodsIdToExchangeIdsMapCassandraService :
        public CassandraService<GicapodsIdToExchangeIdsMap>, public Object {
private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;
public:
GicapodsIdToExchangeIdsMapCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

void addThisExchangeIdToMap(std::string nomadiniDeviceId,std::string partnerId, std::string exchangeName);

std::shared_ptr<GicapodsIdToExchangeIdsMap> readGicapodsIdMappedToExchangeId(
        std::string partnerName,
        std::string partnerId);
};


#endif /* GICAPODS_GICAPODSSERVER_SRC_CASSANDRA_IPDEVICEIDMAPCASSANDRASERVICE_H_ */
