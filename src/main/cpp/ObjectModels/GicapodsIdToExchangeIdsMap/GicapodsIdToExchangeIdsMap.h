#ifndef GicapodsIdToExchangeIdsMap_H
#define GicapodsIdToExchangeIdsMap_H


class StringUtil;

#include "JsonTypeDefs.h"

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "cassandra.h"
#include "CassandraManagedType.h"

class GicapodsIdToExchangeIdsMap;
#include "Object.h"


class GicapodsIdToExchangeIdsMap : public CassandraManagedType, public Object {

public:
//this value is used only when we use this class to batchWrite the mappings
std::string exchangeNameToBatchWrite;

std::string nomadiniDeviceId;
std::unordered_map<std::string, std::string> exchangeNameToExchangeId;

GicapodsIdToExchangeIdsMap();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toString();

std::string toJson();
static std::shared_ptr<GicapodsIdToExchangeIdsMap> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<GicapodsIdToExchangeIdsMap> fromJson(std::string json);
virtual ~GicapodsIdToExchangeIdsMap();
};


#endif
