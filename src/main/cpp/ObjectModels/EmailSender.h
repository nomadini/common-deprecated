

#ifndef EmailSender_H
#define EmailSender_H

#include <memory>
#include <string>
#include <vector>
#include "Poco/Net/SMTPClientSession.h"
#include "Object.h"
class EntityToModuleStateStats;

class EmailSender : public Object {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;


std::string mailhost;
std::string username;
std::string password;
int port;

EmailSender(std::string mailhost,
            std::string username,
            std::string password,
            int port);

void sendEmail(std::string sender,
               std::string recipient,
               std::string message,
               std::string subject);

virtual ~EmailSender();

};

#endif
