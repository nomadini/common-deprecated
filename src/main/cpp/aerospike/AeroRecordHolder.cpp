

#include "AeroRecordHolder.h"
#include "GUtil.h"

AeroRecordHolder::AeroRecordHolder() : Object(__FILE__) {
        this->record = NULL;
}

AeroRecordHolder::~AeroRecordHolder() {
        if (this->record) {
                as_record_destroy(record);
        }
}

std::string AeroRecordHolder::getName() {
        return "AeroRecordHolder";
}
