//
// Created by Mahmoud Taabodi on 7/12/16.
//
#include "GUtil.h"
#include "StringUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include <boost/foreach.hpp>
#include "AerospikeDriverNoOp.h"
#include "AeroRecordHolder.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#include <aerospike/aerospike.h>
#include <aerospike/aerospike_index.h>
#include <aerospike/aerospike_key.h>
#include <aerospike/aerospike_udf.h>
#include <aerospike/as_bin.h>
#include <aerospike/as_bytes.h>
#include <aerospike/as_error.h>
#include <aerospike/as_config.h>
#include <aerospike/as_key.h>
#include <aerospike/as_operations.h>
#include <aerospike/as_password.h>
#include <aerospike/as_record.h>
#include <aerospike/as_record_iterator.h>
#include <aerospike/as_status.h>
#include <aerospike/as_string.h>
#include <aerospike/as_val.h>
#include "example_utils.h"
#include "EntityToModuleStateStats.h"

AerospikeDriverNoOp::AerospikeDriverNoOp() {

}

void AerospikeDriverNoOp::removeRecordWithKey(std::string key, std::string namespaceName, std::string set) {

}

AerospikeDriverNoOp::~AerospikeDriverNoOp() {

}

std::string AerospikeDriverNoOp::get_cache(std::string snamespace,
                                           std::string sset,
                                           std::string skey,
                                           std::string sbin,
                                           int& cacheResultStatus)
{
        return "";
}

void AerospikeDriverNoOp::set_cache(std::string snamespace,
                                    std::string sset,
                                    std::string skey,
                                    std::string sbin,
                                    std::string svalue,
                                    int ttlInSeconds)
{

}


long AerospikeDriverNoOp::addValueAndRead(std::string snamespace,
                                          std::string sset,
                                          std::string skey,
                                          std::string sbin,
                                          int valueToAdd,
                                          int ttlInSeconds) {
        return 0;
}

std::string AerospikeDriverNoOp::readTheBin(std::string snamespace,
                                            std::string sset,
                                            std::string skey,
                                            std::string sbin) {
        return "";

}
std::string AerospikeDriverNoOp::addValueAndRead(std::string snamespace,
                                                 std::string sset,
                                                 std::string skey,
                                                 std::string sbin,
                                                 std::string valueToAdd,
                                                 int ttlInSeconds) {
        return "";
}

std::string AerospikeDriverNoOp::setValueAndRead(std::string snamespace,
                                                 std::string sset,
                                                 std::string skey,
                                                 std::string sbin,
                                                 std::string valueToAdd,
                                                 int ttlInSeconds) {
        return "";

}

void AerospikeDriverNoOp::set_cache_async(std::string snamespace,
                                          std::string sset,
                                          std::string skey,
                                          std::string sbin,
                                          std::string svalue,
                                          int ttlInSeconds)
{

}
