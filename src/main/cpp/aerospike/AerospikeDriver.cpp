//
// Created by Mahmoud Taabodi on 7/12/16.
//
#include "GUtil.h"
#include "StringUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include <boost/foreach.hpp>
#include "AerospikeDriver.h"
#include "AeroRecordHolder.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#include <aerospike/aerospike.h>
#include <aerospike/aerospike_index.h>
#include <aerospike/aerospike_key.h>
#include <aerospike/aerospike_udf.h>
#include <aerospike/as_bin.h>
#include <aerospike/as_bytes.h>
#include <aerospike/as_error.h>
#include <aerospike/as_config.h>
#include <aerospike/as_key.h>
#include <aerospike/as_operations.h>
#include <aerospike/as_password.h>
#include <aerospike/as_record.h>
#include <aerospike/as_record_iterator.h>
#include <aerospike/as_status.h>
#include <aerospike/as_string.h>
#include <aerospike/as_val.h>
#include "example_utils.h"
#include "EntityToModuleStateStats.h"

int AerospikeDriver::CACHE_HIT_WITH_VALUE = 500;
int AerospikeDriver::CACHE_HIT_WITH_EMPTY = 501;
int AerospikeDriver::CACHE_MISS = 502;
int AerospikeDriver::CACHE_ERROR = 503;

AerospikeDriver::AerospikeDriver(std::string host, int port, const char* lua_user_path) {

        numberOfCurrentReads = std::make_shared<gicapods::AtomicLong> ();
        numberOfCurrentWrites = std::make_shared<gicapods::AtomicLong>();

        stringValueSeperator = "^;^;^;";

        int g_port = port;
        auto g_host = host.c_str();


        auto g_user = "";
        auto g_password = "";

        // Initialize logging.
//        as_log_set_callback(example_log_callback);

        // Initialize default lua configuration.
        as_config_lua lua;
        as_config_lua_init(&lua);

        // Examples can be run from client binary package-installed lua files or
        // from git client source tree lua files. If client binary package is not
        // installed, look for lua system files in client source tree.
        int rc = access(lua.system_path, R_OK);

        if (rc != 0) {
                // Use lua files in source tree if they exist.
                std::string path = "../../../modules/lua-core/src";

                rc = access(path.c_str(), R_OK);

                if (rc == 0) {
                        strcpy(lua.system_path, path.c_str());
                }
        }

        if (lua_user_path) {
                strcpy(lua.user_path, lua_user_path);
        }

        // Initialize global lua configuration.
        aerospike_init_lua(&lua);

        // Initialize cluster configuration.
        as_config cfg;
        as_config_init(&cfg);
        as_config_add_host(&cfg, g_host, g_port);
        as_config_set_user(&cfg, g_user, g_password);

        aerospike_init(&as, &cfg);

        as_error err;

        // Connect to the Aerospike database cluster. Assume this is the first thing
        // done after calling example_get_opts(), so it's ok to exit on failure.
        if (aerospike_connect(&as, &err) != AEROSPIKE_OK) {
                LOG(ERROR)<<"aerospike_connect() returned "<<err.code << " - "<< err.message;
                aerospike_destroy(&as);
                throwEx("error connecting to aerospike");
        } else {
                LOG(INFO)<<"aerospike successful connection established";
        }
}

void AerospikeDriver::removeRecordWithKey(std::string key, std::string namespaceName, std::string set) {

        as_key g_key;
        // Initialize the test as_key object. We won't need to destroy it since it
        // isn't being created on the heap or with an external as_key_value.
        as_key_init_str(&g_key, namespaceName.c_str(), set.c_str(), key.c_str());

        as_error err;

        // Try to remove the test record from the database. If the example has not
        // inserted the record, or it has already been removed, this call will
        // return as_status AEROSPIKE_ERR_RECORD_NOT_FOUND - which we just ignore.
        aerospike_key_remove(&as, &err, NULL, &g_key);
}

AerospikeDriver::~AerospikeDriver() {
        as_error err;
        LOG(INFO)<<"Disconnecting from the aerospike database cluster and clean up the aerospike object.";

        aerospike_close(&as, &err);
        aerospike_destroy(&as);
}

std::string AerospikeDriver::get_cache(std::string snamespace,
                                       std::string sset,
                                       std::string skey,
                                       std::string sbin,
                                       int& cacheResultStatus)
{

        assertAndThrow(!snamespace.empty());
        assertAndThrow(!sset.empty());
        assertAndThrow(!skey.empty());
        assertAndThrow(!sbin.empty());
        assertAndThrow(!sbin.size() < 14);
        numberOfCurrentReads->increment();
        as_error err;

        //Initialize Key
        as_key key;
        as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

        //Read from database
        AeroRecordHolder aeroRecordHolder;
        const char* select[] = { sbin.c_str(), NULL };
        as_status statusOfRead = aerospike_key_select(&as, &err, NULL, &key, select, &aeroRecordHolder.record);
        if (statusOfRead != AEROSPIKE_OK && statusOfRead !=  AEROSPIKE_ERR_RECORD_NOT_FOUND) {
                LOG(ERROR)<<"aerospike_key_select returned "<<err.code << " - "<< err.message;
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_READING_VALUE",
                        "AerospikeDriver",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                cacheResultStatus = CACHE_ERROR;
                return "";
        }


        numberOfCurrentReads->decrement();
        assertAndThrow(numberOfCurrentReads->getValue() < 1000);

        if (statusOfRead != AEROSPIKE_ERR_RECORD_NOT_FOUND) {
                std::string valueStr;
                char* value = as_record_get_str(aeroRecordHolder.record, sbin.c_str());
                if (value != NULL) {
                        valueStr = StringUtil::toStr(value);
                        cacheResultStatus = CACHE_HIT_WITH_VALUE;
                } else {
                        cacheResultStatus = CACHE_HIT_WITH_EMPTY;
                }
                return valueStr;
        } else {
                cacheResultStatus = CACHE_MISS;
                return "";
        }
}

void AerospikeDriver::set_cache(std::string snamespace,
                                std::string sset,
                                std::string skey,
                                std::string sbin,
                                std::string svalue,
                                int ttlInSeconds)
{

        assertAndThrow(!snamespace.empty());
        assertAndThrow(!sset.empty());
        assertAndThrow(!skey.empty());
        assertAndThrow(!sbin.empty());
        assertAndThrow(!sbin.size() < 14);
        //sometimes we want value to be ""
        //so that we can cache the result of not finding a data for a key
        // assertAndThrow(!svalue.empty());
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        assertAndThrow(ttlInSeconds >= 10);
        as_error err;

        //Initialize Key
        as_key key;
        as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

        //Initialize Record Data
        as_record record;
        as_record_inita(&record, 1);
        as_record_set_str(&record, sbin.c_str(), svalue.c_str());
        record.ttl = ttlInSeconds;

        //Write to Database
        as_status status = aerospike_key_put(&as, &err, NULL, &key, &record);
        as_record_destroy(&record);
        if (status != AEROSPIKE_OK) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_PUTTING_VALUE",
                        "AerospikeDriver",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                throwEx ("writing value to cache " + StringUtil::toStr(err.message));
        }
}

void asyncOpListener(as_error* err, void* udata, as_event_loop* event_loop)
{
        if (err) {
                // printf("Command failed: %d %s\n", err->code, err->message);
                return;
        }
        // printf("Command succeeded\n");
}


long AerospikeDriver::addValueAndRead(std::string snamespace,
                                      std::string sset,
                                      std::string skey,
                                      std::string sbin,
                                      int valueToAdd,
                                      int ttlInSeconds) {
        assertAndThrow(!snamespace.empty());
        assertAndThrow(!sset.empty());
        assertAndThrow(!skey.empty());
        assertAndThrow(!sbin.size() < 14);
        assertAndThrow(!sbin.empty());
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        numberOfCurrentReads->increment();
        long valueToReturn = 0;

        as_operations ops;

        as_operations_inita(&ops, 2);//number Of operations
        as_operations_add_incr(&ops, sbin.c_str(), valueToAdd);
        as_operations_add_read(&ops, sbin.c_str());
        ops.ttl = ttlInSeconds; ///tthis is in second
        //for reference
        // as_operations_add_append_str(&ops, "bin2", "def");

        AeroRecordHolder rec;
        as_key key;
        as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

        as_error err;
        if (aerospike_key_operate(&as, &err, NULL, &key, &ops, &rec.record) != AEROSPIKE_OK) {

                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_ADD_READ_VALUE",
                        "AerospikeDriver",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
        } else {
                // printf("valueToReturn = %ld\n", as_record_get_int64(rec.record, sbin.c_str(), 0));
                valueToReturn = (long) as_record_get_int64(rec.record, sbin.c_str(), 0);
        }

        as_operations_destroy(&ops);

        numberOfCurrentReads->decrement();
        assertAndThrow(numberOfCurrentReads->getValue() < 1000);
        return valueToReturn;
}

std::string AerospikeDriver::readTheBin(std::string snamespace,
                                        std::string sset,
                                        std::string skey,
                                        std::string sbin) {
        assertAndThrow(!snamespace.empty());
        assertAndThrow(!sset.empty());
        assertAndThrow(!skey.empty());
        assertAndThrow(!sbin.empty());
        assertAndThrow(!sbin.size() < 14);
        numberOfCurrentReads->increment();
        std::string valueToReturn;

        as_operations ops;

        as_operations_inita(&ops, 1);                                  //number Of operations
        as_operations_add_read(&ops, sbin.c_str());

        AeroRecordHolder rec;
        as_error err;
        as_key key;
        as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

        if (aerospike_key_operate(&as, &err, NULL, &key, &ops, &rec.record) != AEROSPIKE_OK) {

                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_ADD_READ_VALUE",
                        "AerospikeDriver",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
        } else {
                char* valueToReturnChar =  as_record_get_str(rec.record, sbin.c_str());
                if (valueToReturnChar) {
                        valueToReturn = _toStr(valueToReturnChar);
                }
        }

        as_operations_destroy(&ops);

        numberOfCurrentReads->decrement();
        assertAndThrow(numberOfCurrentReads->getValue() < 1000);
        return valueToReturn;

}
std::string AerospikeDriver::addValueAndRead(std::string snamespace,
                                             std::string sset,
                                             std::string skey,
                                             std::string sbin,
                                             std::string valueToAdd,
                                             int ttlInSeconds) {
        assertAndThrow(!snamespace.empty());
        assertAndThrow(!sset.empty());
        assertAndThrow(!skey.empty());
        assertAndThrow(!sbin.empty());
        assertAndThrow(!sbin.size() < 14);
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        std::string valueToReturn;
        numberOfCurrentReads->increment();
        as_operations ops;

        as_operations_inita(&ops, 2);//number Of operations
        valueToAdd += stringValueSeperator;
        as_operations_add_append_str(&ops, sbin.c_str(), valueToAdd.c_str());
        as_operations_add_read(&ops, sbin.c_str());
        ops.ttl = ttlInSeconds; ///tthis is in second
        //for reference
        // as_operations_add_append_str(&ops, "bin2", "def");

        AeroRecordHolder rec;
        as_error err;
        as_key key;
        as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

        if (aerospike_key_operate(&as, &err, NULL, &key, &ops, &rec.record) != AEROSPIKE_OK) {
                //printf("error(%d) %s at [%s:%d]", err.code, err.message, err.file, err.line);
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_ADD_READ_VALUE",
                        "AerospikeDriver",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
        } else {
                char* valueToReturnChar =  as_record_get_str(rec.record, sbin.c_str());
                if (valueToReturnChar) {
                        valueToReturn = _toStr(valueToReturnChar);
                }
        }

        as_operations_destroy(&ops);

        numberOfCurrentReads->decrement();
        assertAndThrow(numberOfCurrentReads->getValue() < 1000);

        return valueToReturn;
}

std::string AerospikeDriver::setValueAndRead(std::string snamespace,
                                             std::string sset,
                                             std::string skey,
                                             std::string sbin,
                                             std::string valueToAdd,
                                             int ttlInSeconds) {
        assertAndThrow(!snamespace.empty());
        assertAndThrow(!sset.empty());
        assertAndThrow(!skey.empty());
        assertAndThrow(!sbin.empty());
        assertAndThrow(!sbin.size() < 14);
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        numberOfCurrentReads->increment();
        std::string valueToReturn;

        as_operations ops;

        as_operations_inita(&ops, 2);//number Of operations
        // valueToAdd += stringValueSeperator;
        as_operations_add_write_str(&ops, sbin.c_str(), valueToAdd.c_str());
        as_operations_add_read(&ops, sbin.c_str());
        ops.ttl = ttlInSeconds; ///tthis is in second
        //for reference
        // as_operations_add_append_str(&ops, "bin2", "def");

        AeroRecordHolder rec;
        as_error err;
        as_key key;
        as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

        if (aerospike_key_operate(&as, &err, NULL, &key, &ops, &rec.record) != AEROSPIKE_OK) {
                //printf("error(%d) %s at [%s:%d]", err.code, err.message, err.file, err.line);
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_ADD_READ_VALUE",
                        "AerospikeDriver",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
        } else {
                char* valueToReturnChar =  as_record_get_str(rec.record, sbin.c_str());
                if (valueToReturnChar) {
                        valueToReturn = _toStr(valueToReturnChar);
                }
        }

        as_operations_destroy(&ops);
        numberOfCurrentReads->decrement();
        assertAndThrow(numberOfCurrentReads->getValue() < 1000);

        return valueToReturn;

}

void AerospikeDriver::set_cache_async(std::string snamespace,
                                      std::string sset,
                                      std::string skey,
                                      std::string sbin,
                                      std::string svalue,
                                      int ttlInSeconds)
{
        assertAndThrow(!snamespace.empty());
        assertAndThrow(!sset.empty());
        assertAndThrow(!skey.empty());
        assertAndThrow(!sbin.empty());
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        assertAndThrow(!sbin.size() < 14);
        as_error err;

        //Initialize Key
        as_key key;
        as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

        //Initialize Record Data
        as_record record;
        as_record_inita(&record, 1);
        as_record_set_str(&record, sbin.c_str(), svalue.c_str());

        //for reference
        // as_record_set_str(&rec, "bin1", "abc");
        // as_record_set_int64(&rec, "bin2", 123);

        record.ttl = ttlInSeconds;

        //Write to Database
        as_status status = aerospike_key_put_async(&as, &err, NULL, &key, &record, asyncOpListener, NULL, NULL, NULL);
        as_record_destroy(&record);

        if (status != AEROSPIKE_OK) {
                //fprintf(stderr, "err(%d) %s at [%s:%d]\n", err.code, err.message, err.file, err.line);
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_PUT_VALUE_ASYNC_VALUE",
                        "AerospikeDriver",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                throwEx ("writing value to cache " + StringUtil::toStr(err.message));
        }
}
