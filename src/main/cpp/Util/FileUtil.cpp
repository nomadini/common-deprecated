
#include <fstream>
#include <iostream>
#include "StringUtil.h"
#include "FileUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <boost/filesystem.hpp>
#include "Poco/File.h"

#include <Poco/DirectoryIterator.h>
#include <Poco/DateTimeFormatter.h>
#include <Poco/LocalDateTime.h>
#include <iostream>
#include <string>
#include "ExceptionUtil.h"
#include "DateTimeUtil.h"

bool FileUtil::createDirectory(const std::string& nameOfDir) {
								MLOG(10) <<"going to create directory "<< nameOfDir;
								boost::filesystem::path dir(nameOfDir.c_str());
								if(boost::filesystem::create_directories(dir)) {
																MLOG(10) << nameOfDir << " directory was created";
																return true;
								} else {
																MLOG(10) << nameOfDir << " failure : directory was NOT created";
																return false;
								}


}
std::shared_ptr<std::ofstream> FileUtil::getAppenderStreamOfFile(const std::string& filename) {
								std::shared_ptr<std::ofstream> appender(new std::ofstream(filename.c_str(), std::ios_base::app | std::ios_base::out));
								return appender;
}

void FileUtil::appendALineToFile(std::shared_ptr<std::ofstream> log, const std::string& fileContent) {
								(*log) << fileContent;
}


void FileUtil::appendALineToFile(const std::string& filename, const std::string& fileContent) {
								std::ofstream log(filename.c_str(), std::ios_base::app | std::ios_base::out);
								log << fileContent;
}

std::string FileUtil::getCurrentDirectory() {
								char cwd[1024];
								if (getcwd(cwd, sizeof(cwd)) != NULL) {
//			fprintf(stdout, "Current working dir: %s\n", cwd);

																std::string curDir(cwd);
																return curDir;
								}
								throwEx("failed to get Current directory ");
}

std::string FileUtil::readFileInString(const std::string& filename) {

								std::vector<char> allChar;
								char ch;
								std::fstream fin(filename, std::fstream::in);
								while (fin >> std::noskipws >> ch) {
																allChar.push_back(ch);
								}
								std::string fileContent(allChar.begin(), allChar.end());

								return fileContent;
}

std::string FileUtil::readQueryBodyFromFile(std::string filePath) {
								std::string path = getCurrentDirectory();
								std::string fullPath = StringUtil::toStr(path) + filePath;
								//fprintf(stdout, "Current fullPath: %s\n", fullPath.c_str());
								const std::string& fileContent = readFileInString(fullPath);
								//traceme(fileContent);
								return fileContent;
}

int FileUtil::getCountOfLinesInFile(const std::string& filename) {
								int count = 0;
								std::ifstream myfile(filename.c_str());
								std::string line;
								if (myfile.is_open()) {
																while (getline(myfile, line)) {
																								count++;
																}
																myfile.close();
								} else {
																std::cout << "couldn't read the file";
								}
								return count;
}

bool FileUtil::checkIfFileExists(const std::string& name) {
								try {
																auto file = std::make_shared<Poco::File> (name);
																return file->exists();
								} catch(const std::exception& e) {
																ExceptionUtil::logException(
																								e,
																								nullptr,
																								1);
								}

								throwEx("exception thrown");
}

void FileUtil::createFileIfNotExist(const std::string& name) {
								auto file = std::make_shared<Poco::File> (name);
								file->createFile();
}
std::vector<std::string> FileUtil::readFileLineByLine(const std::string& filename) {

								std::vector<std::string> lines;
								std::ifstream myfile(filename.c_str());
								if (myfile.is_open()) {
																std::string line;
																while (getline(myfile, line)) {
																								lines.push_back(line);
																}
																myfile.close();
								}

								return lines;
}
void FileUtil::writeStringToFile(const std::string& filename, const std::string& fileContent) {

								std::ofstream outfile;
								outfile.open(filename.c_str(), std::ios_base::out);
								outfile << fileContent;
								outfile.close();

}

int FileUtil::deleteFile(const std::string& name) {
								assertAndThrow(!name.empty());
								return remove(name.c_str());
}

int FileUtil::getLastModifiedInSeconds(const std::string& name) {
								auto file = std::make_shared<Poco::File> (name);
								return file->getLastModified().epochMicroseconds() / 1000000;
}

int FileUtil::deleteFileOlderThanXMinutesInDir(const std::string& directory, int minutes) {
								auto allFiles = getAllFilesOlderThanXMinutesInDir(directory, minutes);
								int numberOfFilesDeleted = 0;
								for (auto fileName : allFiles) {
																LOG(ERROR)<<"deleting file : "<< fileName;
																numberOfFilesDeleted += deleteFile(fileName);
								}
								return numberOfFilesDeleted;
}

std::vector<std::string> FileUtil::getAllFilesOlderThanXMinutesInDir(const std::string & directory, int minutes) {
								std::vector<std::string> allFiles;
								Poco::DirectoryIterator end;
								long nowInSecond = DateTimeUtil::getNowInSecond();
								for (Poco::DirectoryIterator it(directory); it != end; ++it)
								{
																if (!it->isDirectory()) {
																								long lastModifiedInMinutes = it->getLastModified().epochMicroseconds() / (1000000 * 60);
																								long nowInMinutes = (nowInSecond / 60);
																								if ( nowInMinutes - lastModifiedInMinutes > minutes) {
																																allFiles.push_back(it->path());
																								}
																}

								}
								return allFiles;
}

void FileUtil::printDirectoryContentRecursively(const std::string & path)
{
								Poco::DirectoryIterator end;
								for (Poco::DirectoryIterator it(path); it != end; ++it)
								{
																std::cout << (it->isDirectory() ? "d" : "-");
																std::cout << (it->canRead()     ? "r" : "-");
																std::cout << (it->canWrite()    ? "w" : "-");
																std::cout << (it->canExecute()  ? "x" : "-");
																std::cout << "\t";

																std::cout << it->getSize() << "\t";

																Poco::LocalDateTime lastModified(it->getLastModified());
																std::cout << Poco::DateTimeFormatter::format
																(lastModified, "%Y-%m-%d %H:%M") << "\t";

																std::cout << it->path() << (
																								it->isDirectory() ?
																								"/" : it->canExecute() ? "*" : "") << std::endl;

																if (it->isDirectory())
																{
																								printDirectoryContentRecursively(it->path());
																}
								}
}
