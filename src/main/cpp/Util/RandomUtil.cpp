#include "RandomUtil.h"
#include "GUtil.h"
#include <boost/multiprecision/random.hpp>
#include <random>
#include <iostream>
#include <stdlib.h>

RandomUtil::RandomUtil(int longUpperRange) : Object(__FILE__) {

        assertAndThrow(longUpperRange >= 0);

        longUpperRangeInclusive = longUpperRange - 1;
        randomDevice = std::make_shared<std::random_device>();
        longRandomGenerator = std::make_shared<std::mt19937> ((*randomDevice)());
        longRandomFinder = std::make_shared<std::uniform_int_distribution<> >(0, longUpperRangeInclusive);

}

long RandomUtil::randomNumberV2() {
        assertAndThrow(longUpperRangeInclusive >= 0);

        return (*longRandomFinder)(*longRandomGenerator);
}

int RandomUtil::sudoRandomNumber(int range) {
        assertAndThrow(range > 0);
        static bool first = true;
        if ( first )
        {
                srand(time(NULL)); //seeding for the first time only!
                first = false;
        }
        return rand() % range + 1;
}

RandomUtil::~RandomUtil() {

}
