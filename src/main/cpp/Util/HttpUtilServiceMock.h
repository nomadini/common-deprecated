/*
 * HttpUtilServiceMock.h
 *
 *  Created on: Aug 2, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_HttpUtilServiceMock_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_HttpUtilServiceMock_H_




#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerResponseImpl.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPCookie.h"

#include <string>
#include <memory>
#include <curl/curl.h>
#include <unordered_map>
#include "HttpUtilService.h"
#include "gmock/gmock.h"

class HttpUtilServiceMock;



class HttpUtilServiceMock : public HttpUtilService{

public:

    MOCK_METHOD3(sendPostRequest, std::string (const std::string& url ,const std::string& requestBody, int timeoutInMillis));

};
#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_HttpUtilServiceMock_H_ */
