/*
 * TempUtil.h
 *
 *  Created on: Feb 16, 2016
 *      Author: mtaabodi

 */


#include "LogLevelManager.h"
#include "GUtil.h"
#include "SignalHandler.h"
#include "StringUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "ConfigService.h"
#include "CollectionUtil.h"
#include <thread>
#include <boost/exception/all.hpp>
#include "Poco/Exception.h"
#include "ExceptionUtil.h"
#include "EntityToModuleStateStats.h"

std::string ExceptionUtil::logException(
        const std::exception& e,
        EntityToModuleStateStats* entityToModuleStateStats,
        int freqToLog) {

        auto diagnosticInformation = boost::diagnostic_information (e);
        LOG_EVERY_N(ERROR, freqToLog) << "Got the " << google::COUNTER << "th exception "
                                      <<" while handling request : "<<diagnosticInformation;

        std::string pocoMsg;
        Poco::SystemException* assertException = dynamic_cast<Poco::SystemException*>(const_cast<std::exception*>(&e));
        if (assertException) {
                pocoMsg = assertException->message();
        }

        Poco::AssertionViolationException* assertException1 =
                dynamic_cast<Poco::AssertionViolationException*>(const_cast<std::exception*>(&e));
        if(assertException1 && pocoMsg.empty()) {
                pocoMsg = assertException1->message();
        }

        Poco::FileException* fileException =
                dynamic_cast<Poco::FileException*>(const_cast<std::exception*>(&e));
        if(fileException  && pocoMsg.empty()) {
                pocoMsg = fileException->message();
        }

        Poco::Exception* generalException =
                dynamic_cast<Poco::Exception*>(const_cast<std::exception*>(&e));
        if(generalException && pocoMsg.empty()) {
                pocoMsg = generalException->message();
        }

        if (gicapods::Util::allowedToCall(_FL_, freqToLog)) {
                gicapods::Util::showStackTrace();
        }

        if (entityToModuleStateStats != nullptr) {
                entityToModuleStateStats->addStateModuleForEntity("EXCEPTION_"+diagnosticInformation,
                                                                  "ExceptionUtil",
                                                                  "ALL",
                                                                  EntityToModuleStateStats::exception);

        }
        LOG_EVERY_N(ERROR, freqToLog) << "poco exception message: "<< pocoMsg;
        return pocoMsg;
}
