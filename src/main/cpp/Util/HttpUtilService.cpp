

#include <curl/curl.h>
#include "GUtil.h"
#include "HttpUtilService.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include "Poco/URI.h"
#include <string>
#include <memory>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/NameValueCollection.h"
#include "ConfigService.h"
#include "GUtil.h"
#include <boost/foreach.hpp>

HttpUtilService::HttpUtilService() : Object(__FILE__) {

}

std::unordered_map<std::string, std::string>  HttpUtilService::getMapOfQueryParams(Poco::Net::HTTPServerRequest& request) {
								return HttpUtil::getMapOfQueryParams(request);

}
void HttpUtilService::printQueryParamsOfRequest(Poco::Net::HTTPServerRequest & request) {
								HttpUtil::printQueryParamsOfRequest(request);
}


std::string HttpUtilService::getDomainFromUrl(std::string url) {
								return HttpUtilService::getDomainFromUrl(url);
}

std::string HttpUtilService::getRequestBody(Poco::Net::HTTPServerRequest& request) {
								return HttpUtil::getRequestBody(request);
}

void HttpUtilService::sendHttpPostViaCurl(
								const std::string & url,
								const std::string & postData,
								int numberOfTries) {
								HttpUtil::sendHttpPostViaCurl(url, postData, numberOfTries);
}

std::string HttpUtilService::sendGetRequestAndGetFullResponse(
								const std::string &  cookieKey,  const std::string &  cookieValue,
								Poco::Net::HTTPResponse& response,
								const std::string & url,
								int timeoutInMillis,
								int numberOfTries) {
								return HttpUtil::sendGetRequestAndGetFullResponse(
																cookieKey,
																cookieValue,
																response,
																url,
																timeoutInMillis,
																numberOfTries);

}

std::string HttpUtilService::sendGetRequestAndGetFullResponse(
								const std::string & url,
								Poco::Net::HTTPResponse& response,
								int timeoutInMillis,
								int numberOfTries)
{
								return HttpUtil::sendGetRequestAndGetFullResponse(
																url,
																response,
																timeoutInMillis,
																numberOfTries);
}
std::string HttpUtilService::sendGetRequest(
								const std::string & url,
								int timeoutInMillis,
								int numberOfTries)
{
								return HttpUtil::sendGetRequest(
																url,
																timeoutInMillis,
																numberOfTries);
}

std::string HttpUtilService::sendPostRequest(
								Poco::Net::HTTPClientSession& session,
								const std::string & url,
								const std::string & requestBody,
								int numberOfTries) {
								return HttpUtil::sendPostRequest(
																session,
																url,
																requestBody,
																numberOfTries);
}

std::string HttpUtilService::sendPostRequest(
								const std::string & url,
								const std::string & requestBody,
								int timeoutInMillis,
								int numberOfTries) {
								return HttpUtil::sendPostRequest(
																url,
																requestBody,
																timeoutInMillis,
																numberOfTries);
}

void HttpUtilService::printRequestProperties(Poco::Net::HTTPServerRequest& request) {
								HttpUtil::printRequestProperties(request);

}

void HttpUtilService::printCookies(Poco::Net::HTTPServerRequest& request) {
								HttpUtil::printCookies(request);
}

void HttpUtilService::setBadRequestResponse(Poco::Net::HTTPServerResponse& response, std::string msg) {
								HttpUtil::setBadRequestResponse(
																response,
																msg);
}


void HttpUtilService::setEmptyResponse(Poco::Net::HTTPServerResponse& response) {
								HttpUtil::setEmptyResponse(response);
}

std::string HttpUtilService::getCookieFromResponse(Poco::Net::HTTPResponse& response,  const std::string& cookieName) {

								HttpUtil::getCookieFromResponse(response,cookieName);

}

std::unordered_map<std::string, std::string> HttpUtilService::getCookiesFromResponse(Poco::Net::HTTPResponse& response) {
								HttpUtil::getCookiesFromResponse(response);
}

std::string HttpUtilService::getDomainFromRequest(Poco::Net::HTTPServerRequest& request) {
								return HttpUtil::getDomainFromRequest(request);
}

std::unordered_map<std::string, std::string>
HttpUtilService::getCookiesFromRequest(Poco::Net::HTTPServerRequest& request) {
								return HttpUtil::getCookiesFromRequest(request);
}

void HttpUtilService::setOneCookieOnRequest(
								Poco::Net::HTTPRequest& request,
								const std::string& key,
								const std::string& value) {
								HttpUtil::setOneCookieOnRequest(
																request,
																key,
																value);
}
void HttpUtilService::setCookiesOnRequest(Poco::Net::HTTPRequest& request, Poco::Net::NameValueCollection cookies) {
								HttpUtil::setCookiesOnRequest(request,cookies);
}

void HttpUtilService::setCookiesOnRequest(Poco::Net::HTTPRequest& request, std::unordered_map<std::string, std::string> cookies) {
								HttpUtil::setCookiesOnRequest(request, cookies);
}

std::shared_ptr<HttpUtilService>
HttpUtilService::getInstance(gicapods::ConfigService* configService) {
								static auto instance = std::make_shared<HttpUtilService>();
								return instance;
}
