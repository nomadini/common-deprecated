
#include "GUtil.h"
#include "TempUtil.h"
#include <chrono>
#include <thread>

#include "StringUtil.h"
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <exception>
#include <iostream>
#include <cxxabi.h>

#include <boost/exception/all.hpp>

//this is needed for the showStackTrace function
#define UNW_LOCAL_ONLY

#include <libunwind.h>
#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <tbb/concurrent_hash_map.h>
namespace gicapods {

std::shared_ptr<tbb::concurrent_hash_map<std::string, long> >
Util::filelineNumberToCallCount = std::make_shared<tbb::concurrent_hash_map<std::string, long> >();
bool Util::exitOnErrorConditions = true;
int gicapods::Util::getProcessId() {
								LOG(INFO) << "Process ID "<< getpid();
								return getpid();
}

void gicapods::Util::myterminate() {

								LOG(ERROR)<<"terminate handler called!, pay attention to thread number!";
								showStackTrace();
}

int gicapods::Util::parseLine(char* line) {
								int i = strlen(line);
								while (*line < '0' || *line > '9')
																line++;
								line[i - 3] = '\0';
								i = atoi(line);
								return i;
}

int gicapods::Util::getMemoryUsedByProcess() { //Note: this value is in KB!
								FILE* file = fopen("/proc/self/status", "r");
								int result = -1;
								char line[128];

								while (fgets(line, 128, file) != NULL) {
																if (strncmp(line, "VmSize:", 7) == 0) {
																								result = parseLine(line);
																								break;
																}
								}
								fclose(file);
								return result;
}

bool gicapods::Util::allowedToCall(std::string fileLineNum, int everyNtimes) {
								tbb::concurrent_hash_map<std::string, long>::accessor accessor;
								long count = 1;
								if (!filelineNumberToCallCount->find(accessor, fileLineNum)) {
																filelineNumberToCallCount->insert(accessor, fileLineNum);
																accessor->second = 1;
																count = accessor->second;
								} else {
																accessor->second++;
																count = accessor->second;
								}

								if (count % everyNtimes == 0 && count > 1) {
																return true;
								} else {
																return false;
								}
}
//////////////////////////////////////////////////////////////////////////////
//
// process_mem_usage(double &, double &) - takes two doubles by reference,
// attempts to read the system-dependent data for a process' virtual memory
// size and resident set size, and return the results in KB.
//
// On failure, returns 0.0, 0.0
void gicapods::Util::getMemoryUsage(double& vm_usage, double& resident_set) {

								using std::ios_base;
								using std::ifstream;
								using std::string;

								vm_usage     = 0.0;
								resident_set = 0.0;

								// 'file' stat seems to give the most reliable results
								//
								ifstream stat_stream("/proc/self/stat",ios_base::in);

								// dummy vars for leading entries in stat that we don't care about
								//
								string pid, comm, state, ppid, pgrp, session, tty_nr;
								string tpgid, flags, minflt, cminflt, majflt, cmajflt;
								string utime, stime, cutime, cstime, priority, nice;
								string O, itrealvalue, starttime;

								// the two fields we want
								//
								unsigned long vsize;
								long rss;

								stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
								>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
								>> utime >> stime >> cutime >> cstime >> priority >> nice
								>> O >> itrealvalue >> starttime >> vsize >> rss;         // don't care about the rest

								stat_stream.close();

								long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024;         // in case x86-64 is configured to use 2MB pages
								vm_usage     = vsize / 1024.0;
								resident_set = rss * page_size_kb;
}

void gicapods::Util::printMemoryUsage() {
								double virtualMemory = 0;
								double residentMemory = 0;

								gicapods::Util::getMemoryUsage(virtualMemory, residentMemory);
								LOG(ERROR) << "virtualMemory used in GB : "<< virtualMemory / (1024 * 1024);
								LOG(ERROR) << "residentMemory used in GB : "<< residentMemory / (1024 * 1024);
}

void gicapods::Util::assertAndWarn_(const std::string& caller, bool resultOfOperation,
																																				const std::string& operation) {
								if(!resultOfOperation) {
																LOG(WARNING)<<"caller:  "<<caller<<", result of '"<<operation<<"' is not true";
								}
}

void gicapods::Util::assertAndThrow_(const std::string& caller,
																																					bool resultOfOperation,
																																					const std::string& operation) {
								if(!resultOfOperation) {
																LOG(ERROR)<<"caller:  "<<caller<<", result of '" << operation;
																showStackTrace();
																throw std::logic_error("result of operation is not true at " + caller + " operation : " + operation);
								}
}

void gicapods::Util::throwEx_(
								const std::string& caller,
								const std::string& msg,
								bool loud) {
								LOG_EVERY_N(ERROR, 1000)<<google::COUNTER<<"th caller:  "<<caller<<", throwing exception : "<<msg;
								if (loud) {
																//this is too much but its good to see where exception is throwing from
																//by default its off
																showStackTrace();
								}
								throw std::logic_error(msg);
}

void gicapods::Util::sleepMicroSecond(int microseconds) {
								std::this_thread::sleep_for(std::chrono::nanoseconds(microseconds * 1000));
}

void gicapods::Util::sleepNanoSecond(int nanoseconds) {
								std::this_thread::sleep_for(std::chrono::nanoseconds(nanoseconds));
}

template<typename T>
T gicapods::Util::calc_hash_code(T obj) {

								std::string objStr = StringUtil::toStr(obj);
								std::size_t hash_code = std::hash<std::string>()(objStr);

								return hash_code;

}

//this macro is just for debugging

void gicapods::Util::sleepMillis(int millis) {

								// boost::this_thread::sleep(boost::posix_time::seconds(seconds));
								//this throws some weird exception
								std::chrono::milliseconds dura(millis);
								std::this_thread::sleep_for(dura);

}

void gicapods::Util::sleepMiliSecond(int x) {
								std::chrono::milliseconds dura(x);
								std::this_thread::sleep_for(dura);
}

void gicapods::Util::_exit(std::string caller, std::string msg) {
								LOG(ERROR)<< "exiting based on unacceptable condition at "<< caller<<
																"error message : "<<msg;
								gicapods::Util::showStackTrace();
								if (exitOnErrorConditions) {
																exit(1);
								}
}

void gicapods::Util::sleepViaBoost(std::string caller, int seconds) {

								//TODO : change this back to 1
								if(seconds < 0) {
																throwEx(caller + " can't sleep less than 1 seconds");
								}
								// LOG_EVERY_N(INFO, 100)<<caller <<" sleeping for "<<seconds<<" seconds";
								std::chrono::milliseconds dura(seconds * 1000);
								std::this_thread::sleep_for(dura);
								// LOG_EVERY_N(INFO, 100)<<caller <<" waking up after "<<seconds<<" seconds";
}

double gicapods::Util::getLatLonDistanceInMile(double lat1d, double lon1d,double lat2d, double lon2d) {
								double distanceInMile = distanceEarthInKm(lat1d, lon1d, lat2d, lon2d) * MILE;
								return distanceInMile;
}

// This function converts decimal degrees to radians
double gicapods::Util::deg2rad(double deg) {
								return (deg * pi_gicapods / 180);
}

//  This function converts radians to decimal degrees
double gicapods::Util::rad2deg(double rad) {
								return (rad * 180 / pi_gicapods);
}

/**
 * Returns the distance between two points on the Earth.
 * Direct translation from http://en.wikipedia.org/wiki/Haversine_formula
 * @param lat1d Latitude of the first point in degrees
 * @param lon1d Longitude of the first point in degrees
 * @param lat2d Latitude of the second point in degrees
 * @param lon2d Longitude of the second point in degrees
 * @return The distance between the two points in kilometers
 */
double gicapods::Util::distanceEarthInKm(double lat1d, double lon1d, double lat2d, double lon2d) {
								double lat1r, lon1r, lat2r, lon2r, u, v;
								lat1r = deg2rad(lat1d);
								lon1r = deg2rad(lon1d);
								lat2r = deg2rad(lat2d);
								lon2r = deg2rad(lon2d);
								u = sin((lat2r - lat1r) / 2);
								v = sin((lon2r - lon1r) / 2);
								return 2.0 * earthRadiusKm
															* asin(sqrt(u * u + cos(lat1r) * cos(lat2r) * v * v));

}

void gicapods::Util::setThreadName(const pthread_t& thread, const std::string & name) {
								pthread_setname_np(thread, name.c_str());
}

std::string gicapods::Util::methodName(const std::string& prettyFunction) {
								size_t colons = prettyFunction.find("::");
								size_t begin = prettyFunction.substr(0, colons).rfind(" ") + 1;
								size_t end = prettyFunction.rfind("(") - begin;

								return prettyFunction.substr(begin, end) + "()";
}


std::string gicapods::Util::className(const std::string& prettyFunction) {
								size_t colons = prettyFunction.find("::");
								if (colons == std::string::npos)
																return "::";
								size_t begin = prettyFunction.substr(0, colons).rfind(" ") + 1;
								size_t end = colons - begin;

								return prettyFunction.substr(begin, end);
}

void gicapods::Util::printTimeOfRunInSec(Poco::Timestamp& now) {
								Poco::Timestamp::TimeDiff diff = now.elapsed(); // how long did it take?
								LOG(INFO)<<"Latency  "<<diff / 1000000<<" seconds";
}

void gicapods::Util::printTimeOfRunInMilis(Poco::Timestamp& now) {
								Poco::Timestamp::TimeDiff diff = now.elapsed();
								LOG(INFO)<<"Latency  "<<diff / 1000<<" milis";
}

/**
   http://blog.bigpixel.ro/2010/09/stack-unwinding-stack-trace-with-gcc/
 **/

void gicapods::Util::showStackTrace(std::exception const* e) {
								if (e) {
																LOG(ERROR) << " exception thrown: " << boost::diagnostic_information (*e);
								}


								char name[256];
								unw_cursor_t cursor;
								unw_context_t uc;
								unw_word_t ip, sp, offp;

								unw_getcontext (&uc);
								unw_init_local (&cursor, &uc);
								LOG(ERROR) << "---------------beginning of stacktrace---------------------";
								while (unw_step (&cursor) > 0) {
																char file[256];
																int line = 0;
																name[0] = '\0';
																unw_get_proc_name (&cursor, name, 256, &offp);
																unw_get_reg (&cursor, UNW_REG_IP, &ip);
																unw_get_reg (&cursor, UNW_REG_SP, &sp);

																int status;
																//A pointer to the start of the NUL-terminated demangled name, or NULL if the demangling fails. The caller is responsible for deallocating this memory using free.
																char* realname = abi::__cxa_demangle(name, 0, 0, &status);
																if (realname !=nullptr) {
																								std::shared_ptr<char> p(realname, &free);
																								std::string str(p.get());
																								LOG(ERROR) << str;
																} else {
																								//demangling has failed!!!
																								//LOG(ERROR) << "demangling failed!";
																}

								}

								LOG(ERROR) << "-----------------end of stacktrace-------------------------";

}
}
