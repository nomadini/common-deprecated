/*
 * MemoryUtil.h
 *
 *  Created on: Feb 16, 2016
 *      Author: mtaabodi

 */


#include "GUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include <sys/resource.h>
#include "MemoryUtil.h"

void MemoryUtil::setStackSize(int megabytes) {

        const rlim_t kStackSize = megabytes * 1024 * 1024; // min stack size = 16 MB
        struct rlimit rl;
        int result;

        result = getrlimit(RLIMIT_STACK, &rl);
        if (result == 0)
        {
                if (rl.rlim_cur < kStackSize)
                {
                        rl.rlim_cur = kStackSize;
                        result = setrlimit(RLIMIT_STACK, &rl);
                        if (result != 0)
                        {
                                fprintf(stderr, "setrlimit returned result = %d\n", result);
                                throwEx("error in setting stack");
                        }
                }
        } else {
                throwEx("error in getting stack");
        }
}
