#ifndef JSON_MAP_UTIL_h
#define JSON_MAP_UTIL_h





#include "rapidjson/document.h"
#include "JsonTypeDefs.h"
#include "JsonArrayUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include <unordered_map>
#include <memory>
#include <string>
#include <vector>
#include <set>

#include <tbb/concurrent_hash_map.h>

namespace gicapods {
template <class K, class V>
class ConcurrentHashMap;
};
//using unnamed namespace to fix the multiple definition problem
namespace {
class JsonMapUtil {

public:


template <class T>
struct is_shared_ptr {
								static const bool value = false;
};

template <class T>
struct is_shared_ptr<std::shared_ptr<T> > {
								static const bool value = true;
};

/**
   writes a map like this ot object value
   "sports" : {
           "soccer.com":23.12,
           "cooking.com":32.23,
           "shipping.com":2.32,
           "booking.com":23.12
           }

 */
template<typename K, typename V>
inline static void addMemberToValue_From_NameMapPair(
								DocumentType* doc,
								std::string nameOfMember,
								std::unordered_map <K, V> keyValueMap,
								RapidJsonValueType parentValue) {

								RapidJsonValueTypeNoRef pairValue;
								pairValue.SetObject();

								for (auto &keyValue : keyValueMap) {
																JsonUtil::addMemberToValue_FromPair(
																								doc,
																								keyValue.first,
																								keyValue.second,
																								pairValue);
								}

								JsonUtil::addMemberToValue_FromPair(
																doc,
																nameOfMember,
																pairValue,
																parentValue);
}
/**
   writes a map like this ot object value
   "sports" : {
           "soccer.com":23.12,
           "cooking.com":32.23,
           "shipping.com":2.32,
           "booking.com":23.12
           }

 */

/*
   populates a map of K, V from a memberValue
   that has a content like this
   "sports" : {
   "soccer.com":23.12,
   "cooking.com":32.23,
   "shipping.com":2.32,
   "booking.com":23.12
   }
 */
template<typename K, typename V>
inline static void read_Map_From_Value_Member(
								RapidJsonValueType valueParent,
								const std::string& elementName,
								std::unordered_map<K, V>& map) {

								const char *nameOfElement = elementName.c_str();
								if(!valueParent.HasMember (elementName.c_str())) {
																throwEx(elementName + " doesnt exist in value");
								}

								RapidJsonValueType valueDocument = (valueParent)[nameOfElement];
								for (auto m = valueDocument.MemberBegin ();
													m != valueDocument.MemberEnd (); ++m) {
																std::string name;
																JsonUtil::GetValueUnsafely<std::string> (m->name, name);
																V value;
																JsonUtil::GetValueUnsafely<V>(m->value, value);
																map.insert(std::pair<K,V> (name, value));
								}
}

template<typename K, typename V>
inline static void read_Map_From_Value_Member(
								RapidJsonValueType valueParent,
								const std::string& elementName,
								std::unordered_map<K, V>& map,
								std::function<void(V& value, RapidJsonValueType valueType)> valuePopulator) {

								const char *nameOfElement = elementName.c_str();
								if(!valueParent.HasMember (elementName.c_str())) {
																throwEx(elementName + " doesnt exist in value");
								}

								RapidJsonValueType valueDocument = (valueParent)[nameOfElement];
								for (auto m = valueDocument.MemberBegin ();
													m != valueDocument.MemberEnd (); ++m) {
																std::string name;
																JsonUtil::GetValueUnsafely<std::string> (m->name, name);
																V value;
																valuePopulator(value, m->value);
																map.insert(std::pair<K,V> (name, value));
								}
}

/**
   writes a map like this ot object value
   {
   "soccer.com":23.12,
   "cooking.com":32.23,
   "shipping.com":2.32,
   "booking.com":23.12
   }

 */
template<typename K, typename V>
inline static void addMapAsMemberToValue(
								DocumentType* doc,
								std::unordered_map <K, V> keyValueMap,
								RapidJsonValueType parentValue) {


								for (auto &keyValue : keyValueMap) {
																JsonUtil::addMemberToValue_FromPair(
																								doc,
																								keyValue.first,
																								keyValue.second,
																								parentValue);
								}
}



/*
   populates a map of K, V from a memberValue
   that is like this
   {
   "soccer.com":23.12,
   "cooking.com":32.23,
   "shipping.com":2.32,
   "booking.com":23.12
   }
 */
template<typename K, typename V>
inline static void read_Map_From_Value(RapidJsonValueType valueDocument, std::unordered_map<K, V>& map) {

								for (auto m = valueDocument.MemberBegin ();
													m != valueDocument.MemberEnd (); ++m) {
																assertAndThrow(m->name.IsString());

																std::string name;
																JsonUtil::GetValueUnsafely<std::string> (m->name, name);
																V value;
																JsonUtil::GetValueUnsafely<V> (m->value, value);
																K nameProper = ConverterUtil::TO<std::string, K>(name);
																map.insert(std::make_pair (nameProper, value));
								}
}

template<typename K, typename V>
inline static void readMapFromJsonString(
								std::string jsonString,
								std::unordered_map<K, V>& map) {

								auto valueDocument = parseJsonSafely(jsonString);
								assertAndThrow(valueDocument->IsArray ());
								// rapidjson uses SizeType instead of size_t.
								for (rapidjson::SizeType i = 0; i < valueDocument->Size (); i++) {
																RapidJsonValueType oneValue = (*valueDocument)[i];
																if (!oneValue.HasMember ("key")) {
																								throwEx("value doesn't have member : key ");
																}

																if (!oneValue.HasMember ("value")) {
																								throwEx("value doesn't have member : value");
																}

																K key;
																std::shared_ptr<V> value;
																JsonUtil::GetValueUnsafely<K>(oneValue["key"], key);
																JsonUtil::GetValueUnsafely<V>(oneValue["value"], value);
																map.insert(std::make_pair(key, *value));

								}
}

template<typename K, typename V>
inline static void readMapFromJsonString(
								std::string jsonString,
								gicapods::ConcurrentHashMap<K, V>& map) {

								auto valueDocument = parseJsonSafely(jsonString);
								assertAndThrow(valueDocument->IsArray ());
								// rapidjson uses SizeType instead of size_t.
								for (rapidjson::SizeType i = 0; i < valueDocument->Size (); i++) {
																RapidJsonValueType oneValue = (*valueDocument)[i];
																if (!oneValue.HasMember ("key")) {
																								throwEx("value doesn't have member : key ");
																}

																if (!oneValue.HasMember ("value")) {
																								throwEx("value doesn't have member : value");
																}

																K key;
																std::shared_ptr<V> value;
																JsonUtil::GetValueUnsafely<K>(oneValue["key"], key);
																JsonUtil::GetValueUnsafely<V>(oneValue["value"], value);
																map.put(key, *value);

								}
}

template<typename K, typename V>
inline static std::string convertMapToJsonArray (std::unordered_map<K, V>& map) {
								auto doc = JsonArrayUtil::createDocumentAsArray ();

								//we create a vector from map and sort it...to send a sorted json back every time
								typename std::vector<std::pair<K, V> > sorted_elements(map.begin(), map.end());
								std::sort(sorted_elements.begin(), sorted_elements.end());
								for(auto elemnt : sorted_elements) {

																RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
																RapidJsonValueTypeNoRef keyJsonValue(rapidjson::kObjectType);

																JsonUtil::populateValue(keyJsonValue, elemnt.first, doc.get());

																RapidJsonValueTypeNoRef valueJsonValue(rapidjson::kObjectType);
																JsonUtil::populateValue(valueJsonValue, elemnt.second, doc.get());


																JsonUtil::addMemberToValue_FromPair(
																								doc.get(),
																								"key", keyJsonValue, value);

																JsonUtil::addMemberToValue_FromPair(
																								doc.get(),
																								"value", valueJsonValue, value);

																doc->PushBack (value, doc->GetAllocator ());
								}

								return JsonUtil::docToString (doc.get());
}

template<typename K, typename V>
inline static std::string convertMapToJsonArray (gicapods::ConcurrentHashMap<K, V>& map) {
								auto doc = JsonArrayUtil::createDocumentAsArray ();

								//we create a vector from map and sort it...to send a sorted json back every time
								// typename std::vector<std::pair<K, V> > sorted_elements(map.begin(), map.end());
								// std::sort(sorted_elements.begin(), sorted_elements.end());
								auto newMap = map.getCopyOfMap();
								for(auto elemnt : *newMap) {

																RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
																RapidJsonValueTypeNoRef keyJsonValue(rapidjson::kObjectType);

																JsonUtil::populateValue(keyJsonValue, elemnt.first, doc.get());

																RapidJsonValueTypeNoRef valueJsonValue(rapidjson::kObjectType);
																JsonUtil::populateValue(valueJsonValue, elemnt.second, doc.get());


																JsonUtil::addMemberToValue_FromPair(
																								doc.get(),
																								"key", keyJsonValue, value);

																JsonUtil::addMemberToValue_FromPair(
																								doc.get(),
																								"value", valueJsonValue, value);

																doc->PushBack (value, doc->GetAllocator ());
								}

								return JsonUtil::docToString (doc.get());
}

template<typename K, typename V>
inline static std::string convertMapValuesToJsonArrayString (gicapods::ConcurrentHashMap<K, V>& map) {
								// return convertMapValuesToJsonArrayString(*map.map);
								auto doc = JsonArrayUtil::createDocumentAsArray ();

								//we create a vector from map and sort it...to send a sorted json back every time
								auto mapCopy = map.getCopyOfMap();
								typename std::vector<std::pair<K,  std::shared_ptr<V> > > sorted_elements(mapCopy->begin(), mapCopy->end());
								std::sort(sorted_elements.begin(), sorted_elements.end());

								for (auto element : sorted_elements) {
																RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
																std::shared_ptr<V> elm = element.second;
																elm->addPropertiesToJsonValue (value, doc.get());
																doc->PushBack (value, doc->GetAllocator ());
								}

								auto responseToClient = JsonUtil::docToString (doc.get());
								return responseToClient;
}

template<typename K, typename V>
inline static std::string convertMapValuesToJsonArrayString (std::unordered_map<K, V >& map) {
								auto doc = JsonArrayUtil::createDocumentAsArray ();

								//we create a vector from map and sort it...to send a sorted json back every time
								typename std::vector<std::pair<K,  V > > sorted_elements(map.begin(), map.end());
								std::sort(sorted_elements.begin(), sorted_elements.end());
								for(auto elemnt : sorted_elements) {
																RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
																// elm.addPropertiesToJsonValue (value, doc.get());
																JsonUtil::populateValue(value, elemnt.second, doc.get());

																doc->PushBack (value, doc->GetAllocator ());
								}

								auto responseToClient = JsonUtil::docToString (doc.get());
								return responseToClient;
}

template<typename K, typename V>
inline static std::string convertMapValuesToJsonArrayString (tbb::concurrent_hash_map<K, V>& map) {
								auto doc = JsonArrayUtil::createDocumentAsArray ();

								//we create a vector from map and sort it...to send a sorted json back every time
								// typename std::vector<std::pair<K,  V > > sorted_elements(map.begin(), map.end());
								// std::sort(sorted_elements.begin(), sorted_elements.end());
								for(auto elemnt : map) {
																RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
																// elm.addPropertiesToJsonValue (value, doc.get());
																JsonUtil::populateValue(value, elemnt.second, doc.get());

																doc->PushBack (value, doc->GetAllocator ());
								}

								auto responseToClient = JsonUtil::docToString (doc.get());
								return responseToClient;
}

};

template<>
void JsonMapUtil::readMapFromJsonString(
								std::string jsonString,
								std::unordered_map<std::string, std::string>& map) {

								auto valueDocument = parseJsonSafely(jsonString);
								assertAndThrow(valueDocument->IsArray ());

								// rapidjson uses SizeType instead of size_t.
								for (rapidjson::SizeType i = 0; i < valueDocument->Size (); i++) {
																RapidJsonValueType oneValue = (*valueDocument)[i];
																if (!oneValue.HasMember ("key")) {
																								throwEx("value doesn't have member : key ");
																}

																if (!oneValue.HasMember ("value")) {
																								throwEx("value doesn't have member : value");
																}

																map.insert(
																								std::make_pair(
																																oneValue["key"].GetString(),
																																oneValue["value"].GetString()));

								}

}
}
#endif
