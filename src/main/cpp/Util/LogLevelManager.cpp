//
// Created by Mahmoud Taabodi on 2/15/16.
//

#include "LogLevelManager.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include "GUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include <set>
#include <tbb/concurrent_hash_map.h>
#include <thread>
#include "JsonMapUtil.h"
#include "JsonArrayUtil.h"
#include "ConfigService.h"

#include "ExceptionUtil.h"
#include "ConcurrentHashMap.h"
#include "StringHolder.h"

LogLevelManager::LogLevelManager(std::vector<std::string> args,
                                 bool allLogsAreOn,
                                 bool allLoggingIsTurendOff) {
        arguments = args;
        this->allLogsAreOn = allLogsAreOn;
        this->allLoggingIsTurendOff = allLoggingIsTurendOff;
        mapOfFileNamesToLogStatus =
                std::make_shared<gicapods::ConcurrentHashMap<std::string, StringHolder> > ();


        stopThread = std::make_shared<gicapods::AtomicBoolean>(false);
        threadIsRunning = std::make_shared<gicapods::AtomicBoolean>(false);
}

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, StringHolder> > LogLevelManager::getLogsToStatusMap() {
        return mapOfFileNamesToLogStatus;
}

std::shared_ptr<LogLevelManager> LogLevelManager::shared_instance() {
        std::vector<std::string> args;
        bool allLogsAreOn = false;
        bool allLoggingIsTurendOff = false;
        static auto instance = std::make_shared<LogLevelManager>(args, allLogsAreOn, allLoggingIsTurendOff);
        return instance;
}

void LogLevelManager::startLogEnablerThread(std::string appPropertyFileName) {
        auto instance = LogLevelManager::shared_instance();
        std::thread threadHandle (&LogLevelManager::enableLogThread, instance.get(), appPropertyFileName);
        threadHandle.detach ();
}

void LogLevelManager::enableLogThread(std::string appPropertyFileName) {
        auto appName = StringUtil::toLowerCase(appPropertyFileName) + ".properties";

        while(stopThread->getValue() == false) {
                threadIsRunning->setValue(true);
                auto configService = std::make_shared<gicapods::ConfigService>(
                        appName,
                        "common.properties");

                try {
                        MLOG(2) << "updating the logs from property";

                        configService->readProperties("common.properties");
                        configService->readProperties(appName);

                        bool allLogsAreOn = configService->getAsBooleanFromString ("allLogsAreOn");
                        shared_instance()->allLogsAreOn = allLogsAreOn;

                        std::string allLogsStr = configService->get ("classesToLog");
                        auto allNewLogsToEnable = StringUtil::tokenizeString (allLogsStr, ",");

                        mapOfFileNamesToLogStatus->clear();
                        for (std::string log : allNewLogsToEnable) {
                                MLOG(2) << "adding the log from property :" << log;
                                auto stringHolder = std::make_shared<StringHolder>();
                                stringHolder->value = "enabled";
                                mapOfFileNamesToLogStatus->put (log, stringHolder);
                        }
                } catch(const std::exception& e) {
                        ExceptionUtil::logException(
                                e,
                                nullptr,
                                1);
                }

                gicapods::Util::sleepViaBoost(_L_,  configService->getAsInt ("periodInSecondsToUpdateLogLevels"));
        }
        threadIsRunning->setValue(false);
}

std::string LogLevelManager::getLogsToStatusMapAsJson() {
        return JsonMapUtil::convertMapValuesToJsonArrayString(*mapOfFileNamesToLogStatus);
}

//file name is like this  LogLevelManager.cpp, DayPartFilter.cpp
bool LogLevelManager::logOrNot(std::string fileName) {

        if (shared_instance()->allLoggingIsTurendOff) {
                //this is for not even having any logging and not doing a lookup in hashmap
                //for performance reasons
                return false;
        }

        if (shared_instance()->allLogsAreOn) {
                //this will turn on all logs without lookup in hashmap
                //this is for debugging purposes
                return true;
        }

        auto level = shared_instance()->mapOfFileNamesToLogStatus->getOptional (fileName);
        if (level != nullptr) {
                if (level->value.compare ("enabled") == 0) {
                        return true;
                }
        }

        auto stringHolder = std::make_shared<StringHolder>();
        stringHolder->value = "disabled";
        shared_instance()->mapOfFileNamesToLogStatus->put (fileName, stringHolder);
        return false;
}

void LogLevelManager::disable(std::string fileName) {
        auto level = mapOfFileNamesToLogStatus->getOptional (fileName);
        if (level != nullptr) {
                level->value = "disabled";
        } else {
                auto stringHolder = std::make_shared<StringHolder>();
                stringHolder->value = "disabled";
                mapOfFileNamesToLogStatus->put(fileName, stringHolder);
                LOG(INFO) << "disabling log level for : " << fileName;
        }
}

void LogLevelManager::enableAll() {
        allLogsAreOn = true;
}


void LogLevelManager::enableOnly(std::string fileName) {
        allLogsAreOn = false;

        auto level = mapOfFileNamesToLogStatus->getOptional (fileName);
        if (level != nullptr) {
                level->value = "enabled";
        } else {
                auto stringHolder = std::make_shared<StringHolder>();
                stringHolder->value = "disabled";
                mapOfFileNamesToLogStatus->put(fileName, stringHolder);
                LOG(INFO) << "disabling log level for : " << fileName;
        }
}

void LogLevelManager::disableAll() {
        allLogsAreOn = false;
        return;
}

void LogLevelManager::printAllLogLevels() {
        auto map = mapOfFileNamesToLogStatus->getCopyOfMap();
        typename tbb::concurrent_hash_map<std::string, StringHolder >::iterator iter;
        for (auto iter = map->begin ();
             iter != map->end ();
             iter++) {
                LOG(INFO) << "log name : " << iter->first
                          << ", level : " << iter->second;
        }
}


LogLevelManager::~LogLevelManager() {
        while(threadIsRunning->getValue() ==  true)  {
                //try stopping the thread before exiting
                stopThread->setValue(true);
                gicapods::Util::sleepViaBoost(_L_, 2);  //until consumer thread goes out
        };
}
