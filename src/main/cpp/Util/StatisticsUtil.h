/*
 * StatisticsUtil.h
 *
 *  Created on: Aug 9, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_StatisticsUtil_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_StatisticsUtil_H_

class RandomUtilDecimalGenerator;
#include <string>
#include <memory>
#include "Object.h"
class StatisticsUtil : public Object {
std::shared_ptr<RandomUtilDecimalGenerator> percentRandomizer;
public:
StatisticsUtil();
bool happensWithProb(double probability);
virtual ~StatisticsUtil();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_StatisticsUtil_H_ */
