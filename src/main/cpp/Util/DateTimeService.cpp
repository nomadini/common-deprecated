#include "GUtil.h"
#include <iostream>

#include "FileUtil.h"
#include "ConverterUtil.h"
#include "DateTimeService.h"
#include "Poco/DateTime.h"
#include "Poco/Timespan.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include <string>
#include <memory>
#include <boost/date_time/gregorian/gregorian.hpp>

#include "boost/date_time/local_time/posix_time_zone.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include "boost/date_time/time_zone_base.hpp"

using namespace boost::posix_time;
using namespace boost::gregorian;
using namespace boost::local_time;
DateTimeService::DateTimeService() : Object(__FILE__) {
        std::string filename = "/var/data/date_time_zonespec.csv";
        if(!FileUtil::checkIfFileExists(filename)) {
                throwEx("File not found " + filename);
        }
        tz.load_from_file (filename.c_str ());
}

int DateTimeService::getClosest5thMinuteNowAsIntegerInUTC() {
        return DateTimeUtil::getClosest5thMinuteNowAsIntegerInUTC();
}

std::string DateTimeService::getNowPlusSecondsInMySqlFormat(int seconds) {
        return DateTimeUtil::getNowPlusSecondsInMySqlFormat(seconds);
}

std::string DateTimeService::getNowPlusHoursInMySqlFormat(int hours) {
        return DateTimeUtil::getNowPlusHoursInMySqlFormat(hours);
}

std::string DateTimeService::getNowPlusDaysInMySqlFormat(int days) {
        return DateTimeUtil::getNowPlusDaysInMySqlFormat(days);
}

std::string DateTimeService::getNowInMySqlFormat() {
        return DateTimeUtil::getNowInMySqlFormat();
}

void DateTimeService::initTimeZoneObject() {

}

int DateTimeService::getUtcOffsetBasedOnTimeZone(std::string timeZoneName) {
        return DateTimeUtil::getUtcOffsetBasedOnTimeZone(timeZoneName);
}

TimeType DateTimeService::StringToMicro(std::string StringToMicro) {
        return DateTimeUtil::StringToMicro(StringToMicro);
}

TimeType DateTimeService::MicroToSecond(TimeType micro) {
        return DateTimeUtil::MicroToSecond(micro);
}

TimeType DateTimeService::getNowInMicroSecond() {
        return DateTimeUtil::getNowInMicroSecond();
}

TimeType DateTimeService::getNowInSecond() {
        return DateTimeUtil::getNowInSecond();
}

TimeType DateTimeService::getDaysInSeconds(int days) {
        return DateTimeUtil::getDaysInSeconds(days);
}

TimeType DateTimeService::getNowInMilliSecond() {
        return DateTimeUtil::getNowInMilliSecond();
}

TimeType DateTimeService::getNowHourInMilliSecond() {
        return DateTimeUtil::getNowHourInMilliSecond();
}

TimeType DateTimeService::getNowMinuteInMilliSecond() {
        return DateTimeUtil::getNowMinuteInMilliSecond();
}

std::string DateTimeService::getNowInYYYMMDDFormat() {
        return DateTimeUtil::getNowInYYYMMDDFormat();
}

std::string DateTimeService::getNDayAgoInStringFormat(int daysAgo) {
        return DateTimeUtil::getNDayAgoInStringFormat(daysAgo);
}

int DateTimeService::getCurrentHourAsIntegerInUTC() {
        return DateTimeUtil::getCurrentHourAsIntegerInUTC();
}

int DateTimeService::getMinuteNowAsIntegerInUTC() {
        return DateTimeUtil::getMinuteNowAsIntegerInUTC();
}

int DateTimeService::getSecondNowAsIntegerInUTC() {
        return DateTimeUtil::getSecondNowAsIntegerInUTC();
}


std::string DateTimeService::getNowInStringFormat(Timestamp now, const std::string &format) {
        return DateTimeUtil::getNowInStringFormat(now, format);
}

TimeType DateTimeService::getThisHourSinceEpoch() {
        return DateTimeUtil::getThisHourSinceEpoch();
}

TimeType DateTimeService::getNowInseconds() {
        return DateTimeUtil::getNowInseconds();
}

TimeType DateTimeService::getNowInNanoSecond() {
        return DateTimeUtil::getNowInNanoSecond();
}

TimeType DateTimeService::getNowInUniversalFormat() {
        return DateTimeUtil::getNowInUniversalFormat();
}

std::string DateTimeService::getTimeNowAsString() {
        return DateTimeUtil::getTimeNowAsString();
}

std::string DateTimeService::getWeekdayBasedOnUTC()
{
        return DateTimeUtil::getWeekdayBasedOnUTC();
}

int DateTimeService::getWeekDayNumberBasedOnUTC() {
        return DateTimeUtil::getWeekDayNumberBasedOnUTC();
}

int DateTimeService::getWeekDayNumberBasedOnUserOffset(int userOffset) {
        return DateTimeUtil::getWeekDayNumberBasedOnUserOffset(userOffset);
}


//DATE
std::string DateTimeService::getCurrentDateAsString() {
        return DateTimeUtil::getCurrentDateAsString();
}

DateTimeService::~DateTimeService() {

}
