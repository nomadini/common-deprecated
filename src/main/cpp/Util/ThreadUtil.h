/*
 * ThreadUtil.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_THREAD_UTIL_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_THREAD_UTIL_H_



#include "Poco/Runnable.h"
#include "Poco/ThreadPool.h"
#include <thread>
#include <vector>

class ThreadUtil {
public:
//    void (*voidFuncVoid)(void) ;//this is a function pointer called voidFuncVoid that returns void and takes void
static void runFunctionInNdetachedThreads(void (*func)(void),
																																										int numOfThreads) {
								std::vector<std::thread> th;
								//Launch a group of threads
								for (int i = 0; i < numOfThreads; ++i) {
																th.push_back(std::thread(func));
								}

								//Join the threads with the main thread
								for (auto &t : th) {
																t.detach();
								}
}

template<typename funcType>
class Worker : public Poco::Runnable {
public:
std::function<funcType> func;
Worker(std::function<funcType> func) {
								this->func = func;
}
virtual void run() {
								func;
}
};

/**
 * this should be used like this
 * std::function<void(std::shared_ptr<DeviceFeatureHistory>)> f_update_device = std::bind(DeviceFeatureHistoryCassandraService::upsertDeviceFeatureHistory, bh );
   ThreadUtil::runFunctionInThreadPool<void(std::shared_ptr<DeviceFeatureHistory>)>(10, f_update_device );
 */
template<typename funcType>
static void runFunctionInThreadPool(
								int sizeOfPool,
								std::function<funcType> func) {
								Poco::ThreadPool threadpool;

								for (int i = 0; i < sizeOfPool; i++) {
																Worker<funcType> work(func);
																threadpool.start(work);
								}
								threadpool.joinAll(); // we should always join the threads
								//or Poco thread pool can't be started properly
}

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_CONVERTERUTIL_H_ */
