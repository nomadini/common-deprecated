/*
 * TestUtil.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_TESTUTIL_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_TESTUTIL_H_



#include <curl/curl.h>



#define assertNotEmpty(args ...) TestUtil::_assertNotEmpty(_L_, args);
#define assertNotNull(args ...) TestUtil::_assertNotNull(_L_, args);
#define assertGreatherThanZero(args ...) TestUtil::_assertGreatherThanZero(_L_, args);

#include "StringUtil.h"
#include "GUtil.h"
#include <string>
#include <memory>

class TestUtil {
public:

static void confirmEqual(const std::string & foo, const std::string & bar) {
								if (foo.compare(bar) != 0) {
																throwEx(foo + " is not equal to " + bar);
								}
}

static void _assertNotEmpty(const std::string& caller, const std::string& val) {
								if (val.empty()) {
																throwEx(StringUtil::toStr(caller) + ":  value for is required");
								}
}

static void _assertGreatherThanZero(const std::string& caller, double val) {
								if (val <= 0) {
																throwEx(StringUtil::toStr(caller) + ": value should be greater than zero");
								}
}

static void _assertGreatherThanZero(const std::string& caller, long val) {
								if (val <= 0) {
																throwEx(StringUtil::toStr(caller) + ": value should be greater than zero");
								}
}

static void _assertGreatherThanZero(const std::string& caller, int val) {
								if (val <= 0) {
																throwEx(StringUtil::toStr(caller) + ": value should be greater than zero");
								}
}

static void _assertNotNull(const std::string& caller, void* val) {
								if (val == NULL) {
																throwEx(StringUtil::toStr(caller) + ": value should not be NULL");
								}
}


};

#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_TESTUTIL_H_ */
