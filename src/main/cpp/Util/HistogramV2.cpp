#include "HistogramV2.h"
#include "GUtil.h"
#include "NumberUtil.h"
#include <cmath>

#include <vector>
#include <algorithm>
#include <iostream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/density.hpp>
#include <boost/accumulators/statistics/stats.hpp>

using namespace boost;
using namespace boost::accumulators;

typedef accumulator_set<double, features<tag::density> > acc;
typedef iterator_range<std::vector<std::pair<double, double> >::iterator > histogram_type;

double HistogramV2::getNthPercentileOfData(std::vector<double> data, int percentile) {
								int numberOfBins = 100 / percentile;
								int c = data.size();//cache size for histogramm.
								//create an accumulator
								acc myAccumulator( tag::density::num_bins = numberOfBins, tag::density::cache_size = 10);

								//fill accumulator
								for (int j = 0; j < c; ++j)
								{
																myAccumulator(data[j]);
								}

								histogram_type hist = density(myAccumulator);

								for( int i = 0; i < hist.size(); i++ )
								{
																if (i >= numberOfBins) {
																								//this is our percentile
																								return hist[i].second;
																}
								}

								throwEx("percentile was not found");
}

void HistogramV2::printHistogramOfData(std::vector<double> data, int numberOfBins)
{
								int c = data.size();//cache size for histogramm.
								//create an accumulator
								acc myAccumulator( tag::density::num_bins = numberOfBins, tag::density::cache_size = 10);

								//fill accumulator
								for (int j = 0; j < c; ++j)
								{
																myAccumulator(data[j]);
								}

								histogram_type hist = density(myAccumulator);

								double total = 0.0;

								for( int i = 0; i < hist.size(); i++ )
								{
																std::cout << "Bin lower bound: " << hist[i].first << ", Value: " << hist[i].second << std::endl;
																total += hist[i].second;
								}

								std::cout << "Total: " << total << std::endl; //should be 1 (and it is)
}

HistogramV2::HistogramV2() : Object(__FILE__) {
}

HistogramV2::~HistogramV2() {

}
