/*
 * TempUtil.h
 *
 *  Created on: Feb 16, 2016
 *      Author: mtaabodi

 */


#include "LogLevelManager.h"
#include "GUtil.h"
#include "SignalHandler.h"
#include "StringUtil.h"
#include "JsonArrayUtil.h"
#include "Poco/Net/HTTPServerRequest.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "ConfigService.h"
#include "JsonUtil.h"
#include "TempUtil.h"
#include "CollectionUtil.h"
#include "FileUtil.h"
#include "MemoryUtil.h"
#include "Poco/SignalHandler.h"
#include <thread>
#include <boost/algorithm/string.hpp>
// //this will uninstall poco SignalHandler
// #define POCO_NO_SIGNAL_HANDLER
void TempUtil::yourFailureFunction() {
        // Reports something...
        gicapods::Util::showStackTrace();
}

void TempUtil::deleteLockFile(std::string appName) {
        std::string lockFileName = "/var/data/" + appName + ".lock";
        FileUtil::deleteFile(lockFileName);
}
/**
 * is used by main function of the different programs to start up the basic threads
 */
void TempUtil::configureLogging(std::string appName, char* argv[]) {

        // Poco::SignalHandler::install();

        // FLAGS_log_dir = "/var/log/";
        std::string logfileName = "/var/log/" + appName;

        FileUtil::createDirectory(logfileName);
        FileUtil::deleteFileOlderThanXMinutesInDir(logfileName, 60);

        FLAGS_log_dir = logfileName;

        FLAGS_logbufsecs = 10;
        //Log messages to stderr instead of logfiles.
        //Note: you can set binary flags to true by specifying 1, true, or yes (case insensitive). Also, you can set binary flags to false by specifying 0, false, or no (again, case insensitive).
        // FLAGS_logtostderr = false;
        FLAGS_alsologtostderr = true;

//    Log messages at or above this level. Again, the numbers of severity levels INFO, WARNING, ERROR, and FATAL are 0, 1, 2, and 3, respectively.

        FLAGS_max_log_size = 1000000; //this is in MB I guess
        FLAGS_stop_logging_if_full_disk = true;


        // minloglevel (int, default=0, which is INFO)
        // Log messages at or above this level. Again, the numbers of severity levels INFO, WARNING, ERROR, and FATAL are 0, 1, 2, and 3, respectively.
        FLAGS_minloglevel = 0;

        FLAGS_v=1;
        //   MLOG(3) << "I'm printed when you run the program with --v=1 or higher";
        // VLOG(2) << "I'm printed when you run the program with --v=2 or higher";
        // v (int, default=0)
        // Show all VLOG(m) messages for m less or equal the value of this flag. Overridable by --vmodule. See the section about verbose logging for more detail.


        // FLAGS_drop_log_memory = true;
//    FLAGS_stderrthreshold = 0;
        FLAGS_colorlogtostderr = true;
//    FLAGS_minloglevel = 0;
//    FLAGS_stderrthreshold = 0;


        google::InstallFailureSignalHandler(); //this option is better than the two lines above

        //creating the lock file//this is very important to make sure that two instances of
        //process are not running
        std::string lockFileName = "/var/data/" + appName + ".lock";
        if (FileUtil::checkIfFileExists(lockFileName)) {
                // throwEx("another instance of program is running...lock file exists : " + lockFileName);
                LOG(ERROR)<<"another instance of program is running...lock file exists : " + lockFileName;
        } else {
                FileUtil::writeStringToFile(lockFileName, "");
        }

        LOG(ERROR)<<"initializing the logging for app : " <<appName;
        google::InitGoogleLogging(appName.c_str());
        //these have to run after InitGoogleLogging is called
        LOG(ERROR)<<"starting the dynamic logging thread";
        LogLevelManager::startLogEnablerThread(appName);
        LOG(ERROR)<<"printing processId";
        gicapods::Util::getProcessId();

        MemoryUtil::setStackSize(128);

}
