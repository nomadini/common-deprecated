/*
 * ProgramOptionParser.h
 *
 *  Created on: Feb 5, 2017
 *      Author: mtaabodi
 */

#ifndef ProgramOptionParser_H_
#define ProgramOptionParser_H_

#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <string>
#include <memory>
#include <boost/program_options.hpp>
#include <iostream>
class ProgramOptionParser {
public:

/* Auxiliary functions for checking input for validity. */

/* Function used to check that 'opt1' and 'opt2' are not specified
   at the same time. */
static void conflicting_options(
        const boost::program_options::variables_map& vm,
        const char* opt1, const char* opt2);

/* Function used to check that of 'for_what' is specified, then
   'required_option' is specified too. */
static void option_dependency(
        const boost::program_options::variables_map& vm,
        const char* for_what, const char* required_option);
};

#endif
