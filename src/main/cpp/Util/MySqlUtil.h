//http://dev.mysql.com/doc/refman/5.1/en/date-and-time-functions.html
//
//string str = "2013-06-06 16:06:00";
//  cout << UnixTimeFromMySqlString(str) << endl;
//
//  time_t current = time(NULL);
//  cout << current << endl;
//When I ran it I got:
//
//1370549160
//1370549171
//To see it as a string you can do:
//
//  time_t ret = UnixTimeFromMySqlString(str);
//  cout << "It is now " << ctime(&ret) << endl;
//
//
//
//  SELECT '12/31/10',
//      STR_TO_DATE('12/31/10', '%m/%d/%y'),
//      UNIX_TIMESTAMP(STR_TO_DATE('12/31/10', '%m/%d/%y'))
//
//
//	  SELECT UNIX_TIMESTAMP(STR_TO_DATE('08/05/10','%m/%d/%y'));
