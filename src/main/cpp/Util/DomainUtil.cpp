//
// Created by Mahmoud Taabodi on 4/19/16.
//

#include "DomainUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include <Poco/URI.h>

//TODO : write a test for this
std::string DomainUtil::getDomainFromUrlString(std::string domain) {

        Poco::URI uri(domain);
        auto hostParsedByPoco =  uri.getHost();
        std::string host = hostParsedByPoco;
        if (hostParsedByPoco.empty()) {
                //Poco fails to parse 'bbc.com' ..so we set the host to 'bbc.com' in this case
                host = domain;
        }

        if (StringUtil::containsCaseInSensitive(host, "wwww.")) {
                host = StringUtil::replaceStringCaseInsensitive(host, "wwww.", "");
        }

        MLOG(3)<< " domain : "<< domain << ", host : "<< host;
        return host;
}
