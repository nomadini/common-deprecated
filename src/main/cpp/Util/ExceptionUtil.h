/*
 * TempUtil.h
 *
 *  Created on: Aug 2, 2015
 *      Author: mtaabodi



 */

#ifndef ExceptionUtil_H_
#define ExceptionUtil_H_



#include <memory>
#include <string>
#include <vector>
#include <set>
#include <exception>
class EntityToModuleStateStats;
class ExceptionUtil {
public:

static std::string logException(const std::exception& e,
                                EntityToModuleStateStats* entityToModuleStateStats,
                                int freqToLog = 100);

};
#endif /* ExceptionUtil_H_ */
