#ifndef FILE_UTILS_H
#define FILE_UTILS_H



#include <string>
#include <memory>
#include <vector>


class FileUtil {

FileUtil();
public:
static int getLastModifiedInSeconds(const std::string& name);
static std::string getCurrentDirectory();
static void writeStringToFile(const std::string& filename, const std::string& fileContent);
static std::string readFileInString(const std::string& filename);

/**
 * this is the slower version of append a line to file
 */
static void appendALineToFile(const std::string& filename, const std::string& fileContent);
static void appendALineToFile(std::shared_ptr<std::ofstream> log, const std::string& fileContent);


static std::string readQueryBodyFromFile(std::string filePath);

static int getCountOfLinesInFile(const std::string& filename);
static void createFileIfNotExist(const std::string& name);
static bool checkIfFileExists(const std::string& name);
static std::vector<std::string> readFileLineByLine(const std::string& filename);
static int deleteFile(const std::string& name);
static int deleteFileOlderThanXMinutesInDir(const std::string& directory, int minutes);
static void printDirectoryContentRecursively(const std::string & path);
static std::vector<std::string> getAllFilesOlderThanXMinutesInDir(const std::string & directory, int minutes);

static bool createDirectory(const std::string& nameOfDir);
static std::shared_ptr<std::ofstream> getAppenderStreamOfFile(const std::string& filename);
};

#endif
