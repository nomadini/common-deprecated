#ifndef DATE_TIME_UTILS_h
#define DATE_TIME_UTILS_h



#include <iostream>
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include "Poco/DateTimeParser.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/LocalDateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
using Poco::Timestamp;
using Poco::DateTime;
using Poco::DateTimeParser;
using Poco::DateTimeFormat;
using Poco::DateTime;
using Poco::DateTimeFormatter;
#include <boost/date_time/local_time/local_time.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
using namespace boost::posix_time;
using namespace boost::local_time;

#include "ConverterUtil.h"
#include "DateTimeMacro.h"
typedef boost::posix_time::ptime Time;

class DateTimeUtil {
DateTimeUtil();
public:

static void init();
static std::vector<Poco::DateTime> getHoursBetweenDates(
        Poco::DateTime startDate,
        Poco::DateTime endDate
        );
static int daysInSeconds;
static int minutesInSeconds;

static Poco::DateTime parseDateTime(const std::string& str);

static Poco::DateTime parseDateTime(const std::string& str, const std::string& format);

static Poco::DateTime parseDateTime(
        const std::string & fmt,
        const std::string & str,
        int & timeZoneDifferential
        );

static std::string dateTimeToStr(Poco::DateTime datetime);

static std::string dateTimeToStr(Poco::DateTime datetime, const std::string& format);

static std::string getMinuteNowAsString();

static std::string getSecondNowAsString();

static std::string getCurrentDateWithHourPercisionAsString();

static std::string getDayInMonthNowAsString();

static std::string getNowPlusSecondsInMySqlFormat(int seconds);

static std::string getNowPlusHoursInMySqlFormat(int hour);

static std::string getNowPlusDaysInMySqlFormat(int day);

static std::string getNowInMySqlFormat();

static boost::local_time::tz_database tz;

static void initTimeZoneObject();

static int getUtcOffsetBasedOnTimeZone(std::string timeZoneName);

static TimeType StringToMicro(std::string str);

static TimeType MicroToSecond(TimeType micro);

static TimeType getNowInMicroSecond();

static TimeType getNowInSecond();

static TimeType getDaysInSeconds(int days);

static TimeType getNowDayInMilliSecond();

static TimeType getNowInMilliSecond();

static TimeType getNowHourInMilliSecond();

static TimeType getNowMinuteInMilliSecond();

/*
 * returns current time in this format 2001-08-23
 * which determines the partition key of device history (rowkey)
 * and pixel_stats
 *
 */
static std::string getNowInYYYMMDDFormat();

static std::string getDateInYYYMMDDFormat(int daysPlusNow);

static Poco::DateTime getCurrentDateWithSecondPercision();

static Poco::DateTime getCurrentDateWithMinutePercision();

static Poco::DateTime getCurrentDateWithHourPercision();

static Poco::DateTime getCurrentDateWithDayPercision();


static Poco::DateTime getDateWithSecondPercision(int secondsPlusNow);

static Poco::DateTime getDateWithMinutePercision(int minutesPlusNow);

static Poco::DateTime getDateWithHourPercision(int hoursPlusNow);

static Poco::DateTime getDateWithDayPercision(int daysPlusNow);


static std::string getNDayAgoInStringFormat(int daysAgo);

static Poco::DateTime truncateDateToMinutePercision(Poco::DateTime date);
static Poco::DateTime truncateDateToHourPercision(Poco::DateTime date);
static Poco::DateTime truncateDateToDayPercision(Poco::DateTime date);

/**
 * this retuns the active hour in UTC
 */
static int getCurrentHourAsIntegerInUTC();
static int getMinuteNowAsIntegerInUTC();
static int getSecondNowAsIntegerInUTC();

static std::string getNowInStringFormat(Poco::Timestamp now, const std::string& format);

/**
   returns the 5 if the minute now is 2 or 3 or 4 or 5 .
   return 25 if the minute now is 21, 22, 23, 24
 */
static int getClosest5thMinuteNowAsIntegerInUTC();

static TimeType getThisHourSinceEpoch();

static TimeType getNowInseconds();

static TimeType getNowInNanoSecond();

static TimeType getNowInUniversalFormat();

//-----------------------------------------------------------------------------
// Format current time (calculated as an offset in current day) in this form:
//
//     "hh:mm:ss.SSS" (where "SSS" are milliseconds)
//-----------------------------------------------------------------------------
static std::string getTimeNowAsString();
//DATE
static std::string getCurrentDateAsString();

static std::string getWeekdayBasedOnUTC();

static int getWeekDayNumberBasedOnUTC();

static int getWeekDayNumberBasedOnUserOffset(int userOffset);

static int getHourNumberInWeekBasedOnUserOffset(int userOffset);
};

#endif
