/*
 * HttpUtilService.h
 *
 *  Created on: Aug 2, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_HttpUtilService_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_HttpUtilService_H_




#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerResponseImpl.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/HTTPClientSession.h"
#include <string>
#include <memory>
#include <curl/curl.h>
#include <unordered_map>
#include "Object.h"
namespace gicapods {
class ConfigService;
};


class HttpUtilService : public Object {
public:
HttpUtilService();

virtual void sendHttpPostViaCurl(const std::string & url,
                                 const std::string & postData,
                                 int numberOfTries);
virtual std::unordered_map<std::string, std::string> getMapOfQueryParams(Poco::Net::HTTPServerRequest& request);

virtual void printQueryParamsOfRequest(Poco::Net::HTTPServerRequest& request);


virtual std::string sendGetRequest(const std::string & url,
                                   int timeoutInMillis,
                                   int numberOfTries);

virtual std::string sendGetRequestAndGetFullResponse(const std::string & url,
                                                     Poco::Net::HTTPResponse& response,
                                                     int timeoutInMillis,
                                                     int numberOfTries);

virtual std::string sendGetRequestAndGetFullResponse(
        const std::string & cookieKey,
        const std::string & cookieValue,
        Poco::Net::HTTPResponse& response,
        const std::string & url,
        int timeoutInMillis,
        int numberOfTries);

virtual std::string sendPostRequest(Poco::Net::HTTPClientSession& session,
                                    const std::string & url,
                                    const std::string & requestBody,
                                    int numberOfTries);

virtual std::string sendPostRequest(const std::string & url,
                                    const std::string & requestBody,
                                    int timeoutInMillis,
                                    int numberOfTries);

virtual void setEmptyResponse(Poco::Net::HTTPServerResponse& response);

virtual void setBadRequestResponse(Poco::Net::HTTPServerResponse& response, std::string msg);
/**
 * returns the request body of an http request
 */
virtual std::string getRequestBody(Poco::Net::HTTPServerRequest& request);

virtual void printRequestProperties(Poco::Net::HTTPServerRequest& request);

virtual void printCookies(Poco::Net::HTTPServerRequest& request);

virtual std::string getDomainFromUrl(std::string url);
virtual std::string getDomainFromRequest(Poco::Net::HTTPServerRequest& request);


virtual void setOneCookieOnRequest(Poco::Net::HTTPRequest& request, const std::string& key, const std::string& value);
virtual void setCookiesOnRequest(Poco::Net::HTTPRequest& request, std::unordered_map<std::string, std::string> cookies);
virtual void setCookiesOnRequest(Poco::Net::HTTPRequest& request, Poco::Net::NameValueCollection cookies);
virtual std::string getCookieFromResponse(Poco::Net::HTTPResponse& response, const std::string& cookieName);
virtual std::unordered_map<std::string, std::string> getCookiesFromResponse(Poco::Net::HTTPResponse& response);
virtual std::unordered_map<std::string, std::string> getCookiesFromRequest(Poco::Net::HTTPServerRequest& request);
static std::shared_ptr<HttpUtilService> getInstance(gicapods::ConfigService* configService);
};
#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_HttpUtilService_H_ */
