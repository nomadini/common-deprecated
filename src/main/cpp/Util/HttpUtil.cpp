

#include <curl/curl.h>
#include "GUtil.h"
#include "HttpUtil.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "Poco/URI.h"
#include <string>
#include <memory>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/NameValueCollection.h"
#include "GUtil.h"
#include <boost/foreach.hpp>

void HttpUtil::downloadFileUsingScpWithCurl(std::string urlString, std::string destinationFileName) {
        CURL *curl;
        FILE *fp;
        CURLcode res;

        curl = curl_easy_init();
        if (curl)
        {
                fp = fopen(destinationFileName.c_str(),"wb");
                curl_easy_setopt(curl, CURLOPT_URL, urlString.c_str());
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
                /* adjust user and password */
                curl_easy_setopt(curl, CURLOPT_USERPWD,
                                 "root:mahinmahin");
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
                res = curl_easy_perform(curl);
                if (res != CURLE_OK) {
                        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                                curl_easy_strerror(res));
                }
                curl_easy_cleanup(curl);
                fclose(fp);
        }

}

void HttpUtil::setRequestHeaders(std::unordered_map<std::string,std::string > headers,
                                 Poco::Net::HTTPServerRequest& request) {
        // Set headers here
        for(std::unordered_map<std::string,std::string >::iterator it = headers.begin();
            it != headers.end(); it++) {
                request.set(it->first, it->second);
        }
}
void HttpUtil::getResponseHeaders(std::unordered_map<std::string,std::string >& headers,
                                  Poco::Net::HTTPServerResponse& res) {
        std::string name;
        std::string value;
        Poco::Net::NameValueCollection::ConstIterator i = res.begin();
        while(i!=res.end()) {

                name=i->first;
                value=i->second;
                MLOG(10)<<" response header : " << name << "=" << value;
                ++i;
                headers.insert(std::make_pair(name, value));
        }
}


std::string HttpUtil::getRequiredParamFromQueryParameters(
        std::string paramName,
        std::unordered_map <std::string, std::string> queryParams)
{
        auto pairPtr = queryParams.find(paramName);
        if(pairPtr != queryParams.end()) {
                return pairPtr->second;
        } else {
                LOG(ERROR)<< "queryParams : " << JsonMapUtil::convertMapToJsonArray(queryParams);
                throwEx ("BAD REQUEST, parameter is missing : " + paramName);
        }
}

std::unordered_map<std::string, std::string>  HttpUtil::getMapOfQueryParams(const Poco::Net::HTTPServerRequest& request) {
        std::string uriStr = request.getURI();
        Poco::URI uri(uriStr);
        std::unordered_map<std::string, std::string> mapOfParams;
        Poco::URI::QueryParameters params = uri.getQueryParameters();
        for (Poco::URI::QueryParameters::const_iterator it = params.begin();
             it != params.end(); ++it) {
                MLOG(10)<<"Query Param  " << it->first << " Value "<< it->second;
                mapOfParams.insert(std::pair<std::string, std::string>(it->first, it->second));
        }

        return mapOfParams;

}

std::unordered_map<std::string, std::string>  HttpUtil::getMapOfQueryParams(Poco::Net::HTTPServerRequest& request) {
        std::string uriStr = request.getURI();
        Poco::URI uri(uriStr);
        std::unordered_map<std::string, std::string> mapOfParams;
        Poco::URI::QueryParameters params = uri.getQueryParameters();
        for (Poco::URI::QueryParameters::const_iterator it = params.begin();
             it != params.end(); ++it) {
                MLOG(10)<<"Query Param  " << it->first << " Value "<< it->second;
                mapOfParams.insert(std::pair<std::string, std::string>(it->first, it->second));
        }

        return mapOfParams;

}
void HttpUtil::printQueryParamsOfRequest(Poco::Net::HTTPServerRequest & request) {
        std::string uriStr = request.getURI();
        Poco::URI uri(uriStr);

        Poco::URI::QueryParameters params = uri.getQueryParameters();
        for (Poco::URI::QueryParameters::const_iterator it = params.begin();
             it != params.end(); ++it) {
                MLOG(10)<<"Query Param  "<< it->first<<" Value "<< it->second;
        }
}


std::string HttpUtil::getDomainFromUrl(std::string url) {
        Poco::URI uri(url);
        return uri.getHost();
}

std::string HttpUtil::getRequestBody(Poco::Net::HTTPServerRequest& request)  {
        std::istreambuf_iterator<char> eos;
        std::string requestBody(std::istreambuf_iterator<char>(request.stream()),
                                eos);

        //MLOG(10)<<"request body : "<< requestBody <<_L_;
        if (requestBody.empty()) {
                // LOG(WARNING)<<"request body is empty ";
        }

        return requestBody;
}
/*
   you can send binary data using this method
 */
void HttpUtil::sendHttpPostViaCurl(const std::string & url,
                                   const std::string & postData,
                                   int numberOfTries) {
        assertAndThrow(!url.empty());
        for (int i=0; i < numberOfTries; i++) {

                try {
                        CURL *curl;
                        CURLcode res;

                        /* In windows, this will init the winsock stuff */
                        curl_global_init(CURL_GLOBAL_ALL);

                        /* get a curl handle  */
                        curl = curl_easy_init();
                        if (curl) {
                                /* First set the URL that is about to receive our POST. This URL can
                                   just as well be a https:// URL if that is what should receive the
                                   data. */
                                curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

                                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData.c_str());
                                /* set the size of the postfields data */
                                curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, postData.size());

                                /* Perform the request, res will get the return code */
                                res = curl_easy_perform(curl);
                                /* Check for errors */
                                if (res != CURLE_OK)
                                        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                                                curl_easy_strerror(res));

                                /* always cleanup */
                                curl_easy_cleanup(curl);
                        }
                        curl_global_cleanup();

                        return;
                } catch(...) {
                        LOG(ERROR)<<"ERROR in sending post request";
                        if (numberOfTries > 1) {
                                gicapods::Util::sleepViaBoost(_L_, 3);
                        }
                }
        }
        throwEx("Exception in sendHttpPostViaCurl");
}

std::string HttpUtil::sendGetRequestAndGetFullResponse(
        std::unordered_map<std::string, std::string> cookieMap,
        Poco::Net::HTTPResponse& response,
        const std::string & url,
        int timeoutInMillis,
        int numberOfTries) {
        assertAndThrow(!url.empty());
        Poco::URI uri(url);
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        Poco::Timespan timespan(0, timeoutInMillis * 1000);

        session.setTimeout(timespan);
        Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, url,
                                   Poco::Net::HTTPMessage::HTTP_1_1);

        for(auto pairPtr : cookieMap) {
                HttpUtil::setOneCookieOnRequest(req, pairPtr.first, pairPtr.second );
        }


        MLOG(10)<<"url is "<< url << "uri.getPathAndQuery() "<< uri.getPathAndQuery();

        for (int i=0; i < numberOfTries; i++) {
                try {

                        session.sendRequest(req);
                        std::istream &rsStr = session.receiveResponse(response);
                        std::string responseStr ((std::istreambuf_iterator<char> (rsStr)),
                                                 (std::istreambuf_iterator<char> ()));
                        return responseStr;
                } catch(Poco::Exception& e) {
                        LOG(ERROR)<<"ERROR in sending request " << e.displayText() << ", code : "<< e.code();
                        if (numberOfTries > 1) {
                                gicapods::Util::sleepViaBoost(_L_, 3);
                        }

                }
        }
        throwEx("Exception in sending request");


}

std::string HttpUtil::sendGetRequestAndGetFullResponse(
        const std::string &  cookieKey,
        const std::string &  cookieValue,
        Poco::Net::HTTPResponse& response,
        const std::string & url,
        int timeoutInMillis,
        int numberOfTries)
{
        assertAndThrow(!url.empty());
        Poco::URI uri(url);
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        Poco::Timespan timespan(0, timeoutInMillis * 1000);

        session.setTimeout(timespan);
        Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, url,
                                   Poco::Net::HTTPMessage::HTTP_1_1);

        HttpUtil::setOneCookieOnRequest(req, cookieKey, cookieValue );

        MLOG(10)<<"url is "<< url << "uri.getPathAndQuery() "<< uri.getPathAndQuery();

        for (int i=0; i < numberOfTries; i++) {
                try {

                        session.sendRequest(req);
                        std::istream &rsStr = session.receiveResponse(response);
                        std::string responseStr ((std::istreambuf_iterator<char> (rsStr)),
                                                 (std::istreambuf_iterator<char> ()));
                        return responseStr;
                } catch(Poco::Exception& e) {
                        if (gicapods::Util::allowedToCall(_FL_, 100000)) {
                                LOG(ERROR)<<google::COUNTER <<"th ERROR in sending request " << e.displayText() << ", code : "<< e.code();
                                gicapods::Util::showStackTrace(); //this is too noisy
                        }
                        if (numberOfTries > 1) {
                                gicapods::Util::sleepViaBoost(_L_, 3);
                        }

                }
        }
        throwEx("Exception in sending request");

}

std::string HttpUtil::sendGetRequestAndGetFullResponse(const std::string & url,
                                                       Poco::Net::HTTPResponse& response,
                                                       int timeoutInMillis,
                                                       int numberOfTries)
{

        assertAndThrow(!url.empty());
        Poco::URI uri(url);

        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        Poco::Timespan timespan(0, timeoutInMillis * 1000);

        session.setTimeout(timespan);

        Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, url,
                                   Poco::Net::HTTPMessage::HTTP_1_1);

        MLOG(10)<<"url is "<<url << "uri.getPathAndQuery() : "<< uri.getPathAndQuery();

        // Prepare and send request
        for (int i=0; i < numberOfTries; i++) {
                try {
                        session.sendRequest(req);
                        std::istream &rsStr = session.receiveResponse(response);
                        std::string responseStr ((std::istreambuf_iterator<char> (rsStr)),
                                                 (std::istreambuf_iterator<char> ()));
                        return responseStr;
                } catch(Poco::Exception& e) {
                        LOG(ERROR)<<"ERROR in sending request " << e.displayText() << ", code : "<< e.code();
                        if (numberOfTries > 1) {
                                gicapods::Util::sleepViaBoost(_L_, 3);
                        }

                }
        }
        throwEx("Exception in sending request");


}
std::string HttpUtil::sendGetRequest(
        const std::string & url,
        int timeoutInMillis,
        int numberOfTries)
{
        assertAndThrow(!url.empty());
        Poco::URI uri(url);

        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        Poco::Timespan timespan(0, timeoutInMillis * 1000);

        session.setTimeout(timespan);

        Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, url,
                                   Poco::Net::HTTPMessage::HTTP_1_1);

        MLOG(10)<<"url is "<< url << " uri.getPathAndQuery() :  " << uri.getPathAndQuery();
        // Prepare and send request
        for (int i=0; i < numberOfTries; i++) {
                try {
                        Poco::Net::HTTPResponse res;
                        session.sendRequest(req);

                        std::istream& rs = session.receiveResponse(res);
                        std::string response((std::istreambuf_iterator<char>(rs)),
                                             (std::istreambuf_iterator<char>()));
                        MLOG(10)<<"this is the response from server :  "<<response<<" , size : "<<response.size();

                        return response;
                } catch(Poco::Exception& e) {
                        LOG(ERROR)<<"ERROR in sending request " << e.displayText() << ", code : "<< e.code();
                        if (numberOfTries > 1) {
                                gicapods::Util::sleepViaBoost(_L_, 3);
                        }

                }
        }
        throwEx("Exception in sending request");

}

std::string HttpUtil::sendPostRequest(Poco::Net::HTTPClientSession& session,
                                      const std::string & url,
                                      const std::string & requestBody,
                                      int numberOfTries) {
        assertAndThrow(!url.empty());
        Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_POST, url,
                                   Poco::Net::HTTPMessage::HTTP_1_1);

        for (int i=0; i < numberOfTries; i++) {
                try {
                        // Prepare and send request
                        MLOG(10)<<"request body "<< requestBody;
                        std::ostream& ostr = session.sendRequest(req);
                        ostr << requestBody.c_str();


                        Poco::Net::HTTPResponse res;
                        std::istream& rs = session.receiveResponse(res);
                        std::string response((std::istreambuf_iterator<char>(rs)),
                                             (std::istreambuf_iterator<char>()));
                        MLOG(10)<<"this is the response from server : " << response;

                        return response;
                } catch(Poco::Exception& e) {
                        if (gicapods::Util::allowedToCall(_FL_, 100000)) {
                                LOG(ERROR)<<google::COUNTER <<"th ERROR in sending request " << e.displayText() << ", code : "<< e.code();
                                gicapods::Util::showStackTrace(); //this is too noisy
                        }
                        auto nested = e.nested();
                        if (nested != nullptr) {
                                LOG_EVERY_N(ERROR, 100)<<google::COUNTER <<"th ERROR in sending request nested message " << nested->displayText();
                        }
                        if (numberOfTries > 1) {
                                MLOG(1)<< "sleeping for 10 seconds";
                                gicapods::Util::sleepMiliSecond(10);
                        }

                }
        }
        throwEx("Exception in sending request", false);
}

std::string HttpUtil::sendPostRequest(const std::string & url,
                                      const std::string & requestBody,
                                      int timeoutInMillis,
                                      int numberOfTries) {

        assertAndThrow(!url.empty());
        Poco::URI uri(url);
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());

        Poco::Timespan timespan(0, timeoutInMillis * 1000);

        session.setTimeout(timespan);

        return sendPostRequest(session, url, requestBody, numberOfTries);
}

std::string HttpUtil::sendPostRequestAndGetFullResponse(const std::string & url,
                                                        const std::string & requestBody,
                                                        Poco::Net::HTTPResponse& res,
                                                        int timeoutInMillis,
                                                        int numberOfTries) {
        assertAndThrow(!url.empty());
        Poco::URI uri(url);

        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        Poco::Timespan timespan(0, timeoutInMillis * 1000);
        session.setTimeout(timespan);

        Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_POST, url,
                                   Poco::Net::HTTPMessage::HTTP_1_1);

        MLOG(3)<<"url is "<< url << " uri.getPathAndQuery() :  " << uri.getPathAndQuery();

        for (int i = 0; i < numberOfTries; i++) {
                try {
                        // Prepare and send request
                        MLOG(10)<<"request body "<< requestBody;
                        std::ostream& ostr = session.sendRequest(req);
                        ostr << requestBody.c_str();



                        std::istream& rs = session.receiveResponse(res);
                        std::string response((std::istreambuf_iterator<char>(rs)),
                                             (std::istreambuf_iterator<char>()));
                        MLOG(10)<<"this is the response from server : " << response;

                        return response;
                } catch(Poco::Exception& e) {
                        LOG(ERROR)<<"ERROR in sending request " << e.displayText() << ", code : "<< e.code();
                        if (numberOfTries > 1) {
                                gicapods::Util::sleepViaBoost(_L_, 3);
                        }

                }
        }
        throwEx("Exception in sending request");
}

void HttpUtil::printRequestProperties(Poco::Net::HTTPServerRequest& request) {

        MLOG(10) <<"request properties : request.getMethod  " <<request.getMethod()
                 <<" request.getURI  "<< request.getURI()
                 <<" request.getVersion  "<< request.getVersion()
                 <<" request.size  "<< request.size()
                 <<" request.Connection" << request["Connection"]
                 <<" request.Host  "<< request["Host"]
                 <<" request.User-Agent  " << request["User-Agent"]
                 <<" request.getContentType  "<< request.getContentType()
                 <<" request.getContentLength  "<<request.getContentLength();

}

void HttpUtil::printCookies(Poco::Net::HTTPServerRequest& request) {

        throwEx("not implemented");
        // Poco::Net::NameValueCollection cookies;
        // request.getCookies(cookies);
        // Poco::Net::NameValueCollection::ConstIterator it = cookies.find(name);
        // if (it != cookies.end()) {
        //         std::string userName = it->second;
        //
        // }
        //
}

void HttpUtil::setBadRequestResponse(Poco::Net::HTTPServerResponse& response, std::string msg) {
        response.setStatus(Poco::Net::HTTPResponse::HTTP_BAD_REQUEST);
        std::ostream& ostr = response.send();
        ostr << msg;
}


void HttpUtil::setEmptyResponse(Poco::Net::HTTPServerResponse& response) {
        response.setStatus(Poco::Net::HTTPResponse::HTTP_NO_CONTENT);
        std::ostream& ostr = response.send();
        ostr << "";
}

std::string HttpUtil::getCookieFromResponse(Poco::Net::HTTPResponse& response,  const std::string& cookieName) {

        std::vector<Poco::Net::HTTPCookie> cookies;
        response.getCookies(cookies);
        for(Poco::Net::HTTPCookie cookie :  cookies) {
                if(cookie.getName().compare(cookieName)==0) {
                        return cookie.getValue();

                }
        }

        LOG(ERROR) << cookieName<< " cookie not found...";
        for(Poco::Net::HTTPCookie cookie :  cookies) {
                LOG(ERROR) << "cookie name : "<< cookie.getName();
        }
        throwEx("cookie with name "+ cookieName + " not found!");

}
std::string HttpUtil::getCookieFromResponse(Poco::Net::HTTPServerResponse& response,  const std::string& cookieName) {

        std::vector<Poco::Net::HTTPCookie> cookies;
        response.getCookies(cookies);
        for(Poco::Net::HTTPCookie cookie :  cookies) {
                if(cookie.getName().compare(cookieName)==0) {
                        return cookie.getValue();

                }
        }

        LOG(ERROR) << cookieName<< " cookie not found...";
        for(Poco::Net::HTTPCookie cookie :  cookies) {
                LOG(ERROR) << "cookie name : "<< cookie.getName();
        }
        throwEx("cookie with name "+ cookieName + " not found!");

}

std::unordered_map<std::string, std::string> HttpUtil::getCookiesFromResponse(Poco::Net::HTTPResponse& response) {
        std::unordered_map<std::string, std::string> myCookies;
        std::vector<Poco::Net::HTTPCookie> cookies;
        response.getCookies(cookies);
        for(Poco::Net::HTTPCookie cookie :  cookies) {
                // LOG(ERROR) << "cookie name : "
                //            << cookie.getName() << " , value : "<<cookie.getValue();
                myCookies.insert(std::pair<std::string,std::string>(cookie.getName(),cookie.getValue()));
        }
        return myCookies;
}
std::unordered_map<std::string, std::string> HttpUtil::getCookiesFromResponse(Poco::Net::HTTPServerResponse& response) {
        std::unordered_map<std::string, std::string> myCookies;
        std::vector<Poco::Net::HTTPCookie> cookies;
        response.getCookies(cookies);
        for(Poco::Net::HTTPCookie cookie :  cookies) {
                // LOG(ERROR) << "cookie name : "
                //            << cookie.getName() << " , value : "<<cookie.getValue();
                myCookies.insert(std::pair<std::string,std::string>(cookie.getName(),cookie.getValue()));
        }
        return myCookies;
}

std::string HttpUtil::getDomainFromRequest(Poco::Net::HTTPServerRequest& request) {
        return request.getHost();
}

std::unordered_map<std::string, std::string> HttpUtil::getCookiesFromRequest(const Poco::Net::HTTPServerRequest& request) {
        std::unordered_map<std::string, std::string> myCookies;
        Poco::Net::NameValueCollection cookies;
        request.getCookies(cookies);
        Poco::Net::NameValueCollection::ConstIterator i = cookies.begin();

        while(i!=cookies.end()) {
                myCookies.insert(std::pair<std::string,std::string>(i->first,
                                                                    i->second));
                ++i;
        }

        return myCookies;
}

std::unordered_map<std::string, std::string> HttpUtil::getCookiesFromRequest(Poco::Net::HTTPServerRequest& request) {

        std::unordered_map<std::string, std::string> myCookies;
        Poco::Net::NameValueCollection cookies;
        request.getCookies(cookies);
        Poco::Net::NameValueCollection::ConstIterator i = cookies.begin();

        while(i!=cookies.end()) {
                myCookies.insert(std::pair<std::string,std::string>(i->first,
                                                                    i->second));
                ++i;
        }

        return myCookies;

}

void HttpUtil::addCookieToResponse(
        std::string cookieName,
        std::string cookieValue,
        Poco::Net::HTTPServerResponse* httpResponse) {
        Poco::Net::NameValueCollection cookieMapOutgoing;
        cookieMapOutgoing.add(cookieName, cookieValue);

        Poco::Net::HTTPCookie cookieOut(cookieMapOutgoing);
        httpResponse->addCookie(cookieOut);
}

void HttpUtil::setOneCookieOnRequest(Poco::Net::HTTPRequest& request,
                                     const std::string& key, const std::string& value) {
        Poco::Net::NameValueCollection currentCookies;
        request.getCookies(currentCookies);
        currentCookies.add(key, value);
        HttpUtil::setCookiesOnRequest(request, currentCookies);
}
void HttpUtil::setCookiesOnRequest(Poco::Net::HTTPRequest& request, Poco::Net::NameValueCollection cookies) {
        request.setCookies(cookies);
}
void HttpUtil::setCookiesOnRequest(Poco::Net::HTTPRequest& request, std::unordered_map<std::string, std::string> cookies) {

        Poco::Net::NameValueCollection cookieMapOutgoing;

        typedef std::unordered_map<std::string, std::string> map_t;
        for(map_t::value_type &entry :
            cookies)
        {
                cookieMapOutgoing.add(entry.first, entry.second);

        }
        request.setCookies(cookieMapOutgoing);
}

void HttpUtil::setNoContentAndEmptyResponse(Poco::Net::HTTPServerResponse &response, std::string responseStr) {
        response.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_NO_CONTENT);
        std::ostream &ostr = response.send ();
        ostr << responseStr;
}
