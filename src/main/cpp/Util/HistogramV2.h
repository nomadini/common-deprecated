/*
 * Histogram.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef Histogram_H_
#define Histogram_H_

#include <string>
#include <memory>
#include "AtomicLong.h"
#include <vector>
#include "Object.h"
class HistogramV2 : public Object {

public:

HistogramV2();
virtual ~HistogramV2();
static void printHistogramOfData(std::vector<double> data, int numberOfBins);

static double getNthPercentileOfData(std::vector<double> data, int percentile);

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_NUMBERUTIL_H_ */
