/*
 * CacheManagerTestHelper.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_CACHEMANAGERTESTHELPER_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_CACHEMANAGERTESTHELPER_H_

#include "CreativeTypeDefs.h"
class Creative;
class CampaignCacheService;
class TargetGroupCacheService;
class CreativeCacheService;
class TargetGroupCacheService;

class CacheManagerServiceTestHelper;


class CacheManagerServiceTestHelper {

public:
TargetGroupCacheService* targetGroupCacheService;
CampaignCacheService* campaignCacheService;
CreativeCacheService* creativeCacheService;
CacheManagerServiceTestHelper(TargetGroupCacheService* targetGroupCacheService,
                              CampaignCacheService* campaignCacheService,
                              CreativeCacheService* creativeCacheService){
        this->targetGroupCacheService = targetGroupCacheService;
        this->campaignCacheService = campaignCacheService;
        this->creativeCacheService = creativeCacheService;
}


void putTgInRelativeMaps(std::shared_ptr<TargetGroup> tg, std::shared_ptr<Campaign> cmp, std::shared_ptr<Creative> creativePtr) {
        auto listOfCreativeIds = CollectionUtil::convertToList<int> (
                creativePtr->id);

        campaignCacheService->getAllEntitiesMap()->insert (
                std::pair<int, std::shared_ptr<Campaign>> (tg->getCampaignId(),
                                             cmp));

        creativeCacheService->getAllEntitiesMap()->insert (
                std::pair<int, std::shared_ptr<Creative>> (creativePtr->id,
                                             creativePtr));

        targetGroupCacheService->getAllEntitiesMap()->insert (
                std::pair<int, std::shared_ptr<TargetGroup>> (tg->id, tg));

        targetGroupCacheService->getAllEntities()->push_back (tg);
        targetGroupCacheService->getAllTargetGroupCreativesMap()->insert (
                std::pair <int, std::vector < int > > (tg->id,
                                                       listOfCreativeIds));

}
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_CACHEMANAGERTESTHELPER_H_ */
