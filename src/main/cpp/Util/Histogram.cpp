#include "Histogram.h"
#include "GUtil.h"
#include "NumberUtil.h"
#include <cmath>

#include <vector>
#include <algorithm>
#include <iostream>

Histogram::Histogram(double min, double max, int numberOfBinsArg) : recordHolder(numberOfBinsArg), Object(__FILE__) {
								this->min = min;
								this->max = max;
								for (int i= 0; i < numberOfBinsArg; i++) {
																recordHolder.at(i) = std::make_shared<gicapods::AtomicLong>();
								}
								lowerOutlierCount = std::make_shared<gicapods::AtomicLong>();
								upperOutlierCount = std::make_shared<gicapods::AtomicLong>();
								totalCount = std::make_shared<gicapods::AtomicLong>();
								this->binCount = numberOfBinsArg;
								double bandWithPrecise = (max - min) / (double) numberOfBinsArg;
								//we make the bandwidth to be a proper integer, by rounding it up
								this->binWidth = NumberUtil::roundDouble(bandWithPrecise, 0);

								this->percentagePerBin = 100 / (double) binCount;

								// LOG(ERROR) << " bandWithPrecise : "<<bandWithPrecise;
								// LOG(ERROR) << " binWidth : "<<binWidth;

}

int Histogram::bins() const {
								return binCount;
}
int Histogram::count(int bin) const {
								assertAndThrow(bin >= 0 && bin < recordHolder.size());
								return (int)recordHolder.at(bin)->getValue();
}
int Histogram::countLowerOutliers() const {
								return lowerOutlierCount->getValue();
}
int Histogram::countUpperOutliers() const {
								return upperOutlierCount->getValue();
}
double Histogram::lowerBound(int bin)  const {
								assertAndThrow(bin >= 0 && bin < recordHolder.size());
								return bin * binWidth;
}

void Histogram::printBinCounts() const {
								for (int i = 0; i < bins(); ++i) {
																LOG(ERROR)<< "In range " << lowerBound(i)
																										<< " to " << upperBound(i)
																										<< ", total values : " << count(i);
								}
								LOG(ERROR)<< "x > "<< max<< " count : " << upperOutlierCount->getValue();

								LOG(ERROR)<< "x < "<< min<< " count : " << lowerOutlierCount->getValue();
}

void Histogram::printBinPercentages() const {
								for (int i = 0; i < bins(); ++i) {
																LOG(ERROR)<< "In range " << lowerBound(i)
																										<< " to " << upperBound(i)
																										<< ", total percentage : " <<
																								100 * (count(i) / ((double)totalCount->getValue()));
								}

								LOG(ERROR)<< "upperOutlierCount percentage : " <<
																100 * (upperOutlierCount->getValue() / ((double)totalCount->getValue()));

								LOG(ERROR)<< "lowerOutlierCount percentage : " <<
																100 * (lowerOutlierCount->getValue() / ((double)totalCount->getValue()));

}

double Histogram::upperBound(int bin)  const {
								assertAndThrow(bin >= 0 && bin < recordHolder.size());
								return (bin + 1) * binWidth;
}


bool Histogram::isAmongTopXPercentOfDataWithHighestValues(double datum, double percentageAskedFor) const {

								// LOG(ERROR) << "checking new datum : "<< datum<<", percentageAskedFor : "<< percentageAskedFor;
								//taking care of extreme edge cases
								if (percentageAskedFor >= 100) {
																return true;
								} else if (percentageAskedFor <= 0) {
																return false;
								}

								int binIndex = (int)((datum - min) / binWidth);


								double sumOfPercentagesCountedSoFar = 0;
								double upperOutliersPercentage =
																100 * (upperOutlierCount->getValue() / ((double)totalCount->getValue()));

								sumOfPercentagesCountedSoFar += upperOutliersPercentage;


								// LOG(ERROR) << "datum : " << datum;
								// LOG(ERROR) << "min : " << min;
								// LOG(ERROR) << "binWidth : " << binWidth;
								// LOG(ERROR) << "binIndex : " << binIndex;
								// LOG(ERROR) << "upperOutliersPercentage : " << upperOutliersPercentage;


								if (binIndex >= binCount) {
																//the datum is an upper outlier
																if (upperOutliersPercentage >= percentageAskedFor) {
																								return true;
																}
								}
								bool binOfDatumIsCounted = false;
								for (int i = binCount - 1; i >= 0; i--) {
																double binPercentageCount = 100 * (count(i) / ((double)totalCount->getValue()));
																sumOfPercentagesCountedSoFar += binPercentageCount;
																// LOG(ERROR) << "binPercentageCount : " << binPercentageCount;
																// LOG(ERROR) << "sumOfPercentagesCountedSoFar : " << sumOfPercentagesCountedSoFar;
																if (binIndex == i) {
																								// LOG(ERROR) << "saw binIndex of datum ";
																								binOfDatumIsCounted = true;
																}
																if (sumOfPercentagesCountedSoFar >= percentageAskedFor) {
																								break;
																}


								}

								if (sumOfPercentagesCountedSoFar < percentageAskedFor) {
																//we don't need to count lower outlier
																//we have counted all the data
																binOfDatumIsCounted = true;
																//this sum is just for showing that we have counted 100 percent of data
																double lowerOutliersPercentage =
																								100 * (lowerOutlierCount->getValue() / ((double)totalCount->getValue()));
																sumOfPercentagesCountedSoFar+= lowerOutliersPercentage;
								}

								// LOG(ERROR) << "sumOfPercentagesCountedSoFar : " << sumOfPercentagesCountedSoFar;

								return binOfDatumIsCounted;
}

void Histogram::record(double datum) {
								totalCount->increment();


								LOG_EVERY_N(ERROR, 10000) << " size of recordHolder : "<< recordHolder.size();

								if (datum < min) {
																lowerOutlierCount->increment();
																return;
								} else if (datum > max) {
																upperOutlierCount->increment();
																return;
								}
								int bin = (int)((datum - min) / (double)binWidth);
								// LOG(ERROR)<< "record : bin of datum is : "<<bin;
								if (bin < 0) {
																lowerOutlierCount->increment();
								} else if (bin >= binCount) {
																upperOutlierCount->increment();
								} else {
																recordHolder[bin]->increment();
								}

}


Histogram::~Histogram() {

}
