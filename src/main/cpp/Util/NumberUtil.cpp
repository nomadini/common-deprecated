#include "NumberUtil.h"
#include <cmath>

NumberUtil::NumberUtil() {
}

double NumberUtil::roundDouble(double value, double precision)
{
								return (floor((value * pow(10, precision) + 0.5)) / pow(10, precision));
}

bool NumberUtil::areDoubleValuesEqual(double value1, double value2, double epsilon) {
								return fabs(value1 - value1) < epsilon;
}

//round to the nearest multiple of a number, short of rounding both up and down
// number   multiple   result
// 12       5          10
// 13       5          15
// 149      10         150
long NumberUtil::getNearestMultipleToNumber(long number, int multiple) {
								return ((number + multiple/2) / multiple) * multiple;
}
