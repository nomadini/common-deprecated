
#ifndef _STACKTRACE_UTIL_H_
#define _STACKTRACE_UTIL_H_



/**
 * you should compile the program with -rdynamic flag
 */
namespace {
class StackTraceUtil {


public:
/** Print a demangled stack backtrace of the caller function to FILE* out. */
static void print_stacktrace11(FILE *out = stdout, unsigned int max_frames = 63);

static void print_trace11();

};

}
#endif // _STACKTRACE_H_
