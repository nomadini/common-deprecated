/*
 * TempUtil.h
 *
 *  Created on: Aug 2, 2015
 *      Author: mtaabodi



 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_TempUtil_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_TempUtil_H_



#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#define UNW_LOCAL_ONLY
#include <libunwind.h>

#include <tbb/concurrent_unordered_set.h>


#define assertAndThrow1(arg1) TempUtil::assertAndThrow1_(_L_, arg1, #arg1);
class TempUtil {
public:

/**
 * helper method that starts up necessary threads and flags and other important stuff before
 * an app runs
 */
static void configureLogging(std::string appName, char* argv[]);
static void deleteLockFile(std::string appName);

static void yourFailureFunction();
};
#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_TempUtil_H_ */
