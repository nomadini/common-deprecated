
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"
#include "CassandraServiceTypeTraits.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "NumberUtil.h"
#include "CassandraServiceDirectDb.h"
#include "AsyncThreadPoolService.h"

#include "Poco/Runnable.h"

#include "EntityToModuleStateStats.h"
#include "StatisticsUtil.h"
#include "RandomUtil.h"
#include "ValueBatchUpdateTask.h"
#include "ValueTraceTask.h"
#include "AtomicLong.h"
#include "ConfigService.h"
#include "CassandraServiceQueueManager.h"
#include "HttpUtil.h"
#include <thread>


template <class V>
CassandraServiceDirectDb<V>::CassandraServiceDirectDb(
								CassandraDriverInterface* cassandraDriver,
								EntityToModuleStateStats* entityToModuleStateStats,
								AsyncThreadPoolService* asyncThreadPoolService,
								gicapods::ConfigService* configService)  : Object(__FILE__) {

								this->cassandraDriver = cassandraDriver;
								this->entityToModuleStateStats =  entityToModuleStateStats;

								this->asyncThreadPoolService =  asyncThreadPoolService;
								this->configService =  configService;

								cassandraServiceTypeTraits = std::make_unique<CassandraServiceTypeTraits<V> > (configService);
								cassandraServiceTypeTraits->loadProperties();


								cassandraReadService = std::make_unique<CassandraReadService<V> > (
																cassandraDriver,
																entityToModuleStateStats,
																asyncThreadPoolService,
																configService
																);

								cassandraTracer = std::make_unique<CassandraTracer<V> > (
																cassandraReadService.get(),
																entityToModuleStateStats,
																asyncThreadPoolService,
																configService
																);
								cassandraBatchInserter = std::make_unique<CassandraBatchInserter<V> > (
																cassandraTracer.get(),
																entityToModuleStateStats,
																asyncThreadPoolService,
																configService
																);

								tracingDecisionRandomizer = std::make_shared<StatisticsUtil> ();

								numberOfReadFailures = std::make_shared<gicapods::AtomicLong>();
								numberOfTotalReads = std::make_shared<gicapods::AtomicLong>();

}

template <class V>
CassandraServiceDirectDb<V>::~CassandraServiceDirectDb() {

}

template <class V>
std::string CassandraServiceDirectDb<V>::getName() {
								return "CassandraServiceDirectDb";
}

template <class V>
void CassandraServiceDirectDb<V>::writeBatchDataToDb(
								std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& entities) {

								int maxSizeOfBatch = 10;
								int totalEventsInBatches = 0;
								for (int totalIndex=0; totalIndex < entities->size(); ) {

																std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > > batch =
																								std::make_shared<std::vector<std::shared_ptr<CassandraManagedType> > >();
																while (batch->size() < maxSizeOfBatch && totalIndex < entities->size()) {

																								if (totalIndex < entities->size()) {
																																batch->push_back(entities->at(totalIndex));
																								}
																								totalIndex++;
																}

																MLOG(3) << "recording "
																								<< batch->size()
																								<<" sized batch of '"<<cassandraServiceTypeTraits->getValueType()
																								<< "' values in cassandra";
																totalEventsInBatches += batch->size();
																writeOneBatch(batch);
								}

								assertAndThrow(totalEventsInBatches ==  entities->size());
}

template <class V>
void CassandraServiceDirectDb<V>::writeOneBatch(
								std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& arrayOfValues) {

								CassSession* session = cassandraDriver->session;
								if (session == nullptr) {
																entityToModuleStateStats->addStateModuleForEntity(
																								"WRITE_CANCELED" + cassandraServiceTypeTraits->getValueType(),
																								getName(),
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::warning);
																LOG(ERROR)<< "session is null, maybe driver is a no op";
																return;
								}
								const CassPrepared* prepared = NULL;
								std::string query = cassandraServiceTypeTraits->getBatchWriteQuery();
								auto resultOfBatchInsertion = cassandraBatchInserter->prepare_insert_into_batch(session, &prepared, query);

								if (resultOfBatchInsertion == CASS_OK) {
																cassandraBatchInserter->insert_into_batch_with_prepared(session, prepared, arrayOfValues);
																entityToModuleStateStats->addStateModuleForEntityWithValue(
																								"write_happened" + cassandraServiceTypeTraits->getValueType(),
																								arrayOfValues->size(),
																								getName(),
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::important);


								} else {
																LOG(ERROR) << "batch failed for datatype ";
																entityToModuleStateStats->addStateModuleForEntity(
																								"BATCHING_FAILED" + cassandraServiceTypeTraits->getValueType(),
																								getName(),
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::exception);
								}
								if(prepared) {
																cass_prepared_free(prepared);
								}
}

#include "GicapodsIdToExchangeIdsMap.h"
//this is special version of writing batch for GicapodsIdToExchangeIdsMap
//because this table has multiple columns , one for each exchange and
//each batch needs to write/update only one column like google, or rubicon


template <>
void CassandraServiceDirectDb<GicapodsIdToExchangeIdsMap>::
writeOneBatch(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& arrayOfValues) {
								CassSession* session = cassandraDriver->session;
								if (session == nullptr) {
																entityToModuleStateStats->addStateModuleForEntity(
																								"WRITE_CANCELED" + cassandraServiceTypeTraits->getValueType(),
																								getName(),
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::warning);
																LOG(ERROR)<< "session is null, maybe driver is a no op";
																return;
								}
								std::vector<std::string> exchangeNamesToBatchFor = {"google", "rubicon"};
								for (auto exchangeNameToBatchWrite : exchangeNamesToBatchFor) {
																auto arrayOfExchangeValues = std::make_shared<std::vector<std::shared_ptr<CassandraManagedType> > >();


																for (auto valueGeneral : *arrayOfValues) {
																								std::shared_ptr<GicapodsIdToExchangeIdsMap> value = std::static_pointer_cast<GicapodsIdToExchangeIdsMap>(valueGeneral);

																								if (StringUtil::equalsIgnoreCase(
																																				value->exchangeNameToBatchWrite, exchangeNameToBatchWrite)) {
																																arrayOfExchangeValues->push_back(value);
																								}
																}
																const CassPrepared* prepared = NULL;

																std::string query =
																								" insert into gicapods.nomadiniDeviceIdToExchangeIdsMap "
																								" ( __EXCHANGE_NAME__, nomadinideviceid, datecreated) "
																								" values (?, ?, ? )";

																query = StringUtil::replaceString(query, "__EXCHANGE_NAME__", exchangeNameToBatchWrite);

																auto resultOfBatchInsertion =
																								cassandraBatchInserter->prepare_insert_into_batch(session, &prepared, query);
																if (resultOfBatchInsertion == CASS_OK) {
																								if(arrayOfExchangeValues->empty()) {
																																//no values exist for this exchange
																																//ignore this exchange
																																return;
																								}
																								cassandraBatchInserter->insert_into_batch_with_prepared(session, prepared, arrayOfExchangeValues);
																								cass_prepared_free(prepared);

																} else {
																								entityToModuleStateStats->addStateModuleForEntity(
																																"EXCEPTION_WRITING_BATACH_FAILED" + cassandraServiceTypeTraits->getValueType(),
																																getName(),
																																EntityToModuleStateStats::all,
																																EntityToModuleStateStats::exception);
																								LOG(ERROR) << "batch failed for datatype ";

																}

								}


}

template <>
void CassandraServiceDirectDb<GicapodsIdToExchangeIdsMap>::
writeBatchDataToDb(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& arrayOfValues) {
								writeOneBatch(arrayOfValues);
}

#include "FeatureDeviceHistory.h"
#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"
#include "PixelDeviceHistory.h"
#include "DeviceSegmentHistory.h"
#include "EventLog.h"
#include "Wiretap.h"
#include "AdHistory.h"
#include "EventLog.h"
#include "ModelScore.h"
#include "FeatureToFeatureHistory.h"
#include "SegmentDevices.h"
template class CassandraServiceDirectDb<SegmentDevices>;
template class CassandraServiceDirectDb<FeatureToFeatureHistory>;
template class CassandraServiceDirectDb<AdHistory>;
template class CassandraServiceDirectDb<EventLog>;
template class CassandraServiceDirectDb<ModelScore>;
template class CassandraServiceDirectDb<BidEventLog>;
template class CassandraServiceDirectDb<GicapodsIdToExchangeIdsMap>;
template class CassandraServiceDirectDb<FeatureDeviceHistory>;
template class CassandraServiceDirectDb<DeviceFeatureHistory>;
template class CassandraServiceDirectDb<IpToDeviceIdsMap>;
template class CassandraServiceDirectDb<DeviceIdToIpsMap>;
template class CassandraServiceDirectDb<PixelDeviceHistory>;

template class CassandraServiceDirectDb<DeviceSegmentHistory>;

template class CassandraServiceDirectDb<Wiretap>;
