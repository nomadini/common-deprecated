//things we store in cassandra : BrowserOffer
#ifndef CassandraTracer_h
#define CassandraTracer_h



#include "cassandra.h"


namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
class CassandraDriverInterface;
class EntityToModuleStateStats;
#include <tbb/concurrent_queue.h>
#include "NomadiniTask.h"
template<class V>
class CassandraServiceTypeTraits;
#include "NomadiniTask.h"
template<class V>
class CassandraReadService;

#include "CassandraManagedType.h"
#include "StatisticsUtil.h"
class AsyncThreadPoolService;
class CassandraServiceQueueManager;

class RandomUtil;
#include "Object.h"

template<class V>
class CassandraTracer : public Object {

public:
std::shared_ptr<StatisticsUtil> tracingDecisionRandomizer;
std::shared_ptr<gicapods::AtomicLong> numberOfReadFailures;
std::shared_ptr<gicapods::AtomicLong> numberOfTotalReads;
std::unique_ptr<CassandraServiceTypeTraits<V> > cassandraServiceTypeTraits;
CassandraReadService<V>* cassandraReadService;
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::ConfigService* configService;
std::unique_ptr<CassandraServiceQueueManager> cassandraServiceQueueManager;

AsyncThreadPoolService* asyncThreadPoolService;

CassandraTracer(CassandraReadService<V>* cassandraReadService,
                EntityToModuleStateStats* entityToModuleStateStats,

                AsyncThreadPoolService* asyncThreadPoolService,
                gicapods::ConfigService* configService);

void scheduleForTracing(std::shared_ptr<V> oldValue);

std::string getName();

virtual ~CassandraTracer();
};

#endif
