//things we store in cassandra : BrowserOffer
#ifndef CassandraService_h
#define CassandraService_h



#include "cassandra.h"


namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
class CassandraDriverInterface;
class EntityToModuleStateStats;
#include <tbb/concurrent_queue.h>
#include "NomadiniTask.h"
class AsyncThreadPoolService;

class CassandraServiceQueueManager;
class CassandraManagedType;
template<class V>
class CassandraServiceTypeTraits;

#include "CassandraServiceDirectDb.h"
#include "CassandraServiceInterface.h"
#include "DateTimeMacro.h"
#include "GUtil.h"
#include "Object.h"
template<class V>
class CassandraService : public CassandraServiceInterface, public Object {

public:

std::unique_ptr<CassandraServiceTypeTraits<V> > cassandraServiceTypeTraits;
std::unique_ptr<CassandraServiceDirectDb<V> > cassandraServiceDirectDb;
bool directDbCallEnabled;
std::string cassandraRemoteServerUrl;
CassandraDriverInterface* cassandraDriver;
CassandraServiceQueueManager* cassandraServiceQueueManager;
EntityToModuleStateStats* entityToModuleStateStats;


AsyncThreadPoolService* asyncThreadPoolService;

void submitTaskToAsyncService();

CassandraService(CassandraDriverInterface* cassandraDriver,
                 EntityToModuleStateStats* entityToModuleStateStats,

                 AsyncThreadPoolService* asyncThreadPoolService,
                 gicapods::ConfigService* configService);

std::shared_ptr<V> readDataOptional(std::shared_ptr<V> value, int timeoutInMillis = 10000);
std::shared_ptr<V> readDataOptionalOverHttp(std::shared_ptr<V> value, int timeoutInMillis = 10000);

void dequeueTheAsyncTasks();


void writeBatchData(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& data);
void writeBatchDataOverHttp(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& data);

void pushToWriteBatchQueue(std::shared_ptr<CassandraManagedType> value);

std::shared_ptr<std::vector<std::shared_ptr<V> > > readWithPaging(int pageSize, int limit);

virtual ~CassandraService();
};

#endif
