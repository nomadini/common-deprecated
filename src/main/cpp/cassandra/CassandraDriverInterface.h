//things we store in cassandra : BrowserOffer
#ifndef CassandraDriverInterface_h
#define CassandraDriverInterface_h



#include "cassandra.h"

#include "Object.h"
namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
class EntityToModuleStateStats;

class CassandraDriverInterface;



/*
   uses this cpp driver documentation is here : https://datastax.github.io/cpp-driver/topics/basics/

 */
class CassandraDriverInterface {

public:
CassSession* session;
std::shared_ptr<gicapods::AtomicLong> numberOfCuncurrentReuqestsNow;

CassandraDriverInterface();
virtual ~CassandraDriverInterface();

virtual CassCluster* create_cluster() = 0;

virtual void closeSessionAndCluster() = 0;
virtual void setRequestTimeout(int timeoutInMillis) = 0;
virtual int getRequestTimeout()= 0;

virtual CassError connect_session(CassSession *session, const CassCluster *cluster)= 0;


virtual int startCassandraCluster()= 0;

virtual void deleteAllTestEntries()= 0;

virtual CassSession* getSession()= 0;

virtual void deleteAll(std::string tableName)= 0;


static std::string getStringValueFromRowAtColumn(CassIterator *rowIterator, int index);
static std::string getStringValueFromRowByName(CassIterator* rowIterator, const char* name);
static long long getLongValueFromRowAtColumn(CassIterator *rowIterator, int index);
static long long getLongValueFromRowByName(CassIterator *rowIterator, const char* name);
static int getIntegerValueFromRowAtColumn(CassIterator *rowIterator, int index);
static int getIntegerValueFromRowByName(CassIterator *rowIterator, const char* name);
static double getDoubleValueFromRowAtColumn(CassIterator *rowIterator, int index);
static double getDoubleValueFromRowByName(CassIterator *rowIterator, const char* name);


};

#define print_error(args ...) CassandraDriver::_print_error(__func__,args)
#define printErrorWithNoExit(args ...) CassandraDriver::_printErrorWithNoExit(__func__,args)

#endif
