



#include "GUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <boost/exception/all.hpp>
#include "ValueBatchUpdateTask.h"

#include "CassandraServiceTypeTraits.h"
#include "EntityToModuleStateStats.h"
#include "CassandraManagedType.h"


ValueBatchUpdateTask::ValueBatchUpdateTask(
        EntityToModuleStateStats* entityToModuleStateStats)
        : NomadiniTask(entityToModuleStateStats), Object(__FILE__) {

        typesToQueueOfWriteBatches =
                std::make_shared<std::unordered_map<std::string, std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > > > >();

        this->entityToModuleStateStats = entityToModuleStateStats;


}


void ValueBatchUpdateTask::pushValueInQueue(std::shared_ptr<CassandraManagedType> value) {
        auto pair = typesToQueueOfWriteBatches->find(value->getValueType());
        if ( pair == typesToQueueOfWriteBatches->end()) {
                typesToQueueOfWriteBatches->insert(std::make_pair(value->getValueType(),
                                                                  std::make_shared<std::vector<std::shared_ptr<CassandraManagedType> > >()
                                                                  ));
                pair = typesToQueueOfWriteBatches->find(value->getValueType());
        }
        pair->second->push_back(value);
}


void ValueBatchUpdateTask::writeBatchDataForType(std::string type,
                                                 std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& queueOfWriteBatches) {
        try {
                auto pairOfTypeToService = mapOfTypesToCassandraServices->find(type);
                if (pairOfTypeToService != mapOfTypesToCassandraServices->end()) {

                        pairOfTypeToService->second->writeBatchData(queueOfWriteBatches);

                        entityToModuleStateStats->addStateModuleForEntity(
                                "BATCH_UPDATE_SUCCESS_IN_TASK" + type,
                                "ValueBatchUpdateTask",
                                EntityToModuleStateStats::all);
                } else {
                        throwEx("no service found for type : " + type);
                }
        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "ValueBatchUpdateTasK_EXCEPTION"+ type,
                        "ValueBatchUpdateTask",
                        "ALL",
                        EntityToModuleStateStats::exception
                        );
                LOG(ERROR)<<"error happening when handling request  "<<boost::diagnostic_information(e);
        } catch (...) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "ValueBatchUpdateTasK_EXCEPTION"+type,
                        "ValueBatchUpdateTask",
                        "ALL",
                        EntityToModuleStateStats::exception
                        );
                LOG(ERROR)<<"unknown error happening when handling request";
        }
}

void ValueBatchUpdateTask::runTask() {

        for (auto pairOfTypeToQueues : *typesToQueueOfWriteBatches) {
                std::string type = pairOfTypeToQueues.first;
                auto queueOfWriteBatches = pairOfTypeToQueues.second;
                writeBatchDataForType(type, queueOfWriteBatches);
        }

}


ValueBatchUpdateTask::~ValueBatchUpdateTask() {

}
