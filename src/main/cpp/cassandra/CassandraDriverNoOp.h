//things we store in cassandra : BrowserOffer
#ifndef CassandraDriverNoOp_h
#define CassandraDriverNoOp_h



#include "cassandra.h"

#include "Object.h"
namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
#include "CassandraDriverInterface.h"
class EntityToModuleStateStats;

class CassandraDriverNoOp;



/*
   uses this cpp driver documentation is here : https://datastax.github.io/cpp-driver/topics/basics/

 */
class CassandraDriverNoOp : public CassandraDriverInterface {

public:


CassandraDriverNoOp();
virtual ~CassandraDriverNoOp();

CassCluster* create_cluster();

void closeSessionAndCluster();
void setRequestTimeout(int timeoutInMillis);
int getRequestTimeout();

CassError connect_session(CassSession *session, const CassCluster *cluster);

int startCassandraCluster();

void deleteAllTestEntries();

CassSession* getSession();

void deleteAll(std::string tableName);

};
#endif
