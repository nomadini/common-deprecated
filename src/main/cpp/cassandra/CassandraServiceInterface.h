/*
 * CassandraServiceInterface.h
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef CassandraServiceInterface_H
#define CassandraServiceInterface_H


class CassandraManagedType;

class CassandraServiceInterface {
public:
virtual void writeBatchData(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& arrayOfValues) = 0;
virtual void pushToWriteBatchQueue(std::shared_ptr<CassandraManagedType> value) = 0;
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_SVMSGDWORKER_H_ */
