//things we store in cassandra : BrowserOffer
#ifndef CassandraServiceDirectDb_h
#define CassandraServiceDirectDb_h



#include "cassandra.h"


namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
class CassandraDriverInterface;
class EntityToModuleStateStats;
#include <tbb/concurrent_queue.h>
#include "NomadiniTask.h"
template<class V>
class CassandraServiceTypeTraits;

#include "CassandraTracer.h"
#include "CassandraManagedType.h"
#include "StatisticsUtil.h"
#include "CassandraReadService.h"
#include "CassandraBatchInserter.h"
class AsyncThreadPoolService;
class CassandraServiceQueueManager;

class RandomUtil;
#include "Object.h"

template<class V>
class CassandraServiceDirectDb : public Object {

public:
std::shared_ptr<StatisticsUtil> tracingDecisionRandomizer;
std::shared_ptr<gicapods::AtomicLong> numberOfReadFailures;
std::shared_ptr<gicapods::AtomicLong> numberOfTotalReads;
std::unique_ptr<CassandraServiceTypeTraits<V> > cassandraServiceTypeTraits;
std::unique_ptr<CassandraBatchInserter<V> > cassandraBatchInserter;
std::unique_ptr<CassandraTracer<V> > cassandraTracer;
std::unique_ptr<CassandraReadService<V> > cassandraReadService;
CassandraDriverInterface* cassandraDriver;
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::ConfigService* configService;
std::unique_ptr<CassandraServiceQueueManager> cassandraServiceQueueManager;

AsyncThreadPoolService* asyncThreadPoolService;

void submitTaskToAsyncService();

CassandraServiceDirectDb(CassandraDriverInterface* cassandraDriver,
                         EntityToModuleStateStats* entityToModuleStateStats,

                         AsyncThreadPoolService* asyncThreadPoolService,
                         gicapods::ConfigService* configService);

std::string getName();

void scheduleForTracing(std::shared_ptr<V> value);

void writeBatchDataToDb(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& data);

void writeOneBatch(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& data);

void recordFailure();

virtual ~CassandraServiceDirectDb();
};

#endif
