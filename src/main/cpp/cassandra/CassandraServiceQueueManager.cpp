
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "CassandraServiceQueueManager.h"
#include "AsyncThreadPoolService.h"

#include "Poco/Runnable.h"

#include "EntityToModuleStateStats.h"
#include "StatisticsUtil.h"
#include "RandomUtil.h"
#include "ValueBatchUpdateTask.h"
#include "ValueTraceTask.h"
#include "AtomicLong.h"
#include "ConfigService.h"
#include "HttpUtil.h"
#include <thread>

CassandraServiceQueueManager::CassandraServiceQueueManager(
								CassandraDriverInterface* cassandraDriver,
								EntityToModuleStateStats* entityToModuleStateStats,

								AsyncThreadPoolService* asyncThreadPoolService,
								int dequeueAsyncTasksIntervalInSeconds)  : Object(__FILE__) {

								this->cassandraDriver = cassandraDriver;
								this->entityToModuleStateStats =  entityToModuleStateStats;

								this->asyncThreadPoolService =  asyncThreadPoolService;
								queueOfWriteBatches = std::make_shared<tbb::concurrent_queue<std::shared_ptr<CassandraManagedType> > > ();
								queueOfTraceValues =  std::make_shared<tbb::concurrent_queue<std::shared_ptr<CassandraManagedType> > > ();

								numberOfJobsInOneTask = 200; //by default its 200
								mapOfTypesToCassandraServices = std::make_shared<std::unordered_map<std::string, CassandraServiceInterface*> >();
								assertAndThrow(dequeueAsyncTasksIntervalInSeconds > 0);
								this->dequeueAsyncTasksIntervalInSeconds = dequeueAsyncTasksIntervalInSeconds;
}

CassandraServiceQueueManager::~CassandraServiceQueueManager() {
								while(dequeueThreadIsRunning->getValue() ==  true)  {
																//try stopping the thread before exiting
																stopConsumingThread->setValue(true);
																gicapods::Util::sleepViaBoost(_L_, 2);//until consumer thread goes out
								};
}

void CassandraServiceQueueManager::dequeueTheAsyncTasks() {

								while(stopConsumingThread->getValue() == false) {
																dequeueThreadIsRunning->setValue(true);
																try {
																								submitTaskToAsyncService();

																								// keep this log line for debugging
																								// LOG_EVERY_N(ERROR, 100) << " queueOfWriteBatches "<<
																								//         cassandraServiceTypeTraits->getValueType() <<
																								//         "size : " <<queueOfWriteBatches->unsafe_size();
																} catch(...) {
																								gicapods::Util::showStackTrace();
																}

																//every n seconds we dequeue the async tasks that were pushed to the queue
																gicapods::Util::sleepViaBoost(_L_, dequeueAsyncTasksIntervalInSeconds);
								}

								dequeueThreadIsRunning->setValue(false);

}


void CassandraServiceQueueManager::pushToWriteBatchQueue(std::shared_ptr<CassandraManagedType> value) {
								queueOfWriteBatches->push(value);
								//we check if queue is too big, we dequeue some items here
								if (queueOfWriteBatches->unsafe_size() > numberOfJobsInOneTask) {
																submitTaskToAsyncService();
								}
}



void CassandraServiceQueueManager::submitTaskToAsyncService() {

								if(queueOfWriteBatches->empty()) {
																//nothing to submit task for
																return;
								}

								auto valueBatchUpdateTask =
																std::make_shared<ValueBatchUpdateTask> (
																								entityToModuleStateStats
																								);

								while(!queueOfWriteBatches->empty()) {
																std::shared_ptr<CassandraManagedType> value;
																if (queueOfWriteBatches->try_pop(value)) {
																								assertAndThrow(!StringUtil::equalsIgnoreCase(value->getValueType(), "CassandraManagedType"));
																								LOG_EVERY_N(ERROR, 100000) <<google::COUNTER<< "th pushing value type : " << value->getValueType();
																								valueBatchUpdateTask->pushValueInQueue(value);
																}
																LOG_EVERY_N(ERROR, 100000)<< google::COUNTER<<"th queueOfWriteBatches->unsafe_size() : "<< queueOfWriteBatches->unsafe_size();
								}

								valueBatchUpdateTask->mapOfTypesToCassandraServices = mapOfTypesToCassandraServices;
								valueBatchUpdateTask->entityToModuleStateStats = entityToModuleStateStats;
								asyncThreadPoolService->submitTask(valueBatchUpdateTask);

}
