
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"
#include "CassandraServiceTypeTraits.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "NumberUtil.h"
#include "CassandraReadService.h"
#include "AsyncThreadPoolService.h"

#include "Poco/Runnable.h"

#include "CassandraDriver.h"
#include "EntityToModuleStateStats.h"
#include "StatisticsUtil.h"
#include "RandomUtil.h"
#include "ValueBatchUpdateTask.h"
#include "ValueTraceTask.h"
#include "AtomicLong.h"
#include "ConfigService.h"
#include "CassandraServiceQueueManager.h"
#include "HttpUtil.h"
#include <thread>


template <class V>
CassandraReadService<V>::CassandraReadService(
								CassandraDriverInterface* cassandraDriver,
								EntityToModuleStateStats* entityToModuleStateStats,
								AsyncThreadPoolService* asyncThreadPoolService,
								gicapods::ConfigService* configService)  : Object(__FILE__) {

								this->cassandraDriver = cassandraDriver;
								this->entityToModuleStateStats =  entityToModuleStateStats;

								this->asyncThreadPoolService =  asyncThreadPoolService;
								this->configService =  configService;

								cassandraServiceTypeTraits = std::make_unique<CassandraServiceTypeTraits<V> > (configService);
								cassandraServiceTypeTraits->loadProperties();

								tracingDecisionRandomizer = std::make_shared<StatisticsUtil> ();

								numberOfReadFailures = std::make_shared<gicapods::AtomicLong>();
								numberOfTotalReads = std::make_shared<gicapods::AtomicLong>();

}

template <class V>
CassandraReadService<V>::~CassandraReadService() {

}

template <class V>
std::string CassandraReadService<V>::getName() {
								return "CassandraReadService";
}

template <class V>
void CassandraReadService<V>::recordFailure() {
								numberOfReadFailures->increment();
								double percentageOfReadFailures =
																100 * numberOfReadFailures->getValue() /
																numberOfTotalReads->getValue();
								if (percentageOfReadFailures > 5) {
																entityToModuleStateStats->addStateModuleForEntity(
																								"READING_DATA_FAILED" + cassandraServiceTypeTraits->getValueType(),
																								getName(),
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::exception);
								}

								if (percentageOfReadFailures > 1) {
																entityToModuleStateStats->addStateModuleForEntityWithValue(
																								"PERCENTAGE_READ_FAILURE" +cassandraServiceTypeTraits->getValueType(),
																								NumberUtil::getNearestMultipleToNumber(percentageOfReadFailures, 10),
																								getName(),
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::exception);
								}

}
template <class V>
std::shared_ptr<V> CassandraReadService<V>::readDataOptionalFromDb(std::shared_ptr<V> value, int timeoutInMillis) {
								numberOfTotalReads->increment();
								NULL_CHECK(value);

								std::string queryStr = cassandraServiceTypeTraits->getReadOneQuery(value.get());
								LOG_EVERY_N(INFO, 10000)<< "sample query for reading data : "<<queryStr;
								const char *query = queryStr.c_str();
								std::shared_ptr<V> object = nullptr;

								CassStatement *statement = cass_statement_new (query, 0);
								CassError rc = CASS_OK;
								CassFuture *future = cass_session_execute (cassandraDriver->getSession (), statement);
								cass_future_wait_timed (future, timeoutInMillis);
								rc = cass_future_error_code (future);

								if (rc != CASS_OK) {
																recordFailure();
								} else {
																const CassResult *result = cass_future_get_result (future);

																CassIterator *rowIterator = cass_iterator_from_result (result);
																bool rowOfDataFound = false;
																while (cass_iterator_next (rowIterator)) {
																								cassandraServiceTypeTraits->extractRowData(rowIterator, value.get(), object);
																								if (rowOfDataFound == false) {
																																rowOfDataFound = true;
																								}

																}
																if (rowOfDataFound) {
																								NULL_CHECK(object);
																								cassandraServiceTypeTraits->afterDataExtraction(object.get());
																} else {
																								//we didn't find any value for the key
																								entityToModuleStateStats->addStateModuleForEntity(
																																"NOT_FOUND_DATA" + cassandraServiceTypeTraits->getValueType(),
																																getName(),
																																EntityToModuleStateStats::all,
																																EntityToModuleStateStats::warning);
																}
																cass_iterator_free (rowIterator);
																cass_result_free (result);
								}

								cass_statement_free (statement);
								cass_future_free (future);

								if (object != nullptr) {
																if (cassandraServiceTypeTraits->isEligibleForPruning(object.get())) {
																								queueForPruning(object);
																}
								}
								return object;
}


template <class V>
void CassandraReadService<V>::queueForPruning(std::shared_ptr<V> value) {
								entityToModuleStateStats->addStateModuleForEntity(
																"ELLIGBILE_FOR_PRUNING" + cassandraServiceTypeTraits->getValueType(),
																getName(),
																EntityToModuleStateStats::all,
																EntityToModuleStateStats::warning);
}
template <class V>
std::shared_ptr<std::vector<std::shared_ptr<V> > > CassandraReadService<V>::readWithPaging(
								int pageSize,
								int limit) {

								auto allValues = std::make_shared<std::vector<std::shared_ptr<V> > >();
								cass_bool_t has_more_pages = cass_false;
								const CassResult* result = NULL;
								std::string queryStr =
																"SELECT * FROM " +
																cassandraServiceTypeTraits->getTableName()
																+ " limit " + _toStr(limit)
								;
								const char* query =  queryStr.c_str();
								LOG(ERROR) << "queryStr : "<<queryStr;
								CassStatement* statement = cass_statement_new(query, 0);

								cass_statement_set_paging_size(statement, pageSize);

								do {
																CassIterator* iterator;

																auto originalTimeout = cassandraDriver->getRequestTimeout();
																cassandraDriver->setRequestTimeout(1000000);
																CassFuture* future = cass_session_execute(cassandraDriver->getSession (), statement);
																cassandraDriver->setRequestTimeout(originalTimeout);

																if (cass_future_error_code(future) != 0) {
																								print_error(future, queryStr, entityToModuleStateStats);
																								break;
																}

																result = cass_future_get_result(future);
																iterator = cass_iterator_from_result(result);
																cass_future_free(future);

																while (cass_iterator_next(iterator)) {
																								std::shared_ptr<V> valueToReturn;
																								cassandraServiceTypeTraits->extractRowData(iterator, nullptr, valueToReturn);
																								if(!valueToReturn) {
																																return nullptr;
																								}
																								allValues->push_back(valueToReturn);

																}

																has_more_pages = cass_result_has_more_pages(result);

																if (has_more_pages) {
																								cass_statement_set_paging_state(statement, result);
																}

																cass_iterator_free(iterator);
																cass_result_free(result);

								} while (has_more_pages);
								cass_statement_free(statement);

								return allValues;
}


#include "FeatureDeviceHistory.h"
#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"
#include "PixelDeviceHistory.h"
#include "DeviceSegmentHistory.h"
#include "EventLog.h"
#include "Wiretap.h"
#include "BidEventLog.h"
#include "AdHistory.h"
#include "FeatureToFeatureHistory.h"
#include "ModelScore.h"
#include "SegmentDevices.h"
template class CassandraReadService<SegmentDevices>;
template class CassandraReadService<FeatureToFeatureHistory>;
template class CassandraReadService<AdHistory>;
template class CassandraReadService<ModelScore>;
template class CassandraReadService<EventLog>;
template class CassandraReadService<BidEventLog>;
template class CassandraReadService<GicapodsIdToExchangeIdsMap>;
template class CassandraReadService<FeatureDeviceHistory>;
template class CassandraReadService<DeviceFeatureHistory>;
template class CassandraReadService<IpToDeviceIdsMap>;
template class CassandraReadService<DeviceIdToIpsMap>;
template class CassandraReadService<PixelDeviceHistory>;

template class CassandraReadService<DeviceSegmentHistory>;

template class CassandraReadService<Wiretap>;
