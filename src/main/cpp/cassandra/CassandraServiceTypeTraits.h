/*
 * AeroCacheService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef CassandraServiceTypeTraits_H_
#define CassandraServiceTypeTraits_H_

#include "GUtil.h"
#include "ConverterUtil.h"
#include "FeatureHistory.h"
#include "EventLogCassandraTraits.h"
#include "StringUtil.h"
#include "Device.h"
#include "CassandraDriverInterface.h"
#include "Feature.h"
#include "DateTimeUtil.h"
#include "ConfigService.h"
#include "DeviceSegmentPair.h"

#include "SegmentDevices.h"
#include "BidEventLog.h"
#include "EventLog.h"
#include "ModelScore.h"

static constexpr const char* NOT_EQUAL_AT_ALL = "NOT_EQUAL_AT_ALL";
static constexpr const char* COMPLETELY_EQUAL = "COMPLETELY_EQUAL";
static constexpr const char* PARTIAL_EQUAL = "PARTIAL_EQUAL";

class GeneralCassandraTypeTraits {
public:
gicapods::ConfigService* configService;
double tracingPercentageRate;
GeneralCassandraTypeTraits(gicapods::ConfigService* configService) {
        tracingPercentageRate = 1;
        NULL_CHECK(configService);
          this->configService = configService;

}
virtual std::string getValueType()=0;
void loadProperties() {
    std::string tracingPropertyName = "tracingPercentageRate" + getValueType();
    this->tracingPercentageRate = configService->getAsDouble(tracingPropertyName);
    MLOG(3) << "tracingPropertyName : " << tracingPropertyName << "tracingPercentageRate :  "<< tracingPercentageRate;
}

virtual ~GeneralCassandraTypeTraits() { }
};

template <class T>
class CassandraServiceTypeTraits : public GeneralCassandraTypeTraits
{

public:
CassandraServiceTypeTraits<T>(gicapods::ConfigService* configService = nullptr) :
        GeneralCassandraTypeTraits(configService) {

}

static bool isEligibleForPruning(T* value) {
        EXIT("not implemented");
}

void afterDataExtraction(T* value) {
        EXIT("not implemented");
}


static const char * equals(T* value, T* value1);
static bool isValueValidForBatchWrite(T* value);

static void bindValuesToCassStatementForBatchWrite(T* value, CassStatement* statement);

static std::string getReadOneQuery(T* value);

double getTracingPercentageRate() {
        EXIT("not implemented");
}
static void extractRowData(
        CassIterator *rowIterator,
        T* requestObject,
        std::shared_ptr<T>& responseObject);

static std::string  getBatchWriteQuery();

static std::string  getTableName();

};


 #include "DeviceSegmentHistory.h"

 #include "FeatureDeviceHistory.h"
 #include "DeviceHistory.h"
 #include "Feature.h"
 #include "DeviceFeatureHistory.h"
 #include "IpToDeviceIdsMap.h"
 #include "DeviceIdToIpsMap.h"
 #include "PixelDeviceHistory.h"
 #include "GicapodsIdToExchangeIdsMap.h"
 #include "AdHistory.h"
 #include "DateTimeUtil.h"
 #include "CollectionUtil.h"
 #include "DeviceHistory.h"
 #include "PixelDeviceHistory.h"
 #include "Wiretap.h"
 #include "DateTimeMacro.h"

template <>
class CassandraServiceTypeTraits <DeviceSegmentHistory>: public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<DeviceSegmentHistory>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}

void afterDataExtraction(DeviceSegmentHistory* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "DeviceSegmentHistory";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 90; //90 days
}

static bool isEligibleForPruning(DeviceSegmentHistory* value) {
        if (value->deviceSegmentAssociations->size() > 100) {
                return true;
        }

        return false;
}
static const char * equals(DeviceSegmentHistory* deviceSegment,
                           DeviceSegmentHistory* deviceSegment1) {

        std::set<std::string> thisSegmentIds;
        for (auto deviceSegmentPair : *deviceSegment1->deviceSegmentAssociations) {
                assertAndThrow(deviceSegmentPair->segment->id > 0);
                thisSegmentIds.insert(_toStr(deviceSegmentPair->segment->id));
        }
        std::set<std::string> thatSegmentIds;
        for (auto deviceSegmentPair : *deviceSegment->deviceSegmentAssociations) {
                assertAndThrow(deviceSegmentPair->segment->id > 0);
                thatSegmentIds.insert(_toStr(deviceSegmentPair->segment->id));
        }

        if (CollectionUtil::getCommonItems(thisSegmentIds, thatSegmentIds).empty()) {
                return NOT_EQUAL_AT_ALL;
        }
        return PARTIAL_EQUAL;

}

static bool isValueValidForBatchWrite(DeviceSegmentHistory* deviceSegment) {
        return deviceSegment->deviceSegmentAssociations->size() > 0;
}

static void bindValuesToCassStatementForBatchWrite(
        DeviceSegmentHistory* deviceSegment,
        CassStatement* statement) {
          NULL_CHECK(deviceSegment);
          NULL_CHECK(statement);

        for (auto association : *deviceSegment->deviceSegmentAssociations) {
                cass_statement_bind_string_by_name(statement, "deviceid", deviceSegment->device->getDeviceId().c_str());
                cass_statement_bind_string_by_name(statement, "devicetype", deviceSegment->device->getDeviceType().c_str());
                if(association->segment->id <= 0) {
                  LOG(ERROR)<< "association has unregistered segments : "<<association->toJson();
                }
                assertAndThrow(association->segment->id > 0);
                cass_statement_bind_int64_by_name(statement, "segmentId", association->segment->id);
                cass_statement_bind_int64_by_name(statement, "dateCreated", DateTimeUtil::getNowDayInMilliSecond());
        }

}

static void extractRowData(
        CassIterator *rowIterator,
        DeviceSegmentHistory* requestObject,
        std::shared_ptr<DeviceSegmentHistory>& responseObject) {

        if (responseObject == nullptr) {
                responseObject = std::make_shared<DeviceSegmentHistory>(requestObject->device);
        }

        TimeType timeOfVisit =
                CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "dateCreated");

        int segmentId =
                CassandraDriverInterface::getIntegerValueFromRowByName (rowIterator, "segmentId");


        if (segmentId <= 0) {
          //we have bad data in our system. we entered some AT segments
          //without having a segment in mysql
          //we ignore these bad segments for now
          return;
        }
        auto segment =  std::make_shared<Segment>(timeOfVisit);

        segment->id = segmentId;
        auto deviceSegmentPair = std::make_shared<DeviceSegmentPair>(
                requestObject->device,
                segment
                );

        responseObject->deviceSegmentAssociations->push_back(deviceSegmentPair);
}

static std::string getReadOneQuery(DeviceSegmentHistory* value) {
        std::string query =
                " select segmentId, dateCreated  from gicapods.deviceSegments "
                " where deviceid =  '" + value->device->getDeviceId()+ "'  and "
                " devicetype =  '" + value->device->getDeviceType()+ "' ";
        return query;
}

static std::string getTableName() {
    return "gicapods.deviceSegments";
}
static std::string getBatchWriteQuery() {
        static std::string query =
                " insert into gicapods.deviceSegments (deviceId, deviceType, segmentId, dateCreated )"
                " values (?, ?, ?, ?) using TTL " + _toStr(getTTLInSeconds());

        return query;
}


};


template <>
class CassandraServiceTypeTraits <GicapodsIdToExchangeIdsMap> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<GicapodsIdToExchangeIdsMap>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}

void afterDataExtraction(GicapodsIdToExchangeIdsMap* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "GicapodsIdToExchangeIdsMap";
}
static int getTTLInSeconds()
{
        return 3600 * 24 * 90; //90 days
}

static bool isEligibleForPruning(GicapodsIdToExchangeIdsMap* value) {
        if(value->exchangeNameToExchangeId.size() > 10) {
                return true;
        }
        return false;
}


static const char * equals(GicapodsIdToExchangeIdsMap* gicapodsIdToExchangeIdsMap1,
                           GicapodsIdToExchangeIdsMap* gicapodsIdToExchangeIdsMap2) {
        //TODO : make this better
        if (StringUtil::equalsIgnoreCase(
                    gicapodsIdToExchangeIdsMap1->nomadiniDeviceId, gicapodsIdToExchangeIdsMap2->nomadiniDeviceId) &&
            gicapodsIdToExchangeIdsMap1->exchangeNameToExchangeId.size() == gicapodsIdToExchangeIdsMap2->exchangeNameToExchangeId.size()) {
                return PARTIAL_EQUAL;
        }

        return NOT_EQUAL_AT_ALL;

}

static bool isValueValidForBatchWrite(GicapodsIdToExchangeIdsMap* gicapodsIdToExchangeIdsMap) {
        if(gicapodsIdToExchangeIdsMap->exchangeNameToExchangeId.size() == 0) {
                return false;
        }
        return true;
}

static void bindValuesToCassStatementForBatchWrite(
        GicapodsIdToExchangeIdsMap* gicapodsIdToExchangeIdsMap,
        CassStatement* statement) {
          NULL_CHECK(gicapodsIdToExchangeIdsMap);
          NULL_CHECK(statement);
        //for reference
        // cass_statement_bind_string(statement, 0, cass_string_init(key));
        // cass_statement_bind_bool(statement, 1, basic->bln);
        // cass_statement_bind_float(statement, 2, basic->flt);
        // cass_statement_bind_double(statement, 3, basic->dbl);
        // cass_statement_bind_int32(statement, 4, basic->i32);
        // cass_statement_bind_int64(statement, 5, basic->i64);

        assertAndThrow(!gicapodsIdToExchangeIdsMap->exchangeNameToExchangeId.empty());
        assertAndThrow(!gicapodsIdToExchangeIdsMap->exchangeNameToBatchWrite.empty());
        for (auto pair : gicapodsIdToExchangeIdsMap->exchangeNameToExchangeId ) {
                auto exchangeName = pair.first;
                auto exchangeId = pair.second;
                if (!StringUtil::equalsIgnoreCase(gicapodsIdToExchangeIdsMap->exchangeNameToBatchWrite, exchangeName)) {
                        //value doesn't belong to this batch..skipping
                        continue;
                }
                assertAndThrow(!exchangeId.empty());
                assertAndThrow(!gicapodsIdToExchangeIdsMap->nomadiniDeviceId.empty());
                cass_statement_bind_string_by_name(statement, exchangeName.c_str(), exchangeId.c_str());
                cass_statement_bind_string_by_name(statement, "nomadiniDeviceId", gicapodsIdToExchangeIdsMap->nomadiniDeviceId.c_str());
                cass_statement_bind_int64_by_name(statement, "datecreated", DateTimeUtil::getNowInMilliSecond());
        }


}

static void extractRowData(
        CassIterator *rowIterator,
        GicapodsIdToExchangeIdsMap* requestObject,
        std::shared_ptr<GicapodsIdToExchangeIdsMap>& responseObject) {
        if (responseObject == nullptr) {
                responseObject = std::make_shared<GicapodsIdToExchangeIdsMap>();
        }
        responseObject->nomadiniDeviceId =
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "nomadiniDeviceId");



        std::string exchangeAId =
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "google");
        responseObject->exchangeNameToExchangeId.insert(std::make_pair("google", exchangeAId));

        std::string exchangeBId =
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "rubicon");
        responseObject->exchangeNameToExchangeId.insert(std::make_pair("rubicon", exchangeBId));
}

static std::string getTableName() {
    return "gicapods.nomadiniDeviceIdToExchangeIdsMap";
}

static std::string getBatchWriteQuery(std::string exchangeNameToBatchWrite) {
        return "";
}

static std::string getReadOneQuery(GicapodsIdToExchangeIdsMap* value) {
        std::string query =
                " select google, rubicon from gicapods.nomadiniDeviceIdToExchangeIdsMap "
                " where nomadiniDeviceId = '" + value->nomadiniDeviceId +"'";

        return query;
}

};



template <>
class CassandraServiceTypeTraits <FeatureDeviceHistory>  : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<FeatureDeviceHistory> (gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(FeatureDeviceHistory* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "FeatureDeviceHistory";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 90; //90 days
}

static bool isEligibleForPruning(FeatureDeviceHistory* value) {
        return false;
}


static const char * equals(FeatureDeviceHistory* featureDeviceHistory,
                           FeatureDeviceHistory* featureDeviceHistory1) {
        //TODO : make this better
        if (featureDeviceHistory1->feature->equals(featureDeviceHistory->feature.get()) &&
            featureDeviceHistory1->devicehistories.size() == featureDeviceHistory->devicehistories.size()) {
                return PARTIAL_EQUAL;
        }

        return NOT_EQUAL_AT_ALL;

}

static bool isValueValidForBatchWrite(FeatureDeviceHistory* featureDeviceHistory) {
        if(featureDeviceHistory->devicehistories.size() == 0) {
                return false;
        }
        return true;
}
static void bindValuesToCassStatementForBatchWrite(
        FeatureDeviceHistory* featureDeviceHistory,
        CassStatement* statement) {
          NULL_CHECK(featureDeviceHistory);
          NULL_CHECK(statement);
        //for reference
        // cass_statement_bind_string(statement, 0, cass_string_init(key));
        // cass_statement_bind_bool(statement, 1, basic->bln);
        // cass_statement_bind_float(statement, 2, basic->flt);
        // cass_statement_bind_double(statement, 3, basic->dbl);
        // cass_statement_bind_int32(statement, 4, basic->i32);
        // cass_statement_bind_int64(statement, 5, basic->i64);

        assertAndThrow(!featureDeviceHistory->devicehistories.empty());
        for (auto history : featureDeviceHistory->devicehistories ) {
                cass_statement_bind_string_by_name(statement, "feature", StringUtil::toStr(featureDeviceHistory->feature->getName()).c_str());
                cass_statement_bind_string_by_name(statement, "featureType", StringUtil::toStr(featureDeviceHistory->feature->getType()).c_str());
                cass_statement_bind_string_by_name(statement, "deviceid", StringUtil::toStr(history->device->getDeviceId()).c_str());
                cass_statement_bind_string_by_name(statement, "devicetype", StringUtil::toStr(history->device->getDeviceType()).c_str());
                cass_statement_bind_int64_by_name(statement, "timeofvisit", DateTimeUtil::getNowInMilliSecond());
        }

}

static std::string getReadOneQuery(FeatureDeviceHistory* value) {
        std::string query =
                " select deviceid, devicetype, featureType, timeofvisit from gicapods_mdl.featuredevicehistory "
                " where feature = '" + value->feature->getName()+ "'";

        return query;
}

static std::string getBatchWriteQuery() {
        static std::string query =
                " insert into gicapods_mdl.featuredevicehistory "
                " (feature, featureType, deviceid, devicetype, timeofvisit ) "
                " values (?, ?, ?, ?, ? ) using TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "gicapods_mdl.featuredevicehistory";
}
static void extractRowData(
        CassIterator *rowIterator,
        FeatureDeviceHistory* requestObject,
        std::shared_ptr<FeatureDeviceHistory>& responseObject) {

        if (responseObject == nullptr) {
                responseObject = std::make_shared<FeatureDeviceHistory>(
                        std::make_unique<Feature>(
                                //we might not know the type when we are building the requestObject, so we get it from db
                                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "featureType"),
                                requestObject->feature->getName(),
                                requestObject->feature->getLevel()
                                ));
        }

        auto device = std::make_shared<Device>(
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceid"),
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "devicetype")
                );

        auto deviceHistory = std::make_shared<DeviceHistory>(device);
        deviceHistory->timeOfVisit = CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "timeofvisit");

        responseObject->devicehistories.push_back(deviceHistory);
}


};



template <>
class CassandraServiceTypeTraits <DeviceFeatureHistory> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<DeviceFeatureHistory>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(DeviceFeatureHistory* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "DeviceFeatureHistory";
}
static int getTTLInSeconds()
{
        return 3600 * 24 * 90; //90 days
}

static bool isEligibleForPruning(DeviceFeatureHistory* value) {
        if (value->getFeatures()->size() > 10000) {
                return true;
        }
        return false;
}


static const char* equals(DeviceFeatureHistory* deviceFeatureHistory,
                          DeviceFeatureHistory* deviceFeatureHistory1) {

        if (StringUtil::equalsIgnoreCase(deviceFeatureHistory1->device->getDeviceId(),
                                         deviceFeatureHistory->device->getDeviceId()) &&
            StringUtil::equalsIgnoreCase(deviceFeatureHistory1->device->getDeviceType(),
                                         deviceFeatureHistory->device->getDeviceType())
            && deviceFeatureHistory1->getFeatures()->size() == deviceFeatureHistory->getFeatures()->size()) {
                return PARTIAL_EQUAL;
        }

        return NOT_EQUAL_AT_ALL;
}

static bool isValueValidForBatchWrite(DeviceFeatureHistory* deviceFeatureHistory) {
        if(deviceFeatureHistory->getFeatures()->size() == 0) {
                return false;
        }
        return true;
}

static void
bindValuesToCassStatementForBatchWrite(DeviceFeatureHistory* deviceFeatureHistory,
                                       CassStatement* statement) {
                                         NULL_CHECK(deviceFeatureHistory);
                                         NULL_CHECK(statement);
        if(deviceFeatureHistory->getFeatures()->size() == 0) {
                EXIT("features is empty ");
        }

        for (auto feature : *deviceFeatureHistory->getFeatures()) {

                cass_statement_bind_string_by_name(statement, "deviceid", deviceFeatureHistory->device->getDeviceId().c_str());
                cass_statement_bind_string_by_name(statement, "deviceType", deviceFeatureHistory->device->getDeviceType().c_str());
                cass_statement_bind_int64_by_name(statement, "timeofvisit", feature->featureSeenInMillis);
                cass_statement_bind_string_by_name(statement, "feature", feature->feature->getName().c_str());
                cass_statement_bind_string_by_name(statement, "featureType", feature->feature->getType().c_str());
        }

}

static std::string getBatchWriteQuery() {
        static std::string query =
                " insert into gicapods_mdl.devicehistory (deviceid, deviceType, timeofvisit, feature, featureType )"
                " values (?, ?, ?, ?, ?) using TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "gicapods_mdl.devicehistory";
}
static std::string getReadOneQuery(DeviceFeatureHistory* value) {
        std::string query = "";
        if (value->featureRecencyInSecondToPickUp > 0 ) {
                //we only pick up features that are newer than the recency given to us
                auto dateToPickupInSecond = DateTimeUtil::getNowInSecond() - value->featureRecencyInSecondToPickUp;
                query =" select  timeofvisit, feature, featureType, deviceType from gicapods_mdl.devicehistory "
                        " where deviceid =  '" + value->device->getDeviceId()+"' and timeofvisit > "
                        + StringUtil::toStr(dateToPickupInSecond * 1000) + " ALLOW FILTERING";
        } else {
            query =
                " select timeofvisit, feature, featureType, deviceType from gicapods_mdl.devicehistory "
                " where deviceid =  '" + value->device->getDeviceId()+"'";
              }
        return query;
}

static void extractRowData(
        CassIterator *rowIterator,
        DeviceFeatureHistory* requestObject,
        std::shared_ptr<DeviceFeatureHistory>& responseObject) {
        if (responseObject == nullptr) {
                responseObject = std::make_shared<DeviceFeatureHistory>(requestObject->device);
        }

        TimeType timeOfVisit =
                CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "timeofvisit");

        std::string featureName =
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "feature");
        std::string featureType = CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "featureType");
        try {
        auto feature = std::make_shared<Feature>(featureType, featureName, 0);
        auto featureHistory = std::make_shared<FeatureHistory>(feature, timeOfVisit);

        responseObject->getFeatures()->push_back(featureHistory);
      } catch(...) {
        //some times for some bugs , we have features that are entered with wrong types
        LOG_EVERY_N(WARNING, 1000)<<google::COUNTER<<" ignoring mistyped feature ";
      }
}

};


template <>
class CassandraServiceTypeTraits <IpToDeviceIdsMap> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<IpToDeviceIdsMap> (gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(IpToDeviceIdsMap* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "IpToDeviceIdsMap";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 7; //7 days
}


static bool isEligibleForPruning(IpToDeviceIdsMap* value) {
        if (value->devices.size() > 100) {
                return true;
        }
        return false;
}

static const char* equals(IpToDeviceIdsMap* ipToDeviceIdsMap,
                          IpToDeviceIdsMap* ipToDeviceIdsMap1) {
        std::set<std::string> thisDeviceIds1;

        for (auto&& device : ipToDeviceIdsMap1->devices) {
                thisDeviceIds1.insert(device->getDeviceId());
        }

        std::set<std::string> thatDeviceIds;
        for (auto&& device : ipToDeviceIdsMap->devices) {
                thatDeviceIds.insert(device->getDeviceId());
        }

        if (CollectionUtil::getCommonItems(thisDeviceIds1, thatDeviceIds).empty()) {
                return NOT_EQUAL_AT_ALL;
        }
        return PARTIAL_EQUAL;

}
static bool isValueValidForBatchWrite(IpToDeviceIdsMap* ipToDeviceIdsMap) {
        if(ipToDeviceIdsMap->devices.size() == 0) {
                return false;
        }
        return true;
}


static void bindValuesToCassStatementForBatchWrite(IpToDeviceIdsMap* ipToDeviceIdsMap,
                                                   CassStatement* statement) {
                                                     NULL_CHECK(ipToDeviceIdsMap);
                                                     NULL_CHECK(statement);
        if(ipToDeviceIdsMap->devices.size() == 0) {
                EXIT("devices is empty");
        }
        for (auto&& device : ipToDeviceIdsMap->devices ) {
                cass_statement_bind_string_by_name(statement, "ip", StringUtil::toStr(ipToDeviceIdsMap->ip).c_str());
                cass_statement_bind_int64_by_name(statement, "timeofvisit", DateTimeUtil::getNowInMilliSecond());
                cass_statement_bind_string_by_name(statement, "deviceId", StringUtil::toStr(device->getDeviceId()).c_str());
                cass_statement_bind_string_by_name(statement, "deviceType", StringUtil::toStr(device->getDeviceType()).c_str());
        }
}

static std::string getBatchWriteQuery() {
        static std::string query =
                " insert into gicapods_mdl.IpToDeviceIdsMap (ip, timeofvisit, deviceId , deviceType) "
                " values (?, ?, ?, ?) using TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "gicapods_mdl.IpToDeviceIdsMap";
}
static std::string getReadOneQuery(IpToDeviceIdsMap* value) {
        std::string query =
                " select ip, timeofvisit, deviceId , deviceType from gicapods_mdl.IpToDeviceIdsMap "
                " where ip = '" + value->ip +"'";

        return query;
}

static void extractRowData(
        CassIterator *rowIterator,
        IpToDeviceIdsMap* requestObject,
        std::shared_ptr<IpToDeviceIdsMap>& responseObject) {
        if (responseObject == nullptr) {
                responseObject = std::make_shared<IpToDeviceIdsMap>();
        }

        if (responseObject->ip.empty()) {
                responseObject->ip = requestObject->ip;
        }
        auto device = std::make_shared<Device>(
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceid"),
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "devicetype")
                );
        responseObject->devices.push_back(device);
}


};



template <>
class CassandraServiceTypeTraits <DeviceIdToIpsMap> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<DeviceIdToIpsMap>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(DeviceIdToIpsMap* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "DeviceIdToIpsMap";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 7; //7 days
}

static bool isEligibleForPruning(DeviceIdToIpsMap* value) {
        if (value->ips.size() > 100) {
                return true;
        }

        return false;
}


static const char* equals(DeviceIdToIpsMap* deviceIdToIpsMap,
                          DeviceIdToIpsMap* deviceIdToIpsMap1) {
        if (CollectionUtil::getCommonItems(deviceIdToIpsMap1->ips, deviceIdToIpsMap->ips).empty()) {
                return NOT_EQUAL_AT_ALL;
        }
        return PARTIAL_EQUAL;
}
static bool isValueValidForBatchWrite(DeviceIdToIpsMap* deviceIdToIpsMap) {
        if(deviceIdToIpsMap->ips.size() == 0) {
                return false;
        }
        return true;
}

static void bindValuesToCassStatementForBatchWrite(
        DeviceIdToIpsMap* deviceIdToIpsMap,
        CassStatement* statement) {
          NULL_CHECK(deviceIdToIpsMap);
          NULL_CHECK(statement);
        for (auto ip : deviceIdToIpsMap->ips ) {
                cass_statement_bind_string_by_name(statement, "deviceId", StringUtil::toStr(deviceIdToIpsMap->device->getDeviceId()).c_str());
                cass_statement_bind_int64_by_name(statement, "timeofvisit", DateTimeUtil::getNowInMilliSecond());
                cass_statement_bind_string_by_name(statement, "ip", StringUtil::toStr(ip).c_str());
        }
}

static std::string getBatchWriteQuery() {
        static std::string query =
                " insert into gicapods_mdl.DeviceIdToIpsMap (deviceId, timeofvisit, ip ) "
                " values (?, ?, ?) using TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "gicapods_mdl.DeviceIdToIpsMap ";
}
static std::string getReadOneQuery(DeviceIdToIpsMap* value) {
        std::string query =
                " select ip "
                " from gicapods_mdl.DeviceIdToIpsMap where deviceId = '"
                + value->device->getDeviceId() + "' ";

        return query;
}

static void extractRowData(
        CassIterator *rowIterator,
        DeviceIdToIpsMap* requestObject,
        std::shared_ptr<DeviceIdToIpsMap>& responseObject) {
        if (responseObject == nullptr) {
                responseObject = std::make_shared<DeviceIdToIpsMap>();
        }

        if (responseObject->device == nullptr) {
                responseObject->device = requestObject->device;
        }
        responseObject->ips.insert(CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "ip"));
}

};

template <>
class CassandraServiceTypeTraits <PixelDeviceHistory> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<PixelDeviceHistory>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(PixelDeviceHistory* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "PixelDeviceHistory";
}


static int getTTLInSeconds()
{
        return 3600 * 24 * 90; //90 days
}

static bool isEligibleForPruning(PixelDeviceHistory* value) {
        if (value->devicesHittingPixel->size() > 100) {
                return true;
        }
        return false;
}


static const char * equals(PixelDeviceHistory* pixelDeviceHistory,
                           PixelDeviceHistory* pixelDeviceHistory1) {
        std::set<std::string> thisDeviceIds;
        for (auto&& devicePair : *pixelDeviceHistory1->devicesHittingPixel) {
                thisDeviceIds.insert(devicePair.second->getDeviceId());
        }
        std::set<std::string> thatDeviceIds;
        for (auto&& devicePair : *pixelDeviceHistory->devicesHittingPixel) {
                thatDeviceIds.insert(devicePair.second->getDeviceId());
        }

        if (CollectionUtil::getCommonItems(thisDeviceIds, thatDeviceIds).empty()) {
                return NOT_EQUAL_AT_ALL;
        }
        return PARTIAL_EQUAL;
}
static bool isValueValidForBatchWrite(PixelDeviceHistory* pixelDeviceHistory) {

        if(pixelDeviceHistory->devicesHittingPixel->empty()) {
                return false;
        }
        return true;
}
static void bindValuesToCassStatementForBatchWrite(PixelDeviceHistory* pixelDeviceHistory,
                                                   CassStatement* statement) {
                                                     NULL_CHECK(pixelDeviceHistory);
                                                     NULL_CHECK(statement);
        for (auto&& pair : *pixelDeviceHistory->devicesHittingPixel ) {
                cass_statement_bind_string_by_name(statement, "pixelId", StringUtil::toStr(pixelDeviceHistory->pixelUniqueKey).c_str());
                cass_statement_bind_string_by_name(statement, "deviceid", StringUtil::toStr(pair.second->getDeviceId()).c_str());
                cass_statement_bind_int64_by_name(statement, "timeofvisit", DateTimeUtil::getNowInMilliSecond());
        }

}

static std::string getBatchWriteQuery() {
        static std::string query =
                " insert into gicapods_mdl.PixelDeviceHistory (pixelId, deviceid, timeofvisit ) "
                " values (?, ?, ? ) using TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "gicapods_mdl.PixelDeviceHistory ";
}
static std::string getReadOneQuery(PixelDeviceHistory* value) {
        NULL_CHECK(value);
        std::string query =
                " select deviceid, devicetype, timeofvisit from gicapods_mdl.PixelDeviceHistory "
                " where pixelId = '" + value->pixelUniqueKey + "'";

        return query;
}

static void extractRowData(
        CassIterator *rowIterator,
        PixelDeviceHistory* requestObject,
        std::shared_ptr<PixelDeviceHistory>& responseObject) {

        if (responseObject == nullptr) {
                responseObject = std::make_shared<PixelDeviceHistory>();
        }
        if (responseObject->pixelUniqueKey.empty()) {
                responseObject->pixelUniqueKey = requestObject->pixelUniqueKey;
        }

        auto device = std::make_shared<Device>(
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceid"),
                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "devicetype")
                );

        responseObject->devicesHittingPixel->insert(
                std::make_pair(
                  StringUtil::toStr(CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "timeofvisit")),
                               device));
}

};


//////////////////////////////////////////
/////////////////////////////////////////


template <>
class CassandraServiceTypeTraits <AdHistory> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<AdHistory>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(AdHistory* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "AdHistory";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 60; //60 days
}

static bool isEligibleForPruning(AdHistory* value) {
        return value->listOfAdEntries.size()> 100;
}


static const char * equals(AdHistory* adHistory, AdHistory* adHistory1) {
        if (adHistory->listOfAdEntries.size() ==
            adHistory1->listOfAdEntries.size()) {
                return PARTIAL_EQUAL;
        }
        return NOT_EQUAL_AT_ALL;
}

static bool isValueValidForBatchWrite(AdHistory* adHistory) {
        return !(adHistory->listOfAdEntries.empty());
}

static void bindValuesToCassStatementForBatchWrite(AdHistory* adHistory,
                                                   CassStatement* statement) {

                                                       NULL_CHECK(adHistory);
                                                       NULL_CHECK(statement);
        for (auto adEntry : adHistory->listOfAdEntries ) {
                cass_statement_bind_string_by_name(statement, "deviceid", adHistory->device->getDeviceId().c_str());
                cass_statement_bind_string_by_name(statement, "devicetype", adHistory->device->getDeviceType().c_str());
                cass_statement_bind_int64_by_name(statement, "eventTime", adEntry->timeAdShownInMillis);
                cass_statement_bind_string_by_name(statement, "adentry", adEntry->toJson().c_str());
        }
}

static void extractRowData(
        CassIterator *rowIterator,
        AdHistory* requestObject,
        std::shared_ptr<AdHistory>& responseObject) {

        if (responseObject == nullptr) {
                responseObject = std::make_shared<AdHistory>(requestObject->device);
        }

        std::string adhistoryInJson = CassandraDriverInterface::getStringValueFromRowAtColumn(rowIterator, 0);
        auto adEntry = AdEntry::fromJson(adhistoryInJson);
        responseObject->listOfAdEntries.push_back(adEntry);

}

static std::string getReadOneQuery(AdHistory* value) {

        std::string query =
                "select adEntry from  adhistory where deviceId = '__deviceId__' and deviceType='__deviceType__' ";
        query = StringUtil::replaceString(query, "__deviceId__",
                                          value->device->getDeviceId());
        query = StringUtil::replaceString(query, "__deviceType__",
                                          value->device->getDeviceType());

        return query;
}
static std::string getBatchWriteQuery() {
        static std::string query =
                "INSERT INTO adhistory (deviceId, deviceType, eventTime, adEntry) "
                " VALUES ( ?, ?, ?, ?) using TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "adhistory ";
}
};


template <>
class CassandraServiceTypeTraits <EventLog> : public GeneralCassandraTypeTraits {
public:

static std::string getAllFields() {
  return EventLogCassandraTraits::getAllFields();
}

CassandraServiceTypeTraits<EventLog>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}

void afterDataExtraction(EventLog* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "EventLog";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 60; //60 days
}

static bool isEligibleForPruning(EventLog* value) {
        // return value->listOfAdEntries.size()> 100;
        return false;
}


static const char * equals(EventLog* eventLog, EventLog* eventLog1) {
        // if (eventLog->listOfAdEntries.size() ==
        //     eventLog1->listOfAdEntries.size()) {
        //         return PARTIAL_EQUAL;
        // }
        return NOT_EQUAL_AT_ALL;
}

static bool isValueValidForBatchWrite(EventLog* eventLog) {
        return true;
}

static void bindValuesToCassStatementForBatchWrite(EventLog* event,
                                                   CassStatement* statement) {
        EventLogCassandraTraits::bindValuesToCassStatementForBatchWrite(event, statement);
}

static void extractRowData(
        CassIterator *rowIterator,
        EventLog* requestObject,
        std::shared_ptr<EventLog>& eventLog) {
          EventLogCassandraTraits::extractRowData(rowIterator, requestObject, eventLog);
}

static std::string getReadOneQuery(EventLog* value) {

                   std::string queryStr =
                           "select"
                           + getAllFields() +
                           " from gicapods.eventLog "
                           " where eventId='__EVENT_ID__'";
                           queryStr = StringUtil::replaceString (queryStr, StringUtil::toStr ("__EVENT_ID__"), value->eventId);

        return queryStr;
}
static std::string getBatchWriteQuery() {
        static std::string questionMarks =
        EventLogCassandraTraits::getQuestionMarks();

        static std::string query = " INSERT INTO gicapods.eventLog ("
                             + getAllFields() +
                            ") VALUES ( "
                              + questionMarks +
                            ") using TTL " + _toStr(getTTLInSeconds());

        auto marksSize = StringUtil::tokenizeString(questionMarks, ",").size();
        auto fieldsSize = StringUtil::tokenizeString(getAllFields(), ",").size();
        if(marksSize != fieldsSize){
            LOG_EVERY_N(ERROR, 1)<< "markSize is "<< marksSize << ", but fieldsSize is"<<fieldsSize;
              EXIT("");
          }

        return query;
}
static std::string getTableName() {
    return "gicapods.eventLog ";
}

};


template <>
class CassandraServiceTypeTraits <BidEventLog> : public GeneralCassandraTypeTraits {
public:

static std::string getAllFields() {
  return EventLogCassandraTraits::getAllFields();
}

CassandraServiceTypeTraits<BidEventLog>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}

void afterDataExtraction(BidEventLog* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "BidEventLog";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 60; //7 days
}

static bool isEligibleForPruning(BidEventLog* value) {
        // return value->listOfAdEntries.size()> 100;
        return false;
}


static const char * equals(BidEventLog* eventLog, BidEventLog* eventLog1) {
        // if (eventLog->listOfAdEntries.size() ==
        //     eventLog1->listOfAdEntries.size()) {
        //         return PARTIAL_EQUAL;
        // }
        return NOT_EQUAL_AT_ALL;
}

static bool isValueValidForBatchWrite(BidEventLog* eventLog) {
        return true;
}

static void bindValuesToCassStatementForBatchWrite(BidEventLog* event,
                                                   CassStatement* statement) {
        EventLogCassandraTraits::bindValuesToCassStatementForBatchWrite(event, statement);
}

static void extractRowData(
        CassIterator *rowIterator,
        BidEventLog* requestObject,
        std::shared_ptr<BidEventLog>& eventLog) {
          EventLogCassandraTraits::extractRowDataForBidEventLog(rowIterator, requestObject, eventLog);
}

static std::string getReadOneQuery(BidEventLog* value) {

                   std::string queryStr =
                           "select"
                           + getAllFields() +
                           " from gicapods.bidEventLog "
                           " where eventId='__EVENT_ID__'";
                           queryStr = StringUtil::replaceString (queryStr, StringUtil::toStr ("__EVENT_ID__"), value->eventId);

        return queryStr;
}
static std::string getBatchWriteQuery() {
    static std::string questionMarks =
    EventLogCassandraTraits::getQuestionMarks();

        static std::string query = " INSERT INTO gicapods.bidEventLog ("
                             + getAllFields() +
                            ") VALUES ( "
                              + questionMarks +
                            ") using TTL " + _toStr(getTTLInSeconds());

        auto marksSize = StringUtil::tokenizeString(questionMarks, ",").size();
        auto fieldsSize = StringUtil::tokenizeString(getAllFields(), ",").size();
        if(marksSize != fieldsSize){
            LOG_EVERY_N(ERROR, 1)<< "questionMarks Size is "<< marksSize << ", but fieldsSize is"<<fieldsSize;
              EXIT("");
          }

        return query;
}
static std::string getTableName() {
    return "gicapods.bidEventLog ";
}

};


//////////////////////////////////////////
/////////////////////////////////////////


template <>
class CassandraServiceTypeTraits <Wiretap> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<Wiretap>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(Wiretap* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "Wiretap";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 60; //7 days
}

static bool isEligibleForPruning(Wiretap* value) {
        return false;
}


static const char * equals(Wiretap* wiretap1, Wiretap* wiretap2) {
        if (StringUtil::equalsIgnoreCase(
            wiretap1->eventId,
            wiretap2->eventId)) {
                return PARTIAL_EQUAL;
        }
        return NOT_EQUAL_AT_ALL;
}

static bool isValueValidForBatchWrite(Wiretap* adHistory) {
        return true;
}

static void bindValuesToCassStatementForBatchWrite(Wiretap* wiretap,
                                                   CassStatement* statement) {

                                                       NULL_CHECK(wiretap);
                                                       NULL_CHECK(statement);

                cass_statement_bind_string_by_name(statement, "eventid", wiretap->eventId.c_str());
                cass_statement_bind_int64_by_name(statement, "eventtime", DateTimeUtil::getNowInMilliSecond());
                cass_statement_bind_string_by_name(statement, "appName", wiretap->appName.c_str());
                cass_statement_bind_string_by_name(statement, "appVersion", wiretap->appVersion.c_str());
                cass_statement_bind_string_by_name(statement, "appHost", wiretap->appHost.c_str());
                cass_statement_bind_string_by_name(statement, "moduleName", wiretap->moduleName.c_str());
                cass_statement_bind_string_by_name(statement, "request", wiretap->request.c_str());
                cass_statement_bind_string_by_name(statement, "response", wiretap->response.c_str());

}

static void extractRowData(
        CassIterator *rowIterator,
        Wiretap* requestObject,
        std::shared_ptr<Wiretap>& responseObject) {

        if (responseObject == nullptr) {
                responseObject = std::make_shared<Wiretap>();
        }

        responseObject->eventId =
        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "eventid");

        responseObject->appName =
        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "appName");

        responseObject->appVersion =
        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "appVersion");

        responseObject->appHost =
        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "appHost");

        responseObject->moduleName =
        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "moduleName");

        responseObject->request =
        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "request");

        responseObject->response =
        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "response");
}

static std::string getReadOneQuery(Wiretap* value) {

        std::string query =
                "select * from wiretap where eventid = '__eventid__'";
        query = StringUtil::replaceString(query, "__eventid__", value->eventId);
        return query;
}

static std::string getBatchWriteQuery() {

        static std::string query =
                "INSERT INTO wiretap (eventid, "
                  "eventtime, "
                  "appName,"
                  "appVersion,"
                  "appHost,"
                  "moduleName,"
                  "request,"
                  "response)"
                "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)  USING TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "wiretap";
}
};


//////////////////////////////////////////
/////////////////////////////////////////


template <>
class CassandraServiceTypeTraits <ModelScore> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<ModelScore>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(ModelScore* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "ModelScore";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 60; //7 days
}

static bool isEligibleForPruning(ModelScore* value) {
        return false;
}


static const char * equals(ModelScore* modelScore1, ModelScore* modelScore2) {
        if (modelScore1->modelId == modelScore2->modelId) {
                return PARTIAL_EQUAL;
        }
        return NOT_EQUAL_AT_ALL;
}

static bool isValueValidForBatchWrite(ModelScore* adHistory) {
        return true;
}

static void bindValuesToCassStatementForBatchWrite(ModelScore* modelScore,
                                                   CassStatement* statement) {

                                                       NULL_CHECK(modelScore);
                                                       NULL_CHECK(statement);

                cass_statement_bind_int32_by_name(statement, "modelId", modelScore->modelId);
                cass_statement_bind_int64_by_name(statement, "eventtime", DateTimeUtil::getNowInMilliSecond());
                cass_statement_bind_double_by_name(statement, "score", modelScore->score);


}

static void extractRowData(
        CassIterator *rowIterator,
        ModelScore* requestObject,
        std::shared_ptr<ModelScore>& responseObject) {

        if (responseObject == nullptr) {
                responseObject = std::make_shared<ModelScore>();
        }

        responseObject->modelId =
        CassandraDriverInterface::getIntegerValueFromRowByName (rowIterator, "modelId");

        responseObject->score =
        CassandraDriverInterface::getDoubleValueFromRowByName (rowIterator, "score");
}

static std::string getReadOneQuery(ModelScore* value) {

        std::string query =
                "select * from ModelScore where modelId = '__modelId__'";
        query = StringUtil::replaceString(query, "__modelId__", _toStr(value->modelId));
        return query;
}

static std::string getBatchWriteQuery() {

        static std::string query =
                "INSERT INTO ModelScore (modelId, "
                  "eventtime, "
                  "score)"
                "VALUES ( ?, ?, ?)  USING TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "ModelScore";
}
};


//////////////////////////////////////////
/////////////////////////////////////////
#include "FeatureToFeatureHistory.h"

template <>
class CassandraServiceTypeTraits <FeatureToFeatureHistory> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<FeatureToFeatureHistory>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(FeatureToFeatureHistory* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "FeatureToFeatureHistory";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 60; //7 days
}

static bool isEligibleForPruning(FeatureToFeatureHistory* value) {
        return false;
}


static const char * equals(FeatureToFeatureHistory* ffh1, FeatureToFeatureHistory* ffh2) {
        if (StringUtil::equalsIgnoreCase(ffh1->feature->getName(), ffh2->feature->getName())) {
                return PARTIAL_EQUAL;
        }
        return NOT_EQUAL_AT_ALL;
}

static bool isValueValidForBatchWrite(FeatureToFeatureHistory* ffh) {
        return true;
}

static void bindValuesToCassStatementForBatchWrite(FeatureToFeatureHistory* ffh,
                                                   CassStatement* statement) {

        NULL_CHECK(ffh);
        NULL_CHECK(statement);

        for (auto fh : ffh->featureHistories ) {
            cass_statement_bind_string_by_name(statement, "featurekey", ffh->feature->getName().c_str());
            cass_statement_bind_string_by_name(statement, "featurekeytype", ffh->feature->getType().c_str());
            cass_statement_bind_string_by_name(statement, "feature", fh->feature->getName().c_str());
            cass_statement_bind_string_by_name(statement, "featureType", fh->feature->getType().c_str());
            cass_statement_bind_int64_by_name(statement, "eventTime", fh->featureSeenInMillis);
          }
}

static void extractRowData(
        CassIterator *rowIterator,
        FeatureToFeatureHistory* requestObject,
        std::shared_ptr<FeatureToFeatureHistory>& responseObject) {

        if (responseObject == nullptr) {
                responseObject = std::make_shared<FeatureToFeatureHistory>(
                  std::move(requestObject->feature));
        }

        auto feature = std::make_shared<Feature>(
          CassandraDriverInterface::getStringValueFromRowByName(rowIterator, "featureType"),
          CassandraDriverInterface::getStringValueFromRowByName(rowIterator, "feature"),
          0);
        auto featureHistory = std::make_shared<FeatureHistory>(feature,
        CassandraDriverInterface::getLongValueFromRowByName(rowIterator, "eventtime"));

        responseObject->featureHistories.push_back(featureHistory);

}

static std::string getReadOneQuery(FeatureToFeatureHistory* value) {

        std::string query =
                " select feature, featureType, eventtime from "
                " gicapods_mdl.FeatureToFeatureHistory where "
                " featurekey = '__feature__' and featurekeyType='__featureType' ";
        query = StringUtil::replaceString(query, "__feature__", _toStr(value->feature->getName()));
        query = StringUtil::replaceString(query, "__featureType__", _toStr(value->feature->getType()));
        return query;
}

static std::string getBatchWriteQuery() {

        static std::string query =
                "INSERT INTO gicapods_mdl.FeatureToFeatureHistory ( "
                " featurekey, "
                " featurekeytype, "
                " feature, "
                " featureType, "
                " eventtime)"
                "VALUES (?, ?, ?, ?, ?)  USING TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "FeatureToFeatureHistory";
}
};

//////////////////////////////////////////
/////////////////////////////////////////
#include "SegmentDevices.h"

template <>
class CassandraServiceTypeTraits <SegmentDevices> : public GeneralCassandraTypeTraits {
public:
CassandraServiceTypeTraits<SegmentDevices>(gicapods::ConfigService* configService) :
        GeneralCassandraTypeTraits(configService) {

}
void afterDataExtraction(SegmentDevices* value) {
        LOG_EVERY_N(INFO, 1000000)<< "valueType: "<<getValueType()<< ", fetched value :  "<< value->toJson();
}
/*

 */
double getTracingPercentageRate() {
        return tracingPercentageRate;
}

std::string getValueType()
{
        return "SegmentDevices";
}

static int getTTLInSeconds()
{
        return 3600 * 24 * 60; //7 days
}

static bool isEligibleForPruning(SegmentDevices* value) {
        return false;
}


static const char * equals(SegmentDevices* ffh1, SegmentDevices* ffh2) {
        if (StringUtil::equalsIgnoreCase(ffh1->segment->toJson(), ffh2->segment->toJson())) {
                return PARTIAL_EQUAL;
        }
        return NOT_EQUAL_AT_ALL;
}

static bool isValueValidForBatchWrite(SegmentDevices* ffh) {
        return true;
}

static void bindValuesToCassStatementForBatchWrite(SegmentDevices* ffh,
                                                   CassStatement* statement) {

        NULL_CHECK(ffh);
        NULL_CHECK(statement);

        for (auto fh : ffh->devices ) {
            cass_statement_bind_string_by_name(statement, "deviceId", fh->getDeviceId().c_str());
            cass_statement_bind_string_by_name(statement, "deviceType", fh->getDeviceType().c_str());
            cass_statement_bind_string_by_name(statement, "segmentId", _toStr(ffh->segment->id).c_str());
            cass_statement_bind_int64_by_name(statement, "dateCreated",  ffh->dateCreatedInMillis);
          }
}

static void extractRowData(
        CassIterator *rowIterator,
        SegmentDevices* requestObject,
        std::shared_ptr<SegmentDevices>& responseObject) {

        if (responseObject == nullptr) {
                responseObject = std::make_shared<SegmentDevices>();
        }

        std::string deviceId = CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceId");
        std::string deviceType = CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceType");

        long dateCreatedInMillis = CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "dateCreated");
        int segmentId = CassandraDriverInterface::getIntegerValueFromRowByName (rowIterator, "segmentId");
        auto segment = std::make_shared<Segment>(dateCreatedInMillis);
        segment->id = segmentId;
        responseObject->segment = segment;
        responseObject->dateCreatedInMillis = dateCreatedInMillis;
        responseObject->setDevice(deviceId, deviceType);

}

static std::string getReadOneQuery(SegmentDevices* value) {

        std::string query =
                "select segmentId, deviceId, deviceType, dateCreated from gicapods.SegmentDevices where segmentId = '__segmentId__' ";
        query = StringUtil::replaceString(query, "__segmentId__", _toStr(value->segment->id));
        return query;
}

static std::string getBatchWriteQuery() {

        static std::string query =
                "INSERT INTO gicapods.SegmentDevices ( "
                "deviceId, deviceType, segmentId, dateCreated)"
                "VALUES (?, ?, ?, ?)  USING TTL " + _toStr(getTTLInSeconds());

        return query;
}
static std::string getTableName() {
    return "SegmentDevices";
}
};


#endif
