
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "CassandraDriverInterface.h"
#include "DateTimeUtil.h"
#include "AtomicLong.h"
#include "Poco/Runnable.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include "EntityToModuleStateStats.h"

#define NUMBER_OF_SESSIONS 10
#define NUMBER_OF_CLUSTERS 1
#define NUM_IO_WORKER_THREADS 8

CassandraDriverInterface::CassandraDriverInterface(){
								session = nullptr;
								numberOfCuncurrentReuqestsNow = std::make_shared<gicapods::AtomicLong>();
}

CassandraDriverInterface::~CassandraDriverInterface() {

}

std::string CassandraDriverInterface::getStringValueFromRowByName(CassIterator* rowIterator, const char* name) {
								const CassRow* row = cass_iterator_get_row(rowIterator);
								const char* item_string0;
								size_t item_string0_length = 0;
								cass_value_get_string(cass_row_get_column_by_name(row, name), &item_string0,
																														&item_string0_length);
								if (item_string0 == nullptr || item_string0_length == 0) {
																return "";
								}

								std::string value(item_string0, item_string0_length);
								return value;
}


std::string CassandraDriverInterface::getStringValueFromRowAtColumn(CassIterator* rowIterator, int index) {
								const CassRow* row = cass_iterator_get_row(rowIterator);
								const char* item_string0;
								size_t item_string0_length = 0;
								cass_value_get_string(cass_row_get_column(row, index), &item_string0,
																														&item_string0_length);

								if (item_string0 == nullptr || item_string0_length == 0) {
																return "";
								}

								std::string value(item_string0, item_string0_length);
								return value;
}

int CassandraDriverInterface::getIntegerValueFromRowAtColumn(CassIterator* rowIterator, int index) {
								int value = 0;
								const CassRow* row = cass_iterator_get_row(rowIterator);
								cass_value_get_int32(cass_row_get_column(row, index), &value);
								return value;
}

int CassandraDriverInterface::getIntegerValueFromRowByName(CassIterator *rowIterator, const char* name){
								int value = 0;
								const CassRow* row = cass_iterator_get_row(rowIterator);
								cass_value_get_int32(cass_row_get_column_by_name(row, name), &value);
								return value;
}
long long CassandraDriverInterface::getLongValueFromRowByName(CassIterator *rowIterator, const char* name){
								const CassRow* row = cass_iterator_get_row(rowIterator);
								cass_int64_t timestamp_value = 0;
								cass_value_get_int64(cass_row_get_column_by_name(row, name), &timestamp_value);
								return timestamp_value;
}


long long CassandraDriverInterface::getLongValueFromRowAtColumn(CassIterator* rowIterator, int index) {
								const CassRow* row = cass_iterator_get_row(rowIterator);
								cass_int64_t timestamp_value = 0;
								cass_value_get_int64(cass_row_get_column(row, index), &timestamp_value);
								return timestamp_value;
}

double CassandraDriverInterface::getDoubleValueFromRowByName(CassIterator *rowIterator, const char* name){
								const CassRow* row = cass_iterator_get_row(rowIterator);
								cass_double_t value = 0;
								cass_value_get_double(cass_row_get_column_by_name(row, name), &value);
								return value;
}


double CassandraDriverInterface::getDoubleValueFromRowAtColumn(CassIterator* rowIterator, int index) {
								const CassRow* row = cass_iterator_get_row(rowIterator);
								cass_double_t value = 0;
								cass_value_get_double(cass_row_get_column(row, index), &value);
								return value;
}
