
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"
#include "CassandraServiceTypeTraits.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "NumberUtil.h"
#include "CassandraTracer.h"
#include "CassandraReadService.h"
#include "AsyncThreadPoolService.h"

#include "Poco/Runnable.h"

#include "EntityToModuleStateStats.h"
#include "StatisticsUtil.h"
#include "RandomUtil.h"
#include "ValueBatchUpdateTask.h"
#include "ValueTraceTask.h"
#include "AtomicLong.h"
#include "ConfigService.h"
#include "CassandraServiceQueueManager.h"
#include "HttpUtil.h"
#include <thread>


template <class V>
CassandraTracer<V>::CassandraTracer(
								CassandraReadService<V>* cassandraReadService,
								EntityToModuleStateStats* entityToModuleStateStats,
								AsyncThreadPoolService* asyncThreadPoolService,
								gicapods::ConfigService* configService)  : Object(__FILE__) {

								this->cassandraReadService = cassandraReadService;
								this->entityToModuleStateStats =  entityToModuleStateStats;

								this->asyncThreadPoolService =  asyncThreadPoolService;
								this->configService =  configService;

								cassandraServiceTypeTraits = std::make_unique<CassandraServiceTypeTraits<V> > (configService);
								cassandraServiceTypeTraits->loadProperties();

								tracingDecisionRandomizer = std::make_shared<StatisticsUtil> ();

								numberOfReadFailures = std::make_shared<gicapods::AtomicLong>();
								numberOfTotalReads = std::make_shared<gicapods::AtomicLong>();

}

template <class V>
CassandraTracer<V>::~CassandraTracer() {

}

template <class V>
void CassandraTracer<V>::scheduleForTracing(std::shared_ptr<V> oldValue) {
								double probability = cassandraServiceTypeTraits->getTracingPercentageRate() / 100;
								MLOG(3)
																<<" getValueType : "<< cassandraServiceTypeTraits->getValueType()
																<<" TracingPercentageRate" << cassandraServiceTypeTraits->getTracingPercentageRate()
																<<" probability "<< probability;

								if (!tracingDecisionRandomizer->happensWithProb(probability))
								{
																return;
								}

								entityToModuleStateStats->addStateModuleForEntity(
																"TRACE_TASK_SCHEDULED" + cassandraServiceTypeTraits->getValueType(),
																getName(),
																EntityToModuleStateStats::all);

								auto valueTraceTask =
																std::make_shared<ValueTraceTask<V> > (
																								configService,
																								entityToModuleStateStats);
								valueTraceTask->value = oldValue;
								valueTraceTask->cassandraReadService = cassandraReadService;
								valueTraceTask->entityToModuleStateStats = entityToModuleStateStats;
								//TODO :config driven size for queue of traceTasks
								if (asyncThreadPoolService->queueOfTasksWithDelay->unsafe_size() > 1000) {
																//too many trace tasks in the queue, we don't submit for now, until
																//the rest are finished
																entityToModuleStateStats->
																addStateModuleForEntity(
																								"NOT_SUBMITTING_TRACE_TASK_QUEUE_IS_FULL" + cassandraServiceTypeTraits->getValueType(),
																								getName(),
																								"ALL");

								} else {
																entityToModuleStateStats->
																addStateModuleForEntity(
																								"SUBMITTING_TRACE_TASK" + cassandraServiceTypeTraits->getValueType(),
																								getName(),
																								"ALL");

																asyncThreadPoolService->submitTaskWithDelay(valueTraceTask);
								}
}


template <class V>
std::string CassandraTracer<V>::getName() {
								return "CassandraTracer";
}

#include "FeatureDeviceHistory.h"
#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"
#include "PixelDeviceHistory.h"
#include "DeviceSegmentHistory.h"
#include "EventLog.h"
#include "Wiretap.h"
#include "AdHistory.h"
#include "BidEventLog.h"
#include "FeatureToFeatureHistory.h"
#include "ModelScore.h"

#include "SegmentDevices.h"
template class CassandraTracer<SegmentDevices>;
template class CassandraTracer<FeatureToFeatureHistory>;
template class CassandraTracer<AdHistory>;
template class CassandraTracer<EventLog>;
template class CassandraTracer<ModelScore>;
template class CassandraTracer<BidEventLog>;
template class CassandraTracer<GicapodsIdToExchangeIdsMap>;
template class CassandraTracer<FeatureDeviceHistory>;
template class CassandraTracer<DeviceFeatureHistory>;
template class CassandraTracer<IpToDeviceIdsMap>;
template class CassandraTracer<DeviceIdToIpsMap>;
template class CassandraTracer<PixelDeviceHistory>;

template class CassandraTracer<DeviceSegmentHistory>;

template class CassandraTracer<Wiretap>;
