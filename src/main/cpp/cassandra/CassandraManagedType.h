/*
 * CassandraManagedType.h
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef CassandraManagedType_H
#define CassandraManagedType_H



class CassandraManagedType {

public:
std::string valueType;

std::string getValueType() {
        return valueType;
}

virtual ~CassandraManagedType() {

}
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_SVMSGDWORKER_H_ */
