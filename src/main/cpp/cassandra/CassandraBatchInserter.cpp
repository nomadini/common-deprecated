
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"
#include "CassandraServiceTypeTraits.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "NumberUtil.h"
#include "CassandraBatchInserter.h"
#include "AsyncThreadPoolService.h"

#include "Poco/Runnable.h"

#include "EntityToModuleStateStats.h"
#include "StatisticsUtil.h"
#include "RandomUtil.h"
#include "ValueBatchUpdateTask.h"
#include "ValueTraceTask.h"
#include "AtomicLong.h"
#include "ConfigService.h"
#include "CassandraServiceQueueManager.h"
#include "HttpUtil.h"
#include <thread>

#include "CassandraDriver.h"
#include "CassandraTracer.h"
template <class V>
CassandraBatchInserter<V>::CassandraBatchInserter(
								CassandraTracer<V>* cassandraTracer,
								EntityToModuleStateStats* entityToModuleStateStats,
								AsyncThreadPoolService* asyncThreadPoolService,
								gicapods::ConfigService* configService)  : Object(__FILE__) {

								this->cassandraTracer = cassandraTracer;
								this->entityToModuleStateStats =  entityToModuleStateStats;

								this->asyncThreadPoolService =  asyncThreadPoolService;
								this->configService =  configService;

								cassandraServiceTypeTraits = std::make_unique<CassandraServiceTypeTraits<V> > (configService);
								cassandraServiceTypeTraits->loadProperties();

								tracingDecisionRandomizer = std::make_shared<StatisticsUtil> ();

								numberOfReadFailures = std::make_shared<gicapods::AtomicLong>();
								numberOfTotalReads = std::make_shared<gicapods::AtomicLong>();
}

template <class V>
CassandraBatchInserter<V>::~CassandraBatchInserter() {

}

template <class V>
CassError CassandraBatchInserter<V>::tryInsertIntoPreparedStatement(
								CassSession* session,
								const CassPrepared** prepared,
								std::string query) {
								CassError rc = CASS_OK;
								CassFuture* future = NULL;

								future = cass_session_prepare(session, query.c_str());
								cass_future_wait(future);

								rc = cass_future_error_code(future);
								if (rc == CASS_OK ) {
																*prepared = cass_future_get_prepared(future);

								} else if (rc == CASS_ERROR_LIB_REQUEST_TIMED_OUT) {
																LOG_EVERY_N(ERROR, 1)<< "request timed out trying again, query : " << query;
																//request was timed out, retry again after a second
																gicapods::Util::sleepViaBoost(_L_, 1);
								} else {
																//some other propblem happened , we have to exit now
																print_error(future, query, entityToModuleStateStats);
								}
								if (future != NULL) {
																cass_future_free(future);
								}
								return rc;
}

template <class V>
CassError CassandraBatchInserter<V>::prepare_insert_into_batch(
								CassSession* session,
								const CassPrepared** prepared,
								std::string query) {

								for (int i = 0; i < 100; i++) {
																CassError rc = tryInsertIntoPreparedStatement(
																								session,
																								prepared,
																								query);
																if (rc == CASS_OK ) {
																								return rc;
																}
								}


}


template <class V>
void CassandraBatchInserter<V>::insert_into_batch_with_prepared(
								const CassSession* session,
								const CassPrepared* prepared,
								std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& values) {

								if(values->empty()) {
																return;
								}

								CassBatch* batch = cass_batch_new(CASS_BATCH_TYPE_LOGGED);
								//for reference
								// cass_statement_bind_string(statement, 0, cass_string_init(key));
								// cass_statement_bind_bool(statement, 1, basic->bln);
								// cass_statement_bind_float(statement, 2, basic->flt);
								// cass_statement_bind_double(statement, 3, basic->dbl);
								// cass_statement_bind_int32(statement, 4, basic->i32);
								// cass_statement_bind_int64(statement, 5, basic->i64);

								for (auto valueGeneral : *values) {
																std::shared_ptr<V> value = std::static_pointer_cast<V>(valueGeneral);

																if(!cassandraServiceTypeTraits->isValueValidForBatchWrite(value.get())) {
																								LOG(ERROR) << "value is valid for batch write typeName : " << cassandraServiceTypeTraits->getValueType();

																								entityToModuleStateStats->addStateModuleForEntity(
																																"VALUE_NOT_VALID_FOR_BATCH_WRITE" + cassandraServiceTypeTraits->getValueType(),
																																getName(),
																																EntityToModuleStateStats::all,
																																EntityToModuleStateStats::exception);
																								continue;
																}

																cassandraTracer->scheduleForTracing(value);
																// MLOG(3)<<"batching value : " << value->toJson();
																CassStatement* statement = cass_prepared_bind(prepared);
																cassandraServiceTypeTraits->bindValuesToCassStatementForBatchWrite(value.get(), statement);

																cass_batch_add_statement(batch, statement);
																cass_statement_free(statement);
								}


								executeBatchWithRetry(batch, session);
								cass_batch_free(batch);
}


template <class V>
void CassandraBatchInserter<V>::executeBatchWithRetry(
								const CassBatch* batch,
								const CassSession* session) {
								CassError rc;
								int tryNumber = 0;
								do {

																rc = executeBatch(batch, session, tryNumber);
																if (rc == CASS_OK) {
																								break;
																} else if (rc == CASS_ERROR_LIB_REQUEST_TIMED_OUT) {
																								//request was timed out, retry again after a second

																} else {

																								printf("Prepare result: %s\n", cass_error_desc(rc));

																								//some other propblem happened , we have to exit now

																								EXIT("error in running batch insert ");
																}
																//retry after one secon, maybe the load is alot on server
																gicapods::Util::sleepViaBoost(_L_,1);
																tryNumber++;
																LOG(ERROR) << "trying query for "<< tryNumber + 1 << " time";
								} while(rc != CASS_OK);


}

template <class V>
CassError CassandraBatchInserter<V>::executeBatch(
								const CassBatch* batch,
								const CassSession* session,
								int tryNumber) {
								CassError rc = CASS_OK;
								CassFuture* future = NULL;

								future = cass_session_execute_batch((CassSession*) session, batch);
								cass_future_wait(future);

								rc = cass_future_error_code(future);
								if (rc != CASS_OK) {
																entityToModuleStateStats->addStateModuleForEntity(
																								cassandraServiceTypeTraits->getValueType() + "batch_update_failure",
																								getName(),
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::exception);


								} else {
																entityToModuleStateStats->addStateModuleForEntity(
																								cassandraServiceTypeTraits->getValueType() + "_batch_update_success",
																								getName(),
																								EntityToModuleStateStats::all,
																								EntityToModuleStateStats::important);
								}
								if (tryNumber == 10) {
																LOG(ERROR)<<"tried "<< tryNumber<< " times and failed";
																//this will exit the program too
																print_error(future,
																												"batch value query for type " + cassandraServiceTypeTraits->getValueType(),
																												entityToModuleStateStats);
								}
								if (future != NULL) {
																cass_future_free(future);
								}
								return rc;
}

template <class V>
std::string CassandraBatchInserter<V>::getName() {
								return "CassandraBatchInserter";
}

#include "FeatureDeviceHistory.h"
#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"
#include "PixelDeviceHistory.h"
#include "DeviceSegmentHistory.h"
#include "BidEventLog.h"
#include "EventLog.h"
#include "Wiretap.h"
#include "AdHistory.h"
#include "FeatureToFeatureHistory.h"
#include "ModelScore.h"
#include "SegmentDevices.h"
template class CassandraBatchInserter<SegmentDevices>;
template class CassandraBatchInserter<FeatureToFeatureHistory>;
template class CassandraBatchInserter<AdHistory>;
template class CassandraBatchInserter<ModelScore>;
template class CassandraBatchInserter<EventLog>;
template class CassandraBatchInserter<BidEventLog>;
template class CassandraBatchInserter<GicapodsIdToExchangeIdsMap>;
template class CassandraBatchInserter<FeatureDeviceHistory>;
template class CassandraBatchInserter<DeviceFeatureHistory>;
template class CassandraBatchInserter<IpToDeviceIdsMap>;
template class CassandraBatchInserter<DeviceIdToIpsMap>;
template class CassandraBatchInserter<PixelDeviceHistory>;

template class CassandraBatchInserter<DeviceSegmentHistory>;

template class CassandraBatchInserter<Wiretap>;
