#ifndef PartitionRebalancer_H
#define PartitionRebalancer_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>
#include <string>
#include <memory>
#include <vector>
#include <getopt.h>

#include <librdkafka/rdkafkacpp.h>

class PartitionRebalancer : public RdKafka::RebalanceCb {
private:
static void part_list_print (const std::vector<RdKafka::TopicPartition*>&partitions);
bool readFromStartOfAllPartitions;
public:
PartitionRebalancer(bool readFromStartOfAllPartitions);
void rebalance_cb (
								RdKafka::KafkaConsumer *consumer,
								RdKafka::ErrorCode err,
								std::vector<RdKafka::TopicPartition*> &partitions);
};


#endif
