#ifndef KafkaProducer_h
#define KafkaProducer_h



/*
 * librdkafka - Apache Kafka C library  =====> TAG 8.6
 *
 * Apache Kafka consumer & producer example programs
 * using the Kafka driver from librdkafka
 * (https://github.com/edenhill/librdkafka)

   https://github.com/edenhill/librdkafka/blob/master/INTRODUCTION.md

   http://docs.confluent.io/2.0.0/clients/producer.html

   very very important doc about configuration options
   https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
 */


#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <getopt.h>
#include <string>
#include <memory>
#include <vector>
#include <unordered_map>
#include <librdkafka/rdkafkacpp.h>
#include "MyHashPartitionerCb.h"
#include "AtomicLong.h"
class ExampleDeliveryReportCb;

class EntityToModuleStateStats;


class KafkaProducer {
public:
EntityToModuleStateStats* entityToModuleStateStats;
std::string brokers;
std::string groupId;
std::string topic_str;
std::string mode;
std::string debug;
int32_t partition = RdKafka::Topic::PARTITION_UA;
std::unique_ptr<ExampleDeliveryReportCb> deliveryCallBack;
std::unique_ptr<MyHashPartitionerCb> keyPartitioner;
bool exit_eof;
bool do_conf_dump;
MyHashPartitionerCb hash_partitioner;

std::shared_ptr<gicapods::AtomicLong> queueSizeWaitingForAck;

RdKafka::Conf *conf;
RdKafka::Conf *tconf;
RdKafka::Producer *producer;
RdKafka::Topic *topic;
KafkaProducer(std::string brokers,  std::string groupId, std::string topic_str, int32_t partition);

void sendMsgToBroker(RdKafka::Producer* producer, RdKafka::Topic *topic,
																					std::string kafkaMessage);

void produceMessage(std::string kafkaMessage);

virtual ~KafkaProducer();

};




#endif
