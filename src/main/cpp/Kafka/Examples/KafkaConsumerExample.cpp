#include "KafkaConsumer.h"
#include "CassandraDriverInterface.h"
#include "DoNothingMessageProcessor.h"


void setup_KafkaCommon() {
								std::set_terminate(myterminate);
								//startCassandraCluster();
								SignalHandler::installHandlers();
								setupLoggers();
}
int main(int argc, char* argv[]) {

								setup_KafkaCommon();
								std::shared_ptr<KafkaConsumer> consumer = std::make_shared<KafkaConsumer>("localhost", "activebrowsers", 0,
																																																																				DoNothingMessageProcessor::getInstance());
								consumer->consume();

}
