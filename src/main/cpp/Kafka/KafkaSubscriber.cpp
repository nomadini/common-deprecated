
#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <getopt.h>
#include <string>
#include <memory>
#include <vector>
#include <unordered_map>
#include <librdkafka/rdkafkacpp.h>
#include "MyHashPartitionerCb.h"
#include "AtomicLong.h"
#include "EntityToModuleStateStats.h"
#include "KafkaSubscriber.h"
#include "GUtil.h"
#include "LogLevelManager.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "StringUtil.h"

KafkaSubscriber::KafkaSubscriber(
        const char * host,
        std::string groupId,
        const std::vector<std::string> & vecTopics,
        bool startFromBeginningOfAllPartitions,
        std::shared_ptr<MessageProcessor> messageProcessor)
        : m_brokers(std::string(host))
        , m_isRun(true) {
        for (auto elem : vecTopics) {
                m_vecTopics.push_back(elem);

        }

        for (auto elem : m_vecTopics) {
                MLOG(3) << elem << " ";
        }
        MLOG(3) << std::endl;
        this->groupId = groupId;
        this->messageProcessor = messageProcessor;
        ex_rebalance_cb = std::make_shared<PartitionRebalancer> (startFromBeginningOfAllPartitions);
        ex_event_cb = std::make_shared<ExampleEventCb> ();

}

KafkaSubscriber::~KafkaSubscriber() {
        delete m_pConf;
        delete m_pTconf;
        delete m_pTopic;
        delete m_pConsumer;
}

void KafkaSubscriber::connect() {
        setConfiguration();
        setPartition();
        setConsumer();
        setTopics();
        LOG(INFO) << "KafkaSubscriber is Connected";
}

void KafkaSubscriber::checkResultOfConfig(int resultOfConfig,  std::string errstr) {
        if (resultOfConfig != RdKafka::Conf::CONF_OK )
        {
                EXIT("error in config : " + errstr);

        }
}
void KafkaSubscriber::setConfiguration() {
        m_pConf = RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL);
        m_pTconf = RdKafka::Conf::create(RdKafka::Conf::CONF_TOPIC);
        std::string errstr;
        int resultOfConfig;
        /*
           list of all possible configs
           https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
         */
        checkResultOfConfig(m_pConf->set("metadata.broker.list", m_brokers, errstr), errstr);
        checkResultOfConfig(m_pConf->set("group.id", groupId, errstr), errstr);
        checkResultOfConfig(m_pConf->set("statistics.interval.ms","100000", errstr), errstr);
        checkResultOfConfig(m_pTconf->set("auto.commit.enable", "false", errstr), errstr);
        // checkResultOfConfig(m_pTconf->set("auto.commit.interval.ms","10000", errstr), errstr);
        checkResultOfConfig(m_pTconf->set("auto.offset.reset","smallest", errstr), errstr);


        dumpConfigurations(m_pConf, "subscriberPConf");
        dumpConfigurations(m_pTconf, "subscriberPTConf");

}
void KafkaSubscriber::dumpConfigurations(RdKafka::Conf *config, std::string name) {
        std::list<std::string> *dump;
        dump = config->dump();
        LOG(INFO) <<"----------#config for "<<name << std::endl;
        for (std::list<std::string>::iterator it = dump->begin();
             it != dump->end(); )
        {
                std::string config;
                config += *it + " = ";
                it++;
                config += *it;
                it++;

                LOG(INFO) << config << std::endl;
        }
        LOG(INFO) <<"----------end-------------"<<std::endl;
}
void KafkaSubscriber::setPartition() {
        std::string errstr;


        m_pConf->set("rebalance_cb",  &(*ex_rebalance_cb), errstr);
        m_pConf->set("event_cb", &(*ex_event_cb), errstr);
        MLOG(3) << "setPartition" << std::endl;
}


void KafkaSubscriber::setConsumer() {
        std::string errstr;
        m_pConsumer = RdKafka::KafkaConsumer::create(m_pConf, errstr);
        if (!m_pConsumer) {

                EXIT("Failed to create consumer: " + errstr);
        }
}

void KafkaSubscriber::setTopics() {
        std::string errstr;
        m_pConf->set("default_topic_conf", m_pTconf, errstr);
        for(auto topic : m_vecTopics) {
                LOG(INFO)<<"subscribing to topic "<< topic<< std::endl;
        }
        RdKafka::ErrorCode err = m_pConsumer->subscribe(m_vecTopics);
        if (err) {
                LOG(ERROR) << "Failed to subscribe to " << m_vecTopics.size() << " topics: "
                           << RdKafka::err2str(err) << std::endl;
                EXIT("");
        }
}

void KafkaSubscriber::msg_consume(RdKafka::Message * message, void * arg)
{

        // MLOG(3) << message->err() << std::endl;
        // MLOG(3) << "ERR__TIME_OUT = " << RdKafka::ERR__TIMED_OUT << std::endl;

        switch (message->err())
        {

        case RdKafka::ERR__TIMED_OUT:
                entityToModuleStateStats->addStateModuleForEntity("consumerTimeout",
                                                                  "KafkaSubscriber",
                                                                  "ALL");
                consumerTimeout->increment();
                break;
        case RdKafka::ERR_NO_ERROR:
                /* Real message */
                LOG_EVERY_N(INFO, 1000) << "Read good message at offset " << message->offset() << std::endl;
                lastOffsetRead->setValue(message->offset());

                entityToModuleStateStats->addStateModuleForEntity("READ_MSG_FROM_KAFKA",
                                                                  "KafkaSubscriber",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::important);

                entityToModuleStateStats->addStateModuleForEntity("partition_read_from:"
                                                                  + StringUtil::toStr(message->partition ()),
                                                                  "KafkaSubscriber-PartitionDebug",
                                                                  "ALL");
                consumerReadMessage->increment();
                //				printf("%.*s\n", static_cast<int>(message->len()),
                //						static_cast<const char *>(message->payload()));

                invokeTheProcessor(message);

                // if (message->key())
                // {
                //         MLOG(3) << "Key: " << *message->key() << std::endl;
                // }
                // printf("%.*s\n",
                //        static_cast<int>(message->len()),
                //        static_cast<const char *>(message->payload()));
                break;
        case RdKafka::ERR__UNKNOWN_TOPIC:
                LOG_EVERY_N(ERROR, 1000) << google::COUNTER<<"th ERR__UNKNOWN_TOPIC: Consume failed: " << message->errstr() << std::endl;
                entityToModuleStateStats->addStateModuleForEntity("unknownTopic:" + message->errstr(),
                                                                  "KafkaSubscriber",
                                                                  "ALL",
                                                                  EntityToModuleStateStats::exception);
                consumingMsgFailed->increment();
                // m_isRun = false;
                break;
        case RdKafka::ERR__UNKNOWN_PARTITION:
                LOG_EVERY_N(ERROR, 1000) << google::COUNTER<<"th ERR__UNKNOWN_PARTITION: Consume failed: " << message->errstr() << std::endl;
                entityToModuleStateStats->addStateModuleForEntity("unknownPartition:" + message->errstr(),
                                                                  "KafkaSubscriber",
                                                                  "ALL",
                                                                  EntityToModuleStateStats::exception);

                consumingMsgFailed->increment();
                // m_isRun = false;
                break;
        default:
                /* Errors */
                consumingMsgFailed->increment();
                entityToModuleStateStats->addStateModuleForEntity("failed:" + message->errstr(),
                                                                  "KafkaSubscriber",
                                                                  "ALL",
                                                                  EntityToModuleStateStats::exception);
                LOG_EVERY_N(ERROR, 100) << google::COUNTER << "nth time :  Consume failed: " << message->errstr() << std::endl;
                // m_isRun = false;
        }// endswitch
}

void KafkaSubscriber::consumeMSG() {
        while (m_isRun) {

                tryToFetchOneMessage();
        }
}

void KafkaSubscriber::tryToFetchOneMessage() {
        if (scorerHealthFlag->getValue() == false) {
                LOG_EVERY_N(ERROR, 1)<<"consumer stopping because scorer is not healthy";
                entityToModuleStateStats->addStateModuleForEntity("stopping_scorer_is_not_healthy",
                                                                  "KafkaSubscriber",
                                                                  "ALL");
                gicapods::Util::sleepViaBoost(_L_,10);
                return;
        }
        RdKafka::Message *msg = nullptr;
        try {
                if (messageProcessor->isReadyToProcess()) {
                        msg = m_pConsumer->consume(1000);
                        msg_consume(msg, NULL);
                        LOG_EVERY_N(INFO, 10000)<< "consuming msg " << google::COUNTER<<"th";

                        entityToModuleStateStats->addStateModuleForEntity("consumed_msg",
                                                                          "KafkaSubscriber",
                                                                          "ALL");
                } else {
                        entityToModuleStateStats->addStateModuleForEntity("msg_processor_not_ready",
                                                                          "KafkaSubscriber",
                                                                          "ALL");
                        LOG(ERROR) << "Message Processor is not Ready...";
                        gicapods::Util::sleepViaBoost(_L_, 3);
                }

        }
        catch (std::exception const &e) {
                auto diagnosticInformation = boost::diagnostic_information (e);
                LOG_EVERY_N(ERROR, 100) << "Got the " << google::COUNTER << "th exception "
                                        <<" while handling request : "<<diagnosticInformation;

                gicapods::Util::showStackTrace();
        }
        catch(...) {
                gicapods::Util::showStackTrace();
        }
        if (msg!=nullptr) {
                delete msg;
        }
}
void KafkaSubscriber::stop() {
        m_pConsumer->close();
        RdKafka::wait_destroyed(5000);
}

void KafkaSubscriber::invokeTheProcessor(RdKafka::Message *msg) {

        const char *msgChar = static_cast<const char *>(msg->payload());
        int msgLength = static_cast<int>(msg->len());

        std::string messageInString(msgChar, msgLength);
        LOG_EVERY_N(INFO, 100000)<<google::COUNTER<<"th invoking the processor by consumer";

        messageProcessor->process(messageInString);
}
