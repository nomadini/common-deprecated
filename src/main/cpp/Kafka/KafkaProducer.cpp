
#include "GUtil.h"
/*
 * librdkafka - Apache Kafka C library  =====> TAG 8.6
 *

 * Apache Kafka consumer & producer example programs
 * using the Kafka driver from librdkafka
 * (https://github.com/edenhill/librdkafka)
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <getopt.h>

/*
 * Typically include path in a real application would be
 * #include <librdkafka/rdkafkacpp.h>
 */
#include <librdkafka/rdkafkacpp.h>
#include "KafkaProducer.h"
#include "KafkaSubscriber.h"
#include "AtomicLong.h"
#include "ExampleDeliveryReportCb.h"
#include "EntityToModuleStateStats.h"

KafkaProducer::KafkaProducer(std::string brokers, std::string groupId, std::string topic_str, int32_t partition) {
								MLOG(3)<<"starting the kafka producer for partition : " << partition;

								this->brokers = brokers;
								this->topic_str = topic_str;
								this->groupId = groupId;
								this->partition = partition;

								exit_eof = false;
								do_conf_dump = false;

								/*
								 * Create configuration objects
								 */
								conf = RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL);
								tconf = RdKafka::Conf::create(RdKafka::Conf::CONF_TOPIC);

								std::string errstr;
								/*
								   list of all possible configs
								   https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
								 */
								KafkaSubscriber::checkResultOfConfig(conf->set("metadata.broker.list", brokers, errstr), errstr);
								KafkaSubscriber::checkResultOfConfig(conf->set("group.id",  groupId, errstr), errstr);
								KafkaSubscriber::checkResultOfConfig(conf->set("compression.codec",  "snappy", errstr), errstr);

								//When a producer sets acks to "all", min.insync.replicas specifies the minimum number of replicas that must acknowledge a
								//write for the write to be considered successful. If this minimum cannot be met, then the producer will raise an exception
								//(either NotEnoughReplicas or NotEnoughReplicasAfterAppend). When used together, min.insync.replicas and acks allow you to
								// enforce greater durability guarantees. A typical scenario would be to create a topic with a replication factor of 3,
								//set min.insync.replicas to 2, and produce with acks of "all". This will ensure that the producer raises an exception
								// if a majority of replicas do not receive a write.
								// conf->set("min.insync.replicas", "0", errstr);
								/*
								 * Producer mode
								 */

								deliveryCallBack = std::make_unique<ExampleDeliveryReportCb> ();
								keyPartitioner = std::make_unique<MyHashPartitionerCb> ();
								/* Set delivery report callback */
								KafkaSubscriber::checkResultOfConfig(conf->set("dr_cb", &(*deliveryCallBack), errstr), errstr);

								//this means we don't want any acknowledge from broker
								// very very important doc about configuration options
								// https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
								KafkaSubscriber::checkResultOfConfig(tconf->set("acks", "0", errstr), errstr);

								//TODO add this as an argument for key partitioning
								// KafkaSubscriber::checkResultOfConfig(tconf->set("partitioner_cb", &(*keyPartitioner), errstr), errstr);

								/*
								 * Create producer using accumulated global configuration.
								 */
								producer = RdKafka::Producer::create(conf, errstr);
								if (!producer) {
																LOG(ERROR)<<"failed to created producer";
								}

								LOG(INFO) << "Created producer " << producer->name() << std::endl;

								/*
								 * Create topic handle.
								 */
								topic = RdKafka::Topic::create(producer, topic_str, tconf, errstr);
								if (!topic) {
																LOG(ERROR)<<"failed to created topic " << errstr;
								}

								KafkaSubscriber::dumpConfigurations(conf, "Producer Conf");
								KafkaSubscriber::dumpConfigurations(tconf, "Producer Topic Conf");

}

void KafkaProducer::sendMsgToBroker(RdKafka::Producer* producer, RdKafka::Topic *topic, std::string kafkaMessage) {
								NULL_CHECK(topic);
								NULL_CHECK(producer);

								if (kafkaMessage.empty()) {
																producer->poll(0);
																throwEx("message cannot be empty");
								}

								/*
								 * Produce message
								 */
								RdKafka::ErrorCode resp = producer->produce(topic, partition, RdKafka::Producer::RK_MSG_COPY, const_cast<char *>(kafkaMessage.c_str()), kafkaMessage.size(), NULL, NULL);
								if (resp != RdKafka::ERR_NO_ERROR) {
																entityToModuleStateStats->addStateModuleForEntity (
																								"FAILURE_INSERT_MSG_TO_KAFKA",
																								"KafkaProducer",
																								"ALL",
																								EntityToModuleStateStats::exception);
																throwEx("failed to produce the message : " + RdKafka::err2str(resp));
								} else {
																entityToModuleStateStats->addStateModuleForEntity (
																								"SUCCESS_INSERT_MSG_TO_KAFKA",
																								"KafkaProducer",
																								"ALL",
																								EntityToModuleStateStats::important);
								}
								//polling has to be done to dequeue the internal queue
								while (producer->outq_len() > 0) {
																producer->poll(10);
																break;
								}

								queueSizeWaitingForAck->setValue(producer->outq_len());
								LOG_EVERY_N(INFO, 1000)<<google::COUNTER<<"th Waiting for items to be ack by broker  "<<producer->outq_len();
								if (producer->outq_len() > 100) {
																LOG_EVERY_N(ERROR, 10) <<google::COUNTER<<"th producer queue is too long : "<< producer->outq_len();
																//we poll for a second every time to produce more messages
																producer->poll(1000);
								}

}

void KafkaProducer::produceMessage(std::string kafkaMessage) {
								sendMsgToBroker(producer, topic, kafkaMessage);
}

KafkaProducer::~KafkaProducer() {

								if(topic != nullptr) {
																delete topic;
								}
								if (producer != nullptr) {
																delete producer;
								}
}
