
#include "DoNothingMessageProcessor.h"


void DoNothingMessageProcessor::process(std::string msg) {
        //i dont do anything
}

DoNothingMessageProcessor::~DoNothingMessageProcessor() {

}

std::shared_ptr<DoNothingMessageProcessor> DoNothingMessageProcessor::getInstance() {
        std::shared_ptr<DoNothingMessageProcessor> ins(new DoNothingMessageProcessor());
        return ins;
}

bool DoNothingMessageProcessor::isReadyToProcess() {
        return false;
}
