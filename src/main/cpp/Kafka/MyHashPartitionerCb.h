/*
 * MyHashPartitionerCb.h
 *
 *  Created on: Jul 31, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_KAFKA_MYHASHPARTITIONERCB_H_
#define GICAPODS_GICAPODSSERVER_SRC_KAFKA_MYHASHPARTITIONERCB_H_

#include <string>
#include <memory>
#include <vector>
/* Use of this partitioner is pretty pointless since no key is provided
 * in the produce() call. */
#include <librdkafka/rdkafkacpp.h>
class MyHashPartitionerCb: public RdKafka::PartitionerCb {
public:
	int32_t partitioner_cb(const RdKafka::Topic *topic, const std::string *key,
			int32_t partition_cnt, void *msg_opaque);

private:

	static unsigned int djb_hash(const char *str, size_t len);
};


#endif /* GICAPODS_GICAPODSSERVER_SRC_KAFKA_MYHASHPARTITIONERCB_H_ */
