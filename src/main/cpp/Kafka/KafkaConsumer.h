#ifndef KafkaConsumer_h
#define KafkaConsumer_h



/*
 * librdkafka - Apache Kafka C library  =====> TAG 8.6
 *
 * Apache Kafka consumer & producer example programs
 * using the Kafka driver from librdkafka
 * (https://github.com/edenhill/librdkafka)
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <getopt.h>

/*
 * Typically include path in a real application would be
 * #include <librdkafka/rdkafkacpp.h>
 */
 #include <string>
 #include <memory>
 #include <vector>
 #include <unordered_map>
#include <librdkafka/rdkafkacpp.h>
#include "MyHashPartitionerCb.h"
#include "MessageProcessor.h"
#include "AtomicLong.h"
#include "AtomicBoolean.h"
class EntityToModuleStateStats;
#include "KafkaSubscriber.h"

/*

this class is a wrapper around the low level kafka api
vs
the KafkaSubscriber class which is a wrapper around the high level api
*/
class KafkaConsumer {
public:

	bool run;
  EntityToModuleStateStats* entityToModuleStateStats;
	std::string brokers;
	std::string errstr;
	std::string topicName;
	std::string mode;
	std::string debug;
	int32_t partition = RdKafka::Topic::PARTITION_UA;
	int64_t start_offset = RdKafka::Topic::OFFSET_STORED;

	bool exit_eof;
  bool do_conf_dump;
	MyHashPartitionerCb hash_partitioner;

	RdKafka::Conf *conf;
  RdKafka::Conf *tconf;
	RdKafka::Consumer *consumer; //low level api
  std::shared_ptr<KafkaSubscriber> subscriber;
  RdKafka::Topic *topic;
	std::shared_ptr<MessageProcessor> messageProcessor;
  std::shared_ptr<gicapods::AtomicLong> lastOffsetRead;
  std::shared_ptr<gicapods::AtomicLong> consumerTimeout;
  std::shared_ptr<gicapods::AtomicLong> consumingMsgFailed;
  std::shared_ptr<gicapods::AtomicLong> consumerReadMessage;
  std::shared_ptr<gicapods::AtomicLong> endOfPartitionReached;

  std::shared_ptr<gicapods::AtomicBoolean> scorerHealthFlag;

  KafkaConsumer(std::string brokers,
                std::string topicNameArg,
                int32_t partition,
			          std::shared_ptr<MessageProcessor> messageProcessor,
                int64_t offsetPosition);

	void invokeTheProcessor(RdKafka::Message *msg);


	void consume();


	void stop();


	virtual ~KafkaConsumer();


};




#endif
