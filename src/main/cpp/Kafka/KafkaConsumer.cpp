
#include "GUtil.h"
/*
 * librdkafka - Apache Kafka C library  =====> TAG 8.6
 *

 * Apache Kafka consumer & producer example programs
 * using the Kafka driver from librdkafka
 * (https://github.com/edenhill/librdkafka)
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>
#include <getopt.h>
#include "MyHashPartitionerCb.h"
#include "MessageProcessor.h"
#include "ExampleDeliveryReportCb.h"
#include "KafkaConsumer.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"

KafkaConsumer::KafkaConsumer(std::string brokers, std::string topicNameArg, int32_t partition,
																													std::shared_ptr<MessageProcessor> messageProcessor,
																													int64_t start_offsetArg) :
								brokers(brokers), topicName(topicNameArg), partition(partition), messageProcessor(
																messageProcessor) {
								return;//disable this for now

								this->start_offset = start_offsetArg;
								run = true;
								exit_eof = false;
								do_conf_dump = false;

								/*
								 * Create configuration objects
								   auto.commit.enable - If true (default), periodically commit offset of the last message handed to the application. This commited offset will be used when the process restarts to pick up where it left off. If false, the application will manually have to call rd_kafka_offset_store() to store an offset (optional).
								   auto.commit.interval.ms - The frequency in milliseconds that the consumer offsets are commited (written) to offset storage.
								 */
								conf = RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL);
								conf->set("metadata.broker.list", brokers, errstr);
								conf->set("auto.commit.enable", "true", errstr);
								conf->set("auto.commit.interval.ms ","10000", errstr);
								/*
								 * Producer mode
								 */
								ExampleDeliveryReportCb ex_dr_cb;

								/* Set delivery report callback */
								conf->set("dr_cb", &ex_dr_cb, errstr);
								conf->set("group.id", "firstConsumerGroupId1000", errstr);
								/*
								 * Consumer mode
								 */

								std::list<std::string> *dump;
								dump = conf->dump();
								std::cout << "# Global config" << std::endl;
								for (std::list<std::string>::iterator it = dump->begin();
													it != dump->end(); ) {
																std::cout << *it << " = ";
																it++;
																std::cout << *it << std::endl;
																it++;
								}

								/*
								 * Create consumer using accumulated global configuration.
								 */
								consumer = RdKafka::Consumer::create(conf, errstr);
								if (!consumer) {
																LOG(ERROR) << "Failed to create consumer: " << errstr << std::endl;
																EXIT("");
								}

								LOG(INFO) << " Created consumer " << consumer->name() << std::endl;

								/*
								 * Create topic handle.
								 */
								topic = RdKafka::Topic::create(consumer, topicName, tconf, errstr);
								if (!topic) {
																std::cerr << "Failed to create topic: " << errstr << std::endl;
																EXIT("");
								}


								RdKafka::ErrorCode resp = consumer->start(topic, partition, start_offset);
								if (resp != RdKafka::ERR_NO_ERROR) {
																std::cerr << "Failed to start consumer: " << RdKafka::err2str(resp)
																										<< std::endl;
																EXIT("");
								}

}

void KafkaConsumer::invokeTheProcessor(RdKafka::Message *msg) {
								MLOG(3)<<"invoking the processor by consumer";

								const char *msgChar = static_cast<const char *>(msg->payload());
								int msgLength = static_cast<int>(msg->len());

								std::string messageInString(msgChar, msgLength);

								messageProcessor->process(messageInString);
}

void KafkaConsumer::consume() {
								/*
								 * Consume messages
								 */
								while (run) {
																if (scorerHealthFlag->getValue() == false) {
																								LOG_EVERY_N(ERROR, 1)<<"consumer stopping because scorer is not healthy";
																								gicapods::Util::sleepViaBoost(_L_,10);
																								continue;
																}
																RdKafka::Message *msg = consumer->consume(topic, partition, 1000);

																switch (msg->err()) {
																case RdKafka::ERR__TIMED_OUT:
																								consumerTimeout->increment();
																								LOG(INFO)<<"consumer timed out..";
																								break;

																case RdKafka::ERR_NO_ERROR:
																								/* Real message */
																								LOG(INFO)<<"Read msg at offset "<<msg->offset();
																								if (msg->key()) {
																																LOG(INFO)<<"Key: "<<*msg->key();
																								}
																								lastOffsetRead->setValue(msg->offset());
																								consumerReadMessage->increment();
																								//http://docs.confluent.io/2.0.0/clients/librdkafka/classRdKafka_1_1KafkaConsumer.html

//				printf("%.*s\n", static_cast<int>(msg->len()),
//						static_cast<const char *>(msg->payload()));

																								invokeTheProcessor(msg);

																								break;

																case RdKafka::ERR__PARTITION_EOF:
																								endOfPartitionReached->increment();

																								break;

																default:
																								consumingMsgFailed->increment();
																								LOG(ERROR)<<"Consume failed: "<<msg->errstr();

																}

																if(msg!= nullptr) {
																								delete msg;
																}

																consumer->poll(0);
								}

								/*
								 * Stop consumer
								 */
								consumer->stop(topic, partition);

								consumer->poll(1000);
}

void KafkaConsumer::stop() {
								run = false;
}

KafkaConsumer::~KafkaConsumer() {
								stop();

								if (consumer != nullptr) {
																delete consumer;
								}
}
