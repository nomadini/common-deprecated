//
// Created by Mahmoud Taabodi on 7/14/16.
//

#include "LastTimeSeenSource.h"
#include "GUtil.h"
#include "Device.h"

LastTimeSeenSource::LastTimeSeenSource(
        AerospikeDriverInterface* aeroSpikeDriver,
        std::string namespaceName,
        std::string setName,
        std::string binName) {

        this->aeroSpikeDriver = aeroSpikeDriver;
        this->namespaceName = namespaceName;
        this->setName = setName;
        this->binName = binName;

        assertAndThrow(!this->namespaceName.empty());
        assertAndThrow(!this->setName.empty());
        assertAndThrow(!this->binName.empty());
}

long LastTimeSeenSource::countAndGetTimesSeen(std::shared_ptr<Device> device, int ttlInSeconds) {
        return aeroSpikeDriver->addValueAndRead(namespaceName,
                                                setName,
                                                device->getDeviceId() + device->getDeviceType(),
                                                "timesSeen",
                                                1,
                                                ttlInSeconds);
}

bool LastTimeSeenSource::hasSeenInLastXSeconds(std::shared_ptr<Device> device) {
        NULL_CHECK(aeroSpikeDriver);
        NULL_CHECK(device);
        int cacheResultStatus = 0;
        std::string value = aeroSpikeDriver->get_cache(namespaceName,
                                                       setName,
                                                       device->getDeviceId() + device->getDeviceType(),
                                                       binName,
                                                       cacheResultStatus);

        return !value.empty ();
}


void LastTimeSeenSource::markAsSeenInLastXSeconds(std::shared_ptr<Device> device,
                                                  int ttlInSeconds) {
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        aeroSpikeDriver->set_cache(namespaceName,
                                   setName,
                                   device->getDeviceId() + device->getDeviceType(),
                                   binName,
                                   "yes",
                                   ttlInSeconds);
}


bool LastTimeSeenSource::isBadDeviceByCount(std::shared_ptr<Device>& device) {
        NULL_CHECK(aeroSpikeDriver);
        int cacheResultStatus = 0;
        std::string value = aeroSpikeDriver->get_cache(namespaceName,
                                                       setName,
                                                       device->getDeviceId(),
                                                       "badDvcByCnt",
                                                       cacheResultStatus);

        return value.compare("yes") == 0;
}


void LastTimeSeenSource::markAsGoodDeviceByCount(std::shared_ptr<Device>& device,
                                                 int ttlInSeconds) {
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        aeroSpikeDriver->set_cache(namespaceName,
                                   setName,
                                   device->getDeviceId(),
                                   "badDvcByCnt",
                                   "no",
                                   ttlInSeconds);
}
void LastTimeSeenSource::markAsBadDeviceByCount(std::shared_ptr<Device>& device,
                                                int ttlInSeconds) {
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        aeroSpikeDriver->set_cache(namespaceName,
                                   setName,
                                   device->getDeviceId(),
                                   "badDvcByCnt",
                                   "yes",
                                   ttlInSeconds);
}

LastTimeSeenSource::~LastTimeSeenSource() {

}
