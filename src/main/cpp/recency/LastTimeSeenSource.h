//
// Created by Mahmoud Taabodi on 7/14/16.
//

#ifndef COMMON_DIRECTLASTTIMESEENSOURCE_H
#define COMMON_DIRECTLASTTIMESEENSOURCE_H

#include "LastTimeSeenSource.h"
#include "AerospikeDriver.h"
class Device;

class LastTimeSeenSource {

public:
AerospikeDriverInterface* aeroSpikeDriver;
std::string namespaceName;
std::string setName;
std::string binName;
LastTimeSeenSource(AerospikeDriverInterface* aeroSpikeDriver,
                   std::string namespaceName,
                   std::string setName,
                   std::string binName
                   );

long countAndGetTimesSeen(std::shared_ptr<Device> device, int ttlInSeconds);

bool hasSeenInLastXSeconds(std::shared_ptr<Device> device);

void markAsSeenInLastXSeconds(std::shared_ptr<Device> device,
                              int ttlInSeconds);

bool isBadDeviceByCount(std::shared_ptr<Device>& device);

void markAsBadDeviceByCount(std::shared_ptr<Device>& device,
                            int ttlInSeconds);
void markAsGoodDeviceByCount(std::shared_ptr<Device>& device,
                             int ttlInSeconds);
virtual ~LastTimeSeenSource();
};
#endif //COMMON_DIRECTLASTTIMESEENSOURCE_H
