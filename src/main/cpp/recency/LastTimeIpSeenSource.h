//
// Created by Mahmoud Taabodi on 7/14/16.
//

#ifndef COMMON_LASTTIMESEENSOURCE_H
#define COMMON_LASTTIMESEENSOURCE_H

#include <memory>
#include <string>
class Device;
class AerospikeDriverInterface;

class LastTimeIpSeenSource;

class LastTimeIpSeenSource {

public:
AerospikeDriverInterface* aeroSpikeDriver;
std::string namespaceName;
std::string setName;
std::string binName;
LastTimeIpSeenSource(AerospikeDriverInterface* aeroSpikeDriver,
                     std::string namespaceName,
                     std::string setName,
                     std::string binName);
virtual ~LastTimeIpSeenSource();

long countAndGetTimesSeen(std::string& deviceIp, int ttlInSeconds);

bool hasSeenInLastXSeconds(std::string& deviceIp);


void markAsSeenInLastXSeconds(std::string& deviceIp,
                              int ttlInSeconds);

bool isBadIpByCount(std::string& deviceIp);

void markAsBadIpByCount(std::string& deviceIp,
                        int ttlInSeconds);

};
#endif //COMMON_LASTTIMESEENSOURCE_H
