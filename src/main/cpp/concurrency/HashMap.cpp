#include "GUtil.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"
#include "IntWrapper.h"
#include "HashMap.h"


template <class K, class V>
gicapods::HashMap<K, V>::HashMap()   : Object(__FILE__) {
        map = std::make_shared<std::unordered_map<K, std::shared_ptr<V> > > ();
        numberOfPutCalls = std::make_shared<gicapods::AtomicLong>();
        // this->entityToModuleStateStats = entityToModuleStateStatsArg;
}

template <class K, class V>
gicapods::HashMap<K, V>::~HashMap() {

}


template <class K, class V>
void gicapods::HashMap<K, V>::put(K key, std::shared_ptr<V> value) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);
        map->insert(std::make_pair(key, value));
        checkMapSize();
}

template <class K, class V>
void gicapods::HashMap<K, V>::putIfNotExisting(K key, std::shared_ptr<V> value) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);


        auto pairPtr = map->find(key);
        if (pairPtr != map->end()) {
                map->insert(std::make_pair(key, value));
        }

        checkMapSize();
}

template <class K, class V>
std::shared_ptr<V> gicapods::HashMap<K, V>::get(K key) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        auto pairPtr = map->find(key);
        if (pairPtr != map->end()) {
                return pairPtr->second;
        }
        throwEx("value was not found for key ");
}

template <class K, class V>
std::shared_ptr<V> gicapods::HashMap<K, V>::getOptional(K key) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        std::shared_ptr<V> retValue;
        auto pairPtr = map->find(key);
        if (pairPtr != map->end()) {
                retValue = pairPtr->second;
        }
        return retValue;
}

template <class K, class V>
std::shared_ptr<V> gicapods::HashMap<K, V>::getOrCreate(K key) {

        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        std::shared_ptr<V> retValue;

        auto pairPtr = map->find(key);
        if (pairPtr != map->end()) {
                retValue = pairPtr->second;
        } else {
                retValue = CacheTraits::getNewInstance();
                NULL_CHECK(retValue);
                map->insert(std::make_pair(key, retValue));
        }
        return retValue;
}


template <class K, class V>
bool gicapods::HashMap<K, V>::exists(K key) {

        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);
        auto pairPtr = map->find(key);
        if (pairPtr != map->end()) {
                return true;
        }
        return false;
}

template <class K, class V>
long gicapods::HashMap<K, V>::size() {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        return (long) map->size();
}

template <class K, class V>
void gicapods::HashMap<K, V>::checkMapSize() {

        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        //we perform this check every 100 calls
        numberOfPutCalls->increment();
        if (numberOfPutCalls->getValue() > 100) {
                if (size() > 10000) {
                        // if (entityToModuleStateStats == nullptr) {
                        //         LOG(ERROR)<< "MAP IS TOO BIG";
                        //         return;
                        // }
                        // NULL_CHECK(entityToModuleStateStats);
                        // entityToModuleStateStats->addStateModuleForEntity(
                        //         "ERROR_MAP_TOO_BIG",
                        //         "HashMap"+ CacheTraits::getValueName(),
                        //         EntityToModuleStateStats::all,
                        //         EntityToModuleStateStats::exception
                        //         );
                }
                numberOfPutCalls->setValue(0);
        }

}

template <class K, class V>
void gicapods::HashMap<K, V>::clear() {
        //clear method for noraml tbb map is not thread safe,
        //here we get a write lock to clear the method,
        //in all other methods, we are getting shared access lock

        try {
                // get upgradable access
                boost::upgrade_lock<boost::shared_mutex> lock(_access);

                // get exclusive access
                boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
                // now we have exclusive access

                map->clear();
        } catch (...) {
                gicapods::Util::showStackTrace();
        }

}

#include "HashMap.h"
template class gicapods::HashMap<int, IntWrapper>;
