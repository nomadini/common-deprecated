#include <atomic>
#include "AtomicDouble.h"
#include "NumberUtil.h"

gicapods::AtomicDouble::AtomicDouble()  : Object(__FILE__) {
        doubleValue.store (0, std::memory_order_seq_cst);
}

gicapods::AtomicDouble::AtomicDouble(double val) : Object(__FILE__) {
        doubleValue.store (val, std::memory_order_seq_cst);
}

double gicapods::AtomicDouble::getValue() {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        return doubleValue.load();
}
double gicapods::AtomicDouble::getValueRound(int percision) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        return NumberUtil::roundDouble(doubleValue.load(), percision);
}

void gicapods::AtomicDouble::setValue(double val) {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        doubleValue.store (val, std::memory_order_seq_cst);

}

void gicapods::AtomicDouble::addValue(double val) {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        auto current = doubleValue.load ();
        while (!doubleValue.compare_exchange_weak (current, current + val));
}

void gicapods::AtomicDouble::decrement(double val) {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        auto current = doubleValue.load ();
        while (!doubleValue.compare_exchange_weak (current, current - val));
}
