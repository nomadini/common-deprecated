/*
 * AeroCacheService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef HashMapCacheTraits_H_
#define HashMapCacheTraits_H_

#include <string>
#include "GUtil.h"

template <class K, class V>
class HashMapCacheTraits
{

public:
static std::string getValueName()
{
        throwEx("not implemented ");
}

static std::shared_ptr<V> getNewInstance()
{
        throwEx("not implemented ");
        return nullptr;
}

};

#include "IntWrapper.h"

template <>
class HashMapCacheTraits <int, IntWrapper> {
public:
static std::string getValueName()
{
        return "IntWrapper ";
}

static std::shared_ptr<IntWrapper> getNewInstance()
{
        return std::make_shared<IntWrapper>();
}
};

#endif
