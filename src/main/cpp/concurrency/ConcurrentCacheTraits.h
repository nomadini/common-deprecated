/*
 * AeroCacheService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef ConcurrentCacheTraits_H_
#define ConcurrentCacheTraits_H_

#include <string>
#include "FeatureDeviceHistory.h"
#include "GUtil.h"
#include "AtomicLong.h"
#include "DateTimeUtil.h"
#include "DeviceVisitCountHistory.h"
#include "Segment.h"

template <class K, class V>
class ConcurrentCacheTraits
{

public:
static std::string getValueName()
{
        throwEx("not implemented");
}

static std::shared_ptr<V> getNewInstance()
{
        throwEx("not implemented");
        return nullptr;
}

};


template <>
class ConcurrentCacheTraits <std::string, FeatureDeviceHistory> {
public:
static std::string getValueName()
{
        return "FeatureDeviceHistory";
}

static std::shared_ptr<FeatureDeviceHistory> getNewInstance()
{
        throwEx("not implemented");
        return nullptr;//just for warning;
}
};

template <>
class ConcurrentCacheTraits <std::string, gicapods::AtomicLong> {
public:
static std::string getValueName()
{
        return "AtomicLong";
}

static std::shared_ptr<gicapods::AtomicLong> getNewInstance()
{
        return std::make_shared<gicapods::AtomicLong>();
}
};



template <>
class ConcurrentCacheTraits <std::string, DeviceVisitCountHistory> {
public:
static std::string getValueName()
{
        return "DeviceVisitCountHistory";
}

static std::shared_ptr<DeviceVisitCountHistory> getNewInstance()
{
        return std::make_shared<DeviceVisitCountHistory>();
}
};


template <>
class ConcurrentCacheTraits <std::string, Segment> {
public:
static std::string getValueName()
{
        return "Segment";
}

static std::shared_ptr<Segment> getNewInstance()
{
        return std::make_shared<Segment>(DateTimeUtil::getNowInMilliSecond());
}
};

#include "TgBiddingPerformanceMetricInBiddingPeriod.h"
template <>
class ConcurrentCacheTraits <int, TgBiddingPerformanceMetricInBiddingPeriod> {
public:
static std::string getValueName()
{
        return "TgBiddingPerformanceMetricInBiddingPeriod";
}

static std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriod> getNewInstance()
{
        return std::make_shared<TgBiddingPerformanceMetricInBiddingPeriod>();
}
};

#include "ConfirmedWinRequest.h"
template <>
class ConcurrentCacheTraits <std::string, ConfirmedWinRequest> {
public:
static std::string getValueName()
{
        return "ConfirmWinCounterModule";
}

static std::shared_ptr<ConfirmedWinRequest> getNewInstance()
{
        return std::make_shared<ConfirmedWinRequest>();
}
};

#include "IntWrapper.h"
template <>
class ConcurrentCacheTraits <int, IntWrapper> {
public:
static std::string getValueName()
{
        return "IntWrapper";
}

static std::shared_ptr<IntWrapper> getNewInstance()
{
        return std::make_shared<IntWrapper>();
}
};


#endif
