//
//  AtomicLong.h
//  PicoDB
//
//  Created by Mahmoud Taabodi on 5/31/14.
//  Copyright (c) 2014 Mahmoud Taabodi. All rights reserved.
//

#ifndef GICO_AtomicLong_h
#define GICO_AtomicLong_h
#include <atomic>
#include <memory>
#include <boost/thread.hpp>
namespace gicapods {

class AtomicLong { // : public Object {this will cause cyclic problems

private:

boost::shared_mutex _access;
std::shared_ptr<std::atomic<long> > count;

public:
AtomicLong();
AtomicLong(long count);

void increment();
void decrement();
long getValue();

void setValue(long val);

void addValue(long val);

void decrement(long val);

};

}//end of namespace

#endif
