/*
 * MapFunction.h
 *
 *  Created on: Sep 5, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_VECTORFUNCTION_H_
#define GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_VECTORFUNCTION_H_

//an extenstion of this class is InsertDomainToBeReviewed in MySqlDriver.h
template<typename dataType>
class VectorFunction {
public :
	virtual void apply(std::pair<dataType> pair)=0;
	virtual ~VectorFunction()   {     } ;
};

//typedef on a class with template is called alias template
template<typename dataType>
using VectorFunction = std::shared_ptr<VectorFunction<dataType>>;


#endif /* GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_VECTORFUNCTION_H_ */
