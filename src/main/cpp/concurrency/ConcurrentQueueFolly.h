#ifndef ConcurrentQueueFolly_H
#define ConcurrentQueueFolly_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include <tbb/concurrent_hash_map.h>
#include <tbb/concurrent_unordered_set.h>
#include <tbb/concurrent_queue.h>
#include "Object.h"
#include <boost/optional.hpp>
template<class T>
class ConcurrentQueueFolly : public Object {

public:
std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<T> > > entityQueue;

std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<T> > > getEntityQueue();

boost::optional<std::shared_ptr<T> > dequeueEntity();

void enqueueEntity(std::shared_ptr<T> segment);

long size();
bool empty();
void clear();

ConcurrentQueueFolly<T>();

virtual ~ConcurrentQueueFolly<T>();
};

#endif
