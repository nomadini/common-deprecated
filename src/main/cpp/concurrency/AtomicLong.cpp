#include "AtomicLong.h"

gicapods::AtomicLong::AtomicLong()  {
        count = std::make_shared<std::atomic<long> > ();
        count->store (0, std::memory_order_seq_cst);
}

gicapods::AtomicLong::AtomicLong(long count) {
        this->count = std::make_shared<std::atomic<long> > ();
        this->count->store (count, std::memory_order_seq_cst);
}

void gicapods::AtomicLong::increment() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        (*count)++;
}

void gicapods::AtomicLong::decrement() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        (*count)--;
}

long gicapods::AtomicLong::getValue() {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        return count->load ();
}

void gicapods::AtomicLong::setValue(long val) {

        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access

        count->store (val, std::memory_order_seq_cst);
}

void gicapods::AtomicLong::addValue(long val) {

        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        count->fetch_add (val, std::memory_order_seq_cst);
}

void gicapods::AtomicLong::decrement(long val) {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        count->fetch_sub (val, std::memory_order_seq_cst);
}
