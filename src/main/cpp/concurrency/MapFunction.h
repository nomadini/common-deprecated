/*
 * MapFunction.h
 *
 *  Created on: Sep 5, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_MAPFUNCTION_H_
#define GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_MAPFUNCTION_H_

//an extenstion of this class is InsertDomainToBeReviewed in MySqlDriver.h
template<typename keyType, typename valueType>
class MapFunction {
public :
	virtual void apply(std::pair<keyType, valueType> pair)=0;
	virtual ~MapFunction()   {     } ;
};

//typedef on a class with template is called alias template
template<typename keyType, typename valueType>
using MapFunctionPtr = std::shared_ptr<MapFunction<keyType , valueType>>;


#endif /* GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_MAPFUNCTION_H_ */
