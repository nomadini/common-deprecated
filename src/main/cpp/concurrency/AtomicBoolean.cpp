#include "AtomicBoolean.h"

gicapods::AtomicBoolean::AtomicBoolean()  : Object(__FILE__) {
        booleanValue = std::make_shared<std::atomic<bool> > ();
        booleanValue->store (false, std::memory_order_seq_cst);
}

gicapods::AtomicBoolean::AtomicBoolean(bool value) : Object(__FILE__) {
        booleanValue = std::make_shared<std::atomic<bool> > ();
        booleanValue->store (value, std::memory_order_seq_cst);
}

bool gicapods::AtomicBoolean::getValue() {
        return booleanValue->load ();
}

void gicapods::AtomicBoolean::setValue(bool val) {
        booleanValue->store (val, std::memory_order_seq_cst);

}

void gicapods::AtomicBoolean::alternate() {
        auto current = booleanValue->load ();
        while (!booleanValue->compare_exchange_weak (current, !current));
}
