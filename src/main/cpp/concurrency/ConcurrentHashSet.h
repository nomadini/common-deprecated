#ifndef gicapods_ConcurrentHashSet_h
#define gicapods_ConcurrentHashSet_h

#include <atomic>
#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_unordered_set.h>
#include "AtomicLong.h"
class EntityToModuleStateStats;
#include "ConcurrentCacheTraits.h"
#include <boost/thread.hpp>
#include "Object.h"
/*
   General Documentation : https://software.intel.com/en-us/node/506191
   performance notes
   0! Specify the initial size of the table as an appropriate known level.
   It scales bad with default initial size.1. Check the hash function.
   It should take into account the fact that the table size is power of two
   and it requires randomness in the lowest bits of the hash value, e.g.
   multiply it by a prime number (see the Reference).2. Do not use accessors where possible -
   they are effectively locks,
   e.g. rewritecontainsKey() as { return _ds.count(key); }
   3. Use tbbmalloc


   //  tbb::concurrent_unordered_set<std::string, tbb::concurrent_unordered_set<std::string> >::iterator iter;
   //  for (iter = getPartialNamesToLogs ()->begin ();
   //       iter != getPartialNamesToLogs ()->end ();
   //       iter++) {
 */

namespace gicapods {

template<class V>
class ConcurrentHashSet : public Object {
private:

boost::shared_mutex _access;


std::shared_ptr<gicapods::AtomicLong> numberOfPutCalls;

//this is public for accessing the iterators
std::shared_ptr<tbb::concurrent_unordered_set<V> > set;

public:
EntityToModuleStateStats* entityToModuleStateStats;
typedef ConcurrentCacheTraits<std::string,V> CacheTraits;
ConcurrentHashSet<V>();
virtual ~ConcurrentHashSet<V>();

void put(V value);

void putIfNotExisting(V value);

bool exists(V value);

long size();

void checkHashSetSize();

void clear();

std::shared_ptr<tbb::concurrent_unordered_set<V> > getCopyOfSet();
};

}

#endif
