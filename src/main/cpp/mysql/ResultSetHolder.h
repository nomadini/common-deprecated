#ifndef ResultSetHolder_h
#define ResultSetHolder_h

#include <vector>
#include <memory>
#include "ConnectionHolder.h"
#include <string>
#include "Object.h"

/**
   holds a result set and connection that was used
   to create the result set and releases the connection
   when the result set is used
 */
class ResultSetHolder : public Object {

public:
std::shared_ptr<sql::ResultSet> resultset;
std::shared_ptr<ConnectionHolder> connectionHolder;


ResultSetHolder(
        std::shared_ptr<sql::ResultSet> resultset,
        std::shared_ptr<ConnectionHolder> connectionHolder);

bool next();
int getInt(const std::string& field);
int getInt(int field);

long getInt64(const std::string& field);
long getInt64(int field);

long getLong(const std::string& field);
long getLong(int field);

std::string getString(const std::string& field);
std::string getString(int field);

double getDouble(int field);
double getDouble(const std::string& field);

virtual ~ResultSetHolder();


};

#endif
