#ifndef ConnectionPool_h
#define ConnectionPool_h



#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
class RandomUtil;
#include "mysql_connection.h"
#include <vector>
#include <memory>
#include <boost/thread.hpp>
#include "Object.h"
namespace gicapods { class ConfigService; }

class ConnectionHolder;
class ConnectionPool;




class ConnectionPool : public Object {

public:
sql::Driver *driver;
std::shared_ptr<RandomUtil> randomUtil;
std::string urlConnection;
std::string username;
std::string password;
std::string schema;

int minRequiredPoolSize;

gicapods::ConfigService* configService;
std::shared_ptr<boost::shared_mutex> mutex;
std::vector<std::shared_ptr<ConnectionHolder> > pool;

ConnectionPool(std::string urlConnection,
               std::string username,
               std::string password,
               std::string schema,
               int poolSize
               );
virtual ~ConnectionPool();


void releaseConnection(std::shared_ptr<ConnectionHolder> connection);
void addConneciton(std::shared_ptr<ConnectionHolder> connection);
void initializeThePool();
std::shared_ptr<ConnectionHolder> getConnectionFromPool();
};

#endif
