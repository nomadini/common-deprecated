

#include "ResultSetHolder.h"
#include "RandomUtil.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "NumberUtil.h"
#include "ConnectionHolder.h"
#include <boost/thread.hpp>
ResultSetHolder::ResultSetHolder(
        std::shared_ptr<sql::ResultSet> resultset,
        std::shared_ptr<ConnectionHolder> connectionHolder
        )  : Object(__FILE__) {
        this->resultset = resultset;
        this->connectionHolder = connectionHolder;

}

ResultSetHolder::~ResultSetHolder() {
        connectionHolder->markFree();
        connectionHolder->resetConnection();
}

bool ResultSetHolder::next() {
        return resultset->next();
}

int ResultSetHolder::getInt(const std::string& field) {
        return resultset->getInt(field);
}

long ResultSetHolder::getInt64(int field) {
        return resultset->getInt64(field);
}

long ResultSetHolder::getInt64(const std::string& field) {
        return resultset->getInt64(field);
}

long ResultSetHolder::getLong(int field) {
        return resultset->getInt64(field);
}

long ResultSetHolder::getLong(const std::string& field) {
        return resultset->getInt64(field);
}

int ResultSetHolder::getInt(int field) {
        return resultset->getInt(field);
}

std::string ResultSetHolder::getString(const std::string& field) {
        return resultset->getString(field);
}


std::string ResultSetHolder::getString(int field) {
        return resultset->getString(field);
}

double ResultSetHolder::getDouble(int field) {
        return NumberUtil::roundDouble(resultset->getDouble(field), 4);
}

double ResultSetHolder::getDouble(const std::string& field) {
        return NumberUtil::roundDouble(resultset->getDouble(field), 4);
}
