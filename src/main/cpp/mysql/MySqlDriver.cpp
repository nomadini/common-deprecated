#include "MySqlDriver.h"

#include "GUtil.h"

#include "MapFunction.h"

#include "StringUtil.h"
#include "CollectionUtil.h"
#include "ConverterUtil.h"
#include "ConfigService.h"

#include "boost/foreach.hpp"
#include <string>
#include <memory>
#include "RandomUtil.h"
#include "DateTimeUtil.h"
#include "ResultSetHolder.h"
#include "ConnectionPool.h"
#include "ConnectionHolder.h"
MySqlDriver::MySqlDriver(std::string urlConnection,
                         std::string username,
                         std::string password,
                         std::string schema,
                         int poolSize)  : Object(__FILE__) {

        if (schema.empty() || urlConnection.empty()) {
                //this is the case for creating a MySqlDriverMock
                LOG(INFO) << "skipping the connection to database";
                return;
        }

        LOG(ERROR) << "initialize the mysql pool with "<< poolSize << " instances";
        this->pool =
                std::make_shared<ConnectionPool>(
                        urlConnection,
                        username,
                        password,
                        schema,
                        poolSize);
}


std::shared_ptr<ConnectionHolder> MySqlDriver::getConnectionFromPool() {
        return this->pool->getConnectionFromPool();
}

int MySqlDriver::getLastInsertedId() {

        auto res = executeQuery (StringUtil::toStr ("SELECT LAST_INSERT_ID();"));
        while (res->next ()) {
                return res->getInt (1);
        }
        throwEx("failed to get last inserted id");
}


std::shared_ptr<ResultSetHolder> MySqlDriver::executeQuery(std::string query) {
        MLOG(10)<<" query to execute : " << query;
        //we try multiple times to make sure we don't return not result
        //in case of mysql connection gone away or lost connection errors
        std::shared_ptr<ConnectionHolder> connectionInPool;
        for (int tryNumber = 0; tryNumber < 5; tryNumber++) {
                try {
                        connectionInPool = getConnectionFromPool();
                        std::shared_ptr<sql::Statement> stmt(connectionInPool->getConnection()->createStatement());
                        std::shared_ptr<sql::ResultSet> resultSet(stmt->executeQuery (query));

                        auto resultSetHolder =
                                std::make_shared<ResultSetHolder>(resultSet,
                                                                  connectionInPool);
                        return resultSetHolder;
                } catch (const sql::SQLException &e) {
                        if (connectionInPool) {
                                pool->releaseConnection(connectionInPool);
                        }

                        LOG(ERROR) <<
                                "MySqlDriver: SQLException in " << e.what ()
                                   << " , ERROR : MySQL error code:  " << e.getErrorCode () <<
                                " , SQLState  " << e.getSQLState () <<
                                "query causing error : " << query;
                        gicapods::Util::sleepViaBoost(_L_, 5);

                }
        }
        EXIT("mysql has thrown errors....fix it");
}



int MySqlDriver::executedUpdateStatement(std::string query) {

        //we try multiple times to make sure we don't return not result
        //in case of mysql connection gone away or lost connection errors
        std::shared_ptr<ConnectionHolder> connectionInPool;

        for (int tryNumber = 0; tryNumber < 5; tryNumber++) {
                try {
                        MLOG(10) <<"query for updating mysql table : "<<query;
                        connectionInPool = getConnectionFromPool();
                        std::shared_ptr<sql::Statement> stmt(connectionInPool->getConnection()->createStatement ());
                        int numberOfUpdates =  stmt->executeUpdate (query);
                        pool->releaseConnection(connectionInPool);
                        return numberOfUpdates;
                } catch (const sql::SQLException &e) {
                        if (connectionInPool) {
                                pool->releaseConnection(connectionInPool);
                        }

                        //waiting for 5 seconds before next try
                        LOG(ERROR) << "query failed...trying in 5 seconds";
                        LOG(ERROR) <<
                                "MySqlDriver: SQLException in " << e.what ()
                                   << " , ERROR : MySQL error code:  " << e.getErrorCode () <<
                                " , SQLState  " << e.getSQLState () <<
                                "query causing error : " << query;
                        gicapods::Util::sleepViaBoost(_L_, 5);

                }
        }
        EXIT("mysql has thrown errors....fix it");
}


void MySqlDriver::deleteAll(std::string tableName) {
        if(!StringUtil::containsCaseInSensitive(pool->schema, "test")) {
                throwEx("cannot delete table in non-test schema: " + pool->schema);
        }
        std::string query = " DELETE from " + tableName;
        executedUpdateStatement (query);

}

MySqlDriver::~MySqlDriver() {
        if (driver != NULL) {
                // delete driver; this doesnt work
                // driver->threadEnd();//throws exception in tests!
        }
}

std::string MySqlDriver::getString(std::shared_ptr<ResultSetHolder> resultset, int index) {
        std::string result = resultset->getString(index);
        if (StringUtil::equalsIgnoreCase("null", result)) {
                result = "";
        }

        return result;
}

std::string MySqlDriver::getString(std::shared_ptr<ResultSetHolder> resultset, std::string index) {
        std::string result =  resultset->getString(index);
        if (StringUtil::equalsIgnoreCase("null", result)) {
                result = "";
        }

        return result;
}

Poco::DateTime MySqlDriver::parseDateTime(std::shared_ptr<ResultSetHolder> resultset, int index) {
        return DateTimeUtil::parseDateTime(MySqlDriver::getString( resultset, index));
}

Poco::DateTime MySqlDriver::parseDateTime(std::shared_ptr<ResultSetHolder> resultset, std::string index) {
        return DateTimeUtil::parseDateTime(MySqlDriver::getString( resultset, index));
}

std::shared_ptr<MySqlDriver> MySqlDriver::getInstance(gicapods::ConfigService* configService) {
        int poolSize = 1;
        configService->get("mysqlConnectionPoolSize", poolSize);

        static auto ins = std::make_shared<MySqlDriver> (
                configService->get("mysqlUrlConnection"),
                configService->get("mysqlUsername"),
                configService->get("mysqlPassword"),
                configService->get("mysqlSchema"),
                poolSize);
        return ins;
}
