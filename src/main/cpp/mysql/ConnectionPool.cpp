

#include "ConnectionPool.h"
#include "RandomUtil.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "ConnectionHolder.h"
#include <boost/thread.hpp>
ConnectionPool::ConnectionPool(std::string urlConnection,
                               std::string username,
                               std::string password,
                               std::string schema,
                               int poolSize)   : Object(__FILE__) {

        mutex = std::make_shared<boost::shared_mutex> ();
        this->urlConnection = urlConnection;
        this->username = username;
        this->password= password;
        this->schema = schema;
        this->minRequiredPoolSize = poolSize;
        randomUtil = std::make_shared<RandomUtil>(poolSize);
        //you cant delete this driver instance
        driver = get_driver_instance ();

        initializeThePool();

}

void ConnectionPool::releaseConnection(std::shared_ptr<ConnectionHolder> connection) {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access

        connection->markFree();
        connection->resetConnection();
}

ConnectionPool::~ConnectionPool() {

}

void ConnectionPool::addConneciton(std::shared_ptr<ConnectionHolder> connection) {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access

        connection->markFree();
        pool.push_back(connection);
        connection->setIndexInPool(pool.size() - 1);

}

std::shared_ptr<ConnectionHolder> ConnectionPool::getConnectionFromPool() {
        auto startedGettingAConnection = DateTimeUtil::getNowInSecond();
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access

        if(pool.size() < minRequiredPoolSize) {
                // addConneciton();
                LOG(ERROR) << "pool shrinking.... actual size : "
                           << pool.size() << " vs expected : "<< minRequiredPoolSize;
        }


        while(true) {
                if (pool.empty()) {
                        initializeThePool();
                        return getConnectionFromPool();
                }
                int indexOfConnection = randomUtil->randomNumberV2();
                if (indexOfConnection > pool.size()) {
                        LOG(ERROR) << "index larger that size, "<<indexOfConnection << " vs "<< pool.size();
                        indexOfConnection = RandomUtil::sudoRandomNumber(pool.size() - 1);
                }
                std::shared_ptr<ConnectionHolder> connection = pool.at(indexOfConnection);

                auto now = DateTimeUtil::getNowInSecond();
                int ageInSecond = (now - connection->getLastTimeAcquired());
                //if a connection has been taken more than 30 seconds ago, we re use it
                //TODO : make this config driven
                //  ||  ageInSecond > 30
                if (!connection->isBusy()) {
                        if (!connection->getConnection()->isValid()) {
                                //connection is not valid
                                //we remove the connection from the pool and try again
                                LOG(ERROR)<<"connection is not valid...deleting connection";
                                pool.erase(pool.begin() + indexOfConnection);
                                continue;

                        }
                        connection->markBusy();
                        return connection;
                }

                LOG_EVERY_N(ERROR, 1000)<<
                        google::COUNTER<<
                        "nth couldnt acquire a connection, poolSize : "<< pool.size();
                if (now - startedGettingAConnection > 30 ) {
                        throwEx("couldnt acquire a connection");
                }
        }
}


void ConnectionPool::initializeThePool() {
        MLOG(3) << "trying to connect to mysql";
        assertAndThrow(!urlConnection.empty());
        assertAndThrow(!username.empty());
        assertAndThrow(!password.empty());
        assertAndThrow(!schema.empty());
        MLOG(3) << "connecting to database, urlConnection: " << urlConnection <<
                " username: "<< username << " , schema : "<< schema <<
                " , poolSize : "<< minRequiredPoolSize;

        for(int i=0; i < minRequiredPoolSize; i++) {
                std::shared_ptr<ConnectionHolder> connectionHolder = std::make_shared<ConnectionHolder>(
                        driver,
                        urlConnection,
                        username,
                        password,
                        schema
                        );
                connectionHolder->setIndexInPool(i);
                addConneciton(connectionHolder);
        }

        MLOG(3) << "created a connection pool to mysql successfuly";
}
