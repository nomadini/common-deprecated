//
// Created by Mahmoud Taabodi on 6/6/16.
//

#ifndef COMMON_POSTGRESSQL_H
#define COMMON_POSTGRESSQL_H



#include <vector>
#include <memory>
#include <iostream>
#include <pqxx/pqxx>

class PostgresSqlDriver {

public :

    pqxx::connection *con;

    std::string username;
    std::string password;
    std::string schema;
    std::string hostAddress;
    std::string port;

    PostgresSqlDriver(const std::string& hostAddress,
                const std::string& port,
                const std::string& username,
                const std::string& password,
                const std::string& schema);

    virtual void connectToDb();

//    virtual void logTheErrorAndThrow(sql::SQLException &e);
			throwEx("error in mysql");

//
//    int getLastInsertedId();
//
//    virtual void executedUpdateStatement(std::string query);

    virtual ~PostgresSqlDriver();
};

#endif //COMMON_POSTGRESSQL_H
