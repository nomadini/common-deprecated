#Bidder Wiki


##Bidder Architecture
Bidder has four pipelines that execute all the logic in it
1.BidRequestHandlerPipelineProcessor
2.BidderMainPipelineProcessor
3.BidderAsyncPipelineProcessor
4.DataReloadPipeline

### BidRequestHandlerPipelineProcessor
it runs a series of modules that convert the exchange request to the common context
and runs the BidderMainPipelineProcessor, and sends back the response that is the result of
all operations to the exchange.

### BidderMainPipelineProcessor
it runs a series of modules to determine if a request is eligible for bidding or not


### BidderAsyncPipelineProcessor
it reads data from a queue and runs a series of modules that are not required to be executed
during bid time


##BidRequestHandlerPipelineProcessor modules
* throttlerModule
* noBidModeEnforcerModule
* bidProbabilityEnforcerModule
* contextPopulatorModule
* processorInvokerModule
* bidResponseCreatorModule


##BidderMainPipelineProcessor modules
*  asyncPipelineFeederModule
*  lastXSecondSeenVisitorModule
*  lastXSecondSeenIpVisitorModule
*  badIpByCountFilterModule
*  badDeviceByCountFilterModule
  ###Targeting Modules
*  gicapodsIdToExchangeIdMapModule
*  globalWhiteListModule
*  deviceIdSegmentModule
*  targetGroupFilterModule
*  targetGroupFrequencyCapModule
*  targetGroupGeoSegmentFilter
*  targetGroupBudgetFiltersModule
*  bidPriceCalculatorModule
*  targetGroupPerMinuteBidCapFilter
*  targetGroupSelectorModule
  ###Post Bid Modules
*  mgrsBuilderModule
*  eventLogCreatorModule
*  bidderResponseModule



##BidderAsyncPipelineProcessor modules
*  enqueueForScoringModule
*  badIpByCountPopulatorModule
*  badDeviceByCountPopulatorModule
*  bidEventRecorderModule
*   ###Sampled Modules
* featureDeviceHistoryUpdaterModuleWrapper
*  geoFeatureDeviceHistoryUpdaterModuleWrapper
*  visitFeatureDeviceHistoryUpdaterModuleWrapper
*  deviceGeoFeatureUpdaterModuleWrapper
*  crossWalkUpdaterModuleWrapper


##DataReloadPipeline modules
*  activeFilterModule
*  targetGroupPacingByBudgetModule
*  targetGroupPacingByImpressionModule
*  campaignImpressionCappingFilterModule(commented for now)
*  campaignBudgetCappingFilterModule(commented for now)
*  targetGroupImpressionCappingFilterModule
*  targetGroupBudgetCappingFilterModule
*  targetGroupWithoutCreativeFilterModule
*  targetGroupWithoutSegmentFilterModule
