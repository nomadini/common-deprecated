
#how to create a new app
  1.create the files for it
  2.create the CMakeLists for it and link it and commit it in the remote server
  3.create properties for it and link it(the directory is already linked)
  4.add the entry to allAutoBuild.sh file


Commands for creating symlinks

sudo ln -s /home/vagrant/workspace/Common/Config/ /var/data/conf

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-PropertyServer.txt   /home/vagrant/workspace/Common/PropertyServer/src/build/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-MetricHealthChecker.txt   /home/vagrant/workspace/Common/MetricHealthChecker/src/build/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-PlaceFinderApp.txt   /home/vagrant/workspace/Common/PlaceFinderApp/src/build/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-KafkaHttpProxy.txt   /home/vagrant/workspace/Common/KafkaHttpProxy/src/build/Cmake/CMakeLists.txt



sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-MaxmindHttpProxy.txt   /home/vagrant/workspace/Common/MaxmindHttpProxy/src/build/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/allAutoBuild.sh /home/vagrant/workspace/Common/allAutoBuild.sh
sudo ln -s /home/vagrant/workspace/Common/Build/asyncAllAutoBuild.sh /home/vagrant/workspace/Common/asyncAllAutoBuild.sh
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Bidder-Unit-Tests.txt /home/vagrant/workspace/Bidder/Tests/Unit/Cmake/CMakeLists.txt
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Scorer-Tests.txt /home/vagrant/workspace/Scorer/Tests/CMakeLists.txt
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Common-Tests.txt /home/vagrant/workspace/Common/src/test/cpp/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-TargetGroupProject-Tests.txt /home/vagrant/workspace/targetgroup/src/test/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-ActionRecorder.txt /home/vagrant/workspace/ActionRecorder/CMakeLists.txt
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Modeler.txt /home/vagrant/workspace/Modeler/CMakeLists.txt
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-pixelclusterfinder.txt /home/vagrant/workspace/pixelclusterfinder/CMakeLists.txt
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-targetgroup.txt /home/vagrant/workspace/targetgroup/CMakeLists.txt
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-openrtbmodels.txt /home/vagrant/workspace/openrtbmodels/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-openrtbmodels-tests.txt /home/vagrant/workspace/openrtbmodels/OpenRtbModels/src/test/Cmake/CMakeLists.txt

rm /home/vagrant/workspace/Scorer/CMakeLists.txt && sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Scorer.txt /home/vagrant/workspace/Scorer/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Bidder-Unit-Tests.txt /home/vagrant/workspace/Bidder/Tests/Unit/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-BidderForwarder.txt /home/vagrant/workspace/BidderForwarder/CMakeLists.txt
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Bidder.txt /home/vagrant/workspace/Bidder/CMakeLists.txt
sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Bidder-Filters.txt
/home/vagrant/workspace/Bidder/Bidder-1.0/Modules/Filters/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Build/CMakeLists-Bidder-Modules.txt /home/vagrant/workspace/Bidder/Bidder-1.0/Modules/Cmake/CMakeLists.txt

sudo ln -s /home/vagrant/workspace/Common/Config/ /var/data/conf

#How to easily commit different projects

use this command from your workspace : bash Common/pushAll.sh "this is my commit"

#How to Run Tests ?


##Common Tests
/home/vagrant/workspace/Common/src/test/cpp/Cmake/CommonTests.out
/home/vagrant/workspace/Common/src/test/cpp/Cmake/CommonTests.out  --gtest_filter="*testWritingAndReadingAerospikeRecords*"

/home/vagrant/workspace/Common/src/test/cpp/Cmake/CommonTests.out  --gtest_filter="*testWritingVisitHistoryInMultipleThreads*"
/home/vagrant/workspace/Common/src/test/cpp/Cmake/CommonTests.out  --gtest_filter="*testHistogram*"
/home/vagrant/workspace/Common/src/test/cpp/Cmake/CommonTests.out   --gtest_filter="*testFilteringStructure*"
/home/vagrant/workspace/Common/src/test/cpp/Cmake/CommonTests.out   --gtest_filter="*testReloadingCaches*"

 /home/vagrant/workspace/targetgroup/src/test/Cmake/TargetGroupTests.out   --gtest_filter="TargetGroupRecencyModelMapCacheServiceTest.testReloadingCaches"

##OpenRtb Tests
/home/vagrant/workspace/openrtbmodels/OpenRtbModels/src/test/Cmake/MyOpenRtbModelTests.out



#Nomadini JIRA link
https://nodamini.atlassian.net/projects/BT/issues/BT-64?filter=allopenissues
https://nodamini.atlassian.net/browse/MOD-3
https://nodamini.atlassian.net/browse/MAN-40
https://bitbucket.org/nomadini/modeler
https://nodamini.atlassian.net/projects/BT/issues/BT-64?filter=allopenissues



Research this!! and Fb allocator for small objects
As I mention here, I've seen Intel TBB's custom STL allocator significantly improve performance of a multithreaded app simply by changing a single

std::vector<T>
to
std::vector<T,tbb::scalable_allocator<T> >
(this is a quick and convenient way of switching the allocator to use TBB's nifty thread-private heaps; see page 7 in this document)
