# BidderForwarder

###Dashboard
dashboard is located at : http://67.205.132.35:9980/dashboard/

-how to run app in background without nohup

nohup BidderForwarder/BidderForwarder.out >/dev/null 2>&1 &

get the metrics for bidder forwarder :
select module , state, sum(value) from metric where appname like  'BidderForwarder' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 2 MINUTE)  group by state;
