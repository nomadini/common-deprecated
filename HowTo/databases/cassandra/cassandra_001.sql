
    DROP TABLE gicapods_mdl.IpToDeviceIdsMap;
		select * from  gicapods_mdl.IpToDeviceIdsMap;
    ALTER TABLE gicapods_mdl.IpToDeviceIdsMap ADD devicetype text;

    CREATE TABLE gicapods_mdl.IpToDeviceIdsMap (
        ip text,
        timeofvisit timestamp,
        deviceid text,
        devicetype text,
        PRIMARY KEY (ip , timeofvisit)
    ) WITH CLUSTERING ORDER BY (timeofvisit DESC)
        AND bloom_filter_fp_chance = 0.01
        AND caching = '{"keys":"ALL", "rows_per_partition":"NONE"}'
        AND comment = ''
        AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy'}
        AND compression = {'sstable_compression': 'org.apache.cassandra.io.compress.LZ4Compressor'}
        AND dclocal_read_repair_chance = 0.1
        AND default_time_to_live = 0
        AND gc_grace_seconds = 864000
        AND max_index_interval = 2048
        AND memtable_flush_period_in_ms = 0
        AND min_index_interval = 128
        AND read_repair_chance = 0.0
        AND speculative_retry = '99.0PERCENTILE';



DROP TABLE gicapods_mdl.DeviceIdToIpsMap;
CREATE TABLE gicapods_mdl.DeviceIdToIpsMap (
    deviceid text,
    timeofvisit timestamp,
    ip text,
    PRIMARY KEY (deviceid , timeofvisit)
) WITH CLUSTERING ORDER BY (timeofvisit DESC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = '{"keys":"ALL", "rows_per_partition":"NONE"}'
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy'}
    AND compression = {'sstable_compression': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99.0PERCENTILE';
