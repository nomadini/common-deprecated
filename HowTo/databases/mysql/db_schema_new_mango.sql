

ALTER TABLE gicapods_test.targetgroup_device_type_target_map CHANGE HOURS DEVICE_TYPE_TARGETS varchar(2048);

DROP TABLE `metric`;
CREATE TABLE `metric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(200) NOT NULL,
  `appname` varchar(200) NOT NULL,
  `appInstanceUniqueName` varchar(90) NOT NULL,
  `value` decimal(13,3) NOT NULL,
  `module` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `entity` varchar(200) NOT NULL,
  `timeOfRecord` datetime NOT NULL,
  `domain` varchar(200) NOT NULL,
  `level` varchar(200) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`));


drop table targetgroup_device_type_target_map;

CREATE TABLE `targetgroup_device_type_target_map` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TARGET_GROUP_ID` int(11) NOT NULL,
  `DEVICE_TYPE_TARGETS` varchar(2048) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TARGET_GROUP_ID_UNIQ_IDX` (`TARGET_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


drop table targetgroup_day_time_target_map;
CREATE TABLE `targetgroup_day_time_target_map` (
`ID` int(11) NOT NULL AUTO_INCREMENT,
 `TARGET_GROUP_ID` int(11) NOT NULL,
 `HOURS` varchar(2048) NOT NULL,
 `created_at` datetime NOT NULL,
 `updated_at` datetime NOT NULL,
 PRIMARY KEY (`ID`),
 UNIQUE KEY `TARGET_GROUP_ID_UNIQ_IDX` (`TARGET_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 ALTER TABLE creative MODIFY COLUMN `AD_TYPE` varchar(100) NOT NULL;
 ALTER TABLE creative MODIFY COLUMN `API` varchar(200) NOT NULL;




CREATE TABLE `global_whitlisted_domains` (
  `id` int(10)  NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
);


CREATE TABLE targetgroup_realtimeinfo_hourly (
    id int(10) NOT NULL AUTO_INCREMENT,
    targetgroupid int(10)  NOT NULL,
    dayUpdated timestamp,
    hourUpdated timestamp,
    uniqueBidderPacerId varchar(100),

    impressionShownInHour int(10),
    platformBudgetSpentInHour DECIMAL(7,3),
    PRIMARY KEY (`id`)
);


DROP TABLE `gicapods_test`.`counters_to_watch`;
 CREATE TABLE `gicapods_test`.`counters_to_watch` (
       `id` INT NOT NULL AUTO_INCREMENT,
       `hostname` VARCHAR(45) NOT NULL,
       `appname` VARCHAR(45) NOT NULL,
       `category` VARCHAR(45) NOT NULL,
       `module` VARCHAR(45) NOT NULL,
       `state` VARCHAR(45) NOT NULL,
       `entity` VARCHAR(45) NOT NULL,
       `interval_to_watch_in_minute` INT NOT NULL,
       `RED_LIMIT` INT NOT NULL,
       `YELLOW_LIMIT` INT NOT NULL,
       PRIMARY KEY (`id`));

INSERT INTO `gicapods_test`.`counters_to_watch` values(1, 'abc', 'idder', 'ENGINE',
'BidderMainProcessor', 'LastModule-TargetGroupSelectorModule', 'ALL', 100, 1000, 400 );

INSERT INTO `gicapods_test`.`counters_to_watch` values(2, 'abc', 'idder', 'ENGINE',
'BidRequestHandler', 'Exception : NON-US-REQUEST', 'ALL', 100, 200, 10 );


DROP TABLE `gicapods_test`.`TgPacingCountsPersistentDto`;

CREATE TABLE `TgPacingCountsPersistentDto` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `TARGET_GROUP_ID` int(11) NOT NULL,
 `numberOfImpressionsServed` int(11) NOT NULL,
 `platformCostSpent`  decimal(20,8) NOT NULL,
 `HOSTNAME` varchar(30) NOT NULL,
 `TIME_REPORTED` datetime NOT NULL,
 PRIMARY KEY (`id`));


 INSERT INTO `gicapods_test`.`counters_to_watch` values(3, 'abc', 'Bidder', 'ENGINE',
 'BidRequestHandler', 'numberOfBidsThrottled', 'ALL', 5, 20000, 5 );

 INSERT INTO `gicapods_test`.`counters_to_watch` values(3, 'abc', 'Bidder', 'ENGINE',
 'BidRequestHandler', 'numberOfValidBidsResponses', 'ALL', 5, 20000, 5 );



ALTER TABLE counters_to_watch ADD COLUMN `function` varchar(30) NOT NULL;
ALTER TABLE targetgroup ADD COLUMN `place_tag` text NOT NULL DEFAULT '';

ALTER TABLE modelresult ADD COLUMN `process_result` text NOT NULL DEFAULT '';

update counters_to_watch set function='sum';

ALTER TABLE impression ADD COLUMN `count` int(8) NOT NULL;

INSERT INTO `gicapods_test`.`counters_to_watch` values(4, 'abc', 'Bidder', 'ENGINE',
   'BidRequestHandler', 'numberOfValidBidsResponses', 'ALL', 5, 20000, 5, 'sum' );

INSERT INTO `gicapods_test`.`counters_to_watch` values(5, 'abc', 'Bidder', 'ENGINE',
'AdServerStatusChecker', 'DO_BID_BECAUSE_ALL_ADSERVERES_ARE_GOOD', 'ALL', 5, 20000, 5, 'sum');

DROP TABLE TgBiddingPerformanceMetric;
CREATE TABLE `TgBiddingPerformanceMetric` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`TARGET_GROUP_ID` int(11) NOT NULL,
`NUM_BIDS` int(11) NOT NULL,
`SUM_BID_PRICES` int(11) NOT NULL,
`HOST_NAME` varchar(50) NOT NULL,
`TIME_REPORTED` datetime NOT NULL,
 PRIMARY KEY (`id`));
UNIQUE KEY `TARGET_GROUP_ID_UNIQ_IDX` (`TARGET_GROUP_ID`, `TIME_REPORTED`,`HOST_NAME` ),

CREATE TABLE `targetgroup_iabmgrs_map` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`targetgroup_id` int(11) NOT NULL,
`mgrs100` varchar(50) NOT NULL,
`updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
UNIQUE KEY `targetgroup_iabmgrs_map_UNIQ_IDX` (`targetgroup_id`, `mgrs100`),
 PRIMARY KEY (`id`));

CREATE TABLE `top_mgrs_list` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`advertiser_id` int(11) NOT NULL,
`mgrs100` varchar(50) NOT NULL,
`updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
UNIQUE KEY `advertiser_id_map_UNIQ_IDX` (`advertiser_id`, `mgrs100`),
 PRIMARY KEY (`id`));



DROP TABLE TargetGroupWinningPerformanceMetricReportedByAdServer;
CREATE TABLE `TargetGroupWinningPerformanceMetricReportedByAdServer` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`TARGET_GROUP_ID` int(11) NOT NULL,
`NUM_WINS_TILL_MINUTE` int(11) NOT NULL,
`NUM_WINS_IN_HOUR` int(11) NOT NULL,
`NUM_WINS_IN_DAY` int(11) NOT NULL,
`HOSTNAME` varchar(30) NOT NULL,
`MINUTE_REPORTED` datetime NOT NULL,
`HOUR_REPORTED` datetime NOT NULL,
`DATE_REPORTED` datetime NOT NULL,
UNIQUE KEY `TARGET_GROUP_ID_UNIQ_IDX` (`TARGET_GROUP_ID`, `DATE_REPORTED`,`HOUR_REPORTED`, `MINUTE_REPORTED`, `HOSTNAME` ),
 PRIMARY KEY (`id`));

 INSERT INTO `gicapods_test`.`counters_to_watch` values(6, 'abc', 'Bidder', 'ENGINE',
    'BidRequestHandler', 'latency', 'ALL', 5, 20000, 5, 'avg' );


DROP TABLE TargetGroupFilterCountDbRecord;
CREATE TABLE `TargetGroupFilterCountDbRecord` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`TARGET_GROUP_ID` varchar(30) NOT NULL,
`FILTERNAME` varchar(50) NOT NULL,
`NUM_FAILURES` int(11) NOT NULL,
`NUM_PASSES` int(11) NOT NULL,
`NUM_TOTAL` int(11) NOT NULL,
`NUM_TOTAL_REQUEST` int(11) NOT NULL,
`HOSTNAME` varchar(30) NOT NULL,
`APP_VERSION` varchar(30) NOT NULL,
`PERC_FAILURES` decimal(6,2) NOT NULL,
`TIME_REPORTED` datetime NOT NULL,
 PRIMARY KEY (`id`));

 insert into TgPacingCountsPersistentDto(id, TARGET_GROUP_ID, numberOfImpressionsServed, platformCostSpent, HOSTNAME,TIME_REPORTED) values(28,1,13,14.00,'asd', '2016-09-17 17:01:49');

   update advertiser set domain_name = '["khybsiwb.com"]';


  ALTER TABLE targetgroup ADD COLUMN `NumberOfBidsLimitInInADayPerBidder` int(11) NOT NULL DEFAULT 100000;
  ALTER TABLE targetgroup ADD COLUMN `NumberOfBidsLimitInLastHourPerBidder` int(11) NOT NULL DEFAULT 10000;
  ALTER TABLE targetgroup ADD COLUMN `NumberOfBidsLimitInLastPacingPeriodPerBidder` int(11) NOT NULL DEFAULT 10000;
  ALTER TABLE targetgroup ADD COLUMN `NumberOfBidsAllowedPerMinute` int(11) NOT NULL DEFAULT 100;


ALTER TABLE impression ADD COLUMN `count` int(8) NOT NULL DEFAULT 1;
ALTER TABLE iab_category ADD COLUMN iabStandardName varchar(12);
ALTER TABLE iab_sub_category ADD COLUMN iabStandardName varchar(12);

UPDATE iab_category set iabStandardName = 'IAB1';
UPDATE iab_sub_category set iabStandardName = 'IAB-1';

update targetgroup set pacing_plan = "";
  update targetgroup set ad_position = '["ANY"]' where ad_position='';
    update targetgroup set ad_position = '["ANY"]' where ad_position = 'null';
update targetgroup set ad_position = '["ANY"]' where ad_position='Any';


update creative set attributes = '["Audio_Ad_Auto_Play"]';
update creative set api = '["MRAID-1","ORMMA"]';
update creative set advertiser_domain_name = '["aaaa1s.com"]'; //TODO :: add a task in jira for this!! to make it like advertiser page
update creative set ad_type = 'JAVASCRIPT';

update bwlist set list_type = 'blacklist' where list_type = 'black'; //TODO : change this in mango
update bwlist set list_type = 'whitelist' where list_type = 'white';

DROP TABLE targetgroup_device_type_target_map;
  CREATE TABLE `targetgroup_device_type_target_map` (
    `ID` int(11) NOT NULL AUTO_INCREMENT,
    `TARGET_GROUP_ID` int(11) NOT NULL,
    `DEVICE_TYPE_TARGETS` varchar(2048) DEFAULT NULL,
    `created_at` datetime NOT NULL,
    `updated_at` datetime NOT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `TARGET_GROUP_ID_UNIQ_IDX` (`TARGET_GROUP_ID`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;


  update advertiser set created_at = UTC_TIMESTAMP()\G
update targetgroup set pacing_plan = '{"timeZone":"America/New_York","hourPercentages":[{"23":100}]}';
update targetgroup set status='Active';
update targetgroup set start_date='2016-08-29 15:30:00', end_date='2016-12-29 15:30:00';

  DROP  TABLE `global_whitlisted_domains`;
    CREATE TABLE `global_whitlisted_domains` (
      `id` int(10)  NOT NULL AUTO_INCREMENT,
      `domain_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `created_at` timestamp NOT NULL,
      `updated_at` timestamp NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `domain_name_UNIQ_IDX` (`domain_name`)
    );

insert into global_whitlisted_domains values(1, 'bbc.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(2, 'cnn.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(3, 'foxnews.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(4, 'sportcenter.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(5, 'toysrus.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(6, 'babysrus.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into global_whitlisted_domains values(7, 'balatarin.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(8, 'facebook.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(9, 'twitter.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(10, 'pokemon.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());
insert into global_whitlisted_domains values(11, 'immigration.com',UTC_TIMESTAMP(), UTC_TIMESTAMP());

update creative set size='320x150' where size='231x2321';
update creative set size='320x90' where size='23x23';
update creative set size='180x150' where size='25x65';

delete from targetgroup_device_type_target_map;
insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (1,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (2,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (3,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (13,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (20,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (21,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (26,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (27,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (28,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());


insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (29,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (30,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (31,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());


insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (4,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

insert into targetgroup_device_type_target_map(TARGET_GROUP_ID, DEVICE_TYPE_TARGETS, created_at, updated_at)
values (22,'["MOBILE_PHONE", "TABLET ", "DESKTOP"]', UTC_TIMESTAMP(), UTC_TIMESTAMP());

update targetgroup_bidhour_map set hours='{"1":["1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"],"2":["1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"],"3":["1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"],"4":["1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"],"5":["1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"],"6":["1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"],"7":["1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"]}' ;


CREATE TABLE `TgWinningPerformanceMetricDto` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`TARGET_GROUP_ID` int(11) NOT NULL,
`NUM_WINS` int(11) NOT NULL,
`PLATFORM_COST_SPENT` decimal(10,2) NOT NULL,
`HOSTNAME` varchar(30) NOT NULL,
`TIME_REPORTED` datetime NOT NULL,
 PRIMARY KEY (`id`));

 CREATE TABLE `TgPacingCountsPersistentDto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TARGET_GROUP_ID` int(11) NOT NULL,
  `numberOfImpressionsServed` int(11) NOT NULL,
  `platformCostSpent`  decimal(20,8) NOT NULL,
  `HOSTNAME` varchar(30) NOT NULL,
  `TIME_REPORTED` datetime NOT NULL,
  PRIMARY KEY (`id`));

  DROP TABLE TgBiddingPerformanceMetric;
  CREATE TABLE `TgBiddingPerformanceMetric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TARGET_GROUP_ID` int(11) NOT NULL,
  `NUM_BIDS` int(11) NOT NULL,
  `SUM_BID_PRICES` int(11) NOT NULL,
  `HOST_NAME` varchar(50) NOT NULL,
  `TIME_REPORTED` datetime NOT NULL,
  UNIQUE KEY `TARGET_GROUP_ID_UNIQ_IDX` (`TARGET_GROUP_ID`, `TIME_REPORTED`,`HOST_NAME` ),
   PRIMARY KEY (`id`));


select tg.ID as tgId ,
numberOfBids,
sumOfBidPrices,
numberOfWins,
platformCostSpentByAdServer,
numberOfImpressionsServedByPacer,
platformCostSpentByPacer
 from targetgroup tg
 left join (
   SELECT TARGET_GROUP_ID,
          sum(NUM_WINS) as numberOfWins,
          sum(PLATFORM_COST_SPENT) as platformCostSpentByAdServer
          FROM TgWinningPerformanceMetricDto WHERE TIME_REPORTED > DATE_SUB(UTC_TIMESTAMP, INTERVAL 1 DAY) group by TARGET_GROUP_ID) tgw on tg.ID = tgw.TARGET_GROUP_ID

 left join (
   SELECT TARGET_GROUP_ID,
          sum(NUM_BIDS) as numberOfBids,
          sum(SUM_BID_PRICES) as sumOfBidPrices
          FROM TgBiddingPerformanceMetric WHERE TIME_REPORTED > DATE_SUB(UTC_TIMESTAMP, INTERVAL 1 DAY) group by TARGET_GROUP_ID)  tgb on tg.ID = tgb.TARGET_GROUP_ID

          left join (
            SELECT TARGET_GROUP_ID,
                   sum(numberOfImpressionsServed) as numberOfImpressionsServedByPacer,
                   sum(platformCostSpent) as platformCostSpentByPacer
                   FROM TgPacingCountsPersistentDto WHERE TIME_REPORTED > DATE_SUB(UTC_TIMESTAMP, INTERVAL 1 DAY) group by TARGET_GROUP_ID)  tgp on tg.ID = tgp.TARGET_GROUP_ID
 group by tg.ID ;



insert into TgPacingCountsPersistentDto(TARGET_GROUP_ID, numberOfImpressionsServed, platformCostSpent, HOSTNAME,TIME_REPORTED) values(31,13,14.00,'asd', '2016-09-28 17:01:49');
insert into TgBiddingPerformanceMetric(TARGET_GROUP_ID, NUM_BIDS, SUM_BID_PRICES, HOST_NAME,TIME_REPORTED) values(31,13,14.00,'asd', '2016-09-28 17:01:49');
insert into TgWinningPerformanceMetricDto(TARGET_GROUP_ID, NUM_WINS, PLATFORM_COST_SPENT, HOSTNAME,TIME_REPORTED) values(31,13,14.00,'asd', '2016-09-28 17:01:49');


delete from TgWinningPerformanceMetricDto;
delete from TgBiddingPerformanceMetric;
delete from TgPacingCountsPersistentDto;


DROP TABLE `new_mango`.`exchangeSyncedIds`;
  CREATE TABLE `new_mango`.`exchangeSyncedIds` (
    `ID` INT NOT NULL,
    `exchangeName` VARCHAR(100) NOT NULL,
    `exchangeId` VARCHAR(100) NOT NULL,
    `nomadiniDeviceId` VARCHAR(100) NOT NULL,
    `CREATED_AT` datetime NOT NULL,
    UNIQUE INDEX `exchangeId_UNIQUE` (`exchangeId` ASC));


ALTER TABLE TargetGroupFilterCountDbRecord MODIFY FILTERNAME text NOT NULL;
ALTER TABLE TargetGroupFilterCountDbRecord MODIFY HOSTNAME text NOT NULL;
ALTER TABLE TargetGroupFilterCountDbRecord MODIFY APP_VERSION text NOT NULL;

ALTER TABLE model CHANGE positive_offer_id positive_offer_ids text  NOT NULL;
ALTER TABLE model CHANGE negative_offer_id negative_offer_ids text  NOT NULL;
ALTER TABLE model CHANGE description description text  NOT NULL;
ALTER TABLE model CHANGE segment_name_seed segment_name_seed text  NOT NULL;
ALTER TABLE model CHANGE name name varchar(1000)  NOT NULL;

ALTER TABLE model CHANGE feature_score_map feature_score_map longtext  NOT NULL;
ALTER TABLE model CHANGE top_feature_score_map top_feature_score_map longtext  NOT NULL;
ALTER TABLE model CHANGE parent_morel_request_id parent_model_request_id int(11) NOT NULL;

ALTER TABLE TargetGroupFilterCountDbRecord change FILTERNAME FILTER_NAME text NOT NULL;
ALTER TABLE TargetGroupFilterCountDbRecord ADD COLUMN FILTER_TYPE text NOT NULL;

ALTER TABLE model ADD COLUMN parent_morel_request_id int(11);
ALTER TABLE model DROP COLUMN ORIGINAL_MODEL_REQUEST_ID;
ALTER TABLE model CHANGE date_of_request_completion date_of_request_completion timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `model` ADD UNIQUE `unique_name`(`name`, `advertiser_id`);
ALTER TABLE segment ADD CONSTRAINT uc_name UNIQUE (name,advertiser_id, model_id);

ALTER TABLE pixel DROP COLUMN part_b;
ALTER TABLE pixel CHANGE part_a unique_key text  NOT NULL;

ALTER TABLE impression MODIFY latitude decimal(9,6) NOT NULL;
ALTER TABLE impression MODIFY longitude decimal(9,6) NOT NULL;


ALTER TABLE impression ADD COLUMN `deviceCountry` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `deviceState` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `deviceCity` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `deviceZipcode` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `adSize` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `siteDomain` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `siteCategory` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `impressionType` varchar(64) NOT NULL;


ALTER TABLE impression ADD COLUMN `exchangeCostInCpm` decimal(9,6) NOT NULL;
ALTER TABLE impression ADD COLUMN `platformCostInCpm`  decimal(9,6) NOT NULL;
ALTER TABLE impression ADD COLUMN `advertiserCostInCpm`  decimal(9,6) NOT NULL;
ALTER TABLE impression MODIFY exchangeCostInCpm decimal(20,4) NOT NULL;
ALTER TABLE impression MODIFY platformCostInCpm decimal(20,4) NOT NULL;
ALTER TABLE impression MODIFY advertiserCostInCpm decimal(20,4) NOT NULL;


std::string deviceCountry;
std::string deviceState;
std::string deviceCity;
std::string deviceZipcode;
std::string adSize;
std::string siteDomain;
std::string siteCategory;

  ALTER TABLE pixel CHANGE part_a unique_key text  NOT NULL;
ALTER TABLE pixel ADD CONSTRAINT unique_key_name UNIQUE (unique_key(6));

ALTER TABLE segment ADD COLUMN `type` varchar(50) NOT NULL;


CREATE TABLE `deal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exchange_id` int(11) NOT NULL,
  `tp_deal_id` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `deal_UNQIUE_KEY` (`exchange_id`,`tp_deal_id`));

  CREATE TABLE `client_deal_map` (
    `ID` int(11) NOT NULL AUTO_INCREMENT,
    `deal_id` int(11) NOT NULL,
    `client_id` int(11) NOT NULL,
    `status` enum('Active','Inactive') NOT NULL,
    `descr` varchar(255) NOT NULL DEFAULT '',
    `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
    `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `deal_UNQIUE_KEY` (`client_id`,`deal_id`)
  );

  CREATE TABLE `exchange` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `descr` varchar(255) DEFAULT NULL,
   `display_name` varchar(50) DEFAULT NULL,
   `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
   `created_at` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
   `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (`ID`)
 );


 CREATE TABLE `targetgroup_client_deal_map` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `targetgroup_id` int(11) NOT NULL,
   `client_deal_map_id` int(11) NOT NULL,
   `status` enum('Active','Inactive') NOT NULL,
   `created_at` timestamp NOT NULL DEFAULT '2000-01-01 00:00:00',
   `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (`ID`)
 );


 CREATE TABLE `interesting_domains_to_model` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `domain_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
 `domain_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
 `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `domain_name_UNIQ_IDX` (`domain_name`));




 insert into interesting_domains_to_model(
   domain_name, domain_type, status
 ) values (
   'britishairways.com',
   'positive_sample',
   'active'
 );

 select * from global_whitlisted_domains;


 CREATE TABLE `bad_device` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `device_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
 `device_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
 `number_Of_Times_Seen_In_Period` int(10) NOT NULL,
 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `device_id_UNIQ_IDX` (`device_id`, `created_at`));

/*
campaign action takers
*/
DROP TABLE `campaign_offer_map`;
DROP TABLE `targetgroup_offer_map`;
CREATE TABLE `campaign_offer_map` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `offer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `campaign_offer_map_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `campaign_offer_map_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `targetgroup_offer_map` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `targetgroup_id` int(10) unsigned NOT NULL,
  `offer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `targetgroup_offer_map_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `targetgroup_offer_map_targetgroup_id_foreign` FOREIGN KEY (`targetgroup_id`) REFERENCES `targetgroup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);


//this is very important to make queries speed up

ALTER TABLE impression ADD KEY create_at_impression (created_at);


ALTER TABLE campaign ADD COLUMN adv_cost_model text;
ALTER TABLE campaign ADD COLUMN platform_cost_model text;



CREATE TABLE `impression_compressed` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `event_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
 `targetgroup_id` int(10) unsigned NOT NULL,
 `creative_id` int(10) unsigned NOT NULL,
 `campaign_id` int(10) unsigned NOT NULL,
 `advertiser_id` int(10) unsigned NOT NULL,
 `client_id` int(10) unsigned NOT NULL,
 `geosegmentlist_id` int(10) unsigned NOT NULL,
 `count` int(10) unsigned NOT NULL,
 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `latitude` decimal(9,6) NOT NULL,
 `longitude` decimal(9,6) NOT NULL,
 `deviceCountry` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `deviceState` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `deviceCity` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `deviceZipcode` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `adSize` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `siteDomain` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `siteCategory` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `exchangeCostInCpm` decimal(20,4) NOT NULL,
 `platformCostInCpm` decimal(20,4) NOT NULL,
 `advertiserCostInCpm` decimal(20,4) NOT NULL,
 `impressionType` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 `deviceType` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
 PRIMARY KEY (`id`),
 KEY `imp_comp_targetgroup_id_foreign` (`targetgroup_id`),
 KEY `imp_comp_creative_id_foreign` (`creative_id`),
 KEY `imp_comp_campaign_id_foreign` (`campaign_id`),
 KEY `imp_comp_advertiser_id_foreign` (`advertiser_id`),
 KEY `imp_comp_client_id_foreign` (`client_id`),
 KEY `imp_comp_geosegment_id_foreign` (`geosegmentlist_id`),
 KEY `create_at_impression` (`created_at`),
 CONSTRAINT `imp_comp_advertiser_id_foreign` FOREIGN KEY (`advertiser_id`) REFERENCES `advertiser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `imp_comp_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `imp_comp_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `imp_comp_creative_id_foreign` FOREIGN KEY (`creative_id`) REFERENCES `creative` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `imp_comp_geosegment_id_foreign` FOREIGN KEY (`geosegmentlist_id`) REFERENCES `geosegmentlist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `imp_comp_targetgroup_id_foreign` FOREIGN KEY (`targetgroup_id`) REFERENCES `targetgroup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2152908747 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE targetgroup ADD COLUMN TYPE_OF_TARGETING text;

ALTER TABLE impression ADD COLUMN `mgrs1km` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `mgrs100m` varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN `mgrs10m` varchar(64) NOT NULL;

ALTER TABLE impression_compressed ADD COLUMN `mgrs1km` varchar(64) NOT NULL;
ALTER TABLE impression_compressed ADD COLUMN `mgrs100m` varchar(64) NOT NULL;
ALTER TABLE impression_compressed ADD COLUMN `mgrs10m` varchar(64) NOT NULL;

ALTER TABLE metric add column created_at timestamp;


ALTER TABLE impression ADD COLUMN bidderHostname  varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN bidderVersion  varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN adserverHostname  varchar(64) NOT NULL;
ALTER TABLE impression ADD COLUMN adserverVersion  varchar(64) NOT NULL;

ALTER TABLE impression_compressed ADD COLUMN bidderHostname  varchar(64) NOT NULL;
ALTER TABLE impression_compressed ADD COLUMN bidderVersion  varchar(64) NOT NULL;
ALTER TABLE impression_compressed ADD COLUMN adserverHostname  varchar(64) NOT NULL;
ALTER TABLE impression_compressed ADD COLUMN adserverVersion  varchar(64) NOT NULL;

ALTER TABLE TgBiddingPerformanceMetric ADD COLUMN CONFIRMED_WINS_BY_ADSERVERS  int(11) NOT NULL;


DROP TABLE metric;
  CREATE TABLE `metric` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `hostname` varchar(90) NOT NULL,
   `appname` varchar(45) NOT NULL,
   `appInstanceUniqueName` varchar(90) NOT NULL,
   `value` decimal(10,3) NOT NULL,
   `module` varchar(200) NOT NULL,
   `state` varchar(200) NOT NULL,
   `entity` varchar(200) NOT NULL,
   `timeOfRecord` datetime NOT NULL,
   `domain` varchar(90) NOT NULL,
   `level` varchar(90) DEFAULT NULL,
   `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`id`)
     ) ENGINE=InnoDB AUTO_INCREMENT=2408210 DEFAULT CHARSET=latin1;

     DROP TABLE metric_compressed;
     CREATE TABLE `metric_compressed` (
       `id` int(11) NOT NULL AUTO_INCREMENT,
       `hostname` varchar(200) NOT NULL,
       `appname` varchar(200) NOT NULL,
       `appInstanceUniqueName` varchar(90) NOT NULL,
       `value` decimal(13,3) NOT NULL,
       `module` varchar(200) NOT NULL,
       `state` varchar(200) NOT NULL,
       `entity` varchar(200) NOT NULL,
       `timeOfRecord` datetime NOT NULL,
       `domain` varchar(200) NOT NULL,
       `level` varchar(200) DEFAULT NULL,
       `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
       `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
     ) ENGINE=InnoDB AUTO_INCREMENT=111321036 DEFAULT CHARSET=latin1;


       CREATE TABLE `features_under_review` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(256) NOT NULL,
        `type` varchar(45) NOT NULL,
        `status` varchar(20) NOT NULL,
        `iab_category` varchar(20) NOT NULL,
        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=2408210 DEFAULT CHARSET=latin1;

       CREATE TABLE `feature_registry` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(256) NOT NULL,
        `type` varchar(45) NOT NULL,
        `status` varchar(20) NOT NULL,
        `iab_category` varchar(20) NOT NULL,
        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=2408210 DEFAULT CHARSET=latin1;


          CREATE TABLE `recency_model` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `advertiser_id` int(10) unsigned NOT NULL,
            `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `scoreBase` decimal(8,5),
            `feature_score_map` varchar(8192),
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            KEY `recency_model_advertiser_id_foreign` (`advertiser_id`),
            CONSTRAINT `recency_model_advertiser_id_foreign` FOREIGN KEY (`advertiser_id`) REFERENCES `advertiser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
          ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


          CREATE TABLE `advertiser_recency_model_map` (
           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
           `advertiser_id` int(10) unsigned NOT NULL,
           `recency_model_id` int(10) unsigned NOT NULL,
           `status` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
           `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
           `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
           PRIMARY KEY (`id`),
           KEY `advertiser_recency_model_map_advertiser_id_foreign` (`advertiser_id`),
           KEY `advertiser_recency_model_map_model_id_foreign` (`recency_model_id`),
           CONSTRAINT `advertiser_recency_model_map_advertiser_id_foreign` FOREIGN KEY (`advertiser_id`) REFERENCES `advertiser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
           CONSTRAINT `advertiser_recency_model_map_recency_model_id_foreign` FOREIGN KEY (`recency_model_id`) REFERENCES `model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
         ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


         CREATE TABLE `targetgroup_recency_model_map` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `targetgroup_id` int(10) unsigned NOT NULL,
          `recency_model_id` int(10) unsigned NOT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`),
          KEY `targetgroup_recency_model_map_targetgroup_id_foreign` (`targetgroup_id`),
          KEY `targetgroup_recency_model_map_recency_model_id_foreign` (`recency_model_id`),
          CONSTRAINT `targetgroup_recency_model_map_recency_model_id_foreign` FOREIGN KEY (`recency_model_id`) REFERENCES `recency_model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
          CONSTRAINT `targetgroup_recency_model_map_targetgroup_id_foreign` FOREIGN KEY (`targetgroup_id`) REFERENCES `targetgroup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



ALTER TABLE TgBiddingPerformanceMetric CHANGE SUM_BID_PRICES SUM_BID_PRICES decimal(10,5) NOT NULL;


use mango_geo;
ALTER TABLE PLACE CHANGE LAST_MODIFIED LAST_MODIFIED timestamp DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE PLACE ADD COLUMN POLYGON varchar(4096) NOT NULL;



CREATE TABLE targetgroup_realtimeinfo_hourly (
    id int(10) NOT NULL AUTO_INCREMENT,
    targetgroupid int(10)  NOT NULL,
    platformBudgetSpentInHour DECIMAL(7,3),
    PRIMARY KEY (`id`)
);


CREATE TABLE `modelresult` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_request_id` int(10) unsigned NOT NULL,
  `num_neg_devices_used` int(11) NOT NULL,
  `num_pos_devices_used` int(11) NOT NULL,
  `feature_avg_num_history_used` text COLLATE utf8_unicode_ci NOT NULL,
  `negative_feature_used` longtext COLLATE utf8_unicode_ci,
  `positive_feature_used` longtext COLLATE utf8_unicode_ci,
  `feature_score_map` longtext COLLATE utf8_unicode_ci NOT NULL,
  `top_feature_score_map` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_model_request_id` (`model_request_id`)) ENGINE=InnoDB AUTO_INCREMENT=1;


  ALTER TABLE offer CHANGE created_at created_at timestamp DEFAULT CURRENT_TIMESTAMP;
  ALTER TABLE offer ADD COLUMN `action_type` varchar(30) NOT NULL;
  ALTER TABLE offer CHANGE COLUMN action_type action_type varchar(30) NOT NULL DEFAULT 'SITE VISITOR';
  ALTER TABLE offer CHANGE COLUMN action_type audience_type varchar(30) NOT NULL DEFAULT 'SITE VISITOR';



  CREATE TABLE `offer_segment_map` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `offer_id` int(11) NOT NULL,
    `segment_id` int(11) NOT NULL,
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `offer_segment_unique_index` (`offer_id`, `segment_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  ALTER TABLE bwlist CHANGE created_at created_at timestamp DEFAULT CURRENT_TIMESTAMP;

  ALTER TABLE bwlist DROP FOREIGN KEY bwlist_advertiser_id_foreign;
  ALTER TABLE bwlist CHANGE COLUMN advertiser_id client_id  int(10) NOT NULL;
  ALTER TABLE bwlist ADD FOREIGN KEY bwlist_client_id_foreign (client_id)  REFERENCES client(id) ON DELETE CASCADE ON UPDATE CASCADE;




  CREATE TABLE `segment_group` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `advertiser_id` int(11) NOT NULL,
    `name` longtext COLLATE utf8_unicode_ci,
    `descr` longtext COLLATE utf8_unicode_ci,
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  CREATE TABLE `segment_group_to_segment_map` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `segment_group_id` int(11) NOT NULL,
    `segment_id` int(11) NOT NULL,
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  ALTER table segment_group ADD FOREIGN KEY segment_group_advertiser_id_foreign(advertiser_id) REFERENCES advertiser(id) ON DELETE CASCADE ON UPDATE CASCADE;
  ALTER table segment_group_to_segment_map ADD COLUMN rel_type  varchar(20) NOT NULL;


  CREATE TABLE `pixel_stats` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `pixel_id` int(11) NOT NULL,
    `hits` int(11) NOT NULL,
    `hit_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `pixel_cluster_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pixel_id` int(11) NOT NULL,
  `cluster` varchar(40) NOT NULL,
  `cluster_type` varchar(40) NOT NULL,
  `score` decimal(10,3) NOT NULL,
  `score_type` varchar(40) NOT NULL,
  `hit_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `interest_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(96) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  insert into interest_type( `name`) VALUES ('crafted_audience');
  insert into interest_type( `name`) VALUES ('public_location_crafted');

    DROP TABLE IF EXISTS interest;
    CREATE TABLE IF NOT EXISTS interest (
      id int(11) NOT NULL AUTO_INCREMENT,
      parent_id int(11) DEFAULT '0',
      name varchar(96) DEFAULT '',
      interest_type int(11) NOT NULL,
      description varchar(1024) DEFAULT '',
      seeds varchar(1024) DEFAULT NULL,
      created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (id)
    ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

  
