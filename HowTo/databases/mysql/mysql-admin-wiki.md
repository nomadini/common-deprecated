#if for some reason you cannot run mysql...after a crash...uninstall mysql-server-5.7 and install it again
#how to setup mysql 5.7 server
first run these to install mysql 5.7
sudo apt-get update
sudo apt-get install mysql-server-5.7


#how to enable remote access for root user in a host

then change the bind-address to 0.0.0.0 in this file to make it listen to remote connections
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf

then give permission to these root users for hosts in mysql shell

```
mysql -hlocalhost -uroot -pmahin
CREATE USER 'root'@'67.205.132.35' IDENTIFIED BY 'mahin';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'67.205.132.35' WITH GRANT OPTION;
FLUSH PRIVILEGES;

CREATE USER 'root'@'pool-96-246-176-132.nycmny.fios.verizon.net'  IDENTIFIED BY 'mahin';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'pool-96-246-176-132.nycmny.fios.verizon.net' WITH GRANT OPTION;
FLUSH PRIVILEGES;

CREATE USER 'root'@'69.206.241.208' IDENTIFIED BY 'mahin';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'69.206.241.208' WITH GRANT OPTION;

CREATE USER 'root'@'31.56.158.213' IDENTIFIED BY 'mahin';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'31.56.158.213' WITH GRANT OPTION;

then restart mysql
sudo service mysql restart
```
#how to disable invalid value for created_at because of NO_ZERO_IN_DATE
run this command in shell of mysql to change the mode of mysql not to have NO_ZERO_IN_DATE
SET @@session.sql_mode ="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION".


#How to import new_mango ?

```
mysql -hlocalhost -uroot -pmahin
CREATE DATABASE new_mango;
```
mysql -u root -p -h localhost new_mango < ~/new_mango_feb_27.sql

#How to dump mysql database without  data
mysqldump -u root -p new_mango > /root/dump_new_mango_August25_2017.sql

#How to dump mysql database with data
mysqldump -u root -p --all-databases > /root/dump_new_mango_April26_2017.sql
mysqldump -u root -p --all-databases > /root/dump_new_mango_August25_2017.sql


#How to dump mysql database only a table
mysqldump -u root -p new_mango impression_compressed > /root/dump_new_mango_impression_compressed_April26_2017.sql


#how to get size of tables in mysql
SELECT
     table_schema as `Database`,
     table_name AS `Table`,
     round(((data_length + index_length) / 1024 / 1024), 2) `Size in MB`
FROM information_schema.TABLES
ORDER BY (data_length + index_length) DESC;
