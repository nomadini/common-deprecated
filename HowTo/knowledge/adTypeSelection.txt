What is it?
Ability to change adType based on bid request. Basically dynamically serving different adTypes for display devices or mobile devices.

How does it work?
Inspect parsed deviceClass(based on userAgent) to determine adType for the bid response.
If we receive bid from mobile devices than adType will be JS vs. other devices adType will be IFRAME.
