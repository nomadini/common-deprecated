Audience Mapping
Skip to end of metadata
Created by Jillian Katz, last modified on Oct 19, 2016 Go to start of metadata
Overview
Below is the audience mapping for all audience types we have set up in our system.
Good To Know
The Parent ID refers to how we categorize audiences for KPI monitoring and bid profile settings
The Optimus ID refers to how we consider spends for Optimus spend allocations.

Parent ID	Parent Audience
       10	AT
       20	NN
       30	CTRL
       40	3P
       50	RSCRH
       54	ATM6
       60	RTB
      100	UNKNOWN
Parent ID	Optimus ID	Audience Code	Child Audience	Child Audience ID
       10	      10	AT        	Action Taker                	10
       10	      10	SV        	Site Visitor                	51
       10	      10	ACQ        	Buyer                        	52
       10	      10	CLK        	Clicker                      	53
       10	      54	AT_M6      	AT_M6                        	54
       10	      10	AT_COMBO  	Action Taker Combo          	55
       10	      10	ATMV      	ATMV                        	56
       10	      10	BK_FP_DATA	BlueKai FP Data              	152
       20	      20	NN        	Network Neighbor            	20
       20	      20	SUMCL      	SUMCL                        	61
       20	      20	AVGCL      	AVGCL                        	62
       20	      20	MV        	Multivariate                	65
       20	      20	NN_COMBO  	Network Neighbor Combo      	67
       20	      20	CLUST      	Landscape Cluster Scheme    	68
       20	      20	L99        	Stochastic Gradient Descent  	69
       20	      20  IP_SEG      IP Segment                    105
       20	      20	XWALK	Crosswalk Infusion	106
       20	      20	HERE_NOW  	Here Now Segment            	104
       20	      20	LOC_INF_AT	Location Infusion Seed      	111
       20	      20	LOC_INF_NN	Location Infusion Modeled NN	112
       20	      20	BK_TP_DATA	BlueKai TP Data              	151
       20	      20	ACXIOM    	Acxiom TP Data              	153
       20	      20	OLB        	Online Buyer Landscapes      	161
       20	      20	SGD        	SGD Prod                    	162
       30	      30	CTRL      	Control Neighbor            	30
       30	      30	CTRLV1    	ControlV1                    	71
       40	      40	TP        	Third Party                  	40
       40	      40	TP_TWTR    	Twitter                      	163
       50	      50	RSCRH      	Research                    	50
       50	      20	OTHER      	Other (EDU, AGE etc)        	92
       50	      20	TRON      	True RON                    	101
       50	      50	MBL_RON    	Mobile RON                  	103
      100	     100	UNKNOWN    	Unknown                      	100
      100	     100	SPD_TMP    	Spend Template              	180
      100	     100	SPD_TMP_DT	Spend Template Default      	181
