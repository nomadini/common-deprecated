#put these in here : vi /etc/aerospike/aerospike.conf
# restart aerospike : sudo /etc/init.d/aerospike restart
namespace rtb {
   replication-factor 2
       memory-size 1G
       default-ttl 30d # 30 days, use 0 to never expire/evict.

       storage-engine memory
}


namespace CrossWalkedDeviceMap {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 4G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}


namespace DeviceSegmentHistory {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 4G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}
namespace EventLog {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 4G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}
namespace AdHistory {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 4G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}

namespace deviceVisitCounts {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 4G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}

namespace Feature {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 4G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}

namespace EventLock {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 4G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}

namespace PlacesCachedResult {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 4G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}

namespace test {
        replication-factor 1
        high-water-memory-pct 60
        memory-size 1G
        default-ttl 1d # 30 days, use 0 to never expire/evict.

        storage-engine memory
}
