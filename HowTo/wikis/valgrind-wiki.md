#What is Valgrind ?
valgrind memcheck detects memory errors, gprof and oprofile help to find performance issues. You cannot use gprof and valgrind at the same time. you cannot use valgrind on a program compiled with -pg

#Run your app like this for memory errors
nohup valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all Bidder/Bidder.out > leakReport.txt 2>&1 &

#Important Notes
1. Having -pg as compiler flags make the apps so slow. (10 times slower)
2. Having -O3 makes the app fastest
3. linking against tcmalloc causes Bidder to crash on operator new
4. when debugging heisengBugs that don't happen in gdb but happens in real run. set this option in gdb  "set disable-randomization off"

#How to find hot spots (cpu% wise in your app)?

install all these tools
sudo apt install linux-tools-common linux-tools-4.4.0-34-generic  linux-cloud-tools-4.4.0-34-generic linux-tools-generic linux-cloud-tools-generic
run your app like this and get a report

perf record --call-graph dwarf DataMaster/DataMaster.out
perf report


#how to get memory usage and cpu usage percentages
 ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head

#how to Alanyze Core dump
## add these to your bash profile
ulimit -c unlimited
echo 'core.%e.%p.%t' | sudo tee /proc/sys/kernel/core_pattern    --> this will put the core files in

gdb Bidder/Bidder ./core
