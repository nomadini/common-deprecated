
#how to setup a cassandra cluster
0. enable firewall on the system
open these ports for cassandra based on : https://docs.datastax.com/en/cassandra/3.0/cassandra/configuration/secureFireWall.html
sudo apt-get install ufw
sudo ufw enable
sudo ufw allow 22
sudo ufw allow 7000
sudo ufw allow 7001
sudo ufw allow 7199
sudo ufw allow 9042
sudo ufw allow 9160
sudo ufw allow 9142
sudo ufw logging on  -->turn on logging  (tutorial --> https://help.ubuntu.com/community/UFW)
sudo ufw status  --> get status of firewall
sudo ufw status numbered --> check the status numbered
sudo ufw delete 1 -->delete number 1 rule
sudo ufw allow from 192.168.0.4 to any port 22 --> how to allow only an IP to connect to port 22

1.
wget http://apache.claz.org/cassandra/3.9/apache-cassandra-3.9-bin.tar.gz
OR
cp Common/thirdParty/apache-cassandra-3.9-bin.tar.gz /tmp/ && cd /tmp/

tar -xvf apache-cassandra-3.9-bin.tar.gz
mv apache-cassandra-3.9 /opt/
//add path to ~/.bash_profile
vi ~/.bash_profile
PATH=$PATH:/opt/apache-cassandra-3.9/bin
export PATH
source ~/.bash_profile

###

VERY IMPORTANT NOTE : in case of any problem, change the root log level to ALL ,
and OTHER log levels in  /opt/apache-cassandra-3.9/conf/logback.xml and checkout the
/opt/apache-cassandra-3.9/logs/debug.log
/opt/apache-cassandra-3.9/logs/system.log

###

//follow this tutorial https://docs.datastax.com/en/cassandra/3.0/cassandra/initialize/initSingleDS.html
vi /opt/apache-cassandra-3.9/conf/cassandra.yaml

1.choose a cluster_name
2.set the seeds (by using hostname -I to get the IP ) ( if you have a cluster of 3 make 1 one of them seed)
3.set the listen_address to the public ip
4.set this snitch in cassandra.yaml  "endpoint_snitch: GossipingPropertyFileSnitch"
5.set this  "rpc_address: 0.0.0.0"
5.set this  "broadcast_rpc_address: public ip"
5. edit these props in cassandra.yaml  to below values
batch_size_warn_threshold_in_kb: 20
batch_size_fail_threshold_in_kb: 50
6.remove this file  rm  /opt/apache-cassandra-3.9/conf/cassandra-topology.properties

7.change the password of superuser based on this artical : https://docs.datastax.com/en/cassandra/2.1/cassandra/security/security_config_native_authenticate_t.html
7.1 change this to "authenticator : PasswordAuthenticator"

7.2 login using default super password : run cassandra and cqlsh -u cassandra -p cassandra
7.3 create new superuser and drop old one
select * from system_auth.roles;
create a role aka user (https://docs.datastax.com/en/cql/3.3/cql/cql_reference/cqlCreateRole.html)
CREATE ROLE hossein WITH SUPERUSER = true and PASSWORD = '67000099Aa?1BuffaloShirazNY67000099Aa?1'  AND LOGIN = true;
7.4 exit the cqlsh and login with hossein  cqlsh -u hossein -p 67000099Aa?1BuffaloShirazNY67000099Aa?1 and drop the cassandra role
DROP ROLE cassandra;



7.5 create these test cluster and table and data , to verify cluster works
CREATE KEYSPACE verifyCluster WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3'}  AND durable_writes = true;
CREATE TABLE verifyCluster.test (
    id text,
    name text,
    PRIMARY KEY (id, name)
) WITH CLUSTERING ORDER BY (name DESC);
INSERT INTO verifyCluster.test (id,name) VALUES ('1234','JohnDoe');
select * from verifyCluster.test;

7.6 check status of cluster :  nodetool status


7.3 (very important : do this after you have a cluster up) increase replication factor of system_auth keyspace to number of nodes in cluster
describe system_auth;
 ALTER KEYSPACE system_auth WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3'}  AND durable_writes = true;

#usefule commands for dealing with cassandra
how to delete data in cased of bad startup :  rm -rf /opt/apache-cassandra-3.9/data/data/system/*
how to start cassandra : /opt/apache-cassandra-3.9/bin/cassandra -R
how to kill cassandra : ps -ef | awk '/[c]assandra/{print $2}' | xargs kill -9
ps -ef | awk '/[c]assandra/{print $2}' | xargs kill -9 && /opt/apache-cassandra-3.9/bin/cassandra -R


#how to export/import schema in cassandra

To export keyspace schema:
```
cqlsh -e "DESC KEYSPACE user" > user_schema.cql
```
To export entire database schema:
```
cqlsh -e "DESC SCHEMA" > db_schema.cql
```
To import a file
```
cqlsh -e "source '/root/db_schema.cql'"
```
##nodetool commands

nodetool gossipinfo
nodetool status
nodetool rebuild
nodetool repair
nodetool ring
nodetool describecluster
nodetool enablegossip
nodetool netstats

#how to connect to cassnadra on alpha3
/root/apache-cassandra-3.11.0/bin/cqlsh 10.136.64.50
