1.how to install influx db

wget https://dl.influxdata.com/influxdb/releases/influxdb_1.5.2_amd64.deb && sudo dpkg -i influxdb_1.5.2_amd64.deb
nohup influxd -config /etc/influxdb/influxdb.conf &


2. run influx shell
influx
CREATE DATABASE test;
CREATE USER admin5 WITH PASSWORD 'admin5'
GRANT [READ,WRITE,ALL] ON <database_name> TO <username>
GRANT ALL ON test TO admin5
use test;
show measurements;

select * from cpu_load_short;


3.how to write multiple points

curl -i -XPOST 'http://localhost:8086/write?db=test' --data-binary 'cpu_load_short,host=server02 value=0.67
cpu_load_short,host=server02,region=us-west value=0.55 1422568543702900257
cpu_load_short,direction=in,host=server01,region=us-west value=2.0 1422568543702900257'

4.tutorial for influx db
https://docs.influxdata.com/influxdb/v1.2/guides/writing_data/

#how to create continuous query

#List of CQs needed
DROP CONTINUOUS QUERY top_publisher_domain_per_month_daily_query ON test
CREATE CONTINUOUS QUERY "top_publisher_domain_per_month_daily_query" ON "test" RESAMPLE EVERY 10m BEGIN SELECT sum(count) INTO "top_publisher_domain_per_month_daily" from test group by siteDomain, deviceType, client_id, time(1d) END



#how to drop continuous query
//DROP CONTINUOUS QUERY <cq_name> ON <database_name>
DROP CONTINUOUS QUERY top_publisher_domain_per_month_query ON test
DROP CONTINUOUS QUERY top_publisher_domain_per_month_daily ON test

SHOW CONTINUOUS QUERIES

#How to install grafana :
wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_4.4.1_amd64.deb
sudo apt-get install -y adduser libfontconfig
sudo dpkg -i grafana_4.4.1_amd64.deb

    change the port to 3100 in here  vi /etc/grafana/grafana.ini (you have to uncomment the line that starts ; around http port)
      because aerospike uses 3000 already

    check the logs here : tail -f /var/log/grafana/grafana.log
    check if the process has started : ps -ef | grep grafana
sudo service grafana-server start

then you can login here : http://67.205.161.25:3100/
username/password is : admin/admin

**In grafana : you have to use the public ip address of influxdb as the data source

the sign for save dashboard is hidden for some reason

http://67.205.161.25:3100/dashboard/db/new-dashboard

grafana username : admin
grafana password : ******Aa?1

#How to install Kapacitor
wget https://dl.influxdata.com/kapacitor/releases/kapacitor_1.3.1_amd64.deb && sudo dpkg -i kapacitor_1.3.1_amd64.deb && kapacitord config > kapacitor.generated.conf

#Extra config for kapacitor
1.now you need to set your email address in configs, Look at EmailSender class for yahoo email setup
2.change bind-address to 9096,  by default its 9092 which is used by kafka

#How to run and test if its working
sudo service kapacitor start && kapacitor list tasks


#how to install chronograf
wget https://dl.influxdata.com/chronograf/releases/chronograf_1.3.7.0_amd64.deb
sudo dpkg -i chronograf_1.3.7.0_amd64.deb
sudo systemctl start chronograf

address for chronograf  http://67.205.161.25:8888/
