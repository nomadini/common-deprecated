

#official 16.04 image of ubuntu
docker pull cdbishop89/docker-ubuntu-16.04

#how to go to bash of docker image with id 02d7b18aa1a5
docker run -it 02d7b18aa1a5 /bin/bash

#how to build an image from a Dockerfile ( which is in HowTo/Docker)
#make sure you build an empty dir as dockerContext
#sign out of your docker if running on mac
mkdir dockerContext
cd dockerContext && sudo docker build -t bidder .  (run this in the directory where Dockerfile exists)


docker images
docker image prune -a  (removes dangling and unused images)

#how to commit your changes in a container and tag it as an image
 docker commit containerId  bidder:version1

#how to push your image
docker tag #imageId taabodim/bidder:version1
docker push bidder:version1

#how to save your docker image locally
 docker commit containerId  bidder:versionX && docker tag #imageId taabodim/bidder:versionX && docker save bidder:versionX > bidder_versionX.tar

#how to load docker image from file ?
docker load --input bidder_version1.tar
