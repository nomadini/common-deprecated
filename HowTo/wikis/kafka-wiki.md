#where is kafka deployed
on server 10.136.46.157

#Good Documentation
https://github.com/edenhill/librdkafka/wiki/FAQ

#how to list topic in kafka
 /opt/kafka_2.11-0.9.0.1/bin/kafka-topics.sh --list --zookeeper localhost:2181

#how to describe topic
/opt/kafka_2.11-0.9.0.1/bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic devicesToScore

#how to list consumer groups
/opt/kafka_2.11-0.9.0.1/bin/kafka-consumer-groups.sh  --list --zookeeper localhost:2181


https://cwiki.apache.org/confluence/display/KAFKA/System+Tools

#how to get offset of a topic
/opt/kafka_2.11-0.9.0.1/bin/kafka-run-class.sh kafka.tools.GetOffsetShell --topic devicesToScore --broker-list localhost:9092 --time -1

#How to set ttl on topic
/opt/kafka_2.11-0.9.0.1/bin/kafka-configs.sh --alter --entity-type topics --entity-name devicesToScore --add-config 'retention.ms=86400000' --zookeeper localhost:2181

/opt/kafka_2.11-0.9.0.1/bin/kafka-run-class.sh kafka.tools.ConsumerOffsetChecker --broker-info --group scoring1 --topic devicesToScore --zookeeper localhost:2181


/opt/kafka_2.11-0.9.0.1/bin/kafka-run-class.sh kafka.admin.ConsumerGroupCommand --list --new-consumer --bootstrap-server localhost:9092
