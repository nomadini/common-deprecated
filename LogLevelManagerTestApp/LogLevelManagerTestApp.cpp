//
// Created by Mahmoud Taabodi on 3/4/16.
//

#include "GUtil.h"
#include "ConfigService.h"
#include "TempUtil.h"
#include "LogLevelManager.h"
#include <thread>
#include "GUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

#include "spdlog/spdlog.h"
#include <ostream>

class MLogger
        // : public std::ostream
{
public:

std::shared_ptr<spdlog::logger> async_us_ip_file_logger;

MLogger() {
        async_us_ip_file_logger =
                //- a new file is created every day on 2:30am
                // spdlog::daily_logger_mt("daily_logger", "/tmp/logs", 2, 30);

                //100 Mb 3 times rotatation
                spdlog::rotating_logger_mt("daily_logger", "/tmp/logs.log", 1048576 * 100, 3);
        size_t q_size = 4096;//queue size must be power of 2
        spdlog::set_pattern("[%H:%M:%S %z] [thread %t] %v");
        // spdlog::set_pattern("%v");
        spdlog::set_async_mode(q_size);
}

friend MLogger& operator <<(MLogger& log, const std::string & value);
friend MLogger& operator <<(MLogger& log, const int & value);
friend MLogger& operator <<(MLogger& log, const float & value);
friend MLogger& operator <<(MLogger& log, const double & value);
friend MLogger& operator <<(MLogger& log, const bool & value);
friend MLogger& operator <<(MLogger& log, const long & value);

};

MLogger& operator <<(MLogger& log, const std::string & value) {

        log.async_us_ip_file_logger->info(value);
        return log;
}

MLogger& operator <<(MLogger& log, const int & value) {

        log.async_us_ip_file_logger->info(value);
        return log;
}

MLogger& operator <<(MLogger& log, const float & value) {

        log.async_us_ip_file_logger->info(value);
        return log;
}

MLogger& operator <<(MLogger& log, const double & value) {

        log.async_us_ip_file_logger->info(value);
        return log;
}

MLogger& operator <<(MLogger& log, const bool & value) {

        log.async_us_ip_file_logger->info(value);
        return log;
}

MLogger& operator <<(MLogger& log, const long & value) {

        log.async_us_ip_file_logger->info(value);
        return log;
}

int main(int argc, char* argv[]) {
        MLogger mlog;
        long n = 2;
        mlog<<"hi mahmoud2"<<1<<n<<3.1f << true<< false<<"reza";
        // spdlog::get("daily_logger")<<"daily_logger : hi mahmoud2"<<1<<n<<3.1f << true<< false<<"reza";

//         TempUtil::configureLogging("LogLevelManagerTestApp", argv);
//
//
// //    MLOG(3) << "you shouldnt see this log"; //you shouldn't see this
//         assertAndThrow(!LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::enableAll();
//         assertAndThrow(LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::disableAll();
//         assertAndThrow(!LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::enableAll();
//         assertAndThrow(LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::disableAll();
//         assertAndThrow(!LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::enableOnly("LogLevelManagerTestApp.cpp");
//         assertAndThrow(LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::disable("LogLevelManagerTestApp.cpp");
//         assertAndThrow(!LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::enabledPartialLogNames ()->insert ("log");
//         assertAndThrow(LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::disableAll();
//         assertAndThrow(!LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::enabledPartialLogNames ()->insert ("Manager");
//         assertAndThrow(!LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::enableAll();
//         assertAndThrow(LogLevelManager::logOrNot(_F_));
//
//         LogLevelManager::enabledPartialLogNames ()->insert ("Manager");
//         assertAndThrow(LogLevelManager::logOrNot(_F_));



        // LogLevelManager::printAllLogLevels();
}
