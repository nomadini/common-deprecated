#ifndef MetricHealthChecker_H_
#define MetricHealthChecker_H_

#include <memory>
#include <string>
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
#include "Object.h"
#include "AtomicBoolean.h"
class EntityToModuleStateStatsPersistenceService;
class DataReloadService;
namespace gicapods { class ConfigService; }

#include <boost/thread.hpp>
#include <thread>
class ComparatorService;
class TgFilterMeasuresService;
class BeanFactory;
class ModuleContainer;
class MetricHealthChecker;



class MetricHealthChecker : public std::enable_shared_from_this<MetricHealthChecker>
        , public Poco::Util::ServerApplication
{

public:
unsigned short port;
gicapods::ConfigService* configService;
EntityToModuleStateStats* entityToModuleStateStats;

MetricHealthChecker(unsigned short port);
virtual ~MetricHealthChecker();
void initFactory();
std::string getName();

int main(const std::vector<std::string> &args);

private:
bool _helpRequested;
};

#endif
